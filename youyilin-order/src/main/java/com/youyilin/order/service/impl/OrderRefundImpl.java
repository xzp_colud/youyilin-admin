package com.youyilin.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.exception.Assert;
import com.youyilin.common.utils.SecurityUtil;
import com.youyilin.order.entity.OrderRefund;
import com.youyilin.order.mapper.OrderRefundMapper;
import com.youyilin.order.service.OrderRefundService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * 订单退款 实现类
 */
@Service
public class OrderRefundImpl extends ServiceImpl<OrderRefundMapper, OrderRefund> implements OrderRefundService {

    @Override
    public void validateExcludeFail(Long sourceId) {
        long count = super.count(new LambdaQueryWrapper<OrderRefund>()
                .eq(OrderRefund::getSourceId, sourceId)
                .in(OrderRefund::getStatus, OrderRefund.Status.getWaitAndSuccess()));
        Assert.isTrue(count == 0L, "有未处理的退款");
    }

    @Override
    public OrderRefund validateRefund(Long id) {
        OrderRefund refund = super.getById(id);
        Assert.notNull(refund, "退款单不存在");

        return refund;
    }

    @Override
    public OrderRefund validateCanCheck(Long id) {
        OrderRefund refund = this.validateRefund(id);
        boolean statusFlag = StringUtils.isNotBlank(refund.getStatus()) && refund.getStatus().equals(OrderRefund.Status.WAITING.name());
        Assert.isTrue(statusFlag, "当前状态不允许审核");

        return refund;
    }

    @Override
    public void updateSuccess(Long id, String remark) {
        this.doSuccessOrFail(id, remark, OrderRefund.Status.SUCCESS.name());
    }

    @Override
    public void updateFail(Long id, String remark) {
        this.doSuccessOrFail(id, remark, OrderRefund.Status.FAIL.name());
    }

    private void doSuccessOrFail(Long id, String remark, String status) {
        boolean paramsFlag = id != null && StringUtils.isNotBlank(remark);
        Assert.isTrue(paramsFlag, "参数错误");
        // 审核人信息
        Long checkId = SecurityUtil.getUserId();
        String checkName = SecurityUtil.getUsername();
        Date checkDate = new Date();

        super.update(new LambdaUpdateWrapper<OrderRefund>()
                .set(OrderRefund::getStatus, status)
                .set(OrderRefund::getCheckId, checkId)
                .set(OrderRefund::getCheckName, checkName)
                .set(OrderRefund::getCheckDate, checkDate)
                .set(OrderRefund::getCheckRemark, remark)
                .eq(OrderRefund::getId, id));
    }
}
