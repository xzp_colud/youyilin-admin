package com.youyilin.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.order.mapper.OrderDiscountMapper;
import com.youyilin.order.entity.OrderDiscount;
import com.youyilin.order.service.OrderDiscountService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderDiscountImpl extends ServiceImpl<OrderDiscountMapper, OrderDiscount> implements OrderDiscountService {

    @Override
    public List<OrderDiscount> listByOrderId(Long orderId) {
        return super.list(new LambdaQueryWrapper<OrderDiscount>().eq(OrderDiscount::getOrderId, orderId));
    }

    @Override
    public void delByOrderId(Long orderId) {
        super.remove(new LambdaQueryWrapper<OrderDiscount>().eq(OrderDiscount::getOrderId, orderId));
    }

    @Override
    public void saveOrderDiscount(Long orderId, String sn, Long discountAmount, String remark) {
        OrderDiscount save = new OrderDiscount();
        save.setOrderId(orderId)
                .setOrderSn(sn)
                .setAmount(discountAmount)
                .setRemark(remark);

        super.save(save);
    }
}
