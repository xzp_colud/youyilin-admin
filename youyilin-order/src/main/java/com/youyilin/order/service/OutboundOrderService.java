package com.youyilin.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.order.entity.OutboundOrder;

/**
 * 出库单 服务类
 */
public interface OutboundOrderService extends IService<OutboundOrder> {

    /**
     * 验证出库单是否存在
     *
     * @param id 出库单ID
     * @return OutboundOrder
     */
    OutboundOrder validateOutbound(Long id);

    /**
     * 验证出库单是否可以编辑
     *
     * @param id 出库单ID
     * @return OutboundOrder
     */
    OutboundOrder validateEdit(Long id);

    /**
     * 验证出库单是否可以确认
     *
     * @param id 出库单ID
     * @return OutboundOrder
     */
    OutboundOrder validateOutboundConfirm(Long id);

    /**
     * 验证出库单是否可以出库
     *
     * @param id 出库单ID
     * @return OutboundOrder
     */
    OutboundOrder validateOutboundInventory(Long id);

    /**
     * 验证出库单是否可以取消
     *
     * @param id 出库单ID
     * @return OutboundOrder
     */
    OutboundOrder validateOutboundCancel(Long id);

    /**
     * 更新出库单状态为已确认
     *
     * @param id 出库单ID
     */
    void updateConfirm(Long id);

    /**
     * 更新出库单状态为已取消
     *
     * @param id 出库单ID
     */
    void updateCancel(Long id);

    /**
     * 更新出库单状态为已完成
     *
     * @param id 出库单ID
     */
    void updateFinish(Long id);
}
