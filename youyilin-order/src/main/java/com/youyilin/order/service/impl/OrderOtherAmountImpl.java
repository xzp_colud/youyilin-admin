package com.youyilin.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.order.mapper.OrderOtherAmountMapper;
import com.youyilin.order.entity.OrderOtherAmount;
import com.youyilin.order.service.OrderOtherAmountService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderOtherAmountImpl extends ServiceImpl<OrderOtherAmountMapper, OrderOtherAmount> implements OrderOtherAmountService {

    @Override
    public List<OrderOtherAmount> listByOrderId(Long orderId) {
        return super.list(new LambdaQueryWrapper<OrderOtherAmount>().eq(OrderOtherAmount::getOrderId, orderId));
    }

    @Override
    public void deleteByOrderId(Long orderId) {
        super.remove(new LambdaQueryWrapper<OrderOtherAmount>().eq(OrderOtherAmount::getOrderId, orderId));
    }
}
