package com.youyilin.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.order.entity.PurchaseOrder;

/**
 * 采购订单 服务类
 */
public interface PurchaseOrderService extends IService<PurchaseOrder> {

    /**
     * 验证订单
     *
     * @param id 订单ID
     * @return Purchase
     */
    PurchaseOrder validatePurchaseOrder(Long id);

    /**
     * 验证采购单是否能编辑
     *
     * @param id 订单ID
     * @return PurchaseOrder
     */
    PurchaseOrder validatePurchaseOrderEdit(Long id);

    /**
     * 验证采购单是否能确认
     *
     * @param id 订单ID
     * @return PurchaseOrder
     */
    PurchaseOrder validatePurchaseOrderConfirm(Long id);

    /**
     * 验证采购单是否能入库
     *
     * @param id 订单ID
     * @return PurchaseOrder
     */
    PurchaseOrder validatePurchaseOrderInventory(Long id);

    /**
     * 验证采购单是否能取消
     *
     * @param id 订单ID
     * @return Purchase
     */
    PurchaseOrder validatePurchaseOrderCancel(Long id);

    /**
     * 更新订单状态为已确认
     *
     * @param id 订单ID
     */
    void updateConfirm(Long id);

    /**
     * 更新订单状态为已取消
     *
     * @param id 订单ID
     */
    void updateCancel(Long id);

    /**
     * 更新订单状态为已完成
     *
     * @param id 订单ID
     */
    void updateFinish(Long id);
}
