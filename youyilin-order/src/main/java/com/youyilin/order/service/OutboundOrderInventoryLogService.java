package com.youyilin.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.order.entity.OutboundOrderInventoryLog;

import java.util.List;

public interface OutboundOrderInventoryLogService extends IService<OutboundOrderInventoryLog> {

    /**
     * 按出库单ID查询出库明细
     *
     * @param outboundId 出库单ID
     * @return ArrayList
     */
    List<OutboundOrderInventoryLog> listByOutboundId(Long outboundId);

    /**
     * 按出库单子集ID查询出库明细
     *
     * @param outboundItemId 出库单子集ID
     * @return ArrayList
     */
    List<OutboundOrderInventoryLog> listByOutboundItemId(Long outboundItemId);

    /**
     * 按出库单子集ID查询出库明细
     *
     * @param outboundItemIds 出库单子集ID
     * @return ArrayList
     */
    List<OutboundOrderInventoryLog> listByOutboundItemIds(List<Long> outboundItemIds);
}
