package com.youyilin.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.order.entity.PurchaseOrderSource;

import java.util.List;

public interface PurchaseOrderSourceService extends IService<PurchaseOrderSource> {

    /**
     * 根据源单ID和源单类型统计数量
     *
     * @param sourceId   源单ID
     * @param sourceType 源单类型
     */
    Long countBySourceIdAndType(Long sourceId, String sourceType);

    /**
     * 按采购单ID查询
     *
     * @param purchaseId 采购单ID
     * @param sourceType 来源类型
     * @return ArrayList
     */
    List<PurchaseOrderSource> listByPurchaseIdAndType(Long purchaseId, String sourceType);

    /**
     * 按采购单删除
     *
     * @param purchaseId 采购单ID
     * @param sourceType 来源类型
     */
    void delByPurchaseIdAndType(Long purchaseId, String sourceType);
}
