package com.youyilin.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.order.entity.OrderRefund;

/**
 * 订单退款 服务类
 */
public interface OrderRefundService extends IService<OrderRefund> {

    /**
     * 是否存在未处理的记录
     */
    void validateExcludeFail(Long sourceId);

    /**
     * 验证退款单是否存在
     *
     * @param id 退款单ID
     * @return OrderRefund
     */
    OrderRefund validateRefund(Long id);

    /**
     * 验证退款单是否可以审核
     *
     * @param id 退款单ID
     * @return OrderRefund
     */
    OrderRefund validateCanCheck(Long id);

    /**
     * 更新状态为审核成功
     *
     * @param id     退款单ID
     * @param remark 审核备注
     */
    void updateSuccess(Long id, String remark);

    /**
     * 更新状态为审核失败
     *
     * @param id     退款单ID
     * @param remark 审核备注
     */
    void updateFail(Long id, String remark);
}
