package com.youyilin.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.order.entity.CustomerOrderItem;

import java.util.List;

public interface CustomerOrderItemService extends IService<CustomerOrderItem> {

    /**
     * 按订单ID查询
     *
     * @param orderId 订单ID
     * @return CustomerOrderItem
     */
    List<CustomerOrderItem> listByOrderId(Long orderId);

    /**
     * 按订单IDS查询
     *
     * @param orderIds 订单IDS
     * @return ArrayList
     */
    List<CustomerOrderItem> listByOrderIds(List<Long> orderIds);

    /**
     * 出库完成
     */
    void updateOutboundFinishByIds(List<Long> ids);
}
