package com.youyilin.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.order.entity.OrderDiscount;

import java.util.List;

public interface OrderDiscountService extends IService<OrderDiscount> {

    /**
     * 根据订单id查询优惠明细
     *
     * @param orderId 订单id
     */
    List<OrderDiscount> listByOrderId(Long orderId);

    /**
     * 根据订单id删除优惠明细
     *
     * @param orderId 订单id
     */
    void delByOrderId(Long orderId);

    /**
     * 保存订单优惠
     *
     * @param orderId        订单id
     * @param sn             订单编号
     * @param discountAmount 优惠金额
     * @param remark         备注
     */
    void saveOrderDiscount(Long orderId, String sn, Long discountAmount, String remark);
}
