package com.youyilin.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.order.entity.OrderInventoryLog;
import com.youyilin.order.mapper.OrderInventoryLogMapper;
import com.youyilin.order.service.OrderInventoryLogService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 采购单入库日志 实现类
 */
@Service
public class OrderInventoryLogImpl extends ServiceImpl<OrderInventoryLogMapper, OrderInventoryLog> implements OrderInventoryLogService {

    @Override
    public List<OrderInventoryLog> listByPurchaseId(Long purchaseId) {
        return super.list(new LambdaQueryWrapper<OrderInventoryLog>().eq(OrderInventoryLog::getOrderId, purchaseId));
    }

    @Override
    public List<OrderInventoryLog> listByPurchaseItemId(Long purchaseItemId) {
        return super.list(new LambdaQueryWrapper<OrderInventoryLog>().eq(OrderInventoryLog::getOrderItemId, purchaseItemId));
    }
}
