package com.youyilin.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.order.entity.OutboundOrderItemDetail;

import java.util.List;

public interface OutboundOrderItemDetailService extends IService<OutboundOrderItemDetail> {

    /**
     * 按源ID查询
     *
     * @param sourceIds 源IDS
     * @return ArrayList
     */
    List<OutboundOrderItemDetail> listBySourceIds(List<Long> sourceIds);

    /**
     * 按出库单ID查询
     *
     * @param outboundId 订单ID
     * @return ArrayList
     */
    List<OutboundOrderItemDetail> listByOutboundId(Long outboundId);

    /**
     * 按出库单子集ID查询
     *
     * @param outboundItemId 出库单明细ID
     * @return ArrayList
     */
    List<OutboundOrderItemDetail> listByOutboundItemId(Long outboundItemId);

    /**
     * 所有已出库的分组
     *
     * @param outboundId 出库单ID
     * @return ArrayList
     */
    List<Long> refreshInventoryOutGroupId(Long outboundId);

    /**
     * 更新状态为已确认
     *
     * @param outboundId 出库单ID
     */
    void updateConfirmByOutboundId(Long outboundId);

    /**
     * 更新状态为已取消
     *
     * @param outboundId 出库单ID
     */
    void updateCancelByOutboundId(Long outboundId);

    /**
     * 更新状态为已出库
     */
    void updateOutByOutboundItemId(Long outboundItemId);

    /**
     * 按出库单ID删除
     *
     * @param outboundId 订单ID
     */
    void delByOutboundId(Long outboundId);
}
