package com.youyilin.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.exception.Assert;
import com.youyilin.order.entity.PurchaseOrder;
import com.youyilin.order.enums.PurchaseOrderSourceTypeEnum;
import com.youyilin.order.enums.PurchaseOrderStatusEnum;
import com.youyilin.order.mapper.PurchaseOrderMapper;
import com.youyilin.order.service.PurchaseOrderService;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * 采购订单 实现类
 */
@Service
public class PurchaseOrderImpl extends ServiceImpl<PurchaseOrderMapper, PurchaseOrder> implements PurchaseOrderService {

    @Override
    public PurchaseOrder validatePurchaseOrder(Long id) {
        PurchaseOrder po = super.getById(id);
        Assert.notNull(po, "采购单不存在");

        return po;
    }

    /**
     * 单一验证订单状态
     */
    private PurchaseOrder validatePurchaseOrder(Long id, Integer status) {
        PurchaseOrder po = this.validatePurchaseOrder(id);
        boolean statusFlag = po.getStatus() != null && status.equals(po.getStatus());
        Assert.isTrue(statusFlag, "采购单状态异常");

        return po;
    }

    @Override
    public PurchaseOrder validatePurchaseOrderEdit(Long id) {
        PurchaseOrder purchaseOrder = this.validatePurchaseOrder(id, PurchaseOrderStatusEnum.NO_CONFIRM.getCode());
        boolean canEdit = PurchaseOrderSourceTypeEnum.isCanEdit(purchaseOrder.getSourceType());
        Assert.isTrue(canEdit, "该采购单不支持编辑");

        return purchaseOrder;
    }

    @Override
    public PurchaseOrder validatePurchaseOrderConfirm(Long id) {
        return this.validatePurchaseOrder(id, PurchaseOrderStatusEnum.NO_CONFIRM.getCode());
    }

    @Override
    public PurchaseOrder validatePurchaseOrderInventory(Long id) {
        return this.validatePurchaseOrder(id, PurchaseOrderStatusEnum.PROCESSING.getCode());
    }

    @Override
    public PurchaseOrder validatePurchaseOrderCancel(Long id) {
        PurchaseOrder purchaseOrder = this.validatePurchaseOrder(id, PurchaseOrderStatusEnum.NO_CONFIRM.getCode());
        boolean canCancel = PurchaseOrderSourceTypeEnum.isCanCancel(purchaseOrder.getSourceType());
        Assert.isTrue(canCancel, "该采购单不支持取消");

        return purchaseOrder;
    }

    @Override
    public void updateConfirm(Long id) {
        super.update(new LambdaUpdateWrapper<PurchaseOrder>()
                .set(PurchaseOrder::getStatus, PurchaseOrderStatusEnum.PROCESSING.getCode())
                .set(PurchaseOrder::getConfirmDate, new Date())
                .eq(PurchaseOrder::getId, id));
    }

    @Override
    public void updateFinish(Long id) {
        super.update(new LambdaUpdateWrapper<PurchaseOrder>()
                .set(PurchaseOrder::getStatus, PurchaseOrderStatusEnum.FINISH.getCode())
                .set(PurchaseOrder::getFinishDate, new Date())
                .eq(PurchaseOrder::getId, id));
    }

    @Override
    public void updateCancel(Long id) {
        super.update(new LambdaUpdateWrapper<PurchaseOrder>()
                .set(PurchaseOrder::getStatus, PurchaseOrderStatusEnum.CANCEL.getCode())
                .set(PurchaseOrder::getCancelDate, new Date())
                .eq(PurchaseOrder::getId, id));
    }
}
