package com.youyilin.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.bean.Page;
import com.youyilin.common.exception.ApiException;
import com.youyilin.order.enums.CustomerOrderStatusEnum;
import com.youyilin.order.enums.OrderStatusEnum;
import com.youyilin.order.enums.CommonOutboundStatusEnum;
import com.youyilin.order.mapper.CustomerOrderMapper;
import com.youyilin.order.entity.CustomerOrder;
import com.youyilin.order.service.CustomerOrderService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class CustomerOrderImpl extends ServiceImpl<CustomerOrderMapper, CustomerOrder> implements CustomerOrderService {

    @Override
    public Integer getTotal(Page<CustomerOrder> page) {
        return baseMapper.getTotal(page);
    }

    @Override
    public List<CustomerOrder> getPage(Page<CustomerOrder> page) {
        return baseMapper.getPage(page);
    }

    @Override
    public List<CustomerOrder> listByOutboundId(Long outboundId) {
        return super.list(new LambdaQueryWrapper<CustomerOrder>().eq(CustomerOrder::getOutboundId, outboundId));
    }

    @Override
    public void updateStatusCancel(Long id) {
        super.update(new LambdaUpdateWrapper<CustomerOrder>()
                .set(CustomerOrder::getStatus, OrderStatusEnum.CANCEL.getCode())
                .set(CustomerOrder::getCancelTime, new Date())
                .eq(CustomerOrder::getId, id));
    }

    @Override
    public void countDown() {
        baseMapper.countDown();
    }

    @Override
    public void updateStatusPaySuccess(Long id) {
        super.update(new LambdaUpdateWrapper<CustomerOrder>()
                .set(CustomerOrder::getStatus, CustomerOrderStatusEnum.PAID_NO_SEND.getCode())
                .set(CustomerOrder::getPayTime, new Date())
                .eq(CustomerOrder::getId, id));
    }

    @Override
    public void updateStatusSend(Long id) {
        super.update(new LambdaUpdateWrapper<CustomerOrder>()
                .set(CustomerOrder::getStatus, CustomerOrderStatusEnum.SEND_NO_RECEIVED.getCode())
                .eq(CustomerOrder::getId, id));
    }

    @Override
    public void updateStatusSend(List<Long> ids, String expressName, String expressNo, Date sendDate) {
        super.update(new LambdaUpdateWrapper<CustomerOrder>()
                .set(CustomerOrder::getStatus, CustomerOrderStatusEnum.SEND_NO_RECEIVED.getCode())
                .set(CustomerOrder::getExpressName, expressName)
                .set(CustomerOrder::getExpressNo, expressNo)
                .set(CustomerOrder::getSendTime, sendDate)
                .in(CustomerOrder::getId, ids));
    }

    @Override
    public void updateStatusReceive(Long id) {
        super.update(new LambdaUpdateWrapper<CustomerOrder>()
                .set(CustomerOrder::getStatus, CustomerOrderStatusEnum.RECEIVED.getCode())
                .eq(CustomerOrder::getId, id));
    }

    @Override
    public void updateStatusFinish(Long id) {
        super.update(new LambdaUpdateWrapper<CustomerOrder>()
                .set(CustomerOrder::getStatus, OrderStatusEnum.FINISH.getCode())
                .eq(CustomerOrder::getId, id));
    }

    @Override
    public CustomerOrder validateOrder(Long id) {
        CustomerOrder co = super.getById(id);
        if (co == null) {
            throw new ApiException("订单信息不存在");
        }

        return co;
    }

    @Override
    public CustomerOrder validateStatus(Long id) {
        CustomerOrder co = this.validateOrder(id);
        if (co.getStatus() == null) {
            throw new ApiException("订单异常");
        }
        return co;
    }

    @Override
    public CustomerOrder validateStatusToCancel(Long id) {
        CustomerOrder co = this.validateStatus(id);
        if (!co.getStatus().equals(CustomerOrderStatusEnum.DEFAULT.getCode())) {
            throw new ApiException("订单状态异常");
        }
        return co;
    }

    @Override
    public CustomerOrder validateStatusToPay(Long id) {
        return this.validateStatusToCancel(id);
    }

    @Override
    public CustomerOrder validateStatusToSend(Long id) {
        CustomerOrder co = this.validateStatus(id);
        if (!co.getStatus().equals(CustomerOrderStatusEnum.PAID_NO_SEND.getCode())) {
            throw new ApiException("订单状态异常");
        }
        return co;
    }

    @Override
    public CustomerOrder validateStatusToReceive(Long id) {
        CustomerOrder co = this.validateStatus(id);
        if (!co.getStatus().equals(CustomerOrderStatusEnum.SEND_NO_RECEIVED.getCode())) {
            throw new ApiException("订单状态异常");
        }
        return co;
    }

    @Override
    public CustomerOrder validateStatusToFinish(Long id) {
        CustomerOrder co = this.validateStatus(id);
        if (!co.getStatus().equals(CustomerOrderStatusEnum.RECEIVED.getCode())) {
            throw new ApiException("订单状态异常");
        }
        return co;
    }

    @Override
    public void updateOutboundProcessByOutboundId(Long outboundId, String outboundSn, List<Long> orderIds) {
        super.update(new LambdaUpdateWrapper<CustomerOrder>()
                .set(CustomerOrder::getOutboundId, outboundId)
                .set(CustomerOrder::getOutboundSn, outboundSn)
                .set(CustomerOrder::getOutboundStatus, CommonOutboundStatusEnum.PROCESSING.getCode())
                .in(CustomerOrder::getId, orderIds));
    }

    @Override
    public void updateOutboundFinishByIds(List<Long> orderIds) {
        super.update(new LambdaUpdateWrapper<CustomerOrder>()
                .set(CustomerOrder::getOutboundStatus, CommonOutboundStatusEnum.OUTBOUND.getCode())
                .in(CustomerOrder::getId, orderIds));
    }

    @Override
    public List<CustomerOrder> updateOutboundCancelByOutboundId(Long outboundId) {
        List<CustomerOrder> coList = this.listByOutboundId(outboundId);
        if (CollectionUtils.isNotEmpty(coList)) {
            List<CustomerOrder> updateCoList = new ArrayList<>();
            for (CustomerOrder item : coList) {
                CustomerOrder co = new CustomerOrder();
                co.setId(item.getId())
                        .setOutboundId(null)
                        .setOutboundSn(null)
                        .setOutboundStatus(CommonOutboundStatusEnum.CANCEL.getCode());

                updateCoList.add(co);
            }
            super.updateBatchById(updateCoList);
        }
        return coList;
    }

    @Override
    public List<CustomerOrder> updateOutboundByOutboundId(Long outboundId) {
        List<CustomerOrder> coList = this.listByOutboundId(outboundId);
        return this.updateStatusOutbound(coList);
    }

    @Override
    public List<CustomerOrder> updateOutboundByIds(List<Long> orderIds) {
        List<CustomerOrder> list = super.listByIds(orderIds);
        return this.updateStatusOutbound(list);
    }

    private List<CustomerOrder> updateStatusOutbound(List<CustomerOrder> coList) {
        if (CollectionUtils.isNotEmpty(coList)) {
            List<CustomerOrder> updateCoList = new ArrayList<>();
            for (CustomerOrder item : coList) {
                if (item.getOutboundId() == null || StringUtils.isBlank(item.getOutboundSn())) {
                    continue;
                }
                if (item.getStatus() == null || !item.getStatus().equals(CommonOutboundStatusEnum.PROCESSING.getCode())) {
                    continue;
                }
                CustomerOrder co = new CustomerOrder();
                co.setId(item.getId())
                        .setSn(item.getSn())
                        .setOutboundStatus(CommonOutboundStatusEnum.OUTBOUND.getCode());

                updateCoList.add(co);
            }
            super.updateBatchById(updateCoList);

            return updateCoList;
        }
        return new ArrayList<>();
    }
}
