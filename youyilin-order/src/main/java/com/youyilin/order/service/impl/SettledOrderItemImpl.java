package com.youyilin.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.order.entity.SettledOrderItem;
import com.youyilin.order.mapper.SettledOrderItemMapper;
import com.youyilin.order.service.SettledOrderItemService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 结款单明细 实现类
 */
@Service
public class SettledOrderItemImpl extends ServiceImpl<SettledOrderItemMapper, SettledOrderItem> implements SettledOrderItemService {

    @Override
    public List<SettledOrderItem> listBySettledId(Long settledId) {
        return super.list(new LambdaQueryWrapper<SettledOrderItem>().eq(SettledOrderItem::getSettledOrderId, settledId));
    }
}
