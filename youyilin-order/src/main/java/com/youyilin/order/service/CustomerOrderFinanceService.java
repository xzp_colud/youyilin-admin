package com.youyilin.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.order.entity.CustomerOrderFinance;

import java.util.List;

public interface CustomerOrderFinanceService extends IService<CustomerOrderFinance> {

    /**
     * 按订单查询
     *
     * @param orderId 订单ID
     * @return ArrayList
     */
    List<CustomerOrderFinance> listByOrderId(Long orderId);

    /**
     * 保存
     *
     * @param orderId        订单
     * @param orderSn        订单
     * @param sn             流水号
     * @param amount         金额
     * @param paySuccessTime 付款时间
     * @param typeName       支付方式
     * @param outTradeNo     请求单号
     */
    void saveFinance(Long orderId, String orderSn, String sn, Long amount, String paySuccessTime, String typeName, String outTradeNo);
}
