package com.youyilin.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.order.entity.OutboundOrderInventoryLog;
import com.youyilin.order.mapper.OutboundOrderInventoryLogMapper;
import com.youyilin.order.service.OutboundOrderInventoryLogService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OutboundOrderInventoryLogImpl extends ServiceImpl<OutboundOrderInventoryLogMapper, OutboundOrderInventoryLog> implements OutboundOrderInventoryLogService {

    @Override
    public List<OutboundOrderInventoryLog> listByOutboundId(Long outboundId) {
        return super.list(new LambdaQueryWrapper<OutboundOrderInventoryLog>().eq(OutboundOrderInventoryLog::getOutboundId, outboundId));
    }

    @Override
    public List<OutboundOrderInventoryLog> listByOutboundItemId(Long outboundItemId) {
        return super.list(new LambdaQueryWrapper<OutboundOrderInventoryLog>().eq(OutboundOrderInventoryLog::getOutboundItemId, outboundItemId));
    }

    @Override
    public List<OutboundOrderInventoryLog> listByOutboundItemIds(List<Long> outboundItemIds) {
        if (CollectionUtils.isEmpty(outboundItemIds)) {
            return new ArrayList<>();
        }
        return super.list(new LambdaQueryWrapper<OutboundOrderInventoryLog>().in(OutboundOrderInventoryLog::getOutboundItemId, outboundItemIds));
    }
}
