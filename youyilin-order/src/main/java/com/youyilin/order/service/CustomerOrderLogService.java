package com.youyilin.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.order.entity.CustomerOrderLog;

import java.util.List;

public interface CustomerOrderLogService extends IService<CustomerOrderLog> {

    /**
     * 按订单ID查询
     *
     * @param orderId 订单ID
     */
    List<CustomerOrderLog> listByOrderId(Long orderId);

    /**
     * 保存
     *
     * @param orderId 订单Id
     * @param orderSn 订单编号
     * @param title   标题
     * @param content 标题
     */
    void saveLog(Long orderId, String orderSn, String title, String content);
}
