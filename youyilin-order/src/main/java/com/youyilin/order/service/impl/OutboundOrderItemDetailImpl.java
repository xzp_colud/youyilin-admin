package com.youyilin.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.order.enums.OutboundOrderItemStatusEnum;
import com.youyilin.order.mapper.OutboundOrderItemDetailMapper;
import com.youyilin.order.entity.OutboundOrderItemDetail;
import com.youyilin.order.service.OutboundOrderItemDetailService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class OutboundOrderItemDetailImpl extends ServiceImpl<OutboundOrderItemDetailMapper, OutboundOrderItemDetail> implements OutboundOrderItemDetailService {

    @Override
    public List<OutboundOrderItemDetail> listBySourceIds(List<Long> sourceIds) {
        return super.list(new LambdaQueryWrapper<OutboundOrderItemDetail>().in(OutboundOrderItemDetail::getSourceId, sourceIds));
    }

    @Override
    public List<OutboundOrderItemDetail> listByOutboundId(Long outboundId) {
        return super.list(new LambdaQueryWrapper<OutboundOrderItemDetail>().eq(OutboundOrderItemDetail::getOrderId, outboundId));
    }

    @Override
    public List<OutboundOrderItemDetail> listByOutboundItemId(Long outboundItemId) {
        return super.list(new LambdaQueryWrapper<OutboundOrderItemDetail>().eq(OutboundOrderItemDetail::getOrderItemId, outboundItemId));
    }

    @Override
    public List<Long> refreshInventoryOutGroupId(Long outboundId) {
        List<OutboundOrderItemDetail> list = this.listByOutboundId(outboundId);
        if (CollectionUtils.isEmpty(list)) return new ArrayList<>();

        Map<Long, List<OutboundOrderItemDetail>> oidMap = list.stream().collect(Collectors.groupingBy(OutboundOrderItemDetail::getSourceGroupId));
        List<Long> sourceGroupIds = new ArrayList<>();
        for (Long key : oidMap.keySet()) {
            List<OutboundOrderItemDetail> existList = oidMap.get(key);

            boolean isFinish = true;
            for (OutboundOrderItemDetail itemDetail : existList) {
                if (itemDetail.getStatus() == null || itemDetail.getStatus().equals(OutboundOrderItemStatusEnum.NO_INVENTORY_OUT.getCode())) {
                    isFinish = false;
                    break;
                }
            }
            if (isFinish) {
                sourceGroupIds.add(key);
            }
        }
        return sourceGroupIds;
    }

    @Override
    public void updateConfirmByOutboundId(Long outboundId) {
        super.update(new LambdaUpdateWrapper<OutboundOrderItemDetail>()
                .set(OutboundOrderItemDetail::getStatus, OutboundOrderItemStatusEnum.NO_INVENTORY_OUT.getCode())
                .eq(OutboundOrderItemDetail::getOrderId, outboundId));
    }

    @Override
    public void updateCancelByOutboundId(Long outboundId) {
        super.update(new LambdaUpdateWrapper<OutboundOrderItemDetail>()
                .set(OutboundOrderItemDetail::getStatus, OutboundOrderItemStatusEnum.CANCEL.getCode())
                .eq(OutboundOrderItemDetail::getOrderId, outboundId));
    }

    @Override
    public void updateOutByOutboundItemId(Long outboundItemId) {
        super.update(new LambdaUpdateWrapper<OutboundOrderItemDetail>()
                .set(OutboundOrderItemDetail::getStatus, OutboundOrderItemStatusEnum.INVENTORY_OUT.getCode())
                .eq(OutboundOrderItemDetail::getOrderItemId, outboundItemId));
    }

    @Override
    public void delByOutboundId(Long outboundId) {
        super.remove(new LambdaQueryWrapper<OutboundOrderItemDetail>().eq(OutboundOrderItemDetail::getOrderId, outboundId));
    }
}
