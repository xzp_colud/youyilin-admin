package com.youyilin.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.common.bean.Page;
import com.youyilin.order.entity.CustomerOrder;

import java.util.Date;
import java.util.List;

public interface CustomerOrderService extends IService<CustomerOrder> {

    Integer getTotal(Page<CustomerOrder> page);

    List<CustomerOrder> getPage(Page<CustomerOrder> page);

    /**
     * 按出库单ID查询
     *
     * @param outboundId 出库单ID
     * @return ArrayList
     */
    List<CustomerOrder> listByOutboundId(Long outboundId);

    /**
     * 交易取消
     */
    void updateStatusCancel(Long id);

    /**
     * 倒计时取消
     */
    void countDown();

    /**
     * 付款成功
     */
    void updateStatusPaySuccess(Long id);

    /**
     * 发货
     */
    void updateStatusSend(Long id);

    /**
     * 发货
     */
    void updateStatusSend(List<Long> ids, String waybillNo, String expressNo, Date sendDate);

    /**
     * 收货
     */
    void updateStatusReceive(Long id);

    /**
     * 完成
     */
    void updateStatusFinish(Long id);

    CustomerOrder validateOrder(Long id);

    CustomerOrder validateStatus(Long id);

    CustomerOrder validateStatusToCancel(Long id);

    CustomerOrder validateStatusToPay(Long id);

    CustomerOrder validateStatusToSend(Long id);

    CustomerOrder validateStatusToReceive(Long id);

    CustomerOrder validateStatusToFinish(Long id);

    /**
     * 出库中
     */
    void updateOutboundProcessByOutboundId(Long outboundId, String outboundSn, List<Long> orderIds);

    /**
     * 出库完成
     */
    void updateOutboundFinishByIds(List<Long> updateOutboundOrderIds);

    /**
     * 出库取消
     */
    List<CustomerOrder> updateOutboundCancelByOutboundId(Long outboundId);

    /**
     * 已出库
     */
    List<CustomerOrder> updateOutboundByOutboundId(Long outboundId);

    /**
     * 已出库
     */
    List<CustomerOrder> updateOutboundByIds(List<Long> orderIds);
}
