package com.youyilin.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.order.entity.Order;

import java.util.List;

/**
 * 订单 服务类
 */
public interface OrderService extends IService<Order> {

    /**
     * 验证销售单是否存在
     *
     * @param id 销售单ID
     * @return Order
     */
    Order validateOrder(Long id);

    /**
     * 验证销售单是否可以修改
     *
     * @param id         销售单ID
     * @param customerId 客户ID
     * @return Order
     */
    Order validateCanEdit(Long id, Long customerId);

    /**
     * 验证销售单是否可以提交
     *
     * @param id 销售单ID
     * @return Order
     */
    Order validateOrderSubmit(Long id);

    /**
     * 验证销售单是否可以付款
     *
     * @param id 销售单ID
     * @return Order
     */
    Order validateOrderPay(Long id);

    /**
     * 验证销售单是否可以出库
     *
     * @param orderId 销售单ID
     * @return Order
     */
    Order validateOrderOutbound(Long orderId);

    /**
     * 验证销售单是否可以发货
     *
     * @param id 销售单ID
     * @return Order
     */
    Order validateOrderSend(Long id);

    /**
     * 验证销售单是否能修改收货地址
     *
     * @param id 销售单ID
     * @return Order
     */
    Order validateUpdateAddress(Long id);

    /**
     * 验证销售单是否可以取消
     *
     * @param id 销售单ID
     * @return Order
     */
    Order validateOrderCancel(Long id);

    /**
     * 验证销售单是否可以退款
     *
     * @param id 销售单ID
     * @return Order
     */
    Order validateCanRefund(Long id);

    /**
     * 验证订单是否可以结款
     *
     * @param ids        销售单ID集合
     * @param customerId 客户ID
     * @return ArrayList
     */
    List<Order> validateCanSettledBatch(List<Long> ids, Long customerId);

    /**
     * 验证订单是否可以生成采购
     *
     * @param id 销售单ID
     * @return Order
     */
    Order validateCanPurchase(Long id);

    /**
     * 验证订单是否可以生成大货采购
     *
     * @param id 销售单ID
     * @return Order
     */
    Order validateCanReadyPurchase(Long id);

    /**
     * 更新销售单状态为已提交
     *
     * @param id 销售单ID
     */
    void updateSubmit(Long id);

    /**
     * 更新销售单状态为已付款
     *
     * @param id          订单ID
     * @param financeFlag 结款状态
     * @param payType     支付方式
     */
    void updatePay(Long id, Integer financeFlag, String payType);

    /**
     * 更新销售单状态为已付款
     *
     * @param id          订单ID
     * @param financeFlag 结款状态
     * @param payType     支付方式
     */
    void updatePayReady(Long id, Integer financeFlag, String payType);

    /**
     * 更新销售单状态为备货入库中
     *
     * @param id 销售单ID
     */
    void updateReadyProcessing(Long id);

    /**
     * 更新销售单状态为大货采购完成
     *
     * @param id 销售单ID
     */
    void updateReadFinish(Long id);

    /**
     * 更新销售单状态为已发货
     *
     * @param orderId   订单ID
     * @param waybillId 货运单ID
     * @param waybillSn 货运单系统编号
     */
    void updateSend(Long orderId, Long waybillId, String waybillSn);

    /**
     * 更新销售单状态为已取消
     *
     * @param id 销售单ID
     */
    void updateCancel(Long id);

    /**
     * 修改销售单收货地址
     *
     * @param id               销售单ID
     * @param receiverName     收货人姓名
     * @param receiverPhone    收货人电话
     * @param receiverProvince 收货人省份
     * @param receiverCity     收货人城市
     * @param receiverArea     收货人区县
     * @param receiverDetail   收货人详细地址
     */
    void updateAddress(Long id, String receiverName, String receiverPhone, String receiverProvince, String receiverCity, String receiverArea, String receiverDetail);

    /**
     * 申请出库
     *
     * @param outboundId 出库单ID
     * @param outboundSn 出库单编号
     * @param id         订单ID
     */
    void updateOutboundProcessByOutboundId(Long outboundId, String outboundSn, Long id);

    /**
     * 出库完成
     *
     * @param orderIds 销售单IDS
     */
    void updateOutboundFinishByIds(List<Long> orderIds);

    /**
     * 发货完成
     *
     * @param orderIds 销售单IDS
     */
    void updateSendFinish(List<Long> orderIds);

    /**
     * 更新结款状态
     *
     * @param orderIds      销售单IDS
     * @param settledStatus 结款状态
     */
    void updateSettledStatus(List<Long> orderIds, Integer settledStatus);

    /**
     * 更新订单退款状态为退款中
     *
     * @param id 销售单ID
     */
    void updateRefundProcessing(Long id);

    /**
     * 更新订单退款状态为已退款
     *
     * @param id 销售单ID
     */
    void updateRefundSuccess(Long id);

    /**
     * 更新订单退款状态为已取消
     *
     * @param id 销售单ID
     */
    void updateRefundFail(Long id);
}
