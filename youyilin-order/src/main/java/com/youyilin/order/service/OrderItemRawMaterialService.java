package com.youyilin.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.order.entity.OrderItemRawMaterial;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 销售单子集原料明细 服务类
 */
public interface OrderItemRawMaterialService extends IService<OrderItemRawMaterial> {

    /**
     * 根据销售单查询
     *
     * @param orderId 销售单ID
     * @return ArrayList
     */
    List<OrderItemRawMaterial> listByOrderId(Long orderId);

    /**
     * 根据销售单明细查询
     *
     * @param orderItemId 销售单明细ID
     * @return ArrayList
     */
    List<OrderItemRawMaterial> listByOrderItemId(Long orderItemId);

    /**
     * 根据SKU获取整体的需求量
     *
     * @param skuIds skuIds
     * @return HashMap
     */
    Map<Long, BigDecimal> sumTotalRawMaterialBySkuIds(List<Long> skuIds);
}
