package com.youyilin.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.order.entity.PurchaseOrderSource;
import com.youyilin.order.mapper.PurchaseOrderSourceMapper;
import com.youyilin.order.service.PurchaseOrderSourceService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PurchaseOrderSourceImpl extends ServiceImpl<PurchaseOrderSourceMapper, PurchaseOrderSource> implements PurchaseOrderSourceService {

    @Override
    public Long countBySourceIdAndType(Long sourceId, String sourceType) {
        return super.count(new LambdaQueryWrapper<PurchaseOrderSource>()
                .eq(PurchaseOrderSource::getSourceId, sourceId)
                .eq(PurchaseOrderSource::getSourceType, sourceType));
    }

    @Override
    public List<PurchaseOrderSource> listByPurchaseIdAndType(Long purchaseId, String sourceType) {
        return super.list(new LambdaQueryWrapper<PurchaseOrderSource>()
                .eq(PurchaseOrderSource::getOrderId, purchaseId)
                .eq(PurchaseOrderSource::getSourceType, sourceType));
    }

    @Override
    public void delByPurchaseIdAndType(Long purchaseId, String sourceType) {
        super.remove(new LambdaQueryWrapper<PurchaseOrderSource>()
                .eq(PurchaseOrderSource::getOrderId, purchaseId)
                .eq(PurchaseOrderSource::getSourceType, sourceType));
    }
}
