package com.youyilin.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.order.enums.CommonOutboundStatusEnum;
import com.youyilin.order.mapper.CustomerOrderItemMapper;
import com.youyilin.order.entity.CustomerOrderItem;
import com.youyilin.order.service.CustomerOrderItemService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerOrderItemImpl extends ServiceImpl<CustomerOrderItemMapper, CustomerOrderItem> implements CustomerOrderItemService {

    @Override
    public List<CustomerOrderItem> listByOrderId(Long orderId) {
        return super.list(new LambdaQueryWrapper<CustomerOrderItem>().eq(CustomerOrderItem::getOrderId, orderId));
    }

    @Override
    public List<CustomerOrderItem> listByOrderIds(List<Long> orderIds) {
        return super.list(new LambdaQueryWrapper<CustomerOrderItem>().in(CustomerOrderItem::getOrderId, orderIds));
    }

    @Override
    public void updateOutboundFinishByIds(List<Long> ids) {
        super.update(new LambdaUpdateWrapper<CustomerOrderItem>()
                .set(CustomerOrderItem::getStatus, CommonOutboundStatusEnum.OUTBOUND.getCode())
                .in(CustomerOrderItem::getId, ids));
    }
}
