package com.youyilin.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.order.entity.SettledOrder;

/**
 * 结款单 服务类
 */
public interface SettledOrderService extends IService<SettledOrder> {

    /**
     * 获取上次已完成结款的结款单
     *
     * @param customerId 客户ID
     */
    SettledOrder getLastSettledByCustomerId(Long customerId);

    /**
     * 验证当前客户是否存在结款单
     *
     * @param customerId 客户ID
     */
    void validateSettledProcessingByCustomerId(Long customerId);

    /**
     * 验证结款单是否存在
     *
     * @param id 结款单ID
     * @return SettledOrder
     */
    SettledOrder validateSettled(Long id);

    /**
     * 验证结款单是否可以确认
     *
     * @param id 结款单ID
     * @return SettledOrder
     */
    SettledOrder validateConfirm(Long id);

    /**
     * 验证结款单是否可以取消
     *
     * @param id 结款单ID
     * @return SettledOrder
     */
    SettledOrder validateCancel(Long id);

    /**
     * 验证结款单是否可以完成
     *
     * @param id 结款单ID
     * @return SettledOrder
     */
    SettledOrder validateFinish(Long id);

    /**
     * 更新状态为已确认
     */
    void updateConfirm(Long id);

    /**
     * 更新状态为已取消
     */
    void updateCancel(Long id);
}
