package com.youyilin.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.exception.Assert;
import com.youyilin.order.entity.OrderCollect;
import com.youyilin.order.enums.OrderCollectEnum;
import com.youyilin.order.mapper.OrderCollectMapper;
import com.youyilin.order.service.OrderCollectService;
import com.youyilin.order.utils.NoUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 订单收款记录 实现类
 */
@Service
public class OrderCollectImpl extends ServiceImpl<OrderCollectMapper, OrderCollect> implements OrderCollectService {

    @Override
    public List<OrderCollect> listByOrderId(Long orderId, Integer status) {
        LambdaQueryWrapper<OrderCollect> queryWrapper = new LambdaQueryWrapper<>();
        if (status != null) {
            queryWrapper.eq(OrderCollect::getStatus, status);
        }
        queryWrapper.eq(OrderCollect::getOrderId, orderId);
        return super.list(queryWrapper);
    }

    @Override
    public void saveCollect(Long orderId, String orderSn, Long customerId, Long accountId, Long amount, String payType, String remark) {
        OrderCollect collect = new OrderCollect();
        collect.setSn(NoUtils.getSaleOrderCollectSn())
                .setOrderId(orderId)
                .setOrderSn(orderSn)
                .setCustomerId(customerId)
                .setAccountId(accountId)
                .setAmount(amount)
                .setPayType(payType)
                .setRemark(remark)
                .setStatus(OrderCollectEnum.SUCCESS.getCode())
                .setCheckDate(new Date());

        this.validateCollect(collect);
        super.save(collect);
    }

    @Override
    public void saveCollect(Long orderId, String sn, Long customerId, Long accountId, Long amount, String payType) {
        this.saveCollect(orderId, sn, customerId, accountId, amount, payType, null);
    }

    private void validateCollect(OrderCollect en) {
        Assert.notNull(en, "付款记录异常");
        Assert.notNull(en.getOrderId(), "订单号不能为空");
        Assert.hasLength(en.getOrderSn(), "订单号不能为空");
        Assert.notNull(en.getCustomerId(), "客户不能为空");
        Assert.notNull(en.getAccountId(), "账户不能为空");
        Assert.notNull(en.getAmount(), "金额不能为空");
        Assert.hasLength(en.getPayType(), "类型不能为空");
    }
}
