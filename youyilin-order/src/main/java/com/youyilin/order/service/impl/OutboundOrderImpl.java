package com.youyilin.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.exception.Assert;
import com.youyilin.order.entity.OutboundOrder;
import com.youyilin.order.enums.OutboundOrderStatusEnum;
import com.youyilin.order.mapper.OutboundOrderMapper;
import com.youyilin.order.service.OutboundOrderService;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * 出库单 实现类
 */
@Service
public class OutboundOrderImpl extends ServiceImpl<OutboundOrderMapper, OutboundOrder> implements OutboundOrderService {

    @Override
    public OutboundOrder validateOutbound(Long id) {
        OutboundOrder order = super.getById(id);
        Assert.notNull(order, "出库单不存在");
        Assert.notNull(order.getStatus(), "出库单不存在");

        return order;
    }

    @Override
    public OutboundOrder validateEdit(Long id) {
        OutboundOrder order = this.validateOutbound(id, OutboundOrderStatusEnum.NO_CONFIRM.getCode());
        boolean sourceTypeFlag = order.getSourceType() != null && order.getSourceType().equals(OutboundOrder.SourceType.DEFAULT.name());
        Assert.isTrue(sourceTypeFlag, "当前出库单不支持编辑");

        return order;
    }

    private OutboundOrder validateOutbound(Long id, Integer status) {
        OutboundOrder order = this.validateOutbound(id);
        boolean statusFlag = order.getStatus().equals(status);
        Assert.isTrue(statusFlag, "订单状态异常");

        return order;
    }

    @Override
    public OutboundOrder validateOutboundConfirm(Long id) {
        return this.validateOutbound(id, OutboundOrderStatusEnum.NO_CONFIRM.getCode());
    }

    @Override
    public OutboundOrder validateOutboundInventory(Long id) {
        return this.validateOutbound(id, OutboundOrderStatusEnum.CONFIRM.getCode());
    }

    @Override
    public OutboundOrder validateOutboundCancel(Long id) {
        return this.validateOutbound(id, OutboundOrderStatusEnum.NO_CONFIRM.getCode());
    }

    @Override
    public void updateConfirm(Long id) {
        super.update(new LambdaUpdateWrapper<OutboundOrder>()
                .set(OutboundOrder::getStatus, OutboundOrderStatusEnum.CONFIRM.getCode())
                .set(OutboundOrder::getConfirmDate, new Date())
                .eq(OutboundOrder::getId, id));
    }

    @Override
    public void updateFinish(Long id) {
        super.update(new LambdaUpdateWrapper<OutboundOrder>()
                .set(OutboundOrder::getStatus, OutboundOrderStatusEnum.FINISH.getCode())
                .set(OutboundOrder::getFinishDate, new Date())
                .eq(OutboundOrder::getId, id));
    }

    @Override
    public void updateCancel(Long id) {
        super.update(new LambdaUpdateWrapper<OutboundOrder>()
                .set(OutboundOrder::getStatus, OutboundOrderStatusEnum.CANCEL.getCode())
                .set(OutboundOrder::getCancelDate, new Date())
                .eq(OutboundOrder::getId, id));
    }
}
