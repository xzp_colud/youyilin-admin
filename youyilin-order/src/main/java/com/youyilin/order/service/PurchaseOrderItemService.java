package com.youyilin.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.order.entity.PurchaseOrderItem;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 采购订单详情 服务类
 */
public interface PurchaseOrderItemService extends IService<PurchaseOrderItem> {

    /**
     * 统计多个SKU已确认至未检验之间的数量
     *
     * @param skuIds SKU IDS
     * @return HashMap
     */
    Map<Long, BigDecimal> sumConfirmAndNoCheckQtyBySkuIds(List<Long> skuIds);

    /**
     * 统计多个SKU已检验至未入库之间的数量
     *
     * @param skuIds SKU IDS
     * @return HashMap
     */
    Map<Long, BigDecimal> sumCheckAndNoInventoryQtyBySkuIds(List<Long> skuIds);

    /**
     * 按订单查询
     *
     * @param orderId 订单ID
     * @return ArrayList
     */
    List<PurchaseOrderItem> listByOrderId(Long orderId);

    /**
     * 订单是否全部入库
     */
    boolean isAllInventoryIn(Long orderId);

    /**
     * 按订单删除
     *
     * @param orderId 采购订单ID
     */
    void delByPurchaseId(Long orderId);

    /**
     * 按订单确认
     *
     * @param orderId 订单ID
     */
    void updateConfirm(Long orderId);

    /**
     * 按订单取消
     *
     * @param orderId 订单ID
     */
    void updateCancel(Long orderId);

    /**
     * 验证明细信息
     *
     * @param id 明细ID
     */
    PurchaseOrderItem validatePurchaseOrderItem(Long id);

    /**
     * 商品明细检验
     *
     * @param orderId  订单ID
     * @param itemList 商品
     */
    List<PurchaseOrderItem> validateCheck(Long orderId, List<PurchaseOrderItem> itemList);

    /**
     * 检验是否能入库
     *
     * @param id      明细ID
     * @param orderId 采购单ID
     */
    PurchaseOrderItem validateInventoryIn(Long id, Long orderId);

    /**
     * 产品批量入库
     *
     * @param orderId 订单ID
     * @param itemIds 商品IDS
     */
    List<PurchaseOrderItem> validateInventoryInBatch(Long orderId, List<Long> itemIds);

    /**
     * 更新明细状态为已入库
     *
     * @param id 明细ID
     */
    void updateInventory(Long id);

    /**
     * 批量更新明细状态为已入库
     *
     * @param ids 明细IDS
     */
    void updateInventoryBatch(List<Long> ids);

    /**
     * 修正实际检验米数
     *
     * @param id        采购单明细ID
     * @param reviseNum 修正数量
     */
    void updateReviseNum(Long id, BigDecimal reviseNum);
}
