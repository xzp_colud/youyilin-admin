package com.youyilin.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.order.mapper.CustomerOrderLogMapper;
import com.youyilin.order.entity.CustomerOrderLog;
import com.youyilin.order.service.CustomerOrderLogService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerOrderLogImpl extends ServiceImpl<CustomerOrderLogMapper, CustomerOrderLog> implements CustomerOrderLogService {

    @Override
    public List<CustomerOrderLog> listByOrderId(Long orderId) {
        return super.list(new LambdaQueryWrapper<CustomerOrderLog>()
                .eq(CustomerOrderLog::getOrderId, orderId)
                .orderByDesc(CustomerOrderLog::getId));
    }

    @Override
    public void saveLog(Long orderId, String orderSn, String title, String content) {
        CustomerOrderLog insert = new CustomerOrderLog();
        insert.setOrderId(orderId)
                .setOrderSn(orderSn)
                .setTitle(title)
                .setContent(content);

        super.save(insert);
    }
}
