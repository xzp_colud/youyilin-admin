package com.youyilin.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.enums.BooleanEnum;
import com.youyilin.common.exception.Assert;
import com.youyilin.order.entity.Order;
import com.youyilin.order.enums.*;
import com.youyilin.order.mapper.OrderMapper;
import com.youyilin.order.service.OrderService;
import com.youyilin.waybill.enums.WaybillStatusEnum;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 订单 服务实现类
 */
@Service
public class OrderImpl extends ServiceImpl<OrderMapper, Order> implements OrderService {

    @Override
    public Order validateOrder(Long id) {
        Order order = super.getById(id);
        Assert.notNull(order, "销售单不存在");

        return order;
    }

    /**
     * 单一检验销售单状态
     *
     * @param id     销售单ID
     * @param status 状态
     * @param msg    错误信息
     * @return Order
     */
    private Order validateOrder(Long id, Integer status, String msg) {
        Order order = this.validateOrder(id);
        boolean statusFlag = order.getStatus() != null && order.getStatus().equals(status);
        Assert.isTrue(statusFlag, StringUtils.isNotBlank(msg) ? msg : "订单状态异常");

        return order;
    }

    @Override
    public Order validateCanEdit(Long id, Long customerId) {
        Assert.notNull(customerId, "参数异常");
        Order order = this.validateOrder(id, OrderStatusEnum.NO_SUBMIT.getCode(), "销售单状态异常，不能修改");
        // 断言：客户是否一致
        boolean customerFlag = order.getCustomerId().equals(customerId);
        Assert.isTrue(customerFlag, "禁止修改客户");
        // 断言：状态
        boolean orderTypeFlag = order.getOrderType() != null
                && (order.getOrderType().equals(OrderTypeEnum.OFFLINE.getCode())
                || order.getOrderType().equals(OrderTypeEnum.WHOLESALE.getCode()));
        Assert.isTrue(orderTypeFlag, "当前销售单禁止编辑");

        return order;
    }

    @Override
    public Order validateOrderSubmit(Long id) {
        return this.validateOrder(id, OrderStatusEnum.NO_SUBMIT.getCode(), "订单状态异常，不能提交");
    }

    @Override
    public Order validateOrderPay(Long id) {
        return this.validateOrder(id, OrderStatusEnum.SUBMIT_NO_PAY.getCode(), "订单未提交，不能支付");
    }

    @Override
    public Order validateOrderOutbound(Long id) {
        return this.validateOrder(id, OrderStatusEnum.WAIT_INVENTORY.getCode(), "订单未付款，不能出库");
    }

    @Override
    public Order validateOrderSend(Long id) {
        return this.validateOrder(id, OrderStatusEnum.WAIT_SEND.getCode(), "订单未出库，不能发货");
    }

    @Override
    public Order validateUpdateAddress(Long id) {
        Order order = this.validateOrder(id);
        boolean statusFlag = order.getStatus() != null && (
                order.getStatus().equals(OrderStatusEnum.WAIT_SEND.getCode())
                        || order.getStatus().equals(OrderStatusEnum.WAIT_INVENTORY_APPLY.getCode())
                        || order.getStatus().equals(OrderStatusEnum.WAIT_INVENTORY.getCode()));
        Assert.isTrue(statusFlag, "订单状态异常，禁止修改发货地址");

        return order;
    }

    @Override
    public Order validateOrderCancel(Long id) {
        Order order = this.validateOrder(id);

        boolean statusFlag = order.getStatus() != null
                && (order.getStatus().equals(OrderStatusEnum.NO_SUBMIT.getCode())
                || order.getStatus().equals(OrderStatusEnum.SUBMIT_NO_PAY.getCode()));
        Assert.isTrue(statusFlag, "订单状态异常，不能取消");

        return order;
    }

    @Override
    public Order validateCanRefund(Long orderId) {
        Order order = this.validateOrder(orderId);

        // 断言：订单状态
        boolean statusFlag = order.getStatus() != null
                && (order.getStatus().equals(OrderStatusEnum.WAIT_INVENTORY.getCode())
                || order.getStatus().equals(OrderStatusEnum.WAIT_INVENTORY_APPLY.getCode())
                || order.getStatus().equals(OrderStatusEnum.WAIT_SEND.getCode())
                || order.getStatus().equals(OrderStatusEnum.WAIT_SEND_APPLY.getCode())
                || order.getStatus().equals(OrderStatusEnum.SEND_ALL.getCode()));
        Assert.isTrue(statusFlag, "订单状态异常，不能退款");
        // 断言：退款状态
        boolean isProcessingFlag = order.getRefundStatus() != null && order.getRefundStatus().equals(CommonRefundStatusEnum.PROCESSING.getCode());
        Assert.isFalse(isProcessingFlag, "已处于退款中");
        // 断言：结款状态
        boolean isSettledFlag = order.getSettledStatus() != null
                && (order.getSettledStatus().equals(CommonSettledStatusEnum.DEFAULT.getCode())
                || order.getSettledStatus().equals(CommonSettledStatusEnum.CANCEL.getCode()));
        Assert.isTrue(isSettledFlag, "有未处理的结款");
        // 断言：退款状态
        boolean isRefundFlag = order.getStatus() != null && order.getStatus().equals(OrderStatusEnum.REFUND.getCode());
        Assert.isFalse(isRefundFlag, "订单已退款");

        return order;
    }

    @Override
    public List<Order> validateCanSettledBatch(List<Long> ids, Long customerId) {
        Assert.notEmpty(ids, "销售单不能为空");
        List<Order> orderList = super.listByIds(ids);
        Assert.notEmpty(orderList, "销售单不能为空");
        List<Integer> settledStatusList = OrderStatusEnum.listSettledStatus();
        for (Order item : orderList) {
            boolean statusFlag = settledStatusList.contains(item.getStatus());
            boolean refundFlag = item.getRefundStatus() != null
                    && (item.getRefundStatus().equals(CommonRefundStatusEnum.DEFAULT.getCode())
                    || item.getRefundStatus().equals(CommonRefundStatusEnum.CANCEL.getCode()));
            boolean financeFlag = item.getFinanceFlag() != null && item.getFinanceFlag() == BooleanEnum.FALSE.getCode();
            boolean settledFlag = item.getSettledStatus() != null
                    && (item.getSettledStatus() == CommonSettledStatusEnum.DEFAULT.getCode()
                    || item.getSettledStatus() == CommonSettledStatusEnum.CANCEL.getCode());
            boolean customerFlag = item.getCustomerId().equals(customerId);
            // 断言
            boolean isTrue = statusFlag && refundFlag && financeFlag && settledFlag && customerFlag;
            Assert.isTrue(isTrue, item.getSn() + " 不能结款");
        }

        return orderList;
    }

    @Override
    public Order validateCanPurchase(Long id) {
        Order order = this.validateOrder(id);
        boolean statusFlag = order.getStatus() != null
                && (order.getStatus().equals(OrderStatusEnum.WAIT_INVENTORY.getCode())
                || order.getStatus().equals(OrderStatusEnum.WAIT_READY_PURCHASE.getCode())
                || order.getStatus().equals(OrderStatusEnum.WAIT_READY_PURCHASE_PROCESSING.getCode()));
        Assert.isTrue(statusFlag, "原料采购异常，状态不为未出库或待备货入库");

        return order;
    }

    @Override
    public Order validateCanReadyPurchase(Long id) {
        Order order = this.validateOrder(id);
        boolean statusFlag = order.getStatus() != null && order.getStatus().equals(OrderStatusEnum.WAIT_READY_PURCHASE.getCode());
        Assert.isTrue(statusFlag, "大货采购异常，状态不为待备货入库");

        return order;
    }

    @Override
    public void updateSubmit(Long id) {
        super.update(new LambdaUpdateWrapper<Order>()
                .set(Order::getStatus, OrderStatusEnum.SUBMIT_NO_PAY.getCode())
                .eq(Order::getId, id));
    }

    @Override
    public void updatePay(Long id, Integer financeFlag, String payType) {
        // 断言：参数
        boolean paramsFlag = id != null && financeFlag != null && StringUtils.isNotBlank(payType);
        Assert.isTrue(paramsFlag, "【销售单】参数异常");
        // 更新
        Integer settledStatus = financeFlag == BooleanEnum.TRUE.getCode() ? CommonSettledStatusEnum.FINISH.getCode() : CommonSettledStatusEnum.DEFAULT.getCode();
        super.update(new LambdaUpdateWrapper<Order>()
                .set(Order::getStatus, OrderStatusEnum.WAIT_INVENTORY.getCode())
                .set(Order::getPayDate, new Date())
                .set(Order::getFinanceFlag, financeFlag)
                .set(Order::getPayType, payType)
                .set(Order::getSettledStatus, settledStatus)
                .eq(Order::getId, id));
    }

    @Override
    public void updatePayReady(Long id, Integer financeFlag, String payType) {
        // 断言：参数
        boolean paramsFlag = id != null && financeFlag != null && StringUtils.isNotBlank(payType);
        Assert.isTrue(paramsFlag, "【销售单】参数异常");
        // 更新
        Integer settledStatus = financeFlag == BooleanEnum.TRUE.getCode() ? CommonSettledStatusEnum.FINISH.getCode() : CommonSettledStatusEnum.DEFAULT.getCode();
        super.update(new LambdaUpdateWrapper<Order>()
                .set(Order::getStatus, OrderStatusEnum.WAIT_READY_PURCHASE.getCode())
                .set(Order::getPayDate, new Date())
                .set(Order::getFinanceFlag, financeFlag)
                .set(Order::getPayType, payType)
                .set(Order::getSettledStatus, settledStatus)
                .eq(Order::getId, id));
    }

    @Override
    public void updateReadyProcessing(Long id) {
        super.update(new LambdaUpdateWrapper<Order>()
                .set(Order::getStatus, OrderStatusEnum.WAIT_READY_PURCHASE_PROCESSING.getCode())
                .eq(Order::getId, id));
    }

    @Override
    public void updateReadFinish(Long id) {
        super.update(new LambdaUpdateWrapper<Order>()
                .set(Order::getStatus, OrderStatusEnum.FINISH_READY.getCode())
                .set(Order::getFinishDate, new Date())
                .eq(Order::getId, id));
    }

    @Override
    public void updateSend(Long id, Long waybillId, String waybillSn) {
        // 断言：参数
        boolean paramsFlag = id != null && waybillId != null && StringUtils.isNotBlank(waybillSn);
        Assert.isTrue(paramsFlag, "【销售单】参数异常");
        // 更新
        super.update(new LambdaUpdateWrapper<Order>()
                .set(Order::getStatus, OrderStatusEnum.WAIT_SEND_APPLY.getCode())
                .set(Order::getWaybillId, waybillId)
                .set(Order::getWaybillSn, waybillSn)
                .set(Order::getWaybillStatus, WaybillStatusEnum.DEFAULT.getCode())
                .set(Order::getWaybillDate, new Date())
                .eq(Order::getId, id));
    }

    @Override
    public void updateCancel(Long id) {
        super.update(new LambdaUpdateWrapper<Order>()
                .set(Order::getStatus, OrderStatusEnum.CANCEL.getCode())
                .set(Order::getCancelDate, new Date())
                .eq(Order::getId, id));
    }

    @Override
    public void updateAddress(Long id, String receiverName, String receiverPhone, String receiverProvince, String receiverCity, String receiverArea, String receiverDetail) {
        super.update(new LambdaUpdateWrapper<Order>()
                .set(Order::getReceiverName, receiverName)
                .set(Order::getReceiverPhone, receiverPhone)
                .set(Order::getReceiverProvince, receiverProvince)
                .set(Order::getReceiverCity, receiverCity)
                .set(Order::getReceiverArea, receiverArea)
                .set(Order::getReceiverDetail, receiverDetail)
                .eq(Order::getId, id));
    }

    @Override
    public void updateOutboundProcessByOutboundId(Long outboundId, String outboundSn, Long id) {
        // 断言：参数
        boolean paramsFlag = outboundId != null && id != null && StringUtils.isNotBlank(outboundSn);
        Assert.isTrue(paramsFlag, "【销售单】参数异常");
        // 更新
        super.update(new LambdaUpdateWrapper<Order>()
                .set(Order::getOutboundId, outboundId)
                .set(Order::getOutboundSn, outboundSn)
                .set(Order::getOutboundStatus, CommonOutboundStatusEnum.PROCESSING.getCode())
                .set(Order::getStatus, OrderStatusEnum.WAIT_INVENTORY_APPLY.getCode())
                .eq(Order::getId, id));
    }

    @Override
    public void updateOutboundFinishByIds(List<Long> ids) {
        Assert.notEmpty(ids, "【销售单】参数异常");
        super.update(new LambdaUpdateWrapper<Order>()
                .set(Order::getOutboundStatus, CommonOutboundStatusEnum.OUTBOUND.getCode())
                .set(Order::getStatus, OrderStatusEnum.WAIT_SEND.getCode())
                .set(Order::getOutboundDate, new Date())
                .in(Order::getId, ids));
    }

    @Override
    public void updateSendFinish(List<Long> ids) {
        Assert.notEmpty(ids, "【销售单】参数异常");
        super.update(new LambdaUpdateWrapper<Order>()
                .set(Order::getWaybillStatus, WaybillStatusEnum.FINISH.getCode())
                .set(Order::getStatus, OrderStatusEnum.SEND_ALL.getCode())
                .set(Order::getWaybillDate, new Date())
                .in(Order::getId, ids));
    }

    @Override
    public void updateSettledStatus(List<Long> ids, Integer settledStatus) {
        boolean paramsFlag = CollectionUtils.isNotEmpty(ids) && settledStatus != null;
        Assert.isTrue(paramsFlag, "【销售单】参数异常");
        // 更新
        Integer financeFlag = settledStatus.equals(CommonSettledStatusEnum.FINISH.getCode()) ? BooleanEnum.TRUE.getCode() : BooleanEnum.FALSE.getCode();
        super.update(new LambdaUpdateWrapper<Order>()
                .set(Order::getFinanceFlag, financeFlag)
                .set(Order::getSettledStatus, settledStatus)
                .set(Order::getSettledDate, new Date())
                .in(Order::getId, ids));
    }

    @Override
    public void updateRefundProcessing(Long id) {
        super.update(new LambdaUpdateWrapper<Order>()
                .set(Order::getRefundStatus, CommonRefundStatusEnum.PROCESSING.getCode())
                .eq(Order::getId, id));
    }

    @Override
    public void updateRefundSuccess(Long id) {
        super.update(new LambdaUpdateWrapper<Order>()
                .set(Order::getStatus, OrderStatusEnum.REFUND.getCode())
                .set(Order::getRefundStatus, CommonRefundStatusEnum.REFUND.getCode())
                .set(Order::getRefundDate, new Date())
                .eq(Order::getId, id));
    }

    @Override
    public void updateRefundFail(Long id) {
        super.update(new LambdaUpdateWrapper<Order>()
                .set(Order::getRefundStatus, CommonRefundStatusEnum.CANCEL.getCode())
                .eq(Order::getId, id));
    }
}
