package com.youyilin.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.enums.StatusEnum;
import com.youyilin.order.mapper.CustomerOrderFinanceMapper;
import com.youyilin.order.entity.CustomerOrderFinance;
import com.youyilin.order.service.CustomerOrderFinanceService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerOrderFinanceImpl extends ServiceImpl<CustomerOrderFinanceMapper, CustomerOrderFinance> implements CustomerOrderFinanceService {

    @Override
    public List<CustomerOrderFinance> listByOrderId(Long orderId) {
        return super.list(new LambdaQueryWrapper<CustomerOrderFinance>().eq(CustomerOrderFinance::getOrderId, orderId));
    }

    @Override
    public void saveFinance(Long orderId, String orderSn, String sn, Long amount, String paySuccessTime, String typeName, String outTradeNo) {
        CustomerOrderFinance finance = new CustomerOrderFinance();

        finance.setOrderId(orderId);
        finance.setOrderSn(orderSn);
        finance.setSn(sn);
        finance.setAmount(amount);
        finance.setPaySuccessTime(paySuccessTime);
        finance.setStatus(StatusEnum.NORMAL.getCode());
        finance.setTypeName(typeName);
        finance.setOutTradeNo(outTradeNo);

        super.save(finance);
    }
}
