package com.youyilin.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.order.entity.OrderLog;

import java.util.List;

/**
 * 订单日志 服务类
 */
public interface OrderLogService extends IService<OrderLog> {

    /**
     * 按订单ID查询
     *
     * @param sn 订单系统编号
     * @return ArrayList
     */
    List<OrderLog> listBySn(String sn);

    /**
     * 保存日志
     *
     * @param orderId 订单ID
     * @param sn      订单编号
     * @param type    类型
     */
    void saveLog(Long orderId, String sn, Integer type);

    /**
     * 保存日志
     *
     * @param orderId 订单ID
     * @param orderNo 订单编号
     * @param type    类型
     * @param remark  备注
     */
    void saveLog(Long orderId, String orderNo, Integer type, String remark);
}
