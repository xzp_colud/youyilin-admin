package com.youyilin.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.order.mapper.OrderLogMapper;
import com.youyilin.order.entity.OrderLog;
import com.youyilin.order.service.OrderLogService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 订单日志 实现类
 */
@Service
public class OrderLogImpl extends ServiceImpl<OrderLogMapper, OrderLog> implements OrderLogService {

    @Override
    public List<OrderLog> listBySn(String sn) {
        return super.list(new LambdaQueryWrapper<OrderLog>().eq(OrderLog::getSn, sn));
    }

    private OrderLog create(Long orderId, String sn, Integer type, String remark) {
        OrderLog log = new OrderLog();
        log.setOrderId(orderId);
        log.setSn(sn);
        log.setType(type);
        log.setRemark(remark);
        return log;
    }

    @Override
    public void saveLog(Long orderId, String orderNo, Integer type) {
        super.save(this.create(orderId, orderNo, type, null));
    }

    @Override
    public void saveLog(Long orderId, String orderNo, Integer type, String remark) {
        super.save(this.create(orderId, orderNo, type, remark));
    }
}
