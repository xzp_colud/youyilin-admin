package com.youyilin.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.order.entity.OrderItem;
import com.youyilin.order.enums.OrderStatusEnum;
import com.youyilin.order.mapper.OrderItemMapper;
import com.youyilin.order.service.OrderItemService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 订单详情部件 服务实现类
 */
@Service
public class OrderItemImpl extends ServiceImpl<OrderItemMapper, OrderItem> implements OrderItemService {

    @Override
    public List<OrderItem> listByOrderId(Long orderId) {
        return super.list(new LambdaQueryWrapper<OrderItem>().eq(OrderItem::getOrderId, orderId));
    }

    @Override
    public List<OrderItem> listByOrderIds(List<Long> orderIds) {
        return super.list(new LambdaQueryWrapper<OrderItem>().in(OrderItem::getOrderId, orderIds));
    }

    @Override
    public void delByOrderId(Long orderId) {
        baseMapper.delByOrderId(orderId);
    }

    @Override
    public void updateSubmit(Long orderId) {
        this.updateStatus(orderId, OrderStatusEnum.SUBMIT_NO_PAY.getCode());
    }

    @Override
    public void updatePay(Long orderId) {
        this.updateStatus(orderId, OrderStatusEnum.WAIT_INVENTORY.getCode());
    }

    @Override
    public void updatePayReady(Long orderId) {
        this.updateStatus(orderId, OrderStatusEnum.WAIT_READY_PURCHASE.getCode());
    }

    @Override
    public void updateReadyProcessing(Long orderId) {
        this.updateStatus(orderId, OrderStatusEnum.WAIT_READY_PURCHASE_PROCESSING.getCode());
    }

    @Override
    public void updateReadFinish(Long orderId) {
        this.updateStatus(orderId, OrderStatusEnum.FINISH_READY.getCode());
    }

    @Override
    public void updateOutbound(Long orderId) {
        this.updateStatus(orderId, OrderStatusEnum.WAIT_INVENTORY_APPLY.getCode());
    }

    @Override
    public void updateOutboundFinishByIds(List<Long> ids) {
        this.updateStatus(ids, OrderStatusEnum.WAIT_SEND.getCode());
    }

    @Override
    public void updateSendFinish(List<Long> ids) {
        this.updateStatus(ids, OrderStatusEnum.SEND_ALL.getCode());
    }

    @Override
    public void updateCancel(List<Long> orderIds) {
        super.update(new LambdaUpdateWrapper<OrderItem>()
                .set(OrderItem::getStatus, OrderStatusEnum.CANCEL.getCode())
                .in(OrderItem::getId, orderIds));
    }

    @Override
    public void updateCancelByOrderId(Long orderId) {
        super.update(new LambdaUpdateWrapper<OrderItem>()
                .set(OrderItem::getStatus, OrderStatusEnum.CANCEL.getCode())
                .eq(OrderItem::getOrderId, orderId));
    }

    private void updateStatus(Long orderId, Integer status) {
        super.update(new LambdaUpdateWrapper<OrderItem>().set(OrderItem::getStatus, status).eq(OrderItem::getOrderId, orderId));
    }

    private void updateStatus(List<Long> ids, Integer status) {
        super.update(new LambdaUpdateWrapper<OrderItem>().set(OrderItem::getStatus, status).in(OrderItem::getId, ids));
    }
}
