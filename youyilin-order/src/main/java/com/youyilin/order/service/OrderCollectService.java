package com.youyilin.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.order.entity.OrderCollect;

import java.util.List;

/**
 * 收款记录 服务类
 */
public interface OrderCollectService extends IService<OrderCollect> {

    /**
     * 某订单收款记录
     *
     * @param orderId 订单ID
     * @param status  状态
     * @return ArrayList
     */
    List<OrderCollect> listByOrderId(Long orderId, Integer status);

    /**
     * 创建收款记录
     *
     * @param orderId    订单ID
     * @param sn         订单编号
     * @param customerId 客户ID
     * @param accountId  账户ID
     * @param amount     金额
     * @param payType    支付方式
     * @param remark     备注
     */
    void saveCollect(Long orderId, String sn, Long customerId, Long accountId, Long amount, String payType, String remark);

    /**
     * 创建收款记录
     *
     * @param orderId    订单ID
     * @param sn         订单编号
     * @param customerId 客户ID
     * @param accountId  账户ID
     * @param amount     金额
     * @param payType    支付方式
     */
    void saveCollect(Long orderId, String sn, Long customerId, Long accountId, Long amount, String payType);
}
