package com.youyilin.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.order.entity.OrderItemRawMaterial;
import com.youyilin.order.mapper.OrderItemRawMaterialMapper;
import com.youyilin.order.service.OrderItemRawMaterialService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 销售单子集原料明细 实现类
 */
@Service
public class OrderItemRawMaterialImpl extends ServiceImpl<OrderItemRawMaterialMapper, OrderItemRawMaterial> implements OrderItemRawMaterialService {

    @Override
    public List<OrderItemRawMaterial> listByOrderId(Long orderId) {
        return super.list(new LambdaQueryWrapper<OrderItemRawMaterial>().eq(OrderItemRawMaterial::getOrderId, orderId));
    }

    @Override
    public List<OrderItemRawMaterial> listByOrderItemId(Long orderItemId) {
        return super.list(new LambdaQueryWrapper<OrderItemRawMaterial>().eq(OrderItemRawMaterial::getOrderItemId, orderItemId));
    }

    @Override
    public Map<Long, BigDecimal> sumTotalRawMaterialBySkuIds(List<Long> skuIds) {
        if (CollectionUtils.isEmpty(skuIds)) {
            return new HashMap<>();
        }
        List<OrderItemRawMaterial> list = baseMapper.sumTotalRawMaterialBySkuIds(skuIds);
        if (CollectionUtils.isEmpty(list)) {
            return new HashMap<>();
        }
        return list.stream().collect(Collectors.toMap(OrderItemRawMaterial::getSourceSkuId, OrderItemRawMaterial::getTotalUsage));
    }
}
