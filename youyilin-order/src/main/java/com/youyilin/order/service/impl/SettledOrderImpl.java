package com.youyilin.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.exception.Assert;
import com.youyilin.order.entity.SettledOrder;
import com.youyilin.order.enums.SettledOrderStatusEnum;
import com.youyilin.order.mapper.SettledOrderMapper;
import com.youyilin.order.service.SettledOrderService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 结款单 实现类
 */
@Service
public class SettledOrderImpl extends ServiceImpl<SettledOrderMapper, SettledOrder> implements SettledOrderService {

    @Override
    public SettledOrder getLastSettledByCustomerId(Long customerId) {
        return super.getOne(new LambdaQueryWrapper<SettledOrder>()
                .eq(SettledOrder::getCustomerId, customerId)
                .eq(SettledOrder::getStatus, SettledOrderStatusEnum.FINISH.getCode())
                .orderByDesc(SettledOrder::getId)
                .last("limit 1"));
    }

    @Override
    public void validateSettledProcessingByCustomerId(Long customerId) {
        List<Integer> processStatusList = SettledOrderStatusEnum.listProcessing();
        long count = super.count(new LambdaQueryWrapper<SettledOrder>()
                .eq(SettledOrder::getCustomerId, customerId)
                .in(SettledOrder::getStatus, processStatusList));

        Assert.isTrue(count == 0L, "有未处理的结款单");
    }

    @Override
    public SettledOrder validateSettled(Long id) {
        SettledOrder settled = super.getById(id);
        Assert.notNull(settled, "结款单不存在");

        return settled;
    }

    private SettledOrder validateSettledStatus(Long id, Integer status, String msg) {
        SettledOrder settled = this.validateSettled(id);
        boolean statusFlag = settled.getStatus() != null && settled.getStatus().equals(status);
        Assert.isTrue(statusFlag, StringUtils.isNotBlank(msg) ? msg : "结款单状态异常");

        return settled;
    }

    @Override
    public SettledOrder validateConfirm(Long id) {
        return this.validateSettledStatus(id, SettledOrderStatusEnum.DEFAULT.getCode(), null);
    }

    @Override
    public SettledOrder validateCancel(Long id) {
        return this.validateSettledStatus(id, SettledOrderStatusEnum.DEFAULT.getCode(), null);
    }

    @Override
    public SettledOrder validateFinish(Long id) {
        return this.validateSettledStatus(id, SettledOrderStatusEnum.CONFIRM.getCode(), "结款单未确认");
    }

    @Override
    public void updateConfirm(Long id) {
        super.update(new LambdaUpdateWrapper<SettledOrder>()
                .set(SettledOrder::getStatus, SettledOrderStatusEnum.CONFIRM.getCode())
                .set(SettledOrder::getConfirmDate, new Date())
                .eq(SettledOrder::getId, id));
    }

    @Override
    public void updateCancel(Long id) {
        super.update(new LambdaUpdateWrapper<SettledOrder>()
                .set(SettledOrder::getStatus, SettledOrderStatusEnum.CANCEL.getCode())
                .set(SettledOrder::getCancelDate, new Date())
                .eq(SettledOrder::getId, id));
    }
}
