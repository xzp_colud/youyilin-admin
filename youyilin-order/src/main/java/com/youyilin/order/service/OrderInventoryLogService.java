package com.youyilin.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.order.entity.OrderInventoryLog;

import java.util.List;

/**
 * 采购单入库日志 服务类
 */
public interface OrderInventoryLogService extends IService<OrderInventoryLog> {

    /**
     * 按采购单查询
     *
     * @param purchaseId 采购单ID
     * @return ArrayList
     */
    List<OrderInventoryLog> listByPurchaseId(Long purchaseId);

    /**
     * 按采购单子集查询
     *
     * @param purchaseItemId 采购单子集ID
     * @return ArrayList
     */
    List<OrderInventoryLog> listByPurchaseItemId(Long purchaseItemId);
}
