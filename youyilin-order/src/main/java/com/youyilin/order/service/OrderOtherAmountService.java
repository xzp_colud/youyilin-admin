package com.youyilin.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.order.entity.OrderOtherAmount;

import java.util.List;

public interface OrderOtherAmountService extends IService<OrderOtherAmount> {

    /**
     * 根据订单id查询其他费用
     *
     * @param orderId 订单id
     * @return ArrayList
     */
    List<OrderOtherAmount> listByOrderId(Long orderId);

    /**
     * 根据订单id删除其他费用
     *
     * @param orderId 订单id
     */
    void deleteByOrderId(Long orderId);
}
