package com.youyilin.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.order.entity.SettledOrderItem;

import java.util.List;

/**
 * 结款单明细 服务类
 */
public interface SettledOrderItemService extends IService<SettledOrderItem> {

    /**
     * 根据结款单查询明细
     *
     * @param settledId 结款单ID
     * @return ArrayList
     */
    List<SettledOrderItem> listBySettledId(Long settledId);
}
