package com.youyilin.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.exception.ApiException;
import com.youyilin.common.exception.Assert;
import com.youyilin.order.entity.PurchaseOrderItem;
import com.youyilin.order.enums.PurchaseOrderItemStatusEnum;
import com.youyilin.order.mapper.PurchaseOrderItemMapper;
import com.youyilin.order.service.PurchaseOrderItemService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 采购订单详情 实现类
 */
@Service
public class PurchaseOrderItemImpl extends ServiceImpl<PurchaseOrderItemMapper, PurchaseOrderItem> implements PurchaseOrderItemService {

    @Override
    public Map<Long, BigDecimal> sumConfirmAndNoCheckQtyBySkuIds(List<Long> skuIds) {
        if (CollectionUtils.isEmpty(skuIds)) {
            return new HashMap<>();
        }
        List<PurchaseOrderItem> itemList = baseMapper.sumConfirmAndNoCheckQtyBySkuIds(skuIds);
        if (CollectionUtils.isEmpty(itemList)) {
            return new HashMap<>();
        }

        return itemList.stream().collect(Collectors.toMap(PurchaseOrderItem::getProductPriceId, PurchaseOrderItem::getBuyNum));
    }

    @Override
    public Map<Long, BigDecimal> sumCheckAndNoInventoryQtyBySkuIds(List<Long> skuIds) {
        if (CollectionUtils.isEmpty(skuIds)) {
            return new HashMap<>();
        }
        List<PurchaseOrderItem> itemList = baseMapper.sumCheckAndNoInventoryQtyBySkuIds(skuIds);
        if (CollectionUtils.isEmpty(itemList)) {
            return new HashMap<>();
        }
        return itemList.stream().collect(Collectors.toMap(PurchaseOrderItem::getProductPriceId, PurchaseOrderItem::getReallyNum));
    }

    @Override
    public List<PurchaseOrderItem> listByOrderId(Long orderId) {
        return super.list(new LambdaQueryWrapper<PurchaseOrderItem>().eq(PurchaseOrderItem::getOrderId, orderId));
    }

    @Override
    public boolean isAllInventoryIn(Long orderId) {
        long count = super.count(new LambdaQueryWrapper<PurchaseOrderItem>()
                .eq(PurchaseOrderItem::getOrderId, orderId)
                .ne(PurchaseOrderItem::getStatus, PurchaseOrderItemStatusEnum.IN_FINISH.getCode()));
        return count == 0L;
    }

    @Override
    public void delByPurchaseId(Long orderId) {
        baseMapper.delByPurchaseId(orderId);
    }

    @Override
    public void updateConfirm(Long orderId) {
        this.updateStatus(orderId, PurchaseOrderItemStatusEnum.ARRIVAL_NO_CHECK.getCode());
    }

    @Override
    public void updateCancel(Long orderId) {
        // 排除已入库的
        super.update(new LambdaUpdateWrapper<PurchaseOrderItem>()
                .set(PurchaseOrderItem::getStatus, PurchaseOrderItemStatusEnum.CANCEL.getCode())
                .eq(PurchaseOrderItem::getOrderId, orderId));
    }

    @Override
    public PurchaseOrderItem validatePurchaseOrderItem(Long id) {
        PurchaseOrderItem poi = super.getById(id);
        Assert.notNull(poi, "采购明细不存在");

        return poi;
    }

    @Override
    public List<PurchaseOrderItem> validateCheck(Long orderId, List<PurchaseOrderItem> itemList) {
        List<Long> checkItemIds = new ArrayList<>();
        // 参数验证
        itemList.forEach(item -> {
            if (item.getId() == null || item.getReallyNum() == null || item.getReallyNum().compareTo(BigDecimal.ZERO) < 0) {
                throw new ApiException("检验异常");
            }
            if (checkItemIds.contains(item.getId())) {
                throw new ApiException("检验异常");
            }
            checkItemIds.add(item.getId());
        });
        // 数据检验
        List<PurchaseOrderItem> dbItemList = super.listByIds(checkItemIds);
        if (dbItemList.isEmpty()) {
            throw new ApiException("检验商品异常");
        }
        // 检验数据转换成map
        Map<Long, PurchaseOrderItem> checkItemMap = itemList.stream().collect(Collectors.toMap(PurchaseOrderItem::getId, item -> item));

        Date checkDate = new Date();
        for (PurchaseOrderItem dbItem : dbItemList) {
            if (!dbItem.getStatus().equals(PurchaseOrderItemStatusEnum.ARRIVAL_NO_CHECK.getCode())) {
                throw new ApiException("【" + dbItem.getProductName() + "】已检验，无法再次检验");
            }
            PurchaseOrderItem checkItem = checkItemMap.get(dbItem.getId());
            if (checkItem == null) {
                throw new ApiException("检验商品异常");
            }
            BigDecimal reallyNum = checkItem.getReallyNum();
//            if (dbItem.getBuyNum().compareTo(reallyNum) < 0) {
//                throw new ApiException("检验数量不能超过采购数量");
//            }
            Long totalReallyAmount = reallyNum.longValue() * dbItem.getAmount();
            dbItem.setReallyNum(reallyNum);
            dbItem.setTotalReallyAmount(totalReallyAmount);
            dbItem.setStatus(PurchaseOrderItemStatusEnum.CHECK_NO_IN.getCode());
            dbItem.setCheckDate(checkDate);
        }

        return dbItemList;
    }

    @Override
    public PurchaseOrderItem validateInventoryIn(Long id, Long orderId) {
        PurchaseOrderItem poi = this.validatePurchaseOrderItem(id);
        boolean statusFlag = poi.getStatus() != null && poi.getStatus().equals(PurchaseOrderItemStatusEnum.CHECK_NO_IN.getCode());
        if (!statusFlag) {
            throw new ApiException("采购单明细异常");
        }
        if (!poi.getOrderId().equals(orderId)) {
            throw new ApiException("采购单明细异常");
        }

        return poi;
    }

    @Override
    public List<PurchaseOrderItem> validateInventoryInBatch(Long orderId, List<Long> itemIds) {
        List<PurchaseOrderItem> orderItemList = super.listByIds(itemIds);
        if (orderItemList.isEmpty()) {
            throw new ApiException("采购明细异常");
        }
        for (PurchaseOrderItem orderItem : orderItemList) {
            boolean orderEqualsFlag = orderItem.getOrderId().equals(orderId);
            boolean statusFlag = orderItem.getStatus() != null && orderItem.getStatus().equals(PurchaseOrderItemStatusEnum.CHECK_NO_IN.getCode());

            if (orderEqualsFlag && statusFlag) {
                continue;
            }
            throw new ApiException("采购明细状态异常");
        }

        return orderItemList;
    }

    @Override
    public void updateInventory(Long id) {
        super.update(new LambdaUpdateWrapper<PurchaseOrderItem>()
                .set(PurchaseOrderItem::getStatus, PurchaseOrderItemStatusEnum.IN_FINISH.getCode())
                .set(PurchaseOrderItem::getWarehousingDate, new Date())
                .eq(PurchaseOrderItem::getId, id));
    }

    @Override
    public void updateInventoryBatch(List<Long> ids) {
        super.update(new LambdaUpdateWrapper<PurchaseOrderItem>()
                .set(PurchaseOrderItem::getStatus, PurchaseOrderItemStatusEnum.IN_FINISH.getCode())
                .set(PurchaseOrderItem::getWarehousingDate, new Date())
                .in(PurchaseOrderItem::getId, ids));
    }

    @Override
    public void updateReviseNum(Long id, BigDecimal reviseNum) {
        super.update(new LambdaUpdateWrapper<PurchaseOrderItem>()
                .set(PurchaseOrderItem::getReviseNum, reviseNum)
                .eq(PurchaseOrderItem::getId, id));
    }

    private void updateStatus(Long orderId, Integer status) {
        super.update(new LambdaUpdateWrapper<PurchaseOrderItem>()
                .set(PurchaseOrderItem::getStatus, status)
                .eq(PurchaseOrderItem::getOrderId, orderId));
    }
}
