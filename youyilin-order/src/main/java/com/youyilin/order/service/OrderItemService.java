package com.youyilin.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.order.entity.OrderItem;

import java.util.List;

/**
 * 订单详情 服务类
 */
public interface OrderItemService extends IService<OrderItem> {

    /**
     * 某订单详情
     *
     * @param orderId 订单ID
     * @return ArrayList
     */
    List<OrderItem> listByOrderId(Long orderId);

    /**
     * 某订单详情
     *
     * @param orderIds 订单IDS
     * @return ArrayList
     */
    List<OrderItem> listByOrderIds(List<Long> orderIds);

    /**
     * 按订单删除
     *
     * @param orderId 订单ID
     */
    void delByOrderId(Long orderId);

    /**
     * 提交
     *
     * @param orderId 订单ID
     */
    void updateSubmit(Long orderId);

    /**
     * 付款 -> 待出库
     *
     * @param orderId 订单ID
     */
    void updatePay(Long orderId);

    /**
     * 付款 -> 待备货入库
     *
     * @param orderId 订单ID
     */
    void updatePayReady(Long orderId);

    /**
     * 备货入库中
     *
     * @param orderId 订单ID
     */
    void updateReadyProcessing(Long orderId);

    /**
     * 备货完成
     *
     * @param orderId 订单ID
     */
    void updateReadFinish(Long orderId);

    /**
     * 申请出库
     *
     * @param orderId 订单ID
     */
    void updateOutbound(Long orderId);

    /**
     * 出库完成
     *
     * @param ids IDS
     */
    void updateOutboundFinishByIds(List<Long> ids);

    /**
     * 发货完成
     *
     * @param ids IDS
     */
    void updateSendFinish(List<Long> ids);

    /**
     * 取消
     *
     * @param orderIds 订单IDS
     */
    void updateCancel(List<Long> orderIds);

    /**
     * 更新明细状态为已取消
     *
     * @param orderId 销售单ID
     */
    void updateCancelByOrderId(Long orderId);
}
