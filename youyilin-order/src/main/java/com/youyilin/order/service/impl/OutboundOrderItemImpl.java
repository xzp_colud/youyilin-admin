package com.youyilin.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.exception.Assert;
import com.youyilin.order.entity.OutboundOrderItem;
import com.youyilin.order.enums.OutboundOrderItemStatusEnum;
import com.youyilin.order.mapper.OutboundOrderItemMapper;
import com.youyilin.order.service.OutboundOrderItemService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 出库单 子集 实现类
 */
@Service
public class OutboundOrderItemImpl extends ServiceImpl<OutboundOrderItemMapper, OutboundOrderItem> implements OutboundOrderItemService {

    @Override
    public Map<Long, BigDecimal> sumNoOutboundQtyBySkuIds(List<Long> skuIds) {
        if (CollectionUtils.isEmpty(skuIds)) {
            return new HashMap<>();
        }
        List<OutboundOrderItem> itemList = baseMapper.sumNoOutboundQtyBySkuIds(skuIds);
        if (CollectionUtils.isEmpty(itemList)) {
            return new HashMap<>();
        }
        return itemList.stream().collect(Collectors.toMap(OutboundOrderItem::getSkuId, OutboundOrderItem::getQty));
    }

    @Override
    public List<OutboundOrderItem> listByOutboundId(Long orderId) {
        return super.list(new LambdaQueryWrapper<OutboundOrderItem>().eq(OutboundOrderItem::getOrderId, orderId));
    }

    @Override
    public void delByOutboundId(Long outboundId) {
        baseMapper.delByOutboundId(outboundId);
    }

    @Override
    public boolean isInventoryOutAll(Long orderId) {
        long num = super.count(new LambdaQueryWrapper<OutboundOrderItem>()
                .eq(OutboundOrderItem::getOrderId, orderId)
                .eq(OutboundOrderItem::getStatus, OutboundOrderItemStatusEnum.NO_INVENTORY_OUT.getCode()));
        return num == 0L;
    }

    @Override
    public OutboundOrderItem validateOutboundItem(Long id) {
        OutboundOrderItem item = super.getById(id);
        Assert.notNull(item, "出库单明细异常");
        Assert.notNull(item.getStatus(), "出库单明细异常");

        return item;
    }

    @Override
    public OutboundOrderItem validateOutboundItemInventory(Long id, BigDecimal outQty) {
        OutboundOrderItem item = this.validateOutboundItem(id);
        // 断言：状态
        boolean statusFlag = item.getStatus().equals(OutboundOrderItemStatusEnum.NO_INVENTORY_OUT.getCode());
        Assert.isTrue(statusFlag, "状态异常，出库失败");
        // 断言：数量
        boolean qtyFlag = outQty.compareTo(item.getNoOutQty()) == 0;
        Assert.isTrue(qtyFlag, "出库数量不一致");

        return item;
    }

    private void updateStatus(Long orderId, Integer status) {
        super.update(new LambdaUpdateWrapper<OutboundOrderItem>()
                .set(OutboundOrderItem::getStatus, status)
                .eq(OutboundOrderItem::getOrderId, orderId));
    }

    @Override
    public void updateConfirmByOutboundId(Long outboundId) {
        super.update(new LambdaUpdateWrapper<OutboundOrderItem>()
                .set(OutboundOrderItem::getStatus, OutboundOrderItemStatusEnum.NO_INVENTORY_OUT.getCode())
                .set(OutboundOrderItem::getConfirmDate, new Date())
                .eq(OutboundOrderItem::getOrderId, outboundId));
    }

    @Override
    public void updateCancelByOutboundId(Long outboundId) {
        this.updateStatus(outboundId, OutboundOrderItemStatusEnum.CANCEL.getCode());
    }

    @Override
    public void updateInventory(Long id) {
        super.update(new LambdaUpdateWrapper<OutboundOrderItem>()
                .set(OutboundOrderItem::getStatus, OutboundOrderItemStatusEnum.INVENTORY_OUT.getCode())
                .set(OutboundOrderItem::getNoOutQty, BigDecimal.ZERO)
                .set(OutboundOrderItem::getOutboundDate, new Date())
                .eq(OutboundOrderItem::getId, id));
    }
}
