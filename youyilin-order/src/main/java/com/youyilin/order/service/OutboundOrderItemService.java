package com.youyilin.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.order.entity.OutboundOrderItem;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 出库单 子集 服务类
 */
public interface OutboundOrderItemService extends IService<OutboundOrderItem> {

    /**
     * 统计SKU未出库占用的数量
     *
     * @param skuIds SKU IDS
     * @return HashMap
     */
    Map<Long, BigDecimal> sumNoOutboundQtyBySkuIds(List<Long> skuIds);

    /**
     * 按订单查询
     *
     * @param outboundId 出库单ID
     * @return ArrayList
     */
    List<OutboundOrderItem> listByOutboundId(Long outboundId);

    /**
     * 按出库单删除明细
     *
     * @param outboundId 出库单ID
     */
    void delByOutboundId(Long outboundId);

    /**
     * 是否全部出库
     *
     * @param outboundId 出库单ID
     * @return boolean
     */
    boolean isInventoryOutAll(Long outboundId);

    /**
     * 验证出库单明细是否存在
     *
     * @param id ID
     * @return OutboundOrderItem
     */
    OutboundOrderItem validateOutboundItem(Long id);

    /**
     * 验证出库单明细是否能出库
     *
     * @param id ID
     * @return OutboundOrderItem
     */
    OutboundOrderItem validateOutboundItemInventory(Long id, BigDecimal outQty);

    /**
     * 更新出库单明细为已确认
     *
     * @param outboundId 出库单ID
     */
    void updateConfirmByOutboundId(Long outboundId);

    /**
     * 更新出库单明细为已取消
     *
     * @param outboundId 出库单ID
     */
    void updateCancelByOutboundId(Long outboundId);

    /**
     * 更新出库单明细为已出库
     *
     * @param id ID
     */
    void updateInventory(Long id);
}
