package com.youyilin.order.enums;

import java.util.ArrayList;
import java.util.List;

/**
 * 订单状态枚举
 */
public enum OrderStatusEnum {

    /**
     * 流程一(正常销售)： 未提交 -> 待付款 -> 未出库 -> 出库中 -> 待发货 -> 发货中 -> 已发货 -> 完成
     * 流程二(备货)： 未提交 -> 待付款 -> 待备货入库 -> 备货入库中 -> 备货完成
     */
    REFUND(-1, "已退款"),
    CANCEL(0, "已取消"),
    NO_SUBMIT(1, "未提交"),
    SUBMIT_NO_PAY(10, "待付款"),
    WAIT_READY_PURCHASE(15, "待备货"),
    WAIT_READY_PURCHASE_PROCESSING(16, "备货中"),
    WAIT_INVENTORY(20, "未出库"),
    WAIT_INVENTORY_APPLY(21, "出库中"),
    WAIT_SEND(30, "待发货"),
    WAIT_SEND_APPLY(31, "发货中"),
    SEND_ALL(50, "已发货"),
    FINISH(60, "完成"),
    FINISH_READY(61, "已备货");

    private int code;
    private String info;

    OrderStatusEnum(int code, String info) {
        this.code = code;
        this.info = info;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public static String queryInfoByCode(Integer code) {
        if (code == null) {
            return "";
        }
        for (OrderStatusEnum t : OrderStatusEnum.values()) {
            if (t.getCode() == code) {
                return t.getInfo();
            }
        }
        return "";
    }

    public static List<Integer> listSettledStatus() {
        List<Integer> list = new ArrayList<>();
        list.add(WAIT_INVENTORY.getCode());
        list.add(WAIT_INVENTORY_APPLY.getCode());
        list.add(WAIT_SEND.getCode());
        list.add(WAIT_SEND_APPLY.getCode());
        list.add(SEND_ALL.getCode());
        list.add(FINISH.getCode());
        return list;
    }
}
