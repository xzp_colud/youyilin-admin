package com.youyilin.order.enums;

import com.youyilin.common.entity.EnumEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * 出库反馈状态枚举
 */
public enum CommonOutboundStatusEnum {

    DEFAULT(1, "待出库"),
    PROCESSING(10, "出库中"),
    CANCEL(20, "已取消"),
    OUTBOUND(30, "已出库");

    private int code;
    private String info;

    CommonOutboundStatusEnum(int code, String info) {
        this.code = code;
        this.info = info;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public static List<EnumEntity> listEnum() {
        List<EnumEntity> list = new ArrayList<>();
        for (CommonOutboundStatusEnum t : CommonOutboundStatusEnum.values()) {
            EnumEntity ee = new EnumEntity();
            ee.setCode(t.getCode());
            ee.setDesc(t.getInfo());

            list.add(ee);
        }
        return list;
    }

    public static String queryInfoByCode(Integer code) {
        if (code == null) {
            return "";
        }
        for (CommonOutboundStatusEnum t : CommonOutboundStatusEnum.values()) {
            if (t.getCode() == code) {
                return t.getInfo();
            }
        }
        return "";
    }
}
