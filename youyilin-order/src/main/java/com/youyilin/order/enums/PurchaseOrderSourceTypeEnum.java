package com.youyilin.order.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * 采购来源类型
 */
public enum PurchaseOrderSourceTypeEnum {
    DEFAULT,
    SALE_ORDER,
    SALE_ORDER_READY;

    public static String getSourceTypeText(String sourceType) {
        if (StringUtils.isBlank(sourceType)) {
            return "";
        }
        if (sourceType.equals(SALE_ORDER.name())) {
            return "销售采购";
        }
        if (sourceType.equals(SALE_ORDER_READY.name())) {
            return "销售备货";
        }
        return "手动创建";
    }

    public static boolean isCanCancel(String sourceType) {
        if (StringUtils.isBlank(sourceType)) {
            return true;
        }
        return !sourceType.equals(SALE_ORDER_READY.name());
    }

    public static boolean isCanEdit(String sourceType) {
        return isCanCancel(sourceType);
    }
}
