package com.youyilin.order.enums;

/**
 * 订单结款状态枚举
 */
public enum CommonSettledStatusEnum {

    DEFAULT(1, "待结款"),

    PROCESSING(10, "结款中"),
    CANCEL(20, "已取消"),
    FINISH(30, "已结款");

    private int code;
    private String info;

    CommonSettledStatusEnum(int code, String info) {
        this.code = code;
        this.info = info;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public static String queryInfoByCode(Integer code) {
        if (code == null) {
            return "";
        }
        for (CommonSettledStatusEnum t : CommonSettledStatusEnum.values()) {
            if (t.getCode() == code) {
                return t.getInfo();
            }
        }
        return "";
    }
}
