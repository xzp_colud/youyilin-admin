package com.youyilin.order.enums;

/**
 * 采购订单状态
 */
public enum PurchaseOrderStatusEnum {

    CANCEL(0, "已取消"),
    NO_CONFIRM(1, "未确认"),
    PROCESSING(30, "入库中"),
    FINISH(50, "已入库");

    private int code;
    private String info;

    PurchaseOrderStatusEnum(int code, String info) {
        this.code = code;
        this.info = info;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public static String queryInfoByCode(Integer code) {
        if (code == null) {
            return "";
        }
        for (PurchaseOrderStatusEnum t : PurchaseOrderStatusEnum.values()) {
            if (t.getCode() == code) {
                return t.getInfo();
            }
        }
        return "";
    }
}
