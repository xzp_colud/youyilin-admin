package com.youyilin.order.enums;

/**
 * 加工制作单状态
 */
public enum ProcessOrderStatusEnum {

    CANCEL(0, "已取消"),
    NO_CONFIRM(1, "未确认"),
    CONFIRM(2, "已确认"),
    NO_INVENTORY_OUT(10, "未出库"),
    INVENTORY_OUT(20, "已出库"),
    NO_INVENTORY_IN(30, "未入库"),
    INVENTORY_IN(40, "已入库");

    private int code;
    private String info;

    ProcessOrderStatusEnum(int code, String info) {
        this.code = code;
        this.info = info;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public static String queryInfoByCode(int code) {
        for (OrderStatusEnum t : OrderStatusEnum.values()) {
            if (t.getCode() == code) {
                return t.getInfo();
            }
        }
        return null;
    }
}
