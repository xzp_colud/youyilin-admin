package com.youyilin.order.enums;

import java.util.ArrayList;
import java.util.List;

/**
 * 结款单状态枚举
 */
public enum SettledOrderStatusEnum {

    DEFAULT(1, "未确认"),
    CANCEL(2, "已取消"),
    CONFIRM(10, "代收款"),
    FINISH(20, "已结款");

    private int code;
    private String info;

    SettledOrderStatusEnum(int code, String info) {
        this.code = code;
        this.info = info;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public static String queryInfoByCode(Integer code) {
        if (code == null) {
            return "";
        }
        for (SettledOrderStatusEnum t : SettledOrderStatusEnum.values()) {
            if (t.getCode() == code) {
                return t.getInfo();
            }
        }
        return "";
    }

    public static List<Integer> listProcessing() {
        List<Integer> list = new ArrayList<>();
        list.add(DEFAULT.getCode());
        list.add(CONFIRM.getCode());
        return list;
    }
}
