package com.youyilin.order.enums;

/**
 * 订单退款状态枚举
 */
public enum CommonRefundStatusEnum {

    DEFAULT(1, "待退款"),

    PROCESSING(10, "退款中"),
    CANCEL(20, "已取消"),
    REFUND(30, "已退款");

    private int code;
    private String info;

    CommonRefundStatusEnum(int code, String info) {
        this.code = code;
        this.info = info;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public static String queryInfoByCode(Integer code) {
        if (code == null) {
            return "";
        }
        for (CommonRefundStatusEnum t : CommonRefundStatusEnum.values()) {
            if (t.getCode() == code) {
                return t.getInfo();
            }
        }
        return "";
    }
}
