package com.youyilin.order.enums;

/**
 * 订单收款记录状态
 */
public enum OrderCollectEnum {

    REFUND(-1, "退款"),
    WAIT_CHECK(1, "等待核验"),
    SUCCESS(10, "付款成功"),
    FAIL(20, "付款失败");

    private int code;
    private String info;

    OrderCollectEnum(int code, String info) {
        this.code = code;
        this.info = info;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public static String queryInfoByCode(Integer code) {
        if (code == null) {
            return "";
        }
        for (OrderCollectEnum t : OrderCollectEnum.values()) {
            if (t.getCode() == code) {
                return t.getInfo();
            }
        }
        return "";
    }
}
