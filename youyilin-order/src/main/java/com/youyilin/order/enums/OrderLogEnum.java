package com.youyilin.order.enums;

/**
 * 订单日志类型
 */
public enum OrderLogEnum {

    REFUND(-1, "退款"),
    CANCEL(0, "订单取消"),
    CREATE_OR_UPDATE(1, "订单保存"),
    SUBMIT(2, "订单提交"),
    FINISH(3, "订单完结"),
    SETTLED(4, "订单结款"),
    SETTLED_APPLY(5, "申请结款"),
    SETTLED_CANCEL(6, "结款取消"),
    REFUND_APPLY_CHECK(7, "退款审核"),

    SALE_ORDER_TIME_OUT(10, "订单超时"),
    SALE_ORDER_PAY(11, "订单付款"),
    SALE_ORDER_INVENTORY_OUT(12, "申请出库"),
    SALE_ORDER_SEND(13, "申请发货"),
    SALE_ORDER_ADDRESS(14, "修改收货地址"),
    SALE_ORDER_REFUND_SUCCESS(15, "退款通过"),
    SALE_ORDER_REFUND_FAIL(16, "退款无效"),
    SALE_ORDER_INVENTORY_OUT_FINISH(17, "出库完成"),
    SALE_ORDER_SEND_FINISH(18, "发货完成"),
    SALE_ORDER_TO_PURCHASE_ORDER(19, "销售申请采购"),
    SALE_ORDER_TO_PURCHASE_ORDER_CANCEL(20, "申请采购取消"),
    SALE_ORDER_READY_TO_PURCHASE_ORDER(21, "销售申请大货采购"),
    SALE_ORDER_READY_FINISH(22, "销售备货完成"),

    PURCHASE_CHECK(31, "商品核验"),
    PURCHASE_INVENTORY_IN(32, "商品入库"),
    PURCHASE_INVENTORY_IN_BATCH(33, "商品批量入库"),
    PURCHASE_ITEM_REVISE_NUM(34, "修正实际检验米数"),

    OUTBOUND_CONFIRM(40, "出库单确认"),
    OUTBOUND_INVENTORY_OUT(41, "商品出库");

    private int code;
    private String info;

    OrderLogEnum(int code, String info) {
        this.code = code;
        this.info = info;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public static String queryInfoByCode(Integer code) {
        if (code == null) {
            return "";
        }
        for (OrderLogEnum t : OrderLogEnum.values()) {
            if (t.getCode() == code) {
                return t.getInfo();
            }
        }
        return "";
    }
}
