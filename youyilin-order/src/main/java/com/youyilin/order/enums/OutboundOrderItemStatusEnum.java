package com.youyilin.order.enums;

/**
 * 出库单明细状态
 */
public enum OutboundOrderItemStatusEnum {

    CANCEL(0, "已取消"),
    NO_CONFIRM(1, "未确认"),
    NO_INVENTORY_OUT(20, "未出库"),
    INVENTORY_OUT(30, "已出库");

    private int code;
    private String info;

    OutboundOrderItemStatusEnum(int code, String info) {
        this.code = code;
        this.info = info;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public static String queryInfoByCode(Integer code) {
        if (code == null) {
            return "";
        }
        for (OutboundOrderItemStatusEnum t : OutboundOrderItemStatusEnum.values()) {
            if (t.getCode() == code) {
                return t.getInfo();
            }
        }
        return "";
    }
}
