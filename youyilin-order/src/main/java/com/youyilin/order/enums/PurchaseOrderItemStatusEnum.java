package com.youyilin.order.enums;

/**
 * 采购订单明细状态
 */
public enum PurchaseOrderItemStatusEnum {

    CANCEL(0, "已取消"),
    NO_CONFIRM(1, "未确认"),
    CONFIRM_NO_ARRIVAL(10, "待收货"),
    ARRIVAL_NO_CHECK(20, "检验中"),
    CHECK_NO_IN(30, "入库中"),
    IN_FINISH(40, "已入库");

    private int code;
    private String info;

    PurchaseOrderItemStatusEnum(int code, String info) {
        this.code = code;
        this.info = info;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public static String queryInfoByCode(Integer code) {
        if (code == null) {
            return "";
        }
        for (PurchaseOrderItemStatusEnum t : PurchaseOrderItemStatusEnum.values()) {
            if (t.getCode() == code) {
                return t.getInfo();
            }
        }
        return "";
    }
}
