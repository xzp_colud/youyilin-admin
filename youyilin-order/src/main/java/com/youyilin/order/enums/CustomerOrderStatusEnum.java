package com.youyilin.order.enums;

import com.youyilin.common.entity.EnumEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * 订单状态
 */
public enum CustomerOrderStatusEnum {

    DEFAULT(1, "待付款"),
    PAID_NO_SEND(10, "待发货"),
    SEND_NO_RECEIVED(20, "待收货"),
    RECEIVED(30, "已收货"),
    FINISH(40, "交易完成"),
    CANCEL(50, "交易已取消"),
    REFUND_NO_SEND(60, "未发货，退款成功"),
    REFUND_NO_RECEIVED(61, "已发货，退款成功"),
    REFUND_RECEIVED(62, "已收货，退款成功");

    private int code;
    private String info;

    CustomerOrderStatusEnum(int code, String info) {
        this.code = code;
        this.info = info;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public static boolean isExist(int code) {
        for (CustomerOrderStatusEnum t : CustomerOrderStatusEnum.values()) {
            if (t.getCode() == code) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    public static String queryInfoByCode(int code) {
        for (CustomerOrderStatusEnum t : CustomerOrderStatusEnum.values()) {
            if (t.getCode() == code) {
                return t.getInfo();
            }
        }
        return null;
    }

    public static List<EnumEntity> listEnum() {
        List<EnumEntity> list = new ArrayList<>();
        for (CustomerOrderStatusEnum t : CustomerOrderStatusEnum.values()) {
            EnumEntity ee = new EnumEntity();
            ee.setCode(t.getCode());
            ee.setDesc(t.getInfo());

            list.add(ee);
        }
        return list;
    }
}
