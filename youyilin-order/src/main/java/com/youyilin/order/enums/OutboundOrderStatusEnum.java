package com.youyilin.order.enums;

/**
 * 出库单状态
 */
public enum OutboundOrderStatusEnum {

    CANCEL(0, "已取消"),
    NO_CONFIRM(1, "未确认"),
    CONFIRM(2, "出库中"),
    FINISH(40, "已出库");

    private int code;
    private String info;

    OutboundOrderStatusEnum(int code, String info) {
        this.code = code;
        this.info = info;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public static String queryInfoByCode(Integer code) {
        if (code == null) {
            return "";
        }
        for (OutboundOrderStatusEnum t : OutboundOrderStatusEnum.values()) {
            if (t.getCode() == code) {
                return t.getInfo();
            }
        }
        return "";
    }
}
