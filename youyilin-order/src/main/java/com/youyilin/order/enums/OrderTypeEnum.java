package com.youyilin.order.enums;

/**
 * 订单类型
 */
public enum OrderTypeEnum {

    OFFLINE(2, "日常销售"),
    WHOLESALE(3, "批发销售");

    private int code;
    private String info;

    OrderTypeEnum(int code, String info) {
        this.code = code;
        this.info = info;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public static String queryInfoByCode(Integer code) {
        if (code == null) {
            return "";
        }
        for (OrderTypeEnum t : OrderTypeEnum.values()) {
            if (t.getCode() == code) {
                return t.getInfo();
            }
        }
        return "";
    }

    /**
     * 是否能编辑订单
     */
    public static boolean canEdit(int code) {
        return code == 2;
    }
}
