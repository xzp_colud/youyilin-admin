package com.youyilin.order.dto.outbound;

import com.youyilin.order.entity.OrderItem;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.collections.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
@Accessors(chain = true)
public class OutboundOrderItemDetailThirdConstructDTO {

    // 源订单
    private Long sourceGroupId;
    // 源订单明细ID
    private Long sourceId;
    // 源订单明细编号
    private String sourceSn;
    // 数量
    private BigDecimal qty;
    // 商品 ID
    private Long productId;
    // 商品名称
    private String productName;
    // SKU ID
    private Long skuId;
    // SKU名称
    private String skuName;
    // 分类ID
    private Long categoryId;
    // 分类名称
    private String categoryName;
    // 供货商ID
    private Long supplierId;
    // 供货商名称
    private String supplierName;

    public static List<OutboundOrderItemDetailThirdConstructDTO> convertByOrderItem(List<OrderItem> source) {
        if (CollectionUtils.isEmpty(source)) {
            return new ArrayList<>();
        }
        List<OutboundOrderItemDetailThirdConstructDTO> target = new ArrayList<>();
        for (OrderItem item : source) {
            OutboundOrderItemDetailThirdConstructDTO bo = new OutboundOrderItemDetailThirdConstructDTO();
            bo.setSourceGroupId(item.getOrderId())
                    .setSourceId(item.getId())
                    .setSourceSn(item.getItemSn())
                    .setProductId(item.getProductId())
                    .setProductName(item.getProductName())
                    .setSkuId(item.getProductPriceId())
                    .setSkuName(item.getSkuName())
                    .setCategoryId(item.getCategoryId())
                    .setCategoryName(item.getCategoryName())
                    .setSupplierId(item.getSupplierId())
                    .setSupplierName(item.getSupplierName())
                    .setQty(item.getNum());

            target.add(bo);
        }
        return target;
    }
}
