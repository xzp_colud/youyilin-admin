package com.youyilin.order.dto.sales;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 销售单备货入库表单DTO
 */
@Data
public class OrderReadyPurchaseFormDTO {

    // 销售单ID
    @NotNull(message = "销售单不能不能为空")
    private Long id;
}
