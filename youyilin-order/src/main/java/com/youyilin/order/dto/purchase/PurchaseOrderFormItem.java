package com.youyilin.order.dto.purchase;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * 采购单明细
 */
@Data
public class PurchaseOrderFormItem {

    /**
     * 商品ID
     */
    @NotNull(message = "商品不能为空")
    private Long productId;
    /**
     * SKU ID
     */
    @NotNull(message = "商品SKU不能为空")
    private Long skuId;
    /**
     * 数量
     */
    @NotNull(message = "数量不能为空")
    private BigDecimal buyNum;
    /**
     * 使用批发价标识
     */
    private Integer wholesalePriceFlag;
}
