package com.youyilin.order.dto.settled;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 结款单确认表单DTO
 */
@Data
public class SettledOrderConfirmFormDTO {

    @NotNull(message = "结款单不能为空")
    private Long id;
}
