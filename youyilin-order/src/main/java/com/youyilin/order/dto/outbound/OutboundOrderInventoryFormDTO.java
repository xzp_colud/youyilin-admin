package com.youyilin.order.dto.outbound;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 出库单出库表单DTO
 */
@Data
public class OutboundOrderInventoryFormDTO {

    @NotNull(message = "订单不能为空")
    private Long orderId;
    @NotNull(message = "订单不能为空")
    private Long orderItemId;
    @NotNull(message = "出库信息不能为空")
    private List<OutboundOrderInventoryFormOut> outList;
}
