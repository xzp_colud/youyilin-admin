package com.youyilin.order.dto.settled;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

/**
 * 结款单表单DTO
 */
@Data
public class SettledOrderFormDTO {

    // 客户ID
    @NotNull(message = "客户不能为空")
    private Long customerId;
    // 源单号集合
//    @NotNull(message = "订单不能为空")
    private List<Long> orderIds;
    // 实际结款金额
    @NotNull(message = "实际结款金额不能为空")
    private BigDecimal reallyAmount;
    // 备注
    private String remark;
}
