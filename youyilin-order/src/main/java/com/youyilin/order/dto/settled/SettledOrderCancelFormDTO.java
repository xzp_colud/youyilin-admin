package com.youyilin.order.dto.settled;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 结款单取消表单DTO
 */
@Data
public class SettledOrderCancelFormDTO {

    // 结款单ID
    @NotNull(message = "结款单不能为空")
    private Long id;
}
