package com.youyilin.order.dto.outbound;

import com.youyilin.common.utils.BeanHelper;
import com.youyilin.order.entity.OutboundOrder;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 出库单表单DTO
 */
@Data
public class OutboundOrderFormDTO {

    private Long id;
    // 关联订单号
    private String orderNo;
    // 备注
    private String remark;
    // 商品明细
    @NotNull(message = "出库商品不能为空")
    private List<OutboundOrderFormItem> itemList;

    public OutboundOrder convertToEntity() {
        return BeanHelper.map(this, OutboundOrder.class);
    }
}
