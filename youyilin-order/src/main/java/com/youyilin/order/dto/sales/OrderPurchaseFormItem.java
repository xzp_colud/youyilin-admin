package com.youyilin.order.dto.sales;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * 销售单转采购子集
 */
@Data
public class OrderPurchaseFormItem {

    @NotNull(message = "销售单不能为空")
    private Long id;
    @NotNull(message = "数量不能为空")
    private BigDecimal num;
}
