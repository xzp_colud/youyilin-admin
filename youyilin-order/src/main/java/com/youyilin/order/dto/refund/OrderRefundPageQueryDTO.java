package com.youyilin.order.dto.refund;

import lombok.Data;

/**
 * 退款列表查询条件DTO
 */
@Data
public class OrderRefundPageQueryDTO {

    private String status;
    private String sn;
    private String sourceSn;
    private String customerName;
}
