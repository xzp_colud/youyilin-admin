package com.youyilin.order.dto.purchase;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 采购单批量入库DTO
 */
@Data
public class PurchaseOrderInventoryBatchFormDTO {

    @NotNull(message = "采购单不能为空")
    private Long orderId;
    @NotNull(message = "采购单不能为空")
    private List<Long> itemIds;
    @NotNull(message = "入库仓库不能为空")
    private Long warehouseId;
    @NotNull(message = "入库库区不能为空")
    private Long warehouseAreaId;
}
