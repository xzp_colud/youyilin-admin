package com.youyilin.order.dto.purchase;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 采购单确认DTO
 */
@Data
public class PurchaseOrderConfirmFormDTO {

    @NotNull(message = "采购单不能为空")
    private Long id;
    @NotNull(message = "确认明细不能为空")
    private List<PurchaseOrderConfirmFormItem> itemList;
}
