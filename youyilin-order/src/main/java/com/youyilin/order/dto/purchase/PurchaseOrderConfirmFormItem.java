package com.youyilin.order.dto.purchase;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * 采购单确认明细
 */
@Data
public class PurchaseOrderConfirmFormItem {

    @NotNull(message = "采购单不能为空")
    private Long id;
    // 报告采购量
    @NotNull(message = "实际采购量不能为空")
    private BigDecimal reportNum;
    // 报告采购价
    @NotNull(message = "实际采购价不能为空")
    private BigDecimal reportAmount;
}
