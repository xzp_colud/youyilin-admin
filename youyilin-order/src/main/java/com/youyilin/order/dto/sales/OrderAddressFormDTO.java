package com.youyilin.order.dto.sales;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 销售单修改地址表单DTO
 */
@Data
public class OrderAddressFormDTO {

    @NotNull(message = "销售单不能为空")
    private Long id;
    @NotBlank(message = "收货人不能为空")
    private String receiverName;
    @NotBlank(message = "收货电话不能为空")
    private String receiverPhone;
    @NotBlank(message = "收货地址不能为空")
    private String receiverProvince;
    @NotBlank(message = "收货地址不能为空")
    private String receiverCity;
    @NotBlank(message = "收货地址不能为空")
    private String receiverArea;
    @NotBlank(message = "收货地址不能为空")
    private String receiverDetail;
}
