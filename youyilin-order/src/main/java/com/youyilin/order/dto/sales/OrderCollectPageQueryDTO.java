package com.youyilin.order.dto.sales;

import lombok.Data;

/**
 * 收款列表查询参数DTO
 */
@Data
public class OrderCollectPageQueryDTO {

    // 系统编号
    private String sn;
    // 源单号
    private String orderSn;
    // 付款方式
    private String payType;
}
