package com.youyilin.order.dto.purchase;

import lombok.Data;

/**
 * 采购单列表查询参数DTO
 */
@Data
public class PurchaseOrderPageQueryDTO {

    // 状态
    private Integer status;
    // 系统编号
    private String sn;
    // 外部单号
    private String orderNo;
}
