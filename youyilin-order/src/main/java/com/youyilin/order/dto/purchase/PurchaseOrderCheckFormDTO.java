package com.youyilin.order.dto.purchase;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 采购单检验DTO
 */
@Data
public class PurchaseOrderCheckFormDTO {

    @NotNull(message = "订单不能为空")
    private Long id;
    @NotNull(message = "检验商品不能为空")
    private List<PurchaseOrderCheckFormItem> itemList;
}
