package com.youyilin.order.dto.settled;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * 结款单审核表单DTO
 */
@Data
public class SettledOrderFinishFormDTO {

    @NotNull(message = "结款单不能为空")
    private Long id;
    // 交易类型
    @NotBlank(message = "交易类型不能为空")
    private String payType;
    // 交易时间
    @NotNull(message = "收款时间不能为空")
    private String optDate;
    // 实际收款金额
    @NotNull(message = "实际收款不能为空")
    private BigDecimal collectAmount;
    // 备注
    private String remark;
    // 交易图片
//    @NotNull(message = "图片不能为空")
    private String imageUrl;
}
