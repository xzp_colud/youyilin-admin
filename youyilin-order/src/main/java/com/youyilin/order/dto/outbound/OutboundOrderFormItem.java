package com.youyilin.order.dto.outbound;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * 出库单表单明细
 */
@Data
public class OutboundOrderFormItem {

    // 商品ID
    @NotNull(message = "商品不能为空")
    private Long productId;
    // SKU ID
    @NotNull(message = "商品不能为空")
    private Long skuId;
    // 出库数量
    @NotNull(message = "数量不能为空")
    private BigDecimal qty;
}
