package com.youyilin.order.dto.settled;

import lombok.Data;

/**
 * 结款单列表查询参数DTO
 */
@Data
public class SettledOrderPageQueryDTO {

    // 客户
    private Long customerId;
    // 状态
    private Integer status;
    // 编号
    private String sn;
    // 客户名称
    private String customerName;
    // 客户电话
    private String customerPhone;
    // 年份
    private String year;
    // 月份
    private String month;
}
