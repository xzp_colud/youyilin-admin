package com.youyilin.order.dto.sales;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 销售单其他费用明细
 */
@Data
public class OrderFormOtherAmount {

    // 费用名称
    private String title;
    // 金额
    private BigDecimal amount;
    // 备注
    private String remark;
    // 金额
    private Long price;
}
