package com.youyilin.order.dto;

import lombok.Data;

@Data
public class OrderLogPageQueryDTO {

    private Long orderId;
    private String sn;
}
