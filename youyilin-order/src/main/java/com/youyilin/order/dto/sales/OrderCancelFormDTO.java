package com.youyilin.order.dto.sales;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 销售单取消表单DTO
 */
@Data
public class OrderCancelFormDTO {

    @NotNull(message = "销售单不能为空")
    private Long id;
}
