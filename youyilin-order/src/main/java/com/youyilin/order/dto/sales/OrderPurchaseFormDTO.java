package com.youyilin.order.dto.sales;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 销售单转采购
 */
@Data
public class OrderPurchaseFormDTO {

    @NotNull(message = "销售单不能为空")
    private Long id;
    // 辅料标记
    private Integer subMaterialFlag;
    // 销售明细
    @NotNull(message = "销售单不能为空")
    private List<OrderPurchaseFormItem> itemList;
}
