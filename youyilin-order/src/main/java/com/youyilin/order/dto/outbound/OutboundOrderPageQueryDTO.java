package com.youyilin.order.dto.outbound;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 出库单列表查询参数DTO
 */
@Data
@Accessors(chain = true)
public class OutboundOrderPageQueryDTO {

    private Integer status;
    private String sn;
}
