package com.youyilin.order.dto.refund;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 退款审核DTO
 */
@Data
@Accessors(chain = true)
public class OrderRefundCheckDTO {

    @NotNull(message = "退款单不能为空")
    private Long id;
    @NotBlank(message = "审核原因不能为空")
    private String remark;
    @NotBlank(message = "审核结果不能为空")
    private String status;
}
