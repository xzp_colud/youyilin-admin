package com.youyilin.order.dto.sales;

import lombok.Data;

/**
 * 销售单列表查询参数DTO
 */
@Data
public class OrderPageQueryDTO {

    // 客户ID
    private Long customerId;
    // 订单类型
    private Integer orderType;
    // 状态
    private Integer status;
    // 销售单号
    private String sn;
    // 客户名称
    private String customerName;
    // 客户电话
    private String customerPhone;
}
