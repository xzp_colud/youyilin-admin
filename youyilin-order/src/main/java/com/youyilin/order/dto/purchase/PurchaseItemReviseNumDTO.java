package com.youyilin.order.dto.purchase;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * 修正采购单明细米数DTO
 */
@Data
@Accessors(chain = true)
public class PurchaseItemReviseNumDTO {

    @NotNull(message = "采购单不能为空")
    private Long id;
    @NotNull(message = "采购单不能为空")
    private Long purchaseId;
    // 修正的检验米数数量
    @NotNull(message = "修正米数不能为空")
    private BigDecimal reviseNum;
    // 备注
    private String remark;
}
