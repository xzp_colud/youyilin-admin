package com.youyilin.order.dto.purchase;

import com.youyilin.common.utils.BeanHelper;
import com.youyilin.order.entity.PurchaseOrderItem;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

/**
 * 采购单检验明细
 */
@Data
public class PurchaseOrderCheckFormItem {

    @NotNull(message = "检验商品不能为空")
    private Long id;
    @NotNull(message = "检验数量不能为空")
    private BigDecimal reallyNum;

    public static List<PurchaseOrderItem> convertToEntity(List<PurchaseOrderCheckFormItem> list) {
        return BeanHelper.map(list, PurchaseOrderItem.class);
    }
}
