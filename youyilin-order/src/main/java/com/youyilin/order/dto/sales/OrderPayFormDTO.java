package com.youyilin.order.dto.sales;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 销售单付款表单DTO
 */
@Data
public class OrderPayFormDTO {

    // 销售单ID
    @NotNull(message = "销售单不能不能为空")
    private Long orderId;
    // 客户ID
    @NotNull(message = "付款账户不能为空")
    private Long customerId;
    // 付款账户ID
    @NotNull(message = "付款账户不能为空")
    private Long accountId;
}
