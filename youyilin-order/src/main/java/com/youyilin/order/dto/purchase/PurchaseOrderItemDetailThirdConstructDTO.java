package com.youyilin.order.dto.purchase;

import com.youyilin.goods.entity.ProductFabric;
import com.youyilin.goods.entity.ProductIngredient;
import com.youyilin.goods.entity.ProductSubMaterial;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.collections.CollectionUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

@Data
@Accessors(chain = true)
public class PurchaseOrderItemDetailThirdConstructDTO {

    private Long sourceId;
    // 数量
    private BigDecimal buyNum;
    // 商品 ID
    private Long productId;
    // SKU ID
    private Long skuId;
    // 型号
    private String productName;
    // 分类
    private String categoryName;
    // sku 名称
    private String skuName;
    // 单次用量
    private BigDecimal oneUsage;
    // 制作数量
    private BigDecimal makeNum;

    public static List<PurchaseOrderItemDetailThirdConstructDTO> convertByFabric(List<ProductFabric> sourceList, BigDecimal orderItemNum, Long sourceId) {
        if (CollectionUtils.isEmpty(sourceList)) {
            return new ArrayList<>();
        }
        List<PurchaseOrderItemDetailThirdConstructDTO> targetList = new ArrayList<>();
        for (ProductFabric item : sourceList) {
            PurchaseOrderItemDetailThirdConstructDTO target = new PurchaseOrderItemDetailThirdConstructDTO();
            target.setProductId(item.getProductId())
                    .setSkuId(item.getProductPriceId())
                    .setProductName(item.getProductName())
                    .setBuyNum(item.getOneUsage().multiply(orderItemNum).setScale(2, RoundingMode.HALF_UP))
                    .setCategoryName("面料")
                    .setSourceId(sourceId)
                    .setOneUsage(item.getOneUsage())
                    .setMakeNum(orderItemNum);

            targetList.add(target);
        }
        return targetList;
    }

    public static List<PurchaseOrderItemDetailThirdConstructDTO> convertByIngredient(List<ProductIngredient> sourceList, BigDecimal orderItemNum, Long sourceId) {
        if (CollectionUtils.isEmpty(sourceList)) {
            return new ArrayList<>();
        }
        List<PurchaseOrderItemDetailThirdConstructDTO> targetList = new ArrayList<>();
        for (ProductIngredient item : sourceList) {
            PurchaseOrderItemDetailThirdConstructDTO target = new PurchaseOrderItemDetailThirdConstructDTO();
            target.setProductId(item.getProductId())
                    .setSkuId(item.getProductPriceId())
                    .setProductName(item.getProductName())
                    .setBuyNum(item.getOneUsage().multiply(orderItemNum).setScale(2, RoundingMode.HALF_UP))
                    .setCategoryName("配料")
                    .setSourceId(sourceId)
                    .setOneUsage(item.getOneUsage())
                    .setMakeNum(orderItemNum);

            targetList.add(target);
        }
        return targetList;
    }

    public static List<PurchaseOrderItemDetailThirdConstructDTO> convertBySubMaterial(List<ProductSubMaterial> sourceList, BigDecimal orderItemNum, Long sourceId) {
        if (CollectionUtils.isEmpty(sourceList)) {
            return new ArrayList<>();
        }
        List<PurchaseOrderItemDetailThirdConstructDTO> targetList = new ArrayList<>();
        for (ProductSubMaterial item : sourceList) {
            PurchaseOrderItemDetailThirdConstructDTO target = new PurchaseOrderItemDetailThirdConstructDTO();
            target.setProductId(item.getProductId())
                    .setSkuId(item.getProductPriceId())
                    .setProductName(item.getProductName())
                    .setBuyNum(item.getUsed().multiply(orderItemNum).setScale(2, RoundingMode.HALF_UP))
                    .setCategoryName("辅料")
                    .setSourceId(sourceId)
                    .setOneUsage(item.getUsed())
                    .setMakeNum(orderItemNum);

            targetList.add(target);
        }
        return targetList;
    }
}
