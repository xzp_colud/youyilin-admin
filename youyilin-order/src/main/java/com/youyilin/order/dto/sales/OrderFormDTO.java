package com.youyilin.order.dto.sales;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 销售单表单DTO
 */
@Data
public class OrderFormDTO {

    private Long id;
    // 订单类型
    private Integer orderType;
    // 客户ID
    @NotNull(message = "客户不能为空")
    private Long customerId;
    // 收货地址ID
    @NotNull(message = "客户收货地址不能为空")
    private Long addressId;
    // 来源单号
    private String sourceNo;
    // 订单来源
    private String sourceType;
    // 物流快递
    private String expressName;
    // 备注
    private String remark;
    // 预计交期
    private String deliverDate;
    // 备货标记
    private Integer readyFlag;
    // 商品明细
    @NotNull(message = "商品不能为空")
    private List<OrderFormItem> productList;
    // 其他费用明细
    private List<OrderFormOtherAmount> otherAmountList;
}
