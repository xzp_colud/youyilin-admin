package com.youyilin.order.dto.purchase;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 采购单取消DTO
 */
@Data
public class PurchaseOrderCancelFormDTO {

    @NotNull(message = "采购单不能为空")
    private Long id;
}
