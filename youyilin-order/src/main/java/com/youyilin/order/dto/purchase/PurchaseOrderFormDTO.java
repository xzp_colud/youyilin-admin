package com.youyilin.order.dto.purchase;

import com.youyilin.common.utils.BeanHelper;
import com.youyilin.order.entity.PurchaseOrder;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 采购单表单DTO
 */
@Data
public class PurchaseOrderFormDTO {

    private Long id;
    // 外部采购单号
    private String orderNo;
    // 采购时间
    @NotNull(message = "采购时间不能为空")
    private String purchaseTime;
    // 单据时间
    @NotNull(message = "单据时间不能为空")
    private String billTime;
    // 备注
    private String remark;
    // 采购商品明细
    @NotNull(message = "商品不能为空")
    private List<PurchaseOrderFormItem> productList;

    public PurchaseOrder convertToEntity() {
        return BeanHelper.map(this, PurchaseOrder.class);
    }
}
