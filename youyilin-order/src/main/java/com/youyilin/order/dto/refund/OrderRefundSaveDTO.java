package com.youyilin.order.dto.refund;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 退款保存DTO
 */
@Data
@Accessors(chain = true)
public class OrderRefundSaveDTO {

    // 订单
    @NotNull(message = "订单不能为空")
    private Long orderId;
    // 退款原因
    @NotBlank(message = "退款原因不能为空")
    private String remark;
}
