package com.youyilin.order.dto.purchase;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * 采购单入库DTO
 */
@Data
public class PurchaseOrderInventoryFormItem {

    // 仓库
    @NotNull(message = "仓库不能为空")
    private Long warehouseId;
    // 库区
    @NotNull(message = "库区不能为空")
    private Long warehouseAreaId;
    // 批次号
    private String lotSn;
    // 入库数
    @NotNull(message = "入库数不能为空")
    private BigDecimal qty;
}
