package com.youyilin.order.dto.outbound;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * 出库单出库明细
 */
@Data
public class OutboundOrderInventoryFormOut {

    // 库存批次ID
    @NotNull(message = "库存不能为空")
    private Long id;
    // 出库数量
    @NotNull(message = "出库数量不能为空")
    private BigDecimal qty;
}
