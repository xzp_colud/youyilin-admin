package com.youyilin.order.dto.outbound;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 出库单取消表单DTO
 */
@Data
public class OutboundOrderCancelFormDTO {

    @NotNull(message = "出库单不能为空")
    private Long id;
}
