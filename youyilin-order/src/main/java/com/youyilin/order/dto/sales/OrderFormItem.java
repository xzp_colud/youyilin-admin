package com.youyilin.order.dto.sales;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * 销售单明细
 */
@Data
public class OrderFormItem {

    // 商品ID
    @NotNull(message = "商品不能为空")
    private Long productId;
    // 商品SKU ID
    @NotNull(message = "商品SKU不能为空")
    private Long skuId;
    // 数量
    @NotNull(message = "数量不能为空")
    private BigDecimal num;
    // 折扣
    @NotNull(message = "折扣不能为空")
    private Integer discount;
    // 备注
    private String remark;
}
