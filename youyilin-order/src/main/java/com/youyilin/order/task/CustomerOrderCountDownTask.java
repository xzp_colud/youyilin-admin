package com.youyilin.order.task;

import com.youyilin.order.service.CustomerOrderService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class CustomerOrderCountDownTask {

    private final CustomerOrderService customerOrderService;

    public CustomerOrderCountDownTask(CustomerOrderService customerOrderService) {
        this.customerOrderService = customerOrderService;
    }

    @Scheduled(cron = "0 0/1 * * * ? ")
    public void run() {
        customerOrderService.countDown();
    }
}
