//package com.youyilin.order.task;
//
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Component;
//
///**
// * 订单自动取消
// */
//@Component
//public class OrderCancelTask {
//
//    /**
//     * 一分钟执行一次
//     */
//    @Scheduled(cron = "0 0/1 * * * ? ")
//    public void orderCancel() {
//    }
//}
