package com.youyilin.order.vo.sales;

import com.youyilin.order.vo.OrderLogDetailVO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 销售单详情VO
 */
@Data
@Accessors(chain = true)
public class OrderDetailVO {

    // 销售单
    private OrderDetail order;
    // 商品明细
    private List<OrderDetailItem> itemList;
    // 收款明细
    private List<OrderDetailCollect> collectList;
    // 日志明细
    private List<OrderLogDetailVO> logList;
    // 是否执行过采购
    private boolean purchaseFlag;
}
