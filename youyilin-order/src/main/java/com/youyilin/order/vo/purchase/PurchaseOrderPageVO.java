package com.youyilin.order.vo.purchase;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.FormatAmountUtil;
import com.youyilin.order.enums.PurchaseOrderSourceTypeEnum;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 采购单列表VO
 */
@Data
@Accessors(chain = true)
public class PurchaseOrderPageVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    private Date createDate;
    private Date modifyDate;
    // 系统编号
    private String sn;
    // 外部采购编号
    private String orderNo;
    // 采购合计
    @JsonIgnore
    private Long totalAmount;
    // 其他费用
    @JsonIgnore
    private Long otherAmount;
    // 实际金额
    @JsonIgnore
    private Long totalMoney;
    // 状态
    private Integer status;
    // 备注
    private String remark;
    // 采购时间
    private Date purchaseDate;
    // 单据时间
    private Date billDate;
    // 确认时间
    private Date confirmDate;
    @JsonIgnore
    private String sourceType;

    public String getTotalAmountText() {
        return FormatAmountUtil.format(this.totalAmount);
    }

    public String getOtherAmountText() {
        return FormatAmountUtil.format(this.otherAmount);
    }

    public String getTotalMoneyText() {
        return FormatAmountUtil.format(this.totalMoney);
    }

    public String getSourceTypeText() {
        return PurchaseOrderSourceTypeEnum.getSourceTypeText(this.sourceType);
    }

    public boolean isCanCancel() {
        return PurchaseOrderSourceTypeEnum.isCanCancel(this.sourceType);
    }
}
