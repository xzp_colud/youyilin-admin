package com.youyilin.order.vo.sales;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.common.utils.FormatAmountUtil;
import com.youyilin.order.entity.OrderItem;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 销售单明细列表VP
 */
@Data
@Accessors(chain = true)
public class OrderItemPageVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    private Date modifyDate;
    // 销售单号
    private String orderSn;
    // 明细单号
    private String itemSn;
    // 商品名称
    private String productName;
    // 分类名称
    private String categoryName;
    // 图片
    private String imageUrl;
    // SKU名称
    private String skuName;
    // 数量
    private BigDecimal num;
    // 销售单价
    private Long sellPrice;
    // 商品折扣
    private Integer discount;
    // 实际销售单价
    private Long sellAmount;
    // 优惠总价
    private Long totalDiscount;
    // 商品金额
    private Long totalSell;
    // 状态
    private Integer status;
    // 备注
    private String remark;

    public String getSellPriceText() {
        return FormatAmountUtil.format(this.sellPrice);
    }

    public String getSellAmountText() {
        return FormatAmountUtil.format(this.sellAmount);
    }

    public String getTotalDiscountText() {
        return FormatAmountUtil.format(this.totalDiscount);
    }

    public String getTotalSellText() {
        return FormatAmountUtil.format(this.totalSell);
    }

    public static List<OrderItemPageVO> convertByEntity(List<OrderItem> list) {
        return BeanHelper.map(list, OrderItemPageVO.class);
    }
}
