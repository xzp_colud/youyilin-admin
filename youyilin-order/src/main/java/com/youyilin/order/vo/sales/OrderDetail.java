package com.youyilin.order.vo.sales;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.common.utils.FormatAmountUtil;
import com.youyilin.order.entity.Order;
import com.youyilin.order.enums.CommonOutboundStatusEnum;
import com.youyilin.order.enums.CommonRefundStatusEnum;
import com.youyilin.order.enums.CommonSettledStatusEnum;
import com.youyilin.order.enums.OrderTypeEnum;
import com.youyilin.waybill.enums.WaybillStatusEnum;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 销售单详情
 */
@Data
@Accessors(chain = true)
public class OrderDetail {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    private Date createDate;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long customerId;
    private String customerName;
    private String customerPhone;
    private String province;
    private String receiverName;
    private String receiverPhone;
    private String receiverProvince;
    private String receiverCity;
    private String receiverArea;
    private String receiverDetail;
    private BigDecimal totalNum;
    private Long sellAmount;
    private Long discountAmount;
    private Long otherAmount;
    private Long sellMoney;
    private Long costAmount;
    private String sn;
    private Integer orderType;
    private String sourceType;
    private String sourceNo;
    private String remark;
    private String deliverDate;
    private Date timeOutDate;
    private Date cancelDate;
    private Date payDate;
    private Date refundDate;
    private Integer status;
    private Integer readyFlag;
    private String expressName;
    private Integer financeFlag;
    private String payType;
    private String outboundSn;
    private Integer outboundStatus;
    private String waybillSn;
    private Integer waybillStatus;
    private Integer refundStatus;
    private Integer settledStatus;

    public String getSellAmountText() {
        return FormatAmountUtil.format(this.sellAmount);
    }

    public String getDiscountAmountText() {
        return FormatAmountUtil.format(this.discountAmount);
    }

    public String getOtherAmountText() {
        return FormatAmountUtil.format(this.otherAmount);
    }

    public String getSellMoneyText() {
        return FormatAmountUtil.format(this.sellMoney);
    }

    public String getCostAmountText() {
        return FormatAmountUtil.format(this.costAmount);
    }

    public String getOrderTypeText() {
        return OrderTypeEnum.queryInfoByCode(this.orderType);
    }

    public String getOutboundStatusText() {
        if (this.outboundStatus == null) return "";

        return CommonOutboundStatusEnum.queryInfoByCode(this.outboundStatus);
    }

    public String getWaybillStatusText() {
        if (this.waybillStatus == null) return "";

        return WaybillStatusEnum.queryInfoByCode(this.waybillStatus);
    }

    public String getRefundStatusText() {
        if (this.refundStatus == null) return "";

        return CommonRefundStatusEnum.queryInfoByCode(this.refundStatus);
    }

    public String getSettledStatusText() {
        if (this.settledStatus == null) return "";

        return CommonSettledStatusEnum.queryInfoByCode(this.settledStatus);
    }

    public String getFinanceStatus() {
        return Order.getFinanceStatus(this.financeFlag);
    }

    public static OrderDetail convertByEntity(Order order) {
        return BeanHelper.map(order, OrderDetail.class);
    }
}
