package com.youyilin.order.vo.sales;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.order.entity.OrderItemRawMaterial;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.collections.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 销售单详情明细原料列表
 */
@Data
@Accessors(chain = true)
public class OrderDetailItemRawMaterial {

    // 销售单ID
    @JsonIgnore
    private Long orderId;
    // 销售单明细ID
    @JsonIgnore
    private Long orderItemId;
    // 商品名称
    private String sourceProductName;
    // SKU名称
    private String sourceSkuName;
    // 分类
    private String typeName;
    // 单次用量
    private BigDecimal oneUsage;
    // 制作数量
    private BigDecimal makeNum;
    // 使用总量
    private BigDecimal totalUsage;

    public static List<OrderDetailItemRawMaterial> convertByEntity(List<OrderItemRawMaterial> sourceList) {
        if (CollectionUtils.isEmpty(sourceList)) {
            return new ArrayList<>();
        }
        return BeanHelper.map(sourceList, OrderDetailItemRawMaterial.class);
    }
}
