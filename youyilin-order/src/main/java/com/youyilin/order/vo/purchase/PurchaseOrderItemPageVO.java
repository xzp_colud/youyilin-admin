package com.youyilin.order.vo.purchase;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.common.utils.FormatAmountUtil;
import com.youyilin.order.entity.PurchaseOrderItem;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 采购单明细列表VO
 */
@Data
@Accessors(chain = true)
public class PurchaseOrderItemPageVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    private Date modifyDate;
    // 编号
    private String itemSn;
    // 商品名称
    private String productName;
    // 分类名称
    private String categoryName;
    // 供货商名称
    private String supplierName;
    // 图片
    private String imageUrl;
    // SKU名称
    private String skuName;
    // 采购单价
    private Long price;
    // 采购数量
    private BigDecimal buyNum;
    // 实际采购
    private BigDecimal reportNum;
    // 实际数量
    private BigDecimal reallyNum;
    // 修正数量
    private BigDecimal reviseNum;
    // 采购折扣
    private Integer discount;
    // 实际采购价
    private Long amount;
    // 折扣总价
    private Long totalDiscount;
    // 采购总价
    private Long totalAmount;
    // 实际采购总价
    private Long totalReportAmount;
    // 实际采购总价
    private Long totalReallyAmount;
    // 状态
    private Integer status;
    // 备注
    private String remark;

    public String getTotalAmountText() {
        return FormatAmountUtil.format(this.totalAmount);
    }

    public String getTotalReportAmountText() {
        return FormatAmountUtil.format(this.totalReportAmount);
    }

    public String getTotalReallyAmountText() {
        return FormatAmountUtil.format(this.totalReallyAmount);
    }

    public static List<PurchaseOrderItemPageVO> convertByEntity(List<PurchaseOrderItem> list) {
        return BeanHelper.map(list, PurchaseOrderItemPageVO.class);
    }
}
