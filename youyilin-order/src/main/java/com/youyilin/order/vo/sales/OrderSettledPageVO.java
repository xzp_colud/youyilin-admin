package com.youyilin.order.vo.sales;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.Map;

/**
 * 销售单结款列表VO
 */
@Data
@Accessors(chain = true)
public class OrderSettledPageVO {

    // 客户ID
    @JsonSerialize(using = ToStringSerializer.class)
    private Long customerId;
    // 客户名称
    private String customerName;
    // 客户电话
    private String customerPhone;
    // 结款明细
    private Map<String, List<OrderSettledPageItem>> itemList;
    // 日期
//    private String day;

    // 订单明细
//    private List<OrderSettledPageItem> itemList;
}
