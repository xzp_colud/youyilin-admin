package com.youyilin.order.vo.purchase;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.common.utils.FormatAmountUtil;
import com.youyilin.order.entity.PurchaseOrderItem;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.collections.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 采购单编辑明细列表
 */
@Data
@Accessors(chain = true)
public class PurchaseOrderEditItem {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long productId;
    // 商品名称
    private String productName;
    // 分类名称
    private String categoryName;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long supplierId;
    // 供货商名称
    private String supplierName;
    // 图片
    private String imageUrl;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long productPriceId;
    // SKU名称
    private String skuName;
    // 批发价标记
    private Integer wholesalePriceFlag;
    // 采购单价
    private Long price;
    // 折扣
    private Long discount;
    // 采购数量
    private BigDecimal buyNum;
    // 备注
    private String remark;
    // sku 列表
    private List<PurchaseOrderEditItemSku> priceList;

    public String getCostPrice() {
        return FormatAmountUtil.format(this.price);
    }

    public static List<PurchaseOrderEditItem> convertByEntity(List<PurchaseOrderItem> list) {
        if (CollectionUtils.isEmpty(list)) {
            return new ArrayList<>();
        }
        return BeanHelper.map(list, PurchaseOrderEditItem.class);
    }
}
