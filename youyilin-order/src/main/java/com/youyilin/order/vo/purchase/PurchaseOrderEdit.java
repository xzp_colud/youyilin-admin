package com.youyilin.order.vo.purchase;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.order.entity.PurchaseOrder;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 采购单编辑
 */
@Data
@Accessors(chain = true)
public class PurchaseOrderEdit {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 外部采购单号
    private String orderNo;
    // 采购时间
    private Date purchaseDate;
    // 单据时间
    private Date billDate;
    // 备注
    private String remark;

    public static PurchaseOrderEdit convertByEntity(PurchaseOrder po) {
        return BeanHelper.map(po, PurchaseOrderEdit.class);
    }
}
