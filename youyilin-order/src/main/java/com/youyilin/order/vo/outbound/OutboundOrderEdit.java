package com.youyilin.order.vo.outbound;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.order.entity.OutboundOrder;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 出库单
 */
@Data
@Accessors(chain = true)
public class OutboundOrderEdit {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 关联单号
    private String orderNo;
    // 备注
    private String remark;

    public static OutboundOrderEdit convertByEntity(OutboundOrder outbound) {
        return BeanHelper.map(outbound, OutboundOrderEdit.class);
    }
}
