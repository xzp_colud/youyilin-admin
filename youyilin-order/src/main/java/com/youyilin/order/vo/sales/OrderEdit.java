package com.youyilin.order.vo.sales;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.order.entity.Order;
import com.youyilin.order.enums.OrderTypeEnum;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 销售单编辑信息
 */
@Data
@Accessors(chain = true)
public class OrderEdit {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 客户ID
    @JsonSerialize(using = ToStringSerializer.class)
    private Long customerId;
    // 客户姓名
    private String customerName;
    // 客户电话
    private String customerPhone;
    // 客户省份
    private String province;
    // 收货人
    private String receiverName;
    // 收货人电话
    private String receiverPhone;
    // 收货省份
    private String receiverProvince;
    // 收货城市
    private String receiverCity;
    // 收货地区
    private String receiverArea;
    // 收货详细
    private String receiverDetail;
    // 订单来源
    private String sourceType;
    // 来源单号
    private String sourceNo;
    // 备注
    private String remark;
    // 备货标记
    private Integer readyFlag;
    // 预计交期
    private String deliverDate;
    // 物流
    private String expressName;
    // 订单类型
    private Integer orderType;

    public boolean isWholesaleFlag() {
        return this.orderType != null && this.orderType == OrderTypeEnum.WHOLESALE.getCode();
    }

    public static OrderEdit convertByEntity(Order order) {
        return BeanHelper.map(order, OrderEdit.class);
    }
}
