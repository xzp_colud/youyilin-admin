package com.youyilin.order.vo.outbound;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.order.entity.OutboundOrderItem;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.collections.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 出库单商品明细列表VO
 */
@Data
@Accessors(chain = true)
public class OutboundOrderItemPageVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 商品名称
    private String productName;
    // 分类名称
    private String categoryName;
    // 供货商名称
    private String supplierName;
    // SKU名称
    private String skuName;
    // 采购数量
    private BigDecimal qty;
    // 备注
    private String remark;
    // 状态
    private Integer status;

    public static List<OutboundOrderItemPageVO> convertByEntity(List<OutboundOrderItem> list) {
        if (CollectionUtils.isEmpty(list)) {
            return new ArrayList<>();
        }
        return BeanHelper.map(list, OutboundOrderItemPageVO.class);
    }
}
