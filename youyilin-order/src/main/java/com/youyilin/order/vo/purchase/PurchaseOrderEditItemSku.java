package com.youyilin.order.vo.purchase;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.enums.BooleanEnum;
import com.youyilin.common.utils.FormatAmountUtil;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 销售单编辑商品sku
 */
@Data
@Accessors(chain = true)
public class PurchaseOrderEditItemSku {

    // SKU id
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // SKU 名称
    private String skuName;
    // 采购价(面料零剪价)
    private Long costPrice;
    // 批发价(面料整卷价)
    private Long wholesalePrice;
    // 状态
    private Integer status;

    public boolean isDisabled() {
        return !(this.status != null && this.status.equals(BooleanEnum.TRUE.getCode()));
    }

    public String getCostPriceText() {
        return FormatAmountUtil.format(this.costPrice);
    }

    public String getWholesalePriceText() {
        return FormatAmountUtil.format(this.wholesalePrice);
    }
}
