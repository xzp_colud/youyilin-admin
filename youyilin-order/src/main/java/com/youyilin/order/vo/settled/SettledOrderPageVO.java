package com.youyilin.order.vo.settled;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.FormatAmountUtil;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 结款单列表VO
 */
@Data
@Accessors(chain = true)
public class SettledOrderPageVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 创建时间
    private Date createDate;
    // 结款单号
    private String sn;
    // 客户名称
    private String customerName;
    // 客户电话
    private String customerPhone;
    // 状态
    private Integer status;
    // 预计结款
    @JsonIgnore
    private Long planAmount;
    // 实际结款
    @JsonIgnore
    private Long reallyAmount;
    // 实际收款
    @JsonIgnore
    private Long collectAmount;
    // 上期结余
    @JsonIgnore
    private Long lastBalanceAmount;
    // 本期结余
    @JsonIgnore
    private Long balanceAmount;
    // 结款年份
    private String year;
    // 结款月份
    private String month;
    // 确认时间
    private Date confirmDate;
    // 取消时间
    private Date cancelDate;
    // 完成时间
    private Date finishDate;
    // 备注
    private String remark;
    // 收款时间
    private String collectDate;
    // 收款类型
    private String collectType;
    // 收款备注
    private String collectRemark;
    // 收款图片
    private String collectImg;

    public String getPlanAmountText() {
        return FormatAmountUtil.format(this.planAmount);
    }

    public String getReallyAmountText() {
        return FormatAmountUtil.format(this.reallyAmount);
    }

    public String getCollectAmountText() {
        return FormatAmountUtil.format(this.collectAmount);
    }

    public String getLastBalanceAmountText() {
        return FormatAmountUtil.format(this.lastBalanceAmount);
    }

    public String getBalanceAmountText() {
        return FormatAmountUtil.format(this.balanceAmount);
    }
}
