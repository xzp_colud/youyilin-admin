package com.youyilin.order.vo.purchase;

import com.youyilin.order.vo.OrderLogDetailVO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 采购单详情VO
 */
@Data
@Accessors(chain = true)
public class PurchaseOrderDetailVO {

    // 采购单
    private PurchaseOrderDetail order;
    // 商品明细
    private List<PurchaseOrderDetailItem> itemList;
    // 日志明细
    private List<OrderLogDetailVO> logList;
}
