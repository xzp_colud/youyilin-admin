package com.youyilin.order.vo.outbound;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.order.entity.OutboundOrder;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;

/**
 * 出库单列表VO
 */
@Data
@Accessors(chain = true)
public class OutboundOrderPageVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 创建时间
    private Date createDate;
    // 系统编号
    private String sn;
    // 关联单号
    private String orderNo;
    // 状态
    private Integer status;
    // 确认时间
    private Date confirmDate;
    // 完结时间
    private Date finishDate;
    // 取消时间
    private Date cancelDate;
    // 备注
    private String remark;
    // 来源类型
    private String sourceType;

    public boolean isEdit() {
        return StringUtils.isNotBlank(this.sourceType) && this.sourceType.equals(OutboundOrder.SourceType.DEFAULT.name());
    }

    public String getSourceTypeText() {
        return OutboundOrder.SourceType.getSourceTypeText(this.sourceType);
    }
}
