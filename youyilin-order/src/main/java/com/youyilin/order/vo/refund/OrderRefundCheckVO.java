package com.youyilin.order.vo.refund;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.common.utils.FormatAmountUtil;
import com.youyilin.order.entity.OrderRefund;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 退款审核VO
 */
@Data
@Accessors(chain = true)
public class OrderRefundCheckVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 系统编号
    private String sn;
    // 源单号
    private String sourceSn;
    // 客户名称
    private String customerName;
    // 退款金额
    private Long amount;
    // 状态
    private String status;
    // 申请时间
    private Date applyDate;
    // 申请人
    private String applyName;
    // 申请备注
    private String remark;

    public String getAmountText() {
        return FormatAmountUtil.format(this.amount);
    }

    public static OrderRefundCheckVO convertByEntity(OrderRefund refund) {
        return BeanHelper.map(refund, OrderRefundCheckVO.class);
    }
}
