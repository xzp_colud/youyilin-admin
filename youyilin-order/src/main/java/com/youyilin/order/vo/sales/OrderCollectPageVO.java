package com.youyilin.order.vo.sales;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.common.utils.FormatAmountUtil;
import com.youyilin.order.entity.OrderCollect;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 销售单收款明细列表VO
 */
@Data
public class OrderCollectPageVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    private Date createDate;
    private Date modifyDate;
    // 系统编号
    private String sn;
    // 源大那好
    private String orderSn;
    // 客户名称
    private String customerName;
    // 支付方式
    private String payType;
    // 支付金额
    private Long amount;
    // 状态
    private Integer status;
    // 审核时间
    private Date checkDate;
    // 备注
    private String remark;

    public String getAmountText() {
        return FormatAmountUtil.format(this.amount);
    }

    public static List<OrderCollectPageVO> convertByEntity(List<OrderCollect> list) {
        return BeanHelper.map(list, OrderCollectPageVO.class);
    }
}
