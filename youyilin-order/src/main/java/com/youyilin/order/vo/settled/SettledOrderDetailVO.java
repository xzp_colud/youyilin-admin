package com.youyilin.order.vo.settled;

import com.youyilin.order.vo.OrderLogDetailVO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 结款单详情VO
 */
@Data
@Accessors(chain = true)
public class SettledOrderDetailVO {

    // 结款单
    private SettledOrderDetail settled;
    // 结款单明细
    private List<SettledOrderDetailItem> itemList;
    // 日志
    private List<OrderLogDetailVO> logList;
}
