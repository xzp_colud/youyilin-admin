package com.youyilin.order.vo.settled;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.common.utils.FormatAmountUtil;
import com.youyilin.order.entity.SettledOrderItem;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 结款单明细列表VO
 */
@Data
@Accessors(chain = true)
public class SettledOrderItemPageVO {

    // 明细编号
    private String sn;
    // 源编号
    private String sourceNo;
    // 结款金额
    @JsonIgnore
    private Long amount;

    public String getAmountText() {
        return FormatAmountUtil.format(this.amount);
    }

    public static List<SettledOrderItemPageVO> listByEntity(List<SettledOrderItem> list) {
        return BeanHelper.map(list, SettledOrderItemPageVO.class);
    }
}
