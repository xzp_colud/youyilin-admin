package com.youyilin.order.vo.sales;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class OrderToPurchaseQueryQtyVO {

    // 商品名称
    private String productName;
    // sku 名称
    private String skuName;
    // 分类
    private String categoryName;
    // 整体需求数量
    private BigDecimal totalQty;
    // 本次需求量
    private BigDecimal thisQty;
    // 在途采购中
    private BigDecimal purchaseQty;
    // 检验中
    private BigDecimal checkQty;
    // 占用出库量
    private BigDecimal outboundQty;
    // 库存量
    private BigDecimal inventoryQty;
}
