package com.youyilin.order.vo.purchase;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.common.utils.FormatAmountUtil;
import com.youyilin.order.entity.PurchaseOrderItem;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.collections.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 采购单详情VO
 */
@Data
@Accessors(chain = true)
public class PurchaseOrderDetailItem {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 修改时间
    private Date modifyDate;
    // 明细单号
    private String itemSn;
    // 商品ID
    @JsonSerialize(using = ToStringSerializer.class)
    private Long productId;
    // 商品名称
    private String productName;
    // 分类
    private String categoryName;
    // 供货商名称
    private String supplierName;
    // 图片
    private String imageUrl;
    // SKU名称
    private String skuName;
    // 采购单价
    private Long price;
    // 采购数量
    private BigDecimal buyNum;
    // 报告采购数量
    private BigDecimal reportNum;
    // 实际检验数量
    private BigDecimal reallyNum;
    // 修正实际检验数量
    private BigDecimal reviseNum;
    // 采购折扣
    private Integer discount;
    // 实际采购价
    @JsonIgnore
    private Long amount;
    // 报告采购价
    @JsonIgnore
    private Long reportAmount;
    // 折扣总价
    @JsonIgnore
    private Long totalDiscount;
    // 采购总价
    @JsonIgnore
    private Long totalAmount;
    // 报告采购总价
    @JsonIgnore
    private Long totalReportAmount;
    @JsonIgnore
    // 实际采购总价
    private Long totalReallyAmount;
    // 状态
    private Integer status;
    // 入库明细
    private List<PurchaseOrderDetailItemIn> inList;

    public String getPriceText() {
        return FormatAmountUtil.format(this.price);
    }

    public String getAmountText() {
        return FormatAmountUtil.format(this.amount);
    }

    public String getReportAmountText() {
        return FormatAmountUtil.format(this.reportAmount);
    }

    public String getTotalDiscountText() {
        return FormatAmountUtil.format(this.totalDiscount);
    }

    public String getTotalAmountText() {
        return FormatAmountUtil.format(this.totalAmount);
    }

    public String getTotalReportAmountText() {
        return FormatAmountUtil.format(this.totalReportAmount);
    }


    public static List<PurchaseOrderDetailItem> convertByEntity(List<PurchaseOrderItem> list) {
        if (CollectionUtils.isEmpty(list)) {
            return new ArrayList<>();
        }
        return BeanHelper.map(list, PurchaseOrderDetailItem.class);
    }
}
