package com.youyilin.order.vo.sales;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.enums.BooleanEnum;
import com.youyilin.common.utils.FormatAmountUtil;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * 销售单编辑商品sku
 */
@Data
@Accessors(chain = true)
public class OrderEditItemSku {

    // SKU id
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // SKU 名称
    private String skuName;
    // 销售单价
    private Long sellPrice;
    // 批发价
    private Long wholesalePrice;
    // 状态
    private Integer status;
    // 库存
    private BigDecimal inventory;

    public boolean isDisabled() {
        return !(this.status != null && this.status.equals(BooleanEnum.TRUE.getCode()));
    }

    public String getSellPriceText() {
        return FormatAmountUtil.format(this.sellPrice);
    }

    public String getWholesalePriceText() {
        return FormatAmountUtil.format(this.wholesalePrice);
    }
}
