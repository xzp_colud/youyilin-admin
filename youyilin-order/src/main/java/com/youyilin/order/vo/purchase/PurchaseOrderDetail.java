package com.youyilin.order.vo.purchase;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.common.utils.FormatAmountUtil;
import com.youyilin.order.entity.PurchaseOrder;
import com.youyilin.order.enums.PurchaseOrderSourceTypeEnum;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 采购单详情VO
 */
@Data
@Accessors(chain = true)
public class PurchaseOrderDetail {
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 创建时间
    private Date createDate;
    // 修改时间
    private Date modifyDate;
    // 系统编号
    private String sn;
    // 外部采购编号
    private String orderNo;
    // 采购合计
    @JsonIgnore
    private Long totalAmount;
    // 其他费用
    @JsonIgnore
    private Long otherAmount;
    // 实际金额
    @JsonIgnore
    private Long totalMoney;
    // 状态
    private Integer status;
    // 备注
    private String remark;
    // 采购时间
    private Date purchaseDate;
    // 单据时间
    private Date billDate;
    // 确认时间
    private Date confirmDate;
    // 来源类型
    @JsonIgnore
    private String sourceType;

    public String getTotalAmountText() {
        return FormatAmountUtil.format(this.totalAmount);
    }

    public String getOtherAmountText() {
        return FormatAmountUtil.format(this.otherAmount);
    }

    public String getTotalMoneyText() {
        return FormatAmountUtil.format(this.totalMoney);
    }

    public String getSourceTypeText() {
        return PurchaseOrderSourceTypeEnum.getSourceTypeText(this.sourceType);
    }

    public static PurchaseOrderDetail convertByEntity(PurchaseOrder po) {
        return BeanHelper.map(po, PurchaseOrderDetail.class);
    }
}
