package com.youyilin.order.vo.sales;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.common.utils.FormatAmountUtil;
import com.youyilin.order.entity.OrderCollect;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;

/**
 * 销售单详情收款明细
 */
@Data
@Accessors(chain = true)
public class OrderDetailCollect {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 支付时间
    private Date modifyDate;
    // 支付方式
    private String payType;
    // 支付金额
    private Long amount;
    // 状态
    private Integer status;
    // 审核时间
    private Date checkDate;
    // 备注
    private String remark;

    public String getAmountText() {
        return FormatAmountUtil.format(this.amount);
    }

    public static List<OrderDetailCollect> convertByEntity(List<OrderCollect> list) {
        return BeanHelper.map(list, OrderDetailCollect.class);
    }
}
