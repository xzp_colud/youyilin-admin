package com.youyilin.order.vo.sales;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.FormatAmountUtil;
import com.youyilin.order.entity.Order;
import com.youyilin.order.enums.CommonSettledStatusEnum;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 销售单结款列表
 */
@Data
@Accessors(chain = true)
public class OrderSettledPageItem {

    // 订单
    @JsonSerialize(using = ToStringSerializer.class)
    private Long orderId;
    // 订单编号
    private String orderSn;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long customerId;
    // 客户名称
    private String customerName;
    // 客户电话
    private String customerPhone;
    // 日期
    private String day;
    // 金额
    private Long amount;
    // 订单状态
    private Integer status;
    // 结款标记
    private Integer financeFlag;
    // 当前结款状态
    private Integer settledStatus;
    // 创建时间
    private String createDate;
    // 付款时间
    private String payDate;
    // 付款方式
    private String payType;

    public String getFinanceStatus() {
        return Order.getFinanceStatus(this.financeFlag);
    }
    public String getSettledStatus() {
        return CommonSettledStatusEnum.queryInfoByCode(this.financeFlag);
    }

    public String getAmountText() {
        return FormatAmountUtil.format(this.amount);
    }
}
