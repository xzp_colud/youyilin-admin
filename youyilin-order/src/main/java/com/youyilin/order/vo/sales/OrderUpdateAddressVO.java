package com.youyilin.order.vo.sales;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.order.entity.Order;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 销售单修改收货地址VO
 */
@Data
@Accessors(chain = true)
public class OrderUpdateAddressVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    private String sn;
    private String receiverName;
    private String receiverPhone;
    private String receiverProvince;
    private String receiverCity;
    private String receiverArea;
    private String receiverDetail;

    public static OrderUpdateAddressVO convertByEntity(Order order) {
        return BeanHelper.map(order, OrderUpdateAddressVO.class);
    }
}
