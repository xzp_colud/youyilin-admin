package com.youyilin.order.vo.purchase;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.order.entity.OrderInventoryLog;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.collections.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 采购单详情入库明细
 */
@Data
@Accessors(chain = true)
public class PurchaseOrderDetailItemIn {

    @JsonIgnore
    private Long orderItemId;
    // 仓库名称
    private String warehouseName;
    // 库区名称
    private String warehouseAreaName;
    // 批次号
    private String inventoryLotSn;
    // 入库数量
    private BigDecimal qty;

    public static List<PurchaseOrderDetailItemIn> convertByEntity(List<OrderInventoryLog> sourceList) {
        if (CollectionUtils.isEmpty(sourceList)) {
            return new ArrayList<>();
        }
        return BeanHelper.map(sourceList, PurchaseOrderDetailItemIn.class);
    }
}
