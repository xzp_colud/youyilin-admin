package com.youyilin.order.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.order.enums.OrderLogEnum;
import lombok.Data;

import java.util.Date;

@Data
public class OrderLogPageVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 时间
    private Date modifyDate;
    // 操作人
    private String modifyName;
    // 订单类型
    private Integer orderType;
    // 操作类型
    private Integer type;
    // 订单编号
    private String sn;
    // 备注
    private String remark;

    public String getTypeText() {
        if (this.type == null) return "";

        return OrderLogEnum.queryInfoByCode(this.type);
    }
}
