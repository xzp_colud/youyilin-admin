package com.youyilin.order.vo.sales;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.common.utils.FormatAmountUtil;
import com.youyilin.order.entity.OrderOtherAmount;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 销售单其他费用明细
 */
@Data
@Accessors(chain = true)
public class OrderEditOtherAmount {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 费用名称
    private String title;
    // 金额
    private Long price;
    // 备注
    private String remark;

    public String getPriceText() {
        return FormatAmountUtil.format(this.price);
    }

    public static List<OrderEditOtherAmount> convertByEntity(List<OrderOtherAmount> list) {
        return BeanHelper.map(list, OrderEditOtherAmount.class);
    }
}
