package com.youyilin.order.vo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.order.enums.OrderLogEnum;
import com.youyilin.order.entity.OrderLog;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;

/**
 * 日志(通用)详情VO
 */
@Data
@Accessors(chain = true)
public class OrderLogDetailVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 操作时间
    @TableField(value = "modify_date", fill = FieldFill.INSERT)
    private Date modifyDate;
    // 操作人
    @TableField(value = "modify_name", fill = FieldFill.INSERT)
    private String modifyName;
    // 操作类型
    private Integer type;

    public String getTypeText() {
        return this.type == null ? "" : OrderLogEnum.queryInfoByCode(this.type);
    }

    public static List<OrderLogDetailVO> convertByEntity(List<OrderLog> list) {
        return BeanHelper.map(list, OrderLogDetailVO.class);
    }
}
