package com.youyilin.order.vo.sales;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.common.utils.FormatAmountUtil;
import com.youyilin.order.entity.OrderDiscount;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 销售单优惠明细列表VO
 */
@Data
public class OrderDiscountPageVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 时间
    private Date modifyDate;
    // 销售单号
    private String orderSn;
    // 金额
    private Long amount;
    // 备注
    private String remark;

    public String getAmountText() {
        return FormatAmountUtil.format(this.amount);
    }

    public static List<OrderDiscountPageVO> convertByEntity(List<OrderDiscount> list) {
        return BeanHelper.map(list, OrderDiscountPageVO.class);
    }
}
