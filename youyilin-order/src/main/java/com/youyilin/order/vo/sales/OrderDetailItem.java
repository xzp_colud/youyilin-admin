package com.youyilin.order.vo.sales;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.common.utils.FormatAmountUtil;
import com.youyilin.order.entity.OrderItem;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.List;

/**
 * 销售单详情明细
 */
@Data
@Accessors(chain = true)
public class OrderDetailItem {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 商品名称
    private String productName;
    // 分类
    private String categoryName;
    // 图片
    private String imageUrl;
    // SKU名称
    private String skuName;
    // 数量
    private BigDecimal num;
    // 单价
    @JsonIgnore
    private Long sellPrice;
    // 实际单价
    @JsonIgnore
    private Long sellAmount;
    // 折扣
    private Integer discount;
    // 优惠总额
    @JsonIgnore
    private Long totalDiscount;
    // 总额
    @JsonIgnore
    private Long totalSell;
    // 总成本
    @JsonIgnore
    private Long totalCost;
    // 状态
    private Integer status;
    // 备注
    private String remark;
    // 原料列表
    private List<OrderDetailItemRawMaterial> rawMaterialList;

    public String getSellPriceText() {
        return FormatAmountUtil.format(this.sellPrice);
    }

    public String getSellAmountText() {
        return FormatAmountUtil.format(this.sellAmount);
    }

    public String getTotalDiscountText() {
        return FormatAmountUtil.format(this.totalDiscount);
    }

    public String getTotalSellText() {
        return FormatAmountUtil.format(this.totalSell);
    }

    public String getTotalCostText() {
        return FormatAmountUtil.format(this.totalCost);
    }

    public static List<OrderDetailItem> convertByEntity(List<OrderItem> list) {
        return BeanHelper.map(list, OrderDetailItem.class);
    }
}
