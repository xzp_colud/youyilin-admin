package com.youyilin.order.vo.outbound;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.order.entity.OutboundOrderItem;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.collections.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 出库单编辑商品明细
 */
@Data
@Accessors(chain = true)
public class OutboundOrderEditItem {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long productId;
    // 商品名称
    private String productName;
    // 分类名称
    private String categoryName;
    // 供货商名称
    private String supplierName;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long skuId;
    // SKU名称
    private String skuName;
    // 采购数量
    private BigDecimal qty;
    // 备注
    private String remark;
    // sku 列表
    private List<OutboundOrderEditItemSku> priceList;

    public static List<OutboundOrderEditItem> convertByEntity(List<OutboundOrderItem> list) {
        if (CollectionUtils.isEmpty(list)) {
            return new ArrayList<>();
        }
        return BeanHelper.map(list, OutboundOrderEditItem.class);
    }
}
