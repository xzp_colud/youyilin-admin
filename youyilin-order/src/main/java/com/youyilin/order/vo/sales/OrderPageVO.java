package com.youyilin.order.vo.sales;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.common.utils.FormatAmountUtil;
import com.youyilin.order.entity.Order;
import com.youyilin.order.enums.OrderTypeEnum;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 销售单列表VO
 */
@Data
@Accessors(chain = true)
public class OrderPageVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    private Date createDate;
    // 客户姓名
    private String customerName;
    // 客户电话
    private String customerPhone;
    // 客户省份
    private String province;
    // 收货人
    private String receiverName;
    // 收货人电话
    private String receiverPhone;
    // 收货省份
    private String receiverProvince;
    // 收货城市
    private String receiverCity;
    // 收货地区
    private String receiverArea;
    // 收货详细
    private String receiverDetail;
    // 总数
    private BigDecimal totalNum;
    // 商品销售合计
    private Long sellAmount;
    // 优惠合计
    private Long discountAmount;
    // 其他费用合计
    private Long otherAmount;
    // 实际付款合计
    private Long sellMoney;
    // 销售单号
    private String sn;
    // 订单来源
    private String sourceType;
    // 备注
    private String remark;
    // 预计交期
    private String deliverDate;
    // 取消时间
    private Date cancelDate;
    // 收款时间
    private Date payDate;
    // 状态
    private Integer status;
    // 备货标记
    private Integer readyFlag;
    // 物流
    private String expressName;
    // 结款标记
    private Integer financeFlag;
    // 退款状态
    private Integer refundStatus;
    // 收款方式
    private String payType;
    // 订单类型
    private Integer orderType;

    public String getSellAmountText() {
        return FormatAmountUtil.format(this.sellAmount);
    }

    public String getDiscountAmountText() {
        return FormatAmountUtil.format(this.discountAmount);
    }

    public String getOtherAmountText() {
        return FormatAmountUtil.format(this.otherAmount);
    }

    public String getSellMoneyText() {
        return FormatAmountUtil.format(this.sellMoney);
    }

    public String getFinanceStatus() {
        return Order.getFinanceStatus(this.financeFlag);
    }

    public String getOrderTypeText() {
        return OrderTypeEnum.queryInfoByCode(this.orderType);
    }

    public static List<OrderPageVO> convertByEntity(List<Order> list) {
        return BeanHelper.map(list, OrderPageVO.class);
    }
}
