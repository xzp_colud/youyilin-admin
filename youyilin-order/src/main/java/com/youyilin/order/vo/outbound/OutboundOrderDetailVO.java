package com.youyilin.order.vo.outbound;

import com.youyilin.order.vo.OrderLogDetailVO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 出库单详情VO
 */
@Data
@Accessors(chain = true)
public class OutboundOrderDetailVO {

    // 出库单
    private OutboundOrderDetail order;
    // 出库单明细
    private List<OutboundOrderDetailItem> orderItemList;
    // 日志
    private List<OrderLogDetailVO> logList;
}
