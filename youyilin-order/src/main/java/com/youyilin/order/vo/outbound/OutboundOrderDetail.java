package com.youyilin.order.vo.outbound;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.order.entity.OutboundOrder;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 出库单详情
 */
@Data
@Accessors(chain = true)
public class OutboundOrderDetail {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 创建时间
    private Date createDate;
    // 系统编号
    private String sn;
    // 关联单号
    private String orderNo;
    // 状态
    private Integer status;
    // 确认时间
    private Date confirmDate;
    // 完成时间
    private Date finishDate;
    // 取消时间
    private Date cancelDate;
    // 备注
    private String remark;
    // 来源类型
    private String sourceType;

    public String getSourceTypeText() {
        return OutboundOrder.SourceType.getSourceTypeText(this.sourceType);
    }

    public static OutboundOrderDetail convertByEntity(OutboundOrder outbound) {
        return BeanHelper.map(outbound, OutboundOrderDetail.class);
    }
}
