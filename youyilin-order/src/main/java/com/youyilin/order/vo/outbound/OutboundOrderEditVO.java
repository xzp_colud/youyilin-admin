package com.youyilin.order.vo.outbound;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 出库单编辑VO
 */
@Data
@Accessors(chain = true)
public class OutboundOrderEditVO {

    // 出库单
    private OutboundOrderEdit order;
    // 商品明细
    private List<OutboundOrderEditItem> itemList;
}
