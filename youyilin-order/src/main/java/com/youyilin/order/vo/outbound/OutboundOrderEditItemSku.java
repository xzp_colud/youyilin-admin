package com.youyilin.order.vo.outbound;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.enums.BooleanEnum;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 销售单编辑商品sku
 */
@Data
@Accessors(chain = true)
public class OutboundOrderEditItemSku {

    // SKU id
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // SKU 名称
    private String skuName;
    // 状态
    private Integer status;

    public boolean isDisabled() {
        return !(this.status != null && this.status.equals(BooleanEnum.TRUE.getCode()));
    }
}
