package com.youyilin.order.vo.sales;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 销售单编辑VO
 */
@Data
@Accessors(chain = true)
public class OrderEditVO {

    // 销售单
    private OrderEdit order;
    // 商品明细
    private List<OrderEditItem> itemList;
    // 其他费用明细
    private List<OrderEditOtherAmount> otherAmountList;
}
