package com.youyilin.order.vo.sales;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.common.utils.FormatAmountUtil;
import com.youyilin.order.entity.OrderOtherAmount;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 销售单其他费用明细列表VO
 */
@Data
public class OrderOtherAmountPageVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 时间
    private Date modifyDate;
    // 销售单号
    private String orderSn;
    // 费用名称
    private String title;
    // 金额
    private Long price;
    // 备注
    private String remark;

    public String getPriceText() {
        return FormatAmountUtil.format(this.price);
    }

    public static List<OrderOtherAmountPageVO> convertByEntity(List<OrderOtherAmount> list) {
        return BeanHelper.map(list, OrderOtherAmountPageVO.class);
    }
}
