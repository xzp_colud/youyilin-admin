package com.youyilin.order.vo.outbound;

import com.youyilin.common.utils.BeanHelper;
import com.youyilin.order.entity.OutboundOrderItemDetail;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.collections.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 出库单详情商品明细详情
 */
@Data
@Accessors(chain = true)
public class OutboundOrderDetailItemDetail {

    // 源单号
    private String sourceSn;
    // 数量
    private BigDecimal qty;
    // 状态
    private Integer status;

    public static List<OutboundOrderDetailItemDetail> convertByEntity(List<OutboundOrderItemDetail> list) {
        if (CollectionUtils.isEmpty(list)) {
            return new ArrayList<>();
        }
        return BeanHelper.map(list, OutboundOrderDetailItemDetail.class);
    }
}
