package com.youyilin.order.vo.purchase;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 采购单编辑VO
 */
@Data
@Accessors(chain = true)
public class PurchaseOrderEditVO {

    // 采购单
    private PurchaseOrderEdit order;
    // 商品明细
    private List<PurchaseOrderEditItem> itemList;
}
