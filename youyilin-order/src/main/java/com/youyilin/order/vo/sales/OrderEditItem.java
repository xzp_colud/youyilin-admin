package com.youyilin.order.vo.sales;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.common.utils.FormatAmountUtil;
import com.youyilin.order.entity.OrderItem;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.List;

/**
 * 销售单明细
 */
@Data
@Accessors(chain = true)
public class OrderEditItem {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 商品ID
    @JsonSerialize(using = ToStringSerializer.class)
    private Long productId;
    // 商品名称
    private String productName;
    // 分类
    private String categoryName;
    // 图片
    private String imageUrl;
    // SKU ID
    @JsonSerialize(using = ToStringSerializer.class)
    private Long productPriceId;
    // SKU名称
    private String skuName;
    // 数量
    private BigDecimal num;
    // 单价
    private Long sellPrice;
    // 折扣
    private Integer discount;
    // 备注
    private String remark;
    // SKU 列表
    private List<OrderEditItemSku> priceList;

    public String getSellPriceText() {
        return FormatAmountUtil.format(this.sellPrice);
    }

    public static List<OrderEditItem> convertByEntity(List<OrderItem> list) {
        return BeanHelper.map(list, OrderEditItem.class);
    }
}
