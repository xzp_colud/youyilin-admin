package com.youyilin.order.vo.outbound;

import com.youyilin.common.utils.BeanHelper;
import com.youyilin.order.entity.OutboundOrderInventoryLog;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.collections.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 出库商品明细出库明细
 */
@Data
@Accessors(chain = true)
public class OutboundOrderDetailItemOut {

    // 仓库
    private String warehouseName;
    // 库区
    private String warehouseAreaName;
    // 批次号
    private String inventoryLotSn;
    // 数量
    private BigDecimal qty;

    public static List<OutboundOrderDetailItemOut> convertByEntity(List<OutboundOrderInventoryLog> list) {
        if (CollectionUtils.isEmpty(list)) {
            return new ArrayList<>();
        }
        return BeanHelper.map(list, OutboundOrderDetailItemOut.class);
    }
}
