package com.youyilin.order.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 生产加工单
 */
@Data
@Accessors(chain = true)
@TableName("process_order")
public class ProcessOrder implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField(value = "create_date", fill = FieldFill.INSERT)
    private Date createDate;
    @TableField(fill = FieldFill.INSERT)
    private Long creator;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 系统编号
     */
    private String sn;
    /**
     * 来源单号ID
     */
    @TableField(value = "source_id")
    private Long sourceId;
    /**
     * 来源单号
     */
    @TableField(value = "source_sn")
    private String sourceSn;
    /**
     * 加工厂ID
     */
    @TableField(value = "process_factory_id")
    private Long processFactoryId;
    /**
     * 加工厂名称
     */
    @TableField(value = "process_factory_name")
    private String processFactoryName;
    /**
     * 类型
     */
    private Integer type;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 合计数量
     */
    @TableField(value = "total_num")
    private Integer totalNum;
    /**
     * 合计金额
     */
    @TableField(value = "total_amount")
    private Long totalAmount;
    /**
     * 备注
     */
    private String remark;
    /**
     * 确认时间
     */
    @TableField(value = "confirm_date")
    private Date confirmDate;
    /**
     * 逻辑标记
     */
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;
}
