package com.youyilin.order.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 订单收款记录
 */
@Data
@Accessors(chain = true)
@TableName("order_collect")
public class OrderCollect implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField(value = "create_date", fill = FieldFill.INSERT)
    private Date createDate;
    @TableField(fill = FieldFill.INSERT)
    private Long creator;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 付款单号
     */
    private String sn;
    /**
     * 销售单ID
     */
    @TableField(value = "order_id")
    private Long orderId;
    /**
     * 销售单编号
     */
    @TableField(value = "order_sn")
    private String orderSn;
    /**
     * 客户ID
     */
    @TableField(value = "customer_id")
    private Long customerId;
    /**
     * 账户ID
     */
    @TableField(value = "account_id")
    private Long accountId;
    /**
     * 支付方式
     */
    @TableField(value = "pay_type")
    private String payType;
    /**
     * 支付金额
     */
    private Long amount;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 审核时间
     */
    @TableField(value = "check_date")
    private Date checkDate;
    /**
     * 备注
     */
    private String remark;
    /**
     * 逻辑标记
     */
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;
}
