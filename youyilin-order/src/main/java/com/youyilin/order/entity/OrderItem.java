package com.youyilin.order.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 订单商品
 */
@Data
@Accessors(chain = true)
@TableName("order_item")
public class OrderItem implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 销售单ID
     */
    @TableField(value = "order_id")
    private Long orderId;
    /**
     * 销售单编号
     */
    @TableField(value = "order_sn")
    private String orderSn;
    /**
     * 销售单详情编号
     */
    @TableField(value = "item_sn")
    private String itemSn;
    /**
     * 商品ID
     */
    @TableField(value = "product_id")
    private Long productId;
    /**
     * 商品名称
     */
    @TableField(value = "product_name")
    private String productName;
    /**
     * 供货商ID
     */
    @TableField(value = "supplier_id")
    private Long supplierId;
    /**
     * 供货商名称
     */
    @TableField(value = "supplier_name")
    private String supplierName;
    /**
     * 类目ID
     */
    @TableField(value = "category_id")
    private Long categoryId;
    /**
     * 类目名称
     */
    @TableField(value = "category_name")
    private String categoryName;
    /**
     * 图片
     */
    @TableField(value = "image_url")
    private String imageUrl;

    /**
     * SKU ID
     */
    @TableField(value = "product_price_id")
    private Long productPriceId;
    /**
     * SKU名称
     */
    @TableField(value = "sku_name")
    private String skuName;
    /**
     * 购买数量
     */
    private BigDecimal num;
    /**
     * 销售单价
     */
    @TableField(value = "sell_price")
    private Long sellPrice;
    /**
     * 优惠比例
     */
    private Integer discount;
    /**
     * 实际销售单价
     */
    @TableField(value = "sell_amount")
    private Long sellAmount;
    /**
     * 优惠总价
     */
    @TableField(value = "total_discount")
    private Long totalDiscount;
    /**
     * 总销售金额
     */
    @TableField(value = "total_sell")
    private Long totalSell;
    /**
     * 成本单价
     */
    @TableField(value = "cost_amount")
    private Long costAmount;
    /**
     * 总进货成本
     */
    @TableField(value = "total_cost")
    private Long totalCost;

    /**
     * 物流公司
     */
    @TableField(value = "express_name")
    private String expressName;
    /**
     * 物流单号
     */
    @TableField(value = "express_no")
    private String expressNo;
    /**
     * 退款时间
     */
    @TableField(value = "refund_date")
    private Date refundDate;
    /**
     * 状态
     *
     * @see com.youyilin.order.enums.OrderStatusEnum
     */
    private Integer status;
    /**
     * 备注
     */
    private String remark;

    /**
     * 逻辑删除标记
     */
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;

    public Long getTotalSellPrice() {
        return this.num.multiply(new BigDecimal(this.sellPrice)).longValue();
    }
}
