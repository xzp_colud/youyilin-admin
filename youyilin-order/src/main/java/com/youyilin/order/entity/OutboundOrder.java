package com.youyilin.order.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.Date;

/**
 * 出库单
 */
@Data
@Accessors(chain = true)
@TableName("outbound_order")
public class OutboundOrder implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField(value = "create_date", fill = FieldFill.INSERT)
    private Date createDate;
    @TableField(fill = FieldFill.INSERT)
    private Long creator;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 系统编号
     */
    private String sn;
    /**
     * 出库编号
     */
    @TableField(value = "order_no")
    private String orderNo;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 确认时间
     */
    @TableField(value = "confirm_date")
    private Date confirmDate;
    /**
     * 完结时间
     */
    @TableField(value = "finish_date")
    private Date finishDate;
    /**
     * 取消时间
     */
    @TableField(value = "cancel_date")
    private Date cancelDate;
    /**
     * 备注
     */
    private String remark;
    /**
     * 来源类型
     */
    @TableField(value = "source_type")
    private String sourceType;

    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;

    public boolean isEdit() {
        return StringUtils.isNotBlank(this.sourceType) && SourceType.DEFAULT.name().equals(this.sourceType);
    }

    public enum SourceType {
        DEFAULT,
        CUSTOMER_ORDER,
        SALE_ORDER;

        public static String getSourceTypeText(String sourceType) {
            if (StringUtils.isBlank(sourceType)) {
                return "";
            }
            if (sourceType.equals(SALE_ORDER.name())) {
                return "销售出库";
            }
            if (sourceType.equals(CUSTOMER_ORDER.name())) {
                return "小程序出库";
            }
            return "手动创建";
        }
    }
}
