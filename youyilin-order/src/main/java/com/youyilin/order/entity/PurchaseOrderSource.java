package com.youyilin.order.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 采购单来源追溯
 */
@Data
@Accessors(chain = true)
public class PurchaseOrderSource implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 采购单ID
     */
    @TableField(value = "order_id")
    private Long orderId;
    /**
     * 源单ID
     */
    @TableField(value = "source_id")
    private Long sourceId;
    /**
     * 源单编号
     */
    @TableField(value = "source_sn")
    private String sourceSn;
    /**
     * 源类型
     */
    @TableField(value = "source_type")
    private String sourceType;

    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;
}
