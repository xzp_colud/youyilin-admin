package com.youyilin.order.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 订单其他费用
 */
@Data
@Accessors(chain = true)
@TableName("order_other_amount")
public class OrderOtherAmount implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 销售单ID
     */
    @TableField(value = "order_id")
    private Long orderId;
    /**
     * 销售单编号
     */
    @TableField(value = "order_sn")
    private String orderSn;
    /**
     * 标题
     */
    private String title;
    /**
     * 金额
     */
    private Long price;
    /**
     * 说明
     */
    private String remark;
}
