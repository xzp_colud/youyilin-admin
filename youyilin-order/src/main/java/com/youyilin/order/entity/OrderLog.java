package com.youyilin.order.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 订单日志
 */
@Data
@Accessors(chain = true)
@TableName("order_log")
public class OrderLog implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    /**
     * 时间
     */
    @TableField(value = "modify_date", fill = FieldFill.INSERT)
    private Date modifyDate;
    /**
     * 操作人
     */
    @TableField(value = "modify_name", fill = FieldFill.INSERT)
    private String modifyName;
    /**
     * 操作类型
     */
    private Integer type;
    /**
     * 订单ID
     */
    @TableField(value = "order_id")
    private Long orderId;
    /**
     * 订单编号
     */
    private String sn;
    /**
     * 其他备注
     */
    private String remark;

    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;
}
