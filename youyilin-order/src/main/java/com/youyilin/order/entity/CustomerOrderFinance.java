package com.youyilin.order.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.FormatAmountUtil;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

@Data
@Accessors(chain = true)
@TableName("customer_order_finance")
public class CustomerOrderFinance implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    @TableField(value = "create_date", fill = FieldFill.INSERT)
    private Date createDate;
    @TableField(fill = FieldFill.INSERT)
    private Long creator;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 订单ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "order_id")
    private Long orderId;
    /**
     * 订单编号
     */
    @TableField(value = "order_sn")
    private String orderSn;
    /**
     * 付款时间
     */
    @TableField(value = "pay_success_time")
    private String paySuccessTime;
    /**
     * 流水号
     */
    private String sn;
    /**
     * 付款金额
     */
    private Long amount;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 付款方式
     */
    @TableField(value = "type_name")
    private String typeName;
    /**
     * 请求单号
     */
    @TableField(value = "out_trade_no")
    private String outTradeNo;

    @TableLogic
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;

    public String getAmountText() {
        return FormatAmountUtil.format(this.amount);
    }
}
