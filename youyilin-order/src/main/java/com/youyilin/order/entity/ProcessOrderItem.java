package com.youyilin.order.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 生产加工单明细
 */
@Data
@Accessors(chain = true)
@TableName("process_order_item")
public class ProcessOrderItem implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 明细系统编号
     */
    private String sn;
    /**
     * 生产加工单ID
     */
    @TableField(value = "process_id")
    private Long processId;
    /**
     * 生产加工单编号
     */
    @TableField(value = "process_sn")
    private String processSn;
    /**
     * 商品ID
     */
    @TableField(value = "product_id")
    private Long productId;
    /**
     * 商品名称
     */
    @TableField(value = "product_name")
    private String productName;
    /**
     * 商品SKU ID
     */
    @TableField(value = "sku_id")
    private Long skuId;
    /**
     * 商品SKU名称
     */
    @TableField(value = "sku_name")
    private String skuName;
    /**
     * 分类ID
     */
    @TableField(value = "category_id")
    private String categoryId;
    /**
     * 分类名称
     */
    @TableField(value = "category_name")
    private String categoryName;
    /**
     * 供货商ID
     */
    @TableField(value = "supplier_id")
    private Long supplierId;
    /**
     * 供货商名称
     */
    @TableField(value = "supplier_name")
    private String supplierName;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 生产加工数量
     */
    private BigDecimal num;
    /**
     * 单个生产费用
     */
    private Long amount;
    /**
     * 合计生产费用
     */
    @TableField(value = "total_amount")
    private Long totalAmount;
    /**
     * 备注
     */
    private String remark;
    /**
     * 逻辑标记
     */
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;
}
