package com.youyilin.order.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 销售单原材料
 */
@Data
@Accessors(chain = true)
@TableName("order_item_raw_material")
public class OrderItemRawMaterial implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 销售单ID
     */
    @TableField(value = "order_id")
    private Long orderId;
    /**
     * 销售单编号
     */
    @TableField(value = "order_sn")
    private String orderSn;
    /**
     * 销售单子集
     */
    @TableField(value = "order_item_id")
    private Long orderItemId;
    /**
     * 销售单子集编号
     */
    @TableField(value = "order_item_sn")
    private String orderItemSn;
    /**
     * 商品ID
     */
    @TableField(value = "source_product_id")
    private Long sourceProductId;
    /**
     * 商品名称
     */
    @TableField(value = "source_product_name")
    private String sourceProductName;
    /**
     * SKU ID
     */
    @TableField(value = "source_sku_id")
    private Long sourceSkuId;
    /**
     * SKU 名称
     */
    @TableField(value = "source_sku_name")
    private String sourceSkuName;
    /**
     * 分类
     */
    @TableField(value = "type_name")
    private String typeName;
    /**
     * 单次制作用量
     */
    @TableField(value = "one_usage")
    private BigDecimal oneUsage;
    /**
     * 制作总数
     */
    @TableField(value = "make_num")
    private BigDecimal makeNum;
    /**
     * 制作总用量
     */
    @TableField(value = "total_usage")
    private BigDecimal totalUsage;

    /**
     * 逻辑标记
     */
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;
}
