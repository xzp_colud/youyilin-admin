package com.youyilin.order.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 采购订单明细
 */
@Data
@Accessors(chain = true)
@TableName("purchase_order_item")
public class PurchaseOrderItem implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField(value = "create_date", fill = FieldFill.INSERT)
    private Date createDate;
    @TableField(fill = FieldFill.INSERT)
    private Long creator;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 采购订单ID
     */
    @TableField(value = "order_id")
    private Long orderId;
    /**
     * 采购单号
     */
    private String sn;
    /**
     * 采购明细编号
     */
    @TableField(value = "item_sn")
    private String itemSn;
    /**
     * 商品ID
     */
    @TableField(value = "product_id")
    private Long productId;
    /**
     * 商品名称
     */
    @TableField(value = "product_name")
    private String productName;
    /**
     * 类目ID
     */
    @TableField(value = "category_id")
    private Long categoryId;
    /**
     * 类目名称
     */
    @TableField(value = "category_name")
    private String categoryName;
    /**
     * 供货商ID
     */
    @TableField(value = "supplier_id")
    private Long supplierId;
    /**
     * 供货商名称
     */
    @TableField(value = "supplier_name")
    private String supplierName;
    /**
     * 图片
     */
    @TableField(value = "image_url")
    private String imageUrl;
    /**
     * SKU ID
     */
    @TableField(value = "product_price_id")
    private Long productPriceId;
    /**
     * SKU名称
     */
    @TableField(value = "sku_name")
    private String skuName;
    /**
     * 采购单价
     */
    private Long price;
    /**
     * 使用批发价标识
     */
    @TableField(value = "wholesale_price_flag")
    private Integer wholesalePriceFlag;
    /**
     * 采购数量
     */
    @TableField(value = "buy_num")
    private BigDecimal buyNum;
    /**
     * 报告数量
     */
    @TableField(value = "report_num")
    private BigDecimal reportNum;
    /**
     * 实际检验数量
     */
    @TableField(value = "really_num")
    private BigDecimal reallyNum;
    /**
     * 修正检验数量
     */
    @TableField(value = "revise_num")
    private BigDecimal reviseNum;
    /**
     * 采购折扣
     */
    private Integer discount;
    /**
     * 实际采购价
     */
    private Long amount;
    /**
     * 报告采购价
     */
    @TableField(value = "report_amount")
    private Long reportAmount;
    /**
     * 折扣总价
     */
    @TableField(value = "total_discount")
    private Long totalDiscount;
    /**
     * 采购总价
     */
    @TableField(value = "total_amount")
    private Long totalAmount;
    /**
     * 报告采购总价
     */
    @TableField(value = "total_report_amount")
    private Long totalReportAmount;
    /**
     * 实际采购总价
     */
    @TableField(value = "total_really_amount")
    private Long totalReallyAmount;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 检验时间
     */
    @TableField(value = "check_date")
    private Date checkDate;
    /**
     * 入库时间
     */
    @TableField(value = "warehousing_date")
    private Date warehousingDate;

    /**
     * 逻辑标记
     */
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;
}
