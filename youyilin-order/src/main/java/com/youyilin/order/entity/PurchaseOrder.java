package com.youyilin.order.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 采购订单
 */
@Data
@Accessors(chain = true)
@TableName("purchase_order")
public class PurchaseOrder implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField(value = "create_date", fill = FieldFill.INSERT)
    private Date createDate;
    @TableField(fill = FieldFill.INSERT)
    private Long creator;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 系统编号
     */
    private String sn;
    /**
     * 采购编号
     */
    @TableField(value = "order_no")
    private String orderNo;
    /**
     * 采购合计
     */
    @TableField(value = "total_amount")
    private Long totalAmount;
    /**
     * 其他费用
     */
    @TableField(value = "other_amount")
    private Long otherAmount;
    /**
     * 实际金额
     */
    @TableField(value = "total_money")
    private Long totalMoney;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 备注
     */
    private String remark;
    /**
     * 采购时间
     */
    @TableField(value = "purchase_date")
    private Date purchaseDate;
    /**
     * 单据时间
     */
    @TableField(value = "bill_date")
    private Date billDate;
    /**
     * 确认时间
     */
    @TableField(value = "confirm_date")
    private Date confirmDate;
    /**
     * 来源类型
     */
    @TableField(value = "source_type")
    private String sourceType;
    /**
     * 取消时间
     */
    @TableField(value = "cancel_date")
    private Date cancelDate;
    /**
     * 完成时间
     */
    @TableField(value = "finish_date")
    private Date finishDate;
    /**
     * 逻辑标记
     */
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;
}
