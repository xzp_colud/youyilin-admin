package com.youyilin.order.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 生产加工单
 */
@Data
@Accessors(chain = true)
@TableName("process_order_item_material")
public class ProcessOrderItemMaterial implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 订单ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "order_id")
    private Long orderId;
    /**
     * 订单子集ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "order_item_id")
    private Long orderItemId;
    /**
     * 商品ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "product_id")
    private Long productId;
    /**
     * 商品名称
     */
    @TableField(value = "product_name")
    private String productName;
    /**
     * SKU ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "sku_id")
    private Long skuId;
    /**
     * SKU名称
     */
    @TableField(value = "sku_name")
    private String skuName;
    /**
     * 商品分类ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "category_id")
    private Long categoryId;
    /**
     * 商品分类
     */
    @TableField(value = "category_name")
    private String categoryName;
    /**
     * 数量
     */
    private BigDecimal num;
    /**
     * 状态
     */
    private Integer status;

    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;
}
