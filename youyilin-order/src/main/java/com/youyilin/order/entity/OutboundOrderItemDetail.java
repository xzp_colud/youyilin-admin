package com.youyilin.order.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 明细
 */
@Data
@Accessors(chain = true)
@TableName("outbound_order_item_detail")
public class OutboundOrderItemDetail implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 出库单ID
     */
    @TableField(value = "order_id")
    private Long orderId;
    /**
     * 出库单子集ID
     */
    @TableField(value = "order_item_id")
    private Long orderItemId;
    /**
     * 分组ID
     */
    @TableField(value = "source_group_id")
    private Long sourceGroupId;
    /**
     * 源ID
     */
    @TableField(value = "source_id")
    private Long sourceId;
    /**
     * 源单号
     */
    @TableField(value = "source_sn")
    private String sourceSn;
    /**
     * 数量
     */
    private BigDecimal qty;
    /**
     * 状态
     */
    private Integer status;
}
