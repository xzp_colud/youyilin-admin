package com.youyilin.order.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 出库单出库明细
 */
@Data
@Accessors(chain = true)
@TableName("outbound_order_inventory_log")
public class OutboundOrderInventoryLog implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 系统编号
     */
    private String sn;
    /**
     * 出库单订单ID
     */
    @TableField(value = "outbound_id")
    private Long outboundId;
    /**
     * 出库单子集ID
     */
    @TableField(value = "outbound_item_id")
    private Long outboundItemId;
    /**
     * 出库单编号
     */
    @TableField(value = "outbound_sn")
    private String outboundSn;
    /**
     * 出库单子集编号
     */
    @TableField(value = "outbound_item_sn")
    private String outboundItemSn;
    /**
     * 库存ID
     */
    @TableField(value = "inventory_id")
    private Long inventoryId;
    /**
     * 仓库ID
     */
    @TableField(value = "warehouse_id")
    private Long warehouseId;
    /**
     * 仓库名称
     */
    @TableField(value = "warehouse_name")
    private String warehouseName;
    /**
     * 库区ID
     */
    @TableField(value = "warehouse_area_id")
    private Long warehouseAreaId;
    /**
     * 库存名称
     */
    @TableField(value = "warehouse_area_name")
    private String warehouseAreaName;
    /**
     * 批次ID
     */
    @TableField(value = "inventory_lot_id")
    private Long inventoryLotId;
    /**
     * 批次号
     */
    @TableField(value = "inventory_lot_sn")
    private String inventoryLotSn;
    /**
     * 数量
     */
    private BigDecimal qty;
}
