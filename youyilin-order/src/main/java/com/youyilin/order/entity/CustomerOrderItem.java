package com.youyilin.order.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.FormatAmountUtil;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 订单-产品
 */
@Data
@TableName("customer_order_item")
public class CustomerOrderItem implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 订单ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "order_id")
    private Long orderId;
    /**
     * 编号
     */
    private String sn;
    /**
     * 产品分类
     */
    @TableField(value = "category_id")
    private Long categoryId;
    @TableField(value = "category_name")
    private String categoryName;
    /**
     * 供货商
     */
    @TableField(value = "supplier_id")
    private Long supplierId;
    @TableField(value = "supplier_name")
    private String supplierName;
    /**
     * 产品名称
     */
    @TableField(value = "product_id")
    private Long productId;
    @TableField(value = "product_name")
    private String productName;
    /**
     * 图片
     */
    private String image;
    /**
     * SKU 名称
     */
    @TableField(value = "sku_id")
    private Long skuId;
    @TableField(value = "sku_name")
    private String skuName;

    /**
     * 购买数量
     */
    private BigDecimal num;
    /**
     * 销售单价
     */
    @JsonIgnore
    @TableField(value = "sell_amount")
    private Long sellAmount;
    /**
     * 成本单价
     */
    @JsonIgnore
    @TableField(value = "cost_amount")
    private Long costAmount;
    /**
     * 销售总价
     */
    @JsonIgnore
    @TableField(value = "total_sell")
    private Long totalSell;
    /**
     * 成本总价
     */
    @JsonIgnore
    @TableField(value = "total_cost")
    private Long totalCost;
    /**
     * 总优惠
     */
    @JsonIgnore
    @TableField(value = "total_discount")
    private Long totalDiscount;
    /**
     * 实际金额
     */
    @JsonIgnore
    @TableField(value = "total_money")
    private Long totalMoney;
    /**
     * 状态
     */
    private Integer status;

    @JsonIgnore
    @TableLogic
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;

    public String getSellAmountText() {
        return FormatAmountUtil.format(this.sellAmount);
    }

    public String getCostAmountText() {
        return FormatAmountUtil.format(this.costAmount);
    }

    public String getTotalSellText() {
        return FormatAmountUtil.format(this.totalSell);
    }

    public String getTotalCostText() {
        return FormatAmountUtil.format(this.totalCost);
    }

    public String getTotalDiscountText() {
        return FormatAmountUtil.format(this.totalDiscount);
    }

    public String getTotalMoneyText() {
        return FormatAmountUtil.format(this.totalMoney);
    }

    public enum Status {

    }
}
