package com.youyilin.order.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 出库单 子集
 */
@Data
@Accessors(chain = true)
@TableName("outbound_order_item")
public class OutboundOrderItem implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 订单
     */
    @TableField(value = "order_id")
    private Long orderId;
    /**
     * 订单系统编号
     */
    private String sn;
    /**
     * 编号
     */
    @TableField(value = "item_sn")
    private String itemSn;
    /**
     * 商品分类ID
     */
    @TableField(value = "category_id")
    private Long categoryId;
    /**
     * 商品分类名称
     */
    @TableField(value = "category_name")
    private String categoryName;
    /**
     * 商品 ID
     */
    @TableField(value = "product_id")
    private Long productId;
    /**
     * 商品名称
     */
    @TableField(value = "product_name")
    private String productName;
    /**
     * SKU ID
     */
    @TableField(value = "sku_id")
    private Long skuId;
    /**
     * SKU名称
     */
    @TableField(value = "sku_name")
    private String skuName;
    /**
     * 图片
     */
    @TableField(value = "image_url")
    private String imageUrl;
    /**
     * 出库数量
     */
    private BigDecimal qty;
    /**
     * 未出库数量
     */
    @TableField(value = "no_out_qty")
    private BigDecimal noOutQty;
    /**
     * 供货商ID
     */
    @TableField(value = "supplier_id")
    private Long supplierId;
    /**
     * 供货商名称
     */
    @TableField(value = "supplier_name")
    private String supplierName;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 确认时间
     */
    @TableField(value = "confirm_date")
    private Date confirmDate;
    /**
     * 出库时间
     */
    @TableField(value = "outbound_date")
    private Date outboundDate;

    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;
}
