package com.youyilin.order.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 结款单明细
 */
@Data
@Accessors(chain = true)
@TableName("settled_order_item")
public class SettledOrderItem implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 明细编号
     */
    private String sn;
    /**
     * 结款单ID
     */
    @TableField(value = "settled_order_id")
    private Long settledOrderId;
    /**
     * 结款单编号
     */
    @TableField(value = "settled_order_sn")
    private String settledOrderSn;
    /**
     * 源单ID
     */
    @TableField(value = "source_id")
    private Long sourceId;
    /**
     * 源单编号
     */
    @TableField(value = "source_no")
    private String sourceNo;
    /**
     * 订单金额(结款金额)
     */
    private Long amount;
    /**
     * 逻辑标记
     */
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;
}
