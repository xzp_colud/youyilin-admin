package com.youyilin.order.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 订单退款
 */
@Data
@Accessors(chain = true)
@TableName("order_refund")
public class OrderRefund implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 系统编号
     */
    private String sn;
    /**
     * 订单ID
     */
    @TableField(value = "source_id")
    private Long sourceId;
    /**
     * 源单号
     */
    @TableField(value = "source_sn")
    private String sourceSn;
    /**
     * 客户ID
     */
    @TableField(value = "customer_id")
    private Long customerId;
    /**
     * 客户名称
     */
    @TableField(value = "customer_name")
    private String customerName;
    /**
     * 退款金额
     */
    private Long amount;
    /**
     * 状态
     */
    private String status;
    /**
     * 申请时间
     */
    @TableField(value = "apply_date")
    private Date applyDate;
    /**
     * 申请人
     */
    @TableField(value = "apply_id")
    private Long applyId;
    /**
     * 申请人
     */
    @TableField(value = "apply_name")
    private String applyName;
    /**
     * 申请备注
     */
    private String remark;
    /**
     * 审核时间
     */
    @TableField(value = "check_date")
    private Date checkDate;
    /**
     * 审核ID
     */
    @TableField(value = "check_id")
    private Long checkId;
    /**
     * 审核人员
     */
    @TableField(value = "check_name")
    private String checkName;
    /**
     * 审核备注
     */
    @TableField(value = "check_remark")
    private String checkRemark;
    /**
     * 逻辑标记
     */
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;

    public enum Status {
        WAITING,
        SUCCESS,
        FAIL;

        public static List<String> getWaitAndSuccess() {
            List<String> list = new ArrayList<>();
            list.add(WAITING.name());
            list.add(SUCCESS.name());
            return list;
        }
    }
}
