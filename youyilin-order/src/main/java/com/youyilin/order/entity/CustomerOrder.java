package com.youyilin.order.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.FormatAmountUtil;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 客户订单
 */
@Data
@Accessors(chain = true)
@TableName("customer_order")
public class CustomerOrder implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    @TableField(value = "create_date", fill = FieldFill.INSERT)
    private Date createDate;
    @TableField(fill = FieldFill.INSERT)
    private Long creator;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 编号
     */
    private String sn;
    /**
     * 状态
     */
    private Integer status;
//    /**
//     * 发货状态
//     */
//    @TableField(value = "send_status")
//    private Integer sendStatus;

    /**
     * 客户
     */
    @TableField(value = "customer_id")
    private Long customerId;
    private String openid;

    /**
     * 收货地址
     */
    private String name;
    private String mobile;
    private String province;
    private String city;
    private String area;
    private String detail;

    /**
     * 产品合计金额
     */
    @JsonIgnore
    @TableField(value = "total_amount")
    private Long totalAmount;
    /**
     * 优惠总额
     */
    @JsonIgnore
    @TableField(value = "total_discount")
    private Long totalDiscount;
    /**
     * 增值服务
     */
    @JsonIgnore
    @TableField(value = "total_other")
    private Long totalOther;
    /**
     * 实付金额
     */
    @JsonIgnore
    @TableField(value = "total_money")
    private Long totalMoney;
    /**
     * 产品合计总成本
     */
    @JsonIgnore
    @TableField(value = "total_cost")
    private Long totalCost;
    /**
     * 留言备注
     */
    private String remark;
    /**
     * 付款时间
     */
    @TableField(value = "pay_time")
    private Date payTime;
    /**
     * 取消时间
     */
    @TableField(value = "cancel_time")
    private Date cancelTime;
    /**
     * 付款倒计时结束时间
     */
    @TableField(value = "count_down_time")
    private Date countDownTime;
    /**
     * 发货时间
     */
    @TableField(value = "send_time")
    private Date sendTime;
    /**
     * 物流单号
     */
    @TableField(value = "express_no")
    private String expressNo;
    /**
     * 物流公司
     */
    @TableField(value = "express_name")
    private String expressName;
    /**
     * 出库单ID
     */
    @TableField(value = "outbound_id")
    private Long outboundId;
    /**
     * 出库单编号
     */
    @TableField(value = "outbound_sn")
    private String outboundSn;
    /**
     * 出库状态
     */
    @TableField(value = "outbound_status")
    private Integer outboundStatus;

    @JsonIgnore
    @TableLogic
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;

    public String getTotalAmountText() {
        return FormatAmountUtil.format(this.totalAmount);
    }

    public String getTotalCostText() {
        return FormatAmountUtil.format(this.totalCost);
    }

    public String getTotalDiscountText() {
        return FormatAmountUtil.format(this.totalDiscount);
    }

    public String getTotalOtherText() {
        return FormatAmountUtil.format(this.totalOther);
    }

    public String getTotalMoneyText() {
        return FormatAmountUtil.format(this.totalMoney);
    }
}
