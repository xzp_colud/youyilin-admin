package com.youyilin.order.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 结款单
 */
@Data
@Accessors(chain = true)
@TableName("settled_order")
public class SettledOrder implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField(value = "create_date", fill = FieldFill.INSERT)
    private Date createDate;
    @TableField(fill = FieldFill.INSERT)
    private Long creator;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 系统编号
     */
    private String sn;
    /**
     * 客户
     */
    @TableField(value = "customer_id")
    private Long customerId;
    /**
     * 客户名称
     */
    @TableField(value = "customer_name")
    private String customerName;
    /**
     * 客户电话
     */
    @TableField(value = "customer_phone")
    private String customerPhone;
    /**
     * 类型
     */
    @TableField(value = "source_type")
    private Integer sourceType;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 预计结款总额
     */
    @TableField(value = "plan_amount")
    private Long planAmount;
    /**
     * 实际结款金额
     */
    @TableField(value = "really_amount")
    private Long reallyAmount;
    /**
     * 实际收款金额
     */
    @TableField(value = "collect_amount")
    private Long collectAmount;
    /**
     * 上次结款结余
     */
    @TableField(value = "last_balance_amount")
    private Long lastBalanceAmount;
    /**
     * 本次结款结余
     */
    @TableField(value = "balance_amount")
    private Long balanceAmount;
    /**
     * 结款年份
     */
    private String year;
    /**
     * 结款月份
     */
    private String month;
    /**
     * 确认时间
     */
    @TableField(value = "confirm_date")
    private Date confirmDate;
    /**
     * 取消时间
     */
    @TableField(value = "cancel_date")
    private Date cancelDate;
    /**
     * 完成时间
     */
    @TableField(value = "finish_date")
    private Date finishDate;
    /**
     * 备注
     */
    private String remark;
    /**
     * 收款时间
     */
    @TableField(value = "collect_date")
    private String collectDate;
    /**
     * 收款类型
     */
    @TableField(value = "collect_type")
    private String collectType;
    /**
     * 收款图片
     */
    @TableField(value = "collect_img")
    private String collectImg;
    /**
     * 收款备注
     */
    @TableField(value = "collect_remark")
    private String collectRemark;
    /**
     * 逻辑标记
     */
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;
}
