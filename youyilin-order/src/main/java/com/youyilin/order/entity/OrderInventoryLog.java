package com.youyilin.order.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 订单入库日志
 */
@Data
@Accessors(chain = true)
@TableName("order_inventory_log")
public class OrderInventoryLog {

    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT)
    private Date modifyDate;

    /**
     * 采购单ID
     */
    @TableField(value = "order_id")
    private Long orderId;
    /**
     * 订单系统编号
     */
    private String sn;
    /**
     * 子集ID
     */
    @TableField(value = "order_item_id")
    private Long orderItemId;
    /**
     * 子集编号
     */
    @TableField(value = "item_sn")
    private String itemSn;
    /**
     * 仓库ID
     */
    @TableField(value = "warehouse_id")
    private Long warehouseId;
    /**
     * 仓库名称
     */
    @TableField(value = "warehouse_name")
    private String warehouseName;
    /**
     * 库区ID
     */
    @TableField(value = "warehouse_area_id")
    private Long warehouseAreaId;
    /**
     * 库区名称
     */
    @TableField(value = "warehouse_area_name")
    private String warehouseAreaName;
    /**
     * 批次
     */
    @TableField(value = "inventory_lot_id")
    private String inventoryLotId;
    /**
     * 批次号
     */
    @TableField(value = "inventory_lot_sn")
    private String inventoryLotSn;
    /**
     * 数量
     */
    private BigDecimal qty;
    /**
     * 类型
     */
    private String type;

    public enum Type {
        IN,
        OUT,
    }
}
