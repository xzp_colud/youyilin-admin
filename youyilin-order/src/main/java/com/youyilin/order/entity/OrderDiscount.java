package com.youyilin.order.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 订单折扣明细
 */
@Data
@Accessors(chain = true)
@TableName("order_discount")
public class OrderDiscount implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 销售单ID
     */
    @TableField(value = "order_id")
    private Long orderId;
    /**
     * 销售单编号
     */
    @TableField(value = "order_sn")
    private String orderSn;
    /**
     * 金额
     */
    private Long amount;
    /**
     * 备注
     */
    private String remark;
}
