package com.youyilin.order.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.youyilin.common.enums.BooleanEnum;
import com.youyilin.order.enums.CommonSettledStatusEnum;
import com.youyilin.order.enums.OrderStatusEnum;
import com.youyilin.order.enums.OrderTypeEnum;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 销售订单
 */
@Data
@Accessors(chain = true)
@TableName("`order`")
public class Order implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField(value = "create_date", fill = FieldFill.INSERT)
    private Date createDate;
    @TableField(fill = FieldFill.INSERT)
    private Long creator;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 客户ID
     */
    @TableField(value = "customer_id")
    private Long customerId;
    /**
     * 客户姓名
     */
    @TableField(value = "customer_name")
    private String customerName;
    /**
     * 客户电话
     */
    @TableField(value = "customer_phone")
    private String customerPhone;
    /**
     * 客户省份
     */
    private String province;

    /**
     * 收货人
     */
    @TableField(value = "receiver_name")
    private String receiverName;
    /**
     * 收货人电话
     */
    @TableField(value = "receiver_phone")
    private String receiverPhone;
    /**
     * 收货省份
     */
    @TableField(value = "receiver_province")
    private String receiverProvince;
    /**
     * 收货城市
     */
    @TableField(value = "receiver_city")
    private String receiverCity;
    /**
     * 收货地区
     */
    @TableField(value = "receiver_area")
    private String receiverArea;
    /**
     * 收货详细
     */
    @TableField(value = "receiver_detail")
    private String receiverDetail;

    /**
     * 购买总数
     */
    @TableField(value = "total_num")
    private BigDecimal totalNum;
    /**
     * 总产品销售金额
     */
    @TableField(value = "sell_amount")
    private Long sellAmount;
    /**
     * 订单优惠金额
     */
    @TableField(value = "discount_amount")
    private Long discountAmount;
    /**
     * 其他费用
     */
    @TableField(value = "other_amount")
    private Long otherAmount;
    /**
     * 实际总销售金额
     * totalSellMoney = sellAmount - discountAmount + otherAmount
     */
    @TableField(value = "sell_money")
    private Long sellMoney;
    /**
     * 总进货成本
     */
    @TableField(value = "cost_amount")
    private Long costAmount;

    /**
     * 订单编号
     */
    private String sn;
    /**
     * 订单类型
     *
     * @see OrderTypeEnum
     */
    @TableField(value = "order_type")
    private Integer orderType;
    /**
     * 订单来源
     */
    @TableField(value = "source_type")
    private String sourceType;
    /**
     * 来源单号
     */
    @TableField(value = "source_no")
    private String sourceNo;
    /**
     * 订单备注
     */
    private String remark;
    /**
     * 预计交期
     */
    @TableField(value = "deliver_date")
    private String deliverDate;
    /**
     * 超时时间
     */
    @TableField(value = "time_out_date")
    private Date timeOutDate;
    /**
     * 取消时间
     */
    @TableField(value = "cancel_date")
    private Date cancelDate;
    /**
     * 支付时间
     */
    @TableField(value = "pay_date")
    private Date payDate;
    /**
     * 退款时间
     */
    @TableField(value = "refund_date")
    private Date refundDate;
    /**
     * 退款状态
     */
    @TableField(value = "refund_status")
    private Integer refundStatus;
    /**
     * 状态
     *
     * @see OrderStatusEnum
     */
    private Integer status;
    /**
     * 备货标记
     */
    @TableField(value = "ready_flag")
    private Integer readyFlag;
    /**
     * 物流
     */
    @TableField(value = "express_name")
    private String expressName;

    /**
     * 结款标记
     */
    @TableField(value = "finance_flag")
    private Integer financeFlag;
    /**
     * 支付方式
     */
    @TableField(value = "pay_type")
    private String payType;
    /**
     * 完成时间
     */
    @TableField(value = "finish_date")
    private Date finishDate;

    /**
     * 出库单ID
     */
    @TableField(value = "outbound_id")
    private Long outboundId;
    /**
     * 出库单编号
     */
    @TableField(value = "outbound_sn")
    private String outboundSn;
    /**
     * 出库状态
     */
    @TableField(value = "outbound_status")
    private Integer outboundStatus;
    /**
     * 出库时间
     */
    @TableField(value = "outbound_date")
    private Date outboundDate;

    /**
     * 货运单ID
     */
    @TableField(value = "waybill_id")
    private Long waybillId;
    /**
     * 货运单编号
     */
    @TableField(value = "waybill_sn")
    private String waybillSn;
    /**
     * 货运单状态
     */
    @TableField(value = "waybill_status")
    private Integer waybillStatus;
    /**
     * 发货时间
     */
    @TableField(value = "waybill_date")
    private Date waybillDate;
    /**
     * 当前结款状态
     *
     * @see CommonSettledStatusEnum
     */
    @TableField(value = "settled_status")
    private Integer settledStatus;
    /**
     * 结算时间
     */
    @TableField(value = "settled_date")
    private Date settledDate;

    /**
     * 逻辑删除标记
     */
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;

    public static String getFinanceStatus(Integer status) {
        if (status == null) {
            return "";
        }
        if (status == BooleanEnum.TRUE.getCode()) {
            return "是";
        }
        if (status == BooleanEnum.FALSE.getCode()) {
            return "否";
        }
        return "无法结款";
    }
}
