package com.youyilin.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.order.entity.PurchaseOrderItem;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 采购订单详情 Mapper
 */
@Mapper
public interface PurchaseOrderItemMapper extends BaseMapper<PurchaseOrderItem> {

    void delByPurchaseId(Long orderId);

    List<PurchaseOrderItem> sumConfirmAndNoCheckQtyBySkuIds(List<Long> skuIds);

    List<PurchaseOrderItem> sumCheckAndNoInventoryQtyBySkuIds(List<Long> skuIds);
}
