package com.youyilin.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.order.entity.CustomerOrderLog;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CustomerOrderLogMapper extends BaseMapper<CustomerOrderLog> {
}
