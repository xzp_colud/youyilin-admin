package com.youyilin.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.order.entity.CustomerOrderFinance;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CustomerOrderFinanceMapper extends BaseMapper<CustomerOrderFinance> {
}
