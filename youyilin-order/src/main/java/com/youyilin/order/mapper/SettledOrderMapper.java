package com.youyilin.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.common.bean.Page;
import com.youyilin.order.dto.settled.SettledOrderPageQueryDTO;
import com.youyilin.order.entity.SettledOrder;
import com.youyilin.order.vo.settled.SettledOrderPageVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface SettledOrderMapper extends BaseMapper<SettledOrder> {

    Integer getTotal(Page<SettledOrderPageQueryDTO> page);

    List<SettledOrderPageVO> getPage(Page<SettledOrderPageQueryDTO> page);
}
