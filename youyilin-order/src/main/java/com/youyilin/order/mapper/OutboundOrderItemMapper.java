package com.youyilin.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.order.entity.OutboundOrderItem;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 出库单 子集 Mapper
 */
@Mapper
public interface OutboundOrderItemMapper extends BaseMapper<OutboundOrderItem> {

    void delByOutboundId(Long orderId);

    List<OutboundOrderItem> sumNoOutboundQtyBySkuIds(List<Long> skuIds);
}
