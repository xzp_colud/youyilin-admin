package com.youyilin.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.common.bean.Page;
import com.youyilin.order.dto.refund.OrderRefundPageQueryDTO;
import com.youyilin.order.entity.OrderRefund;
import com.youyilin.order.vo.refund.OrderRefundPageVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 订单退款 Mapper
 */
@Mapper
public interface OrderRefundMapper extends BaseMapper<OrderRefund> {

    Integer getTotal(Page<OrderRefundPageQueryDTO> page);

    List<OrderRefundPageVO> getPage(Page<OrderRefundPageQueryDTO> page);
}
