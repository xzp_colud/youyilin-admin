package com.youyilin.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.order.entity.OrderInventoryLog;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购单入库日志
 */
@Mapper
public interface OrderInventoryLogMapper extends BaseMapper<OrderInventoryLog> {

}
