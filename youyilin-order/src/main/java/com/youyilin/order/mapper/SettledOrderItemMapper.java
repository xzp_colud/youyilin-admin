package com.youyilin.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.order.entity.SettledOrderItem;
import org.apache.ibatis.annotations.Mapper;

/**
 * 结款单明细 Mapper
 */
@Mapper
public interface SettledOrderItemMapper extends BaseMapper<SettledOrderItem> {
}
