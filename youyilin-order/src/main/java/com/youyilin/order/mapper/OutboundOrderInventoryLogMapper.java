package com.youyilin.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.order.entity.OutboundOrderInventoryLog;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OutboundOrderInventoryLogMapper extends BaseMapper<OutboundOrderInventoryLog> {
}
