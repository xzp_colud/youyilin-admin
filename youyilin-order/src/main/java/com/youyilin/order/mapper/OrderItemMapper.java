package com.youyilin.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.order.entity.OrderItem;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单详情 Mapper
 */
@Mapper
public interface OrderItemMapper extends BaseMapper<OrderItem> {

    void delByOrderId(Long orderId);
}
