package com.youyilin.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.order.entity.PurchaseOrderSource;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PurchaseOrderSourceMapper extends BaseMapper<PurchaseOrderSource> {
}
