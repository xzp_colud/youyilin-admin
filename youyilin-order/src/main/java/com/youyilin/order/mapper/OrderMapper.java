package com.youyilin.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.common.bean.Page;
import com.youyilin.order.dto.sales.OrderPageQueryDTO;
import com.youyilin.order.entity.Order;
import com.youyilin.order.vo.sales.OrderPageVO;
import com.youyilin.order.vo.sales.OrderSettledPageItem;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 订单 Mapper
 */
@Mapper
public interface OrderMapper extends BaseMapper<Order> {

    Integer getTotal(Page<OrderPageQueryDTO> page);

    List<OrderPageVO> getPage(Page<OrderPageQueryDTO> page);

    List<OrderSettledPageItem> listSettledPage();
}
