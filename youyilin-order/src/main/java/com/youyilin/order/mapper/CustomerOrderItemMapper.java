package com.youyilin.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.order.entity.CustomerOrderItem;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CustomerOrderItemMapper extends BaseMapper<CustomerOrderItem> {
}
