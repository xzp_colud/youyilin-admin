package com.youyilin.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.common.bean.Page;
import com.youyilin.order.dto.purchase.PurchaseOrderPageQueryDTO;
import com.youyilin.order.entity.PurchaseOrder;
import com.youyilin.order.vo.purchase.PurchaseOrderPageVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 采购订单 Mapper
 */
@Mapper
public interface PurchaseOrderMapper extends BaseMapper<PurchaseOrder> {

    Integer getTotal(Page<PurchaseOrderPageQueryDTO> page);

    List<PurchaseOrderPageVO> getPage(Page<PurchaseOrderPageQueryDTO> page);
}
