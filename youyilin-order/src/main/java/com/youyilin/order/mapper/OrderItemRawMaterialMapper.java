package com.youyilin.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.order.entity.OrderItemRawMaterial;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 销售单子集原料明细 Mapper
 */
@Mapper
public interface OrderItemRawMaterialMapper extends BaseMapper<OrderItemRawMaterial> {

    List<OrderItemRawMaterial> sumTotalRawMaterialBySkuIds(List<Long> skuIds);
}
