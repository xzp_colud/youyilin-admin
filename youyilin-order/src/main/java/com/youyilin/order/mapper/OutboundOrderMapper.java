package com.youyilin.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.common.bean.Page;
import com.youyilin.order.dto.outbound.OutboundOrderPageQueryDTO;
import com.youyilin.order.entity.OutboundOrder;
import com.youyilin.order.vo.outbound.OutboundOrderPageVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 出库单 Mapper
 */
@Mapper
public interface OutboundOrderMapper extends BaseMapper<OutboundOrder> {

    Integer getTotal(Page<OutboundOrderPageQueryDTO> page);

    List<OutboundOrderPageVO> getPage(Page<OutboundOrderPageQueryDTO> page);
}
