package com.youyilin.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.common.bean.Page;
import com.youyilin.order.entity.CustomerOrder;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface CustomerOrderMapper extends BaseMapper<CustomerOrder> {

    Integer getTotal(Page<CustomerOrder> page);

    List<CustomerOrder> getPage(Page<CustomerOrder> page);

    void countDown();
}
