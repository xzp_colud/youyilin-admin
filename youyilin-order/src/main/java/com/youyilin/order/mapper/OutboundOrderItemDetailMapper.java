package com.youyilin.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.order.entity.OutboundOrderItemDetail;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OutboundOrderItemDetailMapper extends BaseMapper<OutboundOrderItemDetail> {
}
