package com.youyilin.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.common.bean.Page;
import com.youyilin.order.dto.sales.OrderCollectPageQueryDTO;
import com.youyilin.order.entity.OrderCollect;
import com.youyilin.order.vo.sales.OrderCollectPageVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 订单收款记录 Mapper
 */
@Mapper
public interface OrderCollectMapper extends BaseMapper<OrderCollect> {

    Integer getTotal(Page<OrderCollectPageQueryDTO> page);

    List<OrderCollectPageVO> getPage(Page<OrderCollectPageQueryDTO> page);
}
