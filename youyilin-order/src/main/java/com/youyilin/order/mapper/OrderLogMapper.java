package com.youyilin.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.common.bean.Page;
import com.youyilin.order.dto.OrderLogPageQueryDTO;
import com.youyilin.order.entity.OrderLog;
import com.youyilin.order.vo.OrderLogPageVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 订单日志 Mapper
 */
@Mapper
public interface OrderLogMapper extends BaseMapper<OrderLog> {

    Integer getTotal(Page<OrderLogPageQueryDTO> page);

    List<OrderLogPageVO> getPage(Page<OrderLogPageQueryDTO> page);
}
