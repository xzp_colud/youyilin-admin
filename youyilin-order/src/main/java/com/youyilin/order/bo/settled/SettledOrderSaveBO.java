package com.youyilin.order.bo.settled;

import com.youyilin.customer.entity.Customer;
import com.youyilin.order.dto.settled.SettledOrderFormDTO;
import com.youyilin.order.entity.Order;
import com.youyilin.order.entity.SettledOrder;
import com.youyilin.order.entity.SettledOrderItem;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 结款单保存BO
 */
@Data
@Accessors(chain = true)
public class SettledOrderSaveBO {

    // 结款单表单
    private SettledOrderFormDTO dto;
    // 上次结款单
    private SettledOrder lastSettled;
    // 结款单Id
    private Long id;
    // 结款的那编号
    private String sn;
    // 客户
    private Customer customer;
    // 销售单源列表
    private List<Order> orderList;
    // 结款单
    private SettledOrder settled;
    // 结款单明细
    private List<SettledOrderItem> itemList;
}
