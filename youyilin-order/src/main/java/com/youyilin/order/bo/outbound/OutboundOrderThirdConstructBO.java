package com.youyilin.order.bo.outbound;

import com.youyilin.order.dto.outbound.OutboundOrderItemDetailThirdConstructDTO;
import com.youyilin.order.entity.OutboundOrder;
import com.youyilin.order.entity.OutboundOrderItem;
import com.youyilin.order.entity.OutboundOrderItemDetail;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 出库单构造BO
 * 由内部其他订单转换而来
 */
@Data
@Accessors(chain = true)
public class OutboundOrderThirdConstructBO {

    // 源单号
    private String sourceSn;
    // 出库源数据
    private List<OutboundOrderItemDetailThirdConstructDTO> sourceList;
    // 出库单
    private OutboundOrder outboundOrder;
    // 出库单明细
    private List<OutboundOrderItem> itemList;
    // 出库单明细详情
    private List<OutboundOrderItemDetail> itemDetailList;
}
