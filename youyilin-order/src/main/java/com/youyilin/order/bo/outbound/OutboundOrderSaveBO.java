package com.youyilin.order.bo.outbound;

import com.youyilin.order.dto.outbound.OutboundOrderFormDTO;
import com.youyilin.order.entity.OutboundOrder;
import com.youyilin.order.entity.OutboundOrderItem;
import com.youyilin.order.entity.OutboundOrderItemDetail;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 出库单保存BO
 * 页面手动构建
 */
@Data
@Accessors(chain = true)
public class OutboundOrderSaveBO {

    // 页面出库单表单
    private OutboundOrderFormDTO dto;
    // 出库单ID
    private Long id;
    // 出库单编号
    private String sn;
    // 是否为编辑
    private boolean isEdit;
    // 持久化的出库单
    private OutboundOrder outboundOrder;
    // 商品明细
    private List<OutboundOrderItem> itemList;
    // 商品明细详情
    private List<OutboundOrderItemDetail> itemDetailList;
}
