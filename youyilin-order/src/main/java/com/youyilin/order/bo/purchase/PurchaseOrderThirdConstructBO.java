package com.youyilin.order.bo.purchase;

import com.youyilin.order.dto.purchase.PurchaseOrderItemDetailThirdConstructDTO;
import com.youyilin.order.entity.PurchaseOrder;
import com.youyilin.order.entity.PurchaseOrderItem;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 采购单构造BO
 * 由内部其他订单转换而来
 */
@Data
@Accessors(chain = true)
public class PurchaseOrderThirdConstructBO {

    public PurchaseOrderThirdConstructBO() {
    }

    public PurchaseOrderThirdConstructBO(Long sourceId, String sourceSn, String sourceType) {
        this.sourceId = sourceId;
        this.sourceSn = sourceSn;
        this.sourceType = sourceType;
    }

    // 源单ID
    private Long sourceId;
    // 源单号
    private String sourceSn;
    // 源单类型
    private String sourceType;
    // 采购源明细
    private List<PurchaseOrderItemDetailThirdConstructDTO> sourceList;

    // 采购单总金额
    private Long totalAmount;
    // 持久化的采购单
    private PurchaseOrder purchaseOrder;
    // 持久化的采购单明细
    private List<PurchaseOrderItem> itemList;
}
