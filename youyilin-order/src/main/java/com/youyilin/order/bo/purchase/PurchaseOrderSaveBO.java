package com.youyilin.order.bo.purchase;

import com.youyilin.goods.dto.PurchaseValidateProductDTO;
import com.youyilin.order.dto.purchase.PurchaseOrderFormDTO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 采购单保存BO
 */
@Data
@Accessors(chain = true)
public class PurchaseOrderSaveBO {

    private PurchaseOrderFormDTO dto;

    // 采购单ID
    private Long id;
    // 采购单系统编号
    private String sn;
    // 是否为编辑
    private boolean isEdit;
    // 商品明细
    private List<PurchaseValidateProductDTO> productList;
}
