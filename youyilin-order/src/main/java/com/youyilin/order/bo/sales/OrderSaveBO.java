package com.youyilin.order.bo.sales;

import com.youyilin.customer.entity.Customer;
import com.youyilin.customer.entity.CustomerAddress;
import com.youyilin.order.dto.sales.OrderFormDTO;
import com.youyilin.order.entity.Order;
import com.youyilin.order.entity.OrderItem;
import com.youyilin.order.entity.OrderOtherAmount;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 销售单保存BO
 */
@Data
@Accessors(chain = true)
public class OrderSaveBO {

    // 表单DTO
    private OrderFormDTO dto;
    // 销售单ID
    private Long id;
    // 销售单号
    private String sn;
    // 是否编辑
    private boolean isEdit;
    // 客户信息
    private Customer customer;
    // 收货地址信息
    private CustomerAddress address;
    // 持久化销售单信息
    private Order order;
    // 持久化销售单明细信息
    private List<OrderItem> itemList;
    // 持久化销售单其他费用信息
    private List<OrderOtherAmount> otherAmountList;
}
