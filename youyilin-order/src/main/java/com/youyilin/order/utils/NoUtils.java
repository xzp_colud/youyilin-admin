package com.youyilin.order.utils;

import com.youyilin.common.utils.DateUtils;
import org.apache.commons.lang3.RandomStringUtils;

/**
 * 编号生成规格
 */
public class NoUtils {

    public final static String CUSTOMER_ORDER_PREFIX = "YCMO";
    // 销售单
    public final static String SALE_ORDER_PREFIX = "SO";
    // 销售单
    public final static String SALE_ORDER_COLLECT_PREFIX = "SOC";
    // 采购单
    public final static String PURCHASE_ORDER_PREFIX = "PO";
    // 出库单
    public final static String OUTBOUND_ORDER_PREFIX = "OO";
    // 加工单
    public final static String PROCESS_WORK_ORDER_PREFIX = "PWO";
    // 出库单库存明细
    public final static String OUTBOUND_ORDER_INVENTORY_PREFIX = "OOI";
    // 结款单
    public final static String SETTLED_PAYMENT_ORDER_PREFIX = "SPO";
    // 退款单
    public final static String REFUND_ORDER_PREFIX = "RO";
    // 批次号
    public final static String INVENTORY_LOT_PREFIX = "RKPC";

    public static synchronized String getSn() {
        String id = DateUtils.getDate("yyyyMMddHHmmss");
        String randomString = RandomStringUtils.randomNumeric(8);
        return id + randomString;
    }

    public static synchronized String getCustomerOrderSn() {
        return CUSTOMER_ORDER_PREFIX + getSn();
    }

    // 销售单
    public static synchronized String getSaleOrderSn() {
        return SALE_ORDER_PREFIX + getSn();
    }

    // 销售单
    public static synchronized String getSaleOrderCollectSn() {
        return SALE_ORDER_COLLECT_PREFIX + getSn();
    }

    // 采购订单
    public static synchronized String getPurchaseOrderSn() {
        return PURCHASE_ORDER_PREFIX + getSn();
    }

    // 出库单
    public static synchronized String getOutboundOrderSn() {
        return OUTBOUND_ORDER_PREFIX + getSn();
    }

    // 加工制作单
    public static synchronized String getProcessOrderSn() {
        return PROCESS_WORK_ORDER_PREFIX + getSn();
    }

    // 出库单库存明细
    public static synchronized String getOutboundOrderInventorySn() {
        return OUTBOUND_ORDER_INVENTORY_PREFIX + getSn();
    }

    // 结款单
    public static synchronized String getSettledPaymentOrderPrefix() {
        return SETTLED_PAYMENT_ORDER_PREFIX + getSn();
    }

    // 退款单
    public static synchronized String getRefundOrderPrefix() {
        return REFUND_ORDER_PREFIX + getSn();
    }

    public static synchronized String getLotSn() {
        return INVENTORY_LOT_PREFIX + getSn();
    }
}
