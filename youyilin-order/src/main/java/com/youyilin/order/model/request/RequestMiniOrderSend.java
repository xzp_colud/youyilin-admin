package com.youyilin.order.model.request;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class RequestMiniOrderSend {

    /**
     * 订单ID
     */
    @NotNull(message = "订单不能为空")
    private Long orderId;
}
