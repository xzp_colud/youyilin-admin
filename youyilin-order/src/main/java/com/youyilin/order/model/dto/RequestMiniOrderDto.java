package com.youyilin.order.model.dto;

import com.youyilin.customer.entity.CustomerAddress;
import com.youyilin.customer.entity.CustomerMini;
import com.youyilin.order.entity.CustomerOrder;
import com.youyilin.order.entity.CustomerOrderItem;
import com.youyilin.order.model.request.RequestMiniOrder;
import com.youyilin.goods.model.request.RequestMiniProduct;
import lombok.Data;

import java.util.List;

@Data
public class RequestMiniOrderDto {

    private RequestMiniOrder request;
    private CustomerMini cm;
    private CustomerAddress address;
    private List<RequestMiniProduct> productList;
    private CustomerOrder order;
    private List<CustomerOrderItem> itemList;
}
