package com.youyilin.order.model.request;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Accessors(chain = true)
public class RequestMiniOrderAddress {

    @NotNull(message = "订单不能为空")
    private Long orderId;
    @NotBlank(message = "收货人不能为空")
    private String name;
    @NotBlank(message = "手机号不能为空")
    private String mobile;
    @NotBlank(message = "地址不能为空")
    private String province;
    @NotBlank(message = "地址不能为空")
    private String city;
    @NotBlank(message = "地址不能为空")
    private String area;
    @NotBlank(message = "地址不能为空")
    private String detail;
}
