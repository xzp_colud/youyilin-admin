package com.youyilin.order.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.FormatAmountUtil;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class CustomerOrderVo {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    private Date createDate;

    private String sn;
    private Integer status;

    private String name;
    private String mobile;
    private String province;
    private String city;
    private String area;
    private String detail;

    private String remark;
    @JsonIgnore
    private Long totalMoney;
    private Date payTime;
    private Date countDownTime;
    private Date sendTime;
    private String expressNo;
    private String expressName;

    private List<CustomerOrderItemVo> itemList;

    public String getTotalMoneyText() {
        return FormatAmountUtil.format(this.totalMoney);
    }
}
