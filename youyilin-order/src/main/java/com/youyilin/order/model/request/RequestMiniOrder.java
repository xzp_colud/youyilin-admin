package com.youyilin.order.model.request;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Accessors(chain = true)
public class RequestMiniOrder {

    @NotNull(message = "收货地址不能为空")
    private Long addressId;
    @NotNull(message = "产品不能为空")
    private List<Long> cartIds;
    private String remark;
}
