package com.youyilin.order.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.FormatAmountUtil;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class CustomerOrderItemVo {

    @JsonIgnore
    @JsonSerialize(using = ToStringSerializer.class)
    private Long orderId;
    private String productName;
    private String skuName;
    private String image;
    @JsonIgnore
    private Long sellAmount;
    private BigDecimal num;
    @JsonIgnore
    private Long totalSell;

    public String getSellAmountText() {
        return FormatAmountUtil.format(this.sellAmount);
    }

    public String getTotalSellText() {
        return FormatAmountUtil.format(this.totalSell);
    }
}
