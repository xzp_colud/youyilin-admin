package com.youyilin.order.model.dto;

import com.youyilin.order.entity.OutboundOrderInventoryLog;
import com.youyilin.order.entity.OutboundOrderItem;
import com.youyilin.order.entity.OutboundOrderItemDetail;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class OutboundOrderItemDto extends OutboundOrderItem {
    private static final long serialVersionUID = -253162762849041716L;

    // 商品具体明细
    private List<OutboundOrderItemDetail> detailList;
    // 出库明细
    private List<OutboundOrderInventoryLog> inventoryLogList;
}
