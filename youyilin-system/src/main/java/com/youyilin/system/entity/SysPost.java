package com.youyilin.system.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 岗位
 */
@Data
@Accessors(chain = true)
@TableName("sys_post")
public class SysPost implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 岗位名称
     */
    @TableField(value = "post_name")
    private String postName;
    /**
     * 岗位编码
     */
    @TableField(value = "post_code")
    private String postCode;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 职责描述
     */
    private String description;
    /**
     * 删除标记
     */
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;

    /**
     * 部门ID
     */
    @TableField(value = "dept_id", updateStrategy = FieldStrategy.IGNORED)
    private Long deptId;

    /**
     * 部门名称(冗余)
     */
    @TableField(value = "dept_name", updateStrategy = FieldStrategy.IGNORED)
    private String deptName;
}
