package com.youyilin.system.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.system.vo.notice.SysNoticeVO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 通知
 */
@Data
@Accessors(chain = true)
@TableName("sys_notice")
public class SysNotice implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField(value = "create_date", fill = FieldFill.INSERT)
    private Date createDate;
    @TableField(fill = FieldFill.INSERT)
    private Long creator;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 标题
     */
    private String title;
    /**
     * 富文本内容
     */
    private String content;
    /**
     * 外部文件 链接
     */
    @TableField(value = "content_url", updateStrategy = FieldStrategy.IGNORED)
    private String contentUrl;
    /**
     * 类型
     */
    private String type;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 发送数量
     */
    @TableField(value = "send_num")
    private Integer sendNum;
    /**
     * 已读数量
     */
    @TableField(value = "read_num")
    private Integer readNum;
    /**
     * 备注
     */
    private String remark;
    /**
     * 删除标记
     */
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;

    public SysNoticeVO convertToVO() {
        return BeanHelper.map(this, SysNoticeVO.class);
    }

    /**
     * 通知类型枚举
     */
    public enum NoticeType {
        /**
         * 公告
         */
        ANNOUNCE,
        /**
         * 通知
         */
        NOTICE,
    }
}
