package com.youyilin.system.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.youyilin.system.enums.LoginStatusEnum;
import com.youyilin.system.enums.LoginTypeEnum;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 登录日志
 */
@Data
@Accessors(chain = true)
@TableName("sys_login_log")
public class SysLoginLog implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 登录用户名
     */
    @TableField(value = "user_name")
    private String userName;
    /**
     * 登录时间
     */
    @TableField(value = "login_date")
    private Date loginDate;
    /**
     * 登录ip
     */
    @TableField(value = "login_ip")
    private String loginIp;
    /**
     * 登录地点
     */
    @TableField(value = "login_location")
    private String loginLocation;
    /**
     * 登录形式
     *
     * @see LoginTypeEnum
     */
    @TableField(value = "login_type")
    private String loginType;
    /**
     * 设备
     */
    private String device;
    /**
     * 浏览器
     */
    private String browser;
    /**
     * 操作系统
     */
    private String os;
    /**
     * 版本
     */
    private String version;
    /**
     * 登录状态
     *
     * @see LoginStatusEnum
     */
    private String status;
    /**
     * 提示消息
     */
    private String msg;
    /**
     * 删除标记
     */
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;
}
