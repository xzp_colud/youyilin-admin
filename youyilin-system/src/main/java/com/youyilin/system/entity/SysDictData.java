package com.youyilin.system.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.Date;

/**
 * 字典键值
 */
@Data
@Accessors(chain = true)
@TableName("sys_dict_data")
public class SysDictData implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 数据编码
     */
    @TableField(value = "dict_code")
    private String dictCode;
    /**
     * 数据排序
     */
    @TableField(value = "dict_sort")
    private String dictSort;
    /**
     * 数据标签
     */
    @TableField(value = "dict_label")
    private String dictLabel;
    /**
     * 数据键值
     */
    @TableField(value = "dict_value")
    private String dictValue;
    /**
     * 字典类型
     */
    @TableField(value = "dict_type")
    private String dictType;
    /**
     * 描述
     */
    private String description;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 扩展样式
     */
    @TableField(value = "css_class")
    private String cssClass;
    /**
     * 回显样式
     */
    @TableField(value = "table_class")
    private String tableClass;
    /**
     * 是否默认
     */
    @TableField(value = "default_flag")
    private Integer defaultFlag;
    /**
     * 删除标记
     */
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;

    public boolean isEqualsDictType(String dictType) {
        if (StringUtils.isBlank(dictType) || StringUtils.isBlank(this.dictType)) {
            return false;
        }
        return this.dictType.equals(dictType);
    }
}
