package com.youyilin.system.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.system.vo.config.SysConfigDataVO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统配置项
 */
@Data
@Accessors(chain = true)
@TableName("sys_config_data")
public class SysConfigData implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 配置ID
     */
    @TableField(value = "config_id")
    private Long configId;
    /**
     * 配置标题
     */
    private String title;
    /**
     * 配置编码
     */
    private String code;
    /**
     * 配置类型
     */
    private Integer type;
    /**
     * 配置值
     */
    private String value;
    /**
     * 配置项
     */
    private String options;
    /**
     * 备注
     */
    private String remark;
    /**
     * 排序
     */
    private Integer sort;

    /**
     * 删除标记
     */
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;

    public SysConfigDataVO convertToVO() {
        return BeanHelper.map(this, SysConfigDataVO.class);
    }
}
