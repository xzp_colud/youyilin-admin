package com.youyilin.system.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统上传文件
 */
@Data
@Accessors(chain = true)
@TableName("sys_file")
public class SysFile implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 文件名称
     */
    private String name;
    /**
     * 文件远程地址
     */
    private String url;
    /**
     * 文件分组
     */
    private String fileGroup;
    /**
     * 文件后缀
     */
    private String suffix;
    /**
     * 文件大小
     */
    private String size;
    /**
     * 分辨率 图片类型
     */
    private String resolution;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 逻辑删除
     */
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;
}
