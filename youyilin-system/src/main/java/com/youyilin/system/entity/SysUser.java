package com.youyilin.system.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.youyilin.common.enums.StatusEnum;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * 用户
 */
@Data
@Accessors(chain = true)
@TableName("sys_user")
public class SysUser implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField(value = "create_date", fill = FieldFill.INSERT)
    private Date createDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long creator;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 用户名称
     */
    @TableField(value = "user_name")
    private String userName;
    /**
     * 密码
     */
    private String password;
    /**
     * 工号
     */
    @TableField(value = "job_number")
    private String jobNumber;
    /**
     * 手机号
     */
    private String phone;
    /**
     * 真实姓名
     */
    @TableField(value = "real_name")
    private String realName;
    /**
     * 头像
     */
    @TableField(value = "avatar_url")
    private String avatarUrl;
    /**
     * 性别
     */
    private String sex;
    /**
     * 年龄
     */
    private String age;
    /**
     * 用户描述
     */
    private String description;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 拼音
     */
    @TableField("pin_yin")
    private String pinYin;
    /**
     * 居住地
     */
    private String province;
    private String city;
    private String area;
    private String detail;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 是否共享登录
     */
    @TableField(value = "share_login")
    private Integer shareLogin;
    /**
     * 最后登录IP
     */
    @TableField(value = "login_ip")
    private String loginIp;
    /**
     * 最后登录地
     */
    @TableField(value = "login_location")
    private String loginLocation;
    /**
     * 最后登录时间
     */
    @TableField(value = "login_date")
    private Date loginDate;
    /**
     * 删除标记
     */
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;

    public boolean isAdmin() {
        return isAdmin(this.getId());
    }

    public static boolean isAdmin(Long id) {
        return id != null && Objects.equals(id, getAdminId());
    }

    public static Long getAdminId() {
        return 1L;
    }

    public boolean isStatusNormal() {
        return StatusEnum.isNormal(this.status);
    }

    public static List<Long> getNoticeAdmin() {
        List<Long> list = new ArrayList<>();
        list.add(getAdminId());
        return list;
    }
}
