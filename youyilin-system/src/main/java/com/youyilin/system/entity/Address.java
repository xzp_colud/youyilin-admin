package com.youyilin.system.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 国家地理
 */
@Data
@Accessors(chain = true)
@TableName("address")
public class Address implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /**
     * 上级地址
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long pid;
    /**
     * 地理名称
     */
    @NotNull(message = "地理名称不能为空")
    @Size(max = 100, message = "地理名称长度不能超过100字符")
    private String name;
    /**
     * 地址编码
     */
    @NotNull(message = "地理编码不能为空")
    @Size(max = 50, message = "地理编码长度不能超过50字符")
    private String code;
    /**
     * 逻辑标记
     */
    @JsonIgnore
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;
}
