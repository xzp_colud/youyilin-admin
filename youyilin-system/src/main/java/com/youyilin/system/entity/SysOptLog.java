package com.youyilin.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 操作日志
 */
@Data
@Accessors(chain = true)
@TableName("sys_opt_log")
public class SysOptLog implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Integer id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;

    /**
     * 用户名称
     */
    @TableField(value = "user_name")
    private String userName;
    /**
     * 请求地址
     */
    private String url;
    /**
     * 请求类型
     */
    private String method;
    /**
     * 请求参数
     */
    private String params;
    /**
     * 返回结果
     */
    @TableField(value = "result_data")
    private String resultData;
    /**
     * 返回结果
     */
    @TableField(value = "result_code")
    private String resultCode;
    /**
     * 调用模块
     */
    private String title;
    /**
     * 业务类型
     */
    @TableField(value = "business_type")
    private String businessType;
    /**
     * 调用耗时
     */
    private String time;
    /**
     * 删除标记
     */
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;

    /**
     * 登录日志ID
     */
    @TableField(value = "login_log_id")
    private Long loginLogId;
}
