package com.youyilin.system.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.system.vo.config.SysConfigVO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统配置
 */
@Data
@Accessors(chain = true)
@TableName("sys_config")
public class SysConfig implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 配置名称
     */
    private String name;
    /**
     * 配置类型
     */
    private Integer type;
    /**
     * 排序
     */
    private Integer sort;

    public SysConfigVO convertToVO() {
        return BeanHelper.map(this, SysConfigVO.class);
    }
}
