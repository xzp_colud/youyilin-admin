package com.youyilin.system.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 通知读取
 */
@Data
@Accessors(chain = true)
@TableName("sys_notice_user_read")
public class SysNoticeUserRead implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField(value = "create_date", fill = FieldFill.INSERT)
    private Date createDate;
    @TableField(fill = FieldFill.INSERT)
    private Long creator;
    @TableField(value = "read_date")
    private Date readDate;
    private Long reader;
    @TableField(value = "cancel_date")
    private Date cancelDate;
    private Long canceler;

    /**
     * 通知ID
     */
    @TableField(value = "notice_id")
    private Long noticeId;
    /**
     * 用户ID
     */
    @TableField(value = "user_id")
    private Long userId;
    /**
     * 状态
     */
    private String status;
    /**
     * 删除标记
     */
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;

    /**
     * 状态枚举
     */
    public enum Status {
        /**
         * 未读
         */
        NO_READ,
        /**
         * 已读
         */
        READ,
        /**
         * 撤销
         */
        CANCEL,
    }

    public boolean isEqualsUserIdForLogin(Long userId) {
        return isEqualsUserIdForLogin(this.userId, userId);
    }

    private static boolean isEqualsUserIdForLogin(Long sourceUserId, Long loginUserId) {
        if (sourceUserId == null || loginUserId == null) {
            return false;
        }
        return sourceUserId.equals(loginUserId);
    }
}
