package com.youyilin.system.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.youyilin.system.enums.MenuTypeEnum;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 菜单权限
 */
@Data
@Accessors(chain = true)
@TableName("sys_menu")
public class SysMenu implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 菜单名称
     */
    @TableField(value = "menu_name")
    private String menuName;
    /**
     * 菜单编码
     */
    @TableField(value = "menu_code")
    private String menuCode;
    /**
     * 路由路径 对应router : path
     */
    private String path;
    /**
     * 路由路径 对应router : name
     */
    private String name;
    /**
     * 组件路径
     */
    private String component;
    /**
     * 是否为外链
     */
    private Integer link;
    /**
     * 图标
     */
    private String icon;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 是否显示
     */
    private Integer visible;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 描述
     */
    private String description;
    /**
     * 权限类型
     *
     * @see MenuTypeEnum
     */
    private Integer type;
    /**
     * 权限标识
     */
    private String perms;
    /**
     * 删除标记
     */
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;

    /**
     * 上级菜单 若是目录则为0
     */
    @TableField(value = "pid_id")
    private Long pidId;
    /**
     * 上级菜单
     */
    @TableField(value = "pid_name")
    private String pidName;
}
