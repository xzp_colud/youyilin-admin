package com.youyilin.system.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 部门
 */
@Data
@Accessors(chain = true)
@TableName("sys_dept")
public class SysDept implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 部门名称
     */
    @TableField(value = "dept_name")
    private String deptName;
    /**
     * 部门编码
     */
    @TableField(value = "dept_code")
    private String deptCode;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 描述
     */
    private String description;
    /**
     * 部门领导
     */
    private String leader;
    /**
     * 联系电话
     */
    private String phone;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 删除标记
     */
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;

    /**
     * 上级部门
     */
    @TableField(value = "pid_id", updateStrategy = FieldStrategy.IGNORED)
    private Long pidId;
    /**
     * 上级部门
     */
    @TableField(value = "pid_name", updateStrategy = FieldStrategy.IGNORED)
    private String pidName;
}
