package com.youyilin.system.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 异常日志
 */
@Data
@Accessors(chain = true)
@TableName("sys_error_log")
public class SysErrorLog implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 请求方法
     */
    private String method;
    /**
     * 错误原因
     */
    private String msg;
    /**
     * 错误类型
     */
    private String type;
}
