package com.youyilin.system.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户所属岗位
 */
@Data
@Accessors(chain = true)
@TableName("sys_user_post")
public class SysUserPost implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 用户ID
     */
    @TableField(value = "user_id")
    private Long userId;
    /**
     * 岗位ID
     */
    @TableField(value = "post_id")
    private Long postId;
}
