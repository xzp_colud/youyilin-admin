package com.youyilin.system.mapper;

import com.youyilin.common.bean.Page;
import com.youyilin.system.dto.ip.IpQueryDTO;
import com.youyilin.system.entity.SysIp;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.system.vo.ip.IpPageVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * IP Mapper
 */
@Mapper
public interface SysIpMapper extends BaseMapper<SysIp> {

    Integer getTotal(Page<IpQueryDTO> page);

    List<IpPageVO> getPage(Page<IpQueryDTO> page);
}
