package com.youyilin.system.mapper;

import com.youyilin.system.dto.dict.DictTypeQueryDTO;
import com.youyilin.system.entity.SysDictType;
import com.youyilin.common.bean.Page;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.system.vo.dict.DictTypePageVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 字典 Mapper
 */
@Mapper
public interface SysDictTypeMapper extends BaseMapper<SysDictType> {

    Integer getTotal(Page<DictTypeQueryDTO> page);

    List<DictTypePageVO> getPage(Page<DictTypeQueryDTO> page);
}
