package com.youyilin.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.common.bean.Page;
import com.youyilin.system.dto.log.ErrorLogQueryDTO;
import com.youyilin.system.entity.SysErrorLog;
import com.youyilin.system.vo.log.ErrorLogPageVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 异常日志 Mapper
 */
@Mapper
public interface SysErrorLogMapper extends BaseMapper<SysErrorLog> {

    Integer getTotal(Page<ErrorLogQueryDTO> page);

    List<ErrorLogPageVO> getPage(Page<ErrorLogQueryDTO> page);
}
