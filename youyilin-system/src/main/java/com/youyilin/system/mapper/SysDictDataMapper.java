package com.youyilin.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.common.bean.Page;
import com.youyilin.system.dto.dict.DictDataQueryDTO;
import com.youyilin.system.entity.SysDictData;
import com.youyilin.system.vo.dict.DictDataPageVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 字典键值 Mapper
 */
@Mapper
public interface SysDictDataMapper extends BaseMapper<SysDictData> {

    Integer getTotal(Page<DictDataQueryDTO> page);

    List<DictDataPageVO> getPage(Page<DictDataQueryDTO> page);
}
