package com.youyilin.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.common.bean.Page;
import com.youyilin.system.dto.config.SysConfigDataPageQueryVO;
import com.youyilin.system.entity.SysConfigData;
import com.youyilin.system.vo.config.SysConfigDataVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 系统配置项 Mapper
 */
@Mapper
public interface SysConfigDataMapper extends BaseMapper<SysConfigData> {

    Integer getTotal(Page<SysConfigDataPageQueryVO> page);

    List<SysConfigDataVO> getPage(Page<SysConfigDataPageQueryVO> page);
}
