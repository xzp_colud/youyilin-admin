package com.youyilin.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.system.entity.SysUserPost;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户所属岗位 Mapper
 */
@Mapper
public interface SysUserPostMapper extends BaseMapper<SysUserPost> {
}
