package com.youyilin.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.common.bean.Page;
import com.youyilin.system.entity.SysNoticeUserRead;
import com.youyilin.system.vo.notice.SysNoticeUserReadVO;
import com.youyilin.system.vo.notice.SysNoticeUserReadSendVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 通知读取 Mapper
 */
@Mapper
public interface SysNoticeUserReadMapper extends BaseMapper<SysNoticeUserRead> {

    Integer getTotal(Page<SysNoticeUserReadVO> page);

    List<SysNoticeUserReadVO> getPage(Page<SysNoticeUserReadVO> page);

    List<SysNoticeUserReadSendVO> getSendPage(SysNoticeUserReadSendVO s);
}
