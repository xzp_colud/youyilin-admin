package com.youyilin.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.common.bean.Page;
import com.youyilin.system.entity.SysConfig;
import com.youyilin.system.vo.config.SysConfigVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 系统配置 Mapper
 */
@Mapper
public interface SysConfigMapper extends BaseMapper<SysConfig> {

    Integer getTotal(Page<SysConfigVO> page);

    List<SysConfigVO> getPage(Page<SysConfigVO> page);
}
