package com.youyilin.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.common.bean.Page;
import com.youyilin.system.dto.address.AddressQueryDTO;
import com.youyilin.system.entity.Address;
import com.youyilin.system.vo.address.AddressVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface AddressMapper extends BaseMapper<Address> {

    Integer getTotal(Page<AddressQueryDTO> page);

    List<AddressVO> getPage(Page<AddressQueryDTO> page);
}