package com.youyilin.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.common.bean.Page;
import com.youyilin.system.entity.SysNotice;
import com.youyilin.system.dto.notice.SysNoticeQueryDTO;
import com.youyilin.system.vo.notice.SysNoticeVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 通知 Mapper
 */
@Mapper
public interface SysNoticeMapper extends BaseMapper<SysNotice> {

    Integer getTotal(Page<SysNoticeQueryDTO> page);

    List<SysNoticeVO> getPage(Page<SysNoticeQueryDTO> page);
}
