package com.youyilin.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.system.entity.SysMenu;
import com.youyilin.system.vo.menu.MenuVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 菜单权限 Mapper
 */
@Mapper
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    List<MenuVO> getAdminRouters();

    List<MenuVO> getRouters(Long userId);

    List<String> getAdminPerms();

    List<String> getPerms(Long userId);

    List<MenuVO> getSearch(Long userId);
}
