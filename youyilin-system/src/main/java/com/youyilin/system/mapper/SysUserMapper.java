package com.youyilin.system.mapper;

import com.youyilin.system.dto.user.UserQueryDTO;
import com.youyilin.system.entity.SysUser;
import com.youyilin.common.bean.Page;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.system.vo.user.UserPageVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 用户 Mapper
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {

    Integer getTotal(Page<UserQueryDTO> page);

    List<UserPageVO> getPage(Page<UserQueryDTO> page);
}
