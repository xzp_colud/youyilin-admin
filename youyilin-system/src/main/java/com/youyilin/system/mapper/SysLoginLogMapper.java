package com.youyilin.system.mapper;

import com.youyilin.system.dto.log.LoginLogQueryDTO;
import com.youyilin.system.entity.SysLoginLog;
import com.youyilin.common.bean.Page;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.system.vo.log.LoginLogPageVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 登录日志 Mapper
 */
@Mapper
public interface SysLoginLogMapper extends BaseMapper<SysLoginLog> {

    Integer getTotal(Page<LoginLogQueryDTO> page);

    List<LoginLogPageVO> getPage(Page<LoginLogQueryDTO> page);
}
