package com.youyilin.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.common.bean.Page;
import com.youyilin.system.dto.post.PostQueryDTO;
import com.youyilin.system.entity.SysPost;
import com.youyilin.system.vo.post.PostPageVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 岗位 Mapper
 */
@Mapper
public interface SysPostMapper extends BaseMapper<SysPost> {

    Integer getTotal(Page<PostQueryDTO> page);

    List<PostPageVO> getPage(Page<PostQueryDTO> page);
}
