package com.youyilin.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.common.bean.Page;
import com.youyilin.system.dto.role.RoleQueryDTO;
import com.youyilin.system.entity.SysRole;
import com.youyilin.system.vo.role.RolePageVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 角色 Mapper
 */
@Mapper
public interface SysRoleMapper extends BaseMapper<SysRole> {

    Integer getTotal(Page<RoleQueryDTO> page);

    List<RolePageVO> getPage(Page<RoleQueryDTO> page);
}
