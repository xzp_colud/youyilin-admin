package com.youyilin.system.mapper;

import com.youyilin.system.dto.dept.DeptQueryDTO;
import com.youyilin.system.entity.SysDept;
import com.youyilin.common.bean.Page;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.system.vo.dept.DeptPageVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 部门 Mapper
 */
@Mapper
public interface SysDeptMapper extends BaseMapper<SysDept> {

    Integer getTotal(Page<DeptQueryDTO> page);

    List<DeptPageVO> getPage(Page<DeptQueryDTO> page);
}
