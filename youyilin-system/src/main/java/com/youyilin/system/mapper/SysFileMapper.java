package com.youyilin.system.mapper;

import com.youyilin.system.dto.file.FileQueryDTO;
import com.youyilin.system.entity.SysFile;
import com.youyilin.common.bean.Page;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.system.vo.file.FilePageVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 系统上传文件 Mapper
 */
@Mapper
public interface SysFileMapper extends BaseMapper<SysFile> {

    Integer getTotal(Page<FileQueryDTO> page);

    List<FilePageVO> getPage(Page<FileQueryDTO> page);
}
