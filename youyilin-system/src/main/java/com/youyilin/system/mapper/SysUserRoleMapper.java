package com.youyilin.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.system.entity.SysUserRole;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户所属角色 Mapper
 */
@Mapper
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}
