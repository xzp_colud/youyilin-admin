package com.youyilin.system.mapper;

import com.youyilin.system.dto.log.OptLogQueryDTO;
import com.youyilin.system.entity.SysOptLog;
import com.youyilin.common.bean.Page;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.system.vo.log.OptLogPageVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 操作日志 Mapper
 */
@Mapper
public interface SysOptLogMapper extends BaseMapper<SysOptLog> {

    Integer getTotal(Page<OptLogQueryDTO> page);

    List<OptLogPageVO> getPage(Page<OptLogQueryDTO> page);
}
