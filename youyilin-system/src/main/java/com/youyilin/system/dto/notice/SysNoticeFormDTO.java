package com.youyilin.system.dto.notice;

import com.youyilin.common.utils.BeanHelper;
import com.youyilin.system.entity.SysNotice;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 * 通知表单
 */
@Data
public class SysNoticeFormDTO {

    private Long id;
    /**
     * 标题
     */
    @NotBlank(message = "标题不能为空")
    private String title;
    /**
     * 内容
     */
    @NotBlank(message = "内容不能为空")
    private String content;
    /**
     * 内容 链接
     */
    private String contentUrl;
    /**
     * 类型
     */
    @NotBlank(message = "类型不能为空")
    private String type;
    /**
     * 状态
     */
    @NotNull(message = "状态不能为空")
    private Integer status;
    /**
     * 备注
     */
    private String remark;

    public SysNotice convertToEntity() {
        return BeanHelper.map(this, SysNotice.class);
    }
}
