package com.youyilin.system.dto.notice;

import javax.validation.constraints.NotNull;
import lombok.Data;

import java.util.List;

@Data
public class SysNoticeSendFormDTO {

    @NotNull(message = "通知不能为空")
    private Long noticeId;
    @NotNull(message = "用户不能为空")
    private List<Long> userIds;
}
