package com.youyilin.system.dto.post;

import lombok.Data;

/**
 * 岗位列表查询参数
 */
@Data
public class PostQueryDTO {
    /**
     * 状态
     */
    private Integer status;
    /**
     * 岗位名称
     */
    private String postName;
    /**
     * 岗位编码
     */
    private String postCode;
    /**
     * 所属部门
     */
    private String deptName;
}