package com.youyilin.system.dto.dict;

import lombok.Data;

/**
 * 字典键值查询参数
 */
@Data
public class DictDataQueryDTO {
    private Integer status;
    private String dictCode;
    private String dictLabel;
    private String dictValue;
    private String dictType;
}
