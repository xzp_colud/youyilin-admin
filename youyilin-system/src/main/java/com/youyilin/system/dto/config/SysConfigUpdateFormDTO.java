package com.youyilin.system.dto.config;

import javax.validation.constraints.NotNull;
import lombok.Data;

import java.util.List;

@Data
public class SysConfigUpdateFormDTO {
    @NotNull(message = "配置不能为空")
    private Long configId;
    @NotNull(message = "配置不能为空")
    private List<SysConfigDataUpdateFormDTO> configData;
}
