package com.youyilin.system.dto.dept;

import com.youyilin.common.utils.BeanHelper;
import com.youyilin.system.entity.SysDept;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;

/**
 * 部门表单
 */
@Data
public class DeptFormDTO {
    private Long id;
    /**
     * 部门名称
     */
    @NotBlank(message = "部门名称不能为空")
    @Size(max = 64, message = "部门名称长度不能超过64字符")
    private String deptName;
    /**
     * 部门编码
     */
    @NotBlank(message = "部门编码不能为空")
    @Size(max = 32, message = "部门名称长度不能超过32字符")
    private String deptCode;
    /**
     * 状态
     */
    @NotNull(message = "状态不能为空")
    private Integer status;
    /**
     * 部门描述
     */
    @NotBlank(message = "部门描述不能为空")
    @Size(max = 128, message = "部门描述长度不能超过128字符")
    private String description;
    /**
     * 部门领导
     */
    private String leader;
    /**
     * 部门电话
     */
    private String phone;
    /**
     * 部门邮箱
     */
    private String email;
    /**
     * 上级部门
     */
    private Long pidId;

    public SysDept convertToEntity() {
        return BeanHelper.map(this, SysDept.class);
    }
}