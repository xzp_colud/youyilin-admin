package com.youyilin.system.dto.ip;

import com.youyilin.common.utils.BeanHelper;
import com.youyilin.system.entity.SysIp;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;

/**
 * IP表单
 */
@Data
public class IpFormDTO {
    private Long id;
    /**
     * ip
     */
    @NotBlank(message = "IP不能为空")
    @Size(max = 32, message = "IP长度不能超过32字符")
    private String ip;
    /**
     * 类型
     */
    @NotNull(message = "IP类型不能为空")
    private Integer type;
    /**
     * 状态
     */
    @NotNull(message = "状态不能为空")
    private Integer status;

    public SysIp convertToEntity() {
        return BeanHelper.map(this, SysIp.class);
    }
}
