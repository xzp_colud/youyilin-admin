package com.youyilin.system.dto.address;

import lombok.Data;

/**
 * 国家地理列表查询参数
 */
@Data
public class AddressQueryDTO{
    /**
     * 地理名称
     */
    private String name;
    /**
     * 地址编码
     */
    private String code;
    /**
     * 上级地理
     */
    private String pidName;
}
