package com.youyilin.system.dto.user;

import com.youyilin.common.utils.BeanHelper;
import com.youyilin.system.entity.SysUser;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;

import java.util.List;

/**
 * 用户表单
 */
@Data
public class UserFormDTO {
    private Long id;
    /**
     * 用户名称
     */
    @NotBlank(message = "用户名称不能为空")
    private String userName;
    @NotBlank(message = "工号不能为空")
    @Size(max = 24, message = "工号最长24位")
    private String jobNumber;
    @NotBlank(message = "手机号不能为空")
    @Size(min = 11, max = 13, message = "手机号长度不能超过13字符")
    private String phone;
    @NotBlank(message = "真实姓名不能为空")
    @Size(max = 64, message = "真实姓名长度不能超过64字符")
    private String realName;
    private String avatarUrl;
    private String email;
    private String sex;
    private String age;
    @NotNull(message = "共享状态不能为空")
    private Integer shareLogin;
    @NotNull(message = "状态不能为空")
    private Integer status;
    private String description;

    private List<Long> roleIds;
    private List<Long> postList;

    public SysUser convertToEntity() {
        return BeanHelper.map(this, SysUser.class);
    }
}