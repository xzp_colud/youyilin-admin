package com.youyilin.system.dto.log;

import lombok.Data;

/**
 * 操作日志列表查询参数
 */
@Data
public class OptLogQueryDTO {
    /**
     * 登陆者ID
     */
    private Long loginLogId;
    /**
     * 用户名称
     */
    private String userName;
}
