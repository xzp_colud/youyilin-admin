package com.youyilin.system.dto.file;

import lombok.Data;

/**
 * 文件列表查询参数
 */
@Data
public class FileQueryDTO {
    /**
     * 文件名称
     */
    private String name;
}