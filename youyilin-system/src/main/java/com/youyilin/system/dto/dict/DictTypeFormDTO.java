package com.youyilin.system.dto.dict;

import com.youyilin.common.utils.BeanHelper;
import com.youyilin.system.entity.SysDictType;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;

/**
 * 字典类型表单
 */
@Data
public class DictTypeFormDTO {
    private Long id;
    /**
     * 字典名称
     */
    @NotBlank(message = "字典名称不能为空")
    @Size(max = 32, message = "字典名称长度不能超过32字符")
    private String dictName;
    /**
     * 字典类型
     */
    @NotBlank(message = "字典类型不能为空")
    @Size(max = 64, message = "字典名称长度不能超过64字符")
    private String dictType;
    /**
     * 状态
     */
    @NotNull(message = "状态不能为空")
    private Integer status;
    /**
     * 字典描述
     */
    @NotBlank(message = "描述不能为空")
    @Size(max = 128, message = "字典名称长度不能超过128字符")
    private String description;

    public SysDictType convertToEntity() {
        return BeanHelper.map(this, SysDictType.class);
    }
}
