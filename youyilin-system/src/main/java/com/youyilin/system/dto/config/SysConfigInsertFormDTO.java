package com.youyilin.system.dto.config;

import com.youyilin.common.utils.BeanHelper;
import com.youyilin.system.entity.SysConfig;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class SysConfigInsertFormDTO {
    private Long id;
    @NotBlank(message = "配置名称不能为空")
    private String name;
    @NotNull(message = "配置类型不能为空")
    private Integer type;
    @NotNull(message = "排序不能为空")
    private Integer sort;

    public SysConfig convertToEntity() {
        return BeanHelper.map(this, SysConfig.class);
    }
}
