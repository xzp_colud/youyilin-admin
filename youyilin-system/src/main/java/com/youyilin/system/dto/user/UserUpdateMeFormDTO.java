package com.youyilin.system.dto.user;

import com.youyilin.common.utils.BeanHelper;
import com.youyilin.system.entity.SysUser;
import lombok.Data;

/**
 * 用户个人中心修改信息表单
 */
@Data
public class UserUpdateMeFormDTO {
    private String userName;
    private String realName;
    private String avatarUrl;
    private String email;
    private String sex;
    private String age;
    private String province;
    private String city;
    private String area;
    private String detail;
    private String description;

    public SysUser convertToEntity() {
        return BeanHelper.map(this, SysUser.class);
    }
}