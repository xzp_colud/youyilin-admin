package com.youyilin.system.dto.ip;

import lombok.Data;

/**
 * IP列表查询参数
 */
@Data
public class IpQueryDTO {
    /**
     * 状态
     */
    private Integer status;
    /**
     * 类型
     */
    private Integer type;
    /**
     * IP
     */
    private String ip;
}