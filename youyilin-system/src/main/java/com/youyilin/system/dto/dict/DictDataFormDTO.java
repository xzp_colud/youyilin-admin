package com.youyilin.system.dto.dict;

import com.youyilin.common.utils.BeanHelper;
import com.youyilin.system.entity.SysDictData;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;

/**
 * 字典键值表单
 */
@Data
public class DictDataFormDTO {
    private Long id;
    /**
     * 键值编码
     */
    @NotBlank(message = "编码不能为空")
    @Size(max = 32, message = "编码长度不能超过32字符")
    private String dictCode;
    /**
     * 键值排序
     */
    private String dictSort;
    /**
     * 标签
     */
    @NotBlank(message = "标签不能为空")
    @Size(max = 32, message = "标签长度不能超过32字符")
    private String dictLabel;
    /**
     * 键值值
     */
    @NotBlank(message = "键值不能为空")
    @Size(max = 32, message = "键值长度不能超过32字符")
    private String dictValue;
    /**
     * 字典类型
     */
    @NotBlank(message = "字典类型不能为空")
    private String dictType;
    /**
     * 键值描述
     */
    @Size(max = 128, message = "描述长度不能超过128字符")
    private String description;
    /**
     * 状态
     */
    @NotNull(message = "状态不能为空")
    private Integer status;
    /**
     * 扩展css
     */
    private String cssClass;
    /**
     * table回显样式
     */
    private String tableClass;
    /**
     * 是否默认
     */
    private Integer defaultFlag;

    public SysDictData convertToEntity() {
        return BeanHelper.map(this, SysDictData.class);
    }
}
