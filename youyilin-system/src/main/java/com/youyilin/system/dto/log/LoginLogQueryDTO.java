package com.youyilin.system.dto.log;

import lombok.Data;

/**
 * 登录日志列表查询参数
 */
@Data
public class LoginLogQueryDTO {
    /**
     * 登录结果
     */
    private String status;
    /**
     * 用户名称
     */
    private String userName;
    /**
     * 登录地点
     */
    private String loginLocation;
}
