package com.youyilin.system.dto.config;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class SysConfigDataUpdateFormDTO {
    @NotNull(message = "配置项不能为空")
    private Long id;
    @NotBlank(message = "配置项不能为空")
    private String value;
}
