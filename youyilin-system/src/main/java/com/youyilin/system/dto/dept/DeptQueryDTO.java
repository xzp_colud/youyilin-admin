package com.youyilin.system.dto.dept;

import lombok.Data;

/**
 * 部门列表查询参数
 */
@Data
public class DeptQueryDTO {
    /**
     * 状态
     */
    private Integer status;
    /**
     * 部门名称
     */
    private String deptName;
    /**
     * 名称编码
     */
    private String deptCode;
    /**
     * 上级部门
     */
    private String pidName;
    /**
     * 领导
     */
    private String leader;
    /**
     * 联系电话
     */
    private String phone;
}