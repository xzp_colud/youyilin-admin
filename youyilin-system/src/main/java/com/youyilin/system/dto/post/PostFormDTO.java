package com.youyilin.system.dto.post;

import com.youyilin.common.utils.BeanHelper;
import com.youyilin.system.entity.SysPost;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;

/**
 * 岗位表单
 */
@Data
public class PostFormDTO {
    private Long id;
    /**
     * 岗位名称
     */
    @NotBlank(message = "岗位名称不能为空")
    @Size(max = 64, message = "岗位名称长度不能超过64字符")
    private String postName;
    /**
     * 岗位编码
     */
    @NotBlank(message = "岗位编码不能为空")
    @Size(max = 64, message = "岗位编码长度不能超过64字符")
    private String postCode;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 岗位职责
     */
    @NotBlank(message = "岗位职责描述不能为空")
    @Size(max = 255, message = "职责描述长度不能超过255字符")
    private String description;
    /**
     * 所属部门
     */
    @NotNull(message = "所属部门不能为空")
    private Long deptId;

    public SysPost convertToEntity() {
        return BeanHelper.map(this, SysPost.class);
    }
}