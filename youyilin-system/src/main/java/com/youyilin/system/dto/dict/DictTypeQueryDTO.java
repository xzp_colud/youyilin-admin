package com.youyilin.system.dto.dict;

import lombok.Data;

/**
 * 字典类型查询参数
 */
@Data
public class DictTypeQueryDTO {
    private String dictName;
    private String dictType;
    private Integer status;
}
