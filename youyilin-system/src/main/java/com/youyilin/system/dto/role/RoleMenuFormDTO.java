package com.youyilin.system.dto.role;

import javax.validation.constraints.NotNull;
import lombok.Data;

import java.util.List;

/**
 * 角色授权表单
 */
@Data
public class RoleMenuFormDTO {
    /**
     * 角色ID
     */
    @NotNull(message = "角色不能为空")
    private Long roleId;
    /**
     * 权限ID
     */
    private List<Long> menuIds;
}
