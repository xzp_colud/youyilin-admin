package com.youyilin.system.dto.notice;

import lombok.Data;

@Data
public class SysNoticeQueryDTO {
    private String title;
    private String type;
    private Integer status;
}
