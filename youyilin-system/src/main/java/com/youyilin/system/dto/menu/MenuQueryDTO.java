package com.youyilin.system.dto.menu;

import lombok.Data;

/**
 * 权限列表查询参数
 */
@Data
public class MenuQueryDTO {
    /**
     * 状态
     */
    private Integer status;
    /**
     * 类型
     */
    private Integer type;
    /**
     * 权限名称
     */
    private String menuName;
    /**
     * 长度编码
     */
    private String menuCode;
    /**
     * 上级权限
     */
    private String pidName;
    /**
     * 路由路径
     */
    private String path;
    /**
     * 路由名称
     */
    private String name;
    /**
     * 权限名称
     */
    private String perms;
}
