package com.youyilin.system.dto.address;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;

/**
 * 国家地理表单
 */
@Data
public class AddressFormDTO {

    private Long id;
    /**
     * 上级地址
     */
    private Long pid;
    /**
     * 地理名称
     */
    @NotNull(message = "地理名称不能为空")
    @Size(max = 100, message = "地理名称长度不能超过100字符")
    private String name;
    /**
     * 地址编码
     */
    @NotNull(message = "地理编码不能为空")
    @Size(max = 50, message = "地理编码长度不能超过50字符")
    private String code;
}
