package com.youyilin.system.dto.role;

import lombok.Data;

/**
 * 角色列表查询参数
 */
@Data
public class RoleQueryDTO {
    /**
     * 状态
     */
    private Integer status;
    /**
     * 角色名称
     */
    private String roleName;
    /**
     * 角色权限字符串
     */
    private String roleKey;
}