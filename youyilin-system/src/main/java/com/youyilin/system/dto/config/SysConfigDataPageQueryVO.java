package com.youyilin.system.dto.config;

import lombok.Data;

@Data
public class SysConfigDataPageQueryVO {
    private String title;
    private String code;
    private Long configId;
}
