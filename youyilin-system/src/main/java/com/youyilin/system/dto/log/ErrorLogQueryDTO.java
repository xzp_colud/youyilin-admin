package com.youyilin.system.dto.log;

import lombok.Data;

/**
 * 异常日志列表查询参数
 */
@Data
public class ErrorLogQueryDTO {
    /**
     * 类型
     */
    private String type;
}
