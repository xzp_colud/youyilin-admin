package com.youyilin.system.dto.menu;

import com.youyilin.common.utils.BeanHelper;
import com.youyilin.system.entity.SysMenu;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;

/**
 * 权限表单
 */
@Data
public class MenuFormDTO {

    private Long id;
    /**
     * 菜单名称
     */
    @NotBlank(message = "菜单名称不能为空")
    @Size(max = 64, message = "菜单名称度不能超过64字符")
    private String menuName;
    /**
     * 菜单编码
     */
    @NotBlank(message = "菜单编码不能为空")
    @Size(max = 64, message = "菜单编码长度不能超过64字符")
    private String menuCode;
    /**
     * 路由路径 对应router : path
     */
    private String path;
    /**
     * 路由路径 对应router : name
     */
    private String name;
    /**
     * 组件路径
     */
    private String component;
    /**
     * 是否为外链
     */
    private Integer link;
    /**
     * 图标
     */
    private String icon;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 是否显示
     */
    private Integer visible;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 描述
     */
    private String description;
    /**
     * 权限类型
     */
    @NotNull(message = "类型不能为空")
    private Integer type;
    /**
     * 权限标识
     */
    private String perms;
    /**
     * 上级菜单 若是目录则为0
     */
    private Long pidId;

    public SysMenu convertToEntity() {
        return BeanHelper.map(this, SysMenu.class);
    }
}
