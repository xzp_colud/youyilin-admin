package com.youyilin.system.dto.user;

import javax.validation.constraints.NotBlank;
import lombok.Data;

/**
 * 用户修改密码表单
 */
@Data
public class UserResetPasswordDto {

    // 用户名称
    @NotBlank(message = "参数异常")
    private String userName;
    // 旧密码
    @NotBlank(message = "原密码不能为空")
    private String password;
    // 新密码
    @NotBlank(message = "新密码不能为空")
    private String newPassword;
    // 新密码确认
    @NotBlank(message = "新密码不能为空")
    private String reNewPassword;
}
