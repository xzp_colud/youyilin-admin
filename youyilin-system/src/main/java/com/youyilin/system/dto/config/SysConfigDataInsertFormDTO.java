package com.youyilin.system.dto.config;

import com.youyilin.common.utils.BeanHelper;
import com.youyilin.system.entity.SysConfigData;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class SysConfigDataInsertFormDTO {

    private Long id;
    @NotNull(message = "配置不能为空")
    private Long configId;
    @NotBlank(message = "配置标题不能为空")
    private String title;
    @NotBlank(message = "配置编码不能为空")
    private String code;
    @NotNull(message = "配置类型不能为空")
    private Integer type;
    @NotBlank(message = "配置值不能为空")
    private String value;
    private String options;
    private String remark;
    @NotNull(message = "排序不能为空")
    private Integer sort;

    public SysConfigData convertToEntity() {
        return BeanHelper.map(this, SysConfigData.class);
    }
}
