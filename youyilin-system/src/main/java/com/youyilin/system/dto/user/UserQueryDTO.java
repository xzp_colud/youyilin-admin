package com.youyilin.system.dto.user;

import lombok.Data;

/**
 * 用户列表查询参数
 */
@Data
public class UserQueryDTO {
    private Integer status;
    private String userName;
    private String jobNumber;
    private String phone;
    private String realName;
    private String pinYin;
}