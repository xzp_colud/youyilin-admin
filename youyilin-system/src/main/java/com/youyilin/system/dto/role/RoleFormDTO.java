package com.youyilin.system.dto.role;

import com.youyilin.common.utils.BeanHelper;
import com.youyilin.system.entity.SysRole;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;

/**
 * 角色表单
 */
@Data
public class RoleFormDTO {
    private Long id;
    /**
     * 角色名称
     */
    @NotBlank(message = "角色名称不能为空")
    @Size(max = 64, message = "角色名称长度不能超过64字符")
    private String roleName;
    /**
     * 角色权限字符串
     */
    @NotBlank(message = "权限字符串不能为空")
    @Size(max = 32, message = "权限字符串长度不能超过32字符")
    private String roleKey;
    /**
     * 状态
     */
    @NotNull(message = "状态不能为空")
    private Integer status;
    /**
     * 描述
     */
    @NotBlank(message = "描述不能为空")
    @Size(max = 255, message = "描述长度不能超过255字符")
    private String description;
    /**
     * 排序
     */
    private Integer sort;

    public SysRole convertToEntity() {
        return BeanHelper.map(this, SysRole.class);
    }
}