package com.youyilin.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.system.entity.SysRole;

import java.util.List;

/**
 * 角色 Service
 */
public interface SysRoleService extends IService<SysRole> {

    /**
     * 查询所有角色
     */
    List<SysRole> listAll();

    /**
     * 查询验证角色
     *
     * @param ids 角色ID
     * @return ArrayList
     */
    List<Long> listRoleIdsByIds(List<Long> ids);

    /**
     * 根据角色名称查询
     *
     * @param roleName 角色名称
     * @return SysRole
     */
    SysRole getByRoleName(String roleName);

    /**
     * 根据角色权限字符串查询
     *
     * @param roleKey 权限字符串
     * @return SysRole
     */
    SysRole getByRoleKey(String roleKey);

    /**
     * 验证角色名称唯一性
     */
    void validateRoleNameUnique(SysRole en);

    /**
     * 验证角色权限字符串唯一性
     */
    void validateRoleKeyUnique(SysRole en);

    /**
     * 验证角色
     *
     * @param id 主键ID
     * @return SysRole
     */
    SysRole validateRole(Long id);
}
