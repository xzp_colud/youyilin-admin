package com.youyilin.system.service;

import com.youyilin.system.entity.SysPost;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 岗位 Service
 */
public interface SysPostService extends IService<SysPost> {

    /**
     * 所有岗位
     */
    List<SysPost> listAll();

    /**
     * 按部门ID查询
     *
     * @param deptId 部门ID
     * @return ArrayList
     */
    List<SysPost> listByDeptId(Long deptId);

    /**
     * 按部门ID及岗位名称查询
     *
     * @param deptId   部门ID
     * @param postName 岗位名称
     * @return SysPost
     */
    SysPost getByDeptIdAndPostName(Long deptId, String postName);

    /**
     * 根据岗位编号查询
     *
     * @param postCode 岗位编号
     * @return SysPost
     */
    SysPost getByPostCode(String postCode);

    /**
     * 查询验证岗位
     *
     * @param postIds 岗位ID集合
     * @return ArrayList
     */
    List<Long> listPostIdsByIds(List<Long> postIds);

    /**
     * 更新岗位部门昵称
     */
    void updateDeptNameByDeptId(Long deptId, String deptName);

    /**
     * 验证岗位名称唯一
     */
    void validatePostNameUnique(SysPost en);

    /**
     * 验证岗位编码唯一
     */
    void validatePostCodeUnique(SysPost en);

    /**
     * 验证岗位
     */
    SysPost validatePost(Long id);

    /**
     * 当前部门下的岗位是否都禁用
     */
    boolean validatePostAllDisabledByDeptId(Long deptId);
}
