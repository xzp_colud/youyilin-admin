package com.youyilin.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.system.mapper.SysOptLogMapper;
import com.youyilin.system.entity.SysOptLog;
import com.youyilin.system.service.SysOptLogService;
import org.springframework.stereotype.Service;

/**
 * 操作日志 Impl
 */
@Service
public class SysOptLogImpl extends ServiceImpl<SysOptLogMapper, SysOptLog> implements SysOptLogService {

}
