package com.youyilin.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.common.entity.TreeSelect;
import com.youyilin.common.bean.Page;
import com.youyilin.system.dto.address.AddressQueryDTO;
import com.youyilin.system.entity.Address;
import com.youyilin.system.vo.address.AddressVO;

import java.util.List;

/**
 * 国家地理 Service
 */
public interface AddressService extends IService<Address> {

    /**
     * 启动添加至Redis
     */
    void initToRedis();

    Integer getTotal(Page<AddressQueryDTO> page);

    List<AddressVO> getPage(Page<AddressQueryDTO> page);

    /**
     * 获取省份
     */
    Address getProvinceByName(String name);

    /**
     * 获取 下级 地区数据
     */
    List<Address> listByPid(Long pidId);

    /**
     * 树结构
     */
    List<TreeSelect> listTree();
}
