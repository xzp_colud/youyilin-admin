package com.youyilin.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.enums.StatusEnum;
import com.youyilin.common.exception.Assert;
import com.youyilin.common.utils.ObjectUniqueUtil;
import com.youyilin.system.entity.SysDept;
import com.youyilin.system.mapper.SysDeptMapper;
import com.youyilin.system.service.SysDeptService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 部门 Impl
 */
@Service
public class SysDeptImpl extends ServiceImpl<SysDeptMapper, SysDept> implements SysDeptService {

    @Override
    public List<SysDept> listByPidId(Long pidId) {
        return super.list(new LambdaQueryWrapper<SysDept>().eq(SysDept::getPidId, pidId));
    }

    @Override
    public SysDept getByPidIdAndDeptName(Long pidId, String deptName) {
        if (pidId == null) {
            pidId = 0L;
        }
        return super.getOne(new LambdaQueryWrapper<SysDept>()
                .eq(SysDept::getPidId, pidId)
                .eq(SysDept::getDeptCode, deptName));
    }

    @Override
    public SysDept getByDeptCode(String deptCode) {
        return super.getOne(new LambdaQueryWrapper<SysDept>().eq(SysDept::getDeptCode, deptCode));
    }

    @Override
    public void validateDeptNameUnique(SysDept en) {
        SysDept sysDeptUnique = this.getByPidIdAndDeptName(en.getPidId(), en.getDeptName());
        Assert.isTrue(ObjectUniqueUtil.validateObjectUnique(en, sysDeptUnique), "部门名称已存在");
    }

    @Override
    public void validateDeptCodeUnique(SysDept en) {
        SysDept sysDeptUnique = this.getByDeptCode(en.getDeptCode());
        Assert.isTrue(ObjectUniqueUtil.validateObjectUnique(en, sysDeptUnique), "部门编码已存在");
    }

    @Override
    public SysDept validateDept(Long deptId) {
        SysDept sysDept = super.getById(deptId);
        Assert.notNull(sysDept, "部门不存在");

        return sysDept;
    }

    @Override
    public SysDept validateDeptStatus(Long deptId) {
        SysDept sysDept = this.validateDept(deptId);
        boolean statusFlag = sysDept.getStatus() != null && sysDept.getStatus().equals(StatusEnum.NORMAL.getCode());
        Assert.isTrue(statusFlag, "部门已禁用");

        return sysDept;
    }
}
