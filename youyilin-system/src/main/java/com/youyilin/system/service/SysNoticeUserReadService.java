package com.youyilin.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.system.entity.SysNoticeUserRead;

import java.util.List;

/**
 * 通知读取 Service
 */
public interface SysNoticeUserReadService extends IService<SysNoticeUserRead> {

    /**
     * 按用户统计未读数量
     *
     * @param userId 用ID
     * @return Integer
     */
    Integer countNoReadNumByUserId(Long userId);

    /**
     * 按用户查询所有未读通知
     *
     * @param userId 用户ID
     * @return ArrayList
     */
    List<SysNoticeUserRead> listNoReadByUserId(Long userId);

    /**
     * 验证已发通知是否存在
     *
     * @param id 已发通知ID
     */
    SysNoticeUserRead validateNoticeUserRead(Long id);

    /**
     * 更新(单个已读)
     *
     * @param id 主键ID
     */
    void updateRead(Long id);

    /**
     * 更新(撤销)
     *
     * @param ids 主键IDS
     */
    void updateCancel(List<Long> ids);

    /**
     * 更新(撤销全部)
     *
     * @param noticeId 通知ID
     */
    void updateCancel(Long noticeId);
}
