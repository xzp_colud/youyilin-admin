package com.youyilin.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.exception.ApiException;
import com.youyilin.common.exception.Assert;
import com.youyilin.common.utils.ObjectUniqueUtil;
import com.youyilin.system.enums.ConfigTypeEnum;
import com.youyilin.system.mapper.SysConfigDataMapper;
import com.youyilin.system.entity.SysConfigData;
import com.youyilin.system.service.SysConfigDataService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 系统配置项 Impl
 */
@Service
public class SysConfigDataImpl extends ServiceImpl<SysConfigDataMapper, SysConfigData> implements SysConfigDataService {

    @Override
    public SysConfigData getByConfigIdAndCode(Long configId, String code) {
        return super.getOne(new LambdaQueryWrapper<SysConfigData>()
                .eq(SysConfigData::getConfigId, configId)
                .eq(SysConfigData::getCode, code));
    }

    @Override
    public List<SysConfigData> listByConfigId(Long configId) {
        return super.list(new LambdaQueryWrapper<SysConfigData>()
                .eq(SysConfigData::getConfigId, configId)
                .orderByAsc(SysConfigData::getSort));
    }

    @Override
    public List<SysConfigData> listByConfigIds(List<Long> configIds) {
        return super.list(new LambdaQueryWrapper<SysConfigData>()
                .in(SysConfigData::getConfigId, configIds)
                .orderByAsc(SysConfigData::getSort));
    }

    @Override
    public List<SysConfigData> listAll() {
        return super.list();
    }

    @Override
    public void validateDataOptions(SysConfigData en) {
        Integer type = en.getType();
        if (type == ConfigTypeEnum.CHECKBOX.getCode()
                || type == ConfigTypeEnum.RADIO.getCode()
                || type == ConfigTypeEnum.SELECT.getCode()) {
            try {
                String[] options = en.getOptions().split(",");
                Assert.notEmpty(options, "配置项不能为空");

                Set<String> key = new HashSet<>();
                for (String option : options) {
                    String[] kv = option.split("=");
                    Assert.isTrue(kv.length == 2, "配置项形式 key=value,key=value");
                    Assert.hasLength(kv[0], "配置项 key 异常");
                    Assert.hasLength(kv[1], "配置项 value 异常");

                    key.add(kv[0]);
                }
                Assert.isTrue(key.size() == options.length, "配置项重复配置");
            } catch (ApiException e) {
                throw new ApiException(e.getMessage());
            } catch (Exception e) {
                throw new ApiException("配置项形式 key=value,key=value");
            }
        } else {
            en.setOptions(null);
        }
    }

    @Override
    public void validateCodeUnique(SysConfigData en) {
        SysConfigData codeUnique = this.getByConfigIdAndCode(en.getConfigId(), en.getCode());
        Assert.isTrue(ObjectUniqueUtil.validateObjectUnique(en, codeUnique), "配置编码已存在");
    }

    @Override
    public SysConfigData validateConfigData(Long id) {
        SysConfigData configData = super.getById(id);
        Assert.notNull(configData, "配置项不存在");

        return configData;
    }

    @Override
    public void updateData(Long configId, List<SysConfigData> configData) {
        List<Long> ids = new ArrayList<>();
        configData.forEach(item -> ids.add(item.getId()));

        List<SysConfigData> findList = super.listByIds(ids);
        boolean flag = CollectionUtils.isNotEmpty(findList) && findList.size() == configData.size();
        Assert.isTrue(flag, "配置项数据不完整");

        for (SysConfigData scd : findList) {
            Assert.notNull(scd.getConfigId(), "配置项异常");
            Assert.isTrue(scd.getConfigId().equals(configId), "配置项异常");
        }
        super.updateBatchById(configData);
    }
}