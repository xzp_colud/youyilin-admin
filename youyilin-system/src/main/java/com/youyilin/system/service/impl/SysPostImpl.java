package com.youyilin.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.enums.StatusEnum;
import com.youyilin.common.exception.Assert;
import com.youyilin.common.utils.ObjectUniqueUtil;
import com.youyilin.system.entity.SysPost;
import com.youyilin.system.mapper.SysPostMapper;
import com.youyilin.system.service.SysPostService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 岗位 Impl
 */
@Service
public class SysPostImpl extends ServiceImpl<SysPostMapper, SysPost> implements SysPostService {

    @Override
    public List<SysPost> listAll() {
        return super.list();
    }

    @Override
    public List<SysPost> listByDeptId(Long deptId) {
        return this.list(new LambdaQueryWrapper<SysPost>().eq(SysPost::getDeptId, deptId));
    }

    @Override
    public SysPost getByDeptIdAndPostName(Long deptId, String postName) {
        return super.getOne(new LambdaQueryWrapper<SysPost>()
                .eq(SysPost::getDeptId, deptId)
                .eq(SysPost::getPostName, postName));
    }

    @Override
    public SysPost getByPostCode(String postCode) {
        return this.getOne(new LambdaQueryWrapper<SysPost>().eq(SysPost::getPostCode, postCode));
    }

    @Override
    public List<Long> listPostIdsByIds(List<Long> postIds) {
        if (CollectionUtils.isEmpty(postIds)) {
            return new ArrayList<>();
        }
        List<SysPost> list = this.listByIds(postIds);
        if (CollectionUtils.isEmpty(list)) {
            return new ArrayList<>();
        }
        List<Long> postIdList = new ArrayList<>();
        for (SysPost sysPost : list) {
            postIdList.add(sysPost.getId());
        }
        return postIdList;
    }

    @Override
    public void updateDeptNameByDeptId(Long deptId, String deptName) {
        super.update(new LambdaUpdateWrapper<SysPost>().set(SysPost::getDeptName, deptName).eq(SysPost::getDeptId, deptId));
    }

    @Override
    public void validatePostNameUnique(SysPost en) {
        SysPost sysPostUnique = this.getByDeptIdAndPostName(en.getDeptId(), en.getPostName());
        Assert.isTrue(ObjectUniqueUtil.validateObjectUnique(en, sysPostUnique), "岗位名称已存在");
    }

    @Override
    public void validatePostCodeUnique(SysPost en) {
        SysPost sysPostUnique = this.getByPostCode(en.getPostCode());
        Assert.isTrue(ObjectUniqueUtil.validateObjectUnique(en, sysPostUnique), "岗位编码已存在");
    }

    @Override
    public SysPost validatePost(Long id) {
        SysPost sysPost = super.getById(id);
        Assert.notNull(sysPost, "岗位信息不存在");

        return sysPost;
    }

    @Override
    public boolean validatePostAllDisabledByDeptId(Long deptId) {
        long normalPostNum = this.count(new LambdaQueryWrapper<SysPost>()
                .eq(SysPost::getStatus, StatusEnum.NORMAL.getCode())
                .eq(SysPost::getDeptId, deptId));
        return normalPostNum == 0L;
    }
}
