package com.youyilin.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.system.mapper.SysRoleMenuMapper;
import com.youyilin.system.entity.SysRoleMenu;
import com.youyilin.system.service.SysRoleMenuService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 角色授权 Impl
 */
@Service
public class SysRoleMenuImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements SysRoleMenuService {

    @Override
    public List<String> listMenuIdsByRoleIds(List<Long> roleIds) {
        List<SysRoleMenu> menuIds = super.list(new LambdaQueryWrapper<SysRoleMenu>().in(SysRoleMenu::getRoleId, roleIds));
        if (CollectionUtils.isEmpty(menuIds)) {
            return new ArrayList<>();
        }
        return menuIds.stream().map(item -> item.getMenuId().toString()).collect(Collectors.toList());
    }

    @Override
    public void deleteRoleMenuByRoleId(Long roleId) {
        super.remove(new LambdaQueryWrapper<SysRoleMenu>().eq(SysRoleMenu::getRoleId, roleId));
    }
}
