package com.youyilin.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.bean.Page;
import com.youyilin.common.entity.TreeSelect;
import com.youyilin.common.redis.RedisPrefix;
import com.youyilin.common.redis.RedisService;
import com.youyilin.system.mapper.AddressMapper;
import com.youyilin.system.vo.address.AddressVO;
import com.youyilin.system.dto.address.AddressQueryDTO;
import com.youyilin.system.entity.Address;
import com.youyilin.system.service.AddressService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 国家地理 Impl
 */
@Service
public class AddressImpl extends ServiceImpl<AddressMapper, Address> implements AddressService {

    private final static String ADDRESS_REDIS_KEY = RedisPrefix.ADDRESS_KEY_PREFIX + "0";

    private final RedisService redisService;

    public AddressImpl(RedisService redisService) {
        this.redisService = redisService;
    }

    @Override
    public void initToRedis() {
        boolean hasAddress = redisService.hasKey(ADDRESS_REDIS_KEY);
        if (!hasAddress) {
            toRedis();
        }
    }

    @Override
    public Integer getTotal(Page<AddressQueryDTO> page) {
        return baseMapper.getTotal(page);
    }

    @Override
    public List<AddressVO> getPage(Page<AddressQueryDTO> page) {
        return baseMapper.getPage(page);
    }

    @Override
    public Address getProvinceByName(String name) {
        return this.getOne(new LambdaQueryWrapper<Address>().eq(Address::getName, name));
    }

    @Override
    public List<Address> listByPid(Long pidId) {
        return this.list(new LambdaQueryWrapper<Address>().eq(Address::getPid, pidId));
    }

    @Override
    public List<TreeSelect> listTree() {
        try {
            List<TreeSelect> sysAddressList = redisService.getList(ADDRESS_REDIS_KEY, TreeSelect.class);
            if (!CollectionUtils.isEmpty(sysAddressList)) {
                return sysAddressList;
            }
        } catch (Exception e) {
            log.error("【Redis 异常】 地址缓存", e);
        }

        return toRedis();
    }

    /**
     * 添加至 Redis
     */
    private List<TreeSelect> toRedis() {
        List<Address> all = this.list();
        if (CollectionUtils.isEmpty(all)) {
            return new ArrayList<>();
        }
        List<TreeSelect> province = listTree(all, "0");
        if (CollectionUtils.isEmpty(province)) {
            return new ArrayList<>();
        }

        province.forEach(p -> {
            List<TreeSelect> city = listTree(all, p.getKey());
            if (!CollectionUtils.isEmpty(city)) {
                city.forEach(c -> {
                    List<TreeSelect> area = listTree(all, c.getKey());
                    c.setChildren(area);
                });
            }
            p.setChildren(city);
        });
        String redisKey = RedisPrefix.ADDRESS_KEY_PREFIX + "0";
        if (redisService.hasKey(redisKey)) {
            redisService.delete(redisKey);
        }
        redisService.add(RedisPrefix.ADDRESS_KEY_PREFIX + "0", province);

        return province;
    }

    /**
     * 递归
     */
    private List<TreeSelect> listTree(List<Address> all, String pid) {
        if (CollectionUtils.isEmpty(all) || StringUtils.isBlank(pid)) {
            return new ArrayList<>();
        }
        return all.stream().filter(item -> item.getPid().toString().equals(pid)).map(item -> {
            TreeSelect ts = new TreeSelect();
            ts.setLabel(item.getName());
            ts.setValue(item.getName());
            ts.setKey(item.getId().toString());
            return ts;
        }).collect(Collectors.toList());
    }
}
