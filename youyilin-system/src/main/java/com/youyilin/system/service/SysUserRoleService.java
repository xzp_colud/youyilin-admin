package com.youyilin.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.system.entity.SysUserRole;

import java.util.List;

/**
 * 用户所属角色 Service
 */
public interface SysUserRoleService extends IService<SysUserRole> {

    /**
     * 按用户ID查询角色ID
     *
     * @param userId 用户ID
     * @return ArrayList
     */
    List<String> listRoleIdsByUserId(Long userId);

    /**
     * 新增用户角色
     */
    void insertUserRoleBatch(Long userId, List<Long> roleIds);

    /**
     * 删除用户当前的角色
     */
    void deleteByUserId(Long userId);
}
