package com.youyilin.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.enums.DelFlagEnum;
import com.youyilin.common.exception.Assert;
import com.youyilin.common.utils.ObjectUniqueUtil;
import com.youyilin.system.mapper.SysIpMapper;
import com.youyilin.system.entity.SysIp;
import com.youyilin.system.service.SysIpService;
import org.springframework.stereotype.Service;

/**
 * IP Impl
 */
@Service
public class SysIpImpl extends ServiceImpl<SysIpMapper, SysIp> implements SysIpService {

    @Override
    public SysIp getByIp(String ip) {
        return super.getOne(new LambdaQueryWrapper<SysIp>().eq(SysIp::getIp, ip));
    }

    @Override
    public void delIp(Long id) {
        super.update(new LambdaUpdateWrapper<SysIp>()
                .set(SysIp::getDelFlag, DelFlagEnum.DELETE.getCode())
                .eq(SysIp::getId, id));
    }

    @Override
    public SysIp validateIp(Long id) {
        SysIp ip = super.getById(id);
        Assert.notNull(ip, "IP is Null");

        return ip;
    }

    @Override
    public void validateIpUnique(SysIp en) {
        SysIp ipUnique = this.getByIp(en.getIp());
        Assert.isTrue(ObjectUniqueUtil.validateObjectUnique(en, ipUnique), "IP already exists");
    }
}
