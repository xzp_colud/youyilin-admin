package com.youyilin.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.config.email.EmailUtil;
import com.youyilin.common.exception.Assert;
import com.youyilin.common.utils.ObjectUniqueUtil;
import com.youyilin.system.entity.SysUser;
import com.youyilin.system.mapper.SysUserMapper;
import com.youyilin.system.service.SysUserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.nio.charset.StandardCharsets;
import java.util.Date;

/**
 * 用户 Impl
 */
@Service
public class SysUserImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

    // 初始密码
    private final static String RESET_PASSWORD = "123456";

    @Override
    public SysUser getByUserName(String userName) {
        return super.getOne(new LambdaQueryWrapper<SysUser>().eq(SysUser::getUserName, userName));
    }

    @Override
    public SysUser getByPhone(String phone) {
        return super.getOne(new LambdaQueryWrapper<SysUser>().eq(SysUser::getPhone, phone));
    }

    @Override
    public SysUser getByEmail(String email) {
        return super.getOne(new LambdaQueryWrapper<SysUser>().eq(SysUser::getEmail, email));
    }

    @Override
    public void updateUserByMe(SysUser sysUser) {
        LambdaUpdateWrapper<SysUser> updateWrapper = new LambdaUpdateWrapper<>();
        if (StringUtils.isNotBlank(sysUser.getAvatarUrl())) {
            updateWrapper.set(SysUser::getAvatarUrl, sysUser.getAvatarUrl());
        }
        if (StringUtils.isNotBlank(sysUser.getRealName())) {
            updateWrapper.set(SysUser::getRealName, sysUser.getRealName());
        }
        if (StringUtils.isNotBlank(sysUser.getEmail())) {
            Assert.isFalse(EmailUtil.isNotEmail(sysUser.getEmail()), "邮箱格式不正确");
            updateWrapper.set(SysUser::getEmail, sysUser.getEmail());
        }
        if (StringUtils.isNotBlank(sysUser.getSex())) {
            updateWrapper.set(SysUser::getSex, sysUser.getSex());
        }
        if (StringUtils.isNotBlank(sysUser.getAge())) {
            updateWrapper.set(SysUser::getAge, sysUser.getAge());
        }
        if (StringUtils.isNotBlank(sysUser.getProvince())) {
            updateWrapper.set(SysUser::getProvince, sysUser.getProvince());
        }
        if (StringUtils.isNotBlank(sysUser.getCity())) {
            updateWrapper.set(SysUser::getCity, sysUser.getCity());
        }
        if (StringUtils.isNotBlank(sysUser.getArea())) {
            updateWrapper.set(SysUser::getArea, sysUser.getArea());
        }
        if (StringUtils.isNotBlank(sysUser.getDetail())) {
            updateWrapper.set(SysUser::getDetail, sysUser.getDetail());
        }
        if (StringUtils.isNotBlank(sysUser.getDescription())) {
            updateWrapper.set(SysUser::getDescription, sysUser.getDescription());
        }
        updateWrapper.eq(SysUser::getUserName, sysUser.getUserName());
        super.update(updateWrapper);
    }

    @Override
    public void updateLoginInfo(String userName, String loginIp, String loginLocation, Date loginDate) {
        super.update(new LambdaUpdateWrapper<SysUser>()
                .set(SysUser::getLoginIp, loginIp)
                .set(SysUser::getLoginLocation, loginLocation)
                .set(SysUser::getLoginDate, loginDate)
                .eq(SysUser::getUserName, userName));
    }

    @Override
    public void updatePassword(String userName) {
        String password = this.getResetPassword();
        super.update(new LambdaUpdateWrapper<SysUser>()
                .set(SysUser::getPassword, password)
                .eq(SysUser::getUserName, userName));
    }

    @Override
    public String getResetPassword() {
        return this.refreshPassword(RESET_PASSWORD);
    }

    @Override
    public String refreshPassword(String password) {
        return DigestUtils.md5DigestAsHex(password.getBytes(StandardCharsets.UTF_8)).toUpperCase();
    }

    @Override
    public void validateUserNameUnique(SysUser en) {
        SysUser userNameUnique = this.getByUserName(en.getUserName());
        Assert.isTrue(ObjectUniqueUtil.validateObjectUnique(en, userNameUnique), "用户名已存在");
    }

    @Override
    public void validatePhoneUnique(SysUser en) {
        SysUser phoneUnique = this.getByPhone(en.getPhone());
        Assert.isTrue(ObjectUniqueUtil.validateObjectUnique(en, phoneUnique), "手机号已存在");
    }

    @Override
    public void validateEmailUnique(SysUser en) {
        if (StringUtils.isBlank(en.getEmail())) return;
        SysUser emailUnique = this.getByEmail(en.getEmail());
        Assert.isTrue(ObjectUniqueUtil.validateObjectUnique(en, emailUnique), "邮箱已存在");
    }

    @Override
    public SysUser validateUser(Long id) {
        SysUser sysUser = super.getById(id);
        Assert.notNull(sysUser, "System Error");

        return sysUser;
    }

    @Override
    public SysUser validateUser(String userName) {
        SysUser sysUser = this.getByUserName(userName);
        Assert.notNull(sysUser, "System Error");

        return sysUser;
    }

    @Override
    public SysUser validateUserStatus(String userName) {
        SysUser sysUser = this.validateUser(userName);
        Assert.isTrue(sysUser.isStatusNormal(), "System Error");

        return sysUser;
    }
}
