package com.youyilin.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.enums.StatusEnum;
import com.youyilin.common.exception.Assert;
import com.youyilin.common.utils.ObjectUniqueUtil;
import com.youyilin.system.enums.MenuTypeEnum;
import com.youyilin.system.mapper.SysMenuMapper;
import com.youyilin.system.entity.SysMenu;
import com.youyilin.system.service.SysMenuService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * 菜单权限 Impl
 */
@Service
public class SysMenuImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements SysMenuService {

    @Override
    public SysMenu getByMenuCode(String menuCode) {
        return super.getOne(new LambdaQueryWrapper<SysMenu>().eq(SysMenu::getMenuCode, menuCode));
    }

    @Override
    public SysMenu getByPath(String fullPath) {
        return super.getOne(new LambdaQueryWrapper<SysMenu>().eq(SysMenu::getPath, fullPath));
    }

    @Override
    public SysMenu getByName(String name) {
        return super.getOne(new LambdaQueryWrapper<SysMenu>().eq(SysMenu::getName, name));
    }

    @Override
    public SysMenu getByPerms(String perms) {
        return super.getOne(new LambdaQueryWrapper<SysMenu>().eq(SysMenu::getPerms, perms));
    }

    @Override
    public List<SysMenu> listBySort() {
        return super.list(new LambdaQueryWrapper<SysMenu>().orderByAsc(SysMenu::getSort).orderByDesc(SysMenu::getId));
    }

    @Override
    public List<SysMenu> listByPidId(Long pidId) {
        return super.list(new LambdaQueryWrapper<SysMenu>().eq(SysMenu::getPidId, pidId));
    }

    @Override
    public void validatePidId(SysMenu en) {
        if (en.getPidId() != null && en.getPidId() != 0L) {
            SysMenu parentMenu = super.getById(en.getPidId());
            boolean parentFlag = parentMenu == null || parentMenu.getStatus() == null || parentMenu.getStatus() != StatusEnum.NORMAL.getCode();
            Assert.isFalse(parentFlag, "父节点禁用，不能添加");

            boolean flag = en.getType() != MenuTypeEnum.BUTTON.getCode()
                    && (parentMenu.getType() == MenuTypeEnum.CATALOG.getCode() || parentMenu.getType() == MenuTypeEnum.MENU.getCode()) && StringUtils.isNotBlank(parentMenu.getName());
            Assert.isFalse(flag, "当前目录不能添加");

            en.setPidName(parentMenu.getMenuName());
        }
    }

    @Override
    public void validateMoveNodes(SysMenu en) {
        if (en.getId() != null) {
            SysMenu oldMenu = super.getById(en.getId());
            Assert.notNull(oldMenu, "权限菜单不存在");
            Assert.isFalse(Objects.equals(en.getId(), en.getPidId()), "不能往自身添加子菜单");

            List<SysMenu> sysMenuList = this.listByPidId(en.getId());
            if (CollectionUtils.isNotEmpty(sysMenuList)) {
                for (SysMenu menu : sysMenuList) {
                    Assert.isFalse(Objects.equals(menu.getId(), en.getPidId()), "不允许往子节点移动");

                    menu.setPidName(en.getMenuName());
                }
                // 名称冗余
                super.updateBatchById(sysMenuList);
            }
        }
    }

    @Override
    public SysMenu validateMenu(Long id) {
        SysMenu menu = super.getById(id);
        Assert.notNull(menu, "菜单权限不存在");

        return menu;
    }

    @Override
    public void validateMenuCodeUnique(SysMenu en) {
        SysMenu menuCodeUnique = this.getByMenuCode(en.getMenuCode());
        Assert.isTrue(ObjectUniqueUtil.validateObjectUnique(en, menuCodeUnique), "编码已存在");
    }

    @Override
    public void validatePathUnique(SysMenu en) {
        if (StringUtils.isNotBlank(en.getPath())) {
            SysMenu fullPathUnique = this.getByPath(en.getPath());
            Assert.isTrue(ObjectUniqueUtil.validateObjectUnique(en, fullPathUnique), "全路径已存在");
        }
    }

    @Override
    public void validateNameUnique(SysMenu en) {
        if (StringUtils.isNotBlank(en.getName())) {
            SysMenu nameUnique = this.getByName(en.getName());
            Assert.isTrue(ObjectUniqueUtil.validateObjectUnique(en, nameUnique), "路由名称已存在");
        }
    }

    @Override
    public void validatePermsUnique(SysMenu en) {
        if (StringUtils.isNotBlank(en.getPerms())) {
            SysMenu permsUnique = this.getByPerms(en.getPerms());
            Assert.isTrue(ObjectUniqueUtil.validateObjectUnique(en, permsUnique), "权限标识已存在");
        }
    }
}
