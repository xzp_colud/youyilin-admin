package com.youyilin.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.system.entity.SysErrorLog;

/**
 * 异常日志 Service
 */
public interface SysErrorLogService extends IService<SysErrorLog> {

}
