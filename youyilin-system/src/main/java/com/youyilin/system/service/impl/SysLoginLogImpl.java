package com.youyilin.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.system.mapper.SysLoginLogMapper;
import com.youyilin.system.entity.SysLoginLog;
import com.youyilin.system.service.SysLoginLogService;
import org.springframework.stereotype.Service;

/**
 * 登录日志 Impl
 */
@Service
public class SysLoginLogImpl extends ServiceImpl<SysLoginLogMapper, SysLoginLog> implements SysLoginLogService {

}
