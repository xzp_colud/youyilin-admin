package com.youyilin.system.service;

import com.youyilin.system.entity.SysMenu;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 菜单权限 Service
 */
public interface SysMenuService extends IService<SysMenu> {

    /**
     * 根据菜单编码查询
     *
     * @param menuCode 菜单编码
     * @return SysMenu
     */
    SysMenu getByMenuCode(String menuCode);

    /**
     * 根据路由全路径查询
     *
     * @param path 路由路径
     * @return SysMenu
     */
    SysMenu getByPath(String path);

    /**
     * 根据路由名称查询
     *
     * @param name 路由名称
     * @return SysMenu
     */
    SysMenu getByName(String name);

    /**
     * 根据权限标识查询
     *
     * @param perms 权限标识
     * @return SysMenu
     */
    SysMenu getByPerms(String perms);

    /**
     * 根据父节点查询
     *
     * @param pidId 父节点
     * @return ArrayList
     */
    List<SysMenu> listByPidId(Long pidId);

    /**
     * 根据排序查询
     *
     * @return ArrayList
     */
    List<SysMenu> listBySort();

    /**
     * 验证编码唯一性
     */
    void validateMenuCodeUnique(SysMenu en);

    /**
     * 验证路由全路径唯一性
     */
    void validatePathUnique(SysMenu en);

    /**
     * 验证路由名称唯一性
     */
    void validateNameUnique(SysMenu en);

    /**
     * 验证权限标识唯一性
     */
    void validatePermsUnique(SysMenu en);

    /**
     * 验证父节点
     */
    void validatePidId(SysMenu en);

    /**
     * 验证是否可以移动
     */
    void validateMoveNodes(SysMenu en);

    /**
     * 验证是否存在
     */
    SysMenu validateMenu(Long id);
}
