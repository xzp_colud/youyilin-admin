package com.youyilin.system.service;

import com.youyilin.system.entity.SysDictData;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 字典键值 Service
 */
public interface SysDictDataService extends IService<SysDictData> {

    /**
     * 根据字典类型查询
     *
     * @param dictType 字典类型
     * @return ArrayList
     */
    List<SysDictData> listByDictType(String dictType);

    /**
     * 根据字典类型查询
     *
     * @param dictType 字典类型
     * @return ArrayList
     */
    List<String> listStringByDictType(String dictType);

    /**
     * 根据字典类型数组查询
     *
     * @param dictTypeList 字典类型数组
     * @return ArrayList
     */
    List<SysDictData> listByDictTypeList(List<String> dictTypeList);

    /**
     * 根据字典类型和键值查询
     *
     * @param dictType  字典类型
     * @param dictValue 字典键值
     * @return SysDictData
     */
    SysDictData getByDictTypeAndDictValue(String dictType, String dictValue);

    /**
     * 删除
     */
    void delDictData(Long id);

    /**
     * 取消默认
     */
    void updateDefaultFalse(String dictType);

    /**
     * 验证字典键值编码唯一性
     */
    void validateDictCodeUnique(SysDictData en);

    /**
     * 验证字典键值唯一性
     */
    void validateDictValueUnique(SysDictData en);

    /**
     * 验证是否存在
     */
    SysDictData validateDictData(Long id);
}
