package com.youyilin.system.service;

import com.youyilin.system.entity.SysOptLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 操作日志 Service
 */
public interface SysOptLogService extends IService<SysOptLog> {

}
