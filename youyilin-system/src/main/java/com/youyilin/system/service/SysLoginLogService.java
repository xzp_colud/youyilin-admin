package com.youyilin.system.service;

import com.youyilin.system.entity.SysLoginLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 登录日志 Service
 */
public interface SysLoginLogService extends IService<SysLoginLog> {

}
