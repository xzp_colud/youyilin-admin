package com.youyilin.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.exception.Assert;
import com.youyilin.common.utils.ObjectUniqueUtil;
import com.youyilin.system.mapper.SysConfigMapper;
import com.youyilin.system.entity.SysConfig;
import com.youyilin.system.service.SysConfigService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 系统配置 Impl
 */
@Service
public class SysConfigImpl extends ServiceImpl<SysConfigMapper, SysConfig> implements SysConfigService {

    @Override
    public SysConfig getByName(String name) {
        return super.getOne(new LambdaQueryWrapper<SysConfig>().eq(SysConfig::getName, name));
    }

    @Override
    public List<SysConfig> getByType(Integer type) {
        return super.list(new LambdaQueryWrapper<SysConfig>().eq(SysConfig::getType, type));
    }

    @Override
    public void validateNameUnique(SysConfig en) {
        SysConfig nameUnique = this.getByName(en.getName());
        Assert.isTrue(ObjectUniqueUtil.validateObjectUnique(en, nameUnique), "配置名称已存在");
    }

    @Override
    public void validateTypeUnique(SysConfig en) {
        List<SysConfig> typeUnique = this.getByType(en.getType());
        if (CollectionUtils.isEmpty(typeUnique)) return;

        Assert.isTrue(ObjectUniqueUtil.validateObjectUnique(en, typeUnique.get(0)), "配置类型已存在");
    }

    @Override
    public SysConfig validateConfig(Long id) {
        SysConfig sysConfig = super.getById(id);
        Assert.notNull(sysConfig, "配置不存在");

        return sysConfig;
    }
}
