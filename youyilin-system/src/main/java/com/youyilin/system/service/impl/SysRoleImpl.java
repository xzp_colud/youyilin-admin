package com.youyilin.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.exception.Assert;
import com.youyilin.common.utils.ObjectUniqueUtil;
import com.youyilin.system.entity.SysRole;
import com.youyilin.system.mapper.SysRoleMapper;
import com.youyilin.system.service.SysRoleService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 角色 Impl
 */
@Service
public class SysRoleImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {

    @Override
    public List<SysRole> listAll() {
        List<SysRole> sysRoleList = super.list();
        if (CollectionUtils.isEmpty(sysRoleList)) {
            return new ArrayList<>();
        }
        return sysRoleList.stream().filter(item -> !item.isAdmin()).collect(Collectors.toList());
    }

    @Override
    public List<Long> listRoleIdsByIds(List<Long> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return new ArrayList<>();
        }
        List<SysRole> list = super.listByIds(ids);
        if (CollectionUtils.isEmpty(list)) {
            return new ArrayList<>();
        }
        List<Long> roleIds = new ArrayList<>();
        for (SysRole sysRole : list) {
            roleIds.add(sysRole.getId());
        }
        return roleIds;
    }

    @Override
    public SysRole getByRoleName(String roleName) {
        return super.getOne(new LambdaQueryWrapper<SysRole>().eq(SysRole::getRoleName, roleName));
    }

    @Override
    public SysRole getByRoleKey(String roleKey) {
        return super.getOne(new LambdaQueryWrapper<SysRole>().eq(SysRole::getRoleKey, roleKey));
    }

    @Override
    public void validateRoleNameUnique(SysRole source) {
        SysRole sysRoleNameUnique = this.getByRoleName(source.getRoleName());
        Assert.isTrue(ObjectUniqueUtil.validateObjectUnique(source, sysRoleNameUnique), "角色名称已存在");
    }

    @Override
    public void validateRoleKeyUnique(SysRole source) {
        SysRole sysRoleKeyUnique = this.getByRoleKey(source.getRoleKey());
        Assert.isTrue(ObjectUniqueUtil.validateObjectUnique(source, sysRoleKeyUnique), "角色权限字符串已存在");
    }

    @Override
    public SysRole validateRole(Long id) {
        SysRole sysRole = this.getById(id);
        Assert.notNull(sysRole, "角色不存在");

        return sysRole;
    }
}