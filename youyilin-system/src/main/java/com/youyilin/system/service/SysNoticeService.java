package com.youyilin.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.system.entity.SysNotice;

/**
 * 通知 Service
 */
public interface SysNoticeService extends IService<SysNotice> {

    /**
     * 根据通知标题查询
     *
     * @param title 标题
     * @return SysNotice
     */
    SysNotice getByTitle(String title);

    /**
     * 最新一条公告
     */
    SysNotice getUpdated();

    /**
     * 禁用
     */
    void disabledNotice(Long id);

    /**
     * 保存系统错误通知
     */
    Long saveErrorNotice(String errorMsg);

    /**
     * 更新所有公告为禁用
     */
    void updateAnnounceDisabled();

    /**
     * 更新发送/读取数量
     *
     * @param noticeId 通知ID
     * @param sendNum  发送数量
     * @param readNum  读取数量
     */
    void updateSendReadNum(Long noticeId, Integer sendNum, Integer readNum);

    /**
     * 验证通知标题唯一性
     */
    void validateTitleUnique(SysNotice en);

    /**
     * 验证是否存在
     */
    SysNotice validateNotice(Long id);

    /**
     * 验证状态
     */
    SysNotice validateStatus(Long id);
}
