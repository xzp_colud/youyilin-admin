package com.youyilin.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.system.entity.SysUserPost;

import java.util.List;

/**
 * 用户所属岗位 Service
 */
public interface SysUserPostService extends IService<SysUserPost> {

    /**
     * 统计岗位人数
     *
     * @param postId 岗位ID
     * @return int
     */
    int countByPostId(Long postId);

    /**
     * 按用户ID查询岗位ID
     *
     * @param userId 用户ID
     * @return ArrayList
     */
    List<String> listPostIdsByUserId(Long userId);

    /**
     * 按岗位IDS查询
     *
     * @param postIds 岗位IDS
     * @return ArrayList
     */
    List<SysUserPost> listByPostIds(List<Long> postIds);

    /**
     * 保存
     */
    void insertUserPostBatch(Long userId, List<Long> postIds);

    /**
     * 删除用户当前的岗位
     */
    void deleteByUserId(Long userId);

    /**
     * 验证是否已不存在用户
     */
    void checkNoUserByPostId(Long postId);
}
