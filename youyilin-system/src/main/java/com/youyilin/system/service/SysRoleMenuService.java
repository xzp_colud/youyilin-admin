package com.youyilin.system.service;

import com.youyilin.system.entity.SysRoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 角色授权 Service
 */
public interface SysRoleMenuService extends IService<SysRoleMenu> {

    /**
     * 根据角色IDS查询已授权菜单IDS
     *
     * @param roleIds 角色IDS
     * @return ArrayList
     */
    List<String> listMenuIdsByRoleIds(List<Long> roleIds);

    /**
     * 根据角色删除角色权限关系
     */
    void deleteRoleMenuByRoleId(Long roleId);
}
