package com.youyilin.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.enums.StatusEnum;
import com.youyilin.common.exception.Assert;
import com.youyilin.common.utils.ObjectUniqueUtil;
import com.youyilin.system.mapper.SysDictTypeMapper;
import com.youyilin.system.entity.SysDictType;
import com.youyilin.system.service.SysDictTypeService;
import org.springframework.stereotype.Service;

/**
 * 字典 Impl
 */
@Service
public class SysDictTypeImpl extends ServiceImpl<SysDictTypeMapper, SysDictType> implements SysDictTypeService {

    @Override
    public SysDictType getByDictType(String dictType) {
        return super.getOne(new LambdaQueryWrapper<SysDictType>().eq(SysDictType::getDictType, dictType));
    }

    @Override
    public void validateDictTypeUnique(SysDictType en) {
        SysDictType sysDictTypeUnique = this.getByDictType(en.getDictType());
        Assert.isTrue(ObjectUniqueUtil.validateObjectUnique(en, sysDictTypeUnique), "字典类型已存在");
    }

    @Override
    public SysDictType validateDictType(Long id) {
        SysDictType dictType = super.getById(id);
        Assert.notNull(dictType, "字典类型不存在");

        return dictType;
    }

    @Override
    public void validateDictTypeDisabled(String dictType) {
        SysDictType sysDictType = this.getByDictType(dictType);
        boolean statusFlag = sysDictType != null && sysDictType.getStatus() != null && sysDictType.getStatus().equals(StatusEnum.NORMAL.getCode());
        Assert.isTrue(statusFlag, "字典类型已经禁用");
    }
}
