package com.youyilin.system.service;

import com.youyilin.system.entity.SysIp;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * IP Service
 */
public interface SysIpService extends IService<SysIp> {

    /**
     * 根据IP查询
     *
     * @param ip ip
     * @return SysIp
     */
    SysIp getByIp(String ip);

    /**
     * 删除
     */
    void delIp(Long id);

    /**
     * 验证IP
     */
    SysIp validateIp(Long id);

    /**
     * 验证IP唯一性
     */
    void validateIpUnique(SysIp en);
}
