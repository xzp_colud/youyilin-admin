package com.youyilin.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.enums.BooleanEnum;
import com.youyilin.common.enums.DelFlagEnum;
import com.youyilin.common.exception.Assert;
import com.youyilin.common.utils.ObjectUniqueUtil;
import com.youyilin.system.mapper.SysDictDataMapper;
import com.youyilin.system.entity.SysDictData;
import com.youyilin.system.service.SysDictDataService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 字典键值 Impl
 */
@Service
public class SysDictDataImpl extends ServiceImpl<SysDictDataMapper, SysDictData> implements SysDictDataService {

    @Override
    public List<SysDictData> listByDictType(String dictType) {
        if (StringUtils.isBlank(dictType)) {
            return new ArrayList<>();
        }
        return super.list(new LambdaQueryWrapper<SysDictData>().eq(SysDictData::getDictType, dictType));
    }

    @Override
    public List<String> listStringByDictType(String dictType) {
        List<SysDictData> list = this.listByDictType(dictType);
        if (CollectionUtils.isEmpty(list)) {
            return new ArrayList<>();
        }
        return list.stream().map(SysDictData::getDictValue).collect(Collectors.toList());
    }

    @Override
    public List<SysDictData> listByDictTypeList(List<String> dictTypeList) {
        if (CollectionUtils.isEmpty(dictTypeList)) {
            return new ArrayList<>();
        }
        return super.list(new LambdaQueryWrapper<SysDictData>().in(SysDictData::getDictType, dictTypeList));
    }

    @Override
    public SysDictData getByDictTypeAndDictValue(String dictType, String dictValue) {
        return super.getOne(new LambdaQueryWrapper<SysDictData>()
                .eq(SysDictData::getDictType, dictType)
                .eq(SysDictData::getDictValue, dictValue));
    }

    @Override
    public void updateDefaultFalse(String dictType) {
        super.update(new LambdaUpdateWrapper<SysDictData>()
                .set(SysDictData::getDefaultFlag, BooleanEnum.FALSE.getCode())
                .eq(SysDictData::getDictType, dictType));
    }

    @Override
    public void delDictData(Long id) {
        super.update(new LambdaUpdateWrapper<SysDictData>().set(SysDictData::getDelFlag, DelFlagEnum.DELETE.getCode()).eq(SysDictData::getId, id));
    }

    @Override
    public void validateDictCodeUnique(SysDictData en) {
        SysDictData sysDictDataUnique = super.getOne(new LambdaQueryWrapper<SysDictData>()
                .eq(SysDictData::getDictType, en.getDictType())
                .eq(SysDictData::getDictCode, en.getDictCode()));
        Assert.isTrue(ObjectUniqueUtil.validateObjectUnique(en, sysDictDataUnique), "已存在数据键值编码");
    }

    @Override
    public void validateDictValueUnique(SysDictData en) {
        SysDictData sysDictDataUnique = this.getByDictTypeAndDictValue(en.getDictType(), en.getDictValue());
        Assert.isTrue(ObjectUniqueUtil.validateObjectUnique(en, sysDictDataUnique), "已存在数据键值");
    }

    @Override
    public SysDictData validateDictData(Long id) {
        SysDictData dictData = super.getById(id);
        Assert.notNull(dictData, "字典键值不存在");

        return dictData;
    }
}
