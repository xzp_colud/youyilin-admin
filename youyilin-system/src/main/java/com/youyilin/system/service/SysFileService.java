package com.youyilin.system.service;

import com.youyilin.system.entity.SysFile;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 系统上传文件 Service
 */
public interface SysFileService extends IService<SysFile> {

}
