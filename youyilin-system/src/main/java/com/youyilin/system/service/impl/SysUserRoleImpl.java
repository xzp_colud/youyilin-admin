package com.youyilin.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.exception.Assert;
import com.youyilin.system.mapper.SysUserRoleMapper;
import com.youyilin.system.entity.SysRole;
import com.youyilin.system.entity.SysUserRole;
import com.youyilin.system.service.SysUserRoleService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 用户所属角色 Impl
 */
@Service
public class SysUserRoleImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements SysUserRoleService {

    @Override
    public List<String> listRoleIdsByUserId(Long userId) {
        return super.listObjs(new LambdaQueryWrapper<SysUserRole>()
                .select(SysUserRole::getRoleId)
                .eq(SysUserRole::getUserId, userId), Objects::toString);
    }

    @Override
    @Transactional
    public void insertUserRoleBatch(Long userId, List<Long> roleIds) {
        if (CollectionUtils.isEmpty(roleIds) || userId == null) {
            return;
        }
        List<SysUserRole> saveBatchList = new ArrayList<>();
        for (Long item : roleIds) {
            Assert.isFalse(SysRole.isAdmin(item), "系统内置角色，不能配置");

            SysUserRole sysUserRole = new SysUserRole();
            sysUserRole.setRoleId(item);
            sysUserRole.setUserId(userId);

            saveBatchList.add(sysUserRole);
        }
        super.saveBatch(saveBatchList);
    }

    @Override
    public void deleteByUserId(Long userId) {
        super.remove(new LambdaQueryWrapper<SysUserRole>().eq(SysUserRole::getUserId, userId));
    }
}
