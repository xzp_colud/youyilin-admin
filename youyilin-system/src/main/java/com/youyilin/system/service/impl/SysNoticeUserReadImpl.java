package com.youyilin.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.exception.Assert;
import com.youyilin.common.utils.SecurityUtil;
import com.youyilin.system.mapper.SysNoticeUserReadMapper;
import com.youyilin.system.entity.SysNoticeUserRead;
import com.youyilin.system.service.SysNoticeUserReadService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 通知读取 Impl
 */
@Service
public class SysNoticeUserReadImpl extends ServiceImpl<SysNoticeUserReadMapper, SysNoticeUserRead> implements SysNoticeUserReadService {

    @Override
    public Integer countNoReadNumByUserId(Long userId) {
        long noReadNum = super.count(new LambdaQueryWrapper<SysNoticeUserRead>()
                .eq(SysNoticeUserRead::getUserId, userId)
                .eq(SysNoticeUserRead::getStatus, SysNoticeUserRead.Status.NO_READ.name()));
        return (int) noReadNum;
    }

    @Override
    public List<SysNoticeUserRead> listNoReadByUserId(Long userId) {
        return super.list(new LambdaQueryWrapper<SysNoticeUserRead>()
                .eq(SysNoticeUserRead::getStatus, SysNoticeUserRead.Status.NO_READ.name())
                .eq(SysNoticeUserRead::getUserId, userId));
    }

    @Override
    public SysNoticeUserRead validateNoticeUserRead(Long id) {
        SysNoticeUserRead noticeUserRead = super.getById(id);
        Assert.notNull(noticeUserRead, "通知不存在");

        return noticeUserRead;
    }

    @Override
    public void updateRead(Long id) {
        Long userId = SecurityUtil.getUserId();
        super.update(new LambdaUpdateWrapper<SysNoticeUserRead>()
                .set(SysNoticeUserRead::getStatus, SysNoticeUserRead.Status.READ.name())
                .set(SysNoticeUserRead::getReadDate, new Date())
                .set(SysNoticeUserRead::getReader, userId)
                .eq(SysNoticeUserRead::getId, id));
    }

    @Override
    public void updateCancel(List<Long> ids) {
        Long userId = SecurityUtil.getUserId();
        List<String> statusList = new ArrayList<>();
        statusList.add(SysNoticeUserRead.Status.NO_READ.name());
        statusList.add(SysNoticeUserRead.Status.READ.name());
        super.update(new LambdaUpdateWrapper<SysNoticeUserRead>()
                .set(SysNoticeUserRead::getStatus, SysNoticeUserRead.Status.CANCEL.name())
                .set(SysNoticeUserRead::getCancelDate, new Date())
                .set(SysNoticeUserRead::getCanceler, userId)
                .in(SysNoticeUserRead::getStatus, statusList)
                .in(SysNoticeUserRead::getId, ids));
    }

    @Override
    public void updateCancel(Long noticeId) {
        Long userId = SecurityUtil.getUserId();
        List<String> statusList = new ArrayList<>();
        statusList.add(SysNoticeUserRead.Status.NO_READ.name());
        statusList.add(SysNoticeUserRead.Status.READ.name());
        super.update(new LambdaUpdateWrapper<SysNoticeUserRead>()
                .set(SysNoticeUserRead::getStatus, SysNoticeUserRead.Status.CANCEL.name())
                .set(SysNoticeUserRead::getCancelDate, new Date())
                .set(SysNoticeUserRead::getCanceler, userId)
                .in(SysNoticeUserRead::getStatus, statusList)
                .eq(SysNoticeUserRead::getNoticeId, noticeId));
    }
}
