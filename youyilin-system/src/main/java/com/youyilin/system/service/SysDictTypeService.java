package com.youyilin.system.service;

import com.youyilin.system.entity.SysDictType;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 字典类型 Service
 */
public interface SysDictTypeService extends IService<SysDictType> {

    /**
     * 根据类型查询
     *
     * @param dictType 字典类型
     * @return SysDictType
     */
    SysDictType getByDictType(String dictType);

    /**
     * 验证字典类型唯一性
     */
    void validateDictTypeUnique(SysDictType en);

    /**
     * 验证是否存在
     */
    SysDictType validateDictType(Long id);

    /**
     * 验证是否已经禁用
     */
    void validateDictTypeDisabled(String dictType);
}
