package com.youyilin.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.system.entity.SysConfigData;

import java.util.List;

/**
 * 系统配置项 Service
 */
public interface SysConfigDataService extends IService<SysConfigData> {

    /**
     * 根据配置ID及配置编码查询
     *
     * @param configId 配置ID
     * @param code     配置编码
     * @return Entity
     */
    SysConfigData getByConfigIdAndCode(Long configId, String code);

    /**
     * 根据配置ID查询
     *
     * @param configId 配置ID
     * @return ArrayList
     */
    List<SysConfigData> listByConfigId(Long configId);

    /**
     * 根据配置ID查询
     *
     * @param configIds 配置ID
     * @return ArrayList
     */
    List<SysConfigData> listByConfigIds(List<Long> configIds);

    /**
     * 所有
     */
    List<SysConfigData> listAll();

    /**
     * 验证参数
     */
    void validateDataOptions(SysConfigData en);

    /**
     * 配置编码唯一性
     */
    void validateCodeUnique(SysConfigData en);

    /**
     * 验证是否存在
     */
    SysConfigData validateConfigData(Long id);

    /**
     * 更新配置
     */
    void updateData(Long configId, List<SysConfigData> configData);
}
