package com.youyilin.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.system.mapper.SysFileMapper;
import com.youyilin.system.entity.SysFile;
import com.youyilin.system.service.SysFileService;
import org.springframework.stereotype.Service;

/**
 * 系统上传文件 Impl
 */
@Service
public class SysFileImpl extends ServiceImpl<SysFileMapper, SysFile> implements SysFileService {

}
