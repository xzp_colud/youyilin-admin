package com.youyilin.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.constants.CommonConstants;
import com.youyilin.common.enums.DelFlagEnum;
import com.youyilin.common.enums.StatusEnum;
import com.youyilin.common.exception.Assert;
import com.youyilin.common.utils.DateUtils;
import com.youyilin.common.utils.ObjectUniqueUtil;
import com.youyilin.system.mapper.SysNoticeMapper;
import com.youyilin.system.entity.SysNotice;
import com.youyilin.system.service.SysNoticeService;
import org.springframework.stereotype.Service;

/**
 * 通知 Impl
 */
@Service
public class SysNoticeImpl extends ServiceImpl<SysNoticeMapper, SysNotice> implements SysNoticeService {

    @Override
    public SysNotice getByTitle(String title) {
        return super.getOne(new LambdaQueryWrapper<SysNotice>()
                .eq(SysNotice::getTitle, title));
    }

    @Override
    public SysNotice getUpdated() {
        return super.getOne(new LambdaQueryWrapper<SysNotice>()
                .eq(SysNotice::getStatus, StatusEnum.NORMAL.getCode())
                .eq(SysNotice::getType, SysNotice.NoticeType.ANNOUNCE.name())
                .orderByDesc(SysNotice::getId)
                .last(CommonConstants.LIMIT_ONE));
    }

    @Override
    public void disabledNotice(Long id) {
        super.update(new LambdaUpdateWrapper<SysNotice>()
                .set(SysNotice::getStatus, StatusEnum.DISABLED.getCode())
                .set(SysNotice::getDelFlag, DelFlagEnum.DELETE.getCode())
                .eq(SysNotice::getId, id));
    }

    @Override
    public Long saveErrorNotice(String errorMsg) {
        SysNotice en = new SysNotice();
        en.setTitle("API错误 " + DateUtils.getTime());
        en.setContent(errorMsg);
        en.setRemark("API错误");
        en.setStatus(StatusEnum.NORMAL.getCode());
        en.setType(SysNotice.NoticeType.NOTICE.name());
        en.setSendNum(0);
        en.setReadNum(0);
        super.save(en);

        return en.getId();
    }

    @Override
    public void updateAnnounceDisabled() {
        super.update(new LambdaUpdateWrapper<SysNotice>()
                .set(SysNotice::getStatus, StatusEnum.DISABLED.getCode())
                .eq(SysNotice::getType, SysNotice.NoticeType.ANNOUNCE.name()));
    }

    @Override
    public void updateSendReadNum(Long noticeId, Integer sendNum, Integer readNum) {
        if (sendNum == null) sendNum = 0;
        if (readNum == null) readNum = 0;
        SysNotice sn = super.getById(noticeId);
        if (sn == null) return;
        sendNum = sn.getSendNum() + sendNum;
        readNum = sn.getReadNum() + readNum;
        super.update(new LambdaUpdateWrapper<SysNotice>()
                .set(SysNotice::getSendNum, sendNum)
                .set(SysNotice::getReadNum, readNum)
                .eq(SysNotice::getId, noticeId));
    }

    @Override
    public void validateTitleUnique(SysNotice en) {
        SysNotice titleUnique = this.getByTitle(en.getTitle());
        Assert.isTrue(ObjectUniqueUtil.validateObjectUnique(en, titleUnique), "标题已存在");
    }

    @Override
    public SysNotice validateNotice(Long id) {
        SysNotice notice = super.getById(id);
        Assert.notNull(notice, "通知信息不存在");

        return notice;
    }

    @Override
    public SysNotice validateStatus(Long id) {
        SysNotice notice = this.validateNotice(id);
        boolean statusFlag = notice.getStatus() != null && notice.getStatus().equals(StatusEnum.NORMAL.getCode());
        Assert.isTrue(statusFlag, "通知已禁用");

        return notice;
    }
}
