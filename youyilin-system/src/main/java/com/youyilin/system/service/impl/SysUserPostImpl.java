package com.youyilin.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.exception.Assert;
import com.youyilin.system.entity.SysUserPost;
import com.youyilin.system.mapper.SysUserPostMapper;
import com.youyilin.system.service.SysUserPostService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户所属岗位 Impl
 */
@Service
public class SysUserPostImpl extends ServiceImpl<SysUserPostMapper, SysUserPost> implements SysUserPostService {

    @Override
    public int countByPostId(Long postId) {
        long num = this.count(new LambdaQueryWrapper<SysUserPost>().eq(SysUserPost::getPostId, postId));
        return (int) num;
    }

    @Override
    public List<String> listPostIdsByUserId(Long userId) {
        return this.listObjs(new LambdaQueryWrapper<SysUserPost>()
                .select(SysUserPost::getPostId)
                .eq(SysUserPost::getUserId, userId), Object::toString);
    }

    @Override
    public List<SysUserPost> listByPostIds(List<Long> postIds) {
        return this.list(new LambdaQueryWrapper<SysUserPost>().in(SysUserPost::getPostId, postIds));
    }

    @Override
    public void insertUserPostBatch(Long userId, List<Long> postIds) {
        if (CollectionUtils.isEmpty(postIds) || userId == null) {
            return;
        }

        List<SysUserPost> saveBatchList = new ArrayList<>();
        for (Long item : postIds) {
            SysUserPost userPost = new SysUserPost();
            userPost.setUserId(userId);
            userPost.setPostId(item);

            saveBatchList.add(userPost);
        }
        super.saveBatch(saveBatchList);
    }

    @Override
    public void deleteByUserId(Long userId) {
        super.remove(new LambdaQueryWrapper<SysUserPost>().eq(SysUserPost::getUserId, userId));
    }

    @Override
    public void checkNoUserByPostId(Long postId) {
        boolean noUserFlag = this.countByPostId(postId) == 0;
        Assert.isTrue(noUserFlag, "异常：存在所属用户");
    }
}
