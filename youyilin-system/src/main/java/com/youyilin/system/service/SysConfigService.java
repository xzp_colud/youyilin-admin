package com.youyilin.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.system.entity.SysConfig;

import java.util.List;

/**
 * 系统配置 Service
 */
public interface SysConfigService extends IService<SysConfig> {

    /**
     * 根据配置名称查询
     *
     * @param name 配置名称
     * @return Entity
     */
    SysConfig getByName(String name);

    /**
     * 根据配置类型查询
     *
     * @param type 配置类型
     * @return Entity
     */
    List<SysConfig> getByType(Integer type);

    /**
     * 名称唯一性
     */
    void validateNameUnique(SysConfig en);

    /**
     * 类型唯一性
     */
    void validateTypeUnique(SysConfig en);

    /**
     * 验证是否存在
     */
    SysConfig validateConfig(Long id);
}
