package com.youyilin.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.system.mapper.SysErrorLogMapper;
import com.youyilin.system.entity.SysErrorLog;
import com.youyilin.system.service.SysErrorLogService;
import org.springframework.stereotype.Service;

/**
 * 异常日志 Impl
 */
@Service
public class SysErrorLogImpl extends ServiceImpl<SysErrorLogMapper, SysErrorLog> implements SysErrorLogService {

}
