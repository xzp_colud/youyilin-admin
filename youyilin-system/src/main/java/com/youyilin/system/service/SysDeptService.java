package com.youyilin.system.service;

import com.youyilin.system.entity.SysDept;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 部门 Service
 */
public interface SysDeptService extends IService<SysDept> {

    /**
     * 根据上级ID查询
     *
     * @param pidId 上级ID
     * @return ArrayList
     */
    List<SysDept> listByPidId(Long pidId);

    /**
     * 根据上级部门及部门名称查询
     *
     * @param pidId    上级部门
     * @param deptName 部门名称
     * @return SysDept
     */
    SysDept getByPidIdAndDeptName(Long pidId, String deptName);

    /**
     * 根据部门编码查询
     *
     * @param deptCode 部门编码
     * @return SysDept
     */
    SysDept getByDeptCode(String deptCode);

    /**
     * 验证部门名称唯一
     */
    void validateDeptNameUnique(SysDept en);

    /**
     * 验证部门编码唯一
     */
    void validateDeptCodeUnique(SysDept en);

    /**
     * 验证部门
     */
    SysDept validateDept(Long deptId);

    /**
     * 验证部门
     */
    SysDept validateDeptStatus(Long deptId);
}
