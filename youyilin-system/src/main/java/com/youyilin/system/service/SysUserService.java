package com.youyilin.system.service;

import com.youyilin.system.entity.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Date;

/**
 * 用户 Service
 */
public interface SysUserService extends IService<SysUser> {

    /**
     * 根据用户名称查询
     *
     * @param userName 用户名称
     * @return SysUser
     */
    SysUser getByUserName(String userName);

    /**
     * 根据手机号查询
     *
     * @param phone 手机号
     * @return SysUser
     */
    SysUser getByPhone(String phone);

    /**
     * 根据邮箱查询
     *
     * @param email 邮箱
     * @return SysUser
     */
    SysUser getByEmail(String email);

    /**
     * 更新自己的信息
     */
    void updateUserByMe(SysUser sysUser);

    /**
     * 更新登录信息
     */
    void updateLoginInfo(String userName, String loginIp, String loginLocation, Date loginDate);

    /**
     * 重置密码
     */
    void updatePassword(String userName);

    /**
     * 获取重置的密码
     */
    String getResetPassword();

    /**
     * 初始化密码
     */
    String refreshPassword(String password);

    /**
     * 验证用户名唯一
     */
    void validateUserNameUnique(SysUser en);

    /**
     * 验证手机号唯一
     */
    void validatePhoneUnique(SysUser en);

    /**
     * 验证邮箱唯一
     */
    void validateEmailUnique(SysUser en);

    /**
     * 验证用户
     *
     * @param id 主键ID
     * @return SysUser
     */
    SysUser validateUser(Long id);

    /**
     * 验证用户
     *
     * @param userName 用户名称
     * @return SysUser
     */
    SysUser validateUser(String userName);

    /**
     * 验证用户状态
     *
     * @param userName 用户名称
     * @return SysUser
     */
    SysUser validateUserStatus(String userName);
}
