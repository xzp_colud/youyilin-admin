package com.youyilin.system.convert;

import com.youyilin.common.ConverterList;
import com.youyilin.common.entity.TreeNodes;
import com.youyilin.system.entity.SysDept;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * SysDept - TreeNodes 转换器
 */
public class SysDeptToTreeNodesConvert implements ConverterList<SysDept, TreeNodes> {

    @Override
    public List<TreeNodes> convert(List<SysDept> source) {
        if (CollectionUtils.isEmpty(source)) {
            return new ArrayList<>();
        }
        List<TreeNodes> dataList = new ArrayList<>();
        for (SysDept item : source) {
            TreeNodes data = new TreeNodes();
            data.setId(item.getId());
            data.setTitle(item.getDeptName());
            data.setStatus(item.getStatus());
            data.setPidId(item.getPidId());

            dataList.add(data);
        }
        return dataList;
    }
}
