package com.youyilin.system.convert;

import com.youyilin.common.ConverterList;
import com.youyilin.system.entity.SysMenu;
import com.youyilin.system.entity.SysRoleMenu;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * SysMenu - SysRoleMenu 转换器
 */
public class SysMenuToSysRoleMenuConvert implements ConverterList<SysMenu, SysRoleMenu> {

    @Override
    public List<SysRoleMenu> convert(List<SysMenu> sourceList) {
        if (CollectionUtils.isEmpty(sourceList)) {
            return new ArrayList<>();
        }
        List<SysRoleMenu> resultList = new ArrayList<>();
        for (SysMenu source : sourceList) {
            SysRoleMenu data = new SysRoleMenu();
            data.setMenuId(source.getId());
            data.setMenuType(source.getType());

            resultList.add(data);
        }
        return resultList;
    }
}
