package com.youyilin.system.convert;

import com.youyilin.common.ConverterList;
import com.youyilin.common.entity.TreeNodes;
import com.youyilin.common.enums.StatusEnum;
import com.youyilin.system.entity.SysMenu;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * SysMenu - TreeNodes 转换器
 */
public class SysMenuToTreeNodesConvert implements ConverterList<SysMenu, TreeNodes> {

    @Override
    public List<TreeNodes> convert(List<SysMenu> source) {
        if (CollectionUtils.isEmpty(source)) {
            return new ArrayList<>();
        }
        List<TreeNodes> dataList = new ArrayList<>();
        for (SysMenu item : source) {
            TreeNodes data = new TreeNodes();
            data.setTitle(item.getMenuName());
            data.setStatus(item.getStatus());
            data.setPidId(item.getPidId());
            data.setIcon(item.getIcon());
            data.setId(item.getId());
            data.setDisabled(item.getStatus() == StatusEnum.DISABLED.getCode());

            dataList.add(data);
        }
        return dataList;
    }
}
