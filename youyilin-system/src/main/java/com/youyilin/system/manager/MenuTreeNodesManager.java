package com.youyilin.system.manager;

import com.youyilin.common.entity.TreeNodes;
import com.youyilin.common.entity.TreeNodesUtils;
import com.youyilin.system.convert.SysMenuToTreeNodesConvert;
import com.youyilin.system.entity.SysMenu;
import com.youyilin.system.service.SysMenuService;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 菜单结构构建器
 */
public class MenuTreeNodesManager {

    private final SysMenuService sysMenuService;

    public MenuTreeNodesManager(SysMenuService sysMenuService) {
        this.sysMenuService = sysMenuService;
    }

    /**
     * 执行构建
     *
     * @return ArrayList
     */
    public List<TreeNodes> generate() {
        // 获取菜单列表
        List<SysMenu> menuList = sysMenuService.listBySort();
        // 如果菜单列表为空，则返回一个空列表
        if (CollectionUtils.isEmpty(menuList)) {
            return new ArrayList<>();
        }
        // 数据转换，将SysMenu转换为TreeNodes
        List<TreeNodes> treeNodesList = new SysMenuToTreeNodesConvert().convert(menuList);
        // 将TreeNodes列表按照pidId分组，得到父子关系的Map
        Map<Long, List<TreeNodes>> treeNodesMap = treeNodesList.stream().collect(Collectors.groupingBy(TreeNodes::getPidId));

        // 获取父节点列表
        List<TreeNodes> parentList = treeNodesMap.get(0L);
        // 如果父节点列表为空，则返回一个空列表
        if (CollectionUtils.isEmpty(parentList)) {
            return new ArrayList<>();
        }
        // 根据父子关系生成树结构
        TreeNodesUtils.generate(parentList, treeNodesMap);
        // 返回父节点列表
        return parentList;
    }
}
