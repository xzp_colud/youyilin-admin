package com.youyilin.system.manager;

import com.youyilin.common.entity.TreeNodes;
import com.youyilin.common.entity.TreeNodesUtils;
import com.youyilin.system.convert.SysDeptToTreeNodesConvert;
import com.youyilin.system.entity.SysDept;
import com.youyilin.system.service.SysDeptService;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 部门结构构建器
 */
public class DeptTreeNodesManager {

    private final SysDeptService sysDeptService;

    public DeptTreeNodesManager(SysDeptService sysDeptService) {
        this.sysDeptService = sysDeptService;
    }

    /**
     * 执行构建
     *
     * @return ArrayList
     */
    public List<TreeNodes> generate() {
        // 获取部门列表
        List<SysDept> sysDeptList = sysDeptService.list();
        // 如果部门列表为空, 返回空列表
        if (CollectionUtils.isEmpty(sysDeptList)) {
            return new ArrayList<>();
        }
        // 数据转换
        List<TreeNodes> treeNodesList = new SysDeptToTreeNodesConvert().convert(sysDeptList);
        // 按所属上级分组
        Map<Long, List<TreeNodes>> treeNodesMap = treeNodesList.stream().collect(Collectors.groupingBy(TreeNodes::getPidId));
        // 顶级节点
        List<TreeNodes> parentTreeNodes = treeNodesMap.get(0L);
        // 子节点
        TreeNodesUtils.generate(parentTreeNodes, treeNodesMap);
        return parentTreeNodes;
    }
}
