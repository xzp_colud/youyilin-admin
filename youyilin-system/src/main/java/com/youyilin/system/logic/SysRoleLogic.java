package com.youyilin.system.logic;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.system.dto.role.RoleFormDTO;
import com.youyilin.system.dto.role.RoleQueryDTO;
import com.youyilin.system.vo.role.RoleEditVO;
import com.youyilin.system.vo.role.RolePageVO;

public interface SysRoleLogic {

    /**
     * 列表
     */
    Pager<RolePageVO> getPageList(Page<RoleQueryDTO> page);

    /**
     * 编辑查询
     *
     * @param id 主键ID
     * @return SysRoleVO
     */
    RoleEditVO queryEdit(Long id);

    /**
     * 保存
     */
    void saveRole(RoleFormDTO dto);
}
