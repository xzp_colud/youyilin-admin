package com.youyilin.system.logic.impl;

import com.youyilin.common.entity.TreeNodes;
import com.youyilin.common.exception.Assert;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.common.utils.SecurityUtil;
import com.youyilin.system.dto.menu.MenuFormDTO;
import com.youyilin.system.entity.SysMenu;
import com.youyilin.system.entity.SysUser;
import com.youyilin.system.enums.MenuTypeEnum;
import com.youyilin.system.logic.SysMenuLogic;
import com.youyilin.system.manager.MenuTreeNodesManager;
import com.youyilin.system.mapper.SysMenuMapper;
import com.youyilin.system.service.SysMenuService;
import com.youyilin.system.vo.menu.MenuAddVO;
import com.youyilin.system.vo.menu.MenuEditVO;
import com.youyilin.system.vo.menu.MenuPageVO;
import com.youyilin.system.vo.menu.MenuVO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class SysMenuLogicImpl implements SysMenuLogic {

    private final SysMenuMapper sysMenuMapper;
    private final SysMenuService sysMenuService;

    public SysMenuLogicImpl(SysMenuMapper sysMenuMapper, SysMenuService sysMenuService) {
        this.sysMenuMapper = sysMenuMapper;
        this.sysMenuService = sysMenuService;
    }

    @Override
    public List<MenuPageVO> getPageList() {
        // 获取所有的菜单列表
        List<SysMenu> dataList = sysMenuService.listBySort();
        // 如果菜单列表为空，返回一个空的列表
        if (CollectionUtils.isEmpty(dataList)) {
            return new ArrayList<>();
        }
        // 将菜单列表转换为树形结构的列表
        List<MenuPageVO> dataTreeList = BeanHelper.map(dataList, MenuPageVO.class);
        // 根据父级ID将树形结构的列表分组
        Map<Long, List<MenuPageVO>> dataTreeMap = dataTreeList.stream().collect(Collectors.groupingBy(MenuPageVO::getPidId));
        // 获取父级ID为0的菜单列表
        List<MenuPageVO> pidList = dataTreeMap.get(0L);
        // 递归创建子菜单
        this.createChildren(pidList, dataTreeMap);
        // 返回菜单列表
        return pidList;
    }

    @Override
    public MenuAddVO queryAdd() {
        return new MenuAddVO().setMenuList(this.selectTree());
    }

    @Override
    public MenuEditVO queryEdit(Long id) {
        SysMenu menu = sysMenuService.getById(id);
        if (menu == null) {
            return new MenuEditVO();
        }
        MenuEditVO editVO = MenuEditVO.convertByEntity(menu);
        editVO.setMenuList(this.selectTree());

        return editVO;
    }

    /**
     * 创建子菜单列表
     *
     * @param pidList     包含菜单树节点的列表
     * @param dataTreeMap 菜单树节点映射表
     */
    private void createChildren(List<MenuPageVO> pidList, Map<Long, List<MenuPageVO>> dataTreeMap) {
        // 遍历 pidList 中的每个元素
        for (MenuPageVO item : pidList) {
            // 获取当前元素的子节点列表
            List<MenuPageVO> children = dataTreeMap.get(item.getId());
            // 如果子节点列表为空
            if (CollectionUtils.isEmpty(children)) {
                // 将当前元素的子节点置为 null
                item.setChildren(null);
                continue;
            }
            // 将当前元素的子节点设置为子节点列表
            item.setChildren(children);
            item.setPidId(null);
            // 递归调用 createChildren 方法，处理子节点列表中的元素
            this.createChildren(children, dataTreeMap);
        }
    }

    public TreeNodes selectTree() {
        TreeNodes treeNode = new TreeNodes();
        treeNode.setId(0L);
        treeNode.setTitle("主目录");
        treeNode.setChildren(new MenuTreeNodesManager(sysMenuService).generate());

        return treeNode;
    }

    @Override
    public List<MenuVO> listLoginRouter() {
        // 获取当前登录用户ID
        Long userId = SecurityUtil.getUserIdNull();
        // 如果用户ID为空，返回空列表
        if (userId == null) {
            return new ArrayList<>();
        }

        // 如果用户是管理员，获取管理员列表
        if (SysUser.isAdmin(userId)) {
            return sysMenuMapper.getAdminRouters();
        }
        // 获取用户列表
        return sysMenuMapper.getRouters(userId);
    }

    @Override
    public List<String> listLoginPerms(Long userId) {
        // 如果用户ID为空，返回一个空的列表
        if (userId == null) {
            return new ArrayList<>();
        }
        // 如果用户是管理员，返回管理员列表
        if (SysUser.isAdmin(userId)) {
            return sysMenuMapper.getAdminPerms();
        }
        // 返回用户列表
        return sysMenuMapper.getPerms(userId);
    }

    @Override
    public void saveMenu(MenuFormDTO dto) {
        // 将DTO转换为实体类
        SysMenu en = dto.convertToEntity();
        // 验证参数
        this.validateParams(en);
        // 验证编码唯一性
        sysMenuService.validateMenuCodeUnique(en);
        // 验证全路径唯一性
        sysMenuService.validatePathUnique(en);
        // 验证路由名称唯一性
        sysMenuService.validateNameUnique(en);
        // 验证权限标识唯一性
        sysMenuService.validatePermsUnique(en);
        // 验证是否能添加子节点
        sysMenuService.validatePidId(en);
        // 验证是否能移动节点
        sysMenuService.validateMoveNodes(en);
        if (en.getId() == null) {
            // 如果ID为空，表示是新增菜单，则调用保存方法
            sysMenuService.save(en);
        } else {
            // 如果ID不为空，表示是更新菜单，则先验证菜单是否存在，再调用更新方法
            sysMenuService.validateMenu(en.getId());
            sysMenuService.updateById(en);
        }
    }

    private void validateParams(SysMenu en) {
        if (en.getType() == MenuTypeEnum.CATALOG.getCode()) {
            if (StringUtils.isNotBlank(en.getPath())) {
                Assert.hasLength(en.getComponent(), "路由组件路径不能为空");
                Assert.hasLength(en.getName(), "路由路径名称不能为空");
            }
            en.setPerms(null);
        } else if (en.getType() == MenuTypeEnum.MENU.getCode()) {
            Assert.hasLength(en.getPath(), "路由路径不能为空");
            Assert.hasLength(en.getComponent(), "路由组件路径不能为空");
            Assert.hasLength(en.getName(), "路由路径名称不能为空");
            en.setPerms(null);
        } else if (en.getType() == MenuTypeEnum.BUTTON.getCode()) {
            Assert.hasLength(en.getPerms(), "权限标识不能为空");
            en.setComponent(null);
            en.setPath(null);
            en.setPath(null);
            en.setName(null);
        }
    }

    @Override
    public void delMenu(Long id) {
        sysMenuService.validateMenu(id);
        sysMenuService.removeById(id);
    }
}
