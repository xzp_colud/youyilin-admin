package com.youyilin.system.logic.impl;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.exception.Assert;
import com.youyilin.common.utils.CharterUtil;
import com.youyilin.system.dto.dict.DictTypeFormDTO;
import com.youyilin.system.dto.dict.DictTypeQueryDTO;
import com.youyilin.system.entity.SysDictType;
import com.youyilin.system.logic.SysDictTypeLogic;
import com.youyilin.system.mapper.SysDictTypeMapper;
import com.youyilin.system.service.SysDictTypeService;
import com.youyilin.system.vo.dict.DictTypeEditVO;
import com.youyilin.system.vo.dict.DictTypePageVO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SysDictTypeLogicImpl implements SysDictTypeLogic {

    private final SysDictTypeMapper sysDictTypeMapper;
    private final SysDictTypeService sysDictTypeService;

    public SysDictTypeLogicImpl(SysDictTypeMapper sysDictTypeMapper, SysDictTypeService sysDictTypeService) {
        this.sysDictTypeMapper = sysDictTypeMapper;
        this.sysDictTypeService = sysDictTypeService;
    }

    @Override
    public Pager<DictTypePageVO> getPageList(Page<DictTypeQueryDTO> page) {
        Integer total = sysDictTypeMapper.getTotal(page);
        if (total == 0) {
            return new Pager<>(0, new ArrayList<>());
        }
        List<DictTypePageVO> list = sysDictTypeMapper.getPage(page);
        return new Pager<>(total, list);
    }

    @Override
    public DictTypeEditVO queryEdit(Long id) {
        SysDictType sysDictType = sysDictTypeService.getById(id);
        return sysDictType == null ? null : DictTypeEditVO.convertByEntity(sysDictType);
    }

    @Override
    public void saveDictType(DictTypeFormDTO dto) {
        // 将DTO转换为实体类
        SysDictType en = dto.convertToEntity();
        // 验证字典类型的字符为英文字符
        Assert.isTrue(CharterUtil.characterString(en.getDictType()), "类型只能输入英文");

        // 验证字典类型唯一
        sysDictTypeService.validateDictTypeUnique(en);

        // 保存或更新字典类型
        if (en.getId() == null) {
            sysDictTypeService.save(en);
        } else {
            // 验证旧的字典类型是否存在
            SysDictType oldDictType = sysDictTypeService.validateDictType(en.getId());
            // 判断旧的字典类型和新的字典类型是否相同
            boolean equalsFlag = oldDictType.getDictType().equals(en.getDictType());
            Assert.isTrue(equalsFlag, "禁止修改字典类型");

            sysDictTypeService.updateById(en);
        }
    }
}
