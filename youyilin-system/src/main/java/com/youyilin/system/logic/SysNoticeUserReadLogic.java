package com.youyilin.system.logic;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.system.dto.notice.SysNoticeSendFormDTO;
import com.youyilin.system.vo.notice.SysNoticeUserReadSendVO;
import com.youyilin.system.vo.notice.SysNoticeUserReadVO;

import java.util.List;

public interface SysNoticeUserReadLogic {

    /**
     * 列表
     */
    Pager<SysNoticeUserReadVO> getPageList(Page<SysNoticeUserReadVO> page);

    /**
     * 统计当前登录用户未读通知数量
     */
    Integer countLoginNoReadNum();

    /**
     * 发送通知时选择用户列表
     */
    List<SysNoticeUserReadSendVO> listUser(SysNoticeUserReadSendVO s);

    /**
     * 发送
     *
     * @param dto 表单
     */
    void saveUserRead(SysNoticeSendFormDTO dto);

    /**
     * 发送
     *
     * @param noticeId 通知ID
     * @param userIds  用户ID
     */
    void saveUserRead(Long noticeId, List<Long> userIds);

    /**
     * 批量取消已发送的通知
     *
     * @param ids 已发通知ID
     */
    void updateCancelByIds(List<Long> ids);

    /**
     * 取消单个通知已发送的
     *
     * @param noticeId 通知ID
     */
    void updateCancelByNoticeId(Long noticeId);

    /**
     * 单条通知已读
     *
     * @param id 已发通知ID
     */
    void updateLoginRead(Long id);

    /**
     * 全部已读
     */
    void updateLoginRead();
}
