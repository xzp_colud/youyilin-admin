package com.youyilin.system.logic;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.system.dto.config.SysConfigInsertFormDTO;
import com.youyilin.system.dto.config.SysConfigUpdateFormDTO;
import com.youyilin.system.vo.config.SysConfigVO;

import java.util.List;

public interface SysConfigLogic {

    /**
     * 启动初始化
     */
    void initRefreshConfig() throws IllegalAccessException;

    /**
     * 刷新配置
     */
    void refreshConfig(Long id) throws IllegalAccessException;

    /**
     * 列表
     */
    Pager<SysConfigVO> getPageList(Page<SysConfigVO> page);

    /**
     * 查询
     *
     * @return SysConfigVO
     */
    SysConfigVO getById(Long id);

    /**
     * 所有配置
     *
     * @return ArrayList
     */
    List<SysConfigVO> listAll();

    /**
     * 保存
     */
    void saveConfig(SysConfigInsertFormDTO dto);

    /**
     * 更新配置
     */
    void updateConfig(SysConfigUpdateFormDTO dto);
}
