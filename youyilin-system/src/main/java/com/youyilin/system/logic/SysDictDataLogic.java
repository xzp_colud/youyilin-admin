package com.youyilin.system.logic;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.system.dto.dict.DictDataFormDTO;
import com.youyilin.system.dto.dict.DictDataQueryDTO;
import com.youyilin.system.vo.dict.DictDataEditVO;
import com.youyilin.system.vo.dict.DictDataPageVO;
import com.youyilin.system.vo.dict.DictDataVO;

import java.util.List;
import java.util.Map;

public interface SysDictDataLogic {

    /**
     * 列表
     */
    Pager<DictDataPageVO> getPageList(Page<DictDataQueryDTO> page);

    /**
     * 查询
     *
     * @param id 主键ID
     * @return SysDictDataVO
     */
    DictDataEditVO queryEdit(Long id);

    /**
     * 按字典类型查询
     *
     * @param dictTypeList 字典类型
     * @return ArrayList
     */
    Map<String, List<DictDataVO>> mapByDictTypeList(List<String> dictTypeList);

    /**
     * 保存
     */
    void saveDictData(DictDataFormDTO dto);

    /**
     * 删除
     */
    void delDictData(Long id);
}
