package com.youyilin.system.logic;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.system.dto.log.LoginLogQueryDTO;
import com.youyilin.system.vo.log.LoginLogPageVO;

public interface SysLoginLogLogic {

    Pager<LoginLogPageVO> getPageList(Page<LoginLogQueryDTO> page);

    void delLoginLog(Long id);
}
