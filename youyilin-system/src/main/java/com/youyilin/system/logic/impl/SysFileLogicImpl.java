package com.youyilin.system.logic.impl;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.system.dto.file.FileQueryDTO;
import com.youyilin.system.logic.SysFileLogic;
import com.youyilin.system.mapper.SysFileMapper;
import com.youyilin.system.vo.file.FilePageVO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SysFileLogicImpl implements SysFileLogic {

    private final SysFileMapper sysFileMapper;

    public SysFileLogicImpl(SysFileMapper sysFileMapper) {
        this.sysFileMapper = sysFileMapper;
    }

    @Override
    public Pager<FilePageVO> getPageList(Page<FileQueryDTO> page) {
        Integer total = sysFileMapper.getTotal(page);
        if (total == 0) {
            return new Pager<>(0, new ArrayList<>());
        }
        List<FilePageVO> list = sysFileMapper.getPage(page);
        return new Pager<>(total, list);
    }
}
