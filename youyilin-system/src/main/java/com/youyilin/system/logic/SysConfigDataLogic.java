package com.youyilin.system.logic;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.system.dto.config.SysConfigDataInsertFormDTO;
import com.youyilin.system.dto.config.SysConfigDataPageQueryVO;
import com.youyilin.system.vo.config.SysConfigDataVO;

import java.util.List;

public interface SysConfigDataLogic {

    /**
     * 获取分页列表
     *
     * @param page 分页对象，包含查询条件和分页参数
     * @return 分页列表，包含数据对象和分页信息
     */
    Pager<SysConfigDataVO> getPageList(Page<SysConfigDataPageQueryVO> page);

    /**
     * 根据id获取SysConfigDataVO对象
     *
     * @param id 配置项id
     * @return 对应的SysConfigDataVO对象
     */
    SysConfigDataVO getById(Long id);

    /**
     * 根据配置ID查询配置数据列表
     *
     * @param configId 配置ID
     * @return 配置数据列表
     */
    List<SysConfigDataVO> listByConfigId(Long configId);

    /**
     * 保存配置数据
     *
     * @param dto 配置数据插入表单DTO
     */
    void saveConfigData(SysConfigDataInsertFormDTO dto);

    /**
     * 从配置数据库中删除指定id的数据。
     *
     * @param id 待删除数据的id。
     */
    void delConfigData(Long id);
}
