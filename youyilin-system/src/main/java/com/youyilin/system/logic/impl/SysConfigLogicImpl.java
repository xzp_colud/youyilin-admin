package com.youyilin.system.logic.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.config.ConfigTypeEnum;
import com.youyilin.common.exception.ApiException;
import com.youyilin.common.exception.Assert;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.common.utils.SpringUtil;
import com.youyilin.system.logic.SysConfigLogic;
import com.youyilin.system.mapper.SysConfigMapper;
import com.youyilin.system.dto.config.SysConfigInsertFormDTO;
import com.youyilin.system.dto.config.SysConfigUpdateFormDTO;
import com.youyilin.system.entity.SysConfig;
import com.youyilin.system.entity.SysConfigData;
import com.youyilin.system.vo.config.SysConfigVO;
import com.youyilin.system.service.SysConfigDataService;
import com.youyilin.system.service.SysConfigService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class SysConfigLogicImpl implements SysConfigLogic {

    private final SysConfigMapper sysConfigMapper;
    private final SysConfigService sysConfigService;
    private final SysConfigDataService sysConfigDataService;

    public SysConfigLogicImpl(SysConfigMapper sysConfigMapper, SysConfigService sysConfigService, SysConfigDataService sysConfigDataService) {
        this.sysConfigMapper = sysConfigMapper;
        this.sysConfigService = sysConfigService;
        this.sysConfigDataService = sysConfigDataService;
    }

    @Override
    public void initRefreshConfig() throws IllegalAccessException {
        List<Integer> beanCodeList = ConfigTypeEnum.listCodeExcludeOther();
        List<SysConfig> configList = sysConfigService.list(new LambdaQueryWrapper<SysConfig>().in(SysConfig::getType, beanCodeList));
        if (CollectionUtils.isEmpty(configList)) {
            return;
        }
        List<Long> configIds = new ArrayList<>();
        configList.forEach(item -> configIds.add(item.getId()));
        List<SysConfigData> configDataList = sysConfigDataService.listByConfigIds(configIds);

        this.refreshConfig(configList, configDataList);
    }

    @Override
    public void refreshConfig(Long id) throws IllegalAccessException {
        SysConfig sysConfig = sysConfigService.getById(id);
        if (sysConfig == null || sysConfig.getType() == null || sysConfig.getType().equals(ConfigTypeEnum.OTHER.getCode())) {
            return;
        }
        List<SysConfig> configList = new ArrayList<>();
        configList.add(sysConfig);
        List<SysConfigData> configDataList = sysConfigDataService.listByConfigId(id);

        this.refreshConfig(configList, configDataList);
    }

    /**
     * 刷新配置
     */
    private void refreshConfig(List<SysConfig> configList, List<SysConfigData> configDataList) throws IllegalAccessException {
        Map<Long, List<SysConfigData>> configDataMap = configDataList.stream().collect(Collectors.groupingBy(SysConfigData::getConfigId));

        for (SysConfig config : configList) {
            List<SysConfigData> findConfigDataList = configDataMap.get(config.getId());
            if (CollectionUtils.isEmpty(findConfigDataList)) {
                continue;
            }

            String beanName = ConfigTypeEnum.queryBeanByCode(config.getType());
            if (StringUtils.isBlank(beanName)) {
                continue;
            }
            Object o = SpringUtil.getBean(beanName);
            Assert.notNull(o, "Bean is not exist");
            this.refreshConfigFieldValue(o, findConfigDataList);
        }
    }

    /**
     * 刷新配置
     */
    private void refreshConfigFieldValue(Object config, List<SysConfigData> configDataList) throws IllegalAccessException {
        // 将配置数据按照code进行分组
        Map<String, List<SysConfigData>> codeMap = configDataList.stream().collect(Collectors.groupingBy(SysConfigData::getCode));

        // 默认所有属性值都存在
        boolean isAllExist = true;

        // 获取配置对象的所有属性
        Field[] fields = config.getClass().getDeclaredFields();
        for (Field field : fields) {
            // 设置属性可访问
            field.setAccessible(true);
            // 获取属性名称
            String name = field.getName();

            // 获取该属性对应的配置数据列表
            List<SysConfigData> codeDataList = codeMap.get(name);
            if (CollectionUtils.isEmpty(codeDataList) || StringUtils.isBlank(codeDataList.get(0).getValue())) {
                // 如果配置数据列表为空或者配置数据值为空，则属性值不存在
                isAllExist = false;
                break;
            }
        }

        // 属性值全部存在
        if (isAllExist) {
            for (Field field : fields) {
                // 获取属性名称
                String name = field.getName();
                // 获取该属性对应的配置数据值
                String value = codeMap.get(name).get(0).getValue();
                // 将配置数据值设置到配置对象对应的属性上
                field.set(config, value);
            }
        }
    }

    @Override
    public Pager<SysConfigVO> getPageList(Page<SysConfigVO> page) {
        Integer total = sysConfigMapper.getTotal(page);
        List<SysConfigVO> list = sysConfigMapper.getPage(page);
        return new Pager<>(total, list);
    }

    @Override
    public SysConfigVO getById(Long id) {
        SysConfig sysConfig = sysConfigService.getById(id);
        return sysConfig == null ? null : sysConfig.convertToVO();
    }

    @Override
    public List<SysConfigVO> listAll() {
        List<SysConfig> list = sysConfigService.list();
        return BeanHelper.map(list, SysConfigVO.class);
    }

    @Override
    public void saveConfig(SysConfigInsertFormDTO dto) {
        // 将DTO转换为SysConfig实体类
        SysConfig en = dto.convertToEntity();
        // 验证配置名称是否唯一
        sysConfigService.validateNameUnique(en);
        // 如果配置类型不是其他类型，则验证配置类型是否唯一
        if (en.getType() != ConfigTypeEnum.OTHER.getCode()) {
            sysConfigService.validateTypeUnique(en);
        }

        if (en.getId() == null) {
            // 如果配置实体类Id为空，则保存配置实体类
            sysConfigService.save(en);
        } else {
            // 如果配置实体类Id不为空，则验证是否存在旧的配置实体类
            SysConfig old = sysConfigService.validateConfig(en.getId());
            // 判断旧的配置实体类类型是否与新的配置实体类类型相同
            Assert.isTrue(old.getType().equals(en.getType()), "禁止修改配置类型");

            sysConfigService.updateById(en);
        }
    }

    @Override
    @Transactional
    public void updateConfig(SysConfigUpdateFormDTO dto) {
        // 获取配置信息
        SysConfig config = sysConfigService.validateConfig(dto.getConfigId());
        // 验证配置信息
        sysConfigService.validateConfig(config.getId());
        // 保存配置项
        List<SysConfigData> list = BeanHelper.map(dto.getConfigData(), SysConfigData.class);
        sysConfigDataService.updateData(config.getId(), list);

        try {
            // 刷新配置信息至系统缓存
            this.refreshConfig(config.getId());
        } catch (IllegalAccessException e) {
            throw new ApiException("配置更新异常");
        }
    }
}
