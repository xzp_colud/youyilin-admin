package com.youyilin.system.logic.impl;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.config.email.EmailUtil;
import com.youyilin.common.entity.TreeNodes;
import com.youyilin.common.enums.StatusEnum;
import com.youyilin.common.exception.Assert;
import com.youyilin.common.utils.MobileUtil;
import com.youyilin.system.dto.dept.DeptFormDTO;
import com.youyilin.system.dto.dept.DeptQueryDTO;
import com.youyilin.system.entity.SysDept;
import com.youyilin.system.logic.SysDeptLogic;
import com.youyilin.system.manager.DeptTreeNodesManager;
import com.youyilin.system.mapper.SysDeptMapper;
import com.youyilin.system.service.SysDeptService;
import com.youyilin.system.service.SysPostService;
import com.youyilin.system.vo.dept.DeptAddVO;
import com.youyilin.system.vo.dept.DeptEditVO;
import com.youyilin.system.vo.dept.DeptPageVO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class SysDeptLogicImpl implements SysDeptLogic {

    private final SysDeptMapper sysDeptMapper;
    private final SysDeptService sysDeptService;
    private final SysPostService sysPostService;

    public SysDeptLogicImpl(SysDeptMapper sysDeptMapper, SysDeptService sysDeptService, SysPostService sysPostService) {
        this.sysDeptMapper = sysDeptMapper;
        this.sysDeptService = sysDeptService;
        this.sysPostService = sysPostService;
    }

    @Override
    public Pager<DeptPageVO> getPageList(Page<DeptQueryDTO> page) {
        Integer total = sysDeptMapper.getTotal(page);
        if (total == 0) {
            return new Pager<>(0, new ArrayList<>());
        }
        List<DeptPageVO> list = sysDeptMapper.getPage(page);

        return new Pager<>(total, list);
    }

    @Override
    public List<TreeNodes> queryTree() {
        return this.listTree();
    }

    @Override
    public DeptAddVO queryAdd() {
        return new DeptAddVO().setDeptList(this.listTree());
    }

    @Override
    public DeptEditVO queryEdit(Long id) {
        SysDept dbDept = sysDeptService.getById(id);
        if (dbDept == null) {
            return new DeptEditVO();
        }
        DeptEditVO editVO = DeptEditVO.convertByEntity(dbDept);
        editVO.setDeptList(this.listTree());

        return editVO;
    }

    public List<TreeNodes> listTree() {
        DeptTreeNodesManager manager = new DeptTreeNodesManager(sysDeptService);
        return manager.generate();
    }

    @Override
    @Transactional
    public void saveSysDept(DeptFormDTO dto) {
        SysDept en = dto.convertToEntity();
        if (StringUtils.isNotBlank(en.getPhone())) {
            // 手机号
            Assert.isFalse(MobileUtil.isNotMobile(en.getPhone()), "手机号格式不正确");
        }
        if (StringUtils.isNotBlank(en.getEmail())) {
            // 邮箱
            Assert.isFalse(EmailUtil.isNotEmail(en.getEmail()), "邮箱格式不正确");
        }
        // 验证上级部门
        this.validateParent(en);
        // 验证子部门
        this.validateChildrenAndPost(en);
        // 验证部门名称唯一
        sysDeptService.validateDeptNameUnique(en);
        // 验证部门编码唯一
        sysDeptService.validateDeptCodeUnique(en);
        if (en.getId() == null) {
            sysDeptService.save(en);
        } else {
            sysDeptService.validateDept(en.getId());
            sysDeptService.updateById(en);
        }
    }

    @Override
    public void delSysDept(Long id) {
        // 验证部门是否存在
        SysDept oldDept = sysDeptService.validateDept(id);
        // 获取下级部门列表
        List<SysDept> sysDeptChildrenList = sysDeptService.listByPidId(oldDept.getId());
        // 判断下级部门列表是否为空
        boolean noChildrenDeptFlag = CollectionUtils.isEmpty(sysDeptChildrenList);
        // 确保下级部门列表为空，即没有下级部门
        Assert.isTrue(noChildrenDeptFlag, "删除失败，岗位未移除");
        // 删除部门
        sysDeptService.removeById(oldDept.getId());
    }

    /**
     * 验证父级部门信息
     *
     * @param en 部门实体
     */
    private void validateParent(SysDept en) {
        // 获取父级部门ID
        Long pidId = en.getPidId();

        // 如果父级部门ID为空或者为0，则将父级部门ID设置为0，父级部门名称设置为空，并返回
        if (pidId == null || Objects.equals(pidId, 0L)) {
            en.setPidId(0L);
            en.setPidName(null);
            return;
        }

        // 获取当前部门ID
        Long id = en.getId();

        // 通过父级部门ID验证父级部门状态
        SysDept sysDeptParent = sysDeptService.validateDeptStatus(pidId);

        // 判断当前部门ID是否与父级部门ID相等，如果不相等，则说明上级部门不能作为自身
        boolean isNotSelf = id == null || !Objects.equals(id, pidId);
        Assert.isTrue(isNotSelf, "上级部门不能作为自身");

        // 设置父级部门名称
        en.setPidName(sysDeptParent.getDeptName());
    }

    /**
     * 验证下级部门和岗位是否禁用
     *
     * @param en 部门实体
     */
    private void validateChildrenAndPost(SysDept en) {
        // 判断是否需要检查下级部门和当前岗位，如果不需要则直接返回
        boolean isNotCheckChildren = en.getId() == null || en.getStatus().equals(StatusEnum.NORMAL.getCode());
        if (isNotCheckChildren) {
            return;
        }

        // 查询下级部门列表
        List<SysDept> childrenList = sysDeptService.listByPidId(en.getId());
        if (CollectionUtils.isNotEmpty(childrenList)) {
            for (SysDept children : childrenList) {
                // 判断子部门状态是否正常
                boolean childrenStatusFlag = children.getStatus() == null || !children.getStatus().equals(StatusEnum.NORMAL.getCode());
                Assert.isTrue(childrenStatusFlag, "子部门未禁用");

                // 判断是否允许移动到子部门
                boolean notMoveToChildrenFlag = !Objects.equals(en.getPidId(), children.getId());
                Assert.isTrue(notMoveToChildrenFlag, "禁止移入子部门");
                // 设置冗余部门名称
                children.setPidName(en.getDeptName());
            }
            // 批量更新下级部门状态
            sysDeptService.updateBatchById(childrenList);
        }

        // 校验下属岗位是否全部禁用
        boolean allDisabledFlag = sysPostService.validatePostAllDisabledByDeptId(en.getId());
        Assert.isTrue(allDisabledFlag, "下属岗位未禁用");

        // 更新岗位的部门名称（冗余）
        sysPostService.updateDeptNameByDeptId(en.getId(), en.getDeptName());
    }
}
