package com.youyilin.system.logic;

import com.youyilin.system.dto.role.RoleMenuFormDTO;
import com.youyilin.system.vo.role.RoleAuthVO;

public interface SysRoleMenuLogic {

    /**
     * 根据角色ID获取菜单ID列表
     *
     * @param roleId 角色ID
     * @return 菜单ID列表
     */
    RoleAuthVO getAuth(Long roleId);

    /**
     * 授权角色菜单权限
     *
     * @param dto 包含角色菜单信息的对象
     */
    void auth(RoleMenuFormDTO dto);
}
