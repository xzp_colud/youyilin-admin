package com.youyilin.system.logic.impl;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.system.dto.log.OptLogQueryDTO;
import com.youyilin.system.logic.SysOptLogLogic;
import com.youyilin.system.mapper.SysOptLogMapper;
import com.youyilin.system.vo.log.OptLogPageVO;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SysOptLogLogicImpl implements SysOptLogLogic {

    private final SysOptLogMapper sysOptLogMapper;

    public SysOptLogLogicImpl(SysOptLogMapper sysOptLogMapper) {
        this.sysOptLogMapper = sysOptLogMapper;
    }

    @Override
    public Pager<OptLogPageVO> getPageList(Page<OptLogQueryDTO> page) {
        Integer total = sysOptLogMapper.getTotal(page);
        if (total == 0) {
            return new Pager<>(0, new ArrayList<>());
        }
        List<OptLogPageVO> list = sysOptLogMapper.getPage(page);
        return new Pager<>(total, list);
    }

    @Override
    public void delOptLog(List<Integer> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return;
        }

        sysOptLogMapper.deleteBatchIds(ids);
    }
}
