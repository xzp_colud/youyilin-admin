package com.youyilin.system.logic.impl;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.enums.StatusEnum;
import com.youyilin.system.dto.post.PostFormDTO;
import com.youyilin.system.dto.post.PostQueryDTO;
import com.youyilin.system.entity.SysDept;
import com.youyilin.system.entity.SysPost;
import com.youyilin.system.logic.SysPostLogic;
import com.youyilin.system.manager.DeptTreeNodesManager;
import com.youyilin.system.mapper.SysPostMapper;
import com.youyilin.system.service.SysDeptService;
import com.youyilin.system.service.SysPostService;
import com.youyilin.system.service.SysUserPostService;
import com.youyilin.system.vo.post.PostAddVO;
import com.youyilin.system.vo.post.PostEditVO;
import com.youyilin.system.vo.post.PostPageVO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SysPostLogicImpl implements SysPostLogic {

    private final SysPostMapper sysPostMapper;
    private final SysPostService sysPostService;
    private final SysDeptService sysDeptService;
    private final SysUserPostService sysUserPostService;

    public SysPostLogicImpl(SysPostMapper sysPostMapper, SysPostService sysPostService,
                            SysDeptService sysDeptService, SysUserPostService sysUserPostService) {
        this.sysPostMapper = sysPostMapper;
        this.sysPostService = sysPostService;
        this.sysDeptService = sysDeptService;
        this.sysUserPostService = sysUserPostService;
    }

    @Override
    public Pager<PostPageVO> getPageList(Page<PostQueryDTO> page) {
        Integer total = sysPostMapper.getTotal(page);
        if (total == 0) {
            return new Pager<>(0, new ArrayList<>());
        }
        List<PostPageVO> list = sysPostMapper.getPage(page);
        return new Pager<>(total, list);
    }

    @Override
    public PostAddVO queryAdd() {
        return new PostAddVO().setDeptList(new DeptTreeNodesManager(sysDeptService).generate());
    }

    @Override
    public PostEditVO queryEdit(Long id) {
        SysPost post = sysPostService.getById(id);
        if (post == null) {
            return new PostEditVO();
        }
        PostEditVO vo = PostEditVO.convertByEntity(post);
        vo.setDeptList(new DeptTreeNodesManager(sysDeptService).generate());

        return vo;
    }

    @Override
    public void savePost(PostFormDTO dto) {
        // 将DTO转换为实体类
        SysPost en = dto.convertToEntity();
        // 验证部门是否存在，并获取部门名称
        SysDept sysDept = sysDeptService.validateDeptStatus(en.getDeptId());
        en.setDeptName(sysDept.getDeptName());
        // 验证用户状态，判断用户是否被禁用
        boolean isDisabledFlag = en.getId() != null && !en.getStatus().equals(StatusEnum.NORMAL.getCode());
        if (isDisabledFlag) {
            // 如果用户被禁用，检查该用户是否已经存在于岗位上
            sysUserPostService.checkNoUserByPostId(en.getId());
        }
        // 验证岗位名称是否唯一
        sysPostService.validatePostNameUnique(en);
        // 验证岗位编码是否唯一
        sysPostService.validatePostCodeUnique(en);
        if (en.getId() == null) {
            // 如果岗位ID为空，则保存岗位信息
            sysPostService.save(en);
        } else {
            // 如果岗位ID不为空，则校验岗位是否存在，并更新岗位信息
            sysPostService.validatePost(en.getId());
            sysPostService.updateById(en);
        }
    }

    @Override
    public void delPost(Long id) {
        // 校验岗位是否存在
        SysPost oldPost = sysPostService.validatePost(id);

        // 检查岗位所关联的用户是否存在
        sysUserPostService.checkNoUserByPostId(oldPost.getId());

        // 根据岗位ID删除帖子
        sysPostService.removeById(id);
    }
}
