package com.youyilin.system.logic;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.system.dto.post.PostFormDTO;
import com.youyilin.system.dto.post.PostQueryDTO;
import com.youyilin.system.vo.post.PostAddVO;
import com.youyilin.system.vo.post.PostEditVO;
import com.youyilin.system.vo.post.PostPageVO;

public interface SysPostLogic {

    /**
     * 列表
     */
    Pager<PostPageVO> getPageList(Page<PostQueryDTO> page);

    /**
     * 新增查询
     *
     * @return PostAddVO
     */
    PostAddVO queryAdd();

    /**
     * 编辑查询
     *
     * @param id ID
     * @return PostEditVO
     */
    PostEditVO queryEdit(Long id);

    /**
     * 保存
     */
    void savePost(PostFormDTO dto);

    /**
     * 删除
     */
    void delPost(Long id);
}
