package com.youyilin.system.logic;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.system.dto.notice.SysNoticeFormDTO;
import com.youyilin.system.dto.notice.SysNoticeQueryDTO;
import com.youyilin.system.vo.notice.SysNoticeVO;

public interface SysNoticeLogic {

    /**
     * 列表
     */
    Pager<SysNoticeVO> getPageList(Page<SysNoticeQueryDTO> page);

    /**
     * 查询
     *
     * @param id 主键ID
     * @return SysNoticeVO
     */
    SysNoticeVO getById(Long id);

    /**
     * 保存
     */
    void saveNotice(SysNoticeFormDTO dto);

    /**
     * 禁用
     *
     * @param id 主键ID
     */
    void updateNoticeDisabled(Long id);
}
