package com.youyilin.system.logic;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.system.dto.user.UserFormDTO;
import com.youyilin.system.dto.user.UserQueryDTO;
import com.youyilin.system.dto.user.UserResetPasswordDto;
import com.youyilin.system.dto.user.UserUpdateMeFormDTO;
import com.youyilin.system.vo.user.UserAddVO;
import com.youyilin.system.vo.user.UserEditVO;
import com.youyilin.system.vo.user.UserPageVO;
import com.youyilin.system.vo.user.UserVO;

public interface SysUserLogic {

    /**
     * 获取用户分页列表
     *
     * @param page 分页查询对象
     * @return Pager
     */
    Pager<UserPageVO> getPageList(Page<UserQueryDTO> page);

    UserAddVO queryAdd();

    UserEditVO queryEdit(Long id);

    /**
     * 根据用户名获取SysUserVO对象
     *
     * @param userName 用户名
     * @return SysUserVO
     */
    UserVO getByUserName(String userName);

    /**
     * 保存用户信息
     *
     * @param dto 包含用户信息的SysUserFormDTO对象
     */
    void saveUser(UserFormDTO dto);

    /**
     * 重置用户密码
     *
     * @param userName 用户名
     */
    void resetPwd(String userName);

    /**
     * 更新登录用户信息
     *
     * @param dto 用户更新表单
     */
    void updateLoginUserInfo(UserUpdateMeFormDTO dto);

    /**
     * 更新用户密码
     *
     * @param dto 用户重置密码DTO
     */
    void updatePassword(UserResetPasswordDto dto);
}
