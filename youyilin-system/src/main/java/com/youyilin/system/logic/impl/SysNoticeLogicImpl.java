package com.youyilin.system.logic.impl;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.constants.CommonConstants;
import com.youyilin.common.enums.StatusEnum;
import com.youyilin.common.exception.Assert;
import com.youyilin.system.logic.SysNoticeLogic;
import com.youyilin.system.mapper.SysNoticeMapper;
import com.youyilin.system.dto.notice.SysNoticeFormDTO;
import com.youyilin.system.dto.notice.SysNoticeQueryDTO;
import com.youyilin.system.entity.SysNotice;
import com.youyilin.system.vo.notice.SysNoticeVO;
import com.youyilin.system.service.SysNoticeService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class SysNoticeLogicImpl implements SysNoticeLogic {

    private final SysNoticeMapper sysNoticeMapper;
    private final SysNoticeService sysNoticeService;

    public SysNoticeLogicImpl(SysNoticeMapper sysNoticeMapper, SysNoticeService sysNoticeService) {
        this.sysNoticeMapper = sysNoticeMapper;
        this.sysNoticeService = sysNoticeService;
    }

    @Override
    public Pager<SysNoticeVO> getPageList(Page<SysNoticeQueryDTO> page) {
        Integer total = sysNoticeMapper.getTotal(page);
        if (total == 0) {
            return new Pager<>(0, new ArrayList<>());
        }
        List<SysNoticeVO> list = sysNoticeMapper.getPage(page);
        return new Pager<>(total, list);
    }

    @Override
    public SysNoticeVO getById(Long id) {
        SysNotice notice = sysNoticeService.getById(id);
        return notice == null ? null : notice.convertToVO();
    }

    @Override
    @Transactional
    public void saveNotice(SysNoticeFormDTO dto) {
        // 将DTO转换为实体类
        SysNotice notice = dto.convertToEntity();
        // 验证参数
        this.validateSysNotice(notice);
        // 禁用其他公告
        if (notice.getType() != null
                // 判断公告类型是否为ANNOUNCE
                && notice.getType().equals(SysNotice.NoticeType.ANNOUNCE.name())
                // 判断公告状态是否为NORMAL
                && notice.getStatus() != null
                && notice.getStatus() == StatusEnum.NORMAL.getCode()) {
            // 更新ANNOUNCE类型的公告为禁用状态
            sysNoticeService.updateAnnounceDisabled();
        }
        if (notice.getId() == null) {
            // 如果是新增公告，将发送人数和阅读人数设置为0
            notice.setSendNum(0);
            notice.setReadNum(0);
            // 保存公告
            sysNoticeService.save(notice);
        } else {
            // 校验公告是否存在
            SysNotice oldNotice = sysNoticeService.validateNotice(notice.getId());
            // 判断公告类型是否未修改
            boolean noEditTypeFLag = notice.getType().equals(oldNotice.getType());
            Assert.isTrue(noEditTypeFLag, "禁止修改类型");

            // 更新公告
            sysNoticeService.updateById(notice);
        }
    }

    /**
     * 验证系统公告
     *
     * @param en 系统公告对象
     */
    private void validateSysNotice(SysNotice en) {
        String contentUrl = en.getContentUrl();
        if (StringUtils.isNotBlank(contentUrl)) {
            boolean urlFlag = contentUrl.startsWith(CommonConstants.HTTP) || contentUrl.startsWith(CommonConstants.HTTPS);
            Assert.isTrue(urlFlag, "文件地址不正确");
        }
        Long id = en.getId();
        if (id != null) {
            boolean isAnnounceFlag = en.getType() != null && en.getType().equals(SysNotice.NoticeType.ANNOUNCE.name());
            Assert.isTrue(isAnnounceFlag, "禁止编辑");
        }
    }

    /**
     * 更新通知状态为禁用
     *
     * @param id 通知ID
     */
    @Override
    public void updateNoticeDisabled(Long id) {
        sysNoticeService.validateNotice(id);
        sysNoticeService.disabledNotice(id);
    }
}
