package com.youyilin.system.logic.impl;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.enums.BooleanEnum;
import com.youyilin.common.exception.Assert;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.system.dto.dict.DictDataFormDTO;
import com.youyilin.system.dto.dict.DictDataQueryDTO;
import com.youyilin.system.entity.SysDictData;
import com.youyilin.system.logic.SysDictDataLogic;
import com.youyilin.system.mapper.SysDictDataMapper;
import com.youyilin.system.service.SysDictDataService;
import com.youyilin.system.service.SysDictTypeService;
import com.youyilin.system.vo.dict.DictDataEditVO;
import com.youyilin.system.vo.dict.DictDataPageVO;
import com.youyilin.system.vo.dict.DictDataVO;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class SysDictDataLogicImpl implements SysDictDataLogic {

    private final SysDictDataMapper sysDictDataMapper;
    private final SysDictDataService sysDictDataService;
    private final SysDictTypeService sysDictTypeService;

    public SysDictDataLogicImpl(SysDictDataMapper sysDictDataMapper, SysDictDataService sysDictDataService, SysDictTypeService sysDictTypeService) {
        this.sysDictDataMapper = sysDictDataMapper;
        this.sysDictDataService = sysDictDataService;
        this.sysDictTypeService = sysDictTypeService;
    }

    @Override
    public Pager<DictDataPageVO> getPageList(Page<DictDataQueryDTO> page) {
        Integer total = sysDictDataMapper.getTotal(page);
        if (total == 0) {
            return new Pager<>(0, new ArrayList<>());
        }
        List<DictDataPageVO> list = sysDictDataMapper.getPage(page);
        return new Pager<>(total, list);
    }

    @Override
    public DictDataEditVO queryEdit(Long id) {
        SysDictData dictData = sysDictDataService.getById(id);
        return dictData == null ? null : DictDataEditVO.convertByEntity(dictData);
    }

    @Override
    public Map<String, List<DictDataVO>> mapByDictTypeList(List<String> dictTypeList) {
        if (CollectionUtils.isEmpty(dictTypeList)) {
            return new HashMap<>();
        }
        List<SysDictData> list = sysDictDataService.listByDictTypeList(dictTypeList);
        if (CollectionUtils.isEmpty(list)) {
            return new HashMap<>();
        }
        List<DictDataVO> data = BeanHelper.map(list, DictDataVO.class);
        return data.stream().collect(Collectors.groupingBy(DictDataVO::getDictType));
    }

    @Override
    public void saveDictData(DictDataFormDTO dto) {
        // 将DTO转换为实体类
        SysDictData en = dto.convertToEntity();

        // 验证编码是否唯一
        sysDictDataService.validateDictCodeUnique(en);

        // 验证键值是否唯一
        sysDictDataService.validateDictValueUnique(en);

        // 验证字典类型是否禁用
        sysDictTypeService.validateDictTypeDisabled(en.getDictType());

        // 如果设置了默认值，更新其他字典项的默认值为false
        if (en.getDefaultFlag() != null && en.getDefaultFlag().equals(BooleanEnum.TRUE.getCode())) {
            sysDictDataService.updateDefaultFalse(en.getDictType());
        }

        // 如果字典数据ID为空，则保存字典数据，否则更新字典数据
        if (en.getId() == null) {
            sysDictDataService.save(en);
        } else {
            // 校验要更新的字典数据是否存在
            SysDictData oldSysDictData = sysDictDataService.validateDictData(en.getId());

            // 判断字典类型是否一致
            Assert.isTrue(oldSysDictData.isEqualsDictType(en.getDictType()), "当前字典类型和之前不一致");

            // 更新字典数据
            sysDictDataService.updateById(en);
        }
    }

    @Override
    public void delDictData(Long id) {
        sysDictDataService.validateDictData(id);
        sysDictDataService.delDictData(id);
    }
}
