package com.youyilin.system.logic.impl;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.bean.RMsg;
import com.youyilin.common.exception.Assert;
import com.youyilin.common.utils.SecurityUtil;
import com.youyilin.system.logic.SysNoticeUserReadLogic;
import com.youyilin.system.mapper.SysNoticeUserReadMapper;
import com.youyilin.system.dto.notice.SysNoticeSendFormDTO;
import com.youyilin.system.entity.SysNotice;
import com.youyilin.system.entity.SysNoticeUserRead;
import com.youyilin.system.entity.SysUser;
import com.youyilin.system.vo.notice.SysNoticeUserReadSendVO;
import com.youyilin.system.vo.notice.SysNoticeUserReadVO;
import com.youyilin.system.service.SysNoticeService;
import com.youyilin.system.service.SysNoticeUserReadService;
import com.youyilin.system.service.SysUserService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SysNoticeUserReadLogicImpl implements SysNoticeUserReadLogic {

    private final SysNoticeUserReadMapper sysNoticeUserReadMapper;
    private final SysNoticeUserReadService sysNoticeUserReadService;
    private final SysNoticeService sysNoticeService;
    private final SysUserService sysUserService;

    public SysNoticeUserReadLogicImpl(SysNoticeUserReadMapper sysNoticeUserReadMapper, SysNoticeUserReadService sysNoticeUserReadService,
                                      SysNoticeService sysNoticeService, SysUserService sysUserService) {
        this.sysNoticeUserReadMapper = sysNoticeUserReadMapper;
        this.sysNoticeUserReadService = sysNoticeUserReadService;
        this.sysNoticeService = sysNoticeService;
        this.sysUserService = sysUserService;
    }

    @Override
    public Pager<SysNoticeUserReadVO> getPageList(Page<SysNoticeUserReadVO> page) {
        Integer total = sysNoticeUserReadMapper.getTotal(page);
        if (total == 0) {
            return new Pager<>(0, new ArrayList<>());
        }
        List<SysNoticeUserReadVO> dbList = sysNoticeUserReadMapper.getPage(page);
        return new Pager<>(total, dbList);
    }

    @Override
    public Integer countLoginNoReadNum() {
        Long loginUserId = SecurityUtil.getUserId();
        return sysNoticeUserReadService.countNoReadNumByUserId(loginUserId);
    }

    @Override
    public List<SysNoticeUserReadSendVO> listUser(SysNoticeUserReadSendVO s) {
        return sysNoticeUserReadMapper.getSendPage(s);
    }

    @Override
    @Transactional
    public void saveUserRead(SysNoticeSendFormDTO dto) {
        // 获取公告ID
        Long noticeId = dto.getNoticeId();
        // 获取用户ID列表
        List<Long> userIds = dto.getUserIds();
        // 根据用户ID列表查询用户信息
        List<SysUser> dbUserList = sysUserService.listByIds(userIds);
        // 如果用户列表为空，则直接返回
        if (CollectionUtils.isEmpty(dbUserList)) {
            return;
        }
        // 校验公告状态
        SysNotice dbNotice = sysNoticeService.validateStatus(noticeId);
        boolean noticeTypeFlag = dbNotice.getType().equals(SysNotice.NoticeType.NOTICE.name());
        Assert.isTrue(noticeTypeFlag, "非通知禁止发送");
        // 获取公告ID
        Long dbNoticeId = dbNotice.getId();

        // 构建用户阅读记录列表
        List<SysNoticeUserRead> listBatch = dbUserList.stream().map(item -> {
            SysNoticeUserRead en = new SysNoticeUserRead();
            en.setUserId(item.getId());
            en.setNoticeId(dbNoticeId);
            en.setStatus(SysNoticeUserRead.Status.NO_READ.name());
            return en;
        }).collect(Collectors.toList());

        // 批量保存用户阅读记录
        sysNoticeUserReadService.saveBatch(listBatch);

        // 更新公告发送阅读人数统计信息
        sysNoticeService.updateSendReadNum(dbNoticeId, userIds.size(), 0);
    }

    @Override
    @Transactional
    public void saveUserRead(Long noticeId, List<Long> userIds) {
        SysNoticeSendFormDTO dto = new SysNoticeSendFormDTO();
        dto.setNoticeId(noticeId);
        dto.setUserIds(userIds);

        this.saveUserRead(dto);
    }

    @Override
    public void updateCancelByIds(List<Long> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return;
        }
        List<SysNoticeUserRead> dbList = sysNoticeUserReadService.listByIds(ids);
        if (CollectionUtils.isEmpty(dbList)) {
            return;
        }
        List<Long> updateIds = new ArrayList<>();
        dbList.forEach(item -> {
            if (!item.getStatus().equals(SysNoticeUserRead.Status.CANCEL.name())) {
                updateIds.add(item.getId());
            }
        });
        if (CollectionUtils.isEmpty(updateIds)) {
            return;
        }
        sysNoticeUserReadService.updateCancel(updateIds);
    }

    @Override
    public void updateCancelByNoticeId(Long noticeId) {
        SysNotice dnNotice = sysNoticeService.validateNotice(noticeId);
        sysNoticeUserReadService.updateCancel(dnNotice.getId());
    }

    @Override
    @Transactional
    public void updateLoginRead(Long id) {
        // 校验通知阅读记录
        SysNoticeUserRead dbNoticeUserRead = sysNoticeUserReadService.validateNoticeUserRead(id);
        // 如果状态已经是已读，则直接返回
        if (dbNoticeUserRead.getStatus().equals(SysNoticeUserRead.Status.READ.name())) {
            return;
        }

        // 获取登录用户的ID
        Long loginUserId = SecurityUtil.getUserId();
        // 判断数据库中的记录是否属于当前登录用户
        Assert.isTrue(dbNoticeUserRead.isEqualsUserIdForLogin(loginUserId), RMsg.SERVE_FAIL.getMsg());

        // 更新阅读状态为已读
        sysNoticeUserReadService.updateRead(id);
        // 更新发送通知的阅读人数
        sysNoticeService.updateSendReadNum(dbNoticeUserRead.getNoticeId(), 0, 1);
    }

    @Override
    @Transactional
    public void updateLoginRead() {
        Long loginUserId = SecurityUtil.getUserId();
        List<SysNoticeUserRead> noReadList = sysNoticeUserReadService.listNoReadByUserId(loginUserId);
        if (CollectionUtils.isEmpty(noReadList)) {
            return;
        }

        for (SysNoticeUserRead item : noReadList) {
            sysNoticeUserReadService.updateRead(item.getId());
            sysNoticeService.updateSendReadNum(item.getNoticeId(), 0, 1);
        }
    }
}
