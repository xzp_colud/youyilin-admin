package com.youyilin.system.logic;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.system.dto.file.FileQueryDTO;
import com.youyilin.system.vo.file.FilePageVO;

public interface SysFileLogic {

    /**
     * 列表
     */
    Pager<FilePageVO> getPageList(Page<FileQueryDTO> page);
}
