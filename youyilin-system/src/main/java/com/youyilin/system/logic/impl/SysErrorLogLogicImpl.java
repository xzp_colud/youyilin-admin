package com.youyilin.system.logic.impl;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.system.logic.SysErrorLogLogic;
import com.youyilin.system.mapper.SysErrorLogMapper;
import com.youyilin.system.dto.log.ErrorLogQueryDTO;
import com.youyilin.system.vo.log.ErrorLogPageVO;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SysErrorLogLogicImpl implements SysErrorLogLogic {

    private final SysErrorLogMapper sysErrorLogMapper;

    public SysErrorLogLogicImpl(SysErrorLogMapper sysErrorLogMapper) {
        this.sysErrorLogMapper = sysErrorLogMapper;
    }

    @Override
    public Pager<ErrorLogPageVO> getPageList(Page<ErrorLogQueryDTO> page) {
        Integer total = sysErrorLogMapper.getTotal(page);
        if (total == 0) {
            return new Pager<>(0, new ArrayList<>());
        }
        List<ErrorLogPageVO> list = sysErrorLogMapper.getPage(page);
        return new Pager<>(total, list);
    }

    @Override
    public void delErrorLogList(List<Long> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return;
        }

        sysErrorLogMapper.deleteBatchIds(ids);
    }
}
