package com.youyilin.system.logic;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.system.dto.log.ErrorLogQueryDTO;
import com.youyilin.system.vo.log.ErrorLogPageVO;

import java.util.List;

public interface SysErrorLogLogic {

    /**
     * 列表
     */
    Pager<ErrorLogPageVO> getPageList(Page<ErrorLogQueryDTO> page);

    /**
     * 删除
     */
    void delErrorLogList(List<Long> ids);
}
