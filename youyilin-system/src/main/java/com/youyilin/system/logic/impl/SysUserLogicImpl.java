package com.youyilin.system.logic.impl;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.config.email.EmailUtil;
import com.youyilin.common.exception.Assert;
import com.youyilin.common.utils.CharterUtil;
import com.youyilin.common.utils.ChineseCharToEnUtil;
import com.youyilin.common.utils.MobileUtil;
import com.youyilin.common.utils.SecurityUtil;
import com.youyilin.system.dto.user.UserFormDTO;
import com.youyilin.system.dto.user.UserQueryDTO;
import com.youyilin.system.dto.user.UserResetPasswordDto;
import com.youyilin.system.dto.user.UserUpdateMeFormDTO;
import com.youyilin.system.entity.SysUser;
import com.youyilin.system.logic.SysUserLogic;
import com.youyilin.system.mapper.SysUserMapper;
import com.youyilin.system.service.*;
import com.youyilin.system.vo.post.PostSelectVO;
import com.youyilin.system.vo.role.RoleSelectVO;
import com.youyilin.system.vo.user.UserAddVO;
import com.youyilin.system.vo.user.UserEditVO;
import com.youyilin.system.vo.user.UserPageVO;
import com.youyilin.system.vo.user.UserVO;
import com.youyilin.system.service.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class SysUserLogicImpl implements SysUserLogic {

    private final SysUserMapper sysUserMapper;
    private final SysUserService sysUserService;
    private final SysRoleService sysRoleService;
    private final SysPostService sysPostService;
    private final SysUserRoleService sysUserRoleService;
    private final SysUserPostService sysUserPostService;

    public SysUserLogicImpl(SysUserMapper sysUserMapper, SysUserService sysUserService, SysRoleService sysRoleService,
                            SysPostService sysPostService, SysUserRoleService sysUserRoleService, SysUserPostService sysUserPostService) {
        this.sysUserMapper = sysUserMapper;
        this.sysUserService = sysUserService;
        this.sysRoleService = sysRoleService;
        this.sysUserRoleService = sysUserRoleService;
        this.sysUserPostService = sysUserPostService;
        this.sysPostService = sysPostService;
    }

    @Override
    public Pager<UserPageVO> getPageList(Page<UserQueryDTO> page) {
        Integer total = sysUserMapper.getTotal(page);
        if (total == 0) {
            return new Pager<>(0, new ArrayList<>());
        }
        List<UserPageVO> list = sysUserMapper.getPage(page);
        return new Pager<>(total, list);
    }

    @Override
    public UserAddVO queryAdd() {
        return new UserAddVO()
                .setPostList(PostSelectVO.convertByEntity(sysPostService.listAll()))
                .setRoleList(RoleSelectVO.convertByEntity(sysRoleService.listAll()));
    }

    @Override
    public UserEditVO queryEdit(Long id) {
        SysUser user = sysUserService.getById(id);
        if (user == null) {
            return new UserEditVO();
        }
        UserEditVO vo = UserEditVO.convertByEntity(user);
        vo.setPostList(PostSelectVO.convertByEntity(sysPostService.listAll()));
        vo.setRoleList(RoleSelectVO.convertByEntity(sysRoleService.listAll()));
        vo.setPostIds(sysUserPostService.listPostIdsByUserId(id));
        vo.setRoleIds(sysUserRoleService.listRoleIdsByUserId(id));
        return vo;
    }

    @Override
    public UserVO getByUserName(String userName) {
        SysUser sysUser = sysUserService.getByUserName(userName);
        return sysUser == null ? null : UserVO.convertByEntity(sysUser);
    }

    @Override
    @Transactional
    public void saveUser(UserFormDTO dto) {
        // 将DTO转换为实体类
        SysUser en = dto.convertToEntity();
        // 判断用户名是否为字符或数字
        Assert.isTrue(CharterUtil.characterStringOrNum(en.getUserName()), "用户名只能输入数字和英文");
        // 判断手机号格式是否正确
        Assert.isFalse(MobileUtil.isNotMobile(en.getPhone()), "手机号格式不正确");
        // 判断邮箱格式是否正确
        Assert.isFalse(StringUtils.isNotBlank(en.getEmail()) && EmailUtil.isNotEmail(en.getEmail()), "邮箱格式不正确");
        // 验证用户名是否唯一
        sysUserService.validateUserNameUnique(en);
        // 验证手机号是否唯一
        sysUserService.validatePhoneUnique(en);
        // 验证邮箱是否唯一
        sysUserService.validateEmailUnique(en);
        // 设置用户密码为重置密码
        en.setPassword(sysUserService.getResetPassword());
        // 将用户真实姓名转为拼音，并设置到用户名属性中
        en.setPinYin(ChineseCharToEnUtil.getPinYinHeadChar(en.getRealName()));

        if (en.getId() == null) {
            // 如果用户ID为空，表示是新增用户，进行保存操作
            sysUserService.save(en);
        } else {
            // 如果用户ID不为空，表示是更新用户，进行更新操作
            en.setPassword(null);
            en.setUserName(null);
            // 校验用户是否为内置用户，如果是则禁止操作
            SysUser oldUser = sysUserService.validateUser(en.getId());
            Assert.isTrue(!oldUser.isAdmin(), "内置用户，禁止操作");

            // 更新用户信息
            sysUserService.updateById(en);
        }

        // 删除用户对应的角色记录
        sysUserRoleService.deleteByUserId(en.getId());
        // 删除用户对应的岗位记录
        sysUserPostService.deleteByUserId(en.getId());
        List<Long> roleIds = sysRoleService.listRoleIdsByIds(dto.getRoleIds());
        // 批量插入用户角色记录
        sysUserRoleService.insertUserRoleBatch(en.getId(), roleIds);
        List<Long> postIds = sysPostService.listPostIdsByIds(dto.getPostList());
        // 批量插入用户岗位记录
        sysUserPostService.insertUserPostBatch(en.getId(), postIds);
    }

    @Override
    public void resetPwd(String userName) {
        // 验证用户状态
        sysUserService.validateUserStatus(userName);
        // 更新用户密码
        sysUserService.updatePassword(userName);
    }

    @Override
    public void updateLoginUserInfo(UserUpdateMeFormDTO dto) {
        // 检查参数是否为空
        Assert.notNull(dto.getUserName(), "尚未登录");

        // 获取当前登录用户名
        String loginUserName = SecurityUtil.getUsername();

        // 检查当前登录用户名是否与参数中的用户名一致
        boolean userNameEquals = loginUserName.equals(dto.getUserName());
        Assert.isTrue(userNameEquals, "尚未登录");

        // 将DTO转换为实体类
        SysUser user = dto.convertToEntity();

        // 校验用户是否存在
        SysUser oldUser = sysUserService.validateUser(user.getUserName());

        // 检查用户名是否一致
        Assert.isTrue(dto.getUserName().equals(oldUser.getUserName()), "请登录");

        // 更新用户信息
        sysUserService.updateUserByMe(user);
    }

    @Override
    public void updatePassword(UserResetPasswordDto dto) {
        // 获取用户名
        String userName = dto.getUserName();
        // 获取当前登录用户名
        String loginUserName = SecurityUtil.getUsername();
        // 检查当前用户是否已登录
        Assert.notNull(loginUserName, "请登录");
        // 检查当前用户是否与待修改密码的用户名相同
        Assert.isTrue(loginUserName.equals(userName), "系统错误");
        // 检查新密码是否与旧密码相同
        Assert.isFalse(dto.getPassword().equals(dto.getNewPassword()), "新旧密码不能一样");
        // 检查两次输入的新密码是否一致
        Assert.isTrue(dto.getNewPassword().equals(dto.getReNewPassword()), "两次密码不一致");

        // 验证用户状态
        SysUser sysUser = sysUserService.validateUserStatus(userName);

        // 校验原密码是否正确
        String old = sysUserService.refreshPassword(dto.getPassword());
        Assert.isTrue(old.equals(sysUser.getPassword()), "原密码错误");

        // 校验新密码是否正确
        String news = sysUserService.refreshPassword(dto.getNewPassword());
        sysUserService.update(new LambdaUpdateWrapper<SysUser>().set(SysUser::getPassword, news).eq(SysUser::getUserName, userName));
    }
}
