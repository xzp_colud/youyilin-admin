package com.youyilin.system.logic.impl;

import com.youyilin.common.entity.TreeNodes;
import com.youyilin.system.convert.SysMenuToSysRoleMenuConvert;
import com.youyilin.system.dto.role.RoleMenuFormDTO;
import com.youyilin.system.entity.SysMenu;
import com.youyilin.system.entity.SysRole;
import com.youyilin.system.entity.SysRoleMenu;
import com.youyilin.system.logic.SysRoleMenuLogic;
import com.youyilin.system.manager.MenuTreeNodesManager;
import com.youyilin.system.service.SysMenuService;
import com.youyilin.system.service.SysRoleMenuService;
import com.youyilin.system.service.SysRoleService;
import com.youyilin.system.vo.role.RoleAuthVO;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;

@Service
public class SysRoleMenuLogicImpl implements SysRoleMenuLogic {

    private final SysRoleMenuService sysRoleMenuService;
    private final SysRoleService sysRoleService;
    private final SysMenuService sysMenuService;

    public SysRoleMenuLogicImpl(SysRoleMenuService sysRoleMenuService, SysRoleService sysRoleService, SysMenuService sysMenuService) {
        this.sysRoleMenuService = sysRoleMenuService;
        this.sysRoleService = sysRoleService;
        this.sysMenuService = sysMenuService;
    }

    @Override
    public RoleAuthVO getAuth(Long roleId) {
        SysRole role = sysRoleService.getById(roleId);
        if (role == null) {
            return new RoleAuthVO();
        }
        List<TreeNodes> menuNodes = new MenuTreeNodesManager(sysMenuService).generate();
        List<String> menuIds = sysRoleMenuService.listMenuIdsByRoleIds(Collections.singletonList(roleId));
        return new RoleAuthVO().setRoleId(roleId).setMenuList(menuNodes).setMenuIds(menuIds);
    }

    @Override
    @Transactional
    public void auth(RoleMenuFormDTO dto) {
        // 获取角色ID
        Long roleId = dto.getRoleId();
        // 校验角色是否存在
        SysRole dbRole = sysRoleService.validateRole(roleId);
        // 根据角色ID删除角色菜单关联关系
        sysRoleMenuService.deleteRoleMenuByRoleId(dbRole.getId());
        // 如果菜单ID列表为空，则直接返回
        if (CollectionUtils.isEmpty(dto.getMenuIds())) {
            return;
        }
        // 根据菜单ID列表查询菜单列表
        List<SysMenu> menuList = sysMenuService.listByIds(dto.getMenuIds());
        // 如果菜单列表为空，则直接返回
        if (CollectionUtils.isEmpty(menuList)) {
            return;
        }
        // 将菜单列表转换为角色菜单对象列表
        List<SysRoleMenu> saveList = new SysMenuToSysRoleMenuConvert().convert(menuList);
        // 设置角色ID到每个角色菜单对象中
        saveList.forEach(item -> item.setRoleId(roleId));
        // 批量保存角色菜单对象列表到数据库中
        sysRoleMenuService.saveBatch(saveList);
    }
}
