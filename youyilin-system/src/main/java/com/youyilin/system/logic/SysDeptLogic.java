package com.youyilin.system.logic;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.entity.TreeNodes;
import com.youyilin.system.dto.dept.DeptFormDTO;
import com.youyilin.system.dto.dept.DeptQueryDTO;
import com.youyilin.system.vo.dept.DeptAddVO;
import com.youyilin.system.vo.dept.DeptEditVO;
import com.youyilin.system.vo.dept.DeptPageVO;

import java.util.List;

public interface SysDeptLogic {

    /**
     * 列表
     */
    Pager<DeptPageVO> getPageList(Page<DeptQueryDTO> page);

    /**
     * 部门结构查询
     *
     * @return ArrayList
     */
    List<TreeNodes> queryTree();

    /**
     * 新增查询
     *
     * @return DeptAddVO
     */
    DeptAddVO queryAdd();

    /**
     * 编辑查询
     *
     * @param id ID
     * @return DeptEditVO
     */
    DeptEditVO queryEdit(Long id);

    /**
     * 保存
     */
    void saveSysDept(DeptFormDTO dto);

    /**
     * 删除
     */
    void delSysDept(Long id);
}
