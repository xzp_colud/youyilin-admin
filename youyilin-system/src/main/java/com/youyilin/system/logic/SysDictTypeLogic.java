package com.youyilin.system.logic;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.system.dto.dict.DictTypeFormDTO;
import com.youyilin.system.dto.dict.DictTypeQueryDTO;
import com.youyilin.system.vo.dict.DictTypeEditVO;
import com.youyilin.system.vo.dict.DictTypePageVO;

public interface SysDictTypeLogic {

    /**
     * 列表
     */
    Pager<DictTypePageVO> getPageList(Page<DictTypeQueryDTO> page);

    /**
     * 查询
     *
     * @param id 主键ID
     * @return SysDictTypeVo
     */
    DictTypeEditVO queryEdit(Long id);

    /**
     * 保存
     */
    void saveDictType(DictTypeFormDTO dto);
}
