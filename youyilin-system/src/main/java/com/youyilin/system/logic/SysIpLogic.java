package com.youyilin.system.logic;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.system.dto.ip.IpFormDTO;
import com.youyilin.system.dto.ip.IpQueryDTO;
import com.youyilin.system.vo.ip.IpEditVO;
import com.youyilin.system.vo.ip.IpPageVO;

public interface SysIpLogic {

    /**
     * 初始化至Redis
     */
    void initToRedis();

    /**
     * 列表
     */
    Pager<IpPageVO> getPageList(Page<IpQueryDTO> page);

    /**
     * 查询
     *
     * @param id 主键ID
     * @return SysIpVO
     */
    IpEditVO queryEdit(Long id);

    /**
     * 保存
     */
    void saveIp(IpFormDTO dto);

    /**
     * 删除
     */
    void delIp(Long id);

    /**
     * 是否为黑名单
     */
    Boolean checkBlankIp(String ip);
}
