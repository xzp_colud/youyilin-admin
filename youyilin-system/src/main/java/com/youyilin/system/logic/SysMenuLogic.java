package com.youyilin.system.logic;

import com.youyilin.system.dto.menu.MenuFormDTO;
import com.youyilin.system.vo.menu.MenuAddVO;
import com.youyilin.system.vo.menu.MenuEditVO;
import com.youyilin.system.vo.menu.MenuPageVO;
import com.youyilin.system.vo.menu.MenuVO;

import java.util.List;

public interface SysMenuLogic {

    /**
     * 列表
     *
     * @return ArrayList
     */
    List<MenuPageVO> getPageList();

    /**
     * 新增查询
     *
     * @return MenuAddVO
     */
    MenuAddVO queryAdd();

    /**
     * 编辑查询
     *
     * @param id ID
     * @return MenuEditVO
     */
    MenuEditVO queryEdit(Long id);

    /**
     * 登录路由
     */
    List<MenuVO> listLoginRouter();

    /**
     * 登录权限
     */
    List<String> listLoginPerms(Long userId);

    /**
     * 保存
     */
    void saveMenu(MenuFormDTO dto);

    /**
     * 删除
     */
    void delMenu(Long id);
}
