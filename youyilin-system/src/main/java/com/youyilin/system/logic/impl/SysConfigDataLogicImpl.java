package com.youyilin.system.logic.impl;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.system.logic.SysConfigDataLogic;
import com.youyilin.system.mapper.SysConfigDataMapper;
import com.youyilin.system.dto.config.SysConfigDataInsertFormDTO;
import com.youyilin.system.dto.config.SysConfigDataPageQueryVO;
import com.youyilin.system.entity.SysConfigData;
import com.youyilin.system.vo.config.SysConfigDataVO;
import com.youyilin.system.service.SysConfigDataService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SysConfigDataLogicImpl implements SysConfigDataLogic {

    private final SysConfigDataMapper sysConfigDataMapper;
    private final SysConfigDataService sysConfigDataService;

    public SysConfigDataLogicImpl(SysConfigDataMapper sysConfigDataMapper, SysConfigDataService sysConfigDataService) {
        this.sysConfigDataMapper = sysConfigDataMapper;
        this.sysConfigDataService = sysConfigDataService;
    }

    @Override
    public Pager<SysConfigDataVO> getPageList(Page<SysConfigDataPageQueryVO> page) {
        Integer total = sysConfigDataMapper.getTotal(page);
        if (total == 0) {
            return new Pager<>(0, new ArrayList<>());
        }
        List<SysConfigDataVO> list = sysConfigDataMapper.getPage(page);
        return new Pager<>(total, list);
    }

    @Override
    public SysConfigDataVO getById(Long id) {
        SysConfigData configData = sysConfigDataService.getById(id);
        return configData == null ? null : configData.convertToVO();
    }

    @Override
    public List<SysConfigDataVO> listByConfigId(Long configId) {
        List<SysConfigData> list = sysConfigDataService.listByConfigId(configId);
        return BeanHelper.map(list, SysConfigDataVO.class);
    }

    @Override
    public void saveConfigData(SysConfigDataInsertFormDTO dto) {
        // 将DTO转换为SysConfigData实体类
        SysConfigData en = dto.convertToEntity();

        // 验证数据选项是否合法
        sysConfigDataService.validateDataOptions(en);

        // 验证编码是否唯一
        sysConfigDataService.validateCodeUnique(en);

        // 如果实体类en的id为空，则执行保存操作
        if (en.getId() == null) {
            sysConfigDataService.save(en);
        } else {
            // 验证配置数据是否合法
            sysConfigDataService.validateConfigData(en.getId());

            // 将配置ID置为空
            en.setConfigId(null);

            // 执行更新操作
            sysConfigDataService.updateById(en);
        }
    }

    @Override
    public void delConfigData(Long id) {
        sysConfigDataService.removeById(id);
    }
}
