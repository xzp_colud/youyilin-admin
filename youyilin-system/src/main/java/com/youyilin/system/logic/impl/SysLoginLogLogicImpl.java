package com.youyilin.system.logic.impl;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.system.logic.SysLoginLogLogic;
import com.youyilin.system.mapper.SysLoginLogMapper;
import com.youyilin.system.dto.log.LoginLogQueryDTO;
import com.youyilin.system.vo.log.LoginLogPageVO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SysLoginLogLogicImpl implements SysLoginLogLogic {

    private final SysLoginLogMapper sysLoginLogMapper;

    public SysLoginLogLogicImpl(SysLoginLogMapper sysLoginLogMapper) {
        this.sysLoginLogMapper = sysLoginLogMapper;
    }

    @Override
    public Pager<LoginLogPageVO> getPageList(Page<LoginLogQueryDTO> page) {
        Integer total = sysLoginLogMapper.getTotal(page);
        if (total == 0) {
            return new Pager<>(0, new ArrayList<>());
        }
        List<LoginLogPageVO> list = sysLoginLogMapper.getPage(page);
        return new Pager<>(total, list);
    }

    @Override
    public void delLoginLog(Long id) {
        sysLoginLogMapper.deleteById(id);
    }
}
