package com.youyilin.system.logic.impl;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.exception.Assert;
import com.youyilin.common.redis.RedisPrefix;
import com.youyilin.common.redis.RedisService;
import com.youyilin.common.utils.ip.IpUtils;
import com.youyilin.system.enums.IpTypeEnum;
import com.youyilin.system.logic.SysIpLogic;
import com.youyilin.system.mapper.SysIpMapper;
import com.youyilin.system.dto.ip.IpFormDTO;
import com.youyilin.system.dto.ip.IpQueryDTO;
import com.youyilin.system.entity.SysIp;
import com.youyilin.system.vo.ip.IpEditVO;
import com.youyilin.system.service.SysIpService;
import com.youyilin.system.vo.ip.IpPageVO;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class SysIpLogicImpl implements SysIpLogic {

    private final SysIpMapper sysIpMapper;
    private final SysIpService sysIpService;
    private final RedisService redisService;

    public SysIpLogicImpl(SysIpMapper sysIpMapper, SysIpService sysIpService, RedisService redisService) {
        this.sysIpMapper = sysIpMapper;
        this.sysIpService = sysIpService;
        this.redisService = redisService;
    }

    @Override
    public void initToRedis() {
        List<SysIp> sysIpList = sysIpService.list();
        String ipBlankKey = RedisPrefix.IP_BLANK_KEY_PREFIX + "*";
        String ipWhiteKey = RedisPrefix.IP_WHITE_KEY_PREFIX + "*";
        Set<String> blankKeysSet = redisService.likes(ipBlankKey);
        Set<String> whiteKeysSet = redisService.likes(ipWhiteKey);
        if (CollectionUtils.isNotEmpty(blankKeysSet)) {
            redisService.delete(blankKeysSet);
        }
        if (CollectionUtils.isNotEmpty(whiteKeysSet)) {
            redisService.delete(whiteKeysSet);
        }
        if (CollectionUtils.isNotEmpty(sysIpList)) {
            sysIpList.forEach(this::applyRedisIp);
        }
    }

    @Override
    public Pager<IpPageVO> getPageList(Page<IpQueryDTO> page) {
        Integer total = sysIpMapper.getTotal(page);
        if (total == 0) {
            return new Pager<>(0, new ArrayList<>());
        }
        List<IpPageVO> list = sysIpMapper.getPage(page);
        return new Pager<>(total, list);
    }

    @Override
    public IpEditVO queryEdit(Long id) {
        SysIp sysIp = sysIpService.getById(id);
        return sysIp == null ? null : IpEditVO.convertByEntity(sysIp);
    }

    @Override
    public void saveIp(IpFormDTO dto) {
        SysIp en = dto.convertToEntity();
        // 验证IP的准确性
        Assert.isTrue(IpUtils.isIp(en.getIp()), "IP format fail");
        // 验证IP的唯一性
        sysIpService.validateIpUnique(en);
        if (en.getId() == null) {
            sysIpService.save(en);
        } else {
            // 校验IP是否已存在
            SysIp oldIp = sysIpService.validateIp(en.getId());
            if (!oldIp.getIp().equals(en.getIp())) {
                // 如果IP不一致，删除Redis中的旧IP缓存
                this.delRedisBlankIp(oldIp.getIp());
                this.delRedisBlankIp(oldIp.getIp());
            }
            // 更新已存在的IP记录
            sysIpService.updateById(en);
        }
        // 添加Redis缓存
        this.applyRedisIp(en);
    }

    @Override
    public void delIp(Long id) {
        // 根据id获取SysIp对象
        SysIp sysIp = sysIpService.getById(id);
        // 如果获取的SysIp对象为空，则直接返回
        if (sysIp == null) {
            return;
        }

        // 调用sysIpService的delIp方法，根据id删除SysIp记录
        sysIpService.delIp(id);
        // 调用delRedisWhiteIp方法，根据sysIp的ip地址删除Redis中对应的空闲IP记录
        this.delRedisWhiteIp(sysIp.getIp());
        // 调用delRedisBlankIp方法，根据sysIp的ip地址删除Redis中对应的空闲IP记录
        this.delRedisBlankIp(sysIp.getIp());
    }

    @Override
    public Boolean checkBlankIp(String ip) {
        String blankIp = RedisPrefix.IP_BLANK_KEY_PREFIX + ip;
        return redisService.hasKey(blankIp);
    }

    /**
     * 将IP地址应用到Redis缓存中。
     *
     * @param en 包含IP地址信息的SysIp对象
     */
    private void applyRedisIp(SysIp en) {
        String ip = en.getIp();
        int type = en.getType() == null ? IpTypeEnum.BLANK.getCode() : en.getType();

        String blankIp = this.delRedisBlankIp(en.getIp());
        String whiteIp = this.delRedisWhiteIp(en.getIp());

        if (type == IpTypeEnum.WHITE.getCode()) {
            redisService.add(whiteIp, ip);
        } else {
            redisService.add(blankIp, ip);
        }
    }

    /**
     * 从Redis中删除白名单IP
     *
     * @param ip IP地址
     * @return 删除结果
     */
    private String delRedisWhiteIp(String ip) {
        return this.delRedisIp(ip, RedisPrefix.IP_WHITE_KEY_PREFIX);
    }

    /**
     * 从Redis中删除空IP记录
     *
     * @param ip 要删除的IP地址
     * @return String
     */
    private String delRedisBlankIp(String ip) {
        return this.delRedisIp(ip, RedisPrefix.IP_BLANK_KEY_PREFIX);
    }

    /**
     * 从Redis中删除IP地址。
     *
     * @param ip   IP地址
     * @param type 类型
     * @return String
     */
    private String delRedisIp(String ip, String type) {
        String redisIp = type + ip;
        if (redisService.hasKey(redisIp)) {
            redisService.delete(redisIp);
        }

        return redisIp;
    }
}
