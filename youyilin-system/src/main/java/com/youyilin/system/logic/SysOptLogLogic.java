package com.youyilin.system.logic;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.system.dto.log.OptLogQueryDTO;
import com.youyilin.system.vo.log.OptLogPageVO;

import java.util.List;

public interface SysOptLogLogic {

    /**
     * 列表
     */
    Pager<OptLogPageVO> getPageList(Page<OptLogQueryDTO> page);

    /**
     * 删除
     */
    void delOptLog(List<Integer> ids);
}
