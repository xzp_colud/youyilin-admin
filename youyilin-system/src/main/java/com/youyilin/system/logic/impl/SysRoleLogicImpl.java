package com.youyilin.system.logic.impl;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.exception.Assert;
import com.youyilin.common.utils.CharterUtil;
import com.youyilin.system.dto.role.RoleFormDTO;
import com.youyilin.system.dto.role.RoleQueryDTO;
import com.youyilin.system.entity.SysRole;
import com.youyilin.system.logic.SysRoleLogic;
import com.youyilin.system.mapper.SysRoleMapper;
import com.youyilin.system.service.SysRoleService;
import com.youyilin.system.vo.role.RoleEditVO;
import com.youyilin.system.vo.role.RolePageVO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SysRoleLogicImpl implements SysRoleLogic {

    private final SysRoleMapper sysRoleMapper;
    private final SysRoleService sysRoleService;

    public SysRoleLogicImpl(SysRoleMapper sysRoleMapper, SysRoleService sysRoleService) {
        this.sysRoleMapper = sysRoleMapper;
        this.sysRoleService = sysRoleService;
    }

    @Override
    public Pager<RolePageVO> getPageList(Page<RoleQueryDTO> page) {
        Integer total = sysRoleMapper.getTotal(page);
        if (total == 0) {
            return new Pager<>(0, new ArrayList<>());
        }
        List<RolePageVO> list = sysRoleMapper.getPage(page);
        return new Pager<>(total, list);
    }

    @Override
    public RoleEditVO queryEdit(Long id) {
        SysRole role = sysRoleService.getById(id);
        return role == null ? null : RoleEditVO.convertByEntity(role);
    }

    @Override
    public void saveRole(RoleFormDTO dto) {
        // 将DTO转换为Entity对象
        SysRole en = dto.convertToEntity();
        // 校验角色字符串是否为英文
        Assert.isTrue(CharterUtil.characterString(en.getRoleKey()), "角色字符串只能输入英文");
        // 校验角色名是否唯一
        sysRoleService.validateRoleNameUnique(en);
        // 校验角色键是否唯一
        sysRoleService.validateRoleKeyUnique(en);
        if (en.getId() == null) {
            // 如果角色ID为空，则保存角色
            sysRoleService.save(en);
        } else {
            // 校验是否存在该ID的角色
            SysRole oldRole = sysRoleService.validateRole(en.getId());
            // 判断是否为系统内置角色
            Assert.isFalse(oldRole.isAdmin(), "系统内置角色，禁止操作");
            // 更新角色信息
            sysRoleService.updateById(en);
        }
    }
}
