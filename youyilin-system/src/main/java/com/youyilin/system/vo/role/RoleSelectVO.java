package com.youyilin.system.vo.role;

import com.youyilin.common.enums.StatusEnum;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.system.entity.SysRole;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 角色下拉列表VO
 */
@Data
@Accessors(chain = true)
public class RoleSelectVO {
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 角色名称
    private String roleName;
    // 状态
    private Integer status;
    // 是否禁用
    public boolean isDisabled() {
        return this.status == null || this.status != StatusEnum.NORMAL.getCode();
    }

    public static List<RoleSelectVO> convertByEntity(List<SysRole> list) {
        return BeanHelper.map(list, RoleSelectVO.class);
    }
}
