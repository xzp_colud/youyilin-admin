package com.youyilin.system.vo.file;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.util.Date;

/**
 * 文件列表VO
 */
@Data
public class FilePageVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 修改时间
    private Date modifyDate;
    // 文件名称
    private String name;
    // 文件地址
    private String url;
    // 组别名
    private String fileGroup;
    // 后缀
    private String suffix;
    // 大小
    private String size;
    // 分辨率
    private String resolution;
    // 状态
    private Integer status;
}
