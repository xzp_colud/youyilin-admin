package com.youyilin.system.vo.post;

import com.youyilin.common.entity.TreeNodes;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.system.entity.SysPost;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 岗位编辑VO
 */
@Data
@Accessors(chain = true)
public class PostEditVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 岗位名称
    private String postName;
    // 岗位编码
    private String postCode;
    // 状态
    private Integer status;
    // 描述
    private String description;
    // 所属部门
    @JsonSerialize(using = ToStringSerializer.class)
    private Long deptId;
    // 部门结构列表
    private List<TreeNodes> deptList;

    public static PostEditVO convertByEntity(SysPost post) {
        return BeanHelper.map(post, PostEditVO.class);
    }
}
