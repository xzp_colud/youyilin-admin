package com.youyilin.system.vo.user;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.util.Date;

@Data
public class OnlineUserVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    private String createTokenId;
    /**
     * 用户唯一标识
     */
    private String token;
    /**
     * 用户名
     */
    private String userName;
    /**
     * 关联的手机号
     */
    private String mobile;
    /**
     * 真实的用户姓名
     */
    private String realName;
    /**
     * 登录时间
     */
    private Date loginDate;
    /**
     * 登录ip
     */
    private String loginIp;
    /**
     * 登录地点
     */
    private String loginLocation;
    /**
     * 是否为移动端登录
     */
    private boolean mobileLogin;
}
