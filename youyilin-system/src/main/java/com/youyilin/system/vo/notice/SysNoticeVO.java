package com.youyilin.system.vo.notice;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class SysNoticeVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    private String title;
    private String content;
    private String contentUrl;
    private String type;
    private Integer status;
    private Integer sendNum;
    private Integer readNum;
    private String remark;
}
