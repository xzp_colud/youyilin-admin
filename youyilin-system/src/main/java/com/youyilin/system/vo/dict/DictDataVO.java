package com.youyilin.system.vo.dict;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.util.Date;

@Data
public class DictDataVO {
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    private Date modifyDate;
    private String dictCode;
    private String dictSort;
    private String dictLabel;
    private String dictValue;
    private String dictType;
    private String description;
    private Integer status;
    private String cssClass;
    private String tableClass;
    private Integer defaultFlag;
}
