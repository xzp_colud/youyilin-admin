package com.youyilin.system.vo.user;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 用户列表VO
 */
@Data
@Accessors(chain = true)
public class UserPageVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    private Date modifyDate;
    private String userName;
    private String jobNumber;
    private String phone;
    private String realName;
    private String avatarUrl;
    private String sex;
    private String age;
    private String description;
    private String email;
    private String pinYin;
    private String province;
    private String city;
    private String area;
    private String detail;
    private Integer status;
    private Integer shareLogin;
    private String loginIp;
    private String loginLocation;
    private Date loginDate;
}
