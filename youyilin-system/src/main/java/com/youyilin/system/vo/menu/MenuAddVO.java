package com.youyilin.system.vo.menu;

import com.youyilin.common.entity.TreeNodes;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 菜单新增VO
 */
@Data
@Accessors(chain = true)
public class MenuAddVO {

    // 所有菜单结构
    private TreeNodes menuList;
}
