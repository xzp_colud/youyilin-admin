package com.youyilin.system.vo.dept;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 部门列表VO
 */
@Data
@Accessors(chain = true)
public class DeptPageVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 修改时间
    private Date modifyDate;
    // 部门名称
    private String deptName;
    // 编码
    private String deptCode;
    // 状态
    private Integer status;
    // 描述
    private String description;
    // 负责人
    private String leader;
    // 负责人电话
    private String phone;
    // 负责人邮箱
    private String email;
    // 所属部门名称
    private String pidName;
}
