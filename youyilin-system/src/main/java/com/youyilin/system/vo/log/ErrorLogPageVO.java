package com.youyilin.system.vo.log;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.util.Date;

/**
 * 异常日志率VO
 */
@Data
public class ErrorLogPageVO {
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 修改时间
    private Date modifyDate;
    // 方法
    private String method;
    // 错误信息
    private String msg;
    // 错误类型
    private String type;
}
