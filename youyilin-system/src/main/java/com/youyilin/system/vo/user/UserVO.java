package com.youyilin.system.vo.user;

import com.youyilin.common.utils.BeanHelper;
import com.youyilin.system.entity.SysUser;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.util.Date;

/**
 * 用户VO
 */
@Data
public class UserVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    private String userName;
    private String jobNumber;
    private String phone;
    private String realName;
    private String avatarUrl;
    private String sex;
    private String age;
    private String description;
    private String email;
    private String pinYin;
    private String province;
    private String city;
    private String area;
    private String detail;
    private Integer status;
    private Integer shareLogin;
    private String loginIp;
    private String loginLocation;
    private Date loginDate;

    public boolean isAdmin() {
        return SysUser.isAdmin(this.id);
    }

    public static UserVO convertByEntity(SysUser user) {
        return BeanHelper.map(user, UserVO.class);
    }
}
