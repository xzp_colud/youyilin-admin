package com.youyilin.system.vo.log;

import lombok.Data;

import java.util.Date;

/**
 * 操作日志列表VO
 */
@Data
public class OptLogPageVO {
    private Integer id;
    // 操作时间
    private Date modifyDate;
    // 账号用户名
    private String userName;
    // 操作链接
    private String url;
    // 请求类型
    private String method;
    // 请求参数
    private String params;
    // 返回数据
    private String resultData;
    // 返回编码
    private String resultCode;
    // 标题
    private String title;
    // 业务类型
    private String businessType;
    // 执行时间
    private String time;
}
