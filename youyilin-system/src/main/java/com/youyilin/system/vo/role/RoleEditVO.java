package com.youyilin.system.vo.role;

import com.youyilin.common.utils.BeanHelper;
import com.youyilin.system.entity.SysRole;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 角色编辑VO
 */
@Data
@Accessors(chain = true)
public class RoleEditVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 角色名称
    private String roleName;
    // 关键字
    private String roleKey;
    // 状态
    private Integer status;
    // 描述
    private String description;
    // 排序
    private Integer sort;

    public static RoleEditVO convertByEntity(SysRole role) {
        return BeanHelper.map(role, RoleEditVO.class);
    }
}
