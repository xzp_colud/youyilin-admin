package com.youyilin.system.vo.role;

import com.youyilin.system.entity.SysRole;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 角色列表VO
 */
@Data
@Accessors(chain = true)
public class RolePageVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 修改时间
    private Date modifyDate;
    // 角色名称
    private String roleName;
    // 关键字
    private String roleKey;
    // 状态
    private Integer status;
    // 描述
    private String description;
    // 排序
    private Integer sort;

    public boolean isAdmin() {
        return SysRole.isAdmin(this.id);
    }
}
