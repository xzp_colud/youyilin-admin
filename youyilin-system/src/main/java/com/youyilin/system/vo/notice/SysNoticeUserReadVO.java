package com.youyilin.system.vo.notice;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.util.Date;

@Data
public class SysNoticeUserReadVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long noticeId;

    // 是否为Table查询
    private boolean table;

    private Date createDate;
    private Date readDate;
    private Date cancelDate;

    private String title;
    private String content;
    private String contentUrl;
    private String remark;
    private String status;

    private String userName;
    private String realName;
}
