package com.youyilin.system.vo.notice;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

/**
 * 发送用户列表
 */
@Data
public class SysNoticeUserReadSendVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long noticeId;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;

    // 用户名称
    private String userName;
    // 真实姓名
    private String realName;
    // 用户状态
    private Integer status;
    // 发送状态
    private String sendStatus;
    // 已发数量
    private Integer sendNum;
}
