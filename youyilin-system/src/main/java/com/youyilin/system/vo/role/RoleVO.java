package com.youyilin.system.vo.role;

import com.youyilin.system.entity.SysRole;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 角色VO
 */
@Data
@Accessors(chain = true)
public class RoleVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    private Date createDate;
    private Date modifyDate;
    private String roleName;
    private String roleKey;
    private Integer status;
    private String description;
    private Integer sort;

    public boolean isAdmin() {
        return SysRole.isAdmin(this.id);
    }
}
