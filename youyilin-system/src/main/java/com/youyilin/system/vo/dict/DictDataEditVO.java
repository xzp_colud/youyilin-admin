package com.youyilin.system.vo.dict;

import com.youyilin.common.utils.BeanHelper;
import com.youyilin.system.entity.SysDictData;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

/**
 * 字典键值编辑VO
 */
@Data
public class DictDataEditVO {
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    private String dictCode;
    private String dictSort;
    private String dictLabel;
    private String dictValue;
    private String dictType;
    private String description;
    private Integer status;
    private String cssClass;
    private String tableClass;
    private Integer defaultFlag;

    public static DictDataEditVO convertByEntity(SysDictData dictData) {
        return BeanHelper.map(dictData, DictDataEditVO.class);
    }
}
