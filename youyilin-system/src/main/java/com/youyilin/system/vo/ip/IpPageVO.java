package com.youyilin.system.vo.ip;

import com.youyilin.system.enums.IpTypeEnum;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.util.Date;

/**
 * IP列表VO
 */
@Data
public class IpPageVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 修改时间
    private Date modifyDate;
    // IP
    private String ip;
    // 类型
    private Integer type;
    // 状态
    private Integer status;

    public String getTypeText() {
        return IpTypeEnum.queryDescByCode(this.type);
    }
}
