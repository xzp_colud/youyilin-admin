package com.youyilin.system.vo.user;

import com.youyilin.common.utils.BeanHelper;
import com.youyilin.system.entity.SysUser;
import com.youyilin.system.vo.post.PostSelectVO;
import com.youyilin.system.vo.role.RoleSelectVO;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 用户新增VO
 */
@Data
@Accessors(chain = true)
public class UserEditVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    private String userName;
    private String jobNumber;
    private String phone;
    private String realName;
    private String avatarUrl;
    private String sex;
    private String age;
    private String description;
    private String email;
    private String pinYin;
    private String province;
    private String city;
    private String area;
    private String detail;
    private Integer status;
    private Integer shareLogin;
    // 已选岗位
    private List<String> postIds;
    // 岗位列表
    private List<PostSelectVO> postList;
    // 已选角色
    private List<String> roleIds;
    // 角色列表
    private List<RoleSelectVO> roleList;

    public static UserEditVO convertByEntity(SysUser user) {
        return BeanHelper.map(user, UserEditVO.class);
    }
}
