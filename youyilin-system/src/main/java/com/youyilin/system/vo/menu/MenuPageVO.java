package com.youyilin.system.vo.menu;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;

/**
 * 菜单列表VO
 */
@Data
@Accessors(chain = true)
public class MenuPageVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 创建时间
    private Date createDate;
    // 修改时间
    private Date modifyDate;
    // 菜单名称
    private String menuName;
    // 菜单编码
    private String menuCode;
    // 地址栏路径
    private String path;
    // 组件名称
    private String name;
    // 组件路径
    private String component;
    // 是否外链标记
    private Integer link;
    // 图标
    private String icon;
    // 排序
    private Integer sort;
    // 是否显示标记
    private Integer visible;
    // 状态
    private Integer status;
    // 描述
    private String description;
    // 类型
    private Integer type;
    // 权限
    private String perms;
    // 父级
    private Long pidId;
    // 下级菜单
    private List<MenuPageVO> children;
}
