package com.youyilin.system.vo.log;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.util.Date;

/**
 * 登录日志列表VO
 */
@Data
public class LoginLogPageVO {
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 登录名称
    private String userName;
    // 登录时间
    private Date loginDate;
    // 登录IP
    private String loginIp;
    // 登录地点
    private String loginLocation;
    // 登录类型
    private String loginType;
    // 设备
    private String device;
    // 浏览器
    private String browser;
    // 系统
    private String os;
    // 版本
    private String version;
    // 状态
    private String status;
    // 信息
    private String msg;
}
