package com.youyilin.system.vo.config;

import com.youyilin.system.enums.ConfigTypeEnum;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

@Data
public class SysConfigVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    private String name;
    private Integer type;
    private Integer sort;

    public String getTypeText() {
        return ConfigTypeEnum.queryInfoByCode(this.type);
    }
}
