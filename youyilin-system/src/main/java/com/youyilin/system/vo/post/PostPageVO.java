package com.youyilin.system.vo.post;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.util.Date;

/**
 * 岗位列表VO
 */
@Data
public class PostPageVO {
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 修改时间
    private Date modifyDate;
    // 岗位名称
    private String postName;
    // 岗位编码
    private String postCode;
    // 状态
    private Integer status;
    // 描述
    private String description;
    // 部门名称
    private String deptName;
}
