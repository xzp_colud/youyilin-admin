package com.youyilin.system.vo.menu;

import com.youyilin.common.entity.TreeNodes;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.system.entity.SysMenu;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 菜单编辑VO
 */
@Data
@Accessors(chain = true)
public class MenuEditVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 菜单名称
    private String menuName;
    // 菜单编码
    private String menuCode;
    // 地址栏路径
    private String path;
    // 组件名称
    private String name;
    // 前端组件地址
    private String component;
    // 是否外链标记
    private Integer link;
    // 图标
    private String icon;
    // 排序
    private Integer sort;
    // 是否显示标记
    private Integer visible;
    // 状态
    private Integer status;
    // 描述
    private String description;
    // 类型
    private Integer type;
    // 权限
    private String perms;
    // 所属上级
    @JsonSerialize(using = ToStringSerializer.class)
    private Long pidId;
    // 菜单结构
    private TreeNodes menuList;

    public static MenuEditVO convertByEntity(SysMenu menu) {
        return BeanHelper.map(menu, MenuEditVO.class);
    }
}
