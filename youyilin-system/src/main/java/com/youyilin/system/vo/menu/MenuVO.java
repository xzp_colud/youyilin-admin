package com.youyilin.system.vo.menu;

import com.youyilin.system.enums.MenuTypeEnum;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

/**
 * 菜单VO
 */
@Data
public class MenuVO {
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    private String menuName;
    private String menuCode;
    private String path;
    private String name;
    private String component;
    private Integer link;
    private String icon;
    private Integer sort;
    private Integer visible;
    private Integer status;
    private String description;
    private Integer type;
    private String perms;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long pidId;
    private String pidName;

    public String getTypeText() {
        return MenuTypeEnum.queryDescByCode(this.type);
    }
}
