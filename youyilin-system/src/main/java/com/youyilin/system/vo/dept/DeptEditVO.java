package com.youyilin.system.vo.dept;

import com.youyilin.common.entity.TreeNodes;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.system.entity.SysDept;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 部门编辑VO
 */
@Data
@Accessors(chain = true)
public class DeptEditVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 名称
    private String deptName;
    // 编码
    private String deptCode;
    // 状态
    private Integer status;
    // 描述
    private String description;
    // 负责人
    private String leader;
    // 负责人电话
    private String phone;
    // 负责人邮箱
    private String email;
    // 所属上级部门
    @JsonSerialize(using = ToStringSerializer.class)
    private Long pidId;
    // 所有部门结构
    private List<TreeNodes> deptList;

    public static DeptEditVO convertByEntity(SysDept dept) {
        return BeanHelper.map(dept, DeptEditVO.class);
    }
}
