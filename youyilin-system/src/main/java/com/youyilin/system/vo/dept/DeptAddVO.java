package com.youyilin.system.vo.dept;

import com.youyilin.common.entity.TreeNodes;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 部门新增VO
 */
@Data
@Accessors(chain = true)
public class DeptAddVO {
    // 所有部门结构列表
    private List<TreeNodes> deptList;
}
