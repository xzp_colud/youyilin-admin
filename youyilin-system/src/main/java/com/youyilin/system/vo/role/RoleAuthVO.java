package com.youyilin.system.vo.role;

import com.youyilin.common.entity.TreeNodes;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 角色授权VO
 */
@Data
@Accessors(chain = true)
public class RoleAuthVO {

    // 当前角色
    @JsonSerialize(using = ToStringSerializer.class)
    private Long roleId;
    // 所有菜单列表
    private List<TreeNodes> menuList;
    // 已授权菜单ID
    private List<String> menuIds;
}
