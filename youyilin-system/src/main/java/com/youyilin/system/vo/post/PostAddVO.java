package com.youyilin.system.vo.post;

import com.youyilin.common.entity.TreeNodes;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 岗位新增VO
 */
@Data
@Accessors(chain = true)
public class PostAddVO {

    // 部门结构列表
    private List<TreeNodes> deptList;
}
