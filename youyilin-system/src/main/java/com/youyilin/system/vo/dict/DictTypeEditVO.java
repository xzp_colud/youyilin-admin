package com.youyilin.system.vo.dict;

import com.youyilin.common.utils.BeanHelper;
import com.youyilin.system.entity.SysDictType;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 字典编辑VO
 */
@Data
@Accessors(chain = true)
public class DictTypeEditVO {
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    private String dictName;
    private String dictType;
    private Integer status;
    private String description;

    public static DictTypeEditVO convertByEntity(SysDictType dictType) {
        return BeanHelper.map(dictType, DictTypeEditVO.class);
    }
}
