package com.youyilin.system.vo.dict;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 字典列表VO
 */
@Data
@Accessors(chain = true)
public class DictTypePageVO {
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    private Date modifyDate;
    private String dictName;
    private String dictType;
    private Integer status;
    private String description;
}
