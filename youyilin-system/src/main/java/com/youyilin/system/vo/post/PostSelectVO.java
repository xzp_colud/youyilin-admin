package com.youyilin.system.vo.post;

import com.youyilin.common.enums.StatusEnum;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.system.entity.SysPost;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 岗位下拉列表VO
 */
@Data
@Accessors(chain = true)
public class PostSelectVO {
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 岗位名称
    private String postName;
    // 部门名称
    private String deptName;
    // 状态
    private Integer status;
    // 是否禁用
    public boolean isDisabled() {
        return this.status == null || this.status != StatusEnum.NORMAL.getCode();
    }

    public static List<PostSelectVO> convertByEntity(List<SysPost> list) {
        return BeanHelper.map(list, PostSelectVO.class);
    }
}
