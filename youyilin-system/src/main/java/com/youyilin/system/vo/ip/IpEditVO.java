package com.youyilin.system.vo.ip;

import com.youyilin.common.utils.BeanHelper;
import com.youyilin.system.entity.SysIp;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

/**
 * IP编辑VO
 */
@Data
public class IpEditVO {

    // 当前IP ID
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // IP
    private String ip;
    // 类型
    private Integer type;
    // 状态
    private Integer status;

    public static IpEditVO convertByEntity(SysIp ip) {
        return BeanHelper.map(ip, IpEditVO.class);
    }
}
