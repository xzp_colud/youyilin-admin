package com.youyilin.system.vo.user;

import com.youyilin.system.vo.post.PostSelectVO;
import com.youyilin.system.vo.role.RoleSelectVO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 用户新增VO
 */
@Data
@Accessors(chain = true)
public class UserAddVO {
    // 岗位列表
    private List<PostSelectVO> postList;
    // 角色列表
    private List<RoleSelectVO> roleList;
}
