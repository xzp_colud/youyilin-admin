package com.youyilin.system.enums;

/**
 * 异常类型
 */
public enum ErrorTypeEnum {
    // 自定义异常
    CUSTOM,
    // 系统异常
    SYSTEM,
}
