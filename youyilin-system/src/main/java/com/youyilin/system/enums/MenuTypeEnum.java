package com.youyilin.system.enums;

public enum MenuTypeEnum {
    CATALOG(1, "目录"),
    MENU(2, "菜单"),
    BUTTON(3, "按钮");

    private int code;
    private String desc;

    MenuTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static String queryDescByCode(Integer code) {
        if (code == null) return null;

        for (MenuTypeEnum t : MenuTypeEnum.values()) {
            if (t.getCode() == code) {
                return t.getDesc();
            }
        }
        return null;
    }
}
