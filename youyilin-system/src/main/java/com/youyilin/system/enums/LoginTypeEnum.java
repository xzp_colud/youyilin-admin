package com.youyilin.system.enums;

public enum LoginTypeEnum {
    PASSWORD,
    PHONE,
    EMAIL,
    SMS,
    MINI,
}
