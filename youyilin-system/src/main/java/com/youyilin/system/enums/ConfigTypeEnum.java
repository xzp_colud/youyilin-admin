package com.youyilin.system.enums;

/**
 * 配置类型
 */
public enum ConfigTypeEnum {

    TEXT(1, "单行文本"),
    TEXT_AREA(2, "多行文本"),
    TEST_PASSWORD(3, "密码"),
    TEST_NUMBER(4, "数字"),
    CHECKBOX(5, "多选框"),
    RADIO(6, "单选框"),
    DATE(7, "日期"),
    IMAGE(8, "图片"),
    SELECT(9, "下拉框");

    private int code;
    private String info;

    ConfigTypeEnum(int code, String info) {
        this.code = code;
        this.info = info;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public static boolean isExistByCode(Integer code) {
        if (code == null) return Boolean.FALSE;

        for (ConfigTypeEnum t : ConfigTypeEnum.values()) {
            if (t.getCode() == code) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    public static String queryInfoByCode(Integer code) {
        if (code == null) return null;

        for (ConfigTypeEnum t : ConfigTypeEnum.values()) {
            if (t.getCode() == code) {
                return t.getInfo();
            }
        }
        return null;
    }
}
