package com.youyilin.system.enums;

/**
 * IP类型
 */
public enum IpTypeEnum {
    WHITE(1, "白名单"),
    BLANK(2, "黑名单");

    private int code;
    private String desc;

    IpTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static String queryDescByCode(Integer code) {
        if (code == null) return null;

        for (IpTypeEnum t : IpTypeEnum.values()) {
            if (t.getCode() == code) {
                return t.getDesc();
            }
        }
        return null;
    }
}
