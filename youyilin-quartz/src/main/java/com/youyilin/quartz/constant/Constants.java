package com.youyilin.quartz.constant;

/**
 * 通用常量信息
 */
public class Constants {
    /**
     * http请求
     */
    public static final String HTTP = "http://";

    /**
     * https请求
     */
    public static final String HTTPS = "https://";

    /**
     * 通用成功标识
     */
    public static final Integer SUCCESS = 1;

    /**
     * 通用失败标识
     */
    public static final Integer FAIL = 0;

    /**
     * RMI 远程方法调用
     */
    public static final String LOOKUP_RMI = "rmi://";


    /**
     * LDAP 远程方法调用
     */
    public static final String LOOKUP_LDAP = "ldap://";
}
