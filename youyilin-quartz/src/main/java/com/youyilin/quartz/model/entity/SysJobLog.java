package com.youyilin.quartz.model.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 定时任务调度日志
 */
@Data
@Accessors(chain = true)
@TableName("sys_job_log")
public class SysJobLog implements Serializable {
    private static final long serialVersionUID = 1388955439845273781L;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 任务id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "job_id")
    private Long jobId;

    /**
     * 任务名称
     */
    @TableField(value = "job_name")
    private String jobName;

    /**
     * 任务组名
     */
    @TableField(value = "job_group")
    private String jobGroup;

    /**
     * 调用目标字符串
     */
    @TableField(value = "invoke_target")
    private String invokeTarget;

    /**
     * 日志信息
     */
    @TableField(value = "job_message")
    private String jobMessage;

    /**
     * 执行状态（0正常 1失败）
     */
    private Integer status;

    /**
     * 异常信息
     */
    @TableField(value = "exception_info")
    private String exceptionInfo;
}
