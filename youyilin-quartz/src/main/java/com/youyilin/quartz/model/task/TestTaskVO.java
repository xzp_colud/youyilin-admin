package com.youyilin.quartz.model.task;

import lombok.Data;

@Data
public class TestTaskVO extends BaseTaskVO {

    private String testParams;
}
