package com.youyilin.quartz.model.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.youyilin.quartz.constant.ScheduleConstants;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

/**
 * 定时任务调度表
 */
@Data
@Accessors(chain = true)
@TableName("sys_job")
public class SysJob implements Serializable {
    private static final long serialVersionUID = -5855592720995399917L;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    @TableField(value = "create_date", fill = FieldFill.INSERT)
    private Date createDate;
    @TableField(fill = FieldFill.INSERT)
    private Long creator;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 任务名称
     */
    @NotBlank(message = "任务名称不能为空")
    @TableField(value = "job_name")
    private String jobName;

    /**
     * 任务组名
     */
    @TableField(value = "job_group")
    private String jobGroup;

    /**
     * 调用目标字符串
     */
    @NotBlank(message = "目标字符串不能为空")
    @TableField(value = "invoke_target")
    private String invokeTarget;

    /**
     * cron执行表达式
     */
    @NotBlank(message = "CRON表达式不能为空")
    @TableField(value = "cron")
    private String cron;

    /**
     * 下次执行时间
     */
    @TableField(value = "next_time")
    private long nextTime;

    /**
     * cron计划策略
     */
    @TableField(value = "misfire_policy")
    private String misfirePolicy = ScheduleConstants.MISFIRE_DEFAULT;

    /**
     * 是否并发执行（0允许 1禁止）
     */
    private Integer concurrent;

    /**
     * 任务状态（0正常 1暂停）
     */
    private String status;

    /**
     * 备注
     */
    private String remark;

    /**
     * 执行次数
     */
    @TableField(value = "run_num")
    private Integer runNum;

    @TableLogic
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;
}
