package com.youyilin.quartz.model.task;

import lombok.Data;

@Data
public abstract class BaseTaskVO {

    private String jobId;
    private String cron;
}
