package com.youyilin.quartz.task;

import com.youyilin.common.utils.SpringUtil;
import com.youyilin.goods.mapper.ProductSubMaterialMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 同步商品-方案-辅料价格
 */
@Slf4j
@Component("asyncProductSubMaterialPriceTask")
public class AsyncProductSubMaterialPriceTask {

    public void asyncPrice() {
        ProductSubMaterialMapper productSubMaterialMapper = SpringUtil.getBean(ProductSubMaterialMapper.class);
        productSubMaterialMapper.asyncPrice();
    }
}
