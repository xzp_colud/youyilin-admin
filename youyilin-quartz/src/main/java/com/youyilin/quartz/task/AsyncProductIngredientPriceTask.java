package com.youyilin.quartz.task;

import com.youyilin.common.utils.SpringUtil;
import com.youyilin.goods.mapper.ProductIngredientMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 同步商品-方案-配料价格
 */
@Slf4j
@Component("asyncProductIngredientPriceTask")
public class AsyncProductIngredientPriceTask {

    public void asyncPrice() {
        ProductIngredientMapper productIngredientMapper = SpringUtil.getBean(ProductIngredientMapper.class);
        productIngredientMapper.asyncPrice();
    }
}
