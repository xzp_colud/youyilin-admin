package com.youyilin.quartz.task;

import com.youyilin.common.exception.TaskException;
import com.youyilin.common.utils.SpringUtil;
import com.youyilin.quartz.constant.ScheduleConstants;
import com.youyilin.quartz.model.entity.SysJob;
import com.youyilin.quartz.service.SysJobService;
import org.quartz.SchedulerException;

public abstract class TaskAbstract<T> {

    public void doCreate(String invokeTarget, String cron, String jobName, String jobGroup) throws SchedulerException, TaskException {
        SysJob en = new SysJob();
        en.setJobName(jobName);
        en.setJobGroup(jobGroup);
        en.setCron(cron);
        en.setInvokeTarget(invokeTarget);
        en.setStatus(ScheduleConstants.Status.NORMAL.getValue());

        SysJobService sysJobService = SpringUtil.getBean(SysJobService.class);
        sysJobService.saveJob(en);
    }

    /**
     * 创建
     */
    protected abstract void create(T obj) throws SchedulerException, TaskException;

    /**
     * 执行
     */
    protected abstract void run(Object obj);

    public void doRemove(String jobName) throws SchedulerException {
        SysJobService sysJobService = SpringUtil.getBean(SysJobService.class);
        sysJobService.removeJobByJobName(jobName);
    }

    /**
     * 移除
     */
    protected abstract void remove(String quartzId) throws SchedulerException;
}
