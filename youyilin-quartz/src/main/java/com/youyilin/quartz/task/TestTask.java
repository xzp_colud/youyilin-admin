package com.youyilin.quartz.task;

import com.youyilin.common.exception.TaskException;
import com.youyilin.quartz.model.task.TestTaskVO;
import lombok.extern.slf4j.Slf4j;
import org.quartz.SchedulerException;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class TestTask extends TaskAbstract<TestTaskVO> {

    private final static String SYS_JOB_NAME = "TEST_NAME";
    private final static String SYS_JOB_GROUP = "TEST_GROUP";
    private final static String INVOKE_TARGET = "com.youyilin.quartz.task.TestTask.run";

    @Override
    public void create(TestTaskVO vo) throws SchedulerException, TaskException {
        String params = vo.getTestParams();
        String jobName = SYS_JOB_NAME + "_" + vo.getJobId();
        String jobGroup = SYS_JOB_GROUP + "_" + vo.getJobId();
        super.doCreate(INVOKE_TARGET.concat("('" + params + "')"), vo.getCron(), jobName, jobGroup);
    }

    public void run(String o) {
        log.info("【TestTask】 {}", o);
    }

    @Override
    public void run(Object o) {
        String test = (String) o;
        log.info("【TestTask】 {}", test);
    }

    @Override
    protected void remove(String jobId) throws SchedulerException {
        String jobName = SYS_JOB_NAME + '_' + jobId;
        super.doRemove(jobName);
    }
}
