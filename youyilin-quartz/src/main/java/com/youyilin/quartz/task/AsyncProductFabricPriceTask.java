package com.youyilin.quartz.task;

import com.youyilin.common.utils.SpringUtil;
import com.youyilin.goods.mapper.ProductFabricMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 同步商品-方案-面料价格
 */
@Slf4j
@Component("asyncProductFabricPriceTask")
public class AsyncProductFabricPriceTask {

    public void asyncPrice() {
        ProductFabricMapper productFabricMapper = SpringUtil.getBean(ProductFabricMapper.class);
        productFabricMapper.asyncPrice();
    }
}
