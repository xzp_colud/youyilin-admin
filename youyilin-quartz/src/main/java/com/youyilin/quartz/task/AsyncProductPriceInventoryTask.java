package com.youyilin.quartz.task;

import com.youyilin.common.utils.SpringUtil;
import com.youyilin.warehouse.service.InventoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 同步商品SKU-库存信息
 */
@Slf4j
@Component("asyncProductPriceInventoryTask")
public class AsyncProductPriceInventoryTask {

    public void asyncInventory() {
        InventoryService inventoryService = SpringUtil.getBean(InventoryService.class);
        inventoryService.asyncInventory();
    }
}
