package com.youyilin.quartz.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 定时任务调度测试
 */
@Component("demoTask")
public class DemoTask {

    private static final Logger logger = LoggerFactory.getLogger(DemoTask.class);

    public String multipleParams(String s, Boolean b, Long l, Double d, Integer i) {
        logger.info("执行多参方法： 字符串类型{}，布尔类型{}，长整型{}，浮点型{}，整形{}", s, b, l, d, i);
        return "sdfsdfsdf";
    }

    public void params(String params) {
        System.out.println("执行有参方法：" + params);
    }

    public void noParams() {
        System.out.println("执行无参方法");
    }
}
