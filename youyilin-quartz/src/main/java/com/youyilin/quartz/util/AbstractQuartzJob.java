package com.youyilin.quartz.util;

import java.util.Date;

import com.youyilin.common.utils.ExceptionUtil;
import com.youyilin.common.utils.SpringUtil;
import com.youyilin.quartz.constant.Constants;
import com.youyilin.quartz.constant.ScheduleConstants;
import com.youyilin.quartz.model.entity.SysJob;
import com.youyilin.quartz.model.entity.SysJobLog;
import com.youyilin.quartz.service.SysJobLogService;
import com.youyilin.quartz.service.SysJobService;
import org.apache.commons.lang3.StringUtils;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 抽象quartz调用
 */
public abstract class AbstractQuartzJob implements Job {
    private static final Logger log = LoggerFactory.getLogger(AbstractQuartzJob.class);

    /**
     * 线程本地变量
     */
    private static ThreadLocal<Date> threadLocal = new ThreadLocal<>();

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        SysJob sysJob = new SysJob();
        BeanUtils.copyBeanProp(sysJob, context.getMergedJobDataMap().get(ScheduleConstants.TASK_PROPERTIES));
        try {
            before(context, sysJob);
            doExecute(context, sysJob);
            after(context, sysJob, null);
        } catch (Exception e) {
            log.error("【任务执行异常】 {}：", sysJob.getId(), e);
            after(context, sysJob, e);
        }
    }

    /**
     * 执行前
     *
     * @param context 工作执行上下文对象
     * @param sysJob  系统计划任务
     */
    protected void before(JobExecutionContext context, SysJob sysJob) {
        threadLocal.set(new Date());
    }

    /**
     * 执行后
     *
     * @param context 工作执行上下文对象
     * @param sysJob  系统计划任务
     */
    protected void after(JobExecutionContext context, SysJob sysJob, Exception e) {
        Date startTime = threadLocal.get();
        threadLocal.remove();

        final SysJobLog sysJobLog = new SysJobLog();
        sysJobLog.setJobId(sysJob.getId());
        sysJobLog.setJobName(sysJob.getJobName());
        sysJobLog.setJobGroup(sysJob.getJobGroup());
        sysJobLog.setInvokeTarget(sysJob.getInvokeTarget());
        sysJobLog.setModifyDate(new Date());
        long runMs = sysJobLog.getModifyDate().getTime() - startTime.getTime();
        sysJobLog.setJobMessage(sysJobLog.getJobName() + " 总共耗时：" + runMs + "毫秒");
        if (e != null) {
            sysJobLog.setStatus(Constants.FAIL);
            String errorMsg = StringUtils.substring(ExceptionUtil.getExceptionMessage(e), 0, 2000);
            sysJobLog.setExceptionInfo(errorMsg);

            // TODO 异常 可以发送通知
        } else {
            sysJobLog.setStatus(Constants.SUCCESS);
        }
        // 更新下次执行时间
        SysJob updateJob = new SysJob();
        updateJob.setId(sysJob.getId());
        updateJob.setNextTime(CronUtils.getNextExecution(sysJob.getCron()).getTime());
        updateJob.setRunNum(sysJob.getRunNum() == null ? 1 : sysJob.getRunNum() + 1);

        // 写入数据库当中
        SpringUtil.getBean(SysJobService.class).updateById(updateJob);
        SpringUtil.getBean(SysJobLogService.class).save(sysJobLog);
    }

    /**
     * 执行方法，由子类重载
     *
     * @param context 工作执行上下文对象
     * @param sysJob  系统计划任务
     * @throws Exception 执行过程中的异常
     */
    protected abstract void doExecute(JobExecutionContext context, SysJob sysJob) throws Exception;
}
