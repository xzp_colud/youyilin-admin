package com.youyilin.quartz.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.common.bean.Page;
import com.youyilin.quartz.model.entity.SysJob;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface SysJobMapper extends BaseMapper<SysJob> {

    Integer getTotal(Page<SysJob> page);

    List<SysJob> getPage(Page<SysJob> page);
}
