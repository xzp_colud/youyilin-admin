package com.youyilin.quartz.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.common.bean.Page;
import com.youyilin.quartz.model.entity.SysJobLog;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface SysJobLogMapper extends BaseMapper<SysJobLog> {

    Integer getTotal(Page<SysJobLog> page);

    List<SysJobLog> getPage(Page<SysJobLog> page);
}
