package com.youyilin.quartz.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.bean.Page;
import com.youyilin.quartz.model.entity.SysJobLog;
import com.youyilin.quartz.service.SysJobLogService;
import com.youyilin.quartz.mapper.SysJobLogMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 定时任务调度日志信息
 */
@Service
public class SysJobLogImpl extends ServiceImpl<SysJobLogMapper, SysJobLog> implements SysJobLogService {

    @Override
    public Integer getTotal(Page<SysJobLog> page) {
        return baseMapper.getTotal(page);
    }

    @Override
    public List<SysJobLog> getPage(Page<SysJobLog> page) {
        return baseMapper.getPage(page);
    }

    @Override
    @Transactional
    public void clear(Long jobId) {
        this.remove(new LambdaQueryWrapper<SysJobLog>().eq(SysJobLog::getJobId, jobId));
    }
}
