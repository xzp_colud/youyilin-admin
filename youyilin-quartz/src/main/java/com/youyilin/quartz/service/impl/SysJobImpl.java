package com.youyilin.quartz.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.bean.Page;
import com.youyilin.common.exception.ApiException;
import com.youyilin.common.exception.TaskException;
import com.youyilin.common.utils.ObjectUniqueUtil;
import com.youyilin.quartz.constant.ScheduleConstants;
import com.youyilin.quartz.mapper.SysJobMapper;
import com.youyilin.quartz.service.SysJobService;
import com.youyilin.quartz.util.CronUtils;
import com.youyilin.quartz.util.ScheduleUtils;
import com.youyilin.quartz.model.entity.SysJob;
import org.apache.commons.lang3.StringUtils;
import org.quartz.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 定时任务调度信息 服务层
 */
@Service
public class SysJobImpl extends ServiceImpl<SysJobMapper, SysJob> implements SysJobService {

    private final Scheduler scheduler;

    public SysJobImpl(Scheduler scheduler) {
        this.scheduler = scheduler;
    }

//    /**
//     * 项目启动时，初始化定时器 主要是防止手动修改数据库导致未同步到定时任务处理（注：不能手动修改数据库ID和任务组名，否则会导致脏数据）
//     * 已弃用改方法 改成全局启动调用
//     */
//    @PostConstruct
//    public void init() throws SchedulerException, TaskException {
//        scheduler.clear();
//        List<SysJobEntity> jobList = this.listAllJob();
//        for (SysJobEntity job : jobList) {
//            ScheduleUtils.createScheduleJob(scheduler, job);
//            if (job.getStatus().equals(ScheduleConstants.Status.NORMAL.getValue()) && job.getNextTime() > 0) {
//                if (job.getNextTime() < System.currentTimeMillis()) {
//                    this.updateSysJobRun(job.getId());
//                }
//            }
//        }
//    }

    @Override
    public Integer getTotal(Page<SysJob> page) {
        return baseMapper.getTotal(page);
    }

    @Override
    public List<SysJob> getPage(Page<SysJob> page) {
        return baseMapper.getPage(page);
    }

    @Override
    public List<SysJob> listAllJob() {
        return this.list();
    }

    @Override
    public SysJob getByJobName(String jobName) {
        return this.getOne(new LambdaQueryWrapper<SysJob>()
                .eq(SysJob::getJobName, jobName));
    }

    @Override
    public void saveJob(SysJob en) throws SchedulerException, TaskException {
        if (StringUtils.isBlank(en.getJobGroup())) {
            en.setJobGroup("默认");
        }
        // 验证任务名称唯一性
        this.validateJobNameUnique(en);
        // 验证表达式
        if (!CronUtils.isValid(en.getCron())) {
            throw new ApiException("CRON表达式格式错误");
        }
        // 设置下次执行时间
        en.setNextTime(CronUtils.getNextExecution(en.getCron()).getTime());
        if (en.getId() == null) {
            en.setRunNum(0);
            en.setConcurrent(1);
            boolean rows = this.save(en);
            if (rows) {
                ScheduleUtils.createScheduleJob(scheduler, en);
            }
        } else {
            boolean rows = this.updateById(en);
            if (rows) {
                JobKey jobKey = ScheduleUtils.getJobKey(en.getId(), en.getJobGroup());
                if (scheduler.checkExists(jobKey)) {
                    // 防止创建时存在数据问题 先移除，然后在执行创建操作
                    scheduler.deleteJob(jobKey);
                }
                ScheduleUtils.createScheduleJob(scheduler, en);
            }
        }
    }

    @Override
    @Transactional
    public void updateSysJobRun(Long id) throws SchedulerException {
        SysJob sysJob = this.getById(id);
        if (sysJob == null) {
            throw new ApiException("执行失败");
        }
        JobDataMap dataMap = new JobDataMap();
        dataMap.put(ScheduleConstants.TASK_PROPERTIES, sysJob);
        scheduler.triggerJob(ScheduleUtils.getJobKey(sysJob.getId(), sysJob.getJobGroup()), dataMap);
    }

    @Override
    @Transactional
    public void updateStatus(Long id, String status) throws SchedulerException, TaskException {
        SysJob select = this.getById(id);
        if (select == null) {
            throw new ApiException("执行失败");
        }

        SysJob up = new SysJob();
        up.setId(select.getId());
        up.setNextTime(CronUtils.getNextExecution(select.getCron()).getTime());
        JobKey jobKey = ScheduleUtils.getJobKey(select.getId(), select.getJobGroup());
        if (ScheduleConstants.Status.NORMAL.getValue().equals(String.valueOf(status))) {
            select.setStatus(ScheduleConstants.Status.NORMAL.getValue());
            boolean rows = this.updateById(select);
            if (rows) {
                if (scheduler.checkExists(jobKey)) {
                    scheduler.resumeJob(jobKey);
                } else {
                    ScheduleUtils.createScheduleJob(scheduler, select);
                }
            }
        } else if (ScheduleConstants.Status.PAUSE.getValue().equals(String.valueOf(status))) {
            select.setStatus(ScheduleConstants.Status.PAUSE.getValue());
            boolean rows = this.updateById(select);
            if (rows) {
                scheduler.pauseJob(jobKey);
            }
        }
    }

    @Override
    @Transactional
    public void removeJobByJobName(String jobName) throws SchedulerException {
        SysJob sysJob = this.getByJobName(jobName);
        if (sysJob == null) {
            throw new ApiException("定时任务已移除");
        }

        boolean rows = this.update(new LambdaUpdateWrapper<SysJob>()
                .set(SysJob::getStatus, ScheduleConstants.Status.PAUSE.getValue())
                .eq(SysJob::getId, sysJob.getId()));
        if (rows) {
            scheduler.deleteJob(ScheduleUtils.getJobKey(sysJob.getId(), sysJob.getJobGroup()));
        }
    }

    @Override
    public void validateJobNameUnique(SysJob en) {
        SysJob jobNameUnique = this.getByJobName(en.getJobName());
        if (!ObjectUniqueUtil.validateObjectUnique(en, jobNameUnique)) {
            throw new ApiException("任务名称已存在");
        }
    }
}
