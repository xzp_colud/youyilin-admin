package com.youyilin.quartz.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.common.bean.Page;
import com.youyilin.quartz.model.entity.SysJobLog;

import java.util.List;

/**
 * 定时任务调度日志信息信息 服务层
 */
public interface SysJobLogService extends IService<SysJobLog> {

    Integer getTotal(Page<SysJobLog> page);

    List<SysJobLog> getPage(Page<SysJobLog> page);

    /**
     * 清空定时任务日志
     */
    void clear(Long jobId);
}
