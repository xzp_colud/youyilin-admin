package com.youyilin.quartz.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.common.bean.Page;
import com.youyilin.common.exception.TaskException;
import com.youyilin.quartz.model.entity.SysJob;
import org.quartz.SchedulerException;

import java.util.List;

/**
 * 定时任务调度信息信息 服务层
 */
public interface SysJobService extends IService<SysJob> {

    Integer getTotal(Page<SysJob> page);

    List<SysJob> getPage(Page<SysJob> page);

    /**
     * 获取所有定时任务
     */
    List<SysJob> listAllJob();

    /**
     * 通过定时任务名称查找
     */
    SysJob getByJobName(String jobName);

    /**
     * 保存
     */
    void saveJob(SysJob en) throws TaskException, SchedulerException;

    /**
     * 立即执行
     */
    void updateSysJobRun(Long id) throws SchedulerException;

    /**
     * 改变定时任务状态
     */
    void updateStatus(Long id, String status) throws SchedulerException, TaskException;

    /**
     * 移除定时任务 并设置成不在加入执行
     */
    void removeJobByJobName(String jobName) throws SchedulerException;

    /**
     * 验证任务名称唯一性
     */
    void validateJobNameUnique(SysJob en);
}
