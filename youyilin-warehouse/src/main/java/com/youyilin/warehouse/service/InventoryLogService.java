package com.youyilin.warehouse.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.warehouse.entity.InventoryLog;

import java.util.List;

/**
 * 库存日志 服务类
 */
public interface InventoryLogService extends IService<InventoryLog> {

    /**
     * 按库存查询
     *
     * @param inventoryId 库存ID
     * @return ArrayList
     */
    List<InventoryLog> listByInventoryId(Long inventoryId);

    /**
     * 按库存批次查询
     *
     * @param inventoryLotId 库存批次ID
     * @return ArrayList
     */
    List<InventoryLog> listByInventoryLotId(Long inventoryLotId);

    /**
     * 清理冻结库存
     */
    void clearFrozenQty(List<InventoryLog> clearList);
}
