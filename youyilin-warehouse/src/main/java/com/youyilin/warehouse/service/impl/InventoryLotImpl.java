package com.youyilin.warehouse.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.exception.Assert;
import com.youyilin.warehouse.entity.InventoryLot;
import com.youyilin.warehouse.mapper.InventoryLotMapper;
import com.youyilin.warehouse.service.InventoryLotService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 库存批次
 */
@Service
public class InventoryLotImpl extends ServiceImpl<InventoryLotMapper, InventoryLot> implements InventoryLotService {

    @Override
    public Map<Long, InventoryLot> mapByIds(List<Long> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return new HashMap<>();
        }
        List<InventoryLot> list = super.listByIds(ids);
        if (CollectionUtils.isEmpty(list)) {
            return new HashMap<>();
        }
        return list.stream().collect(Collectors.toMap(InventoryLot::getId, item -> item));
    }

    @Override
    public List<InventoryLot> listByInventoryId(Long inventoryId) {
        return super.list(new LambdaQueryWrapper<InventoryLot>().eq(InventoryLot::getInventoryId, inventoryId));
    }

    @Override
    public List<InventoryLot> listByInventoryIds(List<Long> inventoryIds) {
        if (CollectionUtils.isEmpty(inventoryIds)) {
            return new ArrayList<>();
        }
        return super.list(new LambdaQueryWrapper<InventoryLot>().in(InventoryLot::getInventoryId, inventoryIds));
    }

    @Override
    public List<InventoryLot> listQtyByInventoryIds(List<Long> inventoryIds) {
        return super.list(new LambdaQueryWrapper<InventoryLot>()
                .gt(InventoryLot::getQty, 0L)
                .in(InventoryLot::getInventoryId, inventoryIds));
    }

    @Override
    public Map<Long, List<InventoryLot>> mapByInventoryIds(List<Long> inventoryIds) {
        List<InventoryLot> list = this.listByInventoryIds(inventoryIds);
        if (CollectionUtils.isEmpty(list)) {
            return new HashMap<>();
        }
        return list.stream().collect(Collectors.groupingBy(InventoryLot::getInventoryId));
    }

    @Override
    public InventoryLot validateInventoryLot(Long id) {
        InventoryLot lot = this.getById(id);
        Assert.notNull(lot, "库存批次不存在");

        return lot;
    }

    @Override
    public void delByInventoryId(Long inventoryId) {
        super.remove(new LambdaQueryWrapper<InventoryLot>().eq(InventoryLot::getInventoryId, inventoryId));
    }

    @Override
    public void delByInventoryIds(List<Long> inventoryIds) {
        super.remove(new LambdaQueryWrapper<InventoryLot>().in(InventoryLot::getInventoryId, inventoryIds));
    }
}
