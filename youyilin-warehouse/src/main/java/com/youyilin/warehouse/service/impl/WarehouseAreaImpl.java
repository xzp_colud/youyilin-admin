package com.youyilin.warehouse.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.exception.Assert;
import com.youyilin.common.utils.ObjectUniqueUtil;
import com.youyilin.warehouse.entity.WarehouseArea;
import com.youyilin.warehouse.mapper.WarehouseAreaMapper;
import com.youyilin.warehouse.service.WarehouseAreaService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 仓库库区 实现类
 */
@Service
public class WarehouseAreaImpl extends ServiceImpl<WarehouseAreaMapper, WarehouseArea> implements WarehouseAreaService {

    @Override
    public Map<Long, WarehouseArea> mapByIds(List<Long> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return new HashMap<>();
        }
        List<WarehouseArea> list = super.listByIds(ids);
        if (CollectionUtils.isEmpty(list)) {
            return new HashMap<>();
        }
        return list.stream().collect(Collectors.toMap(WarehouseArea::getId, warehouseArea -> warehouseArea));
    }

    @Override
    public WarehouseArea getByWarehouseIdAndSn(Long warehouseId, String sn) {
        return super.getOne(new LambdaQueryWrapper<WarehouseArea>()
                .eq(WarehouseArea::getWarehouseId, warehouseId)
                .eq(WarehouseArea::getSn, sn));
    }

    @Override
    public List<WarehouseArea> listAllByWarehouseId(Long warehouseId) {
        return super.list(new LambdaQueryWrapper<WarehouseArea>()
                .eq(WarehouseArea::getWarehouseId, warehouseId));
    }

    @Override
    public void delWarehouseAreaByWarehouseId(Long warehouseId) {
        super.remove(new LambdaQueryWrapper<WarehouseArea>()
                .eq(WarehouseArea::getWarehouseId, warehouseId));
    }

    @Override
    public void validateSnUnique(WarehouseArea en) {
        WarehouseArea snUnique = this.getByWarehouseIdAndSn(en.getWarehouseId(), en.getSn());
        Assert.isTrue(ObjectUniqueUtil.validateObjectUnique(en, snUnique), "库区编号已存在");
    }

    @Override
    public WarehouseArea validateWarehouseArea(Long warehouseAreaId) {
        WarehouseArea warehouseArea = super.getById(warehouseAreaId);
        Assert.notNull(warehouseArea, "库区不存在");

        return warehouseArea;
    }

    @Override
    public WarehouseArea validateWarehouseArea(Long warehouseId, Long warehouseAreaId) {
        boolean isTrue1 = warehouseId != null && warehouseAreaId != null;
        Assert.isTrue(isTrue1, "仓库/库区异常");
        // 查询库区
        WarehouseArea warehouseArea = super.getById(warehouseAreaId);
        // 断言：是否一致
        boolean isTrue2 = warehouseArea != null && warehouseArea.getWarehouseId().equals(warehouseId);
        Assert.isTrue(isTrue2, "仓库/库区异常");

        return warehouseArea;
    }

    @Override
    public List<WarehouseArea> listIdsByNames(List<String> areaNames) {
        return super.list(new LambdaQueryWrapper<WarehouseArea>()
                .select(WarehouseArea::getId, WarehouseArea::getName, WarehouseArea::getWarehouseId)
                .in(WarehouseArea::getName, areaNames));
    }
}
