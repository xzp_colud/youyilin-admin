package com.youyilin.warehouse.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.exception.Assert;
import com.youyilin.warehouse.entity.Inventory;
import com.youyilin.warehouse.mapper.InventoryMapper;
import com.youyilin.warehouse.service.InventoryService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 库存 实现类
 */
@Service
public class InventoryImpl extends ServiceImpl<InventoryMapper, Inventory> implements InventoryService {

    @Override
    public Map<Long, Inventory> mapByIds(List<Long> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return new HashMap<>();
        }
        List<Inventory> list = super.listByIds(ids);
        if (CollectionUtils.isEmpty(list)) {
            return new HashMap<>();
        }
        return list.stream().collect(Collectors.toMap(Inventory::getId, inventory -> inventory));
    }

    @Override
    public List<Inventory> listByWarehouseId(Long warehouseId) {
        return super.list(new LambdaQueryWrapper<Inventory>().eq(Inventory::getWarehouseId, warehouseId));
    }

    @Override
    public List<Inventory> listByWarehouseAreaId(Long warehouseAreaId) {
        return super.list(new LambdaQueryWrapper<Inventory>().eq(Inventory::getWarehouseAreaId, warehouseAreaId));
    }

    @Override
    public Map<Long, BigDecimal> sumQtyBySkuIds(List<Long> skuIds) {
        if (CollectionUtils.isEmpty(skuIds)) {
            return new HashMap<>();
        }
        List<Inventory> list = baseMapper.sumQtyBySkuIds(skuIds);
        if (CollectionUtils.isEmpty(list)) {
            return new HashMap<>();
        }
        return list.stream().collect(Collectors.toMap(Inventory::getSkuId, Inventory::getQty));
    }

    @Override
    public List<Inventory> listByWarehouseAreaIdAndSkuIds(Long warehouseAreaId, List<Long> skuIds) {
        if (warehouseAreaId == null || CollectionUtils.isEmpty(skuIds)) {
            return new ArrayList<>();
        }
        return super.list(new LambdaQueryWrapper<Inventory>()
                .eq(Inventory::getWarehouseAreaId, warehouseAreaId)
                .in(Inventory::getSkuId, skuIds));
    }

    @Override
    public List<Inventory> listBySkuIds(List<Long> skuIds) {
        if (CollectionUtils.isEmpty(skuIds)) {
            return new ArrayList<>();
        }
        return super.list(new LambdaQueryWrapper<Inventory>()
                .in(Inventory::getSkuId, skuIds));
    }

    @Override
    public int countProductNumByWarehouseId(Long warehouseId) {
        long num = super.count(new LambdaQueryWrapper<Inventory>()
                .eq(Inventory::getWarehouseId, warehouseId)
                .gt(Inventory::getQty, 0));
        return (int) num;
    }

    @Override
    public int countProductNumByWarehouseAreaId(Long warehouseAreaId) {
        long num = super.count(new LambdaQueryWrapper<Inventory>()
                .eq(Inventory::getWarehouseAreaId, warehouseAreaId)
                .gt(Inventory::getQty, 0));
        return (int) num;
    }

    @Override
    public Inventory validateInventory(Long inventoryId) {
        Inventory inventory = super.getById(inventoryId);
        Assert.notNull(inventory, "库存信息不存在");

        return inventory;
    }

    @Override
    public Inventory validateInventory(Long inventoryId, Long skuId) {
        Inventory inventory = super.getById(inventoryId);
        boolean isMatch = inventory != null && inventory.getSkuId().equals(skuId);
        Assert.isTrue(isMatch, "库存信息不匹配");

        return inventory;
    }

    @Override
    public void delByWarehouseId(Long warehouseId) {
        super.remove(new LambdaQueryWrapper<Inventory>().eq(Inventory::getWarehouseId, warehouseId));
    }

    @Override
    public void delByWarehouseAreaId(Long warehouseAreaId) {
        super.remove(new LambdaQueryWrapper<Inventory>().eq(Inventory::getWarehouseAreaId, warehouseAreaId));
    }

    @Override
    public void asyncInventory() {
        baseMapper.asyncInventoryZero();
        baseMapper.asyncInventory();
    }
}
