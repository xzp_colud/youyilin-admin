package com.youyilin.warehouse.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.warehouse.entity.WarehouseArea;

import java.util.List;
import java.util.Map;

/**
 * 仓库库区 服务类
 */
public interface WarehouseAreaService extends IService<WarehouseArea> {

    /**
     * 查询并分组
     *
     * @param ids 库区IDS
     * @return HashMap
     */
    Map<Long, WarehouseArea> mapByIds(List<Long> ids);

    /**
     * 按仓库和编号查询
     *
     * @param warehouseId 仓库ID
     * @param sn          编号
     * @return Warehouse
     */
    WarehouseArea getByWarehouseIdAndSn(Long warehouseId, String sn);

    /**
     * 某仓库所有库区
     *
     * @return ArrayList
     */
    List<WarehouseArea> listAllByWarehouseId(Long warehouseId);

    /**
     * 按仓库移除库区
     */
    void delWarehouseAreaByWarehouseId(Long warehouseId);

    /**
     * 验证编号唯一性
     */
    void validateSnUnique(WarehouseArea en);

    /**
     * 验证库区
     *
     * @param warehouseAreaId 库区ID
     */
    WarehouseArea validateWarehouseArea(Long warehouseAreaId);

    /**
     * 验证仓库/库区
     *
     * @param warehouseId     仓库ID
     * @param warehouseAreaId 库区ID
     */
    WarehouseArea validateWarehouseArea(Long warehouseId, Long warehouseAreaId);

    /**
     * 按名称查询
     *
     * @param areaNames 库区名称
     * @return ArrayList
     */
    List<WarehouseArea> listIdsByNames(List<String> areaNames);
}
