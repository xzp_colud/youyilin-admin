package com.youyilin.warehouse.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.warehouse.entity.Inventory;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 库存 服务类
 */
public interface InventoryService extends IService<Inventory> {

    /**
     * 查询并分组
     *
     * @param ids 库存IDS
     * @return HashMap
     */
    Map<Long, Inventory> mapByIds(List<Long> ids);

    /**
     * 按仓库查询
     *
     * @param warehouseId 仓库
     * @return ArrayList
     */
    List<Inventory> listByWarehouseId(Long warehouseId);

    /**
     * 按库区查询
     *
     * @param warehouseAreaId 库区
     * @return ArrayList
     */
    List<Inventory> listByWarehouseAreaId(Long warehouseAreaId);

    /**
     * 统计多个SKU现有库存量
     *
     * @param skuIds SKU ID
     * @return BigDecimal
     */
    Map<Long, BigDecimal> sumQtyBySkuIds(List<Long> skuIds);

    /**
     * 某仓库下某些SKU
     *
     * @param warehouseAreaId 仓库/库区
     * @param skuIds          SKU
     * @return ArrayList
     */
    List<Inventory> listByWarehouseAreaIdAndSkuIds(Long warehouseAreaId, List<Long> skuIds);

    /**
     * 按SKU列表查询
     *
     * @param skuIds SKU列表
     * @return ArrayList
     */
    List<Inventory> listBySkuIds(List<Long> skuIds);

    /**
     * 按仓库统计产品有余量的数量
     */
    int countProductNumByWarehouseId(Long warehouseId);

    /**
     * 按库区统计产品有余量的数量
     */
    int countProductNumByWarehouseAreaId(Long warehouseAreaId);

    /**
     * 验证库存
     *
     * @param inventoryId 库存信息ID
     * @return Inventory
     */
    Inventory validateInventory(Long inventoryId);

    /**
     * 验证库存
     *
     * @param inventoryId 库存信息ID
     * @param skuId       SKU ID
     * @return Inventory
     */
    Inventory validateInventory(Long inventoryId, Long skuId);

    /**
     * 按仓库移除库存信息
     */
    void delByWarehouseId(Long warehouseId);

    /**
     * 按库区移除库存信息
     */
    void delByWarehouseAreaId(Long warehouseAreaId);

    /**
     * 同步库存
     */
    void asyncInventory();
}
