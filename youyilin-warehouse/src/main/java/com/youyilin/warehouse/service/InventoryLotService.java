package com.youyilin.warehouse.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.warehouse.entity.InventoryLot;

import java.util.List;
import java.util.Map;

/**
 * 库存批次
 */
public interface InventoryLotService extends IService<InventoryLot> {

    /**
     * 查询并分组
     *
     * @param ids 批次IDS
     */
    Map<Long, InventoryLot> mapByIds(List<Long> ids);

    /**
     * 根据库存ID查询
     *
     * @param inventoryId 库存ID
     * @return ArrayList
     */
    List<InventoryLot> listByInventoryId(Long inventoryId);

    /**
     * 根据库存IDS查询
     *
     * @param inventoryIds 库存IDS
     * @return ArrayList
     */
    List<InventoryLot> listByInventoryIds(List<Long> inventoryIds);

    /**
     * 根据库存IDS查询有库存的批次
     *
     * @param inventoryIds 库存IDS
     * @return ArrayList
     */
    List<InventoryLot> listQtyByInventoryIds(List<Long> inventoryIds);

    /**
     * 根据库存IDS查询并分组
     *
     * @param inventoryIds 库存IDS
     * @return ArrayList
     */
    Map<Long, List<InventoryLot>> mapByInventoryIds(List<Long> inventoryIds);

    /**
     * 验证库存批次是否存在
     *
     * @param id 库存批次ID
     * @return InventoryLot
     */
    InventoryLot validateInventoryLot(Long id);

    /**
     * 按库存删除批次
     *
     * @param inventoryId 库存ID
     */
    void delByInventoryId(Long inventoryId);

    /**
     * 按库存删除批次
     *
     * @param inventoryIds 库存IDS
     */
    void delByInventoryIds(List<Long> inventoryIds);
}
