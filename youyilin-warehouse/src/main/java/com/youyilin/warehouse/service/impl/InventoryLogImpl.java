package com.youyilin.warehouse.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.exception.ApiException;
import com.youyilin.common.exception.Assert;
import com.youyilin.warehouse.entity.InventoryLog;
import com.youyilin.warehouse.mapper.InventoryLogMapper;
import com.youyilin.warehouse.service.InventoryLogService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 库存日志 实现类
 */
@Service
public class InventoryLogImpl extends ServiceImpl<InventoryLogMapper, InventoryLog> implements InventoryLogService {

    @Override
    public List<InventoryLog> listByInventoryId(Long inventoryId) {
        return super.list(new LambdaQueryWrapper<InventoryLog>().eq(InventoryLog::getInventoryId, inventoryId));
    }

    @Override
    public List<InventoryLog> listByInventoryLotId(Long inventoryLotId) {
        return super.list(new LambdaQueryWrapper<InventoryLog>().eq(InventoryLog::getInventoryLotId, inventoryLotId));
    }

    @Override
    public void clearFrozenQty(List<InventoryLog> clearList) {
//        // 库存操作日志
//        this.validateLog(clearList, InventoryLogTypeEnum.CLEAR_FROZEN_QTY.getCode(), true);
//        // 保存
//        super.saveBatch(clearList);
//
//        // 清理冻结库存
//        inventoryService.clearFrozenQty(BeanHelper.map(clearList, Inventory.class));
    }

    private void validateLog(List<InventoryLog> list, Integer insertType, boolean isNegate) {
        Assert.notNull(list, "库存异常");

        for (InventoryLog log : list) {
            if (log.getAmount() == null
                    || log.getSkuId() == null
                    || log.getProductId() == null
                    || log.getWarehouseId() == null
                    || log.getWarehouseAreaId() == null
                    || log.getQty() == null
                    || StringUtils.isBlank(log.getSerialNum())) {
                throw new ApiException("缺少必要参数");
            }
            log.setType(insertType);
            if (isNegate) {
                log.setQty(log.getQty().negate());
            }
        }
    }
}
