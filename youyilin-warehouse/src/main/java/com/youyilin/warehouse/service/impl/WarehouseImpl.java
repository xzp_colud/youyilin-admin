package com.youyilin.warehouse.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.exception.Assert;
import com.youyilin.common.utils.ObjectUniqueUtil;
import com.youyilin.warehouse.entity.Warehouse;
import com.youyilin.warehouse.mapper.WarehouseMapper;
import com.youyilin.warehouse.service.WarehouseService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 仓库 实现类
 */
@Service
public class WarehouseImpl extends ServiceImpl<WarehouseMapper, Warehouse> implements WarehouseService {

    @Override
    public Map<Long, Warehouse> mapByIds(List<Long> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return new HashMap<>();
        }
        List<Warehouse> list = super.listByIds(ids);
        if (CollectionUtils.isEmpty(list)) {
            return new HashMap<>();
        }
        return list.stream().collect(Collectors.toMap(Warehouse::getId, warehouse -> warehouse));
    }

    @Override
    public Warehouse getBySn(String sn) {
        return super.getOne(new LambdaQueryWrapper<Warehouse>().eq(Warehouse::getSn, sn));
    }

    @Override
    public void validateSnUnique(Warehouse en) {
        Warehouse snUnique = this.getBySn(en.getSn());
        Assert.isTrue(ObjectUniqueUtil.validateObjectUnique(en, snUnique), "仓库编号已存在");
    }

    @Override
    public Warehouse validateWarehouse(Long warehouseId) {
        Warehouse warehouse = super.getById(warehouseId);
        Assert.notNull(warehouse, "仓库不存在");

        return warehouse;
    }

    @Override
    public List<Warehouse> listIdsByNames(List<String> warehouseNames) {
        return super.list(new LambdaQueryWrapper<Warehouse>()
                .select(Warehouse::getId, Warehouse::getName)
                .in(Warehouse::getName, warehouseNames));
    }
}
