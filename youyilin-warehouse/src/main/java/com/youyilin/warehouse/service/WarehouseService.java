package com.youyilin.warehouse.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.warehouse.entity.Warehouse;

import java.util.List;
import java.util.Map;

/**
 * 仓库 服务类
 */
public interface WarehouseService extends IService<Warehouse> {

    /**
     * 查询并分组
     *
     * @param ids 仓库IDS
     * @return HashMap
     */
    Map<Long, Warehouse> mapByIds(List<Long> ids);

    /**
     * 按编号查询
     *
     * @param sn 仓库编号
     * @return Warehouse
     */
    Warehouse getBySn(String sn);

    /**
     * 验证编号唯一性
     */
    void validateSnUnique(Warehouse en);

    /**
     * 验证是否存在
     *
     * @param warehouseId 仓库ID
     * @return Warehouse
     */
    Warehouse validateWarehouse(Long warehouseId);

    /**
     * 按名称查询
     *
     * @param warehouseNames 仓库名称
     * @return ArrayList
     */
    List<Warehouse> listIdsByNames(List<String> warehouseNames);
}
