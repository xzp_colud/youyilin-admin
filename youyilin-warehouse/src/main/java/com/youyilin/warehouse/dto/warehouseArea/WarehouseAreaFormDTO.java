package com.youyilin.warehouse.dto.warehouseArea;

import com.youyilin.common.utils.BeanHelper;
import com.youyilin.warehouse.entity.WarehouseArea;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 仓库库区表单DTO
 */
@Data
public class WarehouseAreaFormDTO {

    private Long id;
    // 仓库
    @NotNull(message = "所属仓库不能为空")
    private Long warehouseId;
    // 名称
    @NotBlank(message = "名称不能为空")
    private String name;

    public WarehouseArea convertToEntity() {
        return BeanHelper.map(this, WarehouseArea.class);
    }
}
