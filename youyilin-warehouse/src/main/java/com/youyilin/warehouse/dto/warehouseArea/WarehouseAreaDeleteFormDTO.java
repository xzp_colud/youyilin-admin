package com.youyilin.warehouse.dto.warehouseArea;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * 仓库库区删除DTO
 */
@Data
@Accessors(chain = true)
public class WarehouseAreaDeleteFormDTO {

    @NotNull(message = "库区不能为空")
    private Long id;
}
