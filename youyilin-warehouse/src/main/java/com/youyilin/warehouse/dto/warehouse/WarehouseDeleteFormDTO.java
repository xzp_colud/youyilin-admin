package com.youyilin.warehouse.dto.warehouse;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * 仓库删除DTO
 */
@Data
@Accessors(chain = true)
public class WarehouseDeleteFormDTO {

    @NotNull(message = "仓库不能为空")
    private Long id;
}
