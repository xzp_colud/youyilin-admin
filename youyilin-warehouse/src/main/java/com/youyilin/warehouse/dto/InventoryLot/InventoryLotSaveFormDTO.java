package com.youyilin.warehouse.dto.InventoryLot;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * 盘点新增
 */
@Data
@Accessors(chain = true)
public class InventoryLotSaveFormDTO {

    @NotNull(message = "产品不能为空")
    private Long productId;
    @NotNull(message = "产品SKU不能为空")
    private Long skuId;
    @NotNull(message = "仓库不能为空")
    private Long warehouseId;
    @NotNull(message = "库区不能为空")
    private Long warehouseAreaId;
    private String lotSn;
    @NotNull(message = "盘点数量不能为空")
    private BigDecimal qty;
}
