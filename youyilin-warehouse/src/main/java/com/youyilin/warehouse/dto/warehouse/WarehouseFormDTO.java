package com.youyilin.warehouse.dto.warehouse;

import com.youyilin.common.utils.BeanHelper;
import com.youyilin.warehouse.entity.Warehouse;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 仓库表单DTO
 */
@Data
public class WarehouseFormDTO {

    private Long id;
    @NotBlank(message = "名称不能为空")
    private String name;

    public Warehouse convertToEntity() {
        return BeanHelper.map(this, Warehouse.class);
    }
}
