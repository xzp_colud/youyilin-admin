package com.youyilin.warehouse.dto.InventoryLot;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 批次移库
 */
@Data
public class InventoryLotMoveFormDTO {

    /**
     * 库存信息ID
     */
    @NotNull(message = "库存信息不能为空")
    private Long id;
    /**
     * 仓库ID
     */
    @NotNull(message = "移动库区不能为空")
    private Long warehouseId;
    /**
     * 库区ID
     */
    @NotNull(message = "移动库区不能为空")
    private Long warehouseAreaId;
}
