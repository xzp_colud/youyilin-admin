package com.youyilin.warehouse.dto.warehouse;

import lombok.Data;

/**
 * 仓库列表查询参数DTO
 */
@Data
public class WarehousePageQueryDTO {

    // 编号
    private String sn;
    // 名称
    private String name;
}
