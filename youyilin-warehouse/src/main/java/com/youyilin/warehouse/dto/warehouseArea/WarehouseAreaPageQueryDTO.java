package com.youyilin.warehouse.dto.warehouseArea;

import lombok.Data;

/**
 * 仓库库区列表查询参数DTO
 */
@Data
public class WarehouseAreaPageQueryDTO {

    // 仓库ID
    private Long warehouseId;
    // 编号
    private String sn;
    // 名称
    private String name;
    // 仓库名称
    private String warehouseName;
}
