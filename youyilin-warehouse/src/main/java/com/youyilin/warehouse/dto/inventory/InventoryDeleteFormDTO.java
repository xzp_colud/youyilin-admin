package com.youyilin.warehouse.dto.inventory;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 库存删除表单DTO
 */
@Data
public class InventoryDeleteFormDTO {

    @NotNull(message = "库存不能为空")
    private Long id;
}
