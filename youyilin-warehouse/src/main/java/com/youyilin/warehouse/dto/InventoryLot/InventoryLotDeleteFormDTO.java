package com.youyilin.warehouse.dto.InventoryLot;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 库存批次删除DTO
 */
@Data
public class InventoryLotDeleteFormDTO {

    @NotNull(message = "库存不能为空")
    private Long id;
}
