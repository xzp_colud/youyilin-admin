package com.youyilin.warehouse.dto.inventory;

import lombok.Data;

/**
 * 库存列表查询参数DTO
 */
@Data
public class InventoryPageQueryDTO {

    // 仓库ID
    private Long warehouseId;
    // 库区ID
    private Long warehouseAreaId;
    // 仓库名称
    private String warehouseName;
    // 库区名称
    private String warehouseAreaName;
    // 商品名称
    private String productName;
    // SKU名称
    private String skuName;
}
