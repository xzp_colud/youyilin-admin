package com.youyilin.warehouse.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * 出库DTO
 */
@Data
@Accessors(chain = true)
public class InventoryOutDTO {

    // 来源单号
    private String sourceSn;
    // 库存批次ID
    private Long id;
    // 出库数量
    private BigDecimal qty;
    // 商品ID
    private Long productId;
    private String productName;
    // sku ID
    private Long skuId;
    private String skuName;

    // ----- 返回数据 ----- //
    private Long inventoryId;
    private Long warehouseId;
    private String warehouseName;
    private Long warehouseAreaId;
    private String warehouseAreaName;
    private Long inventoryLotId;
    private String inventoryLotSn;
}
