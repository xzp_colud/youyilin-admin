package com.youyilin.warehouse.dto;

import com.youyilin.common.utils.BeanHelper;
import com.youyilin.warehouse.entity.InventoryLot;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * 入库DTO
 */
@Data
@Accessors(chain = true)
public class InventoryInDTO {

    private String sn;
    // 来源单号
    private String sourceSn;
    // 商品ID
    private Long productId;
    private String productName;
    // SKU ID
    private Long skuId;
    private String skuName;
    // 仓库ID
    private Long warehouseId;
    // 库区ID
    private Long warehouseAreaId;
    // 批次号
    private String lotSn;
    // 入库数量
    private BigDecimal qty;

    // ----- 返回数据 ----- //

    // 库存ID
    private Long inventoryId;
    // 批次
    private Long inventoryLotId;
    // 批次号
    private String inventoryLotSn;
    // 仓库名称
    private String warehouseName;
    // 库区名称
    private String warehouseAreaName;

    public InventoryLot convertToEntity() {
        return BeanHelper.map(this, InventoryLot.class);
    }
}
