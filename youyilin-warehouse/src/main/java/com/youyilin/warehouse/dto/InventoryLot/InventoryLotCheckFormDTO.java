package com.youyilin.warehouse.dto.InventoryLot;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * 出库盘点
 */
@Data
public class InventoryLotCheckFormDTO {

    /**
     * 库存批次ID
     */
    @NotNull(message = "库存信息不能为空")
    private Long id;
    /**
     * 出/入库 数量
     */
    @NotNull(message = "数量不能为空")
    private BigDecimal qty;
}
