package com.youyilin.warehouse.dto.inventoryLog;

import lombok.Data;

/**
 * 库存日志查询参数DTO
 */
@Data
public class InventoryLogPageQueryDTO {

    // 仓库名称
    private String warehouseName;
    // 仓库库区名称
    private String warehouseAreaName;
    // 产品名称
    private String productName;
    // sku名称
    private String skuName;
    // 批次号
    private String inventoryLotSn;
    // 自定义批次
    private String serialNum;
}
