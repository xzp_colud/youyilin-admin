package com.youyilin.warehouse.enums;

/**
 * 库存日志类型
 */
public enum InventoryLogTypeEnum {

    CLEAR_FROZEN_QTY(-1, "清除冻结"),
    PURCHASE_ORDER_IN(1, "采购入库"),
    OUTBOUND_ORDER_OUT(20, "出库单出库"),
    OUTBOUND_ORDER_OUT_TO_FROZE(21, "转出冻结"),
    OUTBOUND_ORDER_OUT_FROZE_IN(22, "转入冻结"),
    SALE_ORDER_OUT(30, "销售出库"),
    PROCESS_ORDER_OUT(40, "加工出库"),
    PROCESS_ORDER_IN(50, "加工入库"),
    CHECK_OUT(60, "盘点出库"),
    CHECK_IN(70, "盘点入库"),
    CHECK_IN_IMPORT(71, "盘点入库导入"),
    MOVE_OUT(80, "移库出库"),
    MOVE_IN(90, "移库入库"),
    DELETE(100, "删除"),
    DELETE_BY_INVENTORY(101, "通过库存删除"),
    DELETE_BY_WAREHOUSE(102, "通过仓库删除"),
    DELETE_BY_WAREHOUSE_AREA(103, "通过库区删除");

    private int code;
    private String info;

    InventoryLogTypeEnum(int code, String info) {
        this.code = code;
        this.info = info;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public static String queryInfoByCode(int code) {
        for (InventoryLogTypeEnum t : InventoryLogTypeEnum.values()) {
            if (t.getCode() == code) {
                return t.getInfo();
            }
        }
        return null;
    }

    public static boolean isOptFrozenQty(Integer code) {
        if (code == OUTBOUND_ORDER_OUT.code
                || code == CHECK_OUT.code
                || code == MOVE_OUT.code) {
            return false;
        }
        return true;
    }
}
