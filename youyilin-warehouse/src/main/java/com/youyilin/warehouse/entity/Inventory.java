package com.youyilin.warehouse.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 库存
 */
@Data
@Accessors(chain = true)
@TableName("warehouse_inventory")
public class Inventory implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField(value = "create_date", fill = FieldFill.INSERT)
    private Date createDate;
    @TableField(fill = FieldFill.INSERT)
    private Long creator;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 仓库ID
     */
    @TableField(value = "warehouse_id")
    private Long warehouseId;
    /**
     * 仓库库区ID
     */
    @TableField(value = "warehouse_area_id")
    private Long warehouseAreaId;
    /**
     * 产品ID
     */
    @TableField(value = "product_id")
    private Long productId;
    /**
     * SKU ID
     */
    @TableField(value = "sku_id")
    private Long skuId;
    /**
     * 库存数量
     */
    private BigDecimal qty;
    /**
     * 冻结库存
     */
    @TableField(value = "frozen_qty")
    private BigDecimal frozenQty;

    /**
     * 逻辑标记
     */
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;
}
