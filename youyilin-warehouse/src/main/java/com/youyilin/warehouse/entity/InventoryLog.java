package com.youyilin.warehouse.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 库存日志
 */
@Data
@Accessors(chain = true)
@TableName("warehouse_inventory_log")
public class InventoryLog implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;
    @TableField(value = "modify_name", fill = FieldFill.INSERT_UPDATE)
    private String modifyName;

    /**
     * 库存ID
     */
    @TableField(value = "inventory_id")
    private Long inventoryId;
    /**
     * 批次ID
     */
    @TableField(value = "inventory_lot_id")
    private Long inventoryLotId;
    /**
     * 批次号
     */
    @TableField(value = "inventory_lot_sn")
    private String inventoryLotSn;
    /**
     * 仓库ID
     */
    @TableField(value = "warehouse_id")
    private Long warehouseId;
    /**
     * 仓库名称
     */
    @TableField(value = "warehouse_name")
    private String warehouseName;
    /**
     * 库区ID
     */
    @TableField(value = "warehouse_area_id")
    private Long warehouseAreaId;
    /**
     * 库区名称
     */
    @TableField(value = "warehouse_area_name")
    private String warehouseAreaName;
    /**
     * 产品ID
     */
    @TableField(value = "product_id")
    private Long productId;
    /**
     * 商品名称
     */
    @TableField(value = "product_name")
    private String productName;
    /**
     * SKU ID
     */
    @TableField(value = "sku_id")
    private Long skuId;
    /**
     * sku名称
     */
    @TableField(value = "sku_name")
    private String skuName;
    /**
     * 来源号
     */
    @TableField(value = "serial_num")
    private String serialNum;
    /**
     * 操作类型
     */
    private Integer type;
    /**
     * 操作数量
     */
    private BigDecimal qty;
    /**
     * 剩余库存
     */
    @TableField(value = "residue_qty")
    private BigDecimal residueQty;
    /**
     * 金额(单价)
     */
    private Long amount;

    /**
     * 逻辑标记
     */
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;
}
