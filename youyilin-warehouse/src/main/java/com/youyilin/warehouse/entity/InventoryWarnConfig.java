//package com.youyilin.warehouse.entity;
//
//import com.baomidou.mybatisplus.annotation.FieldFill;
//import com.baomidou.mybatisplus.annotation.TableField;
//import com.baomidou.mybatisplus.annotation.TableName;
//import lombok.Data;
//import lombok.experimental.Accessors;
//
//import java.io.Serializable;
//import java.math.BigDecimal;
//import java.util.Date;
//
///**
// * TODO 库存预警配置
// */
//@Data
//@Accessors(chain = true)
//@TableName("inventory_warn")
//public class InventoryWarnConfig implements Serializable {
//    private static final long serialVersionUID = 1L;
//
//    private Long id;
//    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
//    private Date modifyDate;
//    @TableField(fill = FieldFill.INSERT_UPDATE)
//    private Long modifier;
//
//    /**
//     * 产品ID
//     */
//    @TableField(value = "product_id")
//    private Long productId;
//    /**
//     * 黄色预警线
//     */
//    @TableField(value = "warn_num")
//    private BigDecimal warnNum;
//    /**
//     * 开启标志
//     */
//    @TableField(value = "warn_flag")
//    private Integer warnFlag;
//    /**
//     * 红色预警线
//     */
//    @TableField(value = "error_num")
//    private BigDecimal errorNum;
//    /**
//     * 开启标志
//     */
//    @TableField(value = "error_flag")
//    private Integer errorFlag;
//}
