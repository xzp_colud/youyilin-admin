package com.youyilin.warehouse.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 库存批次
 */
@Data
@Accessors(chain = true)
@TableName("warehouse_inventory_lot")
public class InventoryLot implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField(value = "create_date", fill = FieldFill.INSERT)
    private Date createDate;
    @TableField(fill = FieldFill.INSERT)
    private Long creator;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    // 系统批次号
    private String sn;
    // 库存ID
    @TableField(value = "inventory_id")
    private Long inventoryId;
    // 自定义批次号
    @TableField(value = "lot_sn")
    private String lotSn;
    // 库存余量
    private BigDecimal qty;
    // 冻结数量
    @TableField(value = "frozen_qty")
    private BigDecimal frozenQty;
    // 逻辑标记
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;
}
