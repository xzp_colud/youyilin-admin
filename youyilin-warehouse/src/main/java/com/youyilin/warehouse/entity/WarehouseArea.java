package com.youyilin.warehouse.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 仓库库区
 */
@Data
@Accessors(chain = true)
@TableName("warehouse_area")
public class WarehouseArea implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 所属仓库ID
     */
    @TableField(value = "warehouse_id")
    private Long warehouseId;
    /**
     * 库区编号
     */
    private String sn;
    /**
     * 库区名称
     */
    private String name;
    /**
     * 备注
     */
    private String remark;
    /**
     * 逻辑标记
     */
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;
}
