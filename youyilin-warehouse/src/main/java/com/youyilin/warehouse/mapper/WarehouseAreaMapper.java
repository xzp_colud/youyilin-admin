package com.youyilin.warehouse.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.common.bean.Page;
import com.youyilin.warehouse.dto.warehouseArea.WarehouseAreaPageQueryDTO;
import com.youyilin.warehouse.entity.WarehouseArea;
import com.youyilin.warehouse.vo.warehouseArea.WarehouseAreaPageVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 仓库库区 Mapper
 */
@Mapper
public interface WarehouseAreaMapper extends BaseMapper<WarehouseArea> {

    Integer getTotal(Page<WarehouseAreaPageQueryDTO> page);

    List<WarehouseAreaPageVO> getPage(Page<WarehouseAreaPageQueryDTO> page);
}
