package com.youyilin.warehouse.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.common.bean.Page;
import com.youyilin.warehouse.dto.inventoryLog.InventoryLogPageQueryDTO;
import com.youyilin.warehouse.entity.InventoryLog;
import com.youyilin.warehouse.vo.inventoryLog.InventoryLogPageVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 库存日志 Mapper
 */
@Mapper
public interface InventoryLogMapper extends BaseMapper<InventoryLog> {

    Integer getTotal(Page<InventoryLogPageQueryDTO> page);

    List<InventoryLogPageVO> getPage(Page<InventoryLogPageQueryDTO> page);
}
