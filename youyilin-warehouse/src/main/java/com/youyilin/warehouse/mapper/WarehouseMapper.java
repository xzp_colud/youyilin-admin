package com.youyilin.warehouse.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.common.bean.Page;
import com.youyilin.warehouse.dto.warehouse.WarehousePageQueryDTO;
import com.youyilin.warehouse.entity.Warehouse;
import com.youyilin.warehouse.vo.warehouse.WarehousePageVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 仓库 Mapper
 */
@Mapper
public interface WarehouseMapper extends BaseMapper<Warehouse> {

    Integer getTotal(Page<WarehousePageQueryDTO> page);

    List<WarehousePageVO> getPage(Page<WarehousePageQueryDTO> page);
}
