package com.youyilin.warehouse.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.warehouse.entity.InventoryLot;
import org.apache.ibatis.annotations.Mapper;

/**
 * 库存批次
 */
@Mapper
public interface InventoryLotMapper extends BaseMapper<InventoryLot> {
}
