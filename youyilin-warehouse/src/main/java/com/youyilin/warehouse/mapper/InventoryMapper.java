package com.youyilin.warehouse.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.common.bean.Page;
import com.youyilin.warehouse.dto.inventory.InventoryPageQueryDTO;
import com.youyilin.warehouse.entity.Inventory;
import com.youyilin.warehouse.vo.inventory.InventoryCheckVO;
import com.youyilin.warehouse.vo.inventory.InventoryErrorVO;
import com.youyilin.warehouse.vo.inventory.InventoryPageVO;
import com.youyilin.warehouse.vo.inventory.InventoryVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 库存 Mapper
 */
@Mapper
public interface InventoryMapper extends BaseMapper<Inventory> {

    Integer getTotal(Page<InventoryPageQueryDTO> page);

    List<InventoryPageVO> getPage(Page<InventoryPageQueryDTO> page);

    List<Inventory> sumQtyBySkuIds(List<Long> skuIds);

    List<InventoryErrorVO> listError();

    List<InventoryVO> listAllWarehouseArea();

    List<InventoryVO> listInventoryOutByProductId(Long productId);

    List<InventoryVO> listInventoryOutBySkuId(Long skuId);

    List<InventoryCheckVO> listByProductName(String productName);

    List<InventoryCheckVO> listLikeByProductName(String productName);

    void asyncInventoryZero();

    void asyncInventory();
}
