package com.youyilin.warehouse.bo;

import com.youyilin.warehouse.dto.InventoryOutDTO;
import com.youyilin.warehouse.entity.Inventory;
import com.youyilin.warehouse.entity.InventoryLot;
import com.youyilin.warehouse.entity.Warehouse;
import com.youyilin.warehouse.entity.WarehouseArea;
import com.youyilin.warehouse.enums.InventoryLogTypeEnum;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.Map;

/**
 * 出库BO
 */
@Data
@Accessors(chain = true)
public class InventoryOutBO {

    public InventoryOutBO(List<InventoryOutDTO> dtoList, InventoryLogTypeEnum typeEnum) {
        this.dtoList = dtoList;
        this.typeEnum = typeEnum;
    }

    // 出库明细
    private List<InventoryOutDTO> dtoList;
    // 操作类型
    private InventoryLogTypeEnum typeEnum;

    // 批次列表
    private Map<Long, InventoryLot> existLotMap;
    // 库存列表
    private Map<Long, Inventory> existInventoryMap;
    // 仓库列表
    private Map<Long, Warehouse> warehouseMap;
    // 库区列表
    private Map<Long, WarehouseArea> warehouseAreaMap;
}
