package com.youyilin.warehouse.bo;

import com.youyilin.warehouse.dto.InventoryInDTO;
import com.youyilin.warehouse.entity.Inventory;
import com.youyilin.warehouse.entity.InventoryLog;
import com.youyilin.warehouse.entity.InventoryLot;
import com.youyilin.warehouse.enums.InventoryLogTypeEnum;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.Map;

/**
 * 入库BO
 */
@Data
@Accessors(chain = true)
public class InventoryInBO {

    public InventoryInBO(List<InventoryInDTO> dtoList, InventoryLogTypeEnum typeEnum) {
        this.dtoList = dtoList;
        this.typeEnum = typeEnum;
    }

    // 入库明细
    private List<InventoryInDTO> dtoList;
    // 操作类型
    private InventoryLogTypeEnum typeEnum;

    // 现有库存列表
    private Map<String, Inventory> existMap;
    // 保存的列表
    private List<Inventory> insertList;
    // 更新的列表
    private List<Inventory> updateList;
    // 保存批次明细
    private List<InventoryLot> lotList;
    // 日志
    private List<InventoryLog> logList;
}
