package com.youyilin.warehouse.task;

import com.youyilin.common.utils.SpringUtil;
import com.youyilin.warehouse.service.InventoryService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 每小时自动同步库存
 */
@Component("asyncInventoryTask")
public class AsyncInventoryTask {

    @Scheduled(cron = "0 0 0/1 * * ? ")
    public void doAsync() {
        InventoryService inventoryService = SpringUtil.getBean(InventoryService.class);
        inventoryService.asyncInventory();
    }
}
