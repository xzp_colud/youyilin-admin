package com.youyilin.warehouse.manager;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.youyilin.common.exception.Assert;
import com.youyilin.warehouse.bo.InventoryInBO;
import com.youyilin.warehouse.bo.InventoryOutBO;
import com.youyilin.warehouse.dto.InventoryInDTO;
import com.youyilin.warehouse.dto.InventoryOutDTO;
import com.youyilin.warehouse.entity.*;
import com.youyilin.warehouse.enums.InventoryLogTypeEnum;
import com.youyilin.warehouse.service.*;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class InventoryManagerImpl implements InventoryManager {

    private final WarehouseService warehouseService;
    private final WarehouseAreaService warehouseAreaService;
    private final InventoryService inventoryService;
    private final InventoryLotService inventoryLotService;
    private final InventoryLogService inventoryLogService;

    public InventoryManagerImpl(WarehouseService warehouseService, WarehouseAreaService warehouseAreaService,
                                InventoryService inventoryService, InventoryLotService inventoryLotService,
                                InventoryLogService inventoryLogService) {
        this.warehouseService = warehouseService;
        this.warehouseAreaService = warehouseAreaService;
        this.inventoryService = inventoryService;
        this.inventoryLotService = inventoryLotService;
        this.inventoryLogService = inventoryLogService;
    }

    @Override
    @Transactional
    public void inventoryInByPurchaseOrder(List<InventoryInDTO> dtoList) {
        // BO组装
        InventoryInBO bo = new InventoryInBO(dtoList, InventoryLogTypeEnum.PURCHASE_ORDER_IN);
        // 入库
        this.doInventoryIn(bo);
    }

    /**
     * 入库
     */
    private void doInventoryIn(InventoryInBO bo) {
        // 验证参数
        this.validateInParams(bo);
        // 执行入库
        this.doIn(bo);
        // 持久化
        if (CollectionUtils.isNotEmpty(bo.getInsertList())) {
            inventoryService.saveBatch(bo.getInsertList());
        }
        if (CollectionUtils.isNotEmpty(bo.getUpdateList())) {
            inventoryService.updateBatchById(bo.getUpdateList());
        }
        inventoryLotService.saveBatch(bo.getLotList());
        inventoryLogService.saveBatch(bo.getLogList());
    }

    /**
     * 入库验证参数
     */
    private void validateInParams(InventoryInBO bo) {
        Set<Long> warehouseIds = new HashSet<>();
        Set<Long> warehouseAreaIds = new HashSet<>();
        // 列表
        List<InventoryInDTO> dtoList = bo.getDtoList();
        // 验证参数
        for (InventoryInDTO item : dtoList) {
            Assert.notNull(item.getProductId(), "商品不能为空");
            Assert.notNull(item.getSkuId(), "SKU不能为空");
            Assert.notNull(item.getWarehouseId(), "仓库不能为空");
            Assert.notNull(item.getWarehouseAreaId(), "库区不能为空");
            Assert.notNull(item.getQty(), "数量不能为空");
            Assert.hasLength(item.getSn(), "批次号不能为空");
            Assert.isFalse(item.getQty().compareTo(BigDecimal.ZERO) < 0, "数量不能小于0");

            warehouseIds.add(item.getWarehouseId());
            warehouseAreaIds.add(item.getWarehouseAreaId());
        }
        // 获取仓库 并分组
        Map<Long, Warehouse> warehouseMap = warehouseService.mapByIds(new ArrayList<>(warehouseIds));
        // 获取库区 并分组
        Map<Long, WarehouseArea> warehouseAreaMap = warehouseAreaService.mapByIds(new ArrayList<>(warehouseAreaIds));
        // 二次验证
        List<Long> skuIds = new ArrayList<>();
        for (InventoryInDTO item : dtoList) {
            // 仓库
            Warehouse warehouse = warehouseMap.get(item.getWarehouseId());
            Assert.notNull(warehouse, "仓库不存在");
            // 库区
            WarehouseArea warehouseArea = warehouseAreaMap.get(item.getWarehouseAreaId());
            Assert.notNull(warehouseArea, "库区不存在");
            // 是否一致
            boolean isEquals = warehouse.getId().equals(warehouseArea.getWarehouseId());
            Assert.isTrue(isEquals, "库区所属仓库不一致");
            // 赋值
            item.setWarehouseName(warehouse.getName())
                    .setWarehouseAreaName(warehouseArea.getName());
            skuIds.add(item.getSkuId());
        }
        // 拉取现有库存
        bo.setExistMap(this.initExistInventory(skuIds));
    }

    /**
     * 拉取现有库存
     *
     * @param skuIds SKU
     * @return HashMap
     */
    private Map<String, Inventory> initExistInventory(List<Long> skuIds) {
        List<Inventory> inventoryList = inventoryService.listBySkuIds(skuIds);
        if (CollectionUtils.isEmpty(inventoryList)) {
            return new HashMap<>();
        }
        Map<String, Inventory> inventoryMap = new HashMap<>();
        for (Inventory item : inventoryList) {
            String key = item.getSkuId().toString() + "-" + item.getWarehouseAreaId().toString();
            inventoryMap.put(key, item);
        }
        return inventoryMap;
    }

    /**
     * 执行入库
     */
    private void doIn(InventoryInBO bo) {
        // 现有库存
        Map<String, Inventory> existMap = bo.getExistMap();
        // 明细
        List<InventoryInDTO> dtoList = bo.getDtoList();
        // 需新增列表
        Map<String, Inventory> insertMap = new HashMap<>();
        // 需更新列表
        Map<String, Inventory> updateMap = new HashMap<>();
        // 批次列表
        List<InventoryLot> lotList = new ArrayList<>();
        // 日志
        List<InventoryLog> logList = new ArrayList<>();
        // 明细列表
        for (InventoryInDTO item : dtoList) {
            String key = item.getSkuId().toString() + "-" + item.getWarehouseAreaId().toString();
            // 四种类型 有库存 - 有更新 | 有库存 - 无更新 | 无库存 - 有新增 | 无库存 - 无新增
            Inventory inventory;
            if (existMap.containsKey(key) && updateMap.containsKey(key)) {
                inventory = updateMap.get(key);
                inventory.setQty(inventory.getQty().add(item.getQty()));
            } else if (existMap.containsKey(key)) {
                inventory = existMap.get(key);
                inventory.setQty(inventory.getQty().add(item.getQty()));
                updateMap.put(key, inventory);
            } else if (insertMap.containsKey(key)) {
                inventory = insertMap.get(key);
                inventory.setQty(inventory.getQty().add(item.getQty()));
                item.setInventoryId(inventory.getId());
            } else {
                inventory = this.constructByIn(item);
                insertMap.put(key, inventory);
            }
            item.setInventoryId(inventory.getId());
            // 批次明细
            InventoryLot lot = item.convertToEntity();
            lot.setId(IdWorker.getId());
            lotList.add(lot);
            // 日志
            logList.add(this.constructByInventory(inventory, lot, bo.getTypeEnum(), item.getQty(), item.getProductName(), item.getSkuName(), item.getWarehouseName(), item.getWarehouseAreaName(), item.getSourceSn()));
            item.setInventoryLotId(lot.getId()).setInventoryLotSn(lot.getSn());
        }
        // 保存列表
        List<Inventory> insertList = new ArrayList<>(insertMap.values());
        // 更新列表
        List<Inventory> updateList = new ArrayList<>(updateMap.values());
        // 日志
        bo.setInsertList(insertList)
                .setUpdateList(updateList)
                .setLotList(lotList)
                .setLogList(logList);
    }

    /**
     * 构造库存信息
     *
     * @param dto 入库信息
     * @return Inventory
     */
    private Inventory constructByIn(InventoryInDTO dto) {
        Inventory inventory = new Inventory();
        inventory.setId(IdWorker.getId())
                .setProductId(dto.getProductId())
                .setSkuId(dto.getSkuId())
                .setWarehouseId(dto.getWarehouseId())
                .setWarehouseAreaId(dto.getWarehouseAreaId())
                .setQty(dto.getQty())
                .setFrozenQty(BigDecimal.ZERO);

        // 设置关联关系
        dto.setInventoryId(inventory.getId());
        return inventory;
    }

    /**
     * 构造日志
     */
    private InventoryLog constructByInventory(Inventory inventory, InventoryLot lot, InventoryLogTypeEnum typeEnum,
                                              BigDecimal qty, String productName, String skuName, String warehouseName,
                                              String warehouseAreaName, String sourceSn) {
        InventoryLog log = new InventoryLog();
        log.setInventoryId(inventory.getId())
                .setInventoryLotId(lot.getId())
                .setInventoryLotSn(lot.getSn())
                .setProductId(inventory.getProductId())
                .setProductName(productName)
                .setSkuId(inventory.getSkuId())
                .setSkuName(skuName)
                .setWarehouseId(inventory.getWarehouseId())
                .setWarehouseName(warehouseName)
                .setWarehouseAreaId(inventory.getWarehouseAreaId())
                .setWarehouseAreaName(warehouseAreaName)
                .setQty(qty)
                .setType(typeEnum.getCode())
                .setAmount(0L)
                .setSerialNum(sourceSn)
                .setResidueQty(lot.getQty());
        return log;
    }

    @Override
    @Transactional
    public void inventoryOutByOutboundOrder(List<InventoryOutDTO> dtoList) {
        // 数据组装
        InventoryOutBO bo = new InventoryOutBO(dtoList, InventoryLogTypeEnum.OUTBOUND_ORDER_OUT);
        // 出库
        this.doInventoryOut(bo);
    }


    /**
     * 出库
     */
    private void doInventoryOut(InventoryOutBO bo) {
        List<InventoryOutDTO> dtoList = bo.getDtoList();
        // 批次列表
        List<Long> lotIds = new ArrayList<>();
        // 一次验证
        for (InventoryOutDTO dto : dtoList) {
            this.validateOutParamsFirst(dto);
            lotIds.add(dto.getId());
        }
        // 获取批次库存信息
        Map<Long, InventoryLot> inventoryLotMap = inventoryLotService.mapByIds(lotIds);
        // 库存信息
        Set<Long> inventoryIds = new HashSet<>();
        for (InventoryLot inventoryLot : inventoryLotMap.values()) {
            inventoryIds.add(inventoryLot.getInventoryId());
        }
        // 获取库存信息
        Map<Long, Inventory> inventoryMap = inventoryService.mapByIds(new ArrayList<>(inventoryIds));
        // 二次验证
        Set<Long> warehouseIds = new HashSet<>();
        Set<Long> warehouseAreaIds = new HashSet<>();
        for (InventoryOutDTO item : dtoList) {
            InventoryLot lot = inventoryLotMap.get(item.getId());
            Inventory inventory = inventoryMap.get(lot.getInventoryId());
            // 验证
            this.validateOutParamsSecond(item, inventory, lot);
            // 赋值 仓库、库区
            warehouseIds.add(inventory.getWarehouseId());
            warehouseAreaIds.add(inventory.getWarehouseAreaId());
        }
        // 仓库
        Map<Long, Warehouse> warehouseMap = warehouseService.mapByIds(new ArrayList<>(warehouseIds));
        // 库区
        Map<Long, WarehouseArea> warehouseAreaMap = warehouseAreaService.mapByIds(new ArrayList<>(warehouseAreaIds));
        // 保存
        Map<Long, Inventory> updateMap = new HashMap<>();
        Map<Long, InventoryLot> updateLotMap = new HashMap<>();
        List<InventoryLog> logList = new ArrayList<>();
        for (InventoryOutDTO item : dtoList) {
            InventoryLot lot = inventoryLotMap.get(item.getId());
            Inventory inventory = inventoryMap.get(lot.getInventoryId());
            Warehouse warehouse = warehouseMap.get(inventory.getWarehouseId());
            WarehouseArea warehouseArea = warehouseAreaMap.get(inventory.getWarehouseAreaId());
            // 验证数量
            BigDecimal lotResidueQty = lot.getQty().subtract(item.getQty());
            BigDecimal residueQty = inventory.getQty().subtract(item.getQty());
            boolean isLtZero = lotResidueQty.compareTo(BigDecimal.ZERO) < 0 || residueQty.compareTo(BigDecimal.ZERO) < 0;
            Assert.isFalse(isLtZero, "【" + lot.getLotSn() + "】数量不足");
            // 设置剩余数量
            lot.setQty(lotResidueQty);
            inventory.setQty(residueQty);
            // 设置冻结数量
            if (InventoryLogTypeEnum.isOptFrozenQty(bo.getTypeEnum().getCode())) {
                BigDecimal lotFrozenQty = lot.getFrozenQty() == null ? BigDecimal.ZERO : lot.getFrozenQty();
                BigDecimal frozenQty = inventory.getFrozenQty() == null ? BigDecimal.ZERO : inventory.getFrozenQty();
                lot.setFrozenQty(lotFrozenQty.add(item.getQty()));
                inventory.setFrozenQty(frozenQty.add(item.getQty()));
            }
            if (!updateLotMap.containsKey(lot.getId())) {
                updateLotMap.put(lot.getId(), lot);
            }
            if (!updateMap.containsKey(inventory.getId())) {
                updateMap.put(inventory.getId(), inventory);
            }
            item.setInventoryId(inventory.getId())
                    .setInventoryLotId(lot.getId())
                    .setInventoryLotSn(lot.getSn())
                    .setWarehouseId(warehouse.getId())
                    .setWarehouseName(warehouse.getName())
                    .setWarehouseAreaId(warehouseArea.getId())
                    .setWarehouseAreaName(warehouseArea.getName());
            logList.add(this.constructByInventory(inventory, lot, bo.getTypeEnum(), item.getQty(), item.getProductName(), item.getSkuName(), warehouse.getName(), warehouseArea.getName(), lot.getLotSn()));
        }
        // 持久化
        inventoryService.updateBatchById(new ArrayList<>(updateMap.values()));
        inventoryLotService.updateBatchById(new ArrayList<>(updateLotMap.values()));
        inventoryLogService.saveBatch(logList);
    }

    private void validateOutParamsFirst(InventoryOutDTO dto) {
        Assert.notNull(dto.getId(), "库存信息不能为空");
        Assert.notNull(dto.getQty(), "出库数量不能为空");
        Assert.isTrue(dto.getQty().compareTo(BigDecimal.ZERO) > 0, "出库数量必须大于0");
        Assert.notNull(dto.getProductId(), "商品不能为空");
        Assert.notNull(dto.getSkuId(), "SKU不能为空");
    }

    private void validateOutParamsSecond(InventoryOutDTO dto, Inventory inventory, InventoryLot lot) {
        // 验证批次
        Assert.notNull(lot, "库存信息不存在");
        // 验证库存
        Assert.notNull(inventory, "库存信息不存在");
        // 验证产品
        boolean productEquals = inventory.getProductId().equals(dto.getProductId());
        Assert.isTrue(productEquals, "【" + lot.getLotSn() + "】产品不一致");
        // 验证SKU
        boolean skuEquals = inventory.getSkuId().equals(dto.getSkuId());
        Assert.isTrue(skuEquals, "【" + lot.getLotSn() + "】SKU不一致");
    }

    @Override
    public void saveLot(InventoryInDTO in) {
        List<InventoryInDTO> dtoList = new ArrayList<>();
        dtoList.add(in);
        // BO组装
        InventoryInBO bo = new InventoryInBO(dtoList, InventoryLogTypeEnum.CHECK_IN);
        // 入库
        this.doInventoryIn(bo);
    }

    @Override
    @Transactional
    public void checkOut(InventoryOutDTO dto) {
        List<InventoryOutDTO> dtoList = new ArrayList<>();
        dtoList.add(dto);
        // 数据组装
        InventoryOutBO bo = new InventoryOutBO(dtoList, InventoryLogTypeEnum.CHECK_OUT);
        // 出库
        this.doInventoryOut(bo);
    }

    @Override
    @Transactional
    public void moveOut(List<InventoryOutDTO> dtoList) {
        // 数据组装
        InventoryOutBO bo = new InventoryOutBO(dtoList, InventoryLogTypeEnum.MOVE_OUT);
        // 出库
        this.doInventoryOut(bo);
    }

    @Override
    @Transactional
    public void moveIn(List<InventoryInDTO> dtoList) {
        // BO组装
        InventoryInBO bo = new InventoryInBO(dtoList, InventoryLogTypeEnum.MOVE_IN);
        // 入库
        this.doInventoryIn(bo);
    }

    /**
     * 删除库存及批次信息
     */
    private void doDelInventory(List<Inventory> inventoryList, InventoryLogTypeEnum typeEnum) {
        // 库存列表
        List<Long> inventoryIds = new ArrayList<>();
        inventoryList.forEach(item -> inventoryIds.add(item.getId()));
        // 批次信息
        List<InventoryLot> lotList = inventoryLotService.listByInventoryIds(inventoryIds);
        // 日志
        if (CollectionUtils.isNotEmpty(lotList)) {
            List<InventoryLog> logList = new ArrayList<>();
            Map<Long, Inventory> inventoryMap = inventoryList.stream().collect(Collectors.toMap(Inventory::getId, item -> item));
            for (InventoryLot lot : lotList) {
                boolean lotNoQty = lot.getQty() != null && lot.getQty().compareTo(BigDecimal.ZERO) == 0;
                Assert.isTrue(lotNoQty, "库存批次数量不为0");
                Inventory inventory = inventoryMap.get(lot.getInventoryId());
                Assert.notNull(inventory, "库存信息异常，联系管理员");
                // 日志
                InventoryLog log = this.constructByInventory(inventory, lot, typeEnum, BigDecimal.ZERO, null, null, null, null, lot.getLotSn());
                logList.add(log);
            }
            // 删除批次
            inventoryLotService.delByInventoryIds(inventoryIds);
            // 日志
            inventoryLogService.saveBatch(logList);
        }
        // 删除库存
        inventoryService.removeByIds(inventoryIds);
    }

    @Override
    @Transactional
    public void delByWarehouseId(Long warehouseId) {
        // 拉取库存信息
        List<Inventory> inventoryList = inventoryService.listByWarehouseId(warehouseId);
        if (CollectionUtils.isEmpty(inventoryList)) {
            return;
        }
        // 执行删除
        this.doDelInventory(inventoryList, InventoryLogTypeEnum.DELETE_BY_WAREHOUSE);
    }

    @Override
    @Transactional
    public void delByWarehouseAreaId(Long warehouseAreaId) {
        // 拉取库存信息
        List<Inventory> inventoryList = inventoryService.listByWarehouseAreaId(warehouseAreaId);
        if (CollectionUtils.isEmpty(inventoryList)) {
            return;
        }
        // 执行删除
        this.doDelInventory(inventoryList, InventoryLogTypeEnum.DELETE_BY_WAREHOUSE_AREA);
    }

    @Override
    @Transactional
    public void delByInventory(Long inventoryId) {
        Inventory inventory = inventoryService.validateInventory(inventoryId);
        List<Inventory> inventoryList = new ArrayList<>();
        inventoryList.add(inventory);
        // 执行删除
        this.doDelInventory(inventoryList, InventoryLogTypeEnum.DELETE_BY_INVENTORY);
    }
}
