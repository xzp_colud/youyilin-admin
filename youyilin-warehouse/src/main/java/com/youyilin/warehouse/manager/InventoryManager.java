package com.youyilin.warehouse.manager;

import com.youyilin.warehouse.dto.InventoryInDTO;
import com.youyilin.warehouse.dto.InventoryOutDTO;

import java.util.List;

public interface InventoryManager {

    /**
     * 采购单入库
     */
    void inventoryInByPurchaseOrder(List<InventoryInDTO> dtoList);

    /**
     * 出库单出库
     */
    void inventoryOutByOutboundOrder(List<InventoryOutDTO> dtoList);

    /**
     * 盘点新增
     */
    void saveLot(InventoryInDTO in);

    /**
     * 盘点出库
     */
    void checkOut(InventoryOutDTO dto);

    /**
     * 移库出库
     */
    void moveOut(List<InventoryOutDTO> dtoList);

    /**
     * 移库入库
     */
    void moveIn(List<InventoryInDTO> dtoList);

    /**
     * 按仓库删除
     */
    void delByWarehouseId(Long warehouseId);

    /**
     * 按库区删除
     */
    void delByWarehouseAreaId(Long warehouseAreaId);

    /**
     * 按库存删除
     */
    void delByInventory(Long inventoryId);
//
//    /**
//     * 清理冻结库存
//     */
//    void clearFrozenQty(List<InventoryLog> clearList);
}
