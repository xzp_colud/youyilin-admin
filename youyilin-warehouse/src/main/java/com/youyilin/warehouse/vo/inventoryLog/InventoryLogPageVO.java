package com.youyilin.warehouse.vo.inventoryLog;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.common.utils.FormatAmountUtil;
import com.youyilin.warehouse.entity.InventoryLog;
import com.youyilin.warehouse.enums.InventoryLogTypeEnum;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.collections.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 库存日志列表VO
 */
@Data
@Accessors(chain = true)
public class InventoryLogPageVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 操作时间
    private Date modifyDate;
    // 操作人
    private String modifyName;
    // 仓库
    private String warehouseName;
    // 库区
    private String warehouseAreaName;
    // 产品
    private String productName;
    // SKU
    private String skuName;
    // SKU 货号
    private String serialNum;
    // 操作类型
    private Integer type;
    // 操作数量
    private BigDecimal qty;
    // 剩余数量
    private BigDecimal residueQty;
    // 金额
    private Long amount;
    // 批次号
    private String inventoryLotSn;

    public String getAmountText() {
        return FormatAmountUtil.format(this.amount);
    }

    public String getTypeText() {
        if (this.type == null) return "";

        return InventoryLogTypeEnum.queryInfoByCode(this.type);
    }

    public static List<InventoryLogPageVO> convertByEntity(List<InventoryLog> sourceList) {
        if (CollectionUtils.isEmpty(sourceList)) {
            return new ArrayList<>();
        }
        return BeanHelper.map(sourceList, InventoryLogPageVO.class);
    }
}
