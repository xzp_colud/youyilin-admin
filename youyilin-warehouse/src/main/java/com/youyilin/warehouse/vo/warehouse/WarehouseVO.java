package com.youyilin.warehouse.vo.warehouse;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.warehouse.entity.Warehouse;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;

/**
 * 仓库实体VO
 */
@Data
@Accessors(chain = true)
public class WarehouseVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 创建时间
    private Date createDate;
    // 修改时间
    private Date modifyDate;
    // 编号
    private String sn;
    // 名称
    private String name;
    // 备注
    private String remark;

    public static List<WarehouseVO> convertByEntity(List<Warehouse> list) {
        return BeanHelper.map(list, WarehouseVO.class);
    }
}
