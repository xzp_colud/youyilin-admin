package com.youyilin.warehouse.vo.inventory;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 库存列表VO
 */
@Data
@Accessors(chain = true)
public class InventoryPageVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 时间
    private Date modifyDate;
    // 仓库
    private String warehouseName;
    // 库区
    private String warehouseAreaName;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long productId;
    // 产品
    private String productName;
    // SKU
    private String skuName;
    // 库存数量
    private BigDecimal qty;
    // 冻结库存
    private BigDecimal frozenQty;
}
