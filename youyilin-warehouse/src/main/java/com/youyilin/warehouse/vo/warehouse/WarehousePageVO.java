package com.youyilin.warehouse.vo.warehouse;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 仓库列表VO
 */
@Data
@Accessors(chain = true)
public class WarehousePageVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 创建时间
    private Date createDate;
    // 修改时间
    private Date modifyDate;
    // 编号
    private String sn;
    // 名称
    private String name;
    // 备注
    private String remark;
}
