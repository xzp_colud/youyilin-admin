package com.youyilin.warehouse.vo.inventory;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 库存盘点VO
 */
@Data
@Accessors(chain = true)
public class InventoryCheckVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    private Date modifyDate;
    // 仓库ID
    @JsonSerialize(using = ToStringSerializer.class)
    private Long warehouseId;
    // 库区ID
    @JsonSerialize(using = ToStringSerializer.class)
    private Long warehouseAreaId;
    // 产品ID
    @JsonSerialize(using = ToStringSerializer.class)
    private Long productId;
    // SKU ID
    @JsonSerialize(using = ToStringSerializer.class)
    private Long skuId;
    // 供货商ID
    @JsonSerialize(using = ToStringSerializer.class)
    private Long supplierId;
    // 仓库
    private String warehouseName;
    // 库区
    private String warehouseAreaName;
    // 产品
    private String productName;
    // 供货商
    private String supplierName;
    // SKU
    private String skuName;
    // 库存数量
    private BigDecimal qty;
    // 冻结库存
    private BigDecimal frozenQty;
    // 批次
    @JsonSerialize(using = ToStringSerializer.class)
    private Long inventoryLotId;
    // 批次
    private String inventoryLotSn;
    // 自定义批次
    private String lotSn;
}
