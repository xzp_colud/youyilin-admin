package com.youyilin.warehouse.vo.inventoryLot;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.warehouse.entity.InventoryLot;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.collections.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 库存批次列表VO
 */
@Data
@Accessors(chain = true)
public class InventoryLotPageVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 操作时间
    private Date modifyDate;
    // 系统批次号
    private String sn;
    // 自定义批次号
    private String lotSn;
    // 库存余量
    private BigDecimal qty;
    // 冻结数量
    private BigDecimal frozenQty;

    public static List<InventoryLotPageVO> conventByEntity(List<InventoryLot> sourceList) {
        if (CollectionUtils.isEmpty(sourceList)) {
            return new ArrayList<>();
        }
        return BeanHelper.map(sourceList, InventoryLotPageVO.class);
    }
}
