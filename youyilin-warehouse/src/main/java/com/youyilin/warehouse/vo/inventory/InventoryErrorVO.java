package com.youyilin.warehouse.vo.inventory;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * 库存异常列表VO
 */
@Data
@Accessors(chain = true)
public class InventoryErrorVO {

    // 商品型号
    private String productName;
    // SKU名称
    private String skuName;
    // 当前库存
    private BigDecimal qty;
    // 冻结库存
    private BigDecimal frozenQty;
    // 错误信息
    private String errorMsg;
}
