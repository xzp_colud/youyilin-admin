package com.youyilin.warehouse.vo.warehouseArea;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.warehouse.entity.WarehouseArea;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 仓库编辑VO
 */
@Data
@Accessors(chain = true)
public class WarehouseAreaEditVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 所属仓库ID
    @JsonSerialize(using = ToStringSerializer.class)
    private Long warehouseId;
    // 名称
    private String name;

    public static WarehouseAreaEditVO convertByEntity(WarehouseArea warehouse) {
        return BeanHelper.map(warehouse, WarehouseAreaEditVO.class);
    }
}
