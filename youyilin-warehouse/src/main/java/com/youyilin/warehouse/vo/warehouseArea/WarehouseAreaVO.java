package com.youyilin.warehouse.vo.warehouseArea;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.warehouse.entity.WarehouseArea;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;

/**
 * 仓库实体VO
 */
@Data
@Accessors(chain = true)
public class WarehouseAreaVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 修改时间
    private Date modifyDate;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long warehouseId;
    // 仓库名称
    private String warehouseName;
    // 编号
    private String sn;
    // 名称
    private String name;
    // 备注
    private String remark;

    public static List<WarehouseAreaVO> convertByEntity(List<WarehouseArea> list) {
        return BeanHelper.map(list, WarehouseAreaVO.class);
    }
}
