package com.youyilin.warehouse.vo.warehouseArea;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 仓库列表VO
 */
@Data
@Accessors(chain = true)
public class WarehouseAreaPageVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 修改时间
    private Date modifyDate;
    // 仓库名称
    private String warehouseName;
    // 编号
    private String sn;
    // 名称
    private String name;
    // 备注
    private String remark;
}
