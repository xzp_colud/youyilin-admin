package com.youyilin.warehouse.vo.warehouse;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.warehouse.entity.Warehouse;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 仓库编辑VO
 */
@Data
@Accessors(chain = true)
public class WarehouseEditVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 名称
    private String name;

    public static WarehouseEditVO convertByEntity(Warehouse warehouse) {
        return BeanHelper.map(warehouse, WarehouseEditVO.class);
    }
}
