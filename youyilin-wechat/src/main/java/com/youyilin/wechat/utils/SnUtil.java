package com.youyilin.wechat.utils;

import com.youyilin.common.utils.DateUtils;
import org.apache.commons.lang3.RandomStringUtils;

public class SnUtil {

    public final static String CUSTOMER_ORDER_PREFIX = "WO";

    public static synchronized String getSn() {
        String id = DateUtils.getDate("yyyyMMddHHmmss");
        String randomString = RandomStringUtils.randomNumeric(8);
        return id + randomString;
    }

    public static synchronized String getPaymentSn() {
        return CUSTOMER_ORDER_PREFIX + getSn();
    }
}
