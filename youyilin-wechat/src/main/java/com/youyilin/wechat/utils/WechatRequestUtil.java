package com.youyilin.wechat.utils;

import com.alibaba.fastjson.JSONObject;
import com.youyilin.common.utils.http.HttpUtils;
import com.youyilin.wechat.constant.UrlConstant;
import com.youyilin.wechat.model.entity.WechatConfig;

public class WechatRequestUtil {

    private String appid;
    private String secret;
    private String token;

    public WechatRequestUtil(String appid, String secret, String token) {
        this.appid = appid;
        this.secret = secret;
        this.token = token;
    }

    // 获取TOKEN
    public <T> T getToken(Class<T> clazz) {
        String url = UrlConstant.GET_TOKEN + "?grant_type=client_credential&appid=" + appid + "&secret=" + secret;
        String response = HttpUtils.sendGet(url, "");

        return JSONObject.parseObject(response, clazz);
    }

    // 小程序登录获取Openid
    public <T> T code2Session(String code, Class<T> clazz) {
        String url = UrlConstant.CODE_2_SESSION + "?appid=" + appid + "&secret=" + secret + "&js_code=" + code + "&grant_type=authorization_code";
        String response = HttpUtils.sendGet(url, "");

        return JSONObject.parseObject(response, clazz);
    }

    // 调用获取手机号
    public <T> T getPhoneNumber(String code, Class<T> clazz) {
        String url = UrlConstant.GET_PHONE_NUMBER + "?access_token=" + token;
        JSONObject params = new JSONObject();
        params.put("code", code);
        String response = HttpUtils.sendPostJson(url, JSONObject.toJSONString(params));

        return JSONObject.parseObject(response, clazz);
    }

    public static class Builder {
        private String appid;
        private String secret;
        private String token;

        public Builder() {

        }

        public Builder config(WechatConfig wechatConfig) {
            this.appid = wechatConfig.getAppid();
            this.secret = wechatConfig.getSecret();
            this.token = wechatConfig.getToken();

            return this;
        }

        public WechatRequestUtil build() {
            return new WechatRequestUtil(this.appid, this.secret, this.token);
        }
    }
}
