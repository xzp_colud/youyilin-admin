package com.youyilin.wechat.model.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 微信商户配置
 */
@Data
@Accessors(chain = true)
@TableName("wechat_config_mch")
public class WechatConfigMch implements Serializable {
    private static final long serialVersionUID = 6143654092245342810L;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    @TableField(value = "create_date", fill = FieldFill.INSERT)
    private Date createDate;
    @TableField(fill = FieldFill.INSERT)
    private Long creator;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 应用ID
     */
    private String appid;
    /**
     * 商户ID
     */
    @TableField(value = "mch_id")
    private String mchId;
    /**
     * 商户编号
     */
    @TableField(value = "mch_number")
    private String mchNumber;
    /**
     * 证书路径
     */
    @TableField(value = "private_key_path")
    private String privateKeyPath;
    /**
     * 微信支付证书路径
     */
    @TableField(value = "wechat_pay_path")
    private String wechatPayPath;
    /**
     * API key
     */
    @TableField(value = "api_key")
    private String apiKey;
    /**
     * 通知回调地址
     */
    @TableField(value = "notify_url")
    private String notifyUrl;

    @TableLogic
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;
}
