package com.youyilin.wechat.model.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 微信流水
 */
@Data
@Accessors(chain = true)
@TableName("wechat_payment")
public class WechatPayment implements Serializable {
    private static final long serialVersionUID = -7336809289157063818L;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    @TableField(value = "creat_date", fill = FieldFill.INSERT)
    private Date createDate;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;

    /**
     * 付款金额
     */
    private Long amount;
    /**
     * openid
     */
    private String openid;
    /**
     * 应用ID
     */
    private String appid;
    /**
     * 商户号
     */
    private String mchid;
    /**
     * 描述
     */
    private String description;
    /**
     * 源单号
     */
    @TableField(value = "source_no")
    private String sourceNo;
    /**
     * 源单号
     */
    @TableField(value = "source_id")
    private Long sourceId;
    /**
     * 预支付ID
     */
    @TableField(value = "prepay_id")
    private String prepayId;
    /**
     * 交易单号
     */
    @TableField(value = "out_trade_no")
    private String outTradeNo;
    /**
     * 交易类型
     */
    @TableField(value = "trade_type")
    private String tradeType;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 支付成功的时间
     */
    @TableField(value = "success_time")
    private String successTime;
    /**
     * 微信交易成功的流水号
     */
    @TableField(value = "transaction_id")
    private String transactionId;
    /**
     * 逻辑标记
     */
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;

    public enum TradeType {
        JSAPI, // 公众号支付
        NATIVE, // 扫码支付
        APP, // APP支付
        MICROPAY, // 付款码支付
        MWEB, // H5支付
        FACEPAY, // 刷脸支付
    }
}
