package com.youyilin.wechat.model.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

@Data
@Accessors(chain = true)
public class WechatConfig implements Serializable {
    private static final long serialVersionUID = -2183165146544755009L;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    @TableField(value = "create_date", fill = FieldFill.INSERT)
    private Date createDate;
    @TableField(fill = FieldFill.INSERT)
    private Long creator;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 应用ID
     */
    @NotBlank(message = "应用ID不能为空")
    private String appid;
    /**
     * 应用名称
     */
    @NotBlank(message = "应用名称不能为空")
    private String name;
    /**
     * 应用秘钥
     */
    @NotBlank(message = "应用秘钥不能为空")
    private String secret;
    /**
     * 应用TOKEN
     */
    private String token;
    /**
     * 过期时间
     */
    @TableField(value = "expired_date")
    private Long expiredDate;

    @TableLogic
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;
}
