package com.youyilin.wechat.model.response;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class JsApiResponse {

    private String timeStamp;
    private String nonceStr;
    private String packages;
    private String signType;
    private String paySign;
}
