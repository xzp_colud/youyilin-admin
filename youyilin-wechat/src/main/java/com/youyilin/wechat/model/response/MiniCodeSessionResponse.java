package com.youyilin.wechat.model.response;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors
public class MiniCodeSessionResponse {

    private String openid;
    private String unionid;
    private String session_key;
    private String errcode;
    private String errmsg;
}
