package com.youyilin.wechat.model.response;

import lombok.Data;

@Data
public class GetPhoneNumberResponse {

    private Long errcode;
    private String errmsg;
    private PhoneNumberInfo phone_info;
}
