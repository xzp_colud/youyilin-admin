package com.youyilin.wechat.model.response;

import lombok.Data;

@Data
public class GetTokenResponse {

    private String access_token;
    private Long expires_in;
}
