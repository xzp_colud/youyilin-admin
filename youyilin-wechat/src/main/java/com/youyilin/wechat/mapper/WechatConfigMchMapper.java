package com.youyilin.wechat.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.wechat.model.entity.WechatConfigMch;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface WechatConfigMchMapper extends BaseMapper<WechatConfigMch> {
}
