package com.youyilin.wechat.enums;

/**
 * 微信支付记录 状态
 */
public enum WechatPaymentStatusEnum {

    NORMAL(0, "正常"),
    PROCESSED(1, "已处理"),
    EXPIRED(2, "已过期");

    private Integer code;
    private String name;

    WechatPaymentStatusEnum(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static boolean hasExist(Integer code) {
        if (code == null) {
            return false;
        }
        for (WechatPaymentStatusEnum orderTypeEnum : WechatPaymentStatusEnum.values()) {
            if (orderTypeEnum.getCode().equals(code)) {
                return true;
            }
        }
        return false;
    }
}
