package com.youyilin.wechat.constant;

public class UrlConstant {

    // 获取TOKEN
    public final static String GET_TOKEN = "https://api.weixin.qq.com/cgi-bin/token";

    // 小程序登录
    public final static String CODE_2_SESSION = "https://api.weixin.qq.com/sns/jscode2session";

    // 获取手机号
    public final static String GET_PHONE_NUMBER = "https://api.weixin.qq.com/wxa/business/getuserphonenumber";
}
