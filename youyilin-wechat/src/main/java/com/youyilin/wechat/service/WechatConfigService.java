package com.youyilin.wechat.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.common.bean.Page;
import com.youyilin.wechat.model.entity.WechatConfig;

import java.util.List;

public interface WechatConfigService extends IService<WechatConfig> {

    Integer getTotal(Page<WechatConfig> page);

    List<WechatConfig> getPage(Page<WechatConfig> page);

    /**
     * 按应用ID查询
     *
     * @param appId 应用ID
     */
    WechatConfig getByAppid(String appId);

    /**
     * 保存
     */
    void saveWechatConfig(WechatConfig en);

    /**
     * 删除
     */
    void delById(Long id);

    /**
     * 验证应用ID唯一性
     */
    void validateAppidUnique(WechatConfig en);

    /**
     * 更新Token
     */
    void updateToken(Long id, String access_token);
}
