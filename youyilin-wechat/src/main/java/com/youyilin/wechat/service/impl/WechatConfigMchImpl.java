package com.youyilin.wechat.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.bean.Page;
import com.youyilin.common.exception.ApiException;
import com.youyilin.common.utils.ObjectUniqueUtil;
import com.youyilin.wechat.mapper.WechatConfigMchMapper;
import com.youyilin.wechat.model.entity.WechatConfigMch;
import com.youyilin.wechat.service.WechatConfigMchService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WechatConfigMchImpl extends ServiceImpl<WechatConfigMchMapper, WechatConfigMch> implements WechatConfigMchService {

    @Override
    public Integer getTotal(Page<WechatConfigMch> page) {
        return null;
    }

    @Override
    public List<WechatConfigMch> getPage(Page<WechatConfigMch> page) {
        return null;
    }

    @Override
    public WechatConfigMch getByAppid(String appId) {
        return super.getOne(new LambdaQueryWrapper<WechatConfigMch>().eq(WechatConfigMch::getAppid, appId));
    }

    @Override
    public void saveWechatConfigMch(WechatConfigMch en) {
        // 验证唯一性
        this.validateAppidUnique(en);

        if (en.getId() == null) {
            super.save(en);
        } else {
            WechatConfigMch old = super.getById(en.getId());
            if (old == null) {
                throw new ApiException("保存失败");
            }
            super.updateById(en);
        }
    }

    @Override
    public void delById(Long id) {
        super.removeById(id);
    }

    @Override
    public void validateAppidUnique(WechatConfigMch en) {
        WechatConfigMch appidUnique = this.getByAppid(en.getAppid());
        if (!ObjectUniqueUtil.validateObjectUnique(en, appidUnique)) {
            throw new ApiException("应用已配置");
        }
    }

    @Override
    public WechatConfigMch getPayConfig(String appId) {
        WechatConfigMch configMch = this.getByAppid(appId);
        if (configMch == null) {
            throw new ApiException("支付异常");
        }
        return configMch;
    }
}
