package com.youyilin.wechat.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.wechat.model.entity.WechatPayment;
import com.youyilin.wechat.model.response.JsApiResponse;

public interface WechatPaymentService extends IService<WechatPayment> {

    /**
     * 获取支付单号
     *
     * @param outTradeNo 商户单号
     * @return WechatPayment
     */
    WechatPayment getByOutTradeNo(String outTradeNo);

    /**
     * 按源单号统计数量
     */
    Integer countBySourceNo(String sourceNo);

    /**
     * JSAPI 小程序
     *
     * @param openId      openid
     * @param appid       应用id
     * @param amount      支付金额
     * @param sourceId    源单号
     * @param sourceNo    源单号
     * @param description 描述
     * @return JsApiResponse
     */
    JsApiResponse miniPay(String openId, String appid, Long amount, Long sourceId, String sourceNo, String description);

    /**
     * 修改付款状态
     */
    void updateSuccess(Long paymentId, String transactionId, String paySuccessTime);
}
