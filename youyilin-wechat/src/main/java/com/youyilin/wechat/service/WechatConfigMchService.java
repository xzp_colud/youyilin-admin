package com.youyilin.wechat.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.common.bean.Page;
import com.youyilin.wechat.model.entity.WechatConfigMch;

import java.util.List;

public interface WechatConfigMchService extends IService<WechatConfigMch> {

    Integer getTotal(Page<WechatConfigMch> page);

    List<WechatConfigMch> getPage(Page<WechatConfigMch> page);

    /**
     * 按应用ID查询
     *
     * @param appId 应用ID
     */
    WechatConfigMch getByAppid(String appId);

    /**
     * 保存
     */
    void saveWechatConfigMch(WechatConfigMch en);

    /**
     * 删除
     */
    void delById(Long id);

    /**
     * 验证应用ID唯一性
     */
    void validateAppidUnique(WechatConfigMch en);

    /**
     * 获取支付配置
     */
    WechatConfigMch getPayConfig(String appId);
}
