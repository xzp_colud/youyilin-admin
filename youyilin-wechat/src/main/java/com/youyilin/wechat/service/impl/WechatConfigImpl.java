package com.youyilin.wechat.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.bean.Page;
import com.youyilin.common.exception.ApiException;
import com.youyilin.common.utils.ObjectUniqueUtil;
import com.youyilin.wechat.mapper.WechatConfigMapper;
import com.youyilin.wechat.model.entity.WechatConfig;
import com.youyilin.wechat.service.WechatConfigService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WechatConfigImpl extends ServiceImpl<WechatConfigMapper, WechatConfig> implements WechatConfigService {

    @Override
    public Integer getTotal(Page<WechatConfig> page) {
        return null;
    }

    @Override
    public List<WechatConfig> getPage(Page<WechatConfig> page) {
        return null;
    }

    @Override
    public WechatConfig getByAppid(String appId) {
        return super.getOne(new LambdaQueryWrapper<WechatConfig>().eq(WechatConfig::getAppid, appId));
    }

    @Override
    public void saveWechatConfig(WechatConfig en) {
        // 验证唯一性
        this.validateAppidUnique(en);

        if (en.getId() == null) {
            super.save(en);
        } else {
            WechatConfig old = super.getById(en.getId());
            if (old == null) {
                throw new ApiException("保存失败");
            }
            super.updateById(en);
        }
    }

    @Override
    public void delById(Long id) {
        super.removeById(id);
    }

    @Override
    public void validateAppidUnique(WechatConfig en) {
        WechatConfig appidUnique = this.getByAppid(en.getAppid());
        if (!ObjectUniqueUtil.validateObjectUnique(en, appidUnique)) {
            throw new ApiException("应用已配置");
        }
    }

    @Override
    public void updateToken(Long id, String access_token) {
        // 1.5小时过期，微信2小时 提前刷新凭证
        Long expiredDate = System.currentTimeMillis() + 60 * 60 * 1500;
        super.update(new LambdaUpdateWrapper<WechatConfig>()
                .set(WechatConfig::getToken, access_token)
                .set(WechatConfig::getExpiredDate, expiredDate)
                .eq(WechatConfig::getId, id));
    }
}
