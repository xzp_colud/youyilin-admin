package com.youyilin.wechat.service.impl;

import com.youyilin.common.exception.ApiException;
import com.youyilin.wechat.model.entity.WechatConfig;
import com.youyilin.wechat.model.response.GetPhoneNumberResponse;
import com.youyilin.wechat.model.response.GetTokenResponse;
import com.youyilin.wechat.model.response.MiniCodeSessionResponse;
import com.youyilin.wechat.service.WechatConfigService;
import com.youyilin.wechat.service.WechatTokenService;
import com.youyilin.wechat.utils.WechatRequestUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Service
public class WechatTokenImpl implements WechatTokenService {

    private final WechatConfigService wechatConfigService;

    public WechatTokenImpl(WechatConfigService wechatConfigService) {
        this.wechatConfigService = wechatConfigService;
    }

    @Override
    public String getCodeSession(String appid, String code) {
        WechatConfig config = wechatConfigService.getByAppid(appid);
        WechatRequestUtil requestUtil = new WechatRequestUtil.Builder().config(config).build();
        MiniCodeSessionResponse response = requestUtil.code2Session(code, MiniCodeSessionResponse.class);
        if (response == null || StringUtils.isBlank(response.getOpenid())) {
            return null;
        }
        return response.getOpenid();
    }

    @Override
    public String getPhoneNumber(String appid, String code) {
        WechatRequestUtil requestUtil = this.getRequestUtil(appid);
        GetPhoneNumberResponse response = requestUtil.getPhoneNumber(code, GetPhoneNumberResponse.class);
        if (response.getPhone_info() != null) {
            return response.getPhone_info().getPhoneNumber();
        }
        return null;
    }

    private WechatRequestUtil getRequestUtil(String appId) {
        WechatConfig config = wechatConfigService.getByAppid(appId);
        Long now = System.currentTimeMillis();
        if (StringUtils.isBlank(config.getToken()) || config.getExpiredDate() == null || config.getExpiredDate() < now) {
            WechatRequestUtil requestUtil = new WechatRequestUtil.Builder().config(config).build();
            GetTokenResponse response = requestUtil.getToken(GetTokenResponse.class);
            if (response == null || StringUtils.isBlank(response.getAccess_token())) {
                throw new ApiException("获取配置异常");
            }

            wechatConfigService.updateToken(config.getId(), response.getAccess_token());
            config.setToken(response.getAccess_token());
        }

        return new WechatRequestUtil.Builder().config(config).build();
    }
}
