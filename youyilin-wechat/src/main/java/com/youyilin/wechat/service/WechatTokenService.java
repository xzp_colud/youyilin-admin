package com.youyilin.wechat.service;

public interface WechatTokenService {

    /**
     * 获取openid
     *
     * @param appid 应用ID
     * @param code  wx.login之后得到的code
     * @return String
     */
    String getCodeSession(String appid, String code);

    /**
     * 获取手机号
     *
     * @param appid 应用ID
     * @param code  授权之后拿到的code
     * @return String
     */
    String getPhoneNumber(String appid, String code);
}
