package com.youyilin.wechat.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wechat.pay.java.core.Config;
import com.wechat.pay.java.core.RSAConfig;
import com.wechat.pay.java.service.payments.jsapi.JsapiServiceExtension;
import com.wechat.pay.java.service.payments.jsapi.model.Amount;
import com.wechat.pay.java.service.payments.jsapi.model.Payer;
import com.wechat.pay.java.service.payments.jsapi.model.PrepayRequest;
import com.wechat.pay.java.service.payments.jsapi.model.PrepayWithRequestPaymentResponse;
import com.youyilin.common.exception.ApiException;
import com.youyilin.wechat.enums.WechatPaymentStatusEnum;
import com.youyilin.wechat.mapper.WechatPaymentMapper;
import com.youyilin.wechat.model.entity.WechatConfigMch;
import com.youyilin.wechat.model.entity.WechatPayment;
import com.youyilin.wechat.model.response.JsApiResponse;
import com.youyilin.wechat.service.WechatConfigMchService;
import com.youyilin.wechat.service.WechatPaymentService;
import com.youyilin.wechat.utils.SnUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
public class WechatPaymentImpl extends ServiceImpl<WechatPaymentMapper, WechatPayment> implements WechatPaymentService {

    private final WechatConfigMchService wechatConfigMchService;

    public WechatPaymentImpl(WechatConfigMchService wechatConfigMchService) {
        this.wechatConfigMchService = wechatConfigMchService;
    }

    @Override
    public WechatPayment getByOutTradeNo(String outTradeNo) {
        return super.getOne(new LambdaQueryWrapper<WechatPayment>().eq(WechatPayment::getOutTradeNo, outTradeNo));
    }

    @Override
    public Integer countBySourceNo(String sourceNo) {
        long num = super.count(new LambdaQueryWrapper<WechatPayment>().eq(WechatPayment::getSourceNo, sourceNo));
        return (int) num;
    }

    @Override
    @Transactional
    public JsApiResponse miniPay(String openId, String appid, Long amount, Long sourceId, String sourceNo, String description) {
        WechatConfigMch configMch = wechatConfigMchService.getPayConfig(appid);
        long paymentId = IdWorker.getId();
        Config config = this.initPayConfig(configMch);
        String mchid = configMch.getMchId();
        String outTradeNo = SnUtil.getPaymentSn();

        this.validateMoney(amount);
        this.validateOutTradeNo(outTradeNo);
        this.validateOpenId(openId);
        this.validateDescription(description);
        // 请求服务类
        JsapiServiceExtension service = new JsapiServiceExtension.Builder().config(config).build();

        // 请求体
        com.wechat.pay.java.service.payments.jsapi.model.PrepayRequest request = new PrepayRequest();
        request.setAmount(this.createAmount(amount));
        request.setPayer(this.createPayer(openId));
        request.setAppid(appid);
        request.setMchid(mchid);
        request.setDescription(description);
        request.setNotifyUrl(configMch.getNotifyUrl() + "/" + paymentId);
        request.setOutTradeNo(outTradeNo);

        // 调用下单
        PrepayWithRequestPaymentResponse response = service.prepayWithRequestPayment(request);
        this.validateResponse(response);
        // 数据签名返回JSAPI支付所需参数
        JsApiResponse jsApiResponse = this.payAfter(response);

        // 保存记录
        WechatPayment save = new WechatPayment();
        save.setId(paymentId);
        save.setAmount(amount);
        save.setAppid(appid);
        save.setMchid(mchid);
        save.setDescription(description);
        save.setOpenid(openId);
        save.setSourceNo(sourceNo);
        save.setSourceId(sourceId);
        save.setOutTradeNo(outTradeNo);
        save.setStatus(WechatPaymentStatusEnum.NORMAL.getCode());
        save.setTradeType(WechatPayment.TradeType.JSAPI.name());
        super.save(save);

        return jsApiResponse;
    }

    @Override
    public void updateSuccess(Long paymentId, String transactionId, String paySuccessTime) {
        super.update(new LambdaUpdateWrapper<WechatPayment>()
                .set(WechatPayment::getStatus, WechatPaymentStatusEnum.PROCESSED.getCode())
                .set(WechatPayment::getTransactionId, transactionId)
                .set(WechatPayment::getSuccessTime, paySuccessTime)
                .eq(WechatPayment::getId, paymentId));
    }

    /**
     * 获取支付配置
     */
    private Config initPayConfig(WechatConfigMch configMch) {
        return new RSAConfig.Builder()
                .merchantId(configMch.getMchId())
                .privateKey(configMch.getPrivateKeyPath())
                .merchantSerialNumber(configMch.getMchNumber())
                .wechatPayCertificates(configMch.getWechatPayPath())
                .build();
    }

    /**
     * 验证金额
     */
    private void validateMoney(Long money) {
        if (money == null || money <= 0) {
            log.error("【JSAPI支付】 支付金额不能为空");
            throw new ApiException("金额不能为0");
        }
    }

    /**
     * 验证单号
     */
    private void validateOutTradeNo(String outTradeNo) {
        if (StringUtils.isBlank(outTradeNo)) {
            log.error("【JSAPI支付】 支付单号不能为空");
            throw new ApiException("支付单号异常");
        }
    }

    /**
     * 验证支付人
     */
    private void validateOpenId(String openId) {
        if (StringUtils.isBlank(openId)) {
            log.error("【JSAPI支付】 支付用户OPENID不能为空");
            throw new ApiException("支付异常");
        }
    }

    /**
     * 验证支付详情
     */
    private void validateDescription(String description) {
        if (StringUtils.isBlank(description)) {
            log.error("【JSAPI支付】 支付详情不能为空");
            throw new ApiException("支付异常");
        }
    }

    /**
     * 设置请求金额
     *
     * @param money 金额
     * @return Amount
     */
    private com.wechat.pay.java.service.payments.jsapi.model.Amount createAmount(Long money) {
        com.wechat.pay.java.service.payments.jsapi.model.Amount amount = new Amount();
        amount.setTotal(money.intValue());
        return amount;
    }

    /**
     * 设置请支付人
     *
     * @param openId 应用下对应的付款人
     * @return Payer
     */
    private Payer createPayer(String openId) {
        Payer payer = new Payer();
        payer.setOpenid(openId);
        return payer;
    }

    /**
     * 验证响应结果
     *
     * @param response 响应结果
     */
    private void validateResponse(PrepayWithRequestPaymentResponse response) {
        log.info("【微信支付】 Response: {}", JSONObject.toJSONString(response));
    }

    /**
     * JSAPI签名
     *
     * @param response 下单响应
     * @return 签名对象
     */
    private JsApiResponse payAfter(PrepayWithRequestPaymentResponse response) {
        JsApiResponse jsApi = new JsApiResponse();

        jsApi.setTimeStamp(response.getTimeStamp());
        jsApi.setNonceStr(response.getNonceStr());
        jsApi.setPackages(response.getPackageVal());
        jsApi.setSignType(response.getSignType());
        jsApi.setPaySign(response.getPaySign());

        return jsApi;
    }
}
