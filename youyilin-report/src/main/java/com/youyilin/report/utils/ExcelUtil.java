package com.youyilin.report.utils;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.exception.ExcelCommonException;
import com.alibaba.excel.read.listener.ReadListener;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.alibaba.excel.util.StringUtils;
import com.alibaba.excel.write.handler.WriteHandler;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.alibaba.excel.write.style.column.LongestMatchColumnWidthStyleStrategy;
import com.alibaba.fastjson.JSON;
import com.youyilin.common.bean.R;
import com.youyilin.report.model.SheetBean;
import com.youyilin.report.writeHandler.ExcelWriteHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * EXCEL 操作工具类
 */
@Slf4j
public class ExcelUtil {

    private static final String XLS = "APPLICATION/VND.MS-EXCEL";
    private static final String XLSX = "APPLICATION/VND.OPENXMLFORMATS-OFFICEDOCUMENT.SPREADSHEETML.SHEET";

    /**
     * 浏览器 下载 EXCEL 文件
     *
     * @param list      list数据
     * @param object    对象名称
     * @param baseName  文件名称
     * @param sheetName 表格名称
     */
    public static <T> void browserWriteExcel(HttpServletResponse response, List<T> list, Object object, String baseName, String sheetName, int sheetIndex) throws IOException {
        try {
            if (StringUtils.isBlank(sheetName)) {
                sheetName = "sheet";
            }
            // 刷新响应
            freshResponse(response, baseName);
            WriteHandler writeHandler = getWriteHandlerInvoke(object);
            // 这里需要设置不关闭流
            EasyExcel.write(response.getOutputStream(), object.getClass())
                    .registerWriteHandler(writeHandler)
                    .registerWriteHandler(new LongestMatchColumnWidthStyleStrategy())
                    .useDefaultStyle(Boolean.FALSE)
                    .excelType(ExcelTypeEnum.XLSX)
                    .autoCloseStream(Boolean.FALSE)
                    .sheet(sheetName)
                    .sheetNo(sheetIndex)
                    .doWrite(list);
        } catch (Exception e) {
            log.error("【导出异常】", e);
            // 重置response
            response.reset();
            response.setHeader("Access-Control-Allow-Origin", "*");
            response.setContentType("application/json");
            response.setCharacterEncoding("utf-8");
            response.getWriter().println(JSON.toJSONString(R.fail(e.getMessage())));
        }
    }

    /**
     * 浏览器 下载 EXCEL 文件
     */
    public static void browserWriteExcel(HttpServletResponse response, String baseName, List<SheetBean> sheetList) throws IOException {
        if (CollectionUtils.isEmpty(sheetList)) {
            return;
        }
        // 刷新响应
        freshResponse(response, baseName);

        ExcelWriter excelWriter = EasyExcel.write(response.getOutputStream()).build();
        for (int i = 0; i < sheetList.size(); i++) {
            SheetBean sheetBean = sheetList.get(i);

            String sheetName = StringUtils.isBlank(sheetBean.getSheetName()) ? "sheet" : sheetBean.getSheetName();
            Integer sheetIndex = sheetBean.getSheetIndex() == null || sheetBean.getSheetIndex() < 0 ? i : sheetBean.getSheetIndex();

            WriteSheet writeSheet = EasyExcel.writerSheet(sheetIndex, sheetName)
                    .registerWriteHandler(getWriteHandlerInvoke(sheetBean.getSheetClass()))
                    .registerWriteHandler(new LongestMatchColumnWidthStyleStrategy())
                    .head(sheetBean.getSheetClass()).build();
            if (CollectionUtils.isEmpty(sheetBean.getData())) {
                excelWriter.write(new ArrayList<>(), writeSheet);
                continue;
            }
            excelWriter.write(sheetBean.getData(), writeSheet);
        }
        excelWriter.finish();
        response.flushBuffer();
    }

    private static void freshResponse(HttpServletResponse response, String baseName) throws UnsupportedEncodingException {
        String name = String.valueOf(System.currentTimeMillis());
        name = StringUtils.isBlank(baseName) ? name : baseName + "_" + name;
        // 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
        String fileName = URLEncoder.encode(name, "UTF-8").replaceAll("\\+", "%20") + ".xlsx";
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        response.setHeader("Access-Control-Expose-Headers", "Content-disposition");
        response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName);
    }

    /**
     * 浏览器 读取 EXCEL 文件
     *
     * @param file         上传文件
     * @param o            解析对象
     * @param readListener 解析器
     * @param sheetIndex   表格序列号
     */
    public static <T> void browserReadExcel(MultipartFile file, Class<?> o, ReadListener<T> readListener, int sheetIndex) {
        try {
            browserReadValidate(file, o);

            EasyExcel.read(file.getInputStream(), o, readListener).sheet(sheetIndex).doRead();
        } catch (ExcelCommonException e) {
            throw new ExcelCommonException(e.getMessage());
        } catch (Exception e) {
            throw new ExcelCommonException("上传失败");
        }
    }

    public static <T> List<T> browserReadExcelAsync(MultipartFile file, Class<?> o, int sheetIndex) {
        try {
            browserReadValidate(file, o);

            return EasyExcel.read(file.getInputStream()).head(o).sheet(sheetIndex).doReadSync();
        } catch (ExcelCommonException e) {
            throw new ExcelCommonException(e.getMessage());
        } catch (Exception e) {
            throw new ExcelCommonException("上传失败");
        }
    }

    private static void browserReadValidate(MultipartFile file, Class<?> o) {
        if (file == null || StringUtils.isBlank(file.getContentType())) {
            throw new ExcelCommonException("文件类型异常");
        }
        String contentType = file.getContentType().toUpperCase();
        boolean isExcel = contentType.equals(XLS) || contentType.equals(XLSX);
        if (!isExcel) {
            throw new ExcelCommonException("请上传.xls或.xlsx文件");
        }
        if (o == null) {
            throw new ExcelCommonException("未配置解析对象");
        }
    }

    /**
     * 反射获取自定义样式
     *
     * @param o 对象
     * @return WriteHandler
     */
    private static WriteHandler getWriteHandlerInvoke(Object o) {
        if (o == null) {
            return new ExcelWriteHandler();
        }
        try {
            Method method = o.getClass().getMethod("getWriteHandler");
            Object writeHandler = method.invoke(o);
            if (writeHandler == null) {
                return new ExcelWriteHandler();
            }
            return (WriteHandler) writeHandler;
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            return new ExcelWriteHandler();
        }
    }

    /**
     * 返回成功
     */
    public static void doResponseSuccess(HttpServletResponse response) throws IOException {
        response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");
        response.getWriter().println(JSON.toJSONString(R.success("导入成功")));
    }
}
