package com.youyilin.report.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.report.model.product.ImportProductTechnology;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ImportProductTechnologyMapper extends BaseMapper<ImportProductTechnology> {
}
