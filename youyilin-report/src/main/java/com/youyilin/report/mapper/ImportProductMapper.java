package com.youyilin.report.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.report.model.product.ImportProduct;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ImportProductMapper extends BaseMapper<ImportProduct> {
}
