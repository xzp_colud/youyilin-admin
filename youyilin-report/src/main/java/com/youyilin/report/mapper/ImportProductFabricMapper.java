package com.youyilin.report.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.report.model.product.ImportProductFabric;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ImportProductFabricMapper extends BaseMapper<ImportProductFabric> {
}
