package com.youyilin.report.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.report.model.product.ImportProductPlan;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ImportProductPlanMapper extends BaseMapper<ImportProductPlan> {
}
