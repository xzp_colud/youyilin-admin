package com.youyilin.report.mapper;

import com.youyilin.report.model.inventory.ExportInventory;
import com.youyilin.report.model.inventory.query.ExportInventoryQuery;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ExportInventoryMapper {

    List<ExportInventory> exportInventory(ExportInventoryQuery query);
}
