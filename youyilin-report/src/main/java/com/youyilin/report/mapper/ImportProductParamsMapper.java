package com.youyilin.report.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.report.model.product.ImportProductParams;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ImportProductParamsMapper extends BaseMapper<ImportProductParams> {
}
