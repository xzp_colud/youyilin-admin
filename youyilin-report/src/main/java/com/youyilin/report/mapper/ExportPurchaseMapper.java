package com.youyilin.report.mapper;

import com.youyilin.report.model.purchase.ExportPurchaseOrder;
import com.youyilin.report.model.purchase.query.ExportPurchaseOrderQuery;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ExportPurchaseMapper {

    List<ExportPurchaseOrder> exportPurchase(ExportPurchaseOrderQuery query);
}
