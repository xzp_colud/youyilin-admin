package com.youyilin.report.coverter;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.converters.WriteConverterContext;
import com.alibaba.excel.metadata.data.WriteCellData;
import com.youyilin.common.enums.StatusEnum;

/**
 * 状态 数值 转换器
 */
public class StatusConverter implements Converter<Integer> {

    @Override
    public WriteCellData<String> convertToExcelData(WriteConverterContext<Integer> context) {
        String value = StatusEnum.DISABLED.getInfo();
        if (context.getValue() != null && context.getValue() == 1) {
            value = StatusEnum.NORMAL.getInfo();
        }
        return new WriteCellData<>(value);
    }
}
