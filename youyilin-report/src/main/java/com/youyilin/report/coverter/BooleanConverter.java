package com.youyilin.report.coverter;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.converters.WriteConverterContext;
import com.alibaba.excel.metadata.data.WriteCellData;
import com.youyilin.common.enums.BooleanEnum;

/**
 * 状态 数值 转换器
 */
public class BooleanConverter implements Converter<Integer> {

    @Override
    public WriteCellData<String> convertToExcelData(WriteConverterContext<Integer> context) {
        return new WriteCellData<>(BooleanEnum.isYes(context.getValue()) ? "是" : "否");
    }
}
