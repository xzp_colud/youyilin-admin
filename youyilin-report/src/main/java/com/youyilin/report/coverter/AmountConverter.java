package com.youyilin.report.coverter;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.converters.WriteConverterContext;
import com.alibaba.excel.metadata.data.WriteCellData;
import com.youyilin.common.utils.FormatAmountUtil;

/**
 * 金额 转换器
 */
public class AmountConverter implements Converter<Long> {

    @Override
    public WriteCellData<String> convertToExcelData(WriteConverterContext<Long> context) {
        String value = FormatAmountUtil.format(context.getValue());
        return new WriteCellData<>(value);
    }
}
