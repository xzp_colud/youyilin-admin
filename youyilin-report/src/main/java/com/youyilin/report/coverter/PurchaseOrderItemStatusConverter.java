package com.youyilin.report.coverter;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.converters.WriteConverterContext;
import com.alibaba.excel.metadata.data.WriteCellData;
import com.youyilin.order.enums.PurchaseOrderItemStatusEnum;

/**
 * 采购单状态 数值 转换器
 */
public class PurchaseOrderItemStatusConverter implements Converter<Integer> {

    @Override
    public WriteCellData<String> convertToExcelData(WriteConverterContext<Integer> context) {
        return new WriteCellData<>(PurchaseOrderItemStatusEnum.queryInfoByCode(context.getValue()));
    }
}
