package com.youyilin.report.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.report.model.product.ImportProductFabric;

import java.util.List;

public interface ImportProductFabricService extends IService<ImportProductFabric> {

    void delByProductIds(List<Long> productIds);
}
