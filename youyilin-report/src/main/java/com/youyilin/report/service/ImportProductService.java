package com.youyilin.report.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.report.model.product.ImportProduct;

public interface ImportProductService extends IService<ImportProduct> {

}
