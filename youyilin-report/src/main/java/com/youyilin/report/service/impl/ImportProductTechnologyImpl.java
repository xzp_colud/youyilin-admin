package com.youyilin.report.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.report.mapper.ImportProductTechnologyMapper;
import com.youyilin.report.model.product.ImportProductTechnology;
import com.youyilin.report.service.ImportProductTechnologyService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ImportProductTechnologyImpl extends ServiceImpl<ImportProductTechnologyMapper, ImportProductTechnology> implements ImportProductTechnologyService {

    @Override
    public void delByProductIds(List<Long> productIds) {
        super.remove(new LambdaQueryWrapper<ImportProductTechnology>().in(ImportProductTechnology::getSourceProductId, productIds));
    }
}
