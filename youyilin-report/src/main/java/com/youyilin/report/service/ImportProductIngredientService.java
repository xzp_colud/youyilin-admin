package com.youyilin.report.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.report.model.product.ImportProductIngredient;

import java.util.List;

public interface ImportProductIngredientService extends IService<ImportProductIngredient> {

    void delByProductIds(List<Long> productIds);
}
