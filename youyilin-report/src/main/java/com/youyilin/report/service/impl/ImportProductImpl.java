package com.youyilin.report.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.report.mapper.ImportProductMapper;
import com.youyilin.report.model.product.ImportProduct;
import com.youyilin.report.service.ImportProductService;
import org.springframework.stereotype.Service;

@Service
public class ImportProductImpl extends ServiceImpl<ImportProductMapper, ImportProduct> implements ImportProductService {

}
