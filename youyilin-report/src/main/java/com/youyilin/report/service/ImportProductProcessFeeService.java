package com.youyilin.report.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.report.model.product.ImportProductProcessFee;

import java.util.List;

public interface ImportProductProcessFeeService extends IService<ImportProductProcessFee> {

    void delByProductIds(List<Long> productIds);
}
