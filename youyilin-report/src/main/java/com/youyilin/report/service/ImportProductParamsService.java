package com.youyilin.report.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.report.model.product.ImportProductParams;

import java.util.List;

public interface ImportProductParamsService extends IService<ImportProductParams> {

    /**
     * 按产品删除
     */
    void delByProductIds(List<String> productIds);
}
