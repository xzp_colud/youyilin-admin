package com.youyilin.report.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.report.mapper.ImportProductFabricMapper;
import com.youyilin.report.model.product.ImportProductFabric;
import com.youyilin.report.service.ImportProductFabricService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ImportProductFabricImpl extends ServiceImpl<ImportProductFabricMapper, ImportProductFabric> implements ImportProductFabricService {

    @Override
    public void delByProductIds(List<Long> productIds) {
        super.remove(new LambdaQueryWrapper<ImportProductFabric>().in(ImportProductFabric::getSourceProductId, productIds));
    }
}
