package com.youyilin.report.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.report.mapper.ImportProductParamsMapper;
import com.youyilin.report.model.product.ImportProductParams;
import com.youyilin.report.service.ImportProductParamsService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ImportProductParamsImpl extends ServiceImpl<ImportProductParamsMapper, ImportProductParams> implements ImportProductParamsService {

    @Override
    public void delByProductIds(List<String> productIds) {
        super.remove(new LambdaQueryWrapper<ImportProductParams>().in(ImportProductParams::getProductId, productIds));
    }
}
