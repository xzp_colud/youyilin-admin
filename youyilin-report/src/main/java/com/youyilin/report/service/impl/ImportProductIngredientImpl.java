package com.youyilin.report.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.report.mapper.ImportProductIngredientMapper;
import com.youyilin.report.model.product.ImportProductIngredient;
import com.youyilin.report.service.ImportProductIngredientService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ImportProductIngredientImpl extends ServiceImpl<ImportProductIngredientMapper, ImportProductIngredient> implements ImportProductIngredientService {

    @Override
    public void delByProductIds(List<Long> productIds) {
        super.remove(new LambdaQueryWrapper<ImportProductIngredient>().in(ImportProductIngredient::getSourceProductId, productIds));
    }
}
