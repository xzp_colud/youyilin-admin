package com.youyilin.report.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.report.model.product.ImportProductPlan;

import java.util.List;

public interface ImportProductPlanService extends IService<ImportProductPlan> {

    /**
     * 按产品删除
     */
    void delByProductIds(List<Long> productIds);
}
