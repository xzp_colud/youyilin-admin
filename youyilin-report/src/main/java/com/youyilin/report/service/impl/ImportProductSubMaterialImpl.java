package com.youyilin.report.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.report.mapper.ImportProductSubMaterialMapper;
import com.youyilin.report.model.product.ImportProductSubMaterial;
import com.youyilin.report.service.ImportProductSubMaterialService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ImportProductSubMaterialImpl extends ServiceImpl<ImportProductSubMaterialMapper, ImportProductSubMaterial> implements ImportProductSubMaterialService {

    @Override
    public void delByProductIds(List<Long> productIds) {
        super.remove(new LambdaQueryWrapper<ImportProductSubMaterial>().in(ImportProductSubMaterial::getSourceProductId, productIds));
    }
}
