package com.youyilin.report.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.report.model.product.ImportProductTechnology;

import java.util.List;

public interface ImportProductTechnologyService extends IService<ImportProductTechnology> {

    void delByProductIds(List<Long> productIds);
}
