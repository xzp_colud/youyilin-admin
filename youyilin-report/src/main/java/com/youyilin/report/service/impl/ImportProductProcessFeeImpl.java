package com.youyilin.report.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.report.mapper.ImportProductProcessFeeMapper;
import com.youyilin.report.model.product.ImportProductProcessFee;
import com.youyilin.report.service.ImportProductProcessFeeService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ImportProductProcessFeeImpl extends ServiceImpl<ImportProductProcessFeeMapper, ImportProductProcessFee> implements ImportProductProcessFeeService {

    @Override
    public void delByProductIds(List<Long> productIds) {
        super.remove(new LambdaQueryWrapper<ImportProductProcessFee>().in(ImportProductProcessFee::getSourceProductId, productIds));
    }
}
