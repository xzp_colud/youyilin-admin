package com.youyilin.report.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.report.mapper.ImportProductPlanMapper;
import com.youyilin.report.model.product.ImportProductPlan;
import com.youyilin.report.service.ImportProductPlanService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ImportProductPlanImpl extends ServiceImpl<ImportProductPlanMapper, ImportProductPlan> implements ImportProductPlanService {

    @Override
    public void delByProductIds(List<Long> productIds) {
        super.remove(new LambdaQueryWrapper<ImportProductPlan>().in(ImportProductPlan::getProductId, productIds));
    }
}
