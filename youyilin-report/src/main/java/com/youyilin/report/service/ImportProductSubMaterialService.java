package com.youyilin.report.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.report.model.product.ImportProductSubMaterial;

import java.util.List;

public interface ImportProductSubMaterialService extends IService<ImportProductSubMaterial> {

    void delByProductIds(List<Long> productIds);
}
