package com.youyilin.report.writeHandler;

import com.alibaba.excel.metadata.Head;
import com.alibaba.excel.metadata.data.WriteCellData;
import com.alibaba.excel.write.handler.CellWriteHandler;
import com.alibaba.excel.write.handler.RowWriteHandler;
import com.alibaba.excel.write.metadata.holder.WriteSheetHolder;
import com.alibaba.excel.write.metadata.holder.WriteTableHolder;
import com.alibaba.excel.write.metadata.style.WriteCellStyle;
import com.youyilin.report.cellStyle.StatusCellStyle;
import org.apache.poi.ss.usermodel.Cell;

import java.util.List;

/**
 * 自定义 单元格样式
 */
public class ExportSysMenuWriteHandler extends ExcelCellStyleWriteHandler implements RowWriteHandler, CellWriteHandler {

    @Override
    public void afterCellDispose(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder, List<WriteCellData<?>> cellDataList, Cell cell, Head head, Integer relativeRowIndex, Boolean isHead) {
        WriteCellData<?> cellData = cellDataList.get(0);
        WriteCellStyle writeCellStyle = cellData.getOrCreateStyle();
        cellCenter(writeCellStyle);
        if (!isHead) {
            String fieldName = head.getFieldName();
            if (fieldName.equals("status")) {
                String value = cellDataList.get(0).getStringValue();
                StatusCellStyle.init((value), writeCellStyle);
            }
        }
    }
}
