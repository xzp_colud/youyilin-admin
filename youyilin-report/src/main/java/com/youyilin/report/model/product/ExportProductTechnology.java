package com.youyilin.report.model.product;

import com.alibaba.excel.annotation.ExcelProperty;
import com.youyilin.report.coverter.AmountConverter;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 产品-工艺
 */
@Data
@EqualsAndHashCode
public class ExportProductTechnology {

    @ExcelProperty(index = 0, value = "方案ID")
    private String planId;
    // 厂家
    @ExcelProperty(index = 1, value = "厂家")
    private String supplierName;
    // 工艺
    @ExcelProperty(index = 2, value = "工艺")
    private String technologyName;
    // 价格
    @ExcelProperty(index = 3, value = "价格", converter = AmountConverter.class)
    private Long totalAmount;
}
