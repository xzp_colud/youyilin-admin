package com.youyilin.report.model.product;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 产品-面料
 */
@Data
@EqualsAndHashCode
public class ExportProductPlan {

    @ExcelProperty(index = 0, value = "产品ID")
    private String productId;
    // 供货商
    @ExcelProperty(index = 1, value = "唯一ID")
    private String id;
    // 产品型号
    @ExcelProperty(index = 2, value = "方案名称")
    private String title;
}
