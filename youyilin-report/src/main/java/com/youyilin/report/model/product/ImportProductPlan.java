package com.youyilin.report.model.product;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 产品-方案
 */
@Data
@EqualsAndHashCode
@TableName("product_plan")
public class ImportProductPlan {

    // ImportProduct uuid
    @TableField(exist = false)
    @ExcelProperty(index = 0, value = "产品ID")
    private String suuid;
    @TableField(exist = false)
    @ExcelProperty(index = 1, value = "唯一ID")
    private String uuid;
    // 方案名称
    @ExcelProperty(index = 2, value = "方案名称")
    private String title;
    @TableField(exist = false)
    @ExcelProperty(index = 3, value = "错误信息")
    private String errorMsg;

    // ----- 忽略字段 ----- //
    @ExcelIgnore
    private Long id;
    @ExcelIgnore
    @TableField(value = "product_id")
    private Long productId;
    @ExcelIgnore
    @TableField(value = "modify_date")
    private Date modifyDate;
}
