package com.youyilin.report.model.product;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@EqualsAndHashCode
public class ExportSku {

    // 唯一ID
    @ExcelProperty(index = 0, value = "唯一ID")
    private String id;
    // sku名称
    @ExcelProperty(index = 1, value = "SKU名称")
    private String skuName;
    // 产品ID
    @ExcelProperty(index = 2, value = "产品ID")
    private String productId;
    // 销售价
    @ExcelProperty(index = 3, value = "销售价")
    private String sellPrice;
    // 批发价
    @ExcelProperty(index = 4, value = "批发价")
    private String wholesalePrice;
    // 采购价
    @ExcelProperty(index = 5, value = "采购价")
    private String purchasePrice;
    // 方案ID
    @ExcelProperty(index = 6, value = "方案ID")
    private String planId;
    // 方案标题
    @ExcelProperty(index = 7, value = "方案标题")
    private String planTitle;
    // 状态
    @ExcelProperty(index = 8, value = "状态")
    private String status;
}
