package com.youyilin.report.model.purchase.query;

import lombok.Data;

@Data
public class ExportPurchaseOrderQuery {

    // 状态
    private Integer status;
    // 采购单号
    private String sn;
    // 外部单号
    private String orderNo;
    private String startDate;
    private String endDate;
}
