package com.youyilin.report.model.product;

import com.alibaba.excel.annotation.ExcelProperty;
import com.youyilin.report.coverter.AmountConverter;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 产品-加工费
 */
@Data
@EqualsAndHashCode
public class ExportProductProcessFee {

    @ExcelProperty(index = 0, value = "方案ID")
    private String planId;
    // 金额
    @ExcelProperty(index = 1, value = "金额", converter = AmountConverter.class)
    private Long amount;
    // 数量
    @ExcelProperty(index = 2, value = "数量")
    private String num;
}
