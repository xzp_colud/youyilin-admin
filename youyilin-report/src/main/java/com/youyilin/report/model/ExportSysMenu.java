package com.youyilin.report.model;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.write.handler.WriteHandler;
import com.youyilin.report.coverter.BooleanConverter;
import com.youyilin.report.coverter.StatusConverter;
import com.youyilin.report.writeHandler.ExportSysMenuWriteHandler;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@EqualsAndHashCode
@ExcelIgnoreUnannotated
@Accessors(chain = true)
public class ExportSysMenu {

    @ExcelProperty("修改时间")
    private Date modifyDate;
    @ExcelProperty("菜单名称")
    private String menuName;
    @ExcelProperty("菜单编码")
    private String menuCode;
    @ExcelProperty("路由路径")
    private String path;
    @ExcelProperty("路由名称")
    private String name;
    @ExcelProperty("组件路径")
    private String component;
    @ExcelProperty("是否为外链")
    private Integer link;
    @ExcelProperty("图标")
    private String icon;
    @ExcelProperty("排序")
    private Integer sort;
    @ExcelProperty(value = "是否显示", converter = BooleanConverter.class)
    private Integer visible;
    @ExcelProperty(value = "状态", converter = StatusConverter.class)
    private Integer status;
    @ExcelProperty("描述")
    private String description;
    @ExcelProperty(value = "权限类型")
    private Integer type;
    @ExcelProperty("权限标识")
    private String perms;
    @ExcelProperty("上级菜单")
    private String pidName;

//    public static class MenuTypeConverter implements Converter<Integer> {
//        @Override
//        public WriteCellData<String> convertToExcelData(WriteConverterContext<Integer> context) {
//            String value = "";
//            Integer contentValue = context.getValue();
//            if (contentValue != null) {
//                if (contentValue == SysMenu.Type.CATALOG.getCode()) {
//                    value = SysMenu.Type.CATALOG.getInfo();
//                } else if (contentValue == SysMenu.Type.MENU.getCode()) {
//                    value = SysMenu.Type.MENU.getInfo();
//                } else if (contentValue == SysMenu.Type.BUTTON.getCode()) {
//                    value = SysMenu.Type.BUTTON.getInfo();
//                }
//            }
//            return new WriteCellData<>(value);
//        }
//    }

    public static WriteHandler getWriteHandler() {
        return new ExportSysMenuWriteHandler();
    }
}
