package com.youyilin.report.model.product;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 产品-辅料
 */
@Data
@EqualsAndHashCode
@TableName("product_sub_material")
public class ImportProductSubMaterial {

    // ImportProductPlan uuid
    @TableField(exist = false)
    @ExcelProperty(index = 0, value = "唯一ID")
    private String uuid;
    // 产品型号
    @ExcelProperty(index = 1, value = "种类")
    @TableField(value = "product_name")
    private String productName;
    // 颜色
    @ExcelProperty(index = 2, value = "颜色")
    private String color;
    // 尺寸
    @ExcelProperty(index = 3, value = "尺寸")
    private String size;
    // 用量
    @TableField(exist = false)
    @ExcelProperty(index = 4, value = "单价用量")
    private String usedString;
    // 金额
    @TableField(exist = false)
    @ExcelProperty(index = 5, value = "金额")
    private String amountString;
    // 总金额
    @TableField(exist = false)
    @ExcelProperty(index = 6, value = "总金额")
    private String totalAmountString;
    @TableField(exist = false)
    @ExcelProperty(index = 7, value = "错误信息")
    private String errorMsg;

    // ----- 忽略字段 ----- //
    @ExcelIgnore
    private Long id;
    @ExcelIgnore
    @TableField(value = "source_product_id")
    private Long sourceProductId;
    @ExcelIgnore
    @TableField(value = "plan_id")
    private Long planId;
    @ExcelIgnore
    @TableField(value = "modify_date")
    private Date modifyDate;
    @ExcelIgnore
    @TableField(value = "product_id")
    private Long productId;
    @ExcelIgnore
    @TableField(value = "product_price_id")
    private Long productPriceId;
    @ExcelIgnore
    private BigDecimal used;
    @ExcelIgnore
    private Long amount;
    @ExcelIgnore
    @TableField(value = "total_amount")
    private Long totalAmount;
}
