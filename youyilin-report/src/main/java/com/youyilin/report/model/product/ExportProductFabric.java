package com.youyilin.report.model.product;

import com.alibaba.excel.annotation.ExcelProperty;
import com.youyilin.report.coverter.AmountConverter;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 产品-面料
 */
@Data
@EqualsAndHashCode
public class ExportProductFabric {

    @ExcelProperty(index = 0, value = "方案ID")
    private String planId;
    // 供货商
    @ExcelProperty(index = 1, value = "面料厂家")
    private String supplierName;
    // 产品型号
    @ExcelProperty(index = 2, value = "面料型号")
    private String productName;
    // 门幅
    @ExcelProperty(index = 3, value = "门幅")
    private String gateWidth;
    // 成分
    @ExcelProperty(index = 4, value = "成分")
    private String makeup;
    // 单价用量
    @ExcelProperty(index = 5, value = "单价用量")
    private String oneUsage;
    // 单价
    @ExcelProperty(index = 6, value = "金额", converter = AmountConverter.class)
    private Long amount;
    // 总价
    @ExcelProperty(index = 7, value = "总金额", converter = AmountConverter.class)
    private Long totalAmount;
}
