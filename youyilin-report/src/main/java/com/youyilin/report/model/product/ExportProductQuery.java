package com.youyilin.report.model.product;

import lombok.Data;

/**
 * 产品导出条件
 */
@Data
public class ExportProductQuery {

    private Long categoryId;
    private Long supplierId;
    private String name;
    private String sn;
}
