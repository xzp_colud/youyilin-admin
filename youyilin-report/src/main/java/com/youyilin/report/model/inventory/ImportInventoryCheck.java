package com.youyilin.report.model.inventory;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode
public class ImportInventoryCheck {

    // 产品型号
    @ExcelProperty(index = 0, value = "商品型号")
    private String name;
    // SKU
    @ExcelProperty(index = 1, value = "SKU名称")
    private String skuName;
    // 仓库
    @ExcelProperty(index = 2, value = "仓库")
    private String warehouseName;
    // 库区
    @ExcelProperty(index = 3, value = "库区")
    private String warehouseAreaName;
    // 数量
    @ExcelProperty(index = 4, value = "盘点数量")
    private String qty;
    // 错误信息
    @TableField(exist = false)
    @ExcelProperty(index = 6, value = "错误信息")
    private String errorMsg;

    // ----- 忽略字段 ----- //
    @ExcelIgnore
    private BigDecimal checkQty;
    @ExcelIgnore
    private Long productId;
    @ExcelIgnore
    private Long skuId;
    @ExcelIgnore
    private Long warehouseId;
    @ExcelIgnore
    private Long warehouseAreaId;
}
