package com.youyilin.report.model.product;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 产品-加工费
 */
@Data
@EqualsAndHashCode
@TableName("product_process_fee")
public class ImportProductProcessFee {

    // ImportProductPlan uuid
    @TableField(exist = false)
    @ExcelProperty(index = 0, value = "唯一ID")
    private String uuid;
    // 金额
    @TableField(exist = false)
    @ExcelProperty(index = 1, value = "金额")
    private String amountString;
    // 数量
    @TableField(exist = false)
    @ExcelProperty(index = 2, value = "数量")
    private String numString;
    // 错误信息
    @TableField(exist = false)
    @ExcelProperty(index = 3, value = "错误信息")
    private String errorMsg;

    // ----- 忽略字段 ----- //
    @ExcelIgnore
    private Long id;
    @ExcelIgnore
    @TableField(value = "source_product_id")
    private Long sourceProductId;
    @ExcelIgnore
    @TableField(value = "plan_id")
    private Long planId;
    @ExcelIgnore
    @TableField(value = "modify_date")
    private Date modifyDate;
    @ExcelIgnore
    private BigDecimal num;
    @ExcelIgnore
    private Long amount;
}
