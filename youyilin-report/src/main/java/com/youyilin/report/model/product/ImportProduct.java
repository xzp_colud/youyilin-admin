package com.youyilin.report.model.product;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 产品
 */
@Data
@EqualsAndHashCode
@TableName("product")
public class ImportProduct {

    // 唯一ID
    @ExcelProperty(index = 0, value = "唯一ID")
    @TableField(exist = false)
    private String uuid;
    // 产品型号
    @ExcelProperty(index = 1, value = "产品型号")
    private String name;
    // 内部编号
    @ExcelProperty(index = 2, value = "内部编号")
    private String sn;
    // 供货商
    @ExcelProperty(index = 3, value = "供货商")
    @TableField(value = "supplier_name")
    private String supplierName;
    // 分类
    @ExcelProperty(index = 4, value = "分类")
    @TableField(value = "category_name")
    private String categoryName;
    // 图片
    @ExcelProperty(index = 5, value = "图片")
    @TableField(value = "image_url")
    private String imageUrl;
    // 错误信息
    @TableField(exist = false)
    @ExcelProperty(index = 6, value = "错误信息")
    private String errorMsg;

    // ----- 忽略字段 ----- //
    @ExcelIgnore
    private Long id;
    @ExcelIgnore
    @TableField(value = "modify_date")
    private Date modifyDate;
    @ExcelIgnore
    @TableField(value = "supplier_id")
    private Long supplierId;
    @ExcelIgnore
    @TableField(value = "category_id")
    private Long categoryId;
    @ExcelIgnore
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;

}
