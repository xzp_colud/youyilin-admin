package com.youyilin.report.model.product;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 批量产品 正常、禁用
 */
@Data
@EqualsAndHashCode
public class ImportProductStatus {

    @ExcelProperty(index = 0, value = "型号")
    private String productName;
    @ExcelProperty(index = 1, value = "状态")
    private String statusString;
    @ExcelProperty(index = 2, value = "错误信息")
    private String errorMsg;

    @ExcelIgnore
    private Long id;
    @ExcelIgnore
    private Integer status;
}
