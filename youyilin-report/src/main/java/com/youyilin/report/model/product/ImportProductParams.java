package com.youyilin.report.model.product;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode
@TableName("product_params")
public class ImportProductParams {

    @TableField(value = "product_id")
    @ExcelProperty(index = 0, value = "产品ID")
    private String productId;
    @TableField(exist = false)
    @ExcelProperty(index = 1, value = "型号")
    private String productName;
    @TableField(value = "params_id")
    @ExcelProperty(index = 2, value = "属性ID")
    private String paramsId;
    @TableField(exist = false)
    @ExcelProperty(index = 3, value = "属性名称")
    private String title;
    @ExcelProperty(index = 4, value = "属性值")
    private String value;
    @TableField(exist = false)
    @ExcelProperty(index = 5, value = "错误信息")
    private String errorMsg;

    @ExcelIgnore
    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT)
    @ExcelIgnore
    private Date modifyDate;
}
