package com.youyilin.report.model.product;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 产品-配料
 */
@Data
@EqualsAndHashCode
@TableName("product_ingredient")
public class ImportProductIngredient {

    // ImportProductPlan uuid
    @TableField(exist = false)
    @ExcelProperty(index = 0, value = "唯一ID")
    private String uuid;
    // 供货商
    @ExcelProperty(index = 1, value = "面料厂家")
    @TableField(value = "supplier_name")
    private String supplierName;
    // 产品型号
    @ExcelProperty(index = 2, value = "面料型号")
    @TableField(value = "product_name")
    private String productName;
    // 门幅
    @ExcelProperty(index = 3, value = "门幅")
    @TableField(value = "gate_width")
    private String gateWidth;
    // 成分
    @ExcelProperty(index = 4, value = "成分")
    private String makeup;
    // 单价用量
    @TableField(exist = false)
    @ExcelProperty(index = 5, value = "单价用量")
    private String oneUsageString;
    // 单价
    @TableField(exist = false)
    @ExcelProperty(index = 6, value = "金额")
    private String amountString;
    // 总价
    @TableField(exist = false)
    @ExcelProperty(index = 7, value = "总金额")
    private String totalAmountString;
    @TableField(exist = false)
    @ExcelProperty(index = 8, value = "错误信息")
    private String errorMsg;

    // ----- 忽略字段 ----- //
    @ExcelIgnore
    private Long id;
    @ExcelIgnore
    @TableField(value = "modify_date")
    private Date modifyDate;
    @ExcelIgnore
    @TableField(value = "source_product_id")
    private Long sourceProductId;
    @ExcelIgnore
    @TableField(value = "plan_id")
    private Long planId;
    @ExcelIgnore
    @TableField(value = "supplier_id")
    private Long supplierId;
    @ExcelIgnore
    @TableField(value = "product_id")
    private Long productId;
    @ExcelIgnore
    @TableField(value = "product_price_id")
    private Long productPriceId;
    @ExcelIgnore
    @TableField(value = "one_usage")
    private BigDecimal oneUsage;
    @ExcelIgnore
    private Long amount;
    @ExcelIgnore
    @TableField(value = "total_amount")
    private Long totalAmount;
}
