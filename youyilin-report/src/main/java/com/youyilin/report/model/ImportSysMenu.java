//package com.youyilin.report.model;
//
//import com.alibaba.excel.annotation.ExcelProperty;
//import lombok.Data;
//import lombok.EqualsAndHashCode;
//
//@Data
//@EqualsAndHashCode
//public class ImportSysMenu {
//
//    @ExcelProperty(index = 1)
//    private String modifyDate;
//    @ExcelProperty(index = 2)
//    private String menuName;
//    @ExcelProperty(index = 3)
//    private String menuCode;
//    @ExcelProperty(index = 4)
//    private String path;
//    @ExcelProperty(index = 5)
//    private String name;
//    @ExcelProperty(index = 6)
//    private String component;
//    @ExcelProperty(index = 7)
//    private String link;
//    @ExcelProperty(index = 8)
//    private String icon;
//    @ExcelProperty(index = 9)
//    private String sort;
//    @ExcelProperty(index = 10)
//    private String visible;
//    @ExcelProperty(index = 11)
//    private String status;
//    @ExcelProperty(index = 12)
//    private String description;
//    @ExcelProperty(index = 13)
//    private String type;
//    @ExcelProperty(index = 14)
//    private String perms;
//    @ExcelProperty(index = 15)
//    private String pidName;
//}
