package com.youyilin.report.model.purchase;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.youyilin.report.coverter.AmountConverter;
import com.youyilin.report.coverter.PurchaseOrderItemStatusConverter;
import lombok.Data;

/**
 * 采购单导出列表
 */
@Data
public class ExportPurchaseOrder {

    @ExcelProperty(index = 0, value = "采购单号")
    private String sn;
    @ExcelProperty(index = 1, value = "明细单号")
    private String itemSn;
    @ExcelProperty(index = 2, value = "供应商")
    private String supplierName;
    @ExcelProperty(index = 3, value = "型号")
    private String productName;
    @ExcelProperty(index = 4, value = "SKU")
    private String skuName;
    @ExcelProperty(index = 5, value = "采购金额")
    private String price;
    @ExcelProperty(index = 6, value = "预计采购")
    private String buyNum;
    @ExcelProperty(index = 7, value = "实际采购")
    private String reportNum;
    @ExcelProperty(index = 8, value = "采购总价", converter = AmountConverter.class)
    private Long totalReportAmount;
    @ExcelProperty(index = 9, value = "实际检验")
    private String reallyNum;
    @ExcelProperty(index = 10, value = "实际金额", converter = AmountConverter.class)
    private Long totalReallyAmount;
    @ExcelProperty(index = 11, value = "状态", converter = PurchaseOrderItemStatusConverter.class)
    private Integer status;
    @ExcelProperty(index = 12, value = "创建时间")
    private String createDate;
    @ExcelProperty(index = 13, value = "创建人")
    private String creator;
    @ExcelProperty(index = 14, value = "检验时间")
    private String checkDate;
    @ExcelProperty(index = 15, value = "入库时间")
    private String warehousingDate;

    @ExcelIgnore
    private Long supplierId;
}
