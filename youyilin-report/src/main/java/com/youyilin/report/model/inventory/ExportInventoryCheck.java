package com.youyilin.report.model.inventory;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@EqualsAndHashCode
public class ExportInventoryCheck {

    // 产品型号
    @ExcelProperty(index = 0, value = "商品型号")
    private String name;
    // SKU
    @ExcelProperty(index = 1, value = "SKU名称")
    private String skuName;
    // 仓库
    @ExcelProperty(index = 2, value = "仓库")
    private String warehouseName;
    // 库区
    @ExcelProperty(index = 3, value = "库区")
    private String warehouseAreaName;
    // 数量
    @ExcelProperty(index = 4, value = "盘点数量")
    private String qty;
}
