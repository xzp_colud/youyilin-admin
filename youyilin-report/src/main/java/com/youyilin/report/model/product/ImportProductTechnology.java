package com.youyilin.report.model.product;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 产品-工艺
 */
@Data
@EqualsAndHashCode
@TableName("product_technology")
public class ImportProductTechnology {

    // ImportProductPlan uuid
    @ExcelProperty(index = 0, value = "唯一ID")
    @TableField(exist = false)
    private String uuid;
    // 厂家
    @ExcelProperty(index = 1, value = "厂家")
    @TableField(value = "supplier_name")
    private String supplierName;
    // 工艺
    @ExcelProperty(index = 2, value = "工艺")
    @TableField(value = "technology_name")
    private String technologyName;
    // 价格
    @ExcelProperty(index = 3, value = "价格")
    @TableField(exist = false)
    private String totalAmountString;
    @TableField(exist = false)
    @ExcelProperty(index = 4, value = "错误信息")
    private String errorMsg;

    // ----- 忽略字段 ----- //
    @ExcelIgnore
    private Long id;
    @ExcelIgnore
    @TableField(value = "source_product_id")
    private Long sourceProductId;
    @ExcelIgnore
    @TableField(value = "plan_id")
    private Long planId;
    @ExcelIgnore
    @TableField(value = "modify_date")
    private Date modifyDate;
    @ExcelIgnore
    @TableField(value = "total_amount")
    private Long totalAmount;
}
