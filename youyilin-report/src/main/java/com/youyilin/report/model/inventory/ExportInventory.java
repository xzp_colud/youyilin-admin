package com.youyilin.report.model.inventory;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 库存信息
 */
@Data
@Accessors(chain = true)
public class ExportInventory {

    @ExcelProperty(index = 0, value = "型号")
    private String productName;
    @ExcelProperty(index = 1, value = "SKU名称")
    private String skuName;
    @ExcelProperty(index = 2, value = "仓库")
    private String warehouseName;
    @ExcelProperty(index = 3, value = "库区")
    private String warehouseAreaName;
    @ExcelProperty(index = 4, value = "批次号")
    private String lotSn;
    @ExcelProperty(index = 5, value = "自定义批次号")
    private String customLotSn;
    @ExcelProperty(index = 6, value = "库存数量")
    private String qty;
    @ExcelProperty(index = 7, value = "冻结库存")
    private String frozenQty;
    @ExcelProperty(index = 8, value = "创建时间")
    private String createDate;
    @ExcelProperty(index = 9, value = "更新时间")
    private String modifyDate;
}
