package com.youyilin.report.model.inventory;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@EqualsAndHashCode
public class ExportInventoryWarehouse {

    // 仓库名称
    @ExcelProperty(index = 0, value = "仓库名称")
    private String warehouseName;
    // 库区名称
    @ExcelProperty(index = 1, value = "库区名称")
    private String warehouseAreaName;
}
