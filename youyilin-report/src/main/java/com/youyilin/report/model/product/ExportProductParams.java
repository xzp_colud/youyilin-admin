package com.youyilin.report.model.product;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@EqualsAndHashCode
public class ExportProductParams {

    @ExcelProperty(index = 0, value = "产品ID")
    private String productId;
    @ExcelProperty(index = 1, value = "型号")
    private String productName;
    @ExcelProperty(index = 2, value = "属性ID")
    private String paramsId;
    @ExcelProperty(index = 3, value = "属性名称")
    private String title;
    @ExcelProperty(index = 4, value = "属性值")
    private String value;
}
