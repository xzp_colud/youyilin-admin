package com.youyilin.report.model.product;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class ExportProduct {

    // 唯一ID
    @ExcelProperty(index = 0, value = "唯一ID")
    private String id;
    // 产品型号
    @ExcelProperty(index = 1, value = "产品型号")
    private String name;
    // 内部编号
    @ExcelProperty(index = 2, value = "内部编号")
    private String sn;
    // 供货商
    @ExcelProperty(index = 3, value = "供货商")
    @TableField(value = "supplier_name")
    private String supplierName;
    // 分类
    @ExcelProperty(index = 4, value = "分类")
    @TableField(value = "category_name")
    private String categoryName;
    // 图片
    @ExcelProperty(index = 5, value = "图片")
    @TableField(value = "image_url")
    private String imageUrl;

    @ExcelIgnore
    private Long supplierId;
    @ExcelIgnore
    private Long categoryId;
}
