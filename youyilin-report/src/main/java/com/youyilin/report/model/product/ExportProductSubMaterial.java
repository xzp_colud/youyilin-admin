package com.youyilin.report.model.product;

import com.alibaba.excel.annotation.ExcelProperty;
import com.youyilin.report.coverter.AmountConverter;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 产品-辅料
 */
@Data
@EqualsAndHashCode
public class ExportProductSubMaterial {

    @ExcelProperty(index = 0, value = "方案ID")
    private String planId;
    // 产品型号
    @ExcelProperty(index = 1, value = "种类")
    private String productName;
    // 颜色
    @ExcelProperty(index = 2, value = "颜色")
    private String color;
    // 尺寸
    @ExcelProperty(index = 3, value = "尺寸")
    private String size;
    // 用量
    @ExcelProperty(index = 4, value = "单价用量")
    private String used;
    // 金额
    @ExcelProperty(index = 5, value = "金额", converter = AmountConverter.class)
    private Long amount;
    // 总金额
    @ExcelProperty(index = 6, value = "总金额", converter = AmountConverter.class)
    private Long totalAmount;
}
