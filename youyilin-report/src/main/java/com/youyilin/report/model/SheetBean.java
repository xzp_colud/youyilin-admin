package com.youyilin.report.model;

import lombok.Data;

import java.util.List;

@Data
public class SheetBean {

    // sheet 名称
    private String sheetName;
    // sheet 序号
    private Integer sheetIndex;
    // class 类
    private Class<?> sheetClass;
    // data 数据
    private List<?> data;

    public SheetBean(String sheetName, Integer sheetIndex, Class<?> sheetClass, List<?> data) {
        this.sheetName = sheetName;
        this.sheetIndex = sheetIndex;
        this.sheetClass = sheetClass;
        this.data = data;
    }
}
