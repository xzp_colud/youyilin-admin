package com.youyilin.report.logic;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface ImportProductLogic {

    /**
     * 导入(新增)
     */
    void importExcel(HttpServletResponse response, MultipartFile file) throws IOException;

    /**
     * 导入(更新)
     */
    void importExcelUpdate(HttpServletResponse response, MultipartFile file) throws IOException;

    /**
     * 导入产品属性
     */
    void importParams(HttpServletResponse response, MultipartFile file) throws IOException;

    /**
     * 批量修改产品状态
     */
    void importProductStatus(HttpServletResponse response, MultipartFile file) throws IOException;
}
