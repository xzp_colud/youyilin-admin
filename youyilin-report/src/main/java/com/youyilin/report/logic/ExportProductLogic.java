package com.youyilin.report.logic;

import com.youyilin.report.model.product.ExportProductQuery;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface ExportProductLogic {

    /**
     * 导出产品
     */
    void exportExcel(HttpServletResponse response, ExportProductQuery query) throws IOException;

    /**
     * 导出产品属性
     */
    void exportParams(HttpServletResponse response, ExportProductQuery query) throws IOException;

    /**
     * 导出产品SKU
     */
    void exportSKU(HttpServletResponse response, ExportProductQuery query) throws IOException;
}
