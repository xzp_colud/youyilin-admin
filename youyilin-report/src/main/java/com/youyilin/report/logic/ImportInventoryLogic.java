package com.youyilin.report.logic;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface ImportInventoryLogic {

    /**
     * 盘点导入
     */
    void importExcelCheck(HttpServletResponse response, MultipartFile file) throws IOException;
}
