package com.youyilin.report.logic;

import com.youyilin.report.model.purchase.query.ExportPurchaseOrderQuery;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface ExportPurchaseLogic {

    /**
     * 采购单导出
     */
    void exportPurchaseExcel(HttpServletResponse response, ExportPurchaseOrderQuery query) throws IOException;
}
