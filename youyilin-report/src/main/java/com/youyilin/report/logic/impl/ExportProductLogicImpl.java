package com.youyilin.report.logic.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.youyilin.common.enums.StatusEnum;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.common.utils.FormatAmountUtil;
import com.youyilin.goods.entity.*;
import com.youyilin.goods.service.*;
import com.youyilin.report.logic.ExportProductLogic;
import com.youyilin.report.model.SheetBean;
import com.youyilin.report.model.product.*;
import com.youyilin.report.utils.ExcelUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ExportProductLogicImpl implements ExportProductLogic {

    private final ProductService productService;
    private final ProductPlanService planService;
    private final ProductFabricService fabricService;
    private final ProductIngredientService ingredientService;
    private final ProductSubMaterialService subMaterialService;
    private final ProductTechnologyService technologyService;
    private final ProductProcessFeeService processFeeService;
    private final ProductParamsService productParamsService;
    private final ParamsService paramsService;
    private final CategoryParamsService categoryParamsService;
    private final ProductPriceService productPriceService;

    public ExportProductLogicImpl(ProductService productService, ProductPlanService planService,
                                  ProductFabricService fabricService, ProductIngredientService ingredientService,
                                  ProductSubMaterialService subMaterialService, ProductTechnologyService technologyService,
                                  ProductProcessFeeService processFeeService, ProductParamsService productParamsService,
                                  ParamsService paramsService, CategoryParamsService categoryParamsService, ProductPriceService productPriceService) {
        this.productService = productService;
        this.planService = planService;
        this.fabricService = fabricService;
        this.ingredientService = ingredientService;
        this.subMaterialService = subMaterialService;
        this.technologyService = technologyService;
        this.processFeeService = processFeeService;
        this.productParamsService = productParamsService;
        this.paramsService = paramsService;
        this.categoryParamsService = categoryParamsService;
        this.productPriceService = productPriceService;
    }

    @Override
    public void exportExcel(HttpServletResponse response, ExportProductQuery query) throws IOException {
        List<ExportProduct> productList = this.getProduct(query);
        List<String> productIds = new ArrayList<>();
        productList.forEach(item -> productIds.add(item.getId()));
        List<ExportProductPlan> planList = this.getProductPlan(productIds);
        List<ExportProductFabric> fabricList = this.getProductFabric(productIds);
        List<ExportProductIngredient> ingredientList = this.getProductIngredient(productIds);
        List<ExportProductSubMaterial> subMaterialList = this.getProductSubMaterial(productIds);
        List<ExportProductTechnology> technologyList = this.getProductTechnology(productIds);
        List<ExportProductProcessFee> processFeeList = this.getProductProcessFee(productIds);

        List<SheetBean> sheetList = new ArrayList<>();
        sheetList.add(new SheetBean("产品", 0, ExportProduct.class, productList));
        sheetList.add(new SheetBean("方案", 1, ExportProductPlan.class, planList));
        sheetList.add(new SheetBean("面料", 2, ExportProductFabric.class, fabricList));
        sheetList.add(new SheetBean("配料", 3, ExportProductIngredient.class, ingredientList));
        sheetList.add(new SheetBean("辅料", 4, ExportProductSubMaterial.class, subMaterialList));
        sheetList.add(new SheetBean("工艺", 5, ExportProductTechnology.class, technologyList));
        sheetList.add(new SheetBean("加工费", 6, ExportProductProcessFee.class, processFeeList));
        ExcelUtil.browserWriteExcel(response, "产品", sheetList);
    }

    @Override
    public void exportParams(HttpServletResponse response, ExportProductQuery query) throws IOException {
        // 产品
        List<ExportProduct> productList = this.getProduct(query);
        List<String> productIds = new ArrayList<>();
        productList.forEach(item -> productIds.add(item.getId()));
        // 产品属性
        List<ProductParams> productParamsList = this.getProductParams(productIds);
        // 按产品分组
        Map<Long, List<ProductParams>> productParamsMap = productParamsList.stream().collect(Collectors.groupingBy(ProductParams::getProductId));
        // 商品分类-属性
        Map<Long, List<Params>> categoryParamsMap = this.getCategoryParams();

        // 构建导出数据
        List<ExportProductParams> exportList = new ArrayList<>();
        for (ExportProduct item : productList) {

            Long categoryId = item.getCategoryId();
            List<Params> paramsList = categoryParamsMap.get(categoryId);
            List<ProductParams> ppList = productParamsMap.get(Long.valueOf(item.getId()));
            if (CollectionUtils.isEmpty(paramsList)) {
                continue;
            }
            for (Params jItem : paramsList) {
                String name = jItem.getName();
                String value = "";
                if (CollectionUtils.isNotEmpty(ppList)) {
                    for (ProductParams kItem : ppList) {
                        if (jItem.getId().equals(kItem.getParamsId())) {
                            value = kItem.getValue();
                            break;
                        }
                    }
                }

                ExportProductParams exportProductParams = new ExportProductParams();
                exportProductParams.setProductId(item.getId())
                        .setProductName(item.getName())
                        .setParamsId(jItem.getId().toString())
                        .setTitle(name)
                        .setValue(value);

                exportList.add(exportProductParams);
            }
        }
        List<SheetBean> sheetList = new ArrayList<>();
        sheetList.add(new SheetBean("产品属性", 0, ExportProductParams.class, exportList));
        ExcelUtil.browserWriteExcel(response, "产品属性", sheetList);
    }

    @Override
    public void exportSKU(HttpServletResponse response, ExportProductQuery query) throws IOException {
        // 产品
        List<ExportProduct> productList = this.getProduct(query);
        List<ExportSku> exportSkuList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(productList)) {
            // 获取SKU列表
            List<Long> productIds = new ArrayList<>();
            for (ExportProduct item : productList) {
                productIds.add(Long.valueOf(item.getId()));
            }
            List<ProductPrice> priceList = productPriceService.listByProductIds(productIds);
            if (CollectionUtils.isNotEmpty(priceList)) {
                for (ProductPrice item : priceList) {
                    ExportSku exportSku = new ExportSku();
                    exportSku.setId(item.getId().toString())
                            .setSkuName(item.getSkuName())
                            .setProductId(item.getProductId().toString())
                            .setPlanId(item.getPlanId() == null ? "" : item.getPlanId().toString())
                            .setPlanTitle(item.getPlanTitle())
                            .setStatus(StatusEnum.queryInfoByCode(item.getStatus()))
                            .setSellPrice(FormatAmountUtil.format(item.getSellPrice()))
                            .setWholesalePrice(FormatAmountUtil.format(item.getWholesalePrice()))
                            .setPurchasePrice(FormatAmountUtil.format(item.getCostPrice()));

                    exportSkuList.add(exportSku);
                }
            }
        }

        List<SheetBean> sheetList = new ArrayList<>();
        sheetList.add(new SheetBean("产品", 0, ExportProduct.class, productList));
        sheetList.add(new SheetBean("产品SKU", 1, ExportSku.class, exportSkuList));
        ExcelUtil.browserWriteExcel(response, "产品SKU", sheetList);
    }

    private List<ExportProduct> getProduct(ExportProductQuery query) {
        LambdaQueryWrapper<Product> queryWrapper = new LambdaQueryWrapper<>();
        if (query.getCategoryId() != null) {
            queryWrapper.eq(Product::getCategoryId, query.getCategoryId());
        }
        if (query.getSupplierId() != null) {
            queryWrapper.eq(Product::getSupplierId, query.getSupplierId());
        }
        if (StringUtils.isNotBlank(query.getName())) {
            queryWrapper.like(Product::getName, query.getName());
        }
        if (StringUtils.isNotBlank(query.getSn())) {
            queryWrapper.like(Product::getSn, query.getSn());
        }
        queryWrapper.orderByAsc(Product::getId);

        List<Product> productList = productService.list(queryWrapper);
        if (CollectionUtils.isEmpty(productList)) {
            return new ArrayList<>();
        }

        return BeanHelper.map(productList, ExportProduct.class);
    }

    private List<ExportProductPlan> getProductPlan(List<String> productIds) {
        List<ProductPlan> planList = planService.list(new LambdaQueryWrapper<ProductPlan>()
                .in(ProductPlan::getProductId, productIds)
                .orderByAsc(ProductPlan::getProductId)
                .orderByAsc(ProductPlan::getId));
        if (CollectionUtils.isEmpty(planList)) return new ArrayList<>();

        return BeanHelper.map(planList, ExportProductPlan.class);
    }

    private List<ExportProductFabric> getProductFabric(List<String> productIds) {
        List<ProductFabric> fabricList = fabricService.list(new LambdaQueryWrapper<ProductFabric>()
                .in(ProductFabric::getSourceProductId, productIds)
                .orderByAsc(ProductFabric::getSourceProductId)
                .orderByAsc(ProductFabric::getPlanId));
        if (CollectionUtils.isEmpty(fabricList)) return new ArrayList<>();

        return BeanHelper.map(fabricList, ExportProductFabric.class);
    }

    private List<ExportProductIngredient> getProductIngredient(List<String> productIds) {
        List<ProductIngredient> ingredientList = ingredientService.list(new LambdaQueryWrapper<ProductIngredient>()
                .in(ProductIngredient::getSourceProductId, productIds)
                .orderByAsc(ProductIngredient::getSourceProductId)
                .orderByAsc(ProductIngredient::getPlanId));
        if (CollectionUtils.isEmpty(ingredientList)) return new ArrayList<>();

        return BeanHelper.map(ingredientList, ExportProductIngredient.class);
    }

    private List<ExportProductSubMaterial> getProductSubMaterial(List<String> productIds) {
        List<ProductSubMaterial> subMaterialList = subMaterialService.list(new LambdaQueryWrapper<ProductSubMaterial>()
                .in(ProductSubMaterial::getSourceProductId, productIds)
                .orderByAsc(ProductSubMaterial::getSourceProductId)
                .orderByAsc(ProductSubMaterial::getPlanId));
        if (CollectionUtils.isEmpty(subMaterialList)) return new ArrayList<>();

        return BeanHelper.map(subMaterialList, ExportProductSubMaterial.class);
    }

    private List<ExportProductTechnology> getProductTechnology(List<String> productIds) {
        List<ProductTechnology> technologyList = technologyService.list(new LambdaQueryWrapper<ProductTechnology>()
                .in(ProductTechnology::getSourceProductId, productIds)
                .orderByAsc(ProductTechnology::getSourceProductId)
                .orderByAsc(ProductTechnology::getPlanId));
        if (CollectionUtils.isEmpty(technologyList)) return new ArrayList<>();

        return BeanHelper.map(technologyList, ExportProductTechnology.class);
    }

    private List<ExportProductProcessFee> getProductProcessFee(List<String> productIds) {
        List<ProductProcessFee> processFeeList = processFeeService.list(new LambdaQueryWrapper<ProductProcessFee>()
                .in(ProductProcessFee::getSourceProductId, productIds)
                .orderByAsc(ProductProcessFee::getSourceProductId)
                .orderByAsc(ProductProcessFee::getPlanId));
        if (CollectionUtils.isEmpty(processFeeList)) return new ArrayList<>();

        return BeanHelper.map(processFeeList, ExportProductProcessFee.class);
    }

    private List<ProductParams> getProductParams(List<String> productIds) {
        return productParamsService.list(new LambdaQueryWrapper<ProductParams>()
                .in(ProductParams::getProductId, productIds)
                .orderByAsc(ProductParams::getProductId)
                .orderByAsc(ProductParams::getId));
    }

    private Map<Long, List<Params>> getCategoryParams() {
        List<Params> paramsList = paramsService.list();
        Map<Long, Params> paramsMap = new HashMap<>();
        paramsList.forEach(item -> paramsMap.put(item.getId(), item));

        List<CategoryParams> categoryParamsList = categoryParamsService.list();
        Map<Long, List<Params>> categoryParamsMap = new HashMap<>();
        for (CategoryParams item : categoryParamsList) {
            List<Params> existList = categoryParamsMap.get(item.getCategoryId());
            Params params = paramsMap.get(item.getParamsId());
            if (CollectionUtils.isEmpty(existList)) {
                existList = new ArrayList<>();
            }
            existList.add(params);
            categoryParamsMap.put(item.getCategoryId(), existList);
        }

        return categoryParamsMap;
    }
}
