package com.youyilin.report.logic.impl;

import com.youyilin.report.logic.ExportPurchaseLogic;
import com.youyilin.report.mapper.ExportPurchaseMapper;
import com.youyilin.report.model.SheetBean;
import com.youyilin.report.model.purchase.ExportPurchaseOrder;
import com.youyilin.report.model.purchase.query.ExportPurchaseOrderQuery;
import com.youyilin.report.utils.ExcelUtil;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class ExportPurchaseLogicImpl implements ExportPurchaseLogic {

    private final ExportPurchaseMapper exportPurchaseMapper;

    public ExportPurchaseLogicImpl(ExportPurchaseMapper exportPurchaseMapper) {
        this.exportPurchaseMapper = exportPurchaseMapper;
    }

    @Override
    public void exportPurchaseExcel(HttpServletResponse response, ExportPurchaseOrderQuery query) throws IOException {
        List<ExportPurchaseOrder> list = exportPurchaseMapper.exportPurchase(query);
        List<SheetBean> sheetList = new ArrayList<>();
        sheetList.add(new SheetBean("采购单", 0, ExportPurchaseOrder.class, list));
        ExcelUtil.browserWriteExcel(response, "采购单", sheetList);
    }
}
