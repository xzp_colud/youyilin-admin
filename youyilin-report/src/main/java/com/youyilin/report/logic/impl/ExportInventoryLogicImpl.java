package com.youyilin.report.logic.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.youyilin.common.enums.StatusEnum;
import com.youyilin.goods.entity.Product;
import com.youyilin.goods.entity.ProductPrice;
import com.youyilin.goods.service.ProductPriceService;
import com.youyilin.goods.service.ProductService;
import com.youyilin.report.logic.ExportInventoryLogic;
import com.youyilin.report.mapper.ExportInventoryMapper;
import com.youyilin.report.model.SheetBean;
import com.youyilin.report.model.inventory.ExportInventory;
import com.youyilin.report.model.inventory.ExportInventoryCheck;
import com.youyilin.report.model.inventory.ExportInventoryWarehouse;
import com.youyilin.report.model.inventory.query.ExportInventoryQuery;
import com.youyilin.report.utils.ExcelUtil;
import com.youyilin.warehouse.entity.Warehouse;
import com.youyilin.warehouse.entity.WarehouseArea;
import com.youyilin.warehouse.service.WarehouseAreaService;
import com.youyilin.warehouse.service.WarehouseService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ExportInventoryLogicImpl implements ExportInventoryLogic {

    private final ExportInventoryMapper exportInventoryMapper;
    private final ProductService productService;
    private final ProductPriceService productPriceService;
    private final WarehouseService warehouseService;
    private final WarehouseAreaService warehouseAreaService;

    public ExportInventoryLogicImpl(ExportInventoryMapper exportInventoryMapper, ProductService productService,
                                    ProductPriceService productPriceService, WarehouseService warehouseService,
                                    WarehouseAreaService warehouseAreaService) {
        this.exportInventoryMapper = exportInventoryMapper;
        this.productService = productService;
        this.productPriceService = productPriceService;
        this.warehouseService = warehouseService;
        this.warehouseAreaService = warehouseAreaService;
    }

    @Override
    public void exportInventoryExcel(HttpServletResponse response, ExportInventoryQuery query) throws IOException {
        List<ExportInventory> list = exportInventoryMapper.exportInventory(query);
        List<SheetBean> sheetList = new ArrayList<>();
        sheetList.add(new SheetBean("库存信息", 0, ExportInventory.class, list));
        ExcelUtil.browserWriteExcel(response, "库存信息", sheetList);
    }

    @Override
    public void exportCheckExcel(HttpServletResponse response) throws IOException {
        // 商品列表
        List<ExportInventoryCheck> exportInventoryList = this.initExportInventoryCheck();
        // 仓库、库区列表
        List<ExportInventoryWarehouse> exportWarehouseList = this.initExportInventoryWarehouse();

        List<SheetBean> sheetList = new ArrayList<>();
        sheetList.add(new SheetBean("盘点商品", 0, ExportInventoryCheck.class, exportInventoryList));
        sheetList.add(new SheetBean("仓库库区", 1, ExportInventoryWarehouse.class, exportWarehouseList));
        ExcelUtil.browserWriteExcel(response, "库存盘点", sheetList);
    }

    private List<ExportInventoryCheck> initExportInventoryCheck() {
        // 获取所有商品
        List<Product> productList = productService.list(new LambdaQueryWrapper<Product>()
                .select(Product::getId, Product::getName)
                .eq(Product::getStatus, StatusEnum.NORMAL.getCode()));
        if (CollectionUtils.isEmpty(productList)) {
            return new ArrayList<>();
        }
        // 获取所有SKU
        List<ProductPrice> priceList = productPriceService.list(new LambdaQueryWrapper<ProductPrice>()
                .select(ProductPrice::getId, ProductPrice::getSkuName, ProductPrice::getProductId)
                .eq(ProductPrice::getStatus, StatusEnum.NORMAL.getCode()));
        Map<Long, List<ProductPrice>> priceMap = new HashMap<>();
        if (CollectionUtils.isNotEmpty(priceList)) {
            priceMap = priceList.stream().collect(Collectors.groupingBy(ProductPrice::getProductId));
        }
        List<ExportInventoryCheck> exportList = new ArrayList<>();
        for (Product item : productList) {
            List<ProductPrice> pList = priceMap.get(item.getId());
            if (CollectionUtils.isEmpty(pList)) {
                continue;
            }
            for (ProductPrice priceItem : pList) {
                ExportInventoryCheck exportItem = new ExportInventoryCheck();
                exportItem.setName(item.getName())
                        .setSkuName(priceItem.getSkuName())
                        .setQty("0");

                exportList.add(exportItem);
            }
        }
        return exportList;
    }

    private List<ExportInventoryWarehouse> initExportInventoryWarehouse() {
        List<Warehouse> warehouseList = warehouseService.list();
        if (CollectionUtils.isEmpty(warehouseList)) {
            return new ArrayList<>();
        }
        List<WarehouseArea> warehouseAreaList = warehouseAreaService.list();
        Map<Long, List<WarehouseArea>> warehouseAreaMap = new HashMap<>();
        if (CollectionUtils.isNotEmpty(warehouseAreaList)) {
            warehouseAreaMap = warehouseAreaList.stream().collect(Collectors.groupingBy(WarehouseArea::getWarehouseId));
        }

        List<ExportInventoryWarehouse> exportList = new ArrayList<>();
        for (Warehouse item : warehouseList) {
            List<WarehouseArea> areaList = warehouseAreaMap.get(item.getId());
            if (CollectionUtils.isEmpty(areaList)) {
                continue;
            }
            for (WarehouseArea areaItem : areaList) {
                ExportInventoryWarehouse exportItem = new ExportInventoryWarehouse();
                exportItem.setWarehouseName(item.getName())
                        .setWarehouseAreaName(areaItem.getName());

                exportList.add(exportItem);
            }
        }
        return exportList;
    }
}
