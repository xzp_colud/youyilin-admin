package com.youyilin.report.logic;

import com.youyilin.report.model.inventory.query.ExportInventoryQuery;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface ExportInventoryLogic {

    /**
     * 库存信息导出
     */
    void exportInventoryExcel(HttpServletResponse response, ExportInventoryQuery query) throws IOException;

    /**
     * 盘点库存导出
     */
    void exportCheckExcel(HttpServletResponse response) throws IOException;
}
