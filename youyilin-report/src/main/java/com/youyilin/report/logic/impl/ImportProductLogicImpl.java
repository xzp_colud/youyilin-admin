package com.youyilin.report.logic.impl;

import com.alibaba.fastjson.JSON;
import com.youyilin.common.bean.R;
import com.youyilin.common.config.qiniu.QiNiuConfig;
import com.youyilin.common.config.qiniu.QiNiuRunnable;
import com.youyilin.common.config.qiniu.QiNiuService;
import com.youyilin.common.exception.ApiException;
import com.youyilin.common.threadPool.GlobalThreadPool;
import com.youyilin.goods.entity.*;
import com.youyilin.goods.service.*;
import com.youyilin.report.logic.ImportProductLogic;
import com.youyilin.report.model.SheetBean;
import com.youyilin.report.model.product.*;
import com.youyilin.report.service.*;
import com.youyilin.report.utils.ExcelUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ImportProductLogicImpl implements ImportProductLogic {

    private final QiNiuConfig qiNiuConfig;
    private final ImportProductService importProductService;
    private final ImportProductPlanService importProductPlanService;
    private final ImportProductFabricService importProductFabricService;
    private final ImportProductIngredientService importProductIngredientService;
    private final ImportProductProcessFeeService importProductProcessFeeService;
    private final ImportProductSubMaterialService importProductSubMaterialService;
    private final ImportProductTechnologyService importProductTechnologyService;
    private final ProductService productService;
    private final SupplierService supplierService;
    private final CategoryService categoryService;
    private final ProductParamsService productParamsService;
    private final ProductPriceService productPriceService;
    private final ParamsService paramsService;
    private final ImportProductParamsService importProductParamsService;

    public ImportProductLogicImpl(QiNiuConfig qiNiuConfig, ImportProductService importProductService, ImportProductPlanService importProductPlanService,
                                  ImportProductFabricService importProductFabricService, ImportProductIngredientService importProductIngredientService,
                                  ImportProductProcessFeeService importProductProcessFeeService, ImportProductSubMaterialService importProductSubMaterialService,
                                  ImportProductTechnologyService importProductTechnologyService, ProductService productService,
                                  SupplierService supplierService, CategoryService categoryService, ProductParamsService productParamsService,
                                  ProductPriceService productPriceService, ParamsService paramsService, ImportProductParamsService importProductParamsService) {
        this.qiNiuConfig = qiNiuConfig;
        this.importProductService = importProductService;
        this.importProductPlanService = importProductPlanService;
        this.importProductFabricService = importProductFabricService;
        this.importProductIngredientService = importProductIngredientService;
        this.importProductProcessFeeService = importProductProcessFeeService;
        this.importProductSubMaterialService = importProductSubMaterialService;
        this.importProductTechnologyService = importProductTechnologyService;
        this.productService = productService;
        this.supplierService = supplierService;
        this.categoryService = categoryService;
        this.productParamsService = productParamsService;
        this.productPriceService = productPriceService;
        this.paramsService = paramsService;
        this.importProductParamsService = importProductParamsService;
    }

    private static final BigDecimal hundred = new BigDecimal("100");

    @Override
    @Transactional
    public void importExcel(HttpServletResponse response, MultipartFile file) throws IOException {
        this.doImport(response, file, false);
    }

    @Override
    @Transactional
    public void importExcelUpdate(HttpServletResponse response, MultipartFile file) throws IOException {
        this.doImport(response, file, true);
    }

    @Override
    @Transactional
    public void importParams(HttpServletResponse response, MultipartFile file) throws IOException {
        // 产品-属性
        List<ImportProductParams> productParamsList = ExcelUtil.browserReadExcelAsync(file, ImportProductParams.class, 0);
        boolean isErrorFirst = this.doReadProductParamsAsyncAfter(productParamsList);
        boolean isErrorSecond = this.doSaveProductParamsValidate(productParamsList);
        if (isErrorFirst || isErrorSecond) {
            this.doImportParamsResponseError(response, productParamsList);
            return;
        }
        // 删除
        List<String> productIds = new ArrayList<>();
        productParamsList.forEach(item -> productIds.add(item.getProductId()));
        importProductParamsService.delByProductIds(productIds);
        // 保存
        importProductParamsService.saveBatch(productParamsList);
        // 返回
        this.doResponseSuccess(response);
    }

    @Override
    public void importProductStatus(HttpServletResponse response, MultipartFile file) throws IOException {
        List<ImportProductStatus> productStatusList = ExcelUtil.browserReadExcelAsync(file, ImportProductStatus.class, 0);
        boolean isErrorFirst = this.doReadProductStatusAsyncAfter(productStatusList);
        boolean isErrorSecond = this.doSaveProductStatusValidate(productStatusList);
        if (isErrorFirst || isErrorSecond) {
            this.doImportStatusResponseError(response, productStatusList);
            return;
        }
        this.doSaveProductStatus(productStatusList);
        this.doResponseSuccess(response);
    }

    /**
     * 执行导入
     *
     * @param response 响应体
     * @param file     文件
     * @param isUpdate 是否是更新操作
     */
    private void doImport(HttpServletResponse response, MultipartFile file, Boolean isUpdate) throws IOException {
        // 产品
        List<ImportProduct> productList = ExcelUtil.browserReadExcelAsync(file, ImportProduct.class, 0);
        // 产品-方案
        List<ImportProductPlan> planList = ExcelUtil.browserReadExcelAsync(file, ImportProductPlan.class, 1);
        // 产品-面料
        List<ImportProductFabric> fabricList = ExcelUtil.browserReadExcelAsync(file, ImportProductFabric.class, 2);
        // 产品-配料
        List<ImportProductIngredient> ingredientList = ExcelUtil.browserReadExcelAsync(file, ImportProductIngredient.class, 3);
        // 产品-辅料
        List<ImportProductSubMaterial> subMaterialList = ExcelUtil.browserReadExcelAsync(file, ImportProductSubMaterial.class, 4);
        // 产品-工艺
        List<ImportProductTechnology> technologyList = ExcelUtil.browserReadExcelAsync(file, ImportProductTechnology.class, 5);
        // 产品-加工费
        List<ImportProductProcessFee> processFeeList = ExcelUtil.browserReadExcelAsync(file, ImportProductProcessFee.class, 6);

        // 第一次匹配
        boolean isErrorFirst = this.firstValidate(productList, planList, fabricList, ingredientList, subMaterialList, technologyList, processFeeList);
        if (isErrorFirst) {
            this.doResponseError(response, productList, planList, fabricList, ingredientList, subMaterialList, technologyList, processFeeList);
            return;
        }

        if (isUpdate) {
            // 第二次验证产品是否存在
            boolean isErrorThird = this.thirdUpdateValidate(productList);
            if (isErrorThird) {
                this.doResponseError(response, productList, planList, fabricList, ingredientList, subMaterialList, technologyList, processFeeList);
                return;
            }
        }

        // 第三次带查询匹配
        boolean isErrorSecond = this.secondValidate(productList, planList, fabricList, ingredientList, subMaterialList, technologyList, processFeeList);
        if (isErrorSecond) {
            this.doResponseError(response, productList, planList, fabricList, ingredientList, subMaterialList, technologyList, processFeeList);
            return;
        }

        if (isUpdate) {
            // 编辑
            this.doUpdate(productList, planList, fabricList, ingredientList, subMaterialList, technologyList, processFeeList);
        } else {
            // 新增
            this.doSave(productList, planList, fabricList, ingredientList, subMaterialList, technologyList, processFeeList);
        }
        this.doResponseSuccess(response);
    }

    /**
     * Excel读取完毕验证
     *
     * @param productList     产品
     * @param planList        方案
     * @param fabricList      面料
     * @param ingredientList  配料
     * @param subMaterialList 辅料
     * @param technologyList  工艺
     * @param processFeeList  加工费
     */
    private boolean firstValidate(List<ImportProduct> productList, List<ImportProductPlan> planList, List<ImportProductFabric> fabricList,
                                  List<ImportProductIngredient> ingredientList, List<ImportProductSubMaterial> subMaterialList,
                                  List<ImportProductTechnology> technologyList, List<ImportProductProcessFee> processFeeList) {
        boolean isErrorProduct = this.doReadProductAsyncAfter(productList);
        List<String> productUUidList = this.getProductUUid(productList);
        boolean isErrorPlan = this.doReadProductPlanAsyncAfter(planList, productUUidList);
        List<String> planUUidList = this.getProductPlanUUid(planList);
        boolean isErrorFabric = this.doReadProductFabricAsyncAfter(fabricList, planUUidList);
        boolean isErrorIngredient = this.doReadProductIngredientAsyncAfter(ingredientList, planUUidList);
        boolean isErrorSubMaterial = this.doReadProductSubMaterialAsyncAfter(subMaterialList, planUUidList);
        boolean isErrorTechnology = this.doReadProductTechnologyAsyncAfter(technologyList, planUUidList);
        boolean isErrorProcessFee = this.doReadProductProcessFeeAsyncAfter(processFeeList, planUUidList);

        return isErrorProduct || isErrorPlan || isErrorFabric || isErrorIngredient || isErrorSubMaterial || isErrorTechnology || isErrorProcessFee;
    }

    /**
     * Dao之前验证
     *
     * @param productList     产品
     * @param planList        方案
     * @param fabricList      面料
     * @param ingredientList  配料
     * @param subMaterialList 辅料
     * @param technologyList  工艺
     * @param processFeeList  加工费
     */
    private boolean secondValidate(List<ImportProduct> productList, List<ImportProductPlan> planList, List<ImportProductFabric> fabricList,
                                   List<ImportProductIngredient> ingredientList, List<ImportProductSubMaterial> subMaterialList,
                                   List<ImportProductTechnology> technologyList, List<ImportProductProcessFee> processFeeList) {

        boolean isErrorProduct = this.doSaveProductValidate(productList);
        boolean isErrorPlan = this.doSaveProductPlanValidate(planList);
        boolean isErrorFabric = this.doSaveProductFabricValidate(fabricList);
        boolean isErrorIngredient = this.doSaveProductIngredientValidate(ingredientList);
        boolean isErrorSubMaterial = this.doSaveProductSubMaterialValidate(subMaterialList);
        boolean isErrorTechnology = this.doSaveProductTechnologyValidate(technologyList);
        boolean isErrorProcessFee = this.doSaveProductProcessFeeValidate(processFeeList);
        return isErrorProduct || isErrorPlan || isErrorFabric || isErrorIngredient || isErrorSubMaterial || isErrorTechnology || isErrorProcessFee;
    }

    /**
     * 更新验证
     *
     * @param productList 产品
     */
    private boolean thirdUpdateValidate(List<ImportProduct> productList) {
        List<String> productUUid = this.getProductUUid(productList);
        if (CollectionUtils.isEmpty(productList)) return true;

        List<Product> existList = productService.listByIds(productUUid);
        if (CollectionUtils.isEmpty(existList)) return false;

        boolean isError = false;
        Map<String, List<Product>> productMap = existList.stream().collect(Collectors.groupingBy(Product::getIdString));
        for (ImportProduct item : productList) {
            List<Product> product = productMap.get(item.getUuid());
            String errorMsg = StringUtils.isBlank(item.getErrorMsg()) ? "" : item.getErrorMsg();
            if (CollectionUtils.isEmpty(product)) {
                errorMsg += "产品信息不存在;";
            }
            if (StringUtils.isBlank(errorMsg)) {
                item.setId(product.get(0).getId());
                continue;
            }
            item.setErrorMsg(errorMsg);
            isError = true;
        }

        return isError;
    }

    /**
     * Dao保存
     *
     * @param productList     产品
     * @param planList        方案
     * @param fabricList      面料
     * @param ingredientList  配料
     * @param subMaterialList 辅料
     * @param technologyList  工艺
     * @param processFeeList  加工费
     */
    private void doSave(List<ImportProduct> productList, List<ImportProductPlan> planList, List<ImportProductFabric> fabricList, List<ImportProductIngredient> ingredientList, List<ImportProductSubMaterial> subMaterialList, List<ImportProductTechnology> technologyList, List<ImportProductProcessFee> processFeeList) {
        Map<String, Long> productMap = this.doSaveProduct(productList);
        List<Long> productIds = this.getProductIds(productList);
        this.doSaveOrUpdateProductAfter(productIds, productMap, planList, fabricList, ingredientList, subMaterialList, technologyList, processFeeList);
    }

    /**
     * Dao更新
     *
     * @param productList     产品
     * @param planList        方案
     * @param fabricList      面料
     * @param ingredientList  配料
     * @param subMaterialList 辅料
     * @param technologyList  工艺
     * @param processFeeList  加工费
     */
    private void doUpdate(List<ImportProduct> productList, List<ImportProductPlan> planList, List<ImportProductFabric> fabricList, List<ImportProductIngredient> ingredientList, List<ImportProductSubMaterial> subMaterialList, List<ImportProductTechnology> technologyList, List<ImportProductProcessFee> processFeeList) {
        Map<String, Long> productMap = this.doUpdateProduct(productList);
        List<Long> productIds = this.getProductIds(productList);
        importProductPlanService.delByProductIds(productIds);
        importProductFabricService.delByProductIds(productIds);
        importProductIngredientService.delByProductIds(productIds);
        importProductSubMaterialService.delByProductIds(productIds);
        importProductTechnologyService.delByProductIds(productIds);
        importProductProcessFeeService.delByProductIds(productIds);
        this.doSaveOrUpdateProductAfter(productIds, productMap, planList, fabricList, ingredientList, subMaterialList, technologyList, processFeeList);
    }

    private void doSaveOrUpdateProductAfter(List<Long> productIds, Map<String, Long> productMap, List<ImportProductPlan> planList, List<ImportProductFabric> fabricList, List<ImportProductIngredient> ingredientList, List<ImportProductSubMaterial> subMaterialList, List<ImportProductTechnology> technologyList, List<ImportProductProcessFee> processFeeList) {
        Map<String, ImportProductPlan> planMap = this.doSaveProductPlan(planList, productMap);
        this.doUpdateProductPricePlan(productIds, planMap);
        this.doSaveProductFabric(fabricList, planMap);
        this.doSaveProductIngredient(ingredientList, planMap);
        this.doSaveProductSubMaterial(subMaterialList, planMap);
        this.doSaveProductTechnology(technologyList, planMap);
        this.doSaveProductProcessFee(processFeeList, planMap);
    }

    /**
     * 返回成功
     */
    private void doResponseSuccess(HttpServletResponse response) throws IOException {
        response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");
        response.getWriter().println(JSON.toJSONString(R.success("导入成功")));
    }

    /**
     * 返回失败
     */
    private void doResponseError(HttpServletResponse response, List<ImportProduct> productList, List<ImportProductPlan> planList,
                                 List<ImportProductFabric> fabricList, List<ImportProductIngredient> ingredientList,
                                 List<ImportProductSubMaterial> subMaterialList, List<ImportProductTechnology> technologyList,
                                 List<ImportProductProcessFee> processFeeList) throws IOException {
        List<SheetBean> sheetList = new ArrayList<>();
        sheetList.add(new SheetBean("产品", 0, ImportProduct.class, productList));
        sheetList.add(new SheetBean("方案", 1, ImportProductPlan.class, planList));
        sheetList.add(new SheetBean("面料", 2, ImportProductFabric.class, fabricList));
        sheetList.add(new SheetBean("配料", 3, ImportProductIngredient.class, ingredientList));
        sheetList.add(new SheetBean("辅料", 4, ImportProductSubMaterial.class, subMaterialList));
        sheetList.add(new SheetBean("工艺", 5, ImportProductTechnology.class, technologyList));
        sheetList.add(new SheetBean("加工费", 6, ImportProductProcessFee.class, processFeeList));
        ExcelUtil.browserWriteExcel(response, "错误信息", sheetList);
    }

    /**
     * 返回失败
     */
    private void doImportParamsResponseError(HttpServletResponse response, List<ImportProductParams> paramsList) throws IOException {
        List<SheetBean> sheetList = new ArrayList<>();
        sheetList.add(new SheetBean("产品属性", 0, ImportProductParams.class, paramsList));
        ExcelUtil.browserWriteExcel(response, "产品属性-错误信息", sheetList);
    }

    /**
     * 返回失败
     */
    private void doImportStatusResponseError(HttpServletResponse response, List<ImportProductStatus> statusList) throws IOException {
        List<SheetBean> sheetList = new ArrayList<>();
        sheetList.add(new SheetBean("产品状态", 0, ImportProductStatus.class, statusList));
        ExcelUtil.browserWriteExcel(response, "产品状态-错误信息", sheetList);
    }

    /**
     * 读取产品
     */
    private boolean doReadProductAsyncAfter(List<ImportProduct> list) {
        boolean isError = false;
        List<String> uuidList = new ArrayList<>();
        for (ImportProduct item : list) {
            String errorMsg = "";
            if (StringUtils.isBlank(item.getUuid())) {
                errorMsg += "唯一ID不能为空;";
            } else {
                String uuid = item.getUuid().trim();
                if (uuidList.contains(uuid)) {
                    errorMsg += "唯一ID存在重复;";
                } else {
                    uuidList.add(uuid);
                }
            }
            if (StringUtils.isBlank(item.getName())) {
                errorMsg += "型号不能为空;";
            }
            if (StringUtils.isBlank(item.getSupplierName())) {
                errorMsg += "供货商不能为空;";
            }
            if (StringUtils.isBlank(item.getCategoryName())) {
                errorMsg += "商品分类不能为空;";
            }
            item.setErrorMsg(errorMsg);
            if (StringUtils.isNotBlank(errorMsg) && !isError) {
                isError = true;
            }
        }
        return isError;
    }

    /**
     * 读取产品-方案
     */
    private boolean doReadProductPlanAsyncAfter(List<ImportProductPlan> list, List<String> productUUidList) {
        boolean isError = false;
        for (ImportProductPlan item : list) {
            String errorMsg = "";
            if (StringUtils.isBlank(item.getSuuid())) {
                errorMsg += "产品ID不能为空;";
            } else {
                if (!productUUidList.contains(item.getSuuid())) {
                    errorMsg += "产品ID不匹配;";
                }
            }
            if (StringUtils.isBlank(item.getUuid())) {
                errorMsg += "唯一ID不能为空;";
            }
            if (StringUtils.isBlank(item.getTitle())) {
                errorMsg += "方案名称不能为空;";
            }

            item.setErrorMsg(errorMsg);
            if (StringUtils.isNotBlank(errorMsg) && !isError) {
                isError = true;
            }
        }
        return isError;
    }

    /**
     * 读取产品-面料
     */
    private boolean doReadProductFabricAsyncAfter(List<ImportProductFabric> list, List<String> uuidList) {
        boolean isError = false;
        for (ImportProductFabric item : list) {
            String errorMsg = "";
            if (StringUtils.isBlank(item.getUuid())) {
                errorMsg += "方案ID不能为空;";
            } else {
                if (!uuidList.contains(item.getUuid())) {
                    errorMsg += "方案ID不匹配;";
                }
            }
            if (StringUtils.isBlank(item.getProductName())) {
                errorMsg += "型号不能为空;";
            }
//            if (StringUtils.isBlank(item.getSupplierName())) {
//                errorMsg += "供货商不能为空;";
//            }
            if (StringUtils.isBlank(item.getOneUsageString())) {
                errorMsg += "单价用量不能为空;";
            } else {
                try {
                    BigDecimal oneUsage = this.getValScale(item.getOneUsageString());
                    if (oneUsage.compareTo(BigDecimal.ZERO) < 0) {
                        errorMsg += "单件用量数值异常;";
                    }
                    item.setOneUsage(oneUsage);
                } catch (Exception e) {
                    errorMsg += "单价用量类型异常;";
                }
            }

            if (StringUtils.isBlank(item.getAmountString())) {
//                errorMsg += "金额不能为空;";
            } else {
                try {
                    BigDecimal amount = this.getValScale(item.getAmountString());
                    if (amount.compareTo(BigDecimal.ZERO) < 0) {
                        errorMsg += "金额数值异常;";
                    }
                    item.setAmount(amount.multiply(hundred).longValue());
                } catch (Exception e) {
                    errorMsg += "金额类型异常;";
                }
            }

            if (StringUtils.isBlank(item.getTotalAmountString())) {
//                errorMsg += "总金额不能为空;";
            } else {
                try {
                    BigDecimal totalAmount = this.getValScale(item.getTotalAmountString());
                    if (totalAmount.compareTo(BigDecimal.ZERO) < 0) {
                        errorMsg += "总金额数值异常;";
                    }
                    item.setTotalAmount(totalAmount.multiply(hundred).longValue());
                } catch (Exception e) {
                    errorMsg += "总金额类型异常;";
                }
            }
            item.setErrorMsg(errorMsg);
            if (StringUtils.isNotBlank(errorMsg) && !isError) {
                isError = true;
            }
        }
        return isError;
    }

    /**
     * 读取产品-配料
     */
    private boolean doReadProductIngredientAsyncAfter(List<ImportProductIngredient> list, List<String> uuidList) {
        boolean isError = false;
        for (ImportProductIngredient item : list) {
            String errorMsg = StringUtils.isBlank(item.getErrorMsg()) ? "" : item.getErrorMsg();
            if (StringUtils.isBlank(item.getUuid())) {
                errorMsg += "方案ID不能为空;";
            } else {
                if (!uuidList.contains(item.getUuid())) {
                    errorMsg += "方案ID不匹配;";
                }
            }
            if (StringUtils.isBlank(item.getProductName())) {
                errorMsg += "型号不能为空;";
            }
//            if (StringUtils.isBlank(item.getSupplierName())) {
//                errorMsg += "供货商不能为空;";
//            }
            if (StringUtils.isBlank(item.getOneUsageString())) {
                errorMsg += "单价用量不能为空;";
            } else {
                try {
                    BigDecimal oneUsage = this.getValScale(item.getOneUsageString());
                    if (oneUsage.compareTo(BigDecimal.ZERO) < 0) {
                        errorMsg += "单件用量数值异常;";
                    }
                    item.setOneUsage(oneUsage);
                } catch (Exception e) {
                    errorMsg += "单价用量类型异常;";
                }
            }

            if (StringUtils.isBlank(item.getAmountString())) {
//                errorMsg += "金额不能为空;";
            } else {
                try {
                    BigDecimal amount = this.getValScale(item.getAmountString());
                    if (amount.compareTo(BigDecimal.ZERO) < 0) {
                        errorMsg += "金额数值异常;";
                    }
                    item.setAmount(amount.multiply(hundred).longValue());
                } catch (Exception e) {
                    errorMsg += "金额类型异常;";
                }
            }

            if (StringUtils.isBlank(item.getTotalAmountString())) {
//                errorMsg += "总金额不能为空;";
            } else {
                try {
                    BigDecimal totalAmount = this.getValScale(item.getTotalAmountString());
                    if (totalAmount.compareTo(BigDecimal.ZERO) < 0) {
                        errorMsg += "总金额数值异常;";
                    }
                    item.setTotalAmount(totalAmount.multiply(hundred).longValue());
                } catch (Exception e) {
                    errorMsg += "总金额类型异常;";
                }
            }
            item.setErrorMsg(errorMsg);
            if (StringUtils.isNotBlank(errorMsg) && !isError) {
                isError = true;
            }
        }
        return isError;
    }

    /**
     * 读取产品-辅料
     */
    private boolean doReadProductSubMaterialAsyncAfter(List<ImportProductSubMaterial> list, List<String> uuidList) {
        boolean isError = false;
        for (ImportProductSubMaterial item : list) {
            String errorMsg = StringUtils.isBlank(item.getErrorMsg()) ? "" : item.getErrorMsg();
            if (StringUtils.isBlank(item.getUuid())) {
                errorMsg += "方案ID不能为空;";
            } else {
                if (!uuidList.contains(item.getUuid())) {
                    errorMsg += "方案ID不匹配;";
                }
            }
            if (StringUtils.isBlank(item.getProductName())) {
                errorMsg += "种类不能为空;";
            }
            if (StringUtils.isBlank(item.getUsedString())) {
                errorMsg += "单价用量不能为空;";
            } else {
                try {
                    BigDecimal used = this.getValScale(item.getUsedString());
                    if (used.compareTo(BigDecimal.ZERO) < 0) {
                        errorMsg += "用量数值异常;";
                    }
                    item.setUsed(used);
                } catch (Exception e) {
                    errorMsg += "用量类型异常;";
                }
            }

            if (StringUtils.isBlank(item.getAmountString())) {
//                errorMsg += "金额不能为空;";
            } else {
                try {
                    BigDecimal amount = this.getValScale(item.getAmountString());
                    if (amount.compareTo(BigDecimal.ZERO) < 0) {
                        errorMsg += "金额数值异常;";
                    }
                    item.setAmount(amount.multiply(hundred).longValue());
                } catch (Exception e) {
                    errorMsg += "金额类型异常;";
                }
            }

            if (StringUtils.isBlank(item.getTotalAmountString())) {
//                errorMsg += "总金额不能为空;";
            } else {
                try {
                    BigDecimal totalAmount = this.getValScale(item.getTotalAmountString());
                    if (totalAmount.compareTo(BigDecimal.ZERO) < 0) {
                        errorMsg += "总金额数值异常;";
                    }
                    item.setTotalAmount(totalAmount.multiply(hundred).longValue());
                } catch (Exception e) {
                    errorMsg += "总金额类型异常;";
                }
            }
            item.setErrorMsg(errorMsg);
            if (StringUtils.isNotBlank(errorMsg) && !isError) {
                isError = true;
            }
        }
        return isError;
    }

    /**
     * 读取产品-工艺
     */
    private boolean doReadProductTechnologyAsyncAfter(List<ImportProductTechnology> list, List<String> uuidList) {
        boolean isError = false;
        for (ImportProductTechnology item : list) {
            String errorMsg = StringUtils.isBlank(item.getErrorMsg()) ? "" : item.getErrorMsg();
            if (StringUtils.isBlank(item.getUuid())) {
                errorMsg += "方案ID不能为空;";
            } else {
                if (!uuidList.contains(item.getUuid())) {
                    errorMsg += "方案ID不匹配;";
                }
            }
            if (StringUtils.isBlank(item.getSupplierName())) {
                errorMsg += "厂家不能为空;";
            }
            if (StringUtils.isBlank(item.getTechnologyName())) {
                errorMsg += "工艺不能为空;";
            }

            if (StringUtils.isBlank(item.getTotalAmountString())) {
//                errorMsg += "金额不能为空;";
            } else {
                try {
                    BigDecimal totalAmount = this.getValScale(item.getTotalAmountString());
                    if (totalAmount.compareTo(BigDecimal.ZERO) < 0) {
                        errorMsg += "金额数值异常;";
                    }
                    item.setTotalAmount(totalAmount.multiply(hundred).longValue());
                } catch (Exception e) {
                    errorMsg += "金额类型异常;";
                }
            }

            item.setErrorMsg(errorMsg);
            if (StringUtils.isNotBlank(errorMsg) && !isError) {
                isError = true;
            }
        }
        return isError;
    }

    /**
     * 读取产品-加工费
     */
    private boolean doReadProductProcessFeeAsyncAfter(List<ImportProductProcessFee> list, List<String> uuidList) {
        boolean isError = false;
        for (ImportProductProcessFee item : list) {
            String errorMsg = StringUtils.isBlank(item.getErrorMsg()) ? "" : item.getErrorMsg();
            if (StringUtils.isBlank(item.getUuid())) {
                errorMsg += "方案ID不能为空;";
            } else {
                if (!uuidList.contains(item.getUuid())) {
                    errorMsg += "方案ID不匹配;";
                }
            }

            if (StringUtils.isBlank(item.getNumString())) {
                errorMsg += "数量不能为空;";
            } else {
                try {
                    BigDecimal num = this.getValScale(item.getNumString());
                    if (num.compareTo(BigDecimal.ZERO) < 0) {
                        errorMsg += "数量数值异常;";
                    }
                    item.setNum(num);
                } catch (Exception e) {
                    errorMsg += "数量类型异常;";
                }
            }

            if (StringUtils.isBlank(item.getAmountString())) {
//                errorMsg += "金额不能为空;";
            } else {
                try {
                    BigDecimal amount = this.getValScale(item.getAmountString());
                    if (amount.compareTo(BigDecimal.ZERO) < 0) {
                        errorMsg += "金额数值异常;";
                    }
                    item.setAmount(amount.multiply(hundred).longValue());
                } catch (Exception e) {
                    errorMsg += "金额类型异常;";
                }
            }

            item.setErrorMsg(errorMsg);
            if (StringUtils.isNotBlank(errorMsg) && !isError) {
                isError = true;
            }
        }
        return isError;
    }

    /**
     * 获取当前产品的uuid
     */
    private List<String> getProductUUid(List<ImportProduct> list) {
        List<String> uuids = new ArrayList<>();
        for (ImportProduct item : list) {
            if (StringUtils.isNotBlank(item.getUuid())) {
                uuids.add(item.getUuid().trim());
            }
        }
        return uuids;
    }

    /**
     * 获取当前产品的uuid
     */
    private List<String> getProductPlanUUid(List<ImportProductPlan> list) {
        List<String> uuids = new ArrayList<>();
        for (ImportProductPlan item : list) {
            if (StringUtils.isNotBlank(item.getUuid())) {
                uuids.add(item.getUuid().trim());
            }
        }
        return uuids;
    }

    /**
     * 获取当前产品的id
     */
    private List<Long> getProductIds(List<ImportProduct> list) {
        List<Long> ids = new ArrayList<>();
        for (ImportProduct item : list) {
            if (item.getId() == null) {
                throw new ApiException("产品异常");
            }
            ids.add(item.getId());
        }
        return ids;
    }

    /**
     * 保存验证-产品
     */
    private boolean doSaveProductValidate(List<ImportProduct> list) {
        Set<String> supplierNames = new HashSet<>();
        Set<String> categoryNames = new HashSet<>();
        for (ImportProduct item : list) {
            if (StringUtils.isNotBlank(item.getSupplierName())) {
                supplierNames.add(item.getSupplierName());
            }
            if (StringUtils.isNotBlank(item.getCategoryName())) {
                categoryNames.add(item.getCategoryName());
            }
        }
        Map<String, List<Supplier>> supplierMap = this.getSupplierGroupByName(supplierNames);
        Map<String, List<Category>> categoryMap = this.getCategoryGroupByName(categoryNames);

        boolean isError = false;
        for (ImportProduct item : list) {
            String errorMsg = StringUtils.isBlank(item.getErrorMsg()) ? "" : item.getErrorMsg();
            List<Supplier> supplier = supplierMap.get(item.getSupplierName());
            List<Category> category = categoryMap.get(item.getCategoryName());
            if (CollectionUtils.isEmpty(supplier)) {
                errorMsg += "供货商不存在;";
            }
            if (CollectionUtils.isEmpty(category)) {
                errorMsg += "商品分类不存在;";
            }
            if (StringUtils.isBlank(errorMsg)) {
                item.setSupplierId(supplier.get(0).getId());
                item.setCategoryId(category.get(0).getId());
                continue;
            }

            item.setErrorMsg(errorMsg);
            if (!isError) {
                isError = true;
            }
        }

        return isError;
    }

    /**
     * 保存验证-产品-方案
     */
    private boolean doSaveProductPlanValidate(List<ImportProductPlan> list) {
        return false;
    }

    /**
     * 保存验证-产品-面料
     */
    private boolean doSaveProductFabricValidate(List<ImportProductFabric> list) {
        Set<String> productNames = new HashSet<>();
        for (ImportProductFabric item : list) {
            if (StringUtils.isNotBlank(item.getProductName())) {
                productNames.add(item.getProductName());
            }
        }
        Map<String, List<Product>> productMap = this.getProductGroupByName(productNames);

        boolean isError = false;
        for (ImportProductFabric item : list) {
            String errorMsg = StringUtils.isBlank(item.getErrorMsg()) ? "" : item.getErrorMsg();
            List<Product> product = productMap.get(item.getProductName());
            if (CollectionUtils.isEmpty(product)) {
                errorMsg += "面料型号不存在;";
            }
            if (StringUtils.isBlank(errorMsg)) {
                Product productItem = product.get(0);
                item.setProductId(productItem.getId());
                item.setSupplierId(productItem.getSupplierId());
                item.setSupplierName(productItem.getSupplierName());
                continue;
            }

            item.setErrorMsg(errorMsg);
            if (!isError) {
                isError = true;
            }
        }

        return isError;
    }

    /**
     * 保存验证-产品-配料
     */
    private boolean doSaveProductIngredientValidate(List<ImportProductIngredient> list) {
        Set<String> productNames = new HashSet<>();
        for (ImportProductIngredient item : list) {
            if (StringUtils.isNotBlank(item.getProductName())) {
                productNames.add(item.getProductName());
            }
        }
        Map<String, List<Product>> productMap = this.getProductGroupByName(productNames);

        boolean isError = false;
        for (ImportProductIngredient item : list) {
            String errorMsg = StringUtils.isBlank(item.getErrorMsg()) ? "" : item.getErrorMsg();
            List<Product> product = productMap.get(item.getProductName());
            if (CollectionUtils.isEmpty(product)) {
                errorMsg += "面料型号不存在;";
            }
            if (StringUtils.isBlank(errorMsg)) {
                Product productItem = product.get(0);
                item.setProductId(productItem.getId());
                item.setSupplierId(productItem.getSupplierId());
                item.setSupplierName(productItem.getSupplierName());
                continue;
            }

            item.setErrorMsg(errorMsg);
            if (!isError) {
                isError = true;
            }
        }

        return isError;
    }

    /**
     * 保存验证-产品-辅料
     */
    private boolean doSaveProductSubMaterialValidate(List<ImportProductSubMaterial> list) {
        Set<String> productNames = new HashSet<>();
        for (ImportProductSubMaterial item : list) {
            if (StringUtils.isNotBlank(item.getProductName())) {
                productNames.add(item.getProductName());
            }
        }
        Map<String, List<Product>> productMap = this.getProductGroupByName(productNames);

        boolean isError = false;
        for (ImportProductSubMaterial item : list) {
            String errorMsg = StringUtils.isBlank(item.getErrorMsg()) ? "" : item.getErrorMsg();
            List<Product> product = productMap.get(item.getProductName());
            if (CollectionUtils.isEmpty(product)) {
                errorMsg += "面料型号不存在;";
            }
            if (StringUtils.isBlank(errorMsg)) {
                item.setProductId(product.get(0).getId());
                continue;
            }

            item.setErrorMsg(errorMsg);
            if (!isError) {
                isError = true;
            }
        }

        return isError;
    }

    /**
     * 保存验证-产品-工艺
     */
    private boolean doSaveProductTechnologyValidate(List<ImportProductTechnology> list) {
        return false;
    }

    /**
     * 保存验证-产品-加工费
     */
    private boolean doSaveProductProcessFeeValidate(List<ImportProductProcessFee> list) {
        return false;
    }

    /**
     * 保存-产品
     */
    private Map<String, Long> doSaveProduct(List<ImportProduct> list) {
        Map<String, Long> productMap = new HashMap<>();

        importProductService.saveBatch(list);
        for (ImportProduct item : list) {
            Long id = item.getId();
            if (id == null) throw new ApiException("产品保存失败");

            productMap.put(item.getUuid(), id);
        }

        return productMap;
    }

    /**
     * 保存产品-方案
     */
    private Map<String, ImportProductPlan> doSaveProductPlan(List<ImportProductPlan> list, Map<String, Long> productMap) {
        list.forEach(item -> {
            Long productId = productMap.get(item.getSuuid());
            if (productId == null) throw new ApiException("方案保存失败");

            item.setProductId(productId);
        });

        importProductPlanService.saveBatch(list);

        Map<String, ImportProductPlan> planMap = new HashMap<>();
        for (ImportProductPlan item : list) {
            planMap.put(item.getUuid(), item);
        }

        return planMap;
    }

    /**
     * 保存-产品-面料
     */
    private void doSaveProductFabric(List<ImportProductFabric> list, Map<String, ImportProductPlan> planMap) {
        Set<Long> productIds = new HashSet<>();
        for (ImportProductFabric item : list) {
            ImportProductPlan plan = planMap.get(item.getUuid());
            if (plan == null || plan.getProductId() == null || plan.getId() == null) {
                throw new ApiException("面料保存失败");
            }

            item.setSourceProductId(plan.getProductId());
            item.setPlanId(plan.getId());
            productIds.add(item.getProductId());
        }
        // 重新赋值 门幅|成分
        Map<Long, List<ProductParams>> paramsMap = this.getProductParamsGroupByProductId(productIds);
        Map<Long, List<ProductPrice>> priceMap = this.getSkuGroupByProductId(productIds);
        for (ImportProductFabric item : list) {
            List<ProductParams> paramsList = paramsMap.get(item.getProductId());
            if (CollectionUtils.isNotEmpty(paramsList)) {
                String gateWidth = null;
                String makeup = null;
                for (ProductParams params : paramsList) {
                    if (StringUtils.isBlank(params.getName())) continue;

                    if (params.getName().equals("门幅")) {
                        gateWidth = params.getValue();
                    } else if (params.getName().equals("成分")) {
                        makeup = params.getValue();
                    }

                    if (gateWidth != null && makeup != null) {
                        break;
                    }
                }
                if (gateWidth != null && StringUtils.isBlank(item.getGateWidth())) {
                    item.setGateWidth(gateWidth);
                }
                if (makeup != null && StringUtils.isBlank(item.getMakeup())) {
                    item.setMakeup(makeup);
                }
            }

            List<ProductPrice> priceList = priceMap.get(item.getProductId());
            if (CollectionUtils.isNotEmpty(priceList)) {
                item.setProductPriceId(priceList.get(0).getId());
                if (item.getAmount() == null) {
                    item.setAmount(priceList.get(0).getCostPrice());
                }
            }
            if (item.getTotalAmount() == null && item.getAmount() != null && item.getOneUsage() != null) {
                item.setTotalAmount(item.getOneUsage().multiply(new BigDecimal(item.getAmount())).longValue());
            }
        }
        importProductFabricService.saveBatch(list);
    }

    /**
     * 保存-产品-配料
     */
    private void doSaveProductIngredient(List<ImportProductIngredient> list, Map<String, ImportProductPlan> planMap) {
        Set<Long> productIds = new HashSet<>();
        for (ImportProductIngredient item : list) {
            ImportProductPlan plan = planMap.get(item.getUuid());
            if (plan == null || plan.getProductId() == null || plan.getId() == null) {
                throw new ApiException("配料保存失败");
            }

            item.setSourceProductId(plan.getProductId());
            item.setPlanId(plan.getId());
            productIds.add(item.getProductId());
        }
        // 重新赋值 门幅|成分
        Map<Long, List<ProductParams>> paramsMap = this.getProductParamsGroupByProductId(productIds);
        Map<Long, List<ProductPrice>> priceMap = this.getSkuGroupByProductId(productIds);
        for (ImportProductIngredient item : list) {
            List<ProductParams> paramsList = paramsMap.get(item.getProductId());
            if (CollectionUtils.isNotEmpty(paramsList)) {
                String gateWidth = null;
                String makeup = null;
                for (ProductParams params : paramsList) {
                    if (StringUtils.isBlank(params.getName())) continue;

                    if (params.getName().equals("门幅")) {
                        gateWidth = params.getValue();
                    } else if (params.getName().equals("成分")) {
                        makeup = params.getValue();
                    }

                    if (gateWidth != null && makeup != null) {
                        break;
                    }
                }
                if (gateWidth != null && StringUtils.isBlank(item.getGateWidth())) {
                    item.setGateWidth(gateWidth);
                }
                if (makeup != null && StringUtils.isBlank(item.getMakeup())) {
                    item.setMakeup(makeup);
                }

                List<ProductPrice> priceList = priceMap.get(item.getProductId());
                if (CollectionUtils.isNotEmpty(priceList)) {
                    item.setProductPriceId(priceList.get(0).getId());
                    if (item.getAmount() == null) {
                        item.setAmount(priceList.get(0).getCostPrice());
                    }
                }
                if (item.getTotalAmount() == null && item.getAmount() != null && item.getOneUsage() != null) {
                    item.setTotalAmount(item.getOneUsage().multiply(new BigDecimal(item.getAmount())).longValue());
                }
            }
        }
        importProductIngredientService.saveBatch(list);
    }

    /**
     * 保存-产品-辅料
     */
    private void doSaveProductSubMaterial(List<ImportProductSubMaterial> list, Map<String, ImportProductPlan> planMap) {
        Set<Long> productIds = new HashSet<>();
        for (ImportProductSubMaterial item : list) {
            ImportProductPlan plan = planMap.get(item.getUuid());
            if (plan == null || plan.getProductId() == null || plan.getId() == null) {
                throw new ApiException("辅料保存失败");
            }

            item.setSourceProductId(plan.getProductId());
            item.setPlanId(plan.getId());
            productIds.add(item.getProductId());
        }
        // 重新赋值 颜色|尺寸
        Map<Long, List<ProductParams>> paramsMap = this.getProductParamsGroupByProductId(productIds);
        Map<Long, List<ProductPrice>> priceMap = this.getSkuGroupByProductId(productIds);
        for (ImportProductSubMaterial item : list) {
            List<ProductParams> paramsList = paramsMap.get(item.getProductId());
            if (CollectionUtils.isNotEmpty(paramsList)) {
                String color = null;
                String size = null;
                for (ProductParams params : paramsList) {
                    if (StringUtils.isBlank(params.getName())) continue;

                    if (params.getName().equals("色号")) {
                        color = params.getValue();
                    } else if (params.getName().equals("规格")) {
                        size = params.getValue();
                    }

                    if (color != null && size != null) {
                        break;
                    }
                }
                if (color != null && StringUtils.isBlank(item.getColor())) {
                    item.setColor(color);
                }
                if (size != null && StringUtils.isBlank(item.getSize())) {
                    item.setSize(size);
                }
            }

            List<ProductPrice> priceList = priceMap.get(item.getProductId());
            if (CollectionUtils.isNotEmpty(priceList)) {
                item.setProductPriceId(priceList.get(0).getId());
                if (item.getAmount() == null) {
                    item.setAmount(priceList.get(0).getCostPrice());
                }
            }
            if (item.getTotalAmount() == null && item.getAmount() != null && item.getUsed() != null) {
                item.setTotalAmount(item.getUsed().multiply(new BigDecimal(item.getAmount())).longValue());
            }
        }
        importProductSubMaterialService.saveBatch(list);
    }

    /**
     * 保存-产品-工艺
     */
    private void doSaveProductTechnology(List<ImportProductTechnology> list, Map<String, ImportProductPlan> planMap) {
        for (ImportProductTechnology item : list) {
            ImportProductPlan plan = planMap.get(item.getUuid());
            if (plan == null || plan.getProductId() == null || plan.getId() == null) {
                throw new ApiException("工艺保存失败");
            }

            item.setSourceProductId(plan.getProductId());
            item.setPlanId(plan.getId());
        }
        importProductTechnologyService.saveBatch(list);
    }

    /**
     * 保存-产品-加工费
     */
    private void doSaveProductProcessFee(List<ImportProductProcessFee> list, Map<String, ImportProductPlan> planMap) {
        for (ImportProductProcessFee item : list) {
            ImportProductPlan plan = planMap.get(item.getUuid());
            if (plan == null || plan.getProductId() == null || plan.getId() == null) {
                throw new ApiException("加工费保存失败");
            }

            item.setSourceProductId(plan.getProductId());
            item.setPlanId(plan.getId());
        }
        importProductProcessFeeService.saveBatch(list);
    }

    /**
     * 更新-产品
     */
    private Map<String, Long> doUpdateProduct(List<ImportProduct> list) {
        Map<String, Long> productMap = new HashMap<>();
        importProductService.saveOrUpdateBatch(list);
        for (ImportProduct item : list) {
            Long id = item.getId();
            if (id == null) throw new ApiException("产品保存失败");

            productMap.put(item.getUuid(), id);
        }
        return productMap;
    }

    /**
     * 更新产品-SKU-关联的方案
     */
    private void doUpdateProductPricePlan(List<Long> productIds, Map<String, ImportProductPlan> planMap) {
        List<ProductPrice> priceList = productPriceService.listByProductIds(productIds);
        if (CollectionUtils.isEmpty(priceList)) return;

        List<ProductPrice> updatePlanList = new ArrayList<>();
        for (ProductPrice item : priceList) {
            if (item.getPlanId() == null) continue;

            ImportProductPlan plan = planMap.get(item.getPlanId().toString());
            Long planId = plan == null ? null : plan.getId();

            ProductPrice price = new ProductPrice();
            price.setId(item.getId());
            price.setPlanId(planId);

            updatePlanList.add(price);
        }

        if (CollectionUtils.isNotEmpty(updatePlanList)) {
            productPriceService.updateBatchById(updatePlanList);
        }
    }

    /**
     * 按供货商名称分组
     */
    private Map<String, List<Supplier>> getSupplierGroupByName(Set<String> supplierNames) {
        if (CollectionUtils.isEmpty(supplierNames)) return new HashMap<>();

        List<Supplier> supplierList = supplierService.listIdsByNames(new ArrayList<>(supplierNames));
        if (CollectionUtils.isEmpty(supplierList)) return new HashMap<>();

        return supplierList.stream().collect(Collectors.groupingBy(Supplier::getName));
    }

    /**
     * 按商品分类名称分组
     */
    private Map<String, List<Category>> getCategoryGroupByName(Set<String> categoryNames) {
        if (CollectionUtils.isEmpty(categoryNames)) return new HashMap<>();

        List<Category> categoryList = categoryService.listIdsByTitles(new ArrayList<>(categoryNames));
        if (CollectionUtils.isEmpty(categoryList)) return new HashMap<>();

        return categoryList.stream().collect(Collectors.groupingBy(Category::getTitle));
    }

    /**
     * 按产品名称分组
     */
    private Map<String, List<Product>> getProductGroupByName(Set<String> productNames) {
        if (CollectionUtils.isEmpty(productNames)) return new HashMap<>();

        List<Product> productList = productService.listIdsByNames(new ArrayList<>(productNames));
        if (CollectionUtils.isEmpty(productList)) return new HashMap<>();

        return productList.stream().collect(Collectors.groupingBy(Product::getName));
    }

    /**
     * 按产品ID分组
     */
    private Map<String, List<Product>> getProductGroupById(Set<String> productIds) {
        if (CollectionUtils.isEmpty(productIds)) return new HashMap<>();

        List<Product> productList = productService.listByIds(new ArrayList<>(productIds));
        if (CollectionUtils.isEmpty(productList)) return new HashMap<>();

        return productList.stream().collect(Collectors.groupingBy(Product::getIdString));
    }

    /**
     * SKU按产品Id分组
     */
    private Map<Long, List<ProductPrice>> getSkuGroupByProductId(Set<Long> productIds) {
        if (CollectionUtils.isEmpty(productIds)) return new HashMap<>();

        List<ProductPrice> skuList = productPriceService.listByProductIds(new ArrayList<>(productIds));
        if (CollectionUtils.isEmpty(skuList)) return new HashMap<>();

        return skuList.stream().collect(Collectors.groupingBy(ProductPrice::getProductId));
    }

    /**
     * 按属性分组
     */
    private Map<Long, List<ProductParams>> getProductParamsGroupByProductId(Set<Long> productIds) {
        if (CollectionUtils.isEmpty(productIds)) return new HashMap<>();

        List<ProductParams> paramsList = productParamsService.listByProductIds(new ArrayList<>(productIds));
        if (CollectionUtils.isEmpty(paramsList)) return new HashMap<>();

        return paramsList.stream().collect(Collectors.groupingBy(ProductParams::getProductId));
    }

    /**
     * 按属性分组
     */
    private Map<String, List<Params>> getAllParamsById() {
        List<Params> paramsList = paramsService.list();
        if (CollectionUtils.isEmpty(paramsList)) return new HashMap<>();

        return paramsList.stream().collect(Collectors.groupingBy(Params::getIdString));
    }

    /**
     * 上传文件
     */
    private void uploadBeforeReadExcel(MultipartFile file) {
        try {
            byte[] bytes = file.getBytes();
            QiNiuService qiNiuService = new QiNiuService.Builder().config(qiNiuConfig).build();
            QiNiuRunnable qiNiuRunnable = new QiNiuRunnable(qiNiuService, bytes);
            GlobalThreadPool.runThread(qiNiuRunnable);
        } catch (IOException e) {
            log.error("【导入-产品-上传失败】", e);
        }
    }

    private BigDecimal getValScale(String val) {
        return new BigDecimal(val).setScale(2, RoundingMode.HALF_UP);
    }

    /**
     * 读取产品属性之后验证
     */
    private boolean doReadProductParamsAsyncAfter(List<ImportProductParams> productParamsList) {
        boolean isError = false;
        if (CollectionUtils.isEmpty(productParamsList)) return isError;

        for (ImportProductParams item : productParamsList) {
            String errorMsg = "";
            if (StringUtils.isBlank(item.getProductId())) {
                errorMsg += "产品ID不能为空;";
            }
            if (StringUtils.isBlank(item.getParamsId())) {
                errorMsg += "属性ID不能为空;";
            }
            item.setErrorMsg(errorMsg);
            if (StringUtils.isNotBlank(errorMsg) && !isError) {
                isError = true;
            }
        }

        return isError;
    }

    /**
     * 保存属性之前验证
     */
    private boolean doSaveProductParamsValidate(List<ImportProductParams> productParamsList) {
        if (CollectionUtils.isEmpty(productParamsList)) return false;

        boolean isError = false;
        Set<String> productIds = new HashSet<>();
        productParamsList.forEach(item -> productIds.add(item.getProductId()));

        Map<String, List<Product>> productMap = this.getProductGroupById(productIds);
        Map<String, List<Params>> paramsMap = this.getAllParamsById();

        for (ImportProductParams item : productParamsList) {
            String errorMsg = StringUtils.isBlank(item.getErrorMsg()) ? "" : item.getErrorMsg();
            List<Product> product = productMap.get(item.getProductId());
            List<Params> param = paramsMap.get(item.getParamsId());
            if (CollectionUtils.isEmpty(product)) {
                errorMsg += "产品不匹配;";
            }
            if (CollectionUtils.isEmpty(param)) {
                errorMsg += "属性不匹配;";
            }

            item.setErrorMsg(errorMsg);
            if (StringUtils.isNotBlank(errorMsg) && !isError) {
                isError = true;
            }
        }
        return isError;
    }

    /**
     * 读取产品之后验证
     */
    private boolean doReadProductStatusAsyncAfter(List<ImportProductStatus> productStatusList) {
        boolean isError = false;
        for (ImportProductStatus item : productStatusList) {
            String errorMsg = "";
            if (StringUtils.isBlank(item.getProductName())) {
                errorMsg += "型号不能为空;";
            }
            if (StringUtils.isBlank(item.getStatusString())) {
                errorMsg += "状态不能为空;";
            } else {
                try {
                    int status = Integer.parseInt(item.getStatusString());
                    if (status == 1 || status == 0) {
                        item.setStatus(status);
                    } else {
                        errorMsg += "状态值只能为1或0;";
                    }
                } catch (NumberFormatException e) {
                    errorMsg += "状态值异常只能为1或0;";
                }
            }

            item.setErrorMsg(errorMsg);
            if (StringUtils.isNotBlank(errorMsg) && !isError) {
                isError = true;
            }
        }

        return isError;
    }

    /**
     * 保存产品状态之前验证
     */
    private boolean doSaveProductStatusValidate(List<ImportProductStatus> productStatusList) {
        boolean isError = false;
        Set<String> productNames = new HashSet<>();
        productStatusList.forEach(item -> productNames.add(item.getProductName()));
        Map<String, List<Product>> productMap = this.getProductGroupByName(productNames);

        for (ImportProductStatus item : productStatusList) {
            String errorMsg = StringUtils.isBlank(item.getErrorMsg()) ? item.getErrorMsg() : "";
            List<Product> productList = productMap.get(item.getProductName());
            if (CollectionUtils.isEmpty(productList)) {
                errorMsg += "型号不匹配;";
            }
            if (productList.size() > 1) {
                errorMsg += "型号存在重复;";
            }
            Product product = productList.get(0);
            item.setId(product.getId());
            item.setErrorMsg(errorMsg);
            if (StringUtils.isNotBlank(errorMsg) && !isError) {
                isError = true;
            }
        }
        return isError;
    }

    /**
     * 保存产品状态
     */
    private void doSaveProductStatus(List<ImportProductStatus> productStatusList) {
        if (CollectionUtils.isEmpty(productStatusList)) return;

        List<Product> productList = productStatusList.stream().map(item -> {
            Product product = new Product();
            product.setId(item.getId());
            product.setStatus(item.getStatus());

            return product;
        }).collect(Collectors.toList());

        productService.updateBatchById(productList);
    }
}
