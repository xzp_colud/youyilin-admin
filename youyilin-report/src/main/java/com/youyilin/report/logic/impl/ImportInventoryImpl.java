package com.youyilin.report.logic.impl;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.youyilin.goods.entity.Product;
import com.youyilin.goods.entity.ProductPrice;
import com.youyilin.goods.service.ProductPriceService;
import com.youyilin.goods.service.ProductService;
import com.youyilin.report.logic.ImportInventoryLogic;
import com.youyilin.report.model.SheetBean;
import com.youyilin.report.model.inventory.ImportInventoryCheck;
import com.youyilin.report.utils.ExcelUtil;
import com.youyilin.warehouse.entity.Inventory;
import com.youyilin.warehouse.entity.InventoryLog;
import com.youyilin.warehouse.entity.Warehouse;
import com.youyilin.warehouse.entity.WarehouseArea;
import com.youyilin.warehouse.enums.InventoryLogTypeEnum;
import com.youyilin.warehouse.service.InventoryLogService;
import com.youyilin.warehouse.service.InventoryService;
import com.youyilin.warehouse.service.WarehouseAreaService;
import com.youyilin.warehouse.service.WarehouseService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ImportInventoryImpl implements ImportInventoryLogic {

    private final ProductService productService;
    private final ProductPriceService productPriceService;
    private final WarehouseService warehouseService;
    private final WarehouseAreaService warehouseAreaService;
    private final InventoryService inventoryService;
    private final InventoryLogService inventoryLogService;

    public ImportInventoryImpl(ProductService productService, ProductPriceService productPriceService,
                               WarehouseService warehouseService, WarehouseAreaService warehouseAreaService,
                               InventoryService inventoryService, InventoryLogService inventoryLogService) {
        this.productService = productService;
        this.productPriceService = productPriceService;
        this.warehouseService = warehouseService;
        this.warehouseAreaService = warehouseAreaService;
        this.inventoryService = inventoryService;
        this.inventoryLogService = inventoryLogService;
    }

    @Override
    public void importExcelCheck(HttpServletResponse response, MultipartFile file) throws IOException {
        // 产品
        List<ImportInventoryCheck> importList = ExcelUtil.browserReadExcelAsync(file, ImportInventoryCheck.class, 0);
        // 一次验证
        boolean isFirstError = this.firstValidParams(importList);
        if (isFirstError) {
            doResponseError(response, importList);
            return;
        }
        // 二次验证
        boolean isSecondError = this.secondValid(importList);
        if (isSecondError) {
            doResponseError(response, importList);
            return;
        }
        // 三保存
        this.doSaveOrUpdate(importList);
        // 返回成功
        ExcelUtil.doResponseSuccess(response);
    }

    private boolean firstValidParams(List<ImportInventoryCheck> importList) {
        boolean isError = false;
        for (ImportInventoryCheck item : importList) {
            String errorMsg = "";
            if (StringUtils.isBlank(item.getName())) {
                errorMsg += "商品型号不能为空;";
            }
            if (StringUtils.isBlank(item.getSkuName())) {
                errorMsg += "商品SKU不能为空;";
            }
            if (StringUtils.isBlank(item.getWarehouseName())) {
                errorMsg += "仓库不能为空;";
            }
            if (StringUtils.isBlank(item.getWarehouseAreaName())) {
                errorMsg += "库区不能为空;";
            }
            if (StringUtils.isBlank(item.getQty())) {
                errorMsg += "盘点数量不能为空;";
            } else {
                try {
                    BigDecimal amount = new BigDecimal(item.getQty());
                    if (amount.compareTo(BigDecimal.ZERO) <= 0) {
                        errorMsg += "盘点数量不能为空";
                    }
                    item.setCheckQty(amount);
                } catch (Exception e) {
                    if (StringUtils.isBlank(errorMsg)) {
                        errorMsg = "盘点数量格式不正确";
                    }
                }
            }
            if (StringUtils.isNotBlank(errorMsg) && !isError) {
                isError = true;
            }
        }

        return isError;
    }

    private boolean secondValid(List<ImportInventoryCheck> importList) {
        boolean isError = false;
        Set<String> productNames = new HashSet<>();
        Set<String> skuNames = new HashSet<>();
        Set<String> warehouseNames = new HashSet<>();
        Set<String> areaName = new HashSet<>();
        for (ImportInventoryCheck item : importList) {
            productNames.add(item.getName());
            skuNames.add(item.getSkuName());
            warehouseNames.add(item.getWarehouseName());
            areaName.add(item.getWarehouseAreaName());
        }
        // 获取 商品、SKU、仓库、库区
        Map<String, List<Product>> productMap = mapByProductName(productNames);
        Map<Long, List<ProductPrice>> priceMap = mapByProductId(skuNames);
        Map<String, List<Warehouse>> warehouseMap = mapByWarehouseName(warehouseNames);
        Map<Long, List<WarehouseArea>> areaMap = mapByWarehouseId(areaName);
        for (ImportInventoryCheck item : importList) {
            String errorMsg = "";
            List<Product> productList = productMap.get(item.getName());
            if (CollectionUtils.isEmpty(productList)) {
                errorMsg += "商品型号不存在;商品SKU不匹配";
            } else {
                Product product = productList.get(0);
                // 赋值 商品ID
                item.setProductId(product.getId());

                List<ProductPrice> priceList = priceMap.get(product.getId());
                if (CollectionUtils.isEmpty(priceList)) {
                    errorMsg += "商品SKU不存在;";
                } else {
                    boolean isNotExist = true;
                    for (ProductPrice price : priceList) {
                        if (price.getSkuName().equals(item.getSkuName())) {
                            isNotExist = false;
                            // 赋值 skuId
                            item.setSkuId(price.getId());
                            break;
                        }
                    }
                    if (isNotExist) {
                        errorMsg += "商品SKU不存在;";
                    }
                }
            }

            List<Warehouse> warehouseList = warehouseMap.get(item.getWarehouseName());
            if (CollectionUtils.isEmpty(warehouseList)) {
                errorMsg += "仓库不存在;";
            } else {
                Warehouse warehouse = warehouseList.get(0);
                // 赋值 仓库ID
                item.setWarehouseId(warehouse.getId());

                List<WarehouseArea> areaList = areaMap.get(warehouse.getId());
                if (CollectionUtils.isEmpty(areaList)) {
                    errorMsg += "库区不存在;";
                } else {
                    boolean isNotExist = true;
                    for (WarehouseArea area : areaList) {
                        if (area.getName().equals(item.getWarehouseAreaName())) {
                            isNotExist = false;
                            // 赋值 库区ID
                            item.setWarehouseAreaId(area.getId());
                            break;
                        }
                    }
                    if (isNotExist) {
                        errorMsg += "库区不存在;";
                    }
                }
            }

            if (StringUtils.isNotBlank(errorMsg) && !isError) {
                isError = true;
            }
        }
        return isError;
    }

    private Map<String, List<Product>> mapByProductName(Set<String> productNames) {
        List<Product> productList = productService.listIdsByNames(new ArrayList<>(productNames));
        if (CollectionUtils.isEmpty(productList)) {
            return new HashMap<>();
        }
        return productList.stream().collect(Collectors.groupingBy(Product::getName));
    }

    private Map<Long, List<ProductPrice>> mapByProductId(Set<String> skuNames) {
        List<ProductPrice> priceList = productPriceService.listIdsByNames(new ArrayList<>(skuNames));
        if (CollectionUtils.isEmpty(priceList)) {
            return new HashMap<>();
        }
        return priceList.stream().collect(Collectors.groupingBy(ProductPrice::getProductId));
    }

    private Map<String, List<Warehouse>> mapByWarehouseName(Set<String> warehouseNames) {
        List<Warehouse> warehouseList = warehouseService.listIdsByNames(new ArrayList<>(warehouseNames));
        if (CollectionUtils.isEmpty(warehouseList)) {
            return new HashMap<>();
        }
        return warehouseList.stream().collect(Collectors.groupingBy(Warehouse::getName));
    }

    private Map<Long, List<WarehouseArea>> mapByWarehouseId(Set<String> areaName) {
        List<WarehouseArea> warehouseAreaList = warehouseAreaService.listIdsByNames(new ArrayList<>(areaName));
        if (CollectionUtils.isEmpty(warehouseAreaList)) {
            return new HashMap<>();
        }
        return warehouseAreaList.stream().collect(Collectors.groupingBy(WarehouseArea::getWarehouseId));
    }

    private void doSaveOrUpdate(List<ImportInventoryCheck> importList) {
        // 拿出所有的SKU
        Set<Long> skuIds = new HashSet<>();
        for (ImportInventoryCheck item : importList) {
            skuIds.add(item.getSkuId());
        }
        List<Inventory> inventoryList = inventoryService.listBySkuIds(new ArrayList<>(skuIds));
        Map<Long, List<Inventory>> inventoryMap = new HashMap<>();
        if (CollectionUtils.isNotEmpty(inventoryList)) {
            inventoryMap = inventoryList.stream().collect(Collectors.groupingBy(Inventory::getSkuId));
        }
        List<Inventory> saveList = new ArrayList<>();
        List<Inventory> updateList = new ArrayList<>();
        List<InventoryLog> saveLogList = new ArrayList<>();
        for (ImportInventoryCheck item : importList) {
            Long productId = item.getProductId();
            Long skuId = item.getSkuId();
            Long warehouseId = item.getWarehouseId();
            Long warehouseAreaId = item.getWarehouseAreaId();
            BigDecimal qty = item.getCheckQty();
            List<Inventory> list = inventoryMap.get(skuId);
            if (CollectionUtils.isEmpty(list)) {
                // 创建需要保存的SKU
                saveList.add(this.createInventory(productId, skuId, warehouseId, warehouseAreaId, qty));
            } else {
                Inventory updateInventory = null;
                for (Inventory inventory : list) {
                    if (inventory.getWarehouseId().equals(warehouseId)
                            && inventory.getWarehouseAreaId().equals(warehouseAreaId)) {
                        updateInventory = inventory;
                        break;
                    }
                }
                if (updateInventory == null) {
                    // 创建需要保存的
                    saveList.add(this.createInventory(productId, skuId, warehouseId, warehouseAreaId, qty));
                } else {
                    BigDecimal dbQty = updateInventory.getQty() == null ? BigDecimal.ZERO : updateInventory.getQty();
                    BigDecimal upQty = dbQty.add(qty).setScale(2, RoundingMode.HALF_UP);

                    // 更新
                    Inventory updateItem = new Inventory();
                    updateItem.setId(updateInventory.getId())
                            .setQty(upQty);
                    updateList.add(updateItem);
                }
            }
            // TODO 盘点导入
//            InventoryLog log = inventoryLogService.createInventoryLog(warehouseId, warehouseAreaId, productId, skuId, null, qty, 0L);
//            log.setType(InventoryLogTypeEnum.CHECK_IN_IMPORT.getCode());
//            saveLogList.add(log);
        }
        // 持久化
        if (CollectionUtils.isNotEmpty(saveList)) {
            inventoryService.saveBatch(saveList);
        }
        if (CollectionUtils.isNotEmpty(updateList)) {
            inventoryService.updateBatchById(updateList);
        }
        inventoryLogService.saveBatch(saveLogList);
    }

    private Inventory createInventory(Long productId, Long skuId, Long warehouseId, Long warehouseAreaId, BigDecimal qty) {
        return new Inventory()
                .setId(IdWorker.getId())
                .setProductId(productId)
                .setSkuId(skuId)
                .setWarehouseId(warehouseId)
                .setWarehouseAreaId(warehouseAreaId)
                .setQty(qty);
    }

    /**
     * 返回失败
     */
    private void doResponseError(HttpServletResponse response, List<ImportInventoryCheck> errorList) throws IOException {
        List<SheetBean> sheetList = new ArrayList<>();
        sheetList.add(new SheetBean("盘点商品", 0, ImportInventoryCheck.class, errorList));
        ExcelUtil.browserWriteExcel(response, "盘点错误信息", sheetList);
    }
}
