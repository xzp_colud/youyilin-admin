//package com.youyilin.report.importListener;
//
//import com.alibaba.excel.context.AnalysisContext;
//import com.alibaba.excel.read.listener.ReadListener;
//import com.alibaba.excel.util.ListUtils;
//import com.alibaba.fastjson.JSON;
//import com.youyilin.report.model.ImportSysMenu;
//import com.youyilin.system.service.SysMenuService;
//import lombok.extern.slf4j.Slf4j;
//
//import java.util.List;
//
//@Slf4j
//public class ImportSysMenuListener implements ReadListener<ImportSysMenu> {
//
//    private static final int BATCH_COUNT = 100;
//    private List<ImportSysMenu> cachedDataList = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);
//
//    private final SysMenuService sysMenuService;
//
//    public ImportSysMenuListener(SysMenuService sysMenuService) {
//        this.sysMenuService = sysMenuService;
//    }
//
//    @Override
//    public void invoke(ImportSysMenu data, AnalysisContext context) {
//        log.info("解析到一条数据:{}", JSON.toJSONString(data));
//        cachedDataList.add(data);
//        // 达到BATCH_COUNT了，需要去存储一次数据库，防止数据几万条数据在内存，容易OOM
//        if (cachedDataList.size() >= BATCH_COUNT) {
//            saveData();
//            // 存储完成清理 list
//            cachedDataList = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);
//        }
//    }
//
//
//    @Override
//    public void doAfterAllAnalysed(AnalysisContext context) {
//        // 这里也要保存数据，确保最后遗留的数据也存储到数据库
//        saveData();
//        log.info("所有数据解析完成！");
//    }
//
//    /**
//     * 加上存储数据库
//     */
//    private void saveData() {
//        log.info("{}条数据，开始存储数据库！", cachedDataList.size());
////        demoDAO.save(cachedDataList);
//        log.info("存储数据库成功！");
//    }
//}
