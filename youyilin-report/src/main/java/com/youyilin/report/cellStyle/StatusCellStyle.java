package com.youyilin.report.cellStyle;

import com.alibaba.excel.write.metadata.style.WriteCellStyle;
import com.youyilin.common.enums.StatusEnum;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;

import java.util.Objects;

/**
 * 状态 单元格样式
 */
public class StatusCellStyle {

    // Excel 单元格样式
    public static void init(String info, WriteCellStyle cellStyle) {
        if (Objects.equals(info, StatusEnum.DISABLED.getInfo())) {
            cellStyle.setFillPatternType(FillPatternType.SOLID_FOREGROUND);
            cellStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
        }
    }
}
