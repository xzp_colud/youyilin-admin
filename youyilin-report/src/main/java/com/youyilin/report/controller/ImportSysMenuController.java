//package com.youyilin.report.controller;
//
//import com.youyilin.common.bean.R;
//import com.youyilin.common.config.email.EmailConfig;
//import com.youyilin.common.config.email.EmailService;
//import com.youyilin.common.config.qiniu.QiNiuConfig;
//import com.youyilin.common.config.qiniu.QiNiuRunnable;
//import com.youyilin.common.config.qiniu.QiNiuService;
//import com.youyilin.common.threadPool.GlobalThreadPool;
//import com.youyilin.report.model.ImportSysMenu;
//import com.youyilin.report.importListener.ImportSysMenuListener;
//import com.youyilin.report.utils.ExcelUtil;
//import com.youyilin.system.service.SysMenuService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.multipart.MultipartFile;
//
//import java.io.IOException;
//
///**
// * 导入 菜单
// */
//@RestController
//@RequestMapping("import.system")
//public class ImportSysMenuController {
//
//    @Autowired
//    private QiNiuConfig qiNiuConfig;
//    @Autowired
//    private EmailConfig emailConfig;
//
//    private final SysMenuService sysMenuService;
//
//    public ImportSysMenuController(SysMenuService sysMenuService) {
//        this.sysMenuService = sysMenuService;
//    }
//
//    //    @PreAuthorize("hasAuthority('import:system:menu')")
//    @PostMapping("menu")
//    public R<String> export(@RequestParam("file") MultipartFile file) throws IOException {
////        byte[] bytes = file.getBytes();
////        QiNiuService qiNiuService = new QiNiuService.Builder().config(qiNiuConfig).build();
////        QiNiuRunnable qiNiuRunnable = new QiNiuRunnable(qiNiuService, bytes);
////        GlobalThreadPool.runThread(qiNiuRunnable);
////        try {
////            ExcelUtil.browserReadExcel(file, new ImportSysMenu(), new ImportSysMenuListener(sysMenuService), 0);
////            return R.success();
////        } catch (Exception e) {
////            return R.fail(e.getMessage());
////        }
//        System.out.println("直接返回");
//        return R.success();
//    }
//}
