package com.youyilin.report.controller;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.report.logic.ImportInventoryLogic;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 库存导入
 */
@RestController
@RequestMapping("import.inventory")
public class ImportInventoryController {

    private final ImportInventoryLogic importInventoryLogic;

    public ImportInventoryController(ImportInventoryLogic importInventoryLogic) {
        this.importInventoryLogic = importInventoryLogic;
    }

    /**
     * 库存盘点导入
     */
    @Log(title = "库存盘点导入", businessType = BusinessTypeEnum.IMPORT)
    @PostMapping("check")
    public void insert(HttpServletResponse response, @RequestParam("file") MultipartFile file) throws IOException {
        importInventoryLogic.importExcelCheck(response, file);
    }
}
