//package com.youyilin.report.controller;
//
//import com.youyilin.common.annotation.Log;
//import com.youyilin.common.bean.Page;
//import com.youyilin.common.enums.BusinessTypeEnum;
//import com.youyilin.common.utils.BeanHelper;
//import com.youyilin.report.utils.ExcelUtil;
//import com.youyilin.report.model.ExportSysMenu;
//import com.youyilin.system.model.entity.SysMenu;
//import com.youyilin.system.service.SysMenuService;
//import org.springframework.security.access.prepost.PreAuthorize;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.util.List;
//
///**
// * 菜单导出
// */
//@RestController
//@RequestMapping("export.system")
//public class ExportSysMenuController {
//
//    private final SysMenuService sysMenuService;
//
//    public ExportSysMenuController(SysMenuService sysMenuService) {
//        this.sysMenuService = sysMenuService;
//    }
//
//    @Log(title = "菜单导出", businessType = BusinessTypeEnum.EXPORT)
//    @PreAuthorize("hasAuthority('export:system:menu')")
//    @PostMapping("menu")
//    public void export(HttpServletResponse response, @RequestBody SysMenu s) throws IOException {
//        Page<SysMenu> page = new Page<>();
//        page.setSearch(s);
//        page.setPageNum(null);
//        List<SysMenu> list = sysMenuService.getPage(page);
//        List<ExportSysMenu> exportList = BeanHelper.map(list, ExportSysMenu.class);
//        ExcelUtil.browserWriteExcel(response, exportList, new ExportSysMenu(), "菜单权限", "菜单sheet", 0);
//    }
//}
