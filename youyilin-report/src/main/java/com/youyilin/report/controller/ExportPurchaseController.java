package com.youyilin.report.controller;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.report.logic.ExportPurchaseLogic;
import com.youyilin.report.model.purchase.query.ExportPurchaseOrderQuery;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 采购单导出
 */
@RestController
@RequestMapping("export.purchase")
public class ExportPurchaseController {

    private final ExportPurchaseLogic exportPurchaseLogic;

    public ExportPurchaseController(ExportPurchaseLogic exportPurchaseLogic) {
        this.exportPurchaseLogic = exportPurchaseLogic;
    }


    /**
     * 采购单导出
     */
    @Log(title = "采购单导出", businessType = BusinessTypeEnum.EXPORT)
    @PostMapping("excel")
    public void export(HttpServletResponse response, @RequestBody ExportPurchaseOrderQuery query) throws IOException {
        exportPurchaseLogic.exportPurchaseExcel(response, query);
    }
}
