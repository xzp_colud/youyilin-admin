package com.youyilin.report.controller;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.report.logic.ExportInventoryLogic;
import com.youyilin.report.model.inventory.query.ExportInventoryQuery;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 库存导出
 */
@RestController
@RequestMapping("export.inventory")
public class ExportInventoryController {

    private final ExportInventoryLogic exportInventoryLogic;

    public ExportInventoryController(ExportInventoryLogic exportInventoryLogic) {
        this.exportInventoryLogic = exportInventoryLogic;
    }

    /**
     * 库存信息导出
     */
    @Log(title = "库存信息导出", businessType = BusinessTypeEnum.EXPORT)
    @PostMapping("excel")
    public void export(HttpServletResponse response, @RequestBody ExportInventoryQuery query) throws IOException {
        exportInventoryLogic.exportInventoryExcel(response, query);
    }

    /**
     * 盘点库存导出
     */
    @Log(title = "盘点库存导出", businessType = BusinessTypeEnum.EXPORT)
    @PostMapping("check")
    public void export(HttpServletResponse response) throws IOException {
        exportInventoryLogic.exportCheckExcel(response);
    }
}
