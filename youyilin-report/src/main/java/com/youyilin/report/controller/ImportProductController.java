package com.youyilin.report.controller;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.report.logic.ImportProductLogic;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 产品导入
 */
@Slf4j
@Controller
@RequestMapping("import.product")
public class ImportProductController {

    private final ImportProductLogic importProductLogic;

    public ImportProductController(ImportProductLogic importProductLogic) {
        this.importProductLogic = importProductLogic;
    }

    @Log(title = "产品导入", businessType = BusinessTypeEnum.IMPORT)
    @PreAuthorize("hasAuthority('import:product')")
    @PostMapping("insert")
    public void insert(HttpServletResponse response, @RequestParam("file") MultipartFile file) throws IOException {
        importProductLogic.importExcel(response, file);
    }

    @Log(title = "产品导入", businessType = BusinessTypeEnum.IMPORT)
    @PreAuthorize("hasAuthority('import:product')")
    @PostMapping("update")
    public void update(HttpServletResponse response, @RequestParam("file") MultipartFile file) throws IOException {
        importProductLogic.importExcelUpdate(response, file);
    }

    @Log(title = "产品属性导入", businessType = BusinessTypeEnum.IMPORT)
    @PreAuthorize("hasAuthority('import:product')")
    @PostMapping("params")
    public void importParams(HttpServletResponse response, @RequestParam("file") MultipartFile file) throws IOException {
        importProductLogic.importParams(response, file);
    }

    @Log(title = "产品批量修改状态", businessType = BusinessTypeEnum.IMPORT)
    @PreAuthorize("hasAuthority('import:product')")
    @PostMapping("status")
    public void importProductStatus(HttpServletResponse response, @RequestParam("file") MultipartFile file) throws IOException {
        importProductLogic.importProductStatus(response, file);
    }
}
