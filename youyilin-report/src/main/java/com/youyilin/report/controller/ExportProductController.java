package com.youyilin.report.controller;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.report.logic.ExportProductLogic;
import com.youyilin.report.model.product.ExportProductQuery;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 产品导出
 */
@Slf4j
@Controller
@RequestMapping("export.product")
public class ExportProductController {

    private final ExportProductLogic exportProductLogic;

    public ExportProductController(ExportProductLogic exportProductLogic) {
        this.exportProductLogic = exportProductLogic;
    }

    @Log(title = "产品导出", businessType = BusinessTypeEnum.EXPORT)
    @PreAuthorize("hasAuthority('exprot:product')")
    @PostMapping("excel")
    public void export(HttpServletResponse response, @RequestBody ExportProductQuery query) throws IOException {
        exportProductLogic.exportExcel(response, query);
    }

    @Log(title = "导出产品属性", businessType = BusinessTypeEnum.EXPORT)
    @PreAuthorize("hasAuthority('exprot:product')")
    @PostMapping("params")
    public void exportParams(HttpServletResponse response, @RequestBody ExportProductQuery query) throws IOException {
        exportProductLogic.exportParams(response, query);
    }

    @Log(title = "导出产品SKU", businessType = BusinessTypeEnum.EXPORT)
    @PreAuthorize("hasAuthority('exprot:product')")
    @PostMapping("sku")
    public void exportSKU(HttpServletResponse response, @RequestBody ExportProductQuery query) throws IOException {
        exportProductLogic.exportSKU(response, query);
    }
}
