package com.youyilin.common.constants;

/**
 * 基本常量
 */
public class CommonConstants {

    public final static String HTTP = "http://";
    public final static String HTTPS = "https://";
    public final static String LIMIT_ONE = "limit 1";
}
