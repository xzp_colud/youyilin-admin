package com.youyilin.common.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.youyilin.common.enums.DelFlagEnum;
import com.youyilin.common.utils.SecurityUtil;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * mybatis plus 默认插入、修改数据
 */
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        Date date = new Date();
        Long modifier = SecurityUtil.getUserIdNull();
        String modifyName = SecurityUtil.getUsernameNull();
        this.strictInsertFill(metaObject, "createDate", Date.class, date);
        this.strictInsertFill(metaObject, "creator", Long.class, modifier);
        this.strictInsertFill(metaObject, "createName", String.class, modifyName);
        this.strictInsertFill(metaObject, "modifyDate", Date.class, date);
        this.strictInsertFill(metaObject, "modifier", Long.class, modifier);
        this.strictInsertFill(metaObject, "modifyName", String.class, modifyName);
        this.strictInsertFill(metaObject, "delFlag", Integer.class, DelFlagEnum.NORMAL.getCode());
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        Date date = new Date();
        Long modifier = SecurityUtil.getUserIdNull();
        String modifyName = SecurityUtil.getUsernameNull();
        this.strictUpdateFill(metaObject, "modifyDate", Date.class, date);
        this.strictInsertFill(metaObject, "modifier", Long.class, modifier);
        this.strictInsertFill(metaObject, "modifyName", String.class, modifyName);
    }
}
