package com.youyilin.common.config.qiniu;

import java.io.File;

/**
 * 七牛 线程上传
 */
public class QiNiuRunnable implements Runnable {

    private final QiNiuService qiNiuService;
    private byte[] uploadBytes;
    private File file;

    public QiNiuRunnable(QiNiuService qiNiuService, byte[] uploadBytes) {
        this.qiNiuService = qiNiuService;
        this.uploadBytes = uploadBytes;
    }

    public QiNiuRunnable(QiNiuService qiNiuService, File file) {
        this.qiNiuService = qiNiuService;
        this.file = file;
    }

    @Override
    public void run() {
        if (this.uploadBytes != null) {
            this.qiNiuService.uploadBytes(this.uploadBytes);
        } else if (this.file != null) {
            this.qiNiuService.uploadFile(this.file);
        }
    }
}
