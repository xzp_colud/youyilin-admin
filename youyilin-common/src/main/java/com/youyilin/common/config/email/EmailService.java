package com.youyilin.common.config.email;

import com.youyilin.common.exception.UtilException;
import com.youyilin.common.utils.SpringUtil;
import com.sun.mail.util.MailSSLSocketFactory;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.URLDataSource;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

/**
 * 邮件
 */
@Slf4j
public class EmailService {

    // 主题
    private final String subject;
    // 发送人
    private final String from;
    // 接收人
    private final String[] tos;
    // 内容
    private final String text;
    private final boolean isHtml;
    private final List<DataSource> ds;
    private final Date sentDate = new Date();
    private final JavaMailSenderImpl javaMailSender;

    private EmailService(Properties properties, String subject, String[] tos, String text, boolean isHtml, List<DataSource> ds) {
        this.subject = subject;
        this.tos = tos;
        this.text = text;
        this.isHtml = isHtml;
        this.ds = ds;
        this.javaMailSender = SpringUtil.getBean(JavaMailSenderImpl.class);
        if (null != properties) {
            javaMailSender.setJavaMailProperties(properties);
        }
        this.from = javaMailSender.getUsername();
    }

    public void send() {
        try {
            MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            // true表示构建一个可以带附件的邮件对象
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);

            helper.setSubject(this.subject);
            helper.setFrom(this.from);
            helper.setTo(this.tos);
            helper.setSentDate(this.sentDate);
            helper.setText(this.text, this.isHtml);

            if (CollectionUtils.isNotEmpty(this.ds)) {
                // 附件
                MimeBodyPart contentPart = new MimeBodyPart();
                // 纯文本
                MimeMultipart contentMultipart = new MimeMultipart("related");
                MimeBodyPart htmlPart = new MimeBodyPart();
                htmlPart.setContent(this.text, "text/html;charset=utf-8");

                // 附件
                for (DataSource d : this.ds) {
                    String fileName = System.currentTimeMillis() + ".xlsx";
                    MimeBodyPart excelBodyPart = new MimeBodyPart();
                    DataHandler dataHandler = new DataHandler(d);
                    excelBodyPart.setDataHandler(dataHandler);
                    excelBodyPart.setFileName(fileName);
                    contentMultipart.addBodyPart(excelBodyPart);
                }

                contentMultipart.addBodyPart(htmlPart);
                contentPart.setContent(contentMultipart);

                MimeMultipart mime = new MimeMultipart("mixed");
                mime.addBodyPart(contentPart);
                mimeMessage.setContent(mime);
            }
            javaMailSender.send(mimeMessage);
        } catch (Exception e) {
            log.error("【发送邮件异常】", e);
            throw new UtilException("发送邮件异常");
        }
    }

    public static class Builder {
        // 主题
        private String subject;
        // 接收人
        private String[] tos;
        // 内容
        private String text;
        private boolean isHtml;
        private List<DataSource> ds;
        private Properties properties;

        public Builder() {

        }

        public Builder config(EmailConfig config) {
            if (config != null) {
                MailSSLSocketFactory sslSocketFactory = null;
                try {
                    sslSocketFactory = new MailSSLSocketFactory();
                    sslSocketFactory.setTrustAllHosts(true);
                } catch (GeneralSecurityException ignored) {

                }
                Properties p = new Properties();
                p.put("mail.transport.protocol", config.getProtocol());
                p.put("mail.smtp.host", config.getHost());
                p.put("mail.smtp.port", config.getPort());
                p.put("mail.smtp.auth", true);
                p.put("mail.debugger", config.isDebugger());
                p.put("mail.smtp.ssl.enable", config.isSsl());
                p.put("mail.smtp.ssl.socketFactory", sslSocketFactory);
                this.properties = p;
            }
            return this;
        }

        public Builder subject(String subject) {
            this.subject = subject;
            return this;
        }

        public Builder tos(String[] tos) {
            this.tos = tos;
            return this;
        }

        public Builder text(String text) {
            this.text = text;
            return this;
        }

        public Builder isHtml(boolean isHtml) {
            this.isHtml = isHtml;
            return this;
        }

        public Builder attachUrl(List<String> attachUrl) {
            for (String fileUrl : attachUrl) {
                URL url = null;
                try {
                    url = new URL(fileUrl);
                } catch (MalformedURLException e) {
                    throw new RuntimeException(e);
                }
                DataSource ds = new URLDataSource(url);
                if (CollectionUtils.isEmpty(this.ds)) {
                    this.ds = new ArrayList<>();
                }
                this.ds.add(ds);
            }
            return this;
        }

        public Builder attachByte(List<byte[]> attachByte) {
            for (byte[] bytes : attachByte) {
                DataSource ds = new ByteArrayDataSource(bytes, "application/excel");
                if (CollectionUtils.isEmpty(this.ds)) {
                    this.ds = new ArrayList<>();
                }
                this.ds.add(ds);
            }
            return this;
        }

        public EmailService build() {
            return new EmailService(this.properties, this.subject, this.tos, this.text, this.isHtml, this.ds);
        }
    }
}