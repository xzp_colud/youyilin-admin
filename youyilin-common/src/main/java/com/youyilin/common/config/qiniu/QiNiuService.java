package com.youyilin.common.config.qiniu;

import com.youyilin.common.exception.UtilException;
import com.google.gson.Gson;
import com.qiniu.common.QiniuException;
import com.qiniu.common.Zone;
import com.qiniu.http.Response;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.io.ByteArrayInputStream;
import java.io.File;

@Slf4j
public class QiNiuService {

    private final String accessKey;
    private final String secretKey;
    private final String bucket;
    private final String domain;
    private final String upToken;

    private QiNiuService(String accessKey, String secretKey, String bucket, String domain) {
        this.accessKey = accessKey;
        this.secretKey = secretKey;
        this.bucket = bucket;
        this.domain = domain;
        this.validate();
        this.upToken = Auth.create(this.accessKey, this.secretKey).uploadToken(this.bucket);
    }

    private void validate() {
        if (StringUtils.isBlank(this.accessKey)
                || StringUtils.isBlank(this.secretKey)
                || StringUtils.isBlank(this.bucket)
                || StringUtils.isBlank(this.domain)) {
            throw new UtilException("上传异常");
        }
    }

    /**
     * 文件上传
     */
    public String uploadFile(File file) {
        // 构造一个带指定 Region 对象的配置类
        Configuration cfg = new Configuration(Zone.zone2());
        // 其他参数参考类注释
        UploadManager uploadManager = new UploadManager(cfg);
//        String localFilePath = "/home/qiniu/test.png";
        // 默认不指定key的情况下，以文件内容的hash值作为文件名
        try {
            Response response = uploadManager.put(file, null, this.upToken);
            return this.uploadAfter(response);
        } catch (QiniuException ex) {
            log.error("【文件上传失败】", ex);
            return null;
        }
    }

    /**
     * 字节上传
     */
    public String uploadBytes(byte[] uploadBytes) {
        // 构造一个带指定 Region 对象的配置类
        Configuration cfg = new Configuration(Zone.zone2());
        // 其他参数参考类注释
        UploadManager uploadManager = new UploadManager(cfg);
        // 默认不指定key的情况下，以文件内容的hash值作为文件名
        ByteArrayInputStream byteInputStream = new ByteArrayInputStream(uploadBytes);
        try {
            Response response = uploadManager.put(byteInputStream, null, this.upToken, null, null);
            return this.uploadAfter(response);
        } catch (QiniuException ex) {
            log.error("【文件上传失败】", ex);
            return null;
        }
    }

    /**
     * 解析上传成功的结果
     */
    private String uploadAfter(Response response) throws QiniuException {
        DefaultPutRet putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class);
        String fileUrl = this.domain + putRet.hash;
        log.info("【七牛】 {}", fileUrl);
        return fileUrl;
    }

    public static class Builder {

        private String accessKey;
        private String secretKey;
        private String bucket;
        private String domain;

        public Builder() {

        }

        public Builder config(QiNiuConfig config) {
            this.accessKey = config.getAccessKey();
            this.secretKey = config.getSecretKey();
            this.bucket = config.getBucket();
            this.domain = config.getDomain();

            return this;
        }

        public QiNiuService build() {
            return new QiNiuService(this.accessKey, this.secretKey, this.bucket, this.domain);
        }
    }
}
