package com.youyilin.common.config.email;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 邮件配置
 */
@Data
@Accessors(chain = true)
@Component("EmailConfigImpl")
@ConfigurationProperties(prefix = "spring.mail")
public class EmailConfig {

    //    private Session session;
    // 协议
    private String protocol;
    // 服务器
    private String host;
    // 端口
    private int port = -1;
    // 账号
    private String username;
    // 密码
    private String password;
    // ssl
    private boolean isSsl;
    // 控制台打印
    private boolean isDebugger = true;
    // 编码
    private String defaultEncoding = "UTF-8";
}
