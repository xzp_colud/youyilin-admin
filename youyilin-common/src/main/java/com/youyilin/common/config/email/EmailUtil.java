package com.youyilin.common.config.email;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import java.util.regex.Pattern;

/**
 * 邮箱 Util
 */
@Slf4j
public class EmailUtil {

    // 邮箱正则
    private static final String EMAIL_PATTERN = "^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*\\.[a-zA-Z0-9]{2,6}$";

    /**
     * 是否为邮箱
     *
     * @param email 邮箱
     * @return boolean
     */
    public static boolean isNotEmail(String email) {
        if (StringUtils.isBlank(email)) {
            return true;
        }
        return !Pattern.matches(EMAIL_PATTERN, email);
    }
}
