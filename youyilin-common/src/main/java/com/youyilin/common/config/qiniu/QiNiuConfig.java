package com.youyilin.common.config.qiniu;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 七牛配置
 */
@Data
@Accessors(chain = true)
@Component("QiNiuConfigImpl")
@ConfigurationProperties(prefix = "qiniu")
public class QiNiuConfig {

    private String accessKey;
    private String secretKey;
    private String bucket;
    private String domain;
}
