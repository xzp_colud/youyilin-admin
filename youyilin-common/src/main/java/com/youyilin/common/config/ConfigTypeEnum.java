package com.youyilin.common.config;

import java.util.ArrayList;
import java.util.List;

/**
 * 配置类型
 */
public enum ConfigTypeEnum {

    OTHER(0, "其他", ""),
    EMAIL(1, "邮件", "EmailConfigImpl"),
    QI_NIU(2, "七牛", "QiNiuConfigImpl");

    private int code;
    private String info;
    private String beanName;

    ConfigTypeEnum(int code, String info, String beanName) {
        this.code = code;
        this.info = info;
        this.beanName = beanName;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getBeanName() {
        return beanName;
    }

    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }

    public static List<Integer> listCodeExcludeOther() {
        List<Integer> beanList = new ArrayList<>();
        for (ConfigTypeEnum cte : ConfigTypeEnum.values()) {
            if (cte.getCode() == ConfigTypeEnum.OTHER.getCode()) {
                continue;
            }
            beanList.add(cte.getCode());
        }
        return beanList;
    }

    public static String queryBeanByCode(int code) {
        for (ConfigTypeEnum cte : ConfigTypeEnum.values()) {
            if (cte.getCode() == code) {
                return cte.getBeanName();
            }
        }
        return null;
    }
}
