package com.youyilin.common.config.email;

import com.youyilin.common.exception.UtilException;

/**
 * 邮件线程 发送
 */
public class EmailRunnable implements Runnable {

    private final EmailService emailService;

    public EmailRunnable(EmailService ef) {
        if (ef == null) {
            throw new UtilException("邮件异常");
        }
        this.emailService = ef;
    }

    @Override
    public void run() {
        emailService.send();
    }
}
