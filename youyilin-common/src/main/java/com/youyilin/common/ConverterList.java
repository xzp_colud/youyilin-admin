package com.youyilin.common;

import java.util.List;

public interface ConverterList<S, T> {

    List<T> convert(List<S> s);
}
