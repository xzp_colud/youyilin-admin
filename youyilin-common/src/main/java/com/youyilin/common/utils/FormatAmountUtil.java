package com.youyilin.common.utils;

import java.math.BigDecimal;

/**
 * 格式化金额
 */
public class FormatAmountUtil {

    public static String format(Integer amount) {
        if (amount == null) {
            return "0";
        }

        return String.format("%.2f", Double.valueOf(amount) / 100L);
    }

    public static String format(Long amount) {
        if (amount == null) {
            return "0";
        }

        return String.format("%.2f", Double.valueOf(amount) / 100L);
    }

    public static BigDecimal formatToBigDecimal(Integer amount) {
        return new BigDecimal(format(amount));
    }

    public static BigDecimal formatToBigDecimal(Long amount) {
        return new BigDecimal(format(amount));
    }
}
