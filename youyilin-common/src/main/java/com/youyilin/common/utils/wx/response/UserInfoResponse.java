package com.youyilin.common.utils.wx.response;

import lombok.Data;

@Data
public class UserInfoResponse {

    private String id;
    private String openid;
    private String nickname;
    private String sex;
    private String province;
    private String city;
    private String country;
    private String headimgurl;
    private String unionid;
    private String appid;
}
