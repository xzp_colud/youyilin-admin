package com.youyilin.common.utils.wx;

import com.alibaba.fastjson.JSONObject;
import com.youyilin.common.utils.http.HttpUtils;
import com.youyilin.common.utils.wx.response.UserInfoResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

@Slf4j
public class WXRequestUtil {

    public static UserInfoResponse userInfo(String url) {
        String result = HttpUtils.sendGet(url, null);
        log.info("【微信用户信息】 {}", result);
        if (StringUtils.isNotBlank(result)) {
            UserInfoResponse response = JSONObject.parseObject(result, UserInfoResponse.class);
            if (StringUtils.isNotBlank(response.getOpenid())) {
                return response;
            }
        }
        return new UserInfoResponse();
    }
}
