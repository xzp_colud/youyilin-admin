package com.youyilin.common.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 手机号验证 util
 */
public class MobileUtil {

    // 手机号正则
    private static final String MOBILE_PATTERN = "^[1](([3|5|8][\\d])|([4][4,5,6,7,8,9])|([6][2,5,6,7])|([7][^9])|([9][1,8,9]))[\\d]{8}$";

    /**
     * 手机号码是否正确
     *
     * @param mobile 手机号
     * @return boolean
     */
    public static boolean isNotMobile(String mobile) {
        if (StringUtils.isBlank(mobile)) {
            return true;
        }
        return !Pattern.matches(MOBILE_PATTERN, mobile);
    }

    /**
     * 手机号处理成 134****0000
     *
     * @param mobile 手机号
     * @return String
     */
    public static String formatMobileToStar(String mobile) {
        if (isNotMobile(mobile)) {
            return null;
        }

        String start = mobile.substring(0, 3);
        String end = mobile.substring(7, 11);
        return start.concat("****").concat(end);
    }

//    public static void main(String[] args) {
//        String a = formatMobileToStar("13750776907");
//        System.out.println(a);
//    }
}
