package com.youyilin.common.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.youyilin.common.exception.UtilException;
import com.youyilin.common.bean.RMsg;
import org.dozer.DozerBeanMapper;
import org.springframework.util.CollectionUtils;

/**
 * 对象映射
 */
public class BeanHelper {

    private static final DozerBeanMapper mapper = new DozerBeanMapper();

    /**
     * 对象转换
     */
    public static <T, S> T map(final S sourceEntry, final Class<T> targetBOClass) {
//        if (mapper == null) {
//            mapper = new DozerBeanMapper();
//        }

        if (sourceEntry == null) {
            try {
                return targetBOClass.newInstance();
            } catch (Exception e) {
                throw new UtilException("数据对象为空");
            }
        }

        return mapper.map(sourceEntry, targetBOClass);
    }

    /**
     * 对象数组转换
     */
    public static <T, S> List<S> map(List<T> sourceBOs, Class<S> targetEntryClass) {
        if (sourceBOs == null) {
            throw new UtilException(RMsg.SERVE_FAIL.getMsg());
        }

        if (CollectionUtils.isEmpty(sourceBOs)) {
            return Collections.emptyList();
        }

        List<S> list = new ArrayList<>(sourceBOs.size());
        for (T t : sourceBOs) {
            list.add(map(t, targetEntryClass));
        }

        return list;
    }
}
