package com.youyilin.common.utils.wx;

import com.youyilin.common.exception.UtilException;
import org.apache.commons.lang3.StringUtils;

public class WXUrlConstants {

    // 微信网页回调 换取CODE
    private final static String AUTH_AUTHORIZE_URL = "https://open.weixin.qq.com/connect/oauth2/authorize";
    // 微信网页回调 CODE换取ACCESS_TOKEN OPENID
    private final static String AUTH_ACCESS_TOKEN_URL = "https://api.weixin.qq.com/sns/oauth2/access_token";
    // 微信网页回调 OPENID换取用户信息
    private final static String AUTH_USER_INFO_URL = "https://api.weixin.qq.com/sns/userinfo";

    /**
     * 回调地址
     *
     * @param appId       应用ID
     * @param redirectUri 自定义回调地址
     * @param scope       授权类型
     * @param state       额外一个参数
     */
    public static String getAuthorizeUrl(String appId, String redirectUri, String scope, String state) {
        if (StringUtils.isBlank(appId)) {
            throw new UtilException("缺少应用ID");
        }
        if (StringUtils.isBlank(redirectUri)) {
            throw new UtilException("缺少回调地址");
        }
        if (StringUtils.isBlank(scope)) {
            scope = "snsapi_base";
        }
        if (!scope.equals("snsapi_base") && !scope.equals("snsapi_userinfo")) {
            throw new UtilException("授权类型异常");
        }
        if (StringUtils.isBlank(state)) {
            state = "";
        }
        return AUTH_AUTHORIZE_URL + "?appid=" + appId + "&redirect_uri=" + redirectUri + "&response_type=code&scope=" + scope + "&state=" + state + "#wechat_redirect";
    }

    /**
     * 获取授权地址
     *
     * @param appId     应用ID
     * @param appSecret 应用秘钥
     * @param code      凭证
     */
    public static String getAccessTokenUrl(String appId, String appSecret, String code) {
        if (StringUtils.isBlank(appId)) {
            throw new UtilException("缺少应用ID");
        }
        if (StringUtils.isBlank(appSecret)) {
            throw new UtilException("缺少应用秘钥");
        }
        if (StringUtils.isBlank(code)) {
            throw new UtilException("缺少CODE凭证");
        }
        return AUTH_ACCESS_TOKEN_URL + "?appid=" + appId + "&secret=" + appSecret + "&code=" + code + "&grant_type=authorization_code";
    }

    /**
     * 获取用户信息地址
     *
     * @param accessToken 授权凭证
     * @param openId      用户ID
     */
    public static String getAccessTokenUrl(String accessToken, String openId) {
        if (StringUtils.isBlank(accessToken)) {
            throw new UtilException("缺少授权凭证");
        }
        if (StringUtils.isBlank(openId)) {
            throw new UtilException("缺少用户ID");
        }
        return AUTH_ACCESS_TOKEN_URL + "?access_token=" + accessToken + "&openid=" + openId + "&lang=zh_CN";
    }
}
