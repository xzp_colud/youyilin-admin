package com.youyilin.common.utils;

/**
 * 文本框限定输入
 */
public class CharterUtil {

    /**
     * Purpose:必须输入英文+._
     *
     * @param str : 要檢核的字符串
     * @return Boolean ： 檢核結果
     * @author Hermanwang
     */
    public static boolean characterString(String str) {
        int letter = 0;
        int space = 0;
        String letterStr = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM._";
        for (int i = 0; i < str.length(); i++) {
            String temp = str.substring(i, i + 1);
            if (letterStr.contains(temp)) {
                letter++;
            } else {
                space++;
            }
        }
        return letter > 0 && space == 0;
    }

    /**
     * Purpose:必须输入英文数字
     *
     * @param str : 要檢核的字符串
     * @return Boolean ： 檢核結果
     * @author Hermanwang
     */
    public static boolean characterStringOrNum(String str) {
        int letter = 0;
        int number = 0;
        int space = 0;
        String letterStr = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";
        String numberStr = "0123456789";
        for (int i = 0; i < str.length(); i++) {
            String temp = str.substring(i, i + 1);
            if (letterStr.contains(temp)) {
                letter++;
            } else if (numberStr.contains(temp)) {
                number++;
            } else {
                space++;
            }
        }
        return (letter > 0 || number > 0) && space == 0;
    }

    /**
     * Purpose:必须输入数字
     *
     * @param str : 要檢核的字符串
     * @return Boolean ： 檢核結果
     * @author Hermanwang
     */
    public static boolean characterNum(String str) {
        int number = 0;
        int space = 0;
        String numberStr = "0123456789";
        for (int i = 0; i < str.length(); i++) {
            String temp = str.substring(i, i + 1);
            if (numberStr.contains(temp)) {
                number++;
            } else {
                space++;
            }
        }
        return number > 0 && space == 0;
    }

    /**
     * Purpose:必须输入英文数字和符号
     *
     * @param str : 要檢核的字符串
     * @return Boolean ： 檢核結果
     * @author Hermanwang
     */
    public static boolean characterAll(String str) {
        int letter = 0;
        int number = 0;
        int character = 0;
        int space = 0;
        String letterStr = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";
        String numberStr = "0123456789";
        String characterStr = "~!@#$%^&*()_+`-=/*\\':'\"\'<>,.?/";
        String spaceStr = " ";
        for (int i = 0; i < str.length(); i++) {
            String temp = str.substring(i, i + 1);
            if (letterStr.contains(temp)) {
                letter++;
            } else if (numberStr.contains(temp)) {
                number++;
            } else if (characterStr.contains(temp)) {
                character++;
            } else if (spaceStr.contains(temp)) {
                space++;
            } else {
                space++;
            }
        }
        return (letter > 0 || number > 0 || character > 0) && space == 0;
    }

}
