package com.youyilin.common.utils;

import com.youyilin.common.exception.SysException;
import com.youyilin.common.entity.LoginAdminEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * 安全服务工具类
 */
public class SecurityUtil {

    /**
     * 获取用户
     **/
    public static LoginAdminEntity getLoginUser() {
        try {
            Authentication authentication = getAuthentication();
            if (authentication != null) {
                Object principal = authentication.getPrincipal();
                if (principal instanceof String && principal.toString().equals("anonymousUser")) {
                    throw new SysException("获取用户信息异常");
                }
                if (principal instanceof LoginAdminEntity) {
                    return (LoginAdminEntity) principal;
                }
            }
            throw new SysException("获取用户信息异常");
        } catch (Exception e) {
            throw new SysException("获取用户信息异常");
        }
    }

    /**
     * 登录Id
     **/
    public static Long getLoginIdNull() {
        try {
            return getLoginUser().getId();
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 用户ID
     **/
    public static Long getUserId() {
        try {
            return getLoginUser().getUserId();
        } catch (Exception e) {
            throw new SysException("获取用户ID异常");
        }
    }

    /**
     * 用户ID
     **/
    public static Long getUserIdNull() {
        try {
            return getLoginUser().getUserId();
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 获取角色
     **/
    public static String getRoleId() {
        try {
            return getLoginUser().getRoleIds();
        } catch (Exception e) {
            throw new SysException("获取用户角色异常");
        }
    }

    /**
     * 获取用户账户
     **/
    public static String getUsername() {
        try {
            return getLoginUser().getUsername();
        } catch (Exception e) {
            throw new SysException("获取用户账户异常");
        }
    }

    /**
     * 获取用户账户
     **/
    public static String getUsernameNull() {
        try {
            return getLoginUser().getUsername();
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 获取openid
     */
    public static String getOpenid() {
        try {
            return getLoginUser().getOpenid();
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 获取Authentication
     */
    public static Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }
}
