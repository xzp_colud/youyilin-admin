package com.youyilin.common.utils.ip;

import com.youyilin.common.utils.http.HttpUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

/**
 * 获取地址类
 */
@Slf4j
public class AddressUtils {
    // IP地址查询
    public static final String IP_URL = "http://whois.pconline.com.cn/ip.jsp";

    // 未知地址
    public static final String UNKNOWN = "XX XX";

    public static String getRealAddressByIP(String ip) {
        // 内网不查询
        if (IpUtils.internalIp(ip)) {
            return "内网IP";
        }
        try {
            String rspStr = HttpUtils.sendGet(IP_URL, "ip=" + ip + "&json=true", "GBK");
            if (StringUtils.isEmpty(rspStr)) {
                log.error("获取地理位置异常 {}", ip);
                return UNKNOWN;
            }
//            JSONObject obj = JSONObject.parseObject(rspStr);
//            String region = obj.getString("pro");
//            String city = obj.getString("city");
//            String addr = obj.getString("addr");
//            if (addr.length() > 50) {
//                addr = addr.substring(0, 50);
//            }
//            return String.format("%s %s", region, city);
            return String.format("%s", rspStr);
        } catch (Exception e) {
            log.error("获取地理位置异常 {}", ip);
        }
        return UNKNOWN;
    }

//    public static void main(String[] args) {
//        getRealAddressByIP("223.94.222.174");
//    }
}
