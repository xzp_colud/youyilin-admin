package com.youyilin.common.utils;

import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Field;
import java.util.List;

/**
 * 去除 空格 util
 */
@Slf4j
public class SpaceUtil {

    /**
     * string 去除空格
     */
    public static String stringCancelSpace(String ob) {
        if (ob != null) {
            ob = ob.trim();
        }
        return ob;
    }

    /**
     * 实体类 去除空格
     */
    public static <T> T entityCancelSpace(T t) {
        cancelSpace(t);
        return t;
    }

    /**
     * list 去除空格
     */
    public static <T> List<T> listCancelSpace(List<T> list) {
        for (T t : list) {
            cancelSpace(t);
        }
        return list;
    }

    /**
     * 去除 前后 空格
     */
    private static void cancelSpace(Object object) {
        if (object != null) {
            Field[] fields = object.getClass().getDeclaredFields();
            for (int i = 0; i < fields.length; i++) {
                try {
                    Field field = fields[i];
                    field.setAccessible(true);
                    // 属性类型
                    String type = field.getGenericType().toString();
                    // 属性值
                    Object o = field.get(object);
                    // 有值 并且是 String 类型
                    if (o != null && type.equals("class java.lang.String")) {
                        String oString = o.toString();
                        oString = oString.trim();
                        field.set(object, oString);
                    }
                } catch (Exception e) {
                    log.error("【去除空格异常】", e);
                }
            }
        }
    }

}
