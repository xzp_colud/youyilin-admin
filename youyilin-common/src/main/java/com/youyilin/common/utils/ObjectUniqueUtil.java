package com.youyilin.common.utils;

import com.youyilin.common.exception.UtilException;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Objects;

/**
 * 验证两个对象是否唯一
 */
@Slf4j
public class ObjectUniqueUtil {

    /**
     * 判断两个对象是否是唯一
     *
     * @param source 传入对象
     * @param unique 验证对象
     */
    public static boolean validateObjectUnique(Object source, Object unique) {
        if (source == null) {
            throw new UtilException("source object is null");
        }
        if (unique == null) {
            return true;
        }

        try {
            Method sourceMethod = source.getClass().getMethod("getId");
            Object sourceId = sourceMethod.invoke(source);
            if (sourceId == null) {
                return false;
            }
            Method uniqueMethod = unique.getClass().getMethod("getId");
            Object uniqueId = uniqueMethod.invoke(unique);
            return Objects.equals(sourceId, uniqueId);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            log.error("【验证唯一性】", e);
            return false;
        }
    }
}
