package com.youyilin.common.utils;

import java.util.Random;

/**
 * 生成随机字符
 */
public class RandomUtil {

    /**
     * 由数字和字符串组合而成的字符串
     *
     * @param length 长度
     * @return 字符串
     */
    public static String getStringRandom(int length) {

        StringBuilder val = new StringBuilder();
        Random random = new Random();

        // 参数length，表示生成几位随机数
        for (int i = 0; i < length; i++) {

            String charOrNum = random.nextInt(2) % 2 == 0 ? "char" : "num";
            //输出字母还是数字
            if ("char".equalsIgnoreCase(charOrNum)) {
                //输出是大写字母还是小写字母
                int temp = random.nextInt(2) % 2 == 0 ? 65 : 97;
                val.append((char) (random.nextInt(26) + temp));
            } else {
                val.append(random.nextInt(10));
            }
        }
        return val.toString();
    }

//    public static void main(String[] args) {
//        System.out.println(getStringRandom(40));
//    }
}
