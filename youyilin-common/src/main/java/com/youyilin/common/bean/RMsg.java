package com.youyilin.common.bean;

/**
 * 系统返回 提示语
 */
public enum RMsg {

    REPEAT(-2, "重复提交"),

    /* 通用 */
    FAIL(-1, "操作失败"),
    SUCCESS(0, "操作成功"),

    /* 登录 */
    NO_LOGIN(5, "尚未登录"),
    LOGIN_SUCCESS(6, "登录成功"),
    LOGIN_Fail(7, "登录失败"),
    LOGOUT_SUCCESS(8, "退出成功"),

    /* 新增 提交 编辑 删除 */
    ADD_SUCCESS(10, "保存成功"),
    ADD_FAIL(11, "保存失败"),
    SUBMIT_SUCCESS(12, "提交成功"),
    SUBMIT_FAIL(13, "提交失败"),
    EDIT_SUCCESS(14, "保存成功"),
    EDIT_FAIL(15, "保存失败"),
    DELETE_SUCCESS(16, "删除成功"),
    DELETE_FAIL(17, "删除失败"),

    /* 登录 */
    PASSWORD_FAIL(100, "密码错误"),
    ADMIN_NO_EXIT(101, "账号不存在"),
    ADMIN_FIXD(102, "用户名或密码错误"),

    /* 一般 */
    NO_AUTHORITY(403, "没有权限操作"),
    SERVE_FAIL(500, "系统错误"),

    NO_PARAMS(10001, "参数错误"),
    MONEY_FAIL(10002, "金额错误"),
    TIME_FAIL(10003, "时间错误"),;

    /**
     * 占位值
     */
    private int index;
    /**
     * 提示语
     */
    private String msg;

    RMsg(int index, String msg) {
        this.index = index;
        this.msg = msg;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static String getMsgByIndex(int index) {
        for (RMsg t : RMsg.values()) {
            if (t.getIndex() == index) {
                return t.msg;
            }
        }
        return null;
    }
}
