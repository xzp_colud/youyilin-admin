package com.youyilin.common.bean;

import lombok.Data;

import java.io.Serializable;

/**
 * 结果集
 */
@Data
public class R<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 是否成功
     */
    private boolean success;
    /**
     * 提示语
     */
    private String msg;
    /**
     * 数据
     */
    private T data;

    /**
     * 成功结果集
     */
    public static <T> R<T> success() {
        return doResult(true, "操作成功", null);
    }

    /**
     * 成功
     */
    public static <T> R<T> success(String msg) {
        return doResult(true, msg, null);
    }

    /**
     * 成功
     */
    public static <T> R<T> success(T t) {
        return doResult(true, "操作成功", t);
    }

    /**
     * 成功
     */
    public static <T> R<T> succ(T t) {
        return doResult(true, null, t);
    }

    /**
     * 成功
     */
    public static <T> R<T> success(String msg, T t) {
        return doResult(true, msg, t);
    }

    /**
     * 失败
     */
    public static <T> R<T> fail() {
        return doResult(false, "操作失败", null);
    }

    /**
     * 失败
     */
    public static <T> R<T> fail(String msg) {
        return doResult(false, msg, null);
    }

    /**
     * 统一调用
     */
    public static <T> R<T> doResult(boolean success, String msg, T t) {
        R<T> r = new R<>();
        r.setSuccess(success);
        r.setMsg(msg);
        r.setData(t);
        return r;
    }
}
