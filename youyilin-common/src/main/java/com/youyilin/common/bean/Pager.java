package com.youyilin.common.bean;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.apache.commons.collections.CollectionUtils;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.List;

@Data
public class Pager<T> implements Serializable {
    private static final long serialVersionUID = 517125196037921858L;

    private Integer totalNum;
    private List<T> pageList;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long limitId;

    public Pager(Integer totalNum, List<T> pageList) {
        this.totalNum = totalNum;
        this.pageList = pageList;

        if (CollectionUtils.isNotEmpty(pageList)) {
            T t = pageList.get(pageList.size() - 1);
            try {
                Method getMethod = t.getClass().getMethod("getId");
                this.limitId = (Long) getMethod.invoke(t);
            } catch (Exception ignored) {
                this.limitId = null;
            }
        }
    }
}
