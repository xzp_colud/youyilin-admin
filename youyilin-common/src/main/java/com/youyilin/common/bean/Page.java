package com.youyilin.common.bean;

import com.youyilin.common.utils.DateUtils;
import com.youyilin.common.utils.SpaceUtil;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.Date;

/**
 * 分页列表
 *
 * @param <T>
 */
public class Page<T> implements Serializable {
    private static final long serialVersionUID = 7973622388148469823L;

    /**
     * 当前页码 默认1
     */
    private Integer pageNum = 1;
    /**
     * 每页显示条数 默认10
     */
    private Integer pageSize = 10;
    /**
     * 页码总数
     */
    private Integer count;
    /**
     * 查询参数
     */
    private T search;
    /**
     * 排序
     */
    private String orderBy;
    /**
     * 升序/降序
     */
    private String ad;
    /**
     * 时间范围
     */
    private Integer dateType;
    /**
     * 时间段
     */
    private String startDate;
    private String endDate;

    public Page() {

    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        if (pageSize == null || pageSize <= 0) {
            return 10;
        }
        if (pageSize > 50) {
            return 50;
        }
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Object getSearch() {
        return search;
    }

    public void setSearch(T search) {
        SpaceUtil.entityCancelSpace(search);
        this.search = search;
    }

    public Integer getPageNo() {
        if (this.pageNum == null || this.pageSize == null) {
            return 0;
        }
        return (this.pageNum - 1) * this.pageSize;
    }

    public String getStartDate() {
        if (StringUtils.isNotBlank(startDate)) {
            Date start = DateUtils.parseDate(startDate);
            return DateUtils.formatDateTime(start);
        }
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        if (StringUtils.isNotBlank(endDate)) {
            Date end = DateUtils.parseDate(endDate);
            return DateUtils.getNextDay(end, 1);
        }
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public String getAd() {
        return ad;
    }

    public void setAd(String ad) {
        this.ad = ad;
    }

    public Integer getDateType() {
        return dateType;
    }

    public void setDateType(Integer dateType) {
        this.dateType = dateType;
    }
}
