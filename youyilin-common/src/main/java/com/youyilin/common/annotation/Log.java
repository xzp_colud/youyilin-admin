package com.youyilin.common.annotation;

import com.youyilin.common.enums.BusinessTypeEnum;

import java.lang.annotation.*;

/**
 * 全局日志
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Log {

    /**
     * 模块
     */
    public String title() default "";

    /**
     * 功能
     */
    public BusinessTypeEnum businessType() default BusinessTypeEnum.OTHER;

    /**
     * 二级描述
     */
    public String remark() default "";
}
