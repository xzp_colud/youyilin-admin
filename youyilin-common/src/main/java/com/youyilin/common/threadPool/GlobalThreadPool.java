package com.youyilin.common.threadPool;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.*;

/**
 * 全局的线程池
 */
@Slf4j
public class GlobalThreadPool {

    private static final int corePoolSize = 2;
    private static final int maximumPoolSize = 3;
    private static final int keepAliveTime = 1;
    private static final TimeUnit unit = TimeUnit.SECONDS;
    private static final int QueueSize = 200;
    private static final BlockingQueue<Runnable> workQueue = new ArrayBlockingQueue<>(QueueSize);

    final static ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue);

    /**
     * 无返回值
     *
     * @param t 线程
     */
    public static void runThread(Runnable t) {
        threadPoolExecutor.submit(t);
    }

    /**
     * 有返回值
     *
     * @param t 线程
     * @return 返回数据
     */
    public static Object runThread(Callable<Object> t) {
        FutureTask<Object> futureTask = new FutureTask<>(t);
        threadPoolExecutor.submit(t);
        try {
            return futureTask.get();
        } catch (InterruptedException | ExecutionException e) {
            log.error("线程池异常", e);
        }
        return null;
    }
}
