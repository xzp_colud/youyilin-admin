package com.youyilin.common.enums;

/**
 * 业务操作类型
 */
public enum BusinessTypeEnum {
    /**
     * 其它
     */
    OTHER,

    /**
     * 分页
     */
    PAGE,

    /**
     * 新增
     */
    INSERT,

    /**
     * 删除
     */
    DELETE,

    /**
     * 修改
     */
    UPDATE,

    /**
     * 查询
     */
    SELECT,

    /**
     * 授权
     */
    GRANT,

    /**
     * 导出
     */
    EXPORT,

    /**
     * 导入
     */
    IMPORT,

    /**
     * 审核通过
     */
    CHECK_SUCCESS,

    /**
     * 审核失败
     */
    CHECK_FAIL,

    /**
     * 充值
     */
    CHARGE,

    /**
     * 消费
     */
    CONSUME,

    /**
     * 冻结
     */
    FORCE,

    /**
     * 解冻
     */
    DEFORCE,

    /**
     * 入库
     */
    INTO,

    /**
     * 出库
     */
    OUT,

    /**
     * 上架
     */
    UP,

    /**
     * 下架
     */
    DOWN,

    /**
     * 状态
     */
    STATUS,

    /**
     * 开启
     */
    OPEN,

    /**
     * 关闭
     */
    CLOSE,
}
