package com.youyilin.common.enums;

public enum BooleanEnum {

    TRUE(1, "是"),
    FALSE(0, "否");

    private int code;
    private String info;

    BooleanEnum(int code, String info) {
        this.code = code;
        this.info = info;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public static boolean isTrue(Integer code) {
        return code != null && code == TRUE.getCode();
    }

    /**
     * 验证是否存在
     */
    public static Integer checkCode(Integer code) {
        for (BooleanEnum status : BooleanEnum.values()) {
            if (status.getCode() == code) {
                return status.code;
            }
        }
        return FALSE.getCode();
    }

    public static boolean isYes(Integer code) {
        if (code == null) {
            return false;
        }
        return code == TRUE.code;
    }
}
