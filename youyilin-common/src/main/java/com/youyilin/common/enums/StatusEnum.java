package com.youyilin.common.enums;

/**
 * 基本状态
 */
public enum StatusEnum {

    DISABLED(0, "禁用"),
    NORMAL(1, "正常"),
    ;

    private int code;
    private String info;

    StatusEnum(int code, String info) {
        this.code = code;
        this.info = info;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public static String queryInfoByCode(Integer code) {
        if (code == null) {
            return "";
        }
        if (code == DISABLED.code) {
            return DISABLED.info;
        } else if (code == NORMAL.code) {
            return NORMAL.info;
        } else {
            return "";
        }
    }

    /**
     * 验证是否存在
     */
    public static Integer checkCode(Integer code) {
        for (StatusEnum status : StatusEnum.values()) {
            if (status.getCode() == code) {
                return status.code;
            }
        }
        return DISABLED.getCode();
    }

    public static boolean isNormal(Integer status) {
        return status != null && status == NORMAL.getCode();
    }
}
