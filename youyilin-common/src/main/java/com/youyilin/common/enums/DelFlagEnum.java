package com.youyilin.common.enums;

/**
 * 删除标记
 */
public enum DelFlagEnum {

    DELETE(0, "删除"),
    NORMAL(1, "正常"),;

    private int code;
    private String info;

    DelFlagEnum(int code, String info) {
        this.code = code;
        this.info = info;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
