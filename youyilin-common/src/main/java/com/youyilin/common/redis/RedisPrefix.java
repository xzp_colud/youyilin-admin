package com.youyilin.common.redis;

/**
 * Redis 缓存Key 前缀
 */
public class RedisPrefix {

    // 默认
    public final static String DEFAULT_KEY_PREFIX = "";
    // IP白名单
    public final static String IP_WHITE_KEY_PREFIX = "ip_white:";
    // IP黑名单
    public final static String IP_BLANK_KEY_PREFIX = "ip_blank:";
    // 登录TOKEN
    public final static String TOKEN_MOBILE_KEY_PREFIX = "token_mobile:";
    // 电脑端 登录TOKEN
    public final static String TOKEN_COMPUTER_KEY_PREFIX = "token_computer:";
    // 图形验证码
    public final static String CAPTCHA_KEY_PREFIX = "captcha:";
    // 短信验证码
    public final static String SMS_KEY_PREFIX = "sms:";
    // 省市区
    public final static String ADDRESS_KEY_PREFIX = "address:";
    // 表单重复提交
    public final static String FORM_SUBMIT_KEY_PREFIX = "form_submit:";
}
