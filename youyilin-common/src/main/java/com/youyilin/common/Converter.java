package com.youyilin.common;

public interface Converter<S, T> {

    T convert(S s);
}
