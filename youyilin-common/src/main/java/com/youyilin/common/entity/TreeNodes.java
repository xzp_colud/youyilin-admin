package com.youyilin.common.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * 树
 */
@Data
@Accessors(chain = true)
public class TreeNodes implements Serializable {
    private static final long serialVersionUID = 614045660098397960L;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // value 与 id 相同
    @JsonSerialize(using = ToStringSerializer.class)
    private Long value;
    // 显示标题
    private String title;
    // 上级
    @JsonSerialize(using = ToStringSerializer.class)
    private Long pidId;
    // 禁用
    private boolean disabled = false;
    private boolean selectable = true;
    // 选中
    private boolean checked = false;
    // 图标
    private String icon;
    // 状态
    private Integer status;
    // 子节点
    private List<TreeNodes> children;

    public void setId(Long id) {
        this.id = id;
        this.value = id;
    }
}
