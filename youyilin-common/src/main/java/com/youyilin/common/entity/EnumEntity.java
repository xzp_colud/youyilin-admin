package com.youyilin.common.entity;

import lombok.Data;

@Data
public class EnumEntity {

    private Integer code;
    private String desc;
}
