package com.youyilin.common.entity;

import org.apache.commons.collections.CollectionUtils;

import java.util.List;
import java.util.Map;

/**
 * 构建工具
 */
public class TreeNodesUtils {

    /**
     * 构建树结构
     *
     * @param parentList   上级节点数据
     * @param treeNodesMap 节点数据
     */
    public static void generate(List<TreeNodes> parentList, Map<Long, List<TreeNodes>> treeNodesMap) {
        for (TreeNodes parent : parentList) {
            List<TreeNodes> childrenList = treeNodesMap.get(parent.getId());
            if (CollectionUtils.isEmpty(childrenList)) {
                parent.setChildren(null);
                continue;
            }
            parent.setChildren(childrenList);
            generate(childrenList, treeNodesMap);
        }
    }
}
