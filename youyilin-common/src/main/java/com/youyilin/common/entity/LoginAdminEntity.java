package com.youyilin.common.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;


/**
 * 当前登录的用户
 */
@Data
public class LoginAdminEntity implements UserDetails {
    private static final long serialVersionUID = 7261011977904329883L;

    private Long id;
    /**
     * 登录的TokenId id + userId
     */
    private String createTokenId;
    /**
     * 用户唯一标识
     */
    private String token;
    /**
     * 用户id
     */
    private Long userId;
    /**
     * 用户名
     */
    private String userName;
    /**
     * 角色id
     */
    private String roleIds;
    /**
     * 当前登录所属角色
     */
    private String roleNames;
    /**
     * 头像
     */
    private String avatarUrl;
    /**
     * 客户ID
     */
    private Long customerId;
    /**
     * openid
     */
    private String openid;
    /**
     * 手机号
     */
    private String mobile;
    /**
     * 真实的用户姓名
     */
    private String realName;
    /**
     * 登录时间
     */
    private Date loginDate;
    /**
     * 登录ip
     */
    private String loginIp;
    /**
     * 登录地点
     */
    private String loginLocation;
    /**
     * 过期时间
     */
    private Date expireDate;
//    /**
//     * 菜单
//     */
//    private List menuList;
    /**
     * 登录形式
     */
    private String type;
    /**
     * 是否为移动端登录
     */
    private boolean mobileLogin;
    /**
     * 是否允许多端登录
     */
    private boolean allowMultiLogin;
    /**
     * 权限集合
     */
    private List<String> authList;
    /**
     * 是否允许登录
     */
    private boolean enabled;
    /**
     * 密码
     */
    private String password;

    /**
     * 反序列化的好像没有生效
     */
    @Override
    @JsonDeserialize(using = CustomAuthorityDeserializer.class)
    public Collection<? extends GrantedAuthority> getAuthorities() {
//        return authList.stream().map(item -> new SimpleGrantedAuthority(item.trim())).collect(Collectors.toList());
        return new ArrayList<>();
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return userName;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public boolean isCustomerMini() {
        return StringUtils.isNotBlank(this.openid);
    }
}
