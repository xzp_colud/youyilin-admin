package com.youyilin.common.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 下拉框 Tree
 */
@Data
public class TreeSelect implements Serializable{

    private static final long serialVersionUID = 5049919761098600633L;

    private String key;
    private String value;
    private String label;
    private int level;
    private Integer status;
    private List<TreeSelect> children;
}
