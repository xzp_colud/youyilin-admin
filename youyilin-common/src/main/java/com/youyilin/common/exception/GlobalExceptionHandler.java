package com.youyilin.common.exception;

import com.youyilin.common.bean.RMsg;
import com.youyilin.common.bean.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 全局异常处理
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    /**
     * 实体类检验异常
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public R<String> handler(MethodArgumentNotValidException e) {
        log.error("【实体类检验异常】 {}", e);
//        String errorMsg = RMsg.SERVE_FAIL.getMsg();
        try {
            BindingResult result = e.getBindingResult();
            FieldError fieldError = result.getFieldError();
//            errorMsg = fieldError.getDefaultMessage();
        } catch (Exception ev) {
            log.error("【实体类检验异常】", ev);
        }
        return R.fail(RMsg.SERVE_FAIL.getMsg());
    }

    /**
     * 运行异常
     */
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = RuntimeException.class)
    public R<String> handler(RuntimeException e) {
        log.error("【运行异常】 {}", e);
        return R.fail(RMsg.SERVE_FAIL.getMsg());
    }

    /**
     * 形参异常
     */
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = IllegalArgumentException.class)
    public R<String> handler(IllegalArgumentException e) {
        log.error("【形参异常】 {}", e);
        return R.fail(RMsg.SERVE_FAIL.getMsg());
    }

    /**
     * 异常
     */
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = Exception.class)
    public R<String> handler(Exception e) {
        log.error("【形参异常】 {}", e);
        return R.fail(RMsg.SERVE_FAIL.getMsg());
    }

    /**
     * 自定义异常
     */
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(value = ApiException.class)
    public R<String> handler(ApiException e) {
        log.error("【自定义异常】 {}", e);
        return R.fail(e.getMessage());
    }
}
