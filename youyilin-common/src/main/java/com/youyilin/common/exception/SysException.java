package com.youyilin.common.exception;

public class SysException extends RuntimeException {
    private static final long serialVersionUID = 2071300473553372679L;

    public SysException(Throwable e) {
        super(e.getMessage(), e);
    }

    public SysException(String message) {
        super(message);
    }

    public SysException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
