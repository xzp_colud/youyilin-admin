package com.youyilin.common.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * 登录验证异常
 */
public class LoginValidException extends AuthenticationException {
    private static final long serialVersionUID = -4613391749286986835L;

    public LoginValidException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public LoginValidException(String msg) {
        super(msg);
    }
}
