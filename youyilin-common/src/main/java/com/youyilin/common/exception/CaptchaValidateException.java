package com.youyilin.common.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * 图片验证码异常
 */
public class CaptchaValidateException extends AuthenticationException {

    public CaptchaValidateException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public CaptchaValidateException(String msg) {
        super(msg);
    }
}
