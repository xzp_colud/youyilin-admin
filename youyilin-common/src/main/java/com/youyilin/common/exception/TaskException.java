package com.youyilin.common.exception;

/**
 * 定时任务异常
 */
public class TaskException extends Exception {
    private static final long serialVersionUID = -3853341649167036305L;

    private Code code;

    public TaskException(String message) {
        super(message);
    }

    public TaskException(String msg, Code code) {
        this(msg, code, null);
    }

    public TaskException(String msg, Code code, Exception nestedEx) {
        super(msg, nestedEx);
        this.code = code;
    }

    public Code getCode() {
        return code;
    }

    public enum Code {
        TASK_EXISTS, NO_TASK_EXISTS, TASK_ALREADY_STARTED, UNKNOWN, CONFIG_ERROR, TASK_NODE_NOT_AVAILABLE
    }
}