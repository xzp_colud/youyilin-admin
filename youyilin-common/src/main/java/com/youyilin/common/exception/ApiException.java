package com.youyilin.common.exception;

/**
 * Api 异常
 */
public class ApiException extends RuntimeException {
    private static final long serialVersionUID = -1742678898559965660L;

    public ApiException(Throwable e) {
        super(e.getMessage(), e);
    }

    public ApiException(String message) {
        super(message);
    }

    public ApiException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
