package com.youyilin.common.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * 短信验证码异常
 */
public class SmsCodeValidateException extends AuthenticationException {
    private static final long serialVersionUID = 7972238055871807279L;

    public SmsCodeValidateException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public SmsCodeValidateException(String msg) {
        super(msg);
    }
}
