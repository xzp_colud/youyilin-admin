package com.youyilin.waybill.utils;

import com.youyilin.common.utils.DateUtils;
import org.apache.commons.lang3.RandomStringUtils;

public class SnUtil {

    public final static String WAYBILL_ORDER_PREFIX = "YWBS";

    public static synchronized String getSn() {
        String id = DateUtils.getDate("yyyyMMddHHmmss");
        String randomString = RandomStringUtils.randomNumeric(8);
        return id + randomString;
    }

    public static synchronized String getWaybillSn() {
        return WAYBILL_ORDER_PREFIX + getSn();
    }
}
