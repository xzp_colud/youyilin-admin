package com.youyilin.waybill.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 货运单 发货人
 */
@Data
@Accessors(chain = true)
@TableName("waybill_sender")
public class WaybillSender implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 发件人
     */
    private String name;
    /**
     * 发件电话
     */
    private String mobile;
    /**
     * 发货地址
     */
    private String province;
    private String city;
    private String area;
    private String detail;
    /**
     * 是否为默认
     */
    @TableField(value = "default_flag")
    private Integer defaultFlag;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 逻辑标记
     */
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;
}
