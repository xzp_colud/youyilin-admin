package com.youyilin.waybill.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 货运单子集
 */
@Data
@Accessors(chain = true)
@TableName("waybill_item")
public class WaybillItem implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 货运单ID
     */
    @TableField(value = "waybill_id")
    private Long waybillId;
    /**
     * 源ID
     */
    @TableField(value = "source_id")
    private Long sourceId;
    /**
     * 源单号
     */
    @TableField(value = "source_no")
    private String sourceNo;
    /**
     * 商品ID
     */
    @TableField(value = "product_id")
    private Long productId;
    /**
     * 商品名称
     */
    @TableField(value = "product_name")
    private String productName;
    /**
     * SKU ID
     */
    @TableField(value = "product_price_id")
    private Long productPriceId;
    /**
     * SKU名称
     */
    @TableField(value = "sku_name")
    private String skuName;
    /**
     * 数量
     */
    private BigDecimal num;
    /**
     * 逻辑标记
     */
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;
}
