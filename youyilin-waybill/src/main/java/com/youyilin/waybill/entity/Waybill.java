package com.youyilin.waybill.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.youyilin.common.utils.FormatAmountUtil;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 货运单
 */
@Data
@Accessors(chain = true)
@TableName("waybill")
public class Waybill implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField(value = "create_date", fill = FieldFill.INSERT)
    private Date createDate;
    @TableField(fill = FieldFill.INSERT)
    private Long creator;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 系统单号
     */
    private String sn;
    /**
     * 运费金额
     */
    private Long amount;
    /**
     * 物流公司
     */
    @TableField(value = "express_name")
    private String expressName;
    /**
     * 物流单号
     */
    @TableField(value = "express_no")
    private String expressNo;
    /**
     * 发货人
     */
    @TableField(value = "sender_name")
    private String senderName;
    /**
     * 发货电话
     */
    @TableField(value = "sender_mobile")
    private String senderMobile;
    /**
     * 发货地址
     */
    @TableField(value = "sender_province")
    private String senderProvince;
    @TableField(value = "sender_city")
    private String senderCity;
    @TableField(value = "sender_area")
    private String senderArea;
    /**
     * 收货人
     */
    @TableField(value = "sender_detail")
    private String senderDetail;
    /**
     * 收货电话
     */
    @TableField(value = "receive_name")
    private String receiveName;
    /**
     * 收货地址
     */
    @TableField(value = "receive_mobile")
    private String receiveMobile;
    @TableField(value = "receive_province")
    private String receiveProvince;
    @TableField(value = "receive_city")
    private String receiveCity;
    @TableField(value = "receive_area")
    private String receiveArea;
    @TableField(value = "receive_detail")
    private String receiveDetail;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 子母件类型
     */
    private Integer type;
    /**
     * 付款方式
     */
    @TableField(value = "pay_type")
    private Integer payType;
    /**
     * 母件ID
     */
    @TableField(value = "parent_id")
    private Long parentId;
    /**
     * 确认时间
     */
    @TableField(value = "sure_date")
    private Date sureDate;
    /**
     * 发货时间
     */
    @TableField(value = "send_date")
    private String sendDate;
    /**
     * 收件时间
     */
    @TableField(value = "receive_date")
    private Date receiveDate;
    /**
     * 打印次数
     */
    @TableField(value = "print_num")
    private Integer printNum;
    /**
     * 来源类型
     */
    @TableField(value = "source_type")
    private String sourceType;
    /**
     * 逻辑标记
     */
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;

    public String getAmountText() {
        return FormatAmountUtil.format(this.amount);
    }

    public enum Type {
        MOTHER(1, "母件"),
        CHILDREN(2, "子件");

        private Integer code;
        private String desc;

        Type(Integer code, String desc) {
            this.code = code;
            this.desc = desc;
        }

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }
    }
}
