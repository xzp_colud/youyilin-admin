package com.youyilin.waybill.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.exception.Assert;
import com.youyilin.waybill.entity.Waybill;
import com.youyilin.waybill.enums.WaybillStatusEnum;
import com.youyilin.waybill.mapper.WaybillMapper;
import com.youyilin.waybill.service.WaybillService;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class WaybillImpl extends ServiceImpl<WaybillMapper, Waybill> implements WaybillService {

    @Override
    public Waybill validateWaybill(Long id) {
        Waybill waybill = super.getById(id);
        Assert.notNull(waybill, "运单不存在");

        return waybill;
    }

    private Waybill validateStatus(Long id, Integer status) {
        Waybill waybill = this.validateWaybill(id);
        boolean statusFlag = waybill.getStatus() != null && waybill.getStatus().equals(status);
        Assert.isTrue(statusFlag, "运单状态异常");

        return waybill;
    }

    @Override
    public Waybill validateConfirm(Long id) {
        return validateStatus(id, WaybillStatusEnum.DEFAULT.getCode());
    }

    @Override
    public Waybill validateInfo(Long id) {
        return validateStatus(id, WaybillStatusEnum.CONFIRM.getCode());
    }

    @Override
    public void updateConfirm(Long id) {
        super.update(new LambdaUpdateWrapper<Waybill>()
                .set(Waybill::getSureDate, new Date())
                .set(Waybill::getStatus, WaybillStatusEnum.CONFIRM.getCode())
                .eq(Waybill::getId, id));
    }
}
