package com.youyilin.waybill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.waybill.entity.WaybillSender;

import java.util.List;

public interface WaybillSenderService extends IService<WaybillSender> {

    /**
     * 所有正常的
     */
    List<WaybillSender> listAllNormal();

    /**
     * 验证是否存在
     *
     * @param id 主键ID
     * @return WaybillSender
     */
    WaybillSender validateSender(Long id);

    /**
     * 验证状态
     *
     * @param id 主键ID
     * @return WaybillSender
     */
    WaybillSender validateSenderStatus(Long id);

    /**
     * 取消默认
     */
    void updateDefaultFalse(Long id);
}
