package com.youyilin.waybill.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.enums.BooleanEnum;
import com.youyilin.common.enums.StatusEnum;
import com.youyilin.common.exception.Assert;
import com.youyilin.waybill.entity.WaybillSender;
import com.youyilin.waybill.mapper.WaybillSenderMapper;
import com.youyilin.waybill.service.WaybillSenderService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WaybillSenderImpl extends ServiceImpl<WaybillSenderMapper, WaybillSender> implements WaybillSenderService {

    @Override
    public List<WaybillSender> listAllNormal() {
        return super.list(new LambdaQueryWrapper<WaybillSender>()
                .eq(WaybillSender::getStatus, StatusEnum.NORMAL.getCode())
                .orderByDesc(WaybillSender::getDefaultFlag)
                .orderByDesc(WaybillSender::getId));
    }

    @Override
    public WaybillSender validateSender(Long id) {
        WaybillSender sender = super.getById(id);
        Assert.notNull(sender, "发件地址不存在");

        return sender;
    }

    @Override
    public WaybillSender validateSenderStatus(Long id) {
        WaybillSender sender = this.validateSender(id);
        boolean isTrue = sender.getStatus() != null && sender.getStatus().equals(StatusEnum.NORMAL.getCode());
        Assert.isTrue(isTrue, "发件地址已禁用");

        return sender;
    }

    @Override
    public void updateDefaultFalse(Long id) {
        super.update(new LambdaUpdateWrapper<WaybillSender>()
                .set(WaybillSender::getDefaultFlag, BooleanEnum.FALSE.getCode()));
    }
}
