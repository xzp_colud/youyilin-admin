package com.youyilin.waybill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.waybill.entity.WaybillItem;

import java.util.List;

public interface WaybillItemService extends IService<WaybillItem> {

    /**
     * 按货运单查询
     *
     * @return ArrayList
     */
    List<WaybillItem> listByWaybillId(Long waybillId);
}
