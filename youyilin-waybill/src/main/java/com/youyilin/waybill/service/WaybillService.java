package com.youyilin.waybill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.waybill.entity.Waybill;

public interface WaybillService extends IService<Waybill> {

    /**
     * 验证货运单是否存在
     *
     * @param id 货运单ID
     * @return Waybill
     */
    Waybill validateWaybill(Long id);

    /**
     * 验证是否能确认
     *
     * @param id 货运单ID
     * @return Waybill
     */
    Waybill validateConfirm(Long id);

    /**
     * 验证是否能提交
     *
     * @param id 货运单ID
     * @return Waybill
     */
    Waybill validateInfo(Long id);

    /**
     * 更新状态为已确认
     *
     * @param id 货运单ID
     */
    void updateConfirm(Long id);
}
