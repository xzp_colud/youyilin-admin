package com.youyilin.waybill.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.waybill.mapper.WaybillItemMapper;
import com.youyilin.waybill.entity.WaybillItem;
import com.youyilin.waybill.service.WaybillItemService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WaybillItemImpl extends ServiceImpl<WaybillItemMapper, WaybillItem> implements WaybillItemService {

    @Override
    public List<WaybillItem> listByWaybillId(Long waybillId) {
        return super.list(new LambdaQueryWrapper<WaybillItem>().eq(WaybillItem::getWaybillId, waybillId));
    }
}
