package com.youyilin.waybill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.common.bean.Page;
import com.youyilin.waybill.dto.sender.WaybillSenderPageQueryDTO;
import com.youyilin.waybill.entity.WaybillSender;
import com.youyilin.waybill.vo.sender.WaybillSenderPageVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface WaybillSenderMapper extends BaseMapper<WaybillSender> {

    Integer getTotal(Page<WaybillSenderPageQueryDTO> page);

    List<WaybillSenderPageVO> getPage(Page<WaybillSenderPageQueryDTO> page);
}
