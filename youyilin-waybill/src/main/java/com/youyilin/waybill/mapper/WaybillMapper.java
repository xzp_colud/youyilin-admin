package com.youyilin.waybill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.common.bean.Page;
import com.youyilin.waybill.dto.waybill.WaybillPageQueryDTO;
import com.youyilin.waybill.entity.Waybill;
import com.youyilin.waybill.vo.waybill.WaybillPageVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface WaybillMapper extends BaseMapper<Waybill> {

    Integer getTotal(Page<WaybillPageQueryDTO> page);

    List<WaybillPageVO> getPage(Page<WaybillPageQueryDTO> page);
}
