package com.youyilin.waybill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.waybill.entity.WaybillItem;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface WaybillItemMapper extends BaseMapper<WaybillItem> {
}
