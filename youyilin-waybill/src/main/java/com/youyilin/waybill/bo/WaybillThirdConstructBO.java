package com.youyilin.waybill.bo;

import com.youyilin.waybill.dto.waybill.WaybillItemThirdConstructDTO;
import com.youyilin.waybill.entity.Waybill;
import com.youyilin.waybill.entity.WaybillItem;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 货运单构建BO
 * 有内部订单转换而来
 */
@Data
@Accessors(chain = true)
public class WaybillThirdConstructBO {

    private String sourceType;
    private String receiveName;
    private String receiveMobile;
    private String receiveProvince;
    private String receiveCity;
    private String receiveArea;
    private String receiveDetail;
    private List<WaybillItemThirdConstructDTO> itemSourceList;

    // ----- 保存数据 ----- //
    private Long id;
    private String sn;
    private Waybill waybill;
    private List<WaybillItem> itemList;
}
