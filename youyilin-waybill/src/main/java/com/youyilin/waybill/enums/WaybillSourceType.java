package com.youyilin.waybill.enums;

public enum WaybillSourceType {
    DEFAULT,
    CUSTOMER_ORDER,
    SALE_ORDER,
}
