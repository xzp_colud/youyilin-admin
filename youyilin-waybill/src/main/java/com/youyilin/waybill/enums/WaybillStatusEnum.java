package com.youyilin.waybill.enums;

/**
 * 订单状态枚举
 */
public enum WaybillStatusEnum {

    DEFAULT(0, "未确认"),
    FINISH(10, "已发货"),
    CONFIRM(20, "已确认");

    private int code;
    private String info;

    WaybillStatusEnum(int code, String info) {
        this.code = code;
        this.info = info;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public static String queryInfoByCode(Integer code) {
        if (code != null) {
            for (WaybillStatusEnum t : WaybillStatusEnum.values()) {
                if (t.getCode() == code) {
                    return t.getInfo();
                }
            }
        }
        return null;
    }
}
