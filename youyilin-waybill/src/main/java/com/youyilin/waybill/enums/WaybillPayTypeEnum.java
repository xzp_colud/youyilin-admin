package com.youyilin.waybill.enums;

/**
 * 结款方式类型
 */
public enum WaybillPayTypeEnum {
    YUE_JIE(1, "月结"),
    JI_FU(2, "寄付"),
    DAO_FU(3, "到付");

    private Integer code;
    private String desc;

    WaybillPayTypeEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
