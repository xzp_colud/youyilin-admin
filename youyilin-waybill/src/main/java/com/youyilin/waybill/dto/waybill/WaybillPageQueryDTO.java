package com.youyilin.waybill.dto.waybill;

import lombok.Data;

/**
 * 货运单列表查询参数DTO
 */
@Data
public class WaybillPageQueryDTO {

    // 系统编号
    private String sn;
    // 物流名称
    private String expressName;
    // 物流单号
    private String expressNo;
    // 收货人
    private String receiveName;
    // 收货电话
    private String receiveMobile;
}
