package com.youyilin.waybill.dto.sender;

import com.youyilin.common.utils.BeanHelper;
import com.youyilin.waybill.entity.WaybillSender;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 发件人表单DTO
 */
@Data
public class WaybillSenderFormDTO {

    private Long id;
    @NotBlank(message = "发件人不能为空")
    private String name;
    @NotBlank(message = "发件电话不能为空")
    private String mobile;
    @NotBlank(message = "发件地址不能为空")
    private String province;
    @NotBlank(message = "发件地址不能为空")
    private String city;
    @NotBlank(message = "发件地址不能为空")
    private String area;
    @NotBlank(message = "发件地址不能为空")
    private String detail;
    private Integer defaultFlag;
    @NotNull(message = "状态不能为空")
    private Integer status;

    public WaybillSender convertToEntity() {
        return BeanHelper.map(this, WaybillSender.class);
    }
}
