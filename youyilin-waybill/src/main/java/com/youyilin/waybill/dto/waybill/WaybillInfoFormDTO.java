package com.youyilin.waybill.dto.waybill;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * 货运单提交DTO
 */
@Data
@Accessors(chain = true)
public class WaybillInfoFormDTO {

    @NotNull(message = "货运单不能为空")
    private Long id;
    @NotNull(message = "发件人不能为空")
    private Long senderId;
    @NotNull(message = "金额不能为空")
    private BigDecimal price;
    @NotBlank(message = "物流名称不能为空")
    private String expressName;
    @NotBlank(message = "物流单号不能为空")
    private String expressNo;
    @NotBlank(message = "发货时间不能为空")
    private String sendDate;
    @NotNull(message = "付款方式不能为空")
    private Integer payType;
}
