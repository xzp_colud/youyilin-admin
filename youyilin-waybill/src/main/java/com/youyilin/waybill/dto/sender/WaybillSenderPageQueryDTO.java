package com.youyilin.waybill.dto.sender;

import lombok.Data;

/**
 * 发件人列表查询参数DTO
 */
@Data
public class WaybillSenderPageQueryDTO {

    private String name;
    private String mobile;
}
