package com.youyilin.waybill.dto.waybill;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * 货运单确认DTO
 */
@Data
@Accessors(chain = true)
public class WaybillConfirmFormDTO {

    @NotNull(message = "货运单不能为空")
    private Long id;
}
