package com.youyilin.waybill.dto.waybill;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * 货运单明细源数据
 */
@Data
@Accessors(chain = true)
public class WaybillItemThirdConstructDTO {

    private Long sourceId;
    private String sourceNo;
    private Long productId;
    private String productName;
    private Long productPriceId;
    private String skuName;
    private BigDecimal num;
}
