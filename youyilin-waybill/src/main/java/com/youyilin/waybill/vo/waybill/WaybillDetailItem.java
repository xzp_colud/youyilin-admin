package com.youyilin.waybill.vo.waybill;

import com.youyilin.common.utils.BeanHelper;
import com.youyilin.waybill.entity.WaybillItem;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.List;

/**
 * 货运单明细
 */
@Data
@Accessors(chain = true)
public class WaybillDetailItem {

    // 源单号
    private String sourceNo;
    // 商品名称
    private String productName;
    // SKU名称
    private String skuName;
    // 数量
    private BigDecimal num;

    public static List<WaybillDetailItem> convertByEntity(List<WaybillItem> list) {
        return BeanHelper.map(list, WaybillDetailItem.class);
    }
}
