package com.youyilin.waybill.vo.sender;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.waybill.entity.WaybillSender;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 发件人列表VO
 */
@Data
@Accessors(chain = true)
public class WaybillSenderPageVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    private String name;
    private String mobile;
    private String province;
    private String city;
    private String area;
    private String detail;
    private Integer defaultFlag;
    private Integer status;

    public static List<WaybillSenderPageVO> convertByEntity(List<WaybillSender> list) {
        return BeanHelper.map(list, WaybillSenderPageVO.class);
    }
}
