package com.youyilin.waybill.vo.sender;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.waybill.entity.WaybillSender;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 发件人编辑VO
 */
@Data
@Accessors(chain = true)
public class WaybillSenderEditVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    private String name;
    private String mobile;
    private String province;
    private String city;
    private String area;
    private String detail;
    private Integer defaultFlag;
    private Integer status;

    public static WaybillSenderEditVO convertByEntity(WaybillSender sender) {
        return BeanHelper.map(sender, WaybillSenderEditVO.class);
    }
}
