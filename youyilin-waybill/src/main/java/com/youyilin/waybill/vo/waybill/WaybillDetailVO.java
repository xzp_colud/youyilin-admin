package com.youyilin.waybill.vo.waybill;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 货运单详情VO
 */
@Data
@Accessors(chain = true)
public class WaybillDetailVO {

    // 货运单
    private WaybillDetail waybill;
    // 明细
    private List<WaybillDetailItem> itemList;
}
