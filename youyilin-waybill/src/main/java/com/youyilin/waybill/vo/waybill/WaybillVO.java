package com.youyilin.waybill.vo.waybill;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.waybill.entity.Waybill;
import lombok.Data;

import java.util.Date;

/**
 * 货运单VO
 */
@Data
public class WaybillVO {


    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 创建时间
    private Date createDate;
    // 系统单号
    private String sn;
    // 运费金额
    private Long amount;
    // 物流公司
    private String expressName;
    // 物流单号
    private String expressNo;
    // 发货人
    private String senderName;
    // 发货电话
    private String senderMobile;
    // 发货地址
    private String senderProvince;
    private String senderCity;
    private String senderArea;
    private String senderDetail;
    // 收货人
    private String receiveName;
    // 收货电话
    private String receiveMobile;
    // 收货地址
    private String receiveProvince;
    private String receiveCity;
    private String receiveArea;
    private String receiveDetail;
    // 状态
    private Integer status;
    // 付款方式
    private Integer payType;
    // 确认时间
    private Date sureDate;
    // 发件时间
    private String sendDate;
    // 收件时间
    private Date receiveDate;
    // 来源类型
    private String sourceType;

    public static WaybillVO convertByEntity(Waybill waybill) {
        return BeanHelper.map(waybill, WaybillVO.class);
    }
}
