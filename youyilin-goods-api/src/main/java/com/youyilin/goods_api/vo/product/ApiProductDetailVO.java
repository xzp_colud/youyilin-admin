package com.youyilin.goods_api.vo.product;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 单个查询商品 DTO
 */
@Data
@Accessors(chain = true)
public class ApiProductDetailVO {

    // 商品
    private ApiProductDetail product;
    // 参数
    private List<ApiProductDetailParams> paramsList;
    // 轮播图
    private List<String> imageList;
    // 规格
    private List<ApiProductDetailUnit> unitList;
    // 规格-SKU
    private List<ApiProductDetailUnitPrice> unitSkuList;
    // SKU
    private List<ApiProductDetailPrice> skuList;
    // 描述文案
    private String description;
}
