package com.youyilin.goods_api.vo.product;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.FormatAmountUtil;
import lombok.Data;

@Data
public class ApiProductDetailPrice {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    private Integer sellPrice;
    private Integer inventory;
    private String skuName;

    public String getSellPriceText() {
        return FormatAmountUtil.format(this.sellPrice);
    }
}
