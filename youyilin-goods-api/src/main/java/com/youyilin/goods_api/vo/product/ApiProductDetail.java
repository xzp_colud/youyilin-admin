package com.youyilin.goods_api.vo.product;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

@Data
public class ApiProductDetail {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 名称
    private String name;
    // 主图
    private String imageUrl;
    // 外侧销售价
    private String priceText;
}
