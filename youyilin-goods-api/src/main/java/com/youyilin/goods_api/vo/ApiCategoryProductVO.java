package com.youyilin.goods_api.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.goods_api.vo.product.ApiProductDetail;
import lombok.Data;

import java.util.List;

/**
 * 单页面 按分类显示商品
 */
@Data
public class ApiCategoryProductVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 分类
    private String title;
    // 图片
    private String imageUrl;
    // 商品
    private List<ApiProductDetail> itemList;
}
