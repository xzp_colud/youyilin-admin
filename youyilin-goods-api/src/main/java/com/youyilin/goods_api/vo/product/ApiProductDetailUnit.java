package com.youyilin.goods_api.vo.product;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.util.List;

@Data
public class ApiProductDetailUnit {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    private String name;

    private List<ApiProductDetailUnitData> itemList;
}
