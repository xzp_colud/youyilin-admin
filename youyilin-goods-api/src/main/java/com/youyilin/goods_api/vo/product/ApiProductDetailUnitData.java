package com.youyilin.goods_api.vo.product;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

@Data
public class ApiProductDetailUnitData {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    private String value;
}
