package com.youyilin.goods_api.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

@Data
public class ApiCategoryVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 分类
    private String title;
    // 图片
    private String imageUrl;
}
