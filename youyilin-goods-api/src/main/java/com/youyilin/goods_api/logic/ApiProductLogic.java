package com.youyilin.goods_api.logic;

import com.youyilin.goods_api.vo.product.ApiProductDetail;
import com.youyilin.goods_api.vo.product.ApiProductDetailVO;

import java.util.List;

public interface ApiProductLogic {

    /**
     * 列表
     *
     * @param current     当前页
     * @param pageSize    每页数量
     * @param frontMenuId 菜单ID
     * @param name        商品名称
     * @return ArrayList
     */
    List<ApiProductDetail> page(Integer current, Integer pageSize, Long frontMenuId, String name);

    /**
     * 商品详情
     *
     * @param productId 商品ID
     * @return ApiProductDetailVO
     */
    ApiProductDetailVO getProduct(Long productId);
}
