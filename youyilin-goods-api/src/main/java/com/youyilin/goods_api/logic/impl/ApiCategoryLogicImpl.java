package com.youyilin.goods_api.logic.impl;

import com.youyilin.common.enums.StatusEnum;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.goods.entity.Category;
import com.youyilin.goods.entity.Product;
import com.youyilin.goods.service.CategoryService;
import com.youyilin.goods.service.ProductService;
import com.youyilin.goods_api.vo.product.ApiProductDetail;
import com.youyilin.goods_api.vo.ApiCategoryProductVO;
import com.youyilin.goods_api.vo.ApiCategoryVO;
import com.youyilin.goods_api.logic.ApiCategoryLogic;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class ApiCategoryLogicImpl implements ApiCategoryLogic {

    private final CategoryService categoryService;
    private final ProductService productService;

    public ApiCategoryLogicImpl(CategoryService categoryService, ProductService productService) {
        this.categoryService = categoryService;
        this.productService = productService;
    }

    @Override
    public List<ApiCategoryVO> listCategory() {
        List<Category> categoryList = categoryService.listAll(StatusEnum.NORMAL.getCode());
        if (CollectionUtils.isEmpty(categoryList)) {
            return new ArrayList<>();
        }
        return BeanHelper.map(categoryList, ApiCategoryVO.class);
    }

    @Override
    public List<ApiCategoryProductVO> listCategoryProduct() {
        List<Category> categoryList = categoryService.listAll(StatusEnum.NORMAL.getCode());
        if (CollectionUtils.isEmpty(categoryList)) {
            return new ArrayList<>();
        }
        List<ApiCategoryProductVO> resultList = BeanHelper.map(categoryList, ApiCategoryProductVO.class);
        Map<Long, List<Product>> productMap = productService.mapByCategory();

        for (ApiCategoryProductVO vo : resultList) {
            List<Product> productList = productMap.get(vo.getId());
            if (CollectionUtils.isEmpty(productList)) continue;

            vo.setItemList(BeanHelper.map(productList, ApiProductDetail.class));
        }

        return resultList;
    }
}
