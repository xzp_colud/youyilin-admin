package com.youyilin.goods_api.logic;

import com.youyilin.goods_api.vo.ApiCategoryProductVO;
import com.youyilin.goods_api.vo.ApiCategoryVO;

import java.util.List;

public interface ApiCategoryLogic {

    List<ApiCategoryVO> listCategory();

    List<ApiCategoryProductVO> listCategoryProduct();
}
