package com.youyilin.goods_api.logic.impl;

import com.youyilin.common.bean.Page;
import com.youyilin.common.enums.StatusEnum;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.goods.entity.Product;
import com.youyilin.goods.entity.ProductPrice;
import com.youyilin.goods.entity.ProductUnitPrice;
import com.youyilin.goods.model.dto.FrontMenuProductDto;
import com.youyilin.goods.model.dto.UnitDto;
import com.youyilin.goods.service.*;
import com.youyilin.goods_api.vo.product.ApiProductDetail;
import com.youyilin.goods_api.vo.product.ApiProductDetailPrice;
import com.youyilin.goods_api.vo.product.ApiProductDetailUnit;
import com.youyilin.goods_api.vo.product.ApiProductDetailUnitPrice;
import com.youyilin.goods_api.vo.product.ApiProductDetailVO;
import com.youyilin.goods_api.logic.ApiProductLogic;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ApiProductLogicImpl implements ApiProductLogic {

    private final FrontMenuProductService frontMenuProductService;
    private final ProductService productService;
    private final UnitService unitService;
    private final ProductPriceService productPriceService;
    private final ProductUnitPriceService productUnitPriceService;
    private final ProductDescriptionService productDescriptionService;

    public ApiProductLogicImpl(FrontMenuProductService frontMenuProductService, ProductService productService, UnitService unitService,
                               ProductPriceService productPriceService, ProductUnitPriceService productUnitPriceService,
                               ProductDescriptionService productDescriptionService) {
        this.frontMenuProductService = frontMenuProductService;
        this.productService = productService;
        this.unitService = unitService;
        this.productPriceService = productPriceService;
        this.productUnitPriceService = productUnitPriceService;
        this.productDescriptionService = productDescriptionService;
    }

    @Override
    public List<ApiProductDetail> page(Integer current, Integer pageSize, Long frontMenuId, String name) {
        Page<FrontMenuProductDto> page = new Page<>();
        page.setPageNum(current);
        page.setPageSize(pageSize);
        FrontMenuProductDto query = new FrontMenuProductDto();
        query.setFrontMenuId(frontMenuId);
        query.setProductName(name);
        query.setStatus(StatusEnum.NORMAL.getCode());
        page.setSearch(query);

        List<FrontMenuProductDto> list = frontMenuProductService.getPage(page);
        if (CollectionUtils.isEmpty(list)) return new ArrayList<>();

        return list.stream().map(item -> {
            ApiProductDetail queryProduct = new ApiProductDetail();
            queryProduct.setId(item.getProductId());
            queryProduct.setImageUrl(item.getImage());
            queryProduct.setName(item.getProductName());
            queryProduct.setPriceText(item.getPriceText());

            return queryProduct;
        }).collect(Collectors.toList());
    }

    @Override
    public ApiProductDetailVO getProduct(Long productId) {
        ApiProductDetailVO vo = new ApiProductDetailVO();

        Product product = productService.getById(productId);
        if (product == null) return vo;

        ApiProductDetail productSearch = BeanHelper.map(product, ApiProductDetail.class);
        vo.setProduct(productSearch);
        this.initProductUnit(vo);
        this.initUnitSku(vo);
        this.initSku(vo);
        vo.setDescription(productDescriptionService.getByProductId(productId));

        return vo;
    }

    /**
     * 销售方式
     */
    private void initProductUnit(ApiProductDetailVO vo) {
        List<UnitDto> unitDtoList = unitService.listForDetail(vo.getProduct().getId());
        if (CollectionUtils.isNotEmpty(unitDtoList)) {
            vo.setUnitList(BeanHelper.map(unitDtoList, ApiProductDetailUnit.class));
        }
    }

    /**
     * 商品 SKU路径
     */
    private void initUnitSku(ApiProductDetailVO vo) {
        List<ProductUnitPrice> unitPriceList = productUnitPriceService.listByProductId(vo.getProduct().getId());
        if (CollectionUtils.isNotEmpty(unitPriceList)) {
            List<ApiProductDetailUnitPrice> pupList = BeanHelper.map(unitPriceList, ApiProductDetailUnitPrice.class);
            vo.setUnitSkuList(pupList);
        }
    }

    /**
     * 商品 价格
     */
    private void initSku(ApiProductDetailVO vo) {
        List<ProductPrice> priceList = productPriceService.listByProductId(vo.getProduct().getId());
        if (CollectionUtils.isNotEmpty(priceList)) {
            vo.setSkuList(BeanHelper.map(priceList, ApiProductDetailPrice.class));
        }
    }
}
