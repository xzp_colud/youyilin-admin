package com.youyilin.runner;

import com.youyilin.system.service.AddressService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * 地理 添加至Redis
 */
@Slf4j
@Component
public class AddressToRedisRunner implements CommandLineRunner {

    private final AddressService addressService;

    public AddressToRedisRunner(AddressService addressService) {
        this.addressService = addressService;
    }

    @Override
    public void run(String... strings) throws Exception {
        log.info("----- Spring Boot 启动初始化 将省市区添加至Redis start -----");
        addressService.initToRedis();
        log.info("----- Spring Boot 启动初始化 将省市区添加至Redis end -----");
    }
}
