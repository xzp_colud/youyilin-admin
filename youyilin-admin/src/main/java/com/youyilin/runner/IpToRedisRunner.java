package com.youyilin.runner;

import com.youyilin.system.logic.SysIpLogic;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * ip黑名单添加至REDIS
 */
@Slf4j
@Component
public class IpToRedisRunner implements CommandLineRunner {

    private final SysIpLogic sysIpLogic;

    public IpToRedisRunner(SysIpLogic sysIpLogic) {
        this.sysIpLogic = sysIpLogic;
    }

    @Override
    public void run(String... strings) throws Exception {
        log.info("----- Spring Boot 启动初始化 将IP添加至Redis start -----");
        sysIpLogic.initToRedis();
        log.info("----- Spring Boot 启动初始化 将IP添加至Redis end -----");
    }
}
