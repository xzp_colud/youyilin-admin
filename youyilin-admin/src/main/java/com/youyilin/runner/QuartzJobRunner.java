package com.youyilin.runner;

import com.youyilin.quartz.constant.ScheduleConstants;
import com.youyilin.quartz.model.entity.SysJob;
import com.youyilin.quartz.service.SysJobService;
import com.youyilin.quartz.util.ScheduleUtils;
import lombok.extern.slf4j.Slf4j;
import org.quartz.Scheduler;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 项目启动时，初始化定时器 主要是防止手动修改数据库导致未同步到定时任务处理（注：不能手动修改数据库ID和任务组名，否则会导致脏数据）
 */
@Slf4j
@Component
public class QuartzJobRunner implements CommandLineRunner {

    private final Scheduler scheduler;
    private final SysJobService jobService;

    public QuartzJobRunner(Scheduler scheduler, SysJobService jobService) {
        this.scheduler = scheduler;
        this.jobService = jobService;
    }

    @Override
    public void run(String... strings) throws Exception {
        log.info("----- Spring Boot 启动初始化 将定时任务初始化 start -----");
        scheduler.clear();
        List<SysJob> jobList = jobService.listAllJob();
        for (SysJob job : jobList) {
            ScheduleUtils.createScheduleJob(scheduler, job);
            if (job.getStatus().equals(ScheduleConstants.Status.NORMAL.getValue()) && job.getNextTime() > 0) {
                if (job.getNextTime() < System.currentTimeMillis()) {
                    jobService.updateSysJobRun(job.getId());
                }
            }
        }
        log.info("----- Spring Boot 启动初始化 将定时任务初始化 end -----");
    }
}
