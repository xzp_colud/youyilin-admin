package com.youyilin.runner;

import com.youyilin.system.logic.SysConfigLogic;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

/**
 * spring boot 启动初始化 不建议使用
 * <p>
 * 注解@PostConstruct 也不建议使用
 * 直接在启动方法 Main 之后添加 也不建议
 */
@Slf4j
@Component
public class InitSysConfig implements InitializingBean {

    private final SysConfigLogic sysConfigLogic;

    public InitSysConfig(SysConfigLogic sysConfigLogic) {
        this.sysConfigLogic = sysConfigLogic;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        log.info("----- Spring Boot 启动初始化 测试 -----");
        sysConfigLogic.initRefreshConfig();
    }
}
