package com.youyilin.framework.enums;

/**
 * 系统体系
 */
public enum SysSignEnum {

    BEFORE(0, "前端"),
    AFTER(1, "后端"),;

    private int code;
    private String info;

    SysSignEnum(int code, String info) {
        this.code = code;
        this.info = info;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
