package com.youyilin.framework.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

/**
 * spring security 配置
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig {

    /**
     * 白名单
     */
    public static String[] URL_WHITELIST = {
            "/",
            "/**.js",
            "/**.css",
            "/**.txt",                     // txt文件
            "/**.html",                    // html文件
            "/apidoc/**",                  // api文档
            "/login",                      // 账号密码登录
            "/login/phone",                // 手机短信登录
            "/miniLogin",                  // 微信小程序登录
            "/logout",                     // 登出
            "/captchaImage",               // 图片验证码
            "/smsCode/**",                 // 短信验证码
            "/address/**",                 // 省市区
            "/api/**",                     // api
            "/website/**", // Image 文件
            "/favicon.ico",
    };

//    // 配置登录
//    public static AdminLoginWebAdapter AdminLoginWebAdapter;
//    public static AdminPhoneLoginWebAdapter adminPhoneLoginWebAdapter;
//    public static MemberLoginWebAdapter memberLoginWebAdapter;
}
