package com.youyilin.framework.config;

import com.youyilin.framework.interceptor.RepeatInterceptor;
import com.youyilin.framework.interceptor.TimestampInterceptor;
import com.youyilin.framework.interceptor.IpInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * WEB配置
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    private final TimestampInterceptor timestampInterceptor;
    private final IpInterceptor ipInterceptor;
    private final RepeatInterceptor repeatInterceptor;

    // URL 白名单
    private static final String[] URL_WHITELIST = {
            "/",         // 首页
            "/txt/**",   // Txt 文件
            "/html/**",  // Html 文件
            "/js/**",    // Js 文件
            "/image/**", // Image 文件
            "/error",
    };

    @Autowired
    public WebMvcConfig(TimestampInterceptor timestampInterceptor, IpInterceptor ipInterceptor,
                        RepeatInterceptor repeatInterceptor) {
        this.timestampInterceptor = timestampInterceptor;
        this.ipInterceptor = ipInterceptor;
        this.repeatInterceptor = repeatInterceptor;
    }

    /**
     * 首页配置
     */
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("forward:/index.html");
        registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
        WebMvcConfigurer.super.addViewControllers(registry);
    }

    /**
     * 跨域配置
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOriginPatterns("*")
                .allowedMethods("GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS")
                .allowCredentials(true)
                .maxAge(3600)
                .allowedHeaders("*");
    }

    /**
     * Interceptor配置
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
//        registry.addInterceptor(apiSignValidateInterceptor)
//                .addPathPatterns("/api/**")
//                .excludePathPatterns(URL_WHITELIST);

        registry.addInterceptor(timestampInterceptor).addPathPatterns("/**").excludePathPatterns(URL_WHITELIST);

        registry.addInterceptor(ipInterceptor).addPathPatterns("/**").excludePathPatterns(URL_WHITELIST);

        registry.addInterceptor(repeatInterceptor).addPathPatterns("/**").excludePathPatterns(URL_WHITELIST);
    }

    private CorsConfiguration buildConfig() {
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        corsConfiguration.setAllowCredentials(true);
        /* 允许跨域的地址 */
        corsConfiguration.addAllowedOriginPattern("*");
        /* 跨域的请求头 */
        corsConfiguration.addAllowedHeader("*");
        /* 跨域的请求方法 */
        corsConfiguration.addAllowedMethod("*");
        corsConfiguration.addExposedHeader("Authorization");

        return corsConfiguration;
    }

    @Bean
    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        /* 配置可以访问的地址 */
        source.registerCorsConfiguration("/**", buildConfig());
        return new CorsFilter(source);
    }
}