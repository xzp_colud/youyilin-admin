package com.youyilin.framework.aspectj;

import com.alibaba.fastjson.JSONObject;
import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import com.youyilin.common.exception.ApiException;
import com.youyilin.common.utils.ExceptionUtil;
import com.youyilin.common.utils.SecurityUtil;
import com.youyilin.common.utils.ServletUtils;
import com.youyilin.system.entity.SysErrorLog;
import com.youyilin.system.entity.SysOptLog;
import com.youyilin.system.entity.SysUser;
import com.youyilin.system.enums.ErrorTypeEnum;
import com.youyilin.system.logic.SysNoticeUserReadLogic;
import com.youyilin.system.service.SysErrorLogService;
import com.youyilin.system.service.SysNoticeService;
import com.youyilin.system.service.SysOptLogService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 全局日志
 */
@Slf4j
@Aspect
@Component
@Order(11)
public class LogAspect {

    private final SysOptLogService sysLoginOptService;
    private final SysErrorLogService sysErrorLogService;

    private final SysNoticeService sysNoticeService;
    private final SysNoticeUserReadLogic sysNoticeUserReadLogic;

    @Autowired
    public LogAspect(SysOptLogService sysLoginOptService, SysErrorLogService sysErrorLogService,
                     SysNoticeService sysNoticeService, SysNoticeUserReadLogic sysNoticeUserReadLogic) {
        this.sysLoginOptService = sysLoginOptService;
        this.sysErrorLogService = sysErrorLogService;
        this.sysNoticeService = sysNoticeService;
        this.sysNoticeUserReadLogic = sysNoticeUserReadLogic;
    }

    @Pointcut("@annotation(com.youyilin.common.annotation.Log)")
    public void methodCachePointcut() {

    }

    @Around(value = "methodCachePointcut()")
    public Object around(ProceedingJoinPoint point) {
        Object resultData = null;
        String msg = RMsg.SERVE_FAIL.getMsg();
        String code = "FAIL";
        // 开始时间
        Date startDate = new Date();
        // 异常类型
        String errorType = ErrorTypeEnum.CUSTOM.name();
        try {
            // 执行
            resultData = point.proceed();
            // 执行结果
            code = "SUCCESS";
        } catch (ApiException | AccessDeniedException e) {
            log.error("【自定义异常】", e);
            msg = e.getMessage();
        } catch (Throwable throwable) {
            log.error("【系统异常】 {}", throwable.getMessage());
            msg = ExceptionUtil.getExceptionMessage(throwable);
            errorType = ErrorTypeEnum.SYSTEM.name();
        }

        if (code.equals("FAIL")) {
            msg = StringUtils.isNotBlank(msg) && msg.length() > 1024 ? msg.substring(0, 1023) : msg;
            // 异常日志
            this.doSaveErrorLog(point, msg, errorType);
            // 响应结果
            resultData = R.fail(msg);
        }

        try {
            // 获取request
            HttpServletRequest request = ServletUtils.getRequest();
            // 获得注解
            Log controllerLog = this.getAnnotationLog(point);
            // 获取参数
            String param = this.getParameter(request, point);
            // 保存
            this.doSaveSysLog(request, controllerLog, param, startDate, code, JSONObject.toJSONString(resultData));
        } catch (Exception e) {
            log.error("【全局日志】", e);
        }
        return resultData;
    }

    /**
     * 获取注解
     */
    private Log getAnnotationLog(JoinPoint joinPoint) throws Exception {
        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        Method method = methodSignature.getMethod();

        if (method != null) {
            return method.getAnnotation(Log.class);
        }
        return Log.class.newInstance();
    }

    /**
     * 获取请求参数
     */
    private String getParameter(HttpServletRequest request, ProceedingJoinPoint point) {
        String param = null;
        String method = request.getMethod();
        if (method.equalsIgnoreCase("GET")) {
            Map<String, String[]> params = request.getParameterMap();
            param = CollectionUtils.isEmpty(params) ? null : JSONObject.toJSONString(params);
        } else if (method.equalsIgnoreCase("POST") || method.equalsIgnoreCase("PUT")) {
            MethodSignature signature = (MethodSignature) point.getSignature();
            String[] parameterName = signature.getParameterNames();//参数名
            Object[] args = point.getArgs();//参数值
            Map<String, Object> map = new HashMap<>();
            for (int i = 0; i < args.length; i++) {
                String name = parameterName[i];
                if (name.equals("file")) {
                    map.put(name, "文件类型");
                    continue;
                }
                Object arg = args[i];

                try {
                    boolean isRequestOrResponse = arg instanceof HttpServletRequest || arg instanceof HttpServletResponse;
                    if (!isRequestOrResponse) {
                        map.put(name, arg);
                    }
                } catch (Exception ignored) {
                }
            }
            param = JSONObject.toJSONString(map);
        }
        return param;
    }

    /**
     * 保存请求日志
     */
    private void doSaveSysLog(HttpServletRequest request, Log controllerLog, String param, Date startDate, String code, String resultData) {
        String method = request.getMethod();
        SysOptLog opt = new SysOptLog();
        // 模块名称
        opt.setTitle(controllerLog.title());
        // 模块类型
        opt.setBusinessType(controllerLog.businessType().name());
        // 请求地址
        opt.setUrl(request.getRequestURI());
        // 请求类型方法
        opt.setMethod(method);
        // 请求参数
        opt.setParams(param);
        // 调用时间
        Date end = new Date();
        String time = String.valueOf(end.getTime() - startDate.getTime());
        opt.setTime(time);
        // 返回结果
        opt.setResultCode(code);
        opt.setResultData(resultData);
        // 用户
        opt.setLoginLogId(SecurityUtil.getLoginIdNull());
        opt.setUserName(SecurityUtil.getUsernameNull());
        // 保存
        sysLoginOptService.save(opt);
        log.info("【操作日志】 {},{},{},{}", opt.getModifyDate(), opt.getLoginLogId(), opt.getUrl(), param);
    }

    /**
     * 保存异常日志
     */
    private void doSaveErrorLog(ProceedingJoinPoint point, String msg, String errorType) {
        try {
            // 保存
            SysErrorLog error = new SysErrorLog();
            error.setMethod(point.getSignature().toString());
            error.setType(errorType);
            error.setMsg(msg);
            sysErrorLogService.save(error);

            // 通知
            if (errorType.equals(ErrorTypeEnum.CUSTOM.name())) return;

            Long noticeId = sysNoticeService.saveErrorNotice(msg);
            if (noticeId != null) {
                sysNoticeUserReadLogic.saveUserRead(noticeId, SysUser.getNoticeAdmin());
            }
        } catch (Exception e) {
            log.error("【保存异常日志】", e);
        }
    }
}
