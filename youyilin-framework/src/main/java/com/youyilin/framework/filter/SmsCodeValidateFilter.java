package com.youyilin.framework.filter;

import com.youyilin.common.redis.RedisPrefix;
import com.youyilin.common.redis.RedisService;
import com.youyilin.common.exception.SmsCodeValidateException;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 短信验证码过滤器
 */
@Component
public class SmsCodeValidateFilter extends OncePerRequestFilter {

    private final static String URI = "/phone/login";

    private final RedisService redisService;
    private final AuthenticationFailureHandler authenticationFailureHandler;

    @Autowired
    public SmsCodeValidateFilter(RedisService redisService, AuthenticationFailureHandler authenticationFailureHandler) {
        this.redisService = redisService;
        this.authenticationFailureHandler = authenticationFailureHandler;
    }

    @Override
    protected void doFilterInternal(@NotNull HttpServletRequest httpServletRequest, @NotNull HttpServletResponse httpServletResponse, @NotNull FilterChain filterChain) throws ServletException, IOException {
        //只拦截post请求且为/phoneLogin的方法
        String uri = httpServletRequest.getRequestURI();
        String method = httpServletRequest.getMethod();
        if (uri.equalsIgnoreCase(URI) && method.equalsIgnoreCase("POST")) {
            try {
                validateCode(httpServletRequest);
            } catch (SmsCodeValidateException e) {
                authenticationFailureHandler.onAuthenticationFailure(httpServletRequest, httpServletResponse, e);
                return;
            }
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

    private void validateCode(HttpServletRequest request) {
        String mobile = request.getParameter("phone");
        String code = request.getParameter("code");
        if (StringUtils.isBlank(mobile) || StringUtils.isBlank(code)) {
            throw new SmsCodeValidateException("验证码错误");
        }

        String key = RedisPrefix.SMS_KEY_PREFIX + mobile;
        String captchaCache;
        try {
            captchaCache = redisService.getObject(key, String.class);
        } catch (Exception e) {
            throw new SmsCodeValidateException("验证码已过期");
        }

        if (StringUtils.isBlank(captchaCache)) {
            throw new SmsCodeValidateException("验证码错误");
        }

        redisService.delete(key);
        if (!code.equals(captchaCache)) {
            throw new SmsCodeValidateException("验证码不正确");
        }
    }
}
