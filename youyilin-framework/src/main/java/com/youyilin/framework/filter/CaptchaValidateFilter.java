package com.youyilin.framework.filter;

import com.youyilin.common.redis.RedisPrefix;
import com.youyilin.common.redis.RedisService;
import com.youyilin.common.exception.CaptchaValidateException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 图片验证码过滤器
 */
@Slf4j
@Component
public class CaptchaValidateFilter extends OncePerRequestFilter {

    private final static String URI = "/login";

    private final RedisService redisService;
    private final AuthenticationFailureHandler authenticationFailureHandler;

    @Autowired
    public CaptchaValidateFilter(RedisService redisService, AuthenticationFailureHandler authenticationFailureHandler) {
        this.redisService = redisService;
        this.authenticationFailureHandler = authenticationFailureHandler;
    }

    @Override
    protected void doFilterInternal(@NotNull HttpServletRequest httpServletRequest, @NotNull HttpServletResponse httpServletResponse, @NotNull FilterChain filterChain) throws ServletException, IOException {
        String uri = httpServletRequest.getRequestURI();
        String method = httpServletRequest.getMethod().toUpperCase();
        if (uri.equalsIgnoreCase(URI) && method.equalsIgnoreCase("POST")) {
            try {
                validateCode(httpServletRequest);
            } catch (CaptchaValidateException e) {
                authenticationFailureHandler.onAuthenticationFailure(httpServletRequest, httpServletResponse, e);
                return;
            }
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

    private void validateCode(HttpServletRequest request) {
        String captcha = request.getParameter("captcha");
        String uuid = request.getParameter("uuid");
        if (StringUtils.isBlank(captcha) || StringUtils.isBlank(uuid)) {
            throw new CaptchaValidateException("验证码不正确");
        }

        uuid = RedisPrefix.CAPTCHA_KEY_PREFIX + uuid;
        String captchaCache;
        try {
            captchaCache = redisService.getObject(uuid, String.class);
        } catch (Exception e) {
            throw new CaptchaValidateException("验证码已过期");
        }

        if (StringUtils.isBlank(captchaCache)) {
            throw new CaptchaValidateException("验证码已过期");
        }
        redisService.delete(uuid);
        if (!captcha.equalsIgnoreCase(captchaCache)) {
            throw new CaptchaValidateException("验证码不正确");
        }
    }
}
