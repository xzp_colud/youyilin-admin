package com.youyilin.framework.userdetails;

import com.youyilin.common.entity.LoginAdminEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * 后台手机号登录
 * spring security 重写userDetailsService接口 登录验证 从数据库中获取用户权限
 */
@Service
public class UserDetailsAdminPhoneServiceImpl implements UserDetailsService {

    private final AdminLoadUserImpl adminLoadUser;

    @Autowired
    public UserDetailsAdminPhoneServiceImpl(AdminLoadUserImpl adminLoadUser) {
        this.adminLoadUser = adminLoadUser;
    }

    @Override
    public LoginAdminEntity loadUserByUsername(String username) throws UsernameNotFoundException {
        return adminLoadUser.loadUserByMobile(username);
    }
}
