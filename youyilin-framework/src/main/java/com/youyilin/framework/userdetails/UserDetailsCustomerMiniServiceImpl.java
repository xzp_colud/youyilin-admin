package com.youyilin.framework.userdetails;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.youyilin.common.entity.LoginAdminEntity;
import com.youyilin.common.utils.ServletUtils;
import com.youyilin.customer.entity.CustomerMini;
import com.youyilin.customer.logic.CustomerMiniLogic;
import com.youyilin.framework.utils.JwtUtils;
import com.youyilin.system.enums.LoginTypeEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

/**
 * 客户登录 基于微信openid
 * spring security 重写userDetailsService接口 登录验证 从数据库中获取用户权限
 */
@Slf4j
@Service
public class UserDetailsCustomerMiniServiceImpl implements UserDetailsService {

    private final JwtUtils jwtUtils;
    private final CustomerMiniLogic customerMiniLogic;

    public UserDetailsCustomerMiniServiceImpl(JwtUtils jwtUtils, CustomerMiniLogic customerMiniLogic) {
        this.jwtUtils = jwtUtils;
        this.customerMiniLogic = customerMiniLogic;
    }

    @Override
    public LoginAdminEntity loadUserByUsername(String username) throws UsernameNotFoundException {
        HttpServletRequest request = ServletUtils.getRequest();
        String appId = request.getParameter("appId");
        CustomerMini cm = customerMiniLogic.registerByOpenid(appId, username);
        return this.refreshAdmin(cm, LoginTypeEnum.MINI.name());
    }

    private LoginAdminEntity refreshAdmin(CustomerMini customer, String type) {
        LoginAdminEntity loginAdmin = new LoginAdminEntity();
        loginAdmin.setId(IdWorker.getId());
        loginAdmin.setUserId(customer.getId());
        loginAdmin.setUserName(customer.getOpenid());
        loginAdmin.setOpenid(customer.getOpenid());
        loginAdmin.setMobile(customer.getPhone());
        loginAdmin.setAvatarUrl(customer.getAvatarUrl());
        loginAdmin.setEnabled(true);
        loginAdmin.setType(type);
        loginAdmin.setExpireDate(jwtUtils.getExpireTime());
        loginAdmin.setAllowMultiLogin(false);

        return loginAdmin;
    }
}
