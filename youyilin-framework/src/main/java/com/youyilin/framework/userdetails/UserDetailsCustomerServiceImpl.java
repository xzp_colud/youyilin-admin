package com.youyilin.framework.userdetails;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.youyilin.common.entity.LoginAdminEntity;
import com.youyilin.common.exception.LoginValidException;
import com.youyilin.customer.entity.Customer;
import com.youyilin.customer.service.CustomerService;
import com.youyilin.framework.utils.JwtUtils;
import com.youyilin.system.enums.LoginTypeEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * 后台登录
 * spring security 重写userDetailsService接口 登录验证 从数据库中获取用户权限
 */
@Slf4j
@Service
public class UserDetailsCustomerServiceImpl implements UserDetailsService {

    private final JwtUtils jwtUtils;
    private final CustomerService customerService;

    public UserDetailsCustomerServiceImpl(JwtUtils jwtUtils, CustomerService customerService) {
        this.jwtUtils = jwtUtils;
        this.customerService = customerService;
    }

    @Override
    public LoginAdminEntity loadUserByUsername(String username) throws UsernameNotFoundException {
        log.info("【登录 客户】");
        Customer userName = customerService.getByUserName(username);
        if (userName != null) {
            return this.refreshAdmin(userName, LoginTypeEnum.PASSWORD.name());
        }
        Customer phone = customerService.getByPhone(username);
        if (phone == null) {
            throw new LoginValidException("账号或密码不正确");
        }
        return this.refreshAdmin(phone, LoginTypeEnum.PHONE.name());
    }

    private LoginAdminEntity refreshAdmin(Customer customer, String type) {
        LoginAdminEntity loginAdmin = new LoginAdminEntity();
        loginAdmin.setId(IdWorker.getId());
        loginAdmin.setUserId(customer.getId());
        loginAdmin.setUserName(customer.getUserName());
        loginAdmin.setPassword(customer.getPassword());
        loginAdmin.setMobile(customer.getPhone());
        loginAdmin.setAvatarUrl(customer.getAvatarUrl());
        loginAdmin.setRealName(customer.getName());
        loginAdmin.setEnabled(true);
        loginAdmin.setType(type);
        loginAdmin.setExpireDate(jwtUtils.getExpireTime());
        loginAdmin.setAllowMultiLogin(true);

        return loginAdmin;
    }
}
