package com.youyilin.framework.userdetails;

import com.youyilin.common.entity.LoginAdminEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * 后台登录
 * spring security 重写userDetailsService接口 登录验证 从数据库中获取用户权限
 */
@Slf4j
@Service
public class UserDetailsAdminServiceImpl implements UserDetailsService {

    private final AdminLoadUserImpl adminLoadUser;

    public UserDetailsAdminServiceImpl(AdminLoadUserImpl adminLoadUser) {
        this.adminLoadUser = adminLoadUser;
    }

    @Override
    public LoginAdminEntity loadUserByUsername(String username) throws UsernameNotFoundException {
        return adminLoadUser.loadUserByUserName(username);
    }
}
