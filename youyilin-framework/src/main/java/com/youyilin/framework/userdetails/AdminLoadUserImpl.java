package com.youyilin.framework.userdetails;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.youyilin.common.entity.LoginAdminEntity;
import com.youyilin.common.enums.BooleanEnum;
import com.youyilin.common.enums.StatusEnum;
import com.youyilin.common.exception.LoginValidException;
import com.youyilin.common.utils.SpringUtil;
import com.youyilin.framework.utils.JwtUtils;
import com.youyilin.system.entity.SysUser;
import com.youyilin.system.enums.LoginTypeEnum;
import com.youyilin.system.logic.SysMenuLogic;
import com.youyilin.system.service.SysUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.support.AopUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.bind.annotation.RestController;

import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 管理后台登录
 */
@Slf4j
@Service
public class AdminLoadUserImpl {

    private final JwtUtils jwtUtils;
    private final SysUserService sysUserService;
    private final SysMenuLogic sysMenuLogic;

    public AdminLoadUserImpl(JwtUtils jwtUtils, SysUserService sysUserService, SysMenuLogic sysMenuLogic) {
        this.jwtUtils = jwtUtils;
        this.sysUserService = sysUserService;
        this.sysMenuLogic = sysMenuLogic;
    }

    /**
     * 密码 用户名|手机号|邮箱
     */
    public LoginAdminEntity loadUserByUserName(String username) {
        // 用户名
        SysUser userName = sysUserService.getByUserName(username);
        if (userName != null) {
            return this.refreshLoginAdmin(LoginTypeEnum.PASSWORD.name(), userName);
        }
        // 手机号
        SysUser phone = sysUserService.getByPhone(username);
        if (phone != null) {
            return this.refreshLoginAdmin(LoginTypeEnum.PHONE.name(), phone);
        }
        // 邮箱
        SysUser email = sysUserService.getByPhone(username);
        return this.refreshLoginAdmin(LoginTypeEnum.EMAIL.name(), email);
    }

    /**
     * 手机验证码
     */
    public LoginAdminEntity loadUserByMobile(String mobile) {
        SysUser sysUser = sysUserService.getByPhone(mobile);
        return this.refreshLoginAdmin(LoginTypeEnum.SMS.name(), sysUser);
    }

    /**
     * 刷新登录信息
     *
     * @param type    登录类型
     * @param sysUser 用户
     * @return 登录信息
     */
    private LoginAdminEntity refreshLoginAdmin(String type, SysUser sysUser) {
        if (sysUser == null) {
            throw new LoginValidException("账号不存在");
        }
        if (sysUser.getStatus() == null || sysUser.getStatus() != StatusEnum.NORMAL.getCode()) {
            throw new LoginValidException("账号异常");
        }

        LoginAdminEntity loginAdmin = new LoginAdminEntity();
        loginAdmin.setId(IdWorker.getId());
        loginAdmin.setUserId(sysUser.getId());
        loginAdmin.setUserName(sysUser.getUserName());
        loginAdmin.setPassword(sysUser.getPassword());
        loginAdmin.setMobile(sysUser.getPhone());
        loginAdmin.setAvatarUrl(sysUser.getAvatarUrl());
        loginAdmin.setRealName(sysUser.getRealName());
        loginAdmin.setEnabled(true);
        loginAdmin.setType(type);
        loginAdmin.setExpireDate(jwtUtils.getExpireTime());
        loginAdmin.setAllowMultiLogin(sysUser.getShareLogin() != null && sysUser.getShareLogin() == BooleanEnum.TRUE.getCode());

        return loginAdmin;
    }

    /**
     * 获取用户的权限
     *
     * @param userId 用户ID
     * @return ArrayList
     */
    private List<String> getAuthorities(long userId) {
        if (SysUser.isAdmin(userId)) {
            return listControllerPerms();
        }
        List<String> authList = new ArrayList<>();

        List<String> perms = sysMenuLogic.listLoginPerms(userId);
        if (CollectionUtils.isNotEmpty(perms)) {
            authList.addAll(perms);
        }

        return authList;
    }

    /**
     * 获取controller上所有权限配置
     *
     * @return ArrayList
     */
    private List<String> listControllerPerms() {
        Map<String, Object> map = SpringUtil.getBeansWithAnnotation(RestController.class);
        Set<String> authList = new HashSet<>();

        for (String key : map.keySet()) {
            Object o = map.get(key);
            Method[] methods = ReflectionUtils.getAllDeclaredMethods(AopUtils.getTargetClass(o));
            for (Method method : methods) {
                PreAuthorize preAuthorize = method.getAnnotation(PreAuthorize.class);
                if (preAuthorize != null) {
                    String auth = preAuthorize.value().replace("hasAuthority('", "").replace("')", "");
                    authList.add(auth);
                }
            }
        }

        return new ArrayList<>(authList);
    }

    public List<GrantedAuthority> listUserAuthorities(long userId) {
        List<String> authList = this.getAuthorities(userId);
        if (CollectionUtils.isNotEmpty(authList)) {
            return authList.stream().map(item -> new SimpleGrantedAuthority(item.trim())).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }
}
