package com.youyilin.framework.interceptor;

import com.youyilin.common.utils.ip.IpUtils;
import com.youyilin.system.logic.SysIpLogic;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * IP 拦截
 */
@Component
public class IpInterceptor implements HandlerInterceptor {

    private final SysIpLogic sysIpLogic;

    public IpInterceptor(SysIpLogic sysIpLogic) {
        this.sysIpLogic = sysIpLogic;
    }

    @Override
    public boolean preHandle(@NotNull HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull Object handler) {
        String ip = IpUtils.getIpAddr(request);
        if (sysIpLogic.checkBlankIp(ip)) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return false;
        }
        request.setAttribute("request_ip", ip);
        return true;
    }
}
