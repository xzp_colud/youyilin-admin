package com.youyilin.framework.interceptor;

import lombok.Data;

/**
 * API 接口认证 request
 */
@Data
public class ApiSignValidateResponse {

    private boolean success;
    private String msg;
}
