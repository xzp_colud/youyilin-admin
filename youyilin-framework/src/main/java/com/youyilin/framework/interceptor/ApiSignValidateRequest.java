package com.youyilin.framework.interceptor;

import lombok.Data;

/**
 * API 接口认证 request
 */
@Data
public class ApiSignValidateRequest {

    private String appId;
    private String timestamp;
    private String nonce;
    private String data;
    private String sign;
}
