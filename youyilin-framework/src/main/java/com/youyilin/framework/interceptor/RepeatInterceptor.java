package com.youyilin.framework.interceptor;

import com.alibaba.fastjson.JSONObject;
import com.youyilin.common.bean.R;
import com.youyilin.common.redis.RedisPrefix;
import com.youyilin.common.redis.RedisService;
import com.youyilin.common.utils.MD5Util;
import com.youyilin.common.utils.SecurityUtil;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.concurrent.TimeUnit;

/**
 * 连续请求 拦截器
 */
@Slf4j
@Component
public class RepeatInterceptor implements HandlerInterceptor {

    // GET 1s内连续超过设定的请求数量
    private final static int GET_CONTINUOUS_NUM = 10;
    // POST PUT 表单重复提交拦截
    private final static int POST_CONTINUOUS_NUM = 1;

    private final RedisService redisService;

    public RepeatInterceptor(RedisService redisService) {
        this.redisService = redisService;
    }

    @Override
    public boolean preHandle(@NotNull HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull Object handler) throws Exception {
        String ip = (String) request.getAttribute("request_ip");
        boolean validate = this.validateRequestNum(request, ip);
        if (!validate) {
            ServletOutputStream outputStream = response.getOutputStream();
            outputStream.write(JSONObject.toJSONBytes(R.fail("点击过快")));
            outputStream.flush();
            outputStream.close();
        }
        return validate;
    }

    /**
     * redis 验证请求次数
     *
     * @param request 请求
     * @param ip      当前访问IP
     * @return boolean
     */
    private boolean validateRequestNum(HttpServletRequest request, String ip) {
        // 登录用户
        Long userId = SecurityUtil.getUserIdNull();
        // 请求链接
        String url = request.getRequestURI();
        // 请求方法
        String method = request.getMethod().toUpperCase();
        // 请求类型
        boolean methodGet = method.equalsIgnoreCase("GET");
        // 加密Key
        String mdKey = url + ip + (userId == null ? "" : userId);
        // 加密后的值
        String mdUrlIp = MD5Util.md5Encrypt(mdKey);
        // Redis Key
        String redisKey = RedisPrefix.FORM_SUBMIT_KEY_PREFIX + mdUrlIp;
        // Redis 存在
        boolean hasKey = redisService.hasKey(redisKey);
        int redisNum = 0;
        if (hasKey) {
            try {
                redisNum = Integer.parseInt(redisService.get(mdUrlIp));
                if (methodGet) {
                    if (redisNum >= GET_CONTINUOUS_NUM) {
                        return false;
                    }
                } else {
                    if (redisNum >= POST_CONTINUOUS_NUM) {
                        return false;
                    }
                }
            } catch (NumberFormatException e) {
                log.error("Redis 读取次数失败", e);
            }
        }
        redisNum++;
        if (methodGet) {
            redisService.add(mdUrlIp, redisNum, 5, TimeUnit.SECONDS);
        } else {
            redisService.add(mdUrlIp, redisNum, 5, TimeUnit.SECONDS);
        }
        return true;
    }
}
