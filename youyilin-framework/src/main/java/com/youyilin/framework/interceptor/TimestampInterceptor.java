package com.youyilin.framework.interceptor;

import com.alibaba.fastjson.JSONObject;
import com.youyilin.common.bean.R;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 有效请求 拦截
 */
@Slf4j
@Component
public class TimestampInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(@NotNull HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull Object handler) throws Exception {
        boolean validTimestamp = this.validateRequestTimestamp(request);
        if (!validTimestamp) {
            ServletOutputStream outputStream = response.getOutputStream();
            outputStream.write(JSONObject.toJSONBytes(R.fail("无效链接")));
            outputStream.flush();
            outputStream.close();
            return false;
        }
        return true;
    }

    /**
     * 验证请求时间是否有效
     *
     * @return boolean
     */
    private boolean validateRequestTimestamp(HttpServletRequest request) {
        String method = request.getMethod();
        if (method.equalsIgnoreCase("GET")) {
            return true;
        }
        String timestamp = request.getHeader("timestamp");
//        if (StringUtils.isBlank(timestamp)) return false;
//        long nowTimestamp = System.currentTimeMillis();
//        long requestTimestamp = Long.parseLong(timestamp);
        // 1分钟内
//        return nowTimestamp - requestTimestamp < 60000L;
        return true;
    }
}
