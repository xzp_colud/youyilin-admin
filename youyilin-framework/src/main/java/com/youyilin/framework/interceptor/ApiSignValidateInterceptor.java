package com.youyilin.framework.interceptor;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import sun.misc.BASE64Encoder;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 * API 接口签名 调用验证 拦截器
 */
@Slf4j
@Component
public class ApiSignValidateInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(@NotNull HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull Object handler) throws Exception {
        ApiSignValidateResponse validateResponse = analyseRequest(request);
        if (!validateResponse.isSuccess()) {
            response.setStatus(HttpServletResponse.SC_OK);

            ServletOutputStream outputStream = response.getOutputStream();
            outputStream.write(JSONObject.toJSONBytes(validateResponse));
            outputStream.flush();
            outputStream.close();
            return false;
        }
        return true;
    }

    @Override
    public void postHandle(@NotNull HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull Object handler, ModelAndView modelAndView) {

    }

    @Override
    public void afterCompletion(@NotNull HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull Object handler, Exception ex) {

    }

    /**
     * 解析头部参数
     */
    private ApiSignValidateResponse analyseRequest(HttpServletRequest request) {
        String method = request.getMethod();
        String sign = request.getHeader("sign");
        String timestamp = request.getHeader("timestamp");
        String appId = request.getHeader("appId");
        String nonce = request.getHeader("nonce");

        String bodyDataJson = null;

        if (method.equalsIgnoreCase("GET")) {
            bodyDataJson = applyGetRequestData(request);
        } else if (method.equalsIgnoreCase("POST")) {
            bodyDataJson = applyPostRequestData(request);
        }

        ApiSignValidateRequest apiRequest = new ApiSignValidateRequest();
        apiRequest.setAppId(appId);
        apiRequest.setTimestamp(timestamp);
        apiRequest.setNonce(nonce);
        apiRequest.setData(bodyDataJson);
        apiRequest.setSign(sign);
        // 验证参数
        ApiSignValidateResponse paramsResponse = validateParams(apiRequest);
        if (!paramsResponse.isSuccess()) {
            return paramsResponse;
        }
        // 验证签名
        return this.createSign(apiRequest);
    }

    private String applyGetRequestData(HttpServletRequest request) {
        return null;
    }

    private String applyPostRequestData(HttpServletRequest request) {
        StringBuilder sb = new StringBuilder();
        InputStream inputStream = null;
        BufferedReader bufferedReader = null;
        try {
            inputStream = request.getInputStream();
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
            char[] readLine = new char[1024];
            int len;
            while ((len = bufferedReader.read(readLine)) != -1) {
                sb.append(new String(readLine, 0, len));
            }
            return JSONObject.toJSONString(JSONObject.parseObject(sb.toString()));
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    /**
     * 验证参数
     */
    private ApiSignValidateResponse validateParams(ApiSignValidateRequest apiRequest) {
        ApiSignValidateResponse apiResponse = new ApiSignValidateResponse();
        apiResponse.setSuccess(true);
        if (StringUtils.isBlank(apiRequest.getTimestamp())
                || StringUtils.isBlank(apiRequest.getAppId())
                || StringUtils.isBlank(apiRequest.getNonce())
                || StringUtils.isBlank(apiRequest.getData())
                || StringUtils.isBlank(apiRequest.getSign())) {
            // 参数不正确
            apiResponse.setSuccess(false);
            apiResponse.setMsg("参数不正确");
            return apiResponse;
        }
        long requestTime = Long.parseLong(apiRequest.getTimestamp());
        long sysTime = System.currentTimeMillis();
        long validTime = 2 * 60 * 1000;
        if (requestTime > sysTime || sysTime - requestTime > validTime) {
            // 无效的请求
            apiResponse.setSuccess(false);
            apiResponse.setMsg("无效的请求");
        }
        return apiResponse;
    }

    /**
     * 验证签名
     */
    private ApiSignValidateResponse createSign(ApiSignValidateRequest apiRequest) {
        ApiSignValidateResponse apiResponse = new ApiSignValidateResponse();
        apiResponse.setSuccess(true);
        String appSecret = "12345678";
        if (StringUtils.isBlank(appSecret)) {
            // 没有秘钥 错误的应用
            apiResponse.setSuccess(false);
            apiResponse.setMsg("应用错误");
            return apiResponse;
        }
        String signString = "appSecret=" + appSecret + "&appId=" + apiRequest.getAppId() + "&timestamp=" + apiRequest.getTimestamp() + "&nonce=" + apiRequest.getNonce() + "&data=" + apiRequest.getData();
        BASE64Encoder encoder = new BASE64Encoder();
        String encodeSign = encoder.encode(signString.getBytes(StandardCharsets.UTF_8));
        String signValid = DigestUtils.md5DigestAsHex(encodeSign.getBytes(StandardCharsets.UTF_8));
        log.info("{} , {}, {}, {}, {}, {},{}", appSecret, apiRequest.getAppId(), apiRequest.getTimestamp(), apiRequest.getNonce(), apiRequest.getData(), apiRequest.getSign(), signValid);
        if (!apiRequest.getSign().equals(signValid)) {
            // 签名验证异常
            apiResponse.setSuccess(false);
            apiResponse.setMsg("签名错误");
        }
        return apiResponse;
    }

//    public static void main(String[] args) {
//        String appSecret = "12345678";
//        String appId = "123456";
//        long timestamp = System.currentTimeMillis();
//        String nonce = RandomStringUtils.randomAlphabetic(32);
//        Map<String, String> map = new HashMap<>();
//        map.put("name", "xzp");
//        String data = JSONObject.toJSONString(map);
//        String signString = "appSecret=" + appSecret + "&appId=" + appId + "&timestamp=" + timestamp + "&nonce=" + nonce + "&data=" + data;
//        BASE64Encoder encoder = new BASE64Encoder();
//        String encodeSign = null;
//        String signValid = null;
//        try {
//            encodeSign = encoder.encode(signString.getBytes("UTF-8"));
//            signValid = DigestUtils.md5DigestAsHex(encodeSign.getBytes("UTF-8"));
//        } catch (UnsupportedEncodingException e) {
//            log.error("【签名异常】", e);
//        }
//        log.info("{} , {}, {}, {}, {}, {}", appSecret, appId, timestamp, nonce, data, signValid);
//    }
}
