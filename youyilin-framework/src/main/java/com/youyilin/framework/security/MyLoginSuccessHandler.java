package com.youyilin.framework.security;

import com.alibaba.fastjson.JSONObject;
import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import com.youyilin.common.entity.LoginAdminEntity;
import com.youyilin.common.redis.RedisPrefix;
import com.youyilin.common.redis.RedisService;
import com.youyilin.common.utils.ip.AddressUtils;
import com.youyilin.common.utils.ip.IpUtils;
import com.youyilin.framework.utils.JwtUtils;
import com.youyilin.framework.utils.MyUserAgent;
import com.youyilin.framework.utils.UserAgentUtil;
import com.youyilin.system.entity.SysLoginLog;
import com.youyilin.system.enums.LoginStatusEnum;
import com.youyilin.system.service.SysLoginLogService;
import com.youyilin.system.service.SysUserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * 登录成功
 */
@Slf4j
@Component
public class MyLoginSuccessHandler implements AuthenticationSuccessHandler {

    private final RedisService redisService;
    private final JwtUtils jwtUtils;
    private final SysLoginLogService sysLoginLogService;
    private final SysUserService sysUserService;

    public MyLoginSuccessHandler(RedisService redisService, JwtUtils jwtUtils,
                                 SysLoginLogService sysLoginLogService, SysUserService sysUserService) {
        this.redisService = redisService;
        this.jwtUtils = jwtUtils;
        this.sysLoginLogService = sysLoginLogService;
        this.sysUserService = sysUserService;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {
        LoginAdminEntity admin = (LoginAdminEntity) authentication.getPrincipal();
//        LoginAdminEntity admin = SecurityUtil.getLoginUser();
        // 生成用户凭证
        this.createToken(admin);
        // Redis 缓存
        this.refreshRedis(request, admin);
        // 登录日志
        this.doSaveLoginLog(request, admin);
        // 登录返回信息
        this.doResponse(response, admin);
    }

    /**
     * 创建Token
     */
    public void createToken(LoginAdminEntity admin) {
        String userId = admin.getUserId().toString();
        String tokenId = admin.getId().toString();
        String createTokenId = userId.concat(tokenId);
        // 过期时间
        Date expireDate = jwtUtils.getExpireTime();
        // 用户凭证 TOKEN
        String jwt = jwtUtils.generateToken(tokenId, createTokenId, admin.getExpireDate());
        // 设置
        admin.setExpireDate(expireDate);
        admin.setToken(jwt);
        admin.setCreateTokenId(createTokenId);
    }

    /**
     * Redis 缓存
     */
    public void refreshRedis(HttpServletRequest request, LoginAdminEntity admin) {
        // IP 地点
        String loginIp = IpUtils.getIpAddr(request);
        String loginLocation = StringUtils.isNotBlank(loginIp) ? AddressUtils.getRealAddressByIP(loginIp) : null;

        // PC/移动端
        boolean mobileLogin = UserAgentUtil.isMobile(request);

        admin.setMobileLogin(mobileLogin);
        admin.setLoginDate(new Date());
        admin.setLoginIp(loginIp);
        admin.setLoginLocation(loginLocation);
        admin.setPassword(null);

        // 设置 Redis 缓存
        String tokenId = admin.getCreateTokenId();
        String redisKey = mobileLogin ? RedisPrefix.TOKEN_MOBILE_KEY_PREFIX : RedisPrefix.TOKEN_COMPUTER_KEY_PREFIX;
        // 多端登录限制
        if (!admin.isAllowMultiLogin()) {
            String deleteRedisKey = redisKey + admin.getUserId() + "*";
            Set<String> redisKeysSet = redisService.likes(deleteRedisKey);
            if (CollectionUtils.isNotEmpty(redisKeysSet)) {
                redisService.delete(redisKeysSet);
            }
        }
        redisKey = redisKey + tokenId;
        redisService.add(redisKey, admin, 7, TimeUnit.DAYS);
    }

    // 登录成功日志
    private void doSaveLoginLog(HttpServletRequest request, LoginAdminEntity admin) {
        try {
            // 冗余最后登录时间、地点
            sysUserService.updateLoginInfo(admin.getUsername(), admin.getLoginIp(), admin.getLoginLocation(), admin.getLoginDate());

            log.info("【登录成功】 {}, {}, {}", admin.getUsername(), admin.getRealName(), System.currentTimeMillis());

            MyUserAgent userAgent = UserAgentUtil.initUserAgent(request);

            SysLoginLog successLogin = new SysLoginLog();
            successLogin.setId(admin.getId());
            successLogin.setUserName(admin.getUsername());
            successLogin.setLoginType(admin.getType());
            successLogin.setStatus(LoginStatusEnum.SUCCESS.name());
            successLogin.setMsg(null);
            successLogin.setBrowser(userAgent.getBrowser());
            successLogin.setOs(userAgent.getOs());
            successLogin.setVersion(userAgent.getVersion());
            successLogin.setDevice(userAgent.getDevice());
            successLogin.setLoginIp(admin.getLoginIp());
            successLogin.setLoginLocation(admin.getLoginLocation());
            successLogin.setLoginDate(admin.getLoginDate());
            sysLoginLogService.save(successLogin);
        } catch (Exception e) {
            log.error("【登录日志异常】", e);
        }
    }

    /**
     * Response 返回
     */
    public void doResponse(HttpServletResponse response, LoginAdminEntity admin) throws IOException {
        // 返回对象
        JSONObject data = new JSONObject();
        // 设置TOKEN凭证
        data.put("token", admin.getToken());

        ServletOutputStream outputStream = response.getOutputStream();
        // 设置数据返回类型
        response.setContentType("application/json;charset=UTF-8");
        // 响应头部携带用户凭证Token
        response.setHeader(jwtUtils.getHeard(), admin.getToken());
        // 输出响应内容
        outputStream.write(JSONObject.toJSONBytes(R.success(RMsg.LOGIN_SUCCESS.getMsg(), data)));
        outputStream.flush();
        outputStream.close();
    }
}
