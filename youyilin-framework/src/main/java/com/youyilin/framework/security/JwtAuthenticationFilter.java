package com.youyilin.framework.security;

import com.youyilin.common.entity.LoginAdminEntity;
import com.youyilin.common.exception.LoginValidException;
import com.youyilin.common.redis.RedisPrefix;
import com.youyilin.common.redis.RedisService;
import com.youyilin.common.utils.SecurityUtil;
import com.youyilin.framework.userdetails.AdminLoadUserImpl;
import com.youyilin.framework.utils.UserAgentUtil;
import com.youyilin.framework.utils.JwtUtils;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.security.sasl.AuthenticationException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * token过滤器来验证token有效性
 * 自定义一个过滤器用来进行识别jwt
 * 获取到用户名之后我们直接把封装成UsernamePasswordAuthenticationToken，
 * 之后交给SecurityContextHolder参数传递authentication对象，这样后续security就能获取到当前登录的用户信息了，也就完成了用户认证。
 */
@Slf4j
@Component
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    private final JwtUtils jwtUtils;
    private final RedisService redisService;
    private final AdminLoadUserImpl adminLoadUser;

    public JwtAuthenticationFilter(JwtUtils jwtUtils, RedisService redisService, AdminLoadUserImpl adminLoadUser) {
        this.jwtUtils = jwtUtils;
        this.redisService = redisService;
        this.adminLoadUser = adminLoadUser;
    }

    @Override
    protected void doFilterInternal(@NotNull HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull FilterChain chain) throws IOException, ServletException {
//        String ip = IpUtils.getIpAddr(request);
        // 验证jwt
        String heard = jwtUtils.getHeard();
        if (StringUtils.isBlank(heard)) {
            chain.doFilter(request, response);
            return;
        }
        String jwt = request.getHeader(heard);
        // 如果jwt为空 则放行不需要登录的路径 如 登录页面  静态资源 白名单等
        if (StringUtils.isBlank(jwt)) {
            chain.doFilter(request, response);
            return;
        }
        // 有jwt的情况 需要解析jwt
        Claims claims = jwtUtils.getClaimsByToken(jwt);
        if (claims == null) {
            chain.doFilter(request, response);
            return;
        }
        // 获取已登录的用户
        String username = claims.getSubject();
        boolean mobileLogin = UserAgentUtil.isMobile(request);
        String redisKey = mobileLogin ? RedisPrefix.TOKEN_MOBILE_KEY_PREFIX : RedisPrefix.TOKEN_COMPUTER_KEY_PREFIX;
        redisKey = redisKey + username;
        LoginAdminEntity loginAdmin;
        try {
//            boolean a = redisService.hasKey(redisKey);
            loginAdmin = redisService.getObject(redisKey, LoginAdminEntity.class);
        } catch (Exception e) {
            throw new AuthenticationException("登录已过期");
        }

        if (loginAdmin == null || !loginAdmin.getToken().equals(jwt)) {
            throw new AuthenticationException("登录已过期");
        }
        // 是否已经过期
        if (jwtUtils.isTokenExpried(claims)) {
            throw new AuthenticationException("登录已过期");
        }

        // 验证是否快过期了，是否需要刷新token
        Date expireDate = claims.getExpiration();
        String newToken = jwtUtils.verifyToken(expireDate, jwt);
        if (newToken != null) {
            loginAdmin.setToken(newToken);
            loginAdmin.setExpireDate(jwtUtils.getExpireTime());
            redisService.add(redisKey, loginAdmin);
            redisService.expire(redisKey, 7, TimeUnit.DAYS);

            response.setHeader(jwtUtils.getHeard(), newToken);
        }

        // loginAdmin.getAuthorities()
        // 自动登录 故密码为空  权限信息 接口调用进行身份认证过滤器时候JWTAuthenticationFilter，需要返回用户权限信息
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(loginAdmin, null, adminLoadUser.listUserAuthorities(loginAdmin.getUserId()));
        // 交给SecurityContextHolder参数传递authentication对象，这样后续security就能获取到当前登录的用户信息了，也就完成了用户认证。
        SecurityContextHolder.getContext().setAuthentication(token);
        chain.doFilter(request, response);
    }

//    @Override
//    protected void doFilterInternal(@NotNull HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull FilterChain chain) throws IOException, ServletException {
//        LoginAdminEntity login = this.getLoginAdmin(request, response);
//        if (login != null && SecurityUtil.getAuthentication() == null) {
//            // 权限
//            List<GrantedAuthority> authList = this.getAuthList(login);
//
//            // 自动登录 故密码为空  权限信息 接口调用进行身份认证过滤器时候JWTAuthenticationFilter，需要返回用户权限信息
//            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(login, null, authList);
//            // 交给SecurityContextHolder参数传递authentication对象，这样后续security就能获取到当前登录的用户信息了，也就完成了用户认证。
//            SecurityContextHolder.getContext().setAuthentication(token);
//        }
//        chain.doFilter(request, response);
//    }
//
//    private LoginAdminEntity getLoginAdmin(HttpServletRequest request, HttpServletResponse response) {
//        String token = request.getHeader(jwtUtils.getHeard());
//        if (StringUtils.isBlank(token)) return null;
//
//        Claims claims = jwtUtils.getClaimsByToken(token);
//        if (claims == null) {
//            throw new LoginValidException("登录已过期");
//        }
//
//        if (jwtUtils.isTokenExpried(claims)) {
//            throw new LoginValidException("登录已过期");
//        }
//
//        String username = claims.getSubject();
//        boolean mobileLogin = UserAgentUtil.isMobile(request);
//        String redisKey = mobileLogin ? RedisPrefix.TOKEN_MOBILE_KEY_PREFIX : RedisPrefix.TOKEN_COMPUTER_KEY_PREFIX;
//        redisKey = redisKey + username;
//        try {
//            LoginAdminEntity login = redisService.getObject(redisKey, LoginAdminEntity.class);
//
//            this.refreshToken(response, token, claims, login, redisKey);
//            return login;
//        } catch (Exception e) {
//            return null;
//        }
//    }
//
//    private void refreshToken(HttpServletResponse response, String token, Claims claims, LoginAdminEntity login, String redisKey) {
//        // 验证是否快过期了，是否需要刷新token
//        Date expireDate = claims.getExpiration();
//        String newToken = jwtUtils.verifyToken(expireDate, token);
//        if (newToken != null) {
//            login.setToken(newToken);
//            login.setExpireDate(jwtUtils.getExpireTime());
//            redisService.add(redisKey, login);
//            redisService.expire(redisKey, 7, TimeUnit.DAYS);
//
//            response.setHeader(jwtUtils.getHeard(), newToken);
//        }
//    }
//
//    private List<GrantedAuthority> getAuthList(LoginAdminEntity login) {
//        return adminLoadUser.listUserAuthorities(login.getUserId());
//    }
}
