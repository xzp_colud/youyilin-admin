package com.youyilin.framework.security;

import com.alibaba.fastjson.JSONObject;
import com.youyilin.common.bean.R;
import com.youyilin.common.exception.CaptchaValidateException;
import com.youyilin.common.exception.LoginValidException;
import com.youyilin.common.exception.SmsCodeValidateException;
import com.youyilin.common.utils.ip.AddressUtils;
import com.youyilin.common.utils.ip.IpUtils;
import com.youyilin.framework.utils.MyUserAgent;
import com.youyilin.framework.utils.UserAgentUtil;
import com.youyilin.system.entity.SysLoginLog;
import com.youyilin.system.enums.LoginStatusEnum;
import com.youyilin.system.enums.LoginTypeEnum;
import com.youyilin.system.service.SysLoginLogService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

/**
 * 登录失败
 */
@Slf4j
@Component
public class MyLoginFailureHandler implements AuthenticationFailureHandler {

    private final SysLoginLogService sysLoginLogService;

    public MyLoginFailureHandler(SysLoginLogService sysLoginLogService) {
        this.sysLoginLogService = sysLoginLogService;
    }

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException e) throws IOException {
        // 用户名
        String username = request.getParameter("username");
        // 错误信息
        String errorMsg = this.failMsg(e);
        // 日志
        this.doSaveLoginLog(request, username, errorMsg);

        // 响应信息
        ServletOutputStream outputStream = response.getOutputStream();
        // 设置数据返回类型
        response.setContentType("application/json;charset=UTF-8");
        // 输出响应内容
        outputStream.write(JSONObject.toJSONBytes(R.fail(errorMsg)));
        outputStream.flush();
        outputStream.close();
    }

    // 错误信息解析
    private String failMsg(AuthenticationException e) {
        String errorMsg = "用户名或密码错误";
        if (e.getClass() == CaptchaValidateException.class) {
            errorMsg = e.getLocalizedMessage();
        } else if (e.getClass() == SmsCodeValidateException.class) {
            errorMsg = e.getLocalizedMessage();
        } else if (e.getClass() == InternalAuthenticationServiceException.class) {
            InternalAuthenticationServiceException e2 = (InternalAuthenticationServiceException) e;
            if (e2.getCause().getClass() == LoginValidException.class) {
                errorMsg = e.getLocalizedMessage();
            }
        }
        return errorMsg;
    }

    // 登录成功日志
    private void doSaveLoginLog(HttpServletRequest request, String username, String msg) {
        try {
            log.error("【登录失败】 {}, {}", username, System.currentTimeMillis());

            MyUserAgent userAgent = UserAgentUtil.initUserAgent(request);
            String loginIp = IpUtils.getIpAddr(request);
            String loginLocation = StringUtils.isNotBlank(loginIp) ? AddressUtils.getRealAddressByIP(loginIp) : null;

            SysLoginLog successLogin = new SysLoginLog();
            successLogin.setUserName(username);
            successLogin.setLoginType(LoginTypeEnum.PASSWORD.name());
            successLogin.setStatus(LoginStatusEnum.FAIL.name());
            successLogin.setMsg(msg);
            successLogin.setBrowser(userAgent.getBrowser());
            successLogin.setOs(userAgent.getOs());
            successLogin.setVersion(userAgent.getVersion());
            successLogin.setDevice(userAgent.getDevice());
            successLogin.setLoginIp(loginIp);
            successLogin.setLoginLocation(loginLocation);
            successLogin.setLoginDate(new Date());
            sysLoginLogService.save(successLogin);
        } catch (Exception e) {
            log.error("【登录失败日志】", e);
        }
    }
}
