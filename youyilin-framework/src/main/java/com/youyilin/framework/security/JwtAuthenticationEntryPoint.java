package com.youyilin.framework.security;

import com.alibaba.fastjson.JSONObject;
import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 未登录
 */
@Slf4j
@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException {
        ServletOutputStream outputStream = response.getOutputStream();

        response.setContentType("application/json;charset=UTF-8");
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);

        outputStream.write(JSONObject.toJSONBytes(R.fail(RMsg.NO_LOGIN.getMsg())));
        outputStream.flush();
        outputStream.close();
    }
}
