package com.youyilin.framework.security;

import com.alibaba.fastjson.JSONObject;
import com.youyilin.common.bean.R;
import com.youyilin.common.entity.LoginAdminEntity;
import com.youyilin.common.redis.RedisPrefix;
import com.youyilin.common.redis.RedisService;
import com.youyilin.framework.utils.JwtUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 退出
 */
@Slf4j
@Component
public class MyLogoutSuccessHandler implements LogoutSuccessHandler {

    private final RedisService redisService;
    private final JwtUtils jwtUtils;

    public MyLogoutSuccessHandler(RedisService redisService, JwtUtils jwtUtils) {
        this.redisService = redisService;
        this.jwtUtils = jwtUtils;
    }

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        if (authentication != null) {
            // 框架退出
            new SecurityContextLogoutHandler().logout(request, response, authentication);

            try {
                LoginAdminEntity loginAdmin = (LoginAdminEntity) authentication.getPrincipal();

                String redisKey = RedisPrefix.TOKEN_COMPUTER_KEY_PREFIX;
                if (loginAdmin.isMobileLogin()) {
                    redisKey = RedisPrefix.TOKEN_MOBILE_KEY_PREFIX;
                }
                // 清除Redis缓存
                redisService.delete(redisKey + loginAdmin.getCreateTokenId());
            } catch (Exception e) {
                log.error("【用户退出】", e);
            }
        }

        ServletOutputStream outputStream = response.getOutputStream();
        response.setContentType("application/json;charset=UTF-8");
        response.setHeader(jwtUtils.getHeard(), "");
        R<String> result = R.success(null);

        outputStream.write(JSONObject.toJSONBytes(result));
        outputStream.flush();
        outputStream.close();
    }
}
