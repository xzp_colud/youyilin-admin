package com.youyilin.framework.security;

import com.alibaba.fastjson.JSONObject;
import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 权限不足
 */
@Slf4j
@Component
public class JwtAccessDeniedHandler implements AccessDeniedHandler {

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException {
        ServletOutputStream outputStream = response.getOutputStream();

        response.setContentType("application/json;charset=UTF-8");
        response.setStatus(HttpServletResponse.SC_FORBIDDEN);

        outputStream.write(JSONObject.toJSONBytes(R.fail(RMsg.NO_AUTHORITY.getMsg())));
        outputStream.flush();
        outputStream.close();
    }
}
