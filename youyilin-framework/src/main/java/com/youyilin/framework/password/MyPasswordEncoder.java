package com.youyilin.framework.password;

import com.youyilin.common.utils.MD5Util;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * spring security 密码管理
 */
@Component
public class MyPasswordEncoder implements PasswordEncoder {

    @Override
    public String encode(CharSequence charSequence) {
//        try {
//            return DigestUtils.md5DigestAsHex(charSequence.toString().getBytes("UTF-8"));
//        } catch (UnsupportedEncodingException e) {
//            return null;
//        }
        return MD5Util.md5Encrypt(charSequence.toString());
    }

    @Override
    public boolean matches(CharSequence charSequence, String s) {
        String password = this.encode(charSequence);
        return s.equals(password);
    }
}
