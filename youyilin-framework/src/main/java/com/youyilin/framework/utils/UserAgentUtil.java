package com.youyilin.framework.utils;

import eu.bitwalker.useragentutils.*;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;

/**
 * 移动端 电脑端 判断
 */
public class UserAgentUtil {
    /**
     * 判断 移动端/PC端
     *
     * @param request 请求头
     * @return boolean
     */
    public static boolean isMobile(HttpServletRequest request) {
        List<String> mobileAgents = Arrays.asList("ipad", "iphone os", "rv:1.2.3.4", "ucweb", "android", "windows ce", "windows mobile");
        String ua = request.getHeader("User-Agent").toLowerCase();
        for (String sua : mobileAgents) {
            if (ua.contains(sua)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断 移动端/PC端
     *
     * @param ua 浏览器标识
     * @return boolean
     */
    public static boolean isMobile(String ua) {
        if (StringUtils.isBlank(ua)) {
            return false;
        }
        List<String> mobileAgents = Arrays.asList("ipad", "iphone os", "rv:1.2.3.4", "ucweb", "android", "windows ce", "windows mobile");
        ua = ua.toLowerCase();
        for (String sua : mobileAgents) {
            if (ua.contains(sua)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 是否微信浏览器
     *
     * @param request 请求头
     * @return boolean
     */
    public static boolean isWechat(HttpServletRequest request) {
        String ua = request.getHeader("User-Agent").toLowerCase();
        return ua.contains("micromessenger");
    }

    /**
     * 初始化当前Request标识
     */
    public static MyUserAgent initUserAgent(HttpServletRequest request) {
        UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeader("User-Agent"));
        MyUserAgent myUserAgent = new MyUserAgent();

        // 浏览器
        Browser browser = userAgent.getBrowser();
        // 操作系统
        OperatingSystem os = userAgent.getOperatingSystem();
        // 设备
        DeviceType dt = os == null ? null : os.getDeviceType();
        // 版本
        Version bv = userAgent.getBrowserVersion();

        if (browser != null) myUserAgent.setBrowser(browser.getName());
        if (os != null) myUserAgent.setOs(os.getName());
        if (dt != null) myUserAgent.setDevice(dt.getName());
        if (bv != null) myUserAgent.setVersion(bv.getVersion());
        return myUserAgent;
    }

    public static void main(String[] args) {
        List<String> mobileAgents = Arrays.asList("ipad", "iphone os", "rv:1.2.3.4", "ucweb", "android", "windows ce", "windows mobile");
        String ua = "mozilla/5.0 (windows nt 10.0; win64; x64) applewebkit/537.36 (khtml, like gecko) chrome/65.0.3325.146 safari/537.36".toLowerCase();
        for (String sua : mobileAgents) {
            if (ua.contains(sua)) {
                System.out.println("移动端");
                return;
            }
        }
        System.out.println("PC端");
    }
}
