package com.youyilin.framework.utils;

import lombok.Data;

/**
 * 浏览器标识
 */
@Data
public class MyUserAgent {

    // 浏览器
    private String browser;
    // 操作系统
    private String os;
    // 版本
    private String version;
    // 设备
    private String device;
}
