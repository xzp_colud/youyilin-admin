package com.youyilin.framework.utils;

import com.alibaba.fastjson.JSONObject;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.Data;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;

@Data
@Component
@ConfigurationProperties(prefix = "jwt-token")
public class JwtUtils {

    private long expire;
    private String secret;
    private String heard;

    // 60分钟内自动刷新token
    private static final Long MILLIS_MINUTE_TEN = 60 * 60 * 1000L;

    public Date getExpireTime() {
        Date nowDate = new Date();
        return new Date(nowDate.getTime() + 1000 * expire);
    }

    /**
     * 构建 JWT
     *
     * @param username   用户
     * @param expireDate 过期时间
     */
    public String generateToken(String id, String username, Date expireDate) {
        return Jwts.builder()
                .setId(id)
                .setHeaderParam("typ", "JWT") // 主体
                .setSubject(username) // 所命令的主体
                .setIssuedAt(new Date()) // 创建时间
                .setExpiration(expireDate) // 过期时间
                .signWith(SignatureAlgorithm.HS512, secret) // 算法 秘钥
                .compact(); // 合成
    }

    /**
     * 验证 JWT
     */
    public String verifyToken(Date expireDate, String token) {
        if (expireDate == null) {
            return null;
        }
        long currentTime = System.currentTimeMillis();
        if (expireDate.getTime() - currentTime <= MILLIS_MINUTE_TEN) {
            return refreshToken(token);
        }
        return null;
    }

    /**
     * 刷新 JWT
     *
     * @param token 当前凭证
     */
    public String refreshToken(String token) {
        Claims claims = getClaimsByToken(token);
        return Jwts.builder()
                .signWith(SignatureAlgorithm.HS512, secret) //算法  秘钥
                .setClaims(claims)
                .setExpiration(getExpireTime())
                .compact();
    }

    /**
     * 解析 JWT
     *
     * @param jwt 当前凭证
     */
    public Claims getClaimsByToken(String jwt) {
        try {
            return Jwts.parser()            // 解析jwt
                    .setSigningKey(secret)  // 解析秘钥
                    .parseClaimsJws(jwt)    // 解析jwt
                    .getBody();             // 获得jwt 3个 字符串
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * JWT 是否过期
     */
    public boolean isTokenExpried(Claims claims) {
        return claims.getExpiration().before(new Date());
    }
}





