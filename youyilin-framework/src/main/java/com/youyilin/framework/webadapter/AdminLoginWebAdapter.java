package com.youyilin.framework.webadapter;

import com.youyilin.framework.password.MyPasswordEncoder;
import com.youyilin.framework.security.*;
import com.youyilin.framework.security.*;
import com.youyilin.framework.userdetails.UserDetailsAdminServiceImpl;
import com.youyilin.framework.config.SecurityConfig;
import com.youyilin.framework.filter.CaptchaValidateFilter;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.LogoutFilter;

/**
 * 管理后台 账号 + 密码登录
 */
@Configuration
@Order(4)
public class AdminLoginWebAdapter extends WebSecurityConfigurerAdapter {

    private final MyPasswordEncoder myPasswordEncoder;
    private final UserDetailsAdminServiceImpl userDetailsService;
    private final CaptchaValidateFilter captchaValidateFilter;
    private final JwtAuthenticationFilter jwtAuthenticationFilter;
    private final JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;
    private final JwtAccessDeniedHandler jwtAccessDeniedHandler;
    private final MyLoginSuccessHandler myLoginSuccessHandler;
    private final MyLoginFailureHandler myLoginFailureHandler;
    private final MyLogoutSuccessHandler myLogoutSuccessHandler;

    public AdminLoginWebAdapter(MyPasswordEncoder myPasswordEncoder, UserDetailsAdminServiceImpl userDetailsService,
                                CaptchaValidateFilter captchaValidateFilter, JwtAuthenticationFilter jwtAuthenticationFilter,
                                JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint, JwtAccessDeniedHandler jwtAccessDeniedHandler,
                                MyLoginSuccessHandler myLoginSuccessHandler, MyLoginFailureHandler myLoginFailureHandler,
                                MyLogoutSuccessHandler myLogoutSuccessHandler) {
        this.myPasswordEncoder = myPasswordEncoder;
        this.userDetailsService = userDetailsService;
        this.captchaValidateFilter = captchaValidateFilter;
        this.jwtAuthenticationFilter = jwtAuthenticationFilter;
        this.jwtAuthenticationEntryPoint = jwtAuthenticationEntryPoint;
        this.jwtAccessDeniedHandler = jwtAccessDeniedHandler;
        this.myLoginSuccessHandler = myLoginSuccessHandler;
        this.myLoginFailureHandler = myLoginFailureHandler;
        this.myLogoutSuccessHandler = myLogoutSuccessHandler;
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(myPasswordEncoder);
    }

    /**
     * 静态资源
     */
    @Override
    public void configure(WebSecurity web) throws Exception {
//        web.ignoring().mvcMatchers("/**.js");
    }

    /**
     * Http
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // 允许跨域
        http.cors().and()
                // 关闭预防工具
                .csrf().disable()

                // 登录
                .formLogin()
                .loginPage("/login")
                .loginProcessingUrl("/login")
                .successHandler(myLoginSuccessHandler)
                .failureHandler(myLoginFailureHandler)

                // 退出
                .and()
                .logout()
                .logoutSuccessHandler(myLogoutSuccessHandler)

                // 禁用 Session
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)

                // 异常处理器
                .and()
                .exceptionHandling()
                .authenticationEntryPoint(jwtAuthenticationEntryPoint)
                .accessDeniedHandler(jwtAccessDeniedHandler)

                // 拦截规则
                .and()
                .authorizeRequests()
                .antMatchers(SecurityConfig.URL_WHITELIST).permitAll()

                // 除了白名单其他都需要登录
                .anyRequest().authenticated()

                // 过滤器
                .and()
                .addFilterBefore(captchaValidateFilter, UsernamePasswordAuthenticationFilter.class)
                .addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class)
                .addFilterBefore(jwtAuthenticationFilter, LogoutFilter.class);
//                http.headers().frameOptions().disable();
    }
}
