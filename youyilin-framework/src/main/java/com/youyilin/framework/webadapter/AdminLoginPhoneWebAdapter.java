package com.youyilin.framework.webadapter;

import com.youyilin.framework.password.PhonePasswordEncoder;
import com.youyilin.framework.security.*;
import com.youyilin.framework.config.SecurityConfig;
import com.youyilin.framework.filter.SmsCodeValidateFilter;
import com.youyilin.framework.security.*;
import com.youyilin.framework.userdetails.UserDetailsAdminPhoneServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.LogoutFilter;

/**
 * 管理后台 手机号 + 验证码登录
 */
@Configuration
@Order(3)
public class AdminLoginPhoneWebAdapter extends WebSecurityConfigurerAdapter {

    private final PhonePasswordEncoder phonePasswordEncoder;
    private final UserDetailsAdminPhoneServiceImpl userDetailsService;
    private final MyLoginSuccessHandler loginSuccessHandler;
    private final MyLoginFailureHandler loginFailureHandler;
    private final JwtAuthenticationFilter jwtAuthenticationFilter;
    private final JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;
    private final JwtAccessDeniedHandler jwtAccessDeniedHandler;
    private final MyLogoutSuccessHandler jwtLogoutSuccessHandler;
    private final SmsCodeValidateFilter smsCodeValidateFilter;

    @Autowired
    public AdminLoginPhoneWebAdapter(UserDetailsAdminPhoneServiceImpl userDetailsService, MyLoginSuccessHandler loginSuccessHandler,
                                     MyLoginFailureHandler loginFailureHandler, JwtAuthenticationFilter jwtAuthenticationFilter,
                                     JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint, JwtAccessDeniedHandler jwtAccessDeniedHandler,
                                     MyLogoutSuccessHandler jwtLogoutSuccessHandler, SmsCodeValidateFilter smsCodeValidateFilter, PhonePasswordEncoder phonePasswordEncoder) {
        this.userDetailsService = userDetailsService;
        this.loginSuccessHandler = loginSuccessHandler;
        this.loginFailureHandler = loginFailureHandler;
        this.jwtAuthenticationFilter = jwtAuthenticationFilter;
        this.jwtAuthenticationEntryPoint = jwtAuthenticationEntryPoint;
        this.jwtAccessDeniedHandler = jwtAccessDeniedHandler;
        this.jwtLogoutSuccessHandler = jwtLogoutSuccessHandler;
        this.smsCodeValidateFilter = smsCodeValidateFilter;
        this.phonePasswordEncoder = phonePasswordEncoder;
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(phonePasswordEncoder);
    }

    /**
     * 静态资源
     */
    @Override
    public void configure(WebSecurity web) throws Exception {
//        web.ignoring().mvcMatchers("/**.js");
    }

    /**
     * Http
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
            /* 允许跨域 */
        http.cors().and()
                /* 关闭预防工具 */
                .csrf().disable()
                /* 只拦截会员登录 */
                .antMatcher("/login/phone")
                /* 登录配置 */
                .formLogin()
                .loginProcessingUrl("/login/phone")
                .successHandler(loginSuccessHandler)
                .failureHandler(loginFailureHandler)

                /* 退出配置 */
                .and()
                .logout()
                .logoutSuccessHandler(jwtLogoutSuccessHandler)

                /* 禁用session */
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)

                /* 配置异常处理器 */
                .and()
                .exceptionHandling()
                .authenticationEntryPoint(jwtAuthenticationEntryPoint)  //认证入口点 用户请求处理过程中遇到认证异常
                .accessDeniedHandler(jwtAccessDeniedHandler)            //权限不足处理器

                /* 配置拦截规则 */
                .and()
                .authorizeRequests()
                .antMatchers(SecurityConfig.URL_WHITELIST).permitAll()

                /* 其他需要登录 */
                .anyRequest().authenticated()

                //配置jwt过滤器
                .and()
                .addFilterBefore(smsCodeValidateFilter, UsernamePasswordAuthenticationFilter.class)
                .addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class)
                .addFilterBefore(jwtAuthenticationFilter, LogoutFilter.class);
    }
}
