package com.youyilin.customer.utils;

import com.youyilin.common.utils.DateUtils;
import org.apache.commons.lang3.RandomStringUtils;

public class SnUtil {

    // 账户流水单号
    public final static String ACCOUNT_LOG_PREFIX = "AW";

    public static synchronized String getCustomerMiniNickName() {
        return RandomStringUtils.randomNumeric(6);
    }

    public static synchronized String getSn() {
        String id = DateUtils.getDate("yyyyMMddHHmmss");
        String randomString = RandomStringUtils.randomNumeric(8);
        return id + randomString;
    }

    public static synchronized String getCustomerMiniSn() {
        return getSn();
    }

    public static synchronized String getAccountLogSn() {
        return ACCOUNT_LOG_PREFIX + getSn();
    }
}
