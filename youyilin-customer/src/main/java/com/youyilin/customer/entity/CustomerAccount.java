package com.youyilin.customer.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.youyilin.common.utils.FormatAmountUtil;
import com.youyilin.customer.enums.AccountTypeEnum;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 账户
 */
@Data
@Accessors(chain = true)
@TableName("customer_account")
public class CustomerAccount implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField(value = "create_date", fill = FieldFill.INSERT)
    private Date createDate;
    @TableField(fill = FieldFill.INSERT)
    private Long creator;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 客户/消费者ID
     */
    @TableField(value = "customer_id")
    private Long customerId;
    /**
     * 分类
     *
     * @see AccountTypeEnum
     */
    private Integer type;
    /**
     * 充值总额
     */
    private Long total;
    /**
     * 当前剩余
     */
    private Long residue;

    public String getTotalText() {
        return FormatAmountUtil.format(this.total);
    }

    public String getResidueText() {
        return FormatAmountUtil.format(this.residue);
    }

    public String getTypeText() {
        if (this.type == null) return "";

        return AccountTypeEnum.queryInfoByCode(this.type);
    }

    public boolean isBalance() {
        return this.type != null && (this.type == AccountTypeEnum.BALANCE.getCode() || this.type == AccountTypeEnum.MONTH.getCode());
    }
}
