package com.youyilin.customer.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 客户/消费者 地址
 */
@Data
@Accessors(chain = true)
@TableName("customer_address")
public class CustomerAddress implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 客户/消费者ID
     */
    @TableField(value = "customer_id")
    private Long customerId;
    /**
     * 收货人
     */
    @TableField(value = "receiver_name")
    private String receiverName;
    /**
     * 收货电话
     */
    @TableField(value = "receiver_phone")
    private String receiverPhone;
    /**
     * 省
     */
    private String province;
    /**
     * 市
     */
    private String city;
    /**
     * 区
     */
    private String area;
    /**
     * 详情
     */
    private String detail;
    /**
     * 默认标记
     */
    @TableField(value = "default_flag")
    private Integer defaultFlag;
    /**
     * 备注
     */
    private String remark;

    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;
}
