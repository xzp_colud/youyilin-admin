package com.youyilin.customer.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.FormatAmountUtil;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 购物车
 */
@Data
@Accessors(chain = true)
@TableName("customer_cart")
public class CustomerCart implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    @TableField(value = "create_date", fill = FieldFill.INSERT)
    private Date createDate;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;

    /**
     * 客户ID
     */
    @TableField(value = "customer_id")
    private Long customerId;
    /**
     * OPENID
     */
    private String openid;
    /**
     * 产品ID
     */
    @TableField(value = "product_id")
    private Long productId;
    /**
     * 产品名称
     */
    @TableField(value = "product_name")
    private String productName;
    /**
     * SKU ID
     */
    @TableField(value = "sku_id")
    private Long skuId;
    /**
     * SKU 名称
     */
    @TableField(value = "sku_name")
    private String skuName;
    /**
     * 图片
     */
    private String image;
    /**
     * 数量
     */
    private BigDecimal num;
    /**
     * 销售单价
     */
    private Long sellPrice;

    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;

    public String getSellPriceText() {
        return FormatAmountUtil.format(this.sellPrice);
    }
}
