package com.youyilin.customer.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.youyilin.customer.enums.AccountOptEnum;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 账户 日志
 */
@Data
@Accessors(chain = true)
@TableName("customer_account_log")
public class CustomerAccountLog implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 系统单号
     */
    private String sn;
    /**
     * 客户ID
     */
    @TableField(value = "customer_id")
    private Long customerId;
    /**
     * 账户ID
     */
    @TableField(value = "account_id")
    private Long accountId;
    /**
     * 订单ID
     */
    @TableField(value = "source_id")
    private Long sourceId;
    /**
     * 订单编号
     */
    @TableField(value = "source_no")
    private String sourceNo;
    /**
     * 操作类型
     *
     * @see AccountOptEnum
     */
    @TableField(value = "opt_type")
    private Integer optType;
    /**
     * 交易类型
     */
    @TableField(value = "pay_type")
    private String payType;
    /**
     * 起初数值
     */
    private Long original;
    /**
     * 操作数值
     */
    private Long opt;
    /**
     * 操作时间
     */
    @TableField(value = "opt_date")
    private Date optDate;
    /**
     * 剩余数值
     */
    private Long residue;
    /**
     * 备注
     */
    private String remark;
    /**
     * 图片
     */
    @TableField(value = "image_url")
    private String imageUrl;

    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;
}
