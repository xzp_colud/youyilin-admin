package com.youyilin.customer.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 客户/消费者
 */
@Data
@Accessors(chain = true)
public class Customer implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField(value = "create_date", fill = FieldFill.INSERT)
    private Date createDate;
    @TableField(fill = FieldFill.INSERT)
    private Long creator;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 用户名称
     */
    @TableField(value = "user_name")
    private String userName;
    /**
     * 密码
     */
    private String password;
    /**
     * 头像
     */
    @TableField(value = "avatar_url")
    private String avatarUrl;
    /**
     * 姓名
     */
    private String name;
    /**
     * 联系电话
     */
    private String phone;
    /**
     * 所在地
     */
    private String province;
    private String city;
    private String area;
    private String detail;
    /**
     * 性别
     */
    private String sex;
    /**
     * 备注/个人描述
     */
    private String autograph;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 是否可以使用商品批发价标记
     */
    @TableField(value = "wholesale_flag")
    private Integer wholesaleFlag;
    /**
     * 最后登录地
     */
    @TableField(value = "login_location")
    private String loginLocation;
    /**
     * 登录IP
     */
    @TableField(value = "login_ip")
    private String loginIp;
    /**
     * 登录时间
     */
    @TableField(value = "login_date")
    private Date loginDate;

    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;
}
