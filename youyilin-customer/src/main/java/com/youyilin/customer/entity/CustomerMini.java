package com.youyilin.customer.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.Date;

@Data
@Accessors(chain = true)
@TableName("customer_mini")
public class CustomerMini implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    @TableField(value = "create_date", fill = FieldFill.INSERT)
    private Date createDate;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 编号
     */
    private String sn;
    /**
     * 应用ID
     */
    @TableField(value = "app_id")
    private String appId;
    /**
     * 微信openid
     */
    private String openid;
    /**
     * 昵称
     */
    @JsonIgnore
    @TableField(value = "nick_name")
    private String nickName;
    /**
     * 省
     */
    private String province;
    /**
     * 市
     */
    private String city;
    /**
     * 区
     */
    private String area;
    /**
     * 头像
     */
    @TableField(value = "avatar_url")
    private String avatarUrl;
    /**
     * 手机号
     */
    private String phone;
    /**
     * 姓名
     */
    @TableField(value = "real_name")
    private String realName;
    /**
     * 是否为VIP
     */
    private Integer grade;
    /**
     * 上次登录的时间
     */
    @TableField(value = "last_login_date")
    private Date lastLoginDate;

    @JsonIgnore
    @TableLogic
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;

    public String getUserName() {
        String nn = StringUtils.isBlank(this.nickName) ? "" : this.nickName;

        if (this.grade == null) {
            return nn;
        }

        if (this.grade == 0) {
            return "游客 " + nn;
        }

        return "VIP " + nn;
    }
}
