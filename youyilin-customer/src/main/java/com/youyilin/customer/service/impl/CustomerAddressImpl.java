package com.youyilin.customer.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.enums.BooleanEnum;
import com.youyilin.common.exception.Assert;
import com.youyilin.customer.entity.CustomerAddress;
import com.youyilin.customer.mapper.CustomerAddressMapper;
import com.youyilin.customer.service.CustomerAddressService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 客户收货地址 服务实现类
 */
@Service
public class CustomerAddressImpl extends ServiceImpl<CustomerAddressMapper, CustomerAddress> implements CustomerAddressService {

    @Override
    public List<CustomerAddress> listByCustomerId(Long customerId) {
        return super.list(new LambdaQueryWrapper<CustomerAddress>()
                .eq(CustomerAddress::getCustomerId, customerId)
                .orderByDesc(CustomerAddress::getId));
    }

    @Override
    public CustomerAddress validateAddress(Long id) {
        CustomerAddress address = super.getById(id);
        Assert.notNull(address, "收货地址不存在");

        return address;
    }

    @Override
    public CustomerAddress validateAddress(Long id, Long customerId) {
        CustomerAddress address = this.validateAddress(id);
        boolean equalsFlag = address.getCustomerId().equals(customerId);
        Assert.isTrue(equalsFlag, "收货地址不存在");

        return address;
    }

    @Override
    public void saveCustomerAddress(CustomerAddress address) {
        if (address.getId() == null) {
            // 如果旧地址不是默认地址且新地址是默认地址，则取消旧地址的默认标志
            if (address.getDefaultFlag() == BooleanEnum.TRUE.getCode()) {
                this.updateDefaultCancel(address.getCustomerId());
            }
            // 保存地址
            super.save(address);
        } else {
            // 验证旧地址是否有效
            this.validateAddress(address.getId());
            // 如果旧地址不是默认地址且新地址是默认地址，则取消旧地址的默认标志
            if (address.getDefaultFlag() == BooleanEnum.TRUE.getCode()) {
                this.updateDefaultCancel(address.getCustomerId());
            }
            // 更新地址
            super.updateById(address);
        }
    }

    @Override
    public void updateDefaultCancel(Long customerId) {
        super.update(new LambdaUpdateWrapper<CustomerAddress>()
                .set(CustomerAddress::getDefaultFlag, BooleanEnum.FALSE.getCode())
                .eq(CustomerAddress::getCustomerId, customerId));
    }

    @Override
    public void updateDefault(Long id) {
        super.update(new LambdaUpdateWrapper<CustomerAddress>()
                .set(CustomerAddress::getDefaultFlag, BooleanEnum.TRUE.getCode())
                .eq(CustomerAddress::getId, id));
    }
}
