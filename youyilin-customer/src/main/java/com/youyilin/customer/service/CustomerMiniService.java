package com.youyilin.customer.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.common.bean.Page;
import com.youyilin.customer.entity.CustomerMini;

import java.util.List;

public interface CustomerMiniService extends IService<CustomerMini> {

    Integer getTotal(Page<CustomerMini> page);

    List<CustomerMini> getPage(Page<CustomerMini> page);

    /**
     * 是否登录
     */
    CustomerMini isLogin();

    /**
     * 是否登录
     */
    CustomerMini isLoginNoException();

    /**
     * 通过openid查询
     *
     * @param openid 微信openid
     */
    CustomerMini getByOpenid(String openid);

    /**
     * 更新头像 昵称
     */
    void updateInfo(CustomerMini cm);

    /**
     * 更新手机号
     */
    void updatePhone(Long id, String phone);
}
