package com.youyilin.customer.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.exception.Assert;
import com.youyilin.customer.entity.CustomerAccount;
import com.youyilin.customer.enums.AccountTypeEnum;
import com.youyilin.customer.mapper.CustomerAccountMapper;
import com.youyilin.customer.service.CustomerAccountService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 账户 服务实现类
 */
@Service
public class CustomerAccountImpl extends ServiceImpl<CustomerAccountMapper, CustomerAccount> implements CustomerAccountService {

    @Override
    public List<CustomerAccount> listByCustomerId(Long customerId) {
        return super.list(new LambdaQueryWrapper<CustomerAccount>().eq(CustomerAccount::getCustomerId, customerId));
    }

    @Override
    public CustomerAccount getByCustomerIdAndType(Long customerId, Integer type) {
        return super.getOne(new LambdaQueryWrapper<CustomerAccount>()
                .eq(CustomerAccount::getCustomerId, customerId)
                .eq(CustomerAccount::getType, type));
    }

    @Override
    public void initAccount(Long customerId) {
        for (AccountTypeEnum item : AccountTypeEnum.values()) {
            if (item.getCode() == AccountTypeEnum.MONTH.getCode()) {
                continue;
            }
            CustomerAccount accountUnique = this.getByCustomerIdAndType(customerId, item.getCode());
            if (accountUnique != null) {
                continue;
            }

            CustomerAccount insert = new CustomerAccount();
            insert.setCustomerId(customerId);
            insert.setTotal(0L);
            insert.setResidue(0L);
            insert.setType(item.getCode());

            super.save(insert);
        }
    }

    @Override
    public void validateUnique(CustomerAccount en) {
        CustomerAccount accountUnique = this.getByCustomerIdAndType(en.getCustomerId(), en.getType());
        Assert.isNull(accountUnique, "系统异常");
    }

    @Override
    public CustomerAccount validateAccount(Long accountId) {
        CustomerAccount account = super.getById(accountId);
        Assert.notNull(account, "账户不存在");

        return account;
    }

    @Override
    public CustomerAccount validateAccount(Long accountId, Long amount) {
        CustomerAccount customerAccount = this.validateAccount(accountId);

        if (customerAccount.getType().equals(AccountTypeEnum.BALANCE.getCode())) {
            // 断言：金额
            boolean amountFlag = amount != null && amount >= 0;
            Assert.isTrue(amountFlag, "消费金额异常");
            // 断言：金额是否充足
            boolean residueFlag = customerAccount.getResidue() <= 0 || amount > customerAccount.getResidue();
            Assert.isFalse(residueFlag, "账户金额不足");
        }
        return customerAccount;
    }

    @Override
    public void updateCharge(Long id, Long amount) {
        baseMapper.charge(id, amount);
    }

    @Override
    public void updateConsume(Long id, Long amount) {
        baseMapper.consume(id, amount);
    }

    @Override
    public void updateRefund(Long id, Long amount) {
        baseMapper.refund(id, amount);
    }
}
