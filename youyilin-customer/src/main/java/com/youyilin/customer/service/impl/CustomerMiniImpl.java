package com.youyilin.customer.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.vdurmont.emoji.EmojiParser;
import com.youyilin.common.bean.Page;
import com.youyilin.common.exception.ApiException;
import com.youyilin.common.utils.SecurityUtil;
import com.youyilin.customer.mapper.CustomerMiniMapper;
import com.youyilin.customer.entity.CustomerMini;
import com.youyilin.customer.service.CustomerMiniService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerMiniImpl extends ServiceImpl<CustomerMiniMapper, CustomerMini> implements CustomerMiniService {

    @Override
    public Integer getTotal(Page<CustomerMini> page) {
        return baseMapper.getTotal(page);
    }

    @Override
    public List<CustomerMini> getPage(Page<CustomerMini> page) {
        return baseMapper.getPage(page);
    }

    @Override
    public CustomerMini isLogin() {
        String openid = SecurityUtil.getOpenid();
        if (StringUtils.isBlank(openid)) {
            throw new ApiException("尚未登录");
        }
        CustomerMini cm = this.getByOpenid(openid);
        if (cm == null) {
            throw new ApiException("尚未登录");
        }
        return cm;
    }

    @Override
    public CustomerMini isLoginNoException() {
        String openid = SecurityUtil.getOpenid();
        if (StringUtils.isBlank(openid)) {
            return null;
        }
        return this.getByOpenid(openid);
    }

    @Override
    public CustomerMini getByOpenid(String openid) {
        return super.getOne(new LambdaQueryWrapper<CustomerMini>().eq(CustomerMini::getOpenid, openid));
    }

    @Override
    public void updateInfo(CustomerMini cm) {
        CustomerMini isLogin = this.isLoginNoException();
        if (isLogin == null) return;

        String nickName = StringUtils.isBlank(cm.getNickName()) ? null : EmojiParser.parseToAliases(cm.getNickName());
        String avatar = cm.getAvatarUrl();

        super.update(new LambdaUpdateWrapper<CustomerMini>()
                .set(CustomerMini::getNickName, nickName)
                .set(CustomerMini::getAvatarUrl, avatar)
                .eq(CustomerMini::getId, isLogin.getId()));
    }

    @Override
    public void updatePhone(Long id, String phone) {
        super.update(new LambdaUpdateWrapper<CustomerMini>()
                .set(CustomerMini::getPhone, phone)
                .set(CustomerMini::getGrade, 1)
                .eq(CustomerMini::getId, id));
    }
}
