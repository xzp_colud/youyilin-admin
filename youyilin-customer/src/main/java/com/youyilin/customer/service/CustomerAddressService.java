package com.youyilin.customer.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.customer.entity.CustomerAddress;

import java.util.List;

/**
 * 地址 服务类
 */
public interface CustomerAddressService extends IService<CustomerAddress> {

    /**
     * 某客户所有地址
     *
     * @param customerId 客户ID
     * @return ArrayList
     */
    List<CustomerAddress> listByCustomerId(Long customerId);

    /**
     * 验证地址是否存在
     *
     * @param id ID
     * @return CustomerAddress
     */
    CustomerAddress validateAddress(Long id);

    /**
     * 验证地址是否存在
     *
     * @param id         ID
     * @param customerId 客户ID
     * @return CustomerAddress
     */
    CustomerAddress validateAddress(Long id, Long customerId);

    /**
     * 保存
     */
    void saveCustomerAddress(CustomerAddress en);

    /**
     * 更新全部取消默认
     *
     * @param customerId 客户ID
     */
    void updateDefaultCancel(Long customerId);

    /**
     * 更新默认地址
     *
     * @param id 地址ID
     */
    void updateDefault(Long id);
}
