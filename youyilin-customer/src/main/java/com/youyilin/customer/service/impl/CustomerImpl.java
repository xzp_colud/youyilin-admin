package com.youyilin.customer.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.enums.StatusEnum;
import com.youyilin.common.exception.Assert;
import com.youyilin.common.utils.ObjectUniqueUtil;
import com.youyilin.customer.entity.Customer;
import com.youyilin.customer.mapper.CustomerMapper;
import com.youyilin.customer.service.CustomerService;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.nio.charset.StandardCharsets;

/**
 * 客户/消费者 服务实现类
 */
@Service
public class CustomerImpl extends ServiceImpl<CustomerMapper, Customer> implements CustomerService {

    @Override
    public Customer getByUserName(String userName) {
        return super.getOne(new LambdaUpdateWrapper<Customer>().eq(Customer::getUserName, userName));
    }

    @Override
    public Customer getByPhone(String phone) {
        return super.getOne(new LambdaUpdateWrapper<Customer>().eq(Customer::getPhone, phone));
    }

    @Override
    public String getResetPassword(String params) {
        return this.resetPassword(params);
    }

    @Override
    public Customer validateCustomer(Long id) {
        Customer customer = super.getById(id);
        // 断言
        boolean isTrue = customer != null && customer.getStatus() != null && customer.getStatus() == StatusEnum.NORMAL.getCode();
        Assert.isTrue(isTrue, "客户不存在或当前禁用");

        return customer;
    }

    /**
     * 重置密码
     */
    private String resetPassword(String phone) {
        return DigestUtils.md5DigestAsHex(phone.getBytes(StandardCharsets.UTF_8)).toUpperCase();
    }

    @Override
    public void validateUserNameUnique(Customer en) {
        Customer userNameUnique = this.getByUserName(en.getUserName());
        Assert.isTrue(ObjectUniqueUtil.validateObjectUnique(en, userNameUnique), "用户名已存在");
    }

    @Override
    public void validatePhoneUnique(Customer en) {
        Customer phoneUnique = this.getByPhone(en.getPhone());
        Assert.isTrue(ObjectUniqueUtil.validateObjectUnique(en, phoneUnique), "联系电话已存在");
    }
}
