package com.youyilin.customer.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.customer.entity.CustomerCart;

import java.util.List;

public interface CustomerCartService extends IService<CustomerCart> {

    /**
     * 统计
     */
    Long countByOpenid(String openid);

    /**
     * 按用户id查询
     *
     * @param customerId 用户ID
     * @return Array
     */
    List<CustomerCart> listByCustomerId(Long customerId);

    /**
     * 按openid查询
     *
     * @param openid 微信openid
     * @return Array
     */
    List<CustomerCart> listByOpenid(String openid);
}
