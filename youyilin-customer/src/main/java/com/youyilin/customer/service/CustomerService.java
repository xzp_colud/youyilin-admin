package com.youyilin.customer.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.customer.entity.Customer;

/**
 * 客户/消费者 服务类
 */
public interface CustomerService extends IService<Customer> {

    /**
     * 通过用户名查询
     *
     * @param userName 用户名
     * @return Object
     */
    Customer getByUserName(String userName);

    /**
     * 通过手机号查询
     *
     * @param phone 手机号
     * @return Object
     */
    Customer getByPhone(String phone);

    /**
     * 获取密码
     *
     * @param params 参数
     * @return String
     */
    String getResetPassword(String params);

    /**
     * 验证客户
     *
     * @param id ID
     * @return Customer
     */
    Customer validateCustomer(Long id);

    /**
     * 验证用户名唯一性
     */
    void validateUserNameUnique(Customer en);

    /**
     * 验证手机号唯一性
     */
    void validatePhoneUnique(Customer en);
}
