package com.youyilin.customer.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.customer.entity.CustomerAccount;

import java.util.List;

/**
 * 账户 服务类
 */
public interface CustomerAccountService extends IService<CustomerAccount> {

    /**
     * 某客户所有账户
     *
     * @param customerId 客户ID
     * @return ArrayList
     */
    List<CustomerAccount> listByCustomerId(Long customerId);

    /**
     * 某客户某类型的账户
     *
     * @param customerId 客户ID
     * @param type       类型
     * @return Object
     */
    CustomerAccount getByCustomerIdAndType(Long customerId, Integer type);

    /**
     * 初始化账户
     */
    void initAccount(Long customerId);

    /**
     * 验证账户唯一性
     */
    void validateUnique(CustomerAccount en);

    /**
     * 验证账户
     *
     * @param accountId 账户ID
     * @return CustomerAccount
     */
    CustomerAccount validateAccount(Long accountId);

    /**
     * 验证账户金额是否充足
     *
     * @param accountId 账户ID
     * @param amount    金额
     * @return CustomerAccount
     */
    CustomerAccount validateAccount(Long accountId, Long amount);

    /**
     * 修改账户金额
     *
     * @param id     账户ID
     * @param amount 修改金额
     */
    void updateCharge(Long id, Long amount);

    /**
     * 修改账户金额
     *
     * @param id     账户ID
     * @param amount 修改金额
     */
    void updateConsume(Long id, Long amount);

    /**
     * 修改账户金额
     *
     * @param id     账户ID
     * @param amount 修改金额
     */
    void updateRefund(Long id, Long amount);
}
