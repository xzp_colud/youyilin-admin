package com.youyilin.customer.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.customer.entity.CustomerAccountLog;

import java.util.Date;

/**
 * 账户 日志 服务类
 */
public interface CustomerAccountLogService extends IService<CustomerAccountLog> {

    /**
     * 创建Log对象
     */
    CustomerAccountLog createLog(Long customerId, Long accountId, Integer optType, Date optDate, Long amount, Long sourceAmount, String remark, String imageUrl);

    /**
     * 保存
     */
    void saveLog(CustomerAccountLog en);
}
