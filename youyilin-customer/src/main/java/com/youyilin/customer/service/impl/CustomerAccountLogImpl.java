package com.youyilin.customer.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.exception.Assert;
import com.youyilin.customer.entity.CustomerAccountLog;
import com.youyilin.customer.mapper.CustomerAccountLogMapper;
import com.youyilin.customer.service.CustomerAccountLogService;
import com.youyilin.customer.utils.SnUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * 账户 日志 服务实现类
 */
@Slf4j
@Service
public class CustomerAccountLogImpl extends ServiceImpl<CustomerAccountLogMapper, CustomerAccountLog> implements CustomerAccountLogService {

    @Override
    public CustomerAccountLog createLog(Long customerId, Long accountId, Integer optType, Date optDate, Long amount, Long sourceAmount, String remark, String imageUrl) {
        CustomerAccountLog log = new CustomerAccountLog();
        log.setCustomerId(customerId)
                .setAccountId(accountId)
                .setOptType(optType)
                .setOptDate(optDate)
                .setOpt(amount)
                .setOriginal(sourceAmount)
                .setRemark(remark)
                .setImageUrl(imageUrl);
        return log;
    }

    @Override
    public void saveLog(CustomerAccountLog en) {
        this.validateProperty(en);
        en.setSn(SnUtil.getAccountLogSn());
        en.setResidue(en.getOriginal() + en.getOpt());
        super.save(en);
    }

    private void validateProperty(CustomerAccountLog en) {
        Assert.notNull(en.getCustomerId(), "操作账户异常");
        Assert.notNull(en.getAccountId(), "操作账户异常");
        Assert.notNull(en.getOpt(), "操作值不能为空");
        Assert.notNull(en.getOriginal(), "初始值不能为空");
    }
}
