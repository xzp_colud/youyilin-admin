package com.youyilin.customer.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.customer.mapper.CustomerCartMapper;
import com.youyilin.customer.entity.CustomerCart;
import com.youyilin.customer.service.CustomerCartService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerCartImpl extends ServiceImpl<CustomerCartMapper, CustomerCart> implements CustomerCartService {

    @Override
    public Long countByOpenid(String openid) {
        return super.count(new LambdaQueryWrapper<CustomerCart>().eq(CustomerCart::getOpenid, openid));
    }

    @Override
    public List<CustomerCart> listByCustomerId(Long customerId) {
        return super.list(new LambdaQueryWrapper<CustomerCart>().eq(CustomerCart::getCustomerId, customerId));
    }

    @Override
    public List<CustomerCart> listByOpenid(String openid) {
        return super.list(new LambdaQueryWrapper<CustomerCart>().eq(CustomerCart::getOpenid, openid).orderByDesc(CustomerCart::getId));
    }
}
