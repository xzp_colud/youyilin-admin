package com.youyilin.customer.bo;

import com.youyilin.customer.enums.AccountTypeEnum;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 客户账户消费BO
 */
@Data
@Accessors(chain = true)
public class CustomerAccountConsumeBO {

    private Long sourceId;
    private String sourceNo;
    private Long customerId;
    private Long accountId;
    private Long amount;
    private String remark;
    private Integer accountType;

    private Long residue;
    private Integer orderFinanceFlag;

    public String getPayType() {
        if (this.accountType == null) return "";

        return AccountTypeEnum.queryInfoByCode(this.accountType);
    }
}
