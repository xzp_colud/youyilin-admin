package com.youyilin.customer.bo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 客户欠款账户充值BO
 */
@Data
@Accessors(chain = true)
public class CustomerAccountMonthChargeBO {

    // 源单ID
    private Long sourceId;
    // 源单号
    private String sourceSn;
    // 客户
    private Long customerId;
    // 金额
    private Long amount;
    // 方式
    private String payType;
    // 时间
    private Date optDate;
    // 备注
    private String remark;
    // 图片
    private String imageUrl;
}
