package com.youyilin.customer.bo;

import com.youyilin.customer.enums.AccountTypeEnum;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 客户账户退款BO
 */
@Data
@Accessors(chain = true)
public class CustomerAccountRefundBO {

    // 源单ID
    private Long sourceId;
    // 源单号
    private String sourceNo;
    // 客户ID
    private Long customerId;
    // 账户ID
    private Long accountId;
    // 退款金额
    private Long amount;
    // 备注
    private String remark;
    // 账户类型
    private Integer accountType;
    // 剩余
    private Long residue;

    public String getPayType() {
        if (this.accountType == null) return "";

        return AccountTypeEnum.queryInfoByCode(this.accountType);
    }
}
