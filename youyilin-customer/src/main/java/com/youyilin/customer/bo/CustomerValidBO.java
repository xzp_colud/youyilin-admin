package com.youyilin.customer.bo;

import com.youyilin.customer.entity.Customer;
import com.youyilin.customer.entity.CustomerAddress;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 客户验证BO
 */
@Data
@Accessors(chain = true)
public class CustomerValidBO {

    // 客户ID
    private Long customerId;
    // 客户地址
    private Long addressId;
    // 客户信息
    private Customer customer;
    // 客户地址
    private CustomerAddress address;
}
