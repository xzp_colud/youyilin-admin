package com.youyilin.customer.logic.impl;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.youyilin.customer.entity.CustomerMini;
import com.youyilin.customer.service.CustomerAccountService;
import com.youyilin.customer.service.CustomerMiniService;
import com.youyilin.customer.utils.SnUtil;
import com.youyilin.customer.logic.CustomerMiniLogic;
import com.youyilin.wechat.service.WechatTokenService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
public class CustomerMiniLogicImpl implements CustomerMiniLogic {

    private final WechatTokenService wechatTokenService;
    private final CustomerMiniService customerMiniService;
    private final CustomerAccountService customerAccountService;

    public CustomerMiniLogicImpl(WechatTokenService wechatTokenService, CustomerMiniService customerMiniService,
                                 CustomerAccountService customerAccountService) {
        this.wechatTokenService = wechatTokenService;
        this.customerMiniService = customerMiniService;
        this.customerAccountService = customerAccountService;
    }

    @Override
    public CustomerMini getUserInfo() {
        CustomerMini isLogin = customerMiniService.isLoginNoException();
        if (isLogin == null) return null;

        CustomerMini cm = new CustomerMini();
        cm.setAvatarUrl(isLogin.getAvatarUrl());
        cm.setPhone(isLogin.getPhone());
        cm.setRealName(isLogin.getRealName());
        cm.setGrade(isLogin.getGrade());
        cm.setNickName(isLogin.getNickName());

        return cm;
    }

    @Override
    @Transactional
    public CustomerMini registerByOpenid(String appId, String code) {
        String openid = wechatTokenService.getCodeSession(appId, code);
        CustomerMini cm = customerMiniService.getByOpenid(openid);
        if (cm != null) {
            customerAccountService.initAccount(cm.getId());
            customerMiniService.update(new LambdaUpdateWrapper<CustomerMini>().set(CustomerMini::getLastLoginDate, new Date()).eq(CustomerMini::getId, cm.getId()));
            return cm;
        }

        CustomerMini insertCm = new CustomerMini();
        insertCm.setNickName(SnUtil.getCustomerMiniNickName());
        insertCm.setAppId(appId);
        insertCm.setOpenid(openid);
        insertCm.setGrade(0);
        insertCm.setSn(SnUtil.getCustomerMiniSn());
        insertCm.setLastLoginDate(new Date());

        // 保存
        customerMiniService.save(insertCm);
        // 刷新账户
        customerAccountService.initAccount(insertCm.getId());

        return insertCm;
    }

    @Override
    public void updatePhone(String code) {
        CustomerMini isLogin = customerMiniService.isLoginNoException();
        if (isLogin == null) return;

        String phoneNumber = wechatTokenService.getPhoneNumber(isLogin.getAppId(), code);
        if (StringUtils.isBlank(phoneNumber)) return;

        if (phoneNumber.equals(isLogin.getPhone()) || (isLogin.getGrade() != null && isLogin.getGrade() > 0)) {
            return;
        }

        customerMiniService.updatePhone(isLogin.getId(), phoneNumber);
    }
}
