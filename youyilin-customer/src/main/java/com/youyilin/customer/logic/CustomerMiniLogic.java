package com.youyilin.customer.logic;

import com.youyilin.customer.entity.CustomerMini;

public interface CustomerMiniLogic {

    /**
     * 获取信息
     */
    CustomerMini getUserInfo();

    /**
     * 通过openid注册
     *
     * @param appId  应用ID
     * @param openid 微信openid
     */
    CustomerMini registerByOpenid(String appId, String openid);

    /**
     * 更新手机号
     */
    void updatePhone(String code);
}
