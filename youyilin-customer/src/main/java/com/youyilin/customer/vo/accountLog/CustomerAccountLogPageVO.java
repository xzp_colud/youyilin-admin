package com.youyilin.customer.vo.accountLog;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.FormatAmountUtil;
import com.youyilin.customer.enums.AccountOptEnum;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 账户日志列表VO
 */
@Data
@Accessors(chain = true)
public class CustomerAccountLogPageVO {
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 流水号
    private String sn;
    // 时间
    private Date modifyDate;
    // 客户名称
    private String userName;
    // 来源单号
    private String sourceNo;
    // 操作类型
    private Integer optType;
    // 交易类型
    private String payType;
    // 起初数值
    private Long original;
    // 操作数值
    private Long opt;
    // 操作时间
    private Date optDate;
    // 剩余数值
    private Long residue;
    // 备注
    private String remark;
    // 图片
    private String imageUrl;

    public String getOriginalText() {
        return FormatAmountUtil.format(this.original);
    }

    public String getOptText() {
        return FormatAmountUtil.format(this.opt);

    }

    public String getResidueText() {
        return FormatAmountUtil.format(this.residue);

    }

    public String getOptTypeText() {
        if (this.optType == null) return "";

        return AccountOptEnum.queryInfoByCode(this.optType);
    }
}
