package com.youyilin.customer.vo.customer;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.customer.entity.Customer;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 客户编辑VO
 */
@Data
@Accessors(chain = true)
public class CustomerEditVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 用户名称
    private String userName;
    // 头像
    private String avatarUrl;
    // 姓名
    private String name;
    // 联系电话
    private String phone;
    // 所在地
    private String province;
    private String city;
    private String area;
    private String detail;
    // 性别
    private String sex;
    // 备注/描述
    private String autograph;
    // 状态
    private Integer status;
    // 批发标记
    private Integer wholesaleFlag;

    public static CustomerEditVO convertByEntity(Customer customer) {
        return BeanHelper.map(customer, CustomerEditVO.class);
    }
}
