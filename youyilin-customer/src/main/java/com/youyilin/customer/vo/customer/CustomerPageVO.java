package com.youyilin.customer.vo.customer;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 客户分页列表VO
 */
@Data
@Accessors(chain = true)
public class CustomerPageVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 创建时间
    private Date createDate;
    // 修改时间
    private Date modifyDate;
    // 用户名称
    private String userName;
    // 头像
    private String avatarUrl;
    // 姓名
    private String name;
    // 联系电话
    private String phone;
    // 所在地
    private String province;
    private String city;
    private String area;
    private String detail;
    // 性别
    private String sex;
    // 备注/个人描述
    private String autograph;
    // 状态
    private Integer status;
    // 登录地
    private String loginLocation;
    // 登录IP
    private String loginIp;
    // 登录时间
    private Date loginDate;
    // 使用批发价标识
    private Integer wholesaleFlag;
}
