package com.youyilin.customer.vo.account;

import lombok.Data;

/**
 * 小程序(我的)账户金额显示VO
 */
@Data
public class CustomerMiniAccountGridVO {

    private String label;
    private String value;
}
