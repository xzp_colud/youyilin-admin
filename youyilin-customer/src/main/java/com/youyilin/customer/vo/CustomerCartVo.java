package com.youyilin.customer.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.FormatAmountUtil;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class CustomerCartVo {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    private String productName;
    private String skuName;
    private String image;
    private BigDecimal num;
    @JsonIgnore
    private Long sellAmount;
    private boolean isSell;

    public String getSellAmountText() {
        return FormatAmountUtil.format(this.sellAmount);
    }
}
