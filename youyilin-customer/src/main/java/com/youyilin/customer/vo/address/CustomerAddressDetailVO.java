package com.youyilin.customer.vo.address;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.customer.entity.CustomerAddress;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 客户收货地址详情VO
 */
@Data
@Accessors(chain = true)
public class CustomerAddressDetailVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 收货人
    private String receiverName;
    // 收货电话
    private String receiverPhone;
    // 省
    private String province;
    // 市
    private String city;
    // 区
    private String area;
    /// 详情
    private String detail;
    // 默认标记
    private Integer defaultFlag;
    // 备注
    private String remark;

    public static CustomerAddressDetailVO convertByEntity(CustomerAddress address) {
        return BeanHelper.map(address, CustomerAddressDetailVO.class);
    }
}
