package com.youyilin.customer.vo.account;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.common.utils.FormatAmountUtil;
import com.youyilin.customer.enums.AccountTypeEnum;
import com.youyilin.customer.entity.CustomerAccount;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;

/**
 * 客户账户列表VO
 */
@Data
@Accessors(chain = true)
public class CustomerAccountPageVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 创建时间
    private Date createDate;
    // 修改时间
    private Date modifyDate;
    // 客户名称
    private String userName;
    // 分类
    private Integer type;
    // 总额
    private Long total;
    // 当前剩余
    private Long residue;

    public String getTotalText() {
        return FormatAmountUtil.format(this.total);
    }

    public String getResidueText() {
        return FormatAmountUtil.format(this.residue);
    }

    public String getTypeText() {
        if (this.type == null) return "";

        return AccountTypeEnum.queryInfoByCode(this.type);
    }

    public static List<CustomerAccountPageVO> convertByEntity(List<CustomerAccount> accountList) {
        return BeanHelper.map(accountList, CustomerAccountPageVO.class);
    }
}
