package com.youyilin.customer.vo.address;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.customer.entity.CustomerAddress;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 客户收货地址编辑VO
 */
@Data
@Accessors(chain = true)
public class CustomerAddressEditVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long customerId;
    // 收货人
    private String receiverName;
    // 收货电话
    private String receiverPhone;
    // 省
    private String province;
    // 市
    private String city;
    // 区
    private String area;
    /// 详情
    private String detail;
    // 默认标记
    private Integer defaultFlag;
    // 备注
    private String remark;

    public static CustomerAddressEditVO convertByEntity(CustomerAddress address) {
        return BeanHelper.map(address, CustomerAddressEditVO.class);
    }
}
