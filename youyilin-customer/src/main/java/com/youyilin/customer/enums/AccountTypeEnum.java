package com.youyilin.customer.enums;

/**
 * 账户分类
 */
public enum AccountTypeEnum {

    BALANCE(1, "余额"),
    MONTH(2,"欠款"),
    INTEGRAL(3, "积分");

    private int code;
    private String info;

    AccountTypeEnum(int code, String info) {
        this.code = code;
        this.info = info;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public static boolean isExist(int code) {
        for (AccountTypeEnum t : AccountTypeEnum.values()) {
            if (t.getCode() == code) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    public static String queryInfoByCode(int code) {
        for (AccountTypeEnum t : AccountTypeEnum.values()) {
            if (t.getCode() == code) {
                return t.getInfo();
            }
        }
        return null;
    }
}
