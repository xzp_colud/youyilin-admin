package com.youyilin.customer.enums;

/**
 * 账户操作类型
 */
public enum AccountOptEnum {

    REFUND(-1, "退款"),
    CONSUME(0, "消费"),
    RECHARGE(1, "充值"),
    IN(2, "转入"),
    OUT(3, "转出"),
    CLEAR(4, "清空");

    private int code;
    private String info;

    AccountOptEnum(int code, String info) {
        this.code = code;
        this.info = info;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public static boolean isExist(int code) {
        for (AccountOptEnum t : AccountOptEnum.values()) {
            if (t.getCode() == code) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    public static String queryInfoByCode(int code) {
        for (AccountOptEnum t : AccountOptEnum.values()) {
            if (t.getCode() == code) {
                return t.getInfo();
            }
        }
        return null;
    }
}
