package com.youyilin.customer.manager;

import com.youyilin.common.exception.ApiException;
import com.youyilin.common.exception.Assert;
import com.youyilin.customer.bo.CustomerAccountConsumeBO;
import com.youyilin.customer.bo.CustomerAccountMonthChargeBO;
import com.youyilin.customer.bo.CustomerAccountRefundBO;
import com.youyilin.customer.bo.CustomerValidBO;
import com.youyilin.customer.entity.Customer;
import com.youyilin.customer.entity.CustomerAccount;
import com.youyilin.customer.entity.CustomerAccountLog;
import com.youyilin.customer.entity.CustomerAddress;
import com.youyilin.customer.enums.AccountOptEnum;
import com.youyilin.customer.enums.AccountTypeEnum;
import com.youyilin.customer.service.CustomerAccountLogService;
import com.youyilin.customer.service.CustomerAccountService;
import com.youyilin.customer.service.CustomerAddressService;
import com.youyilin.customer.service.CustomerService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class CustomerManagerImpl implements CustomerManager {

    private final CustomerService customerService;
    private final CustomerAddressService customerAddressService;
    private final CustomerAccountService customerAccountService;
    private final CustomerAccountLogService customerAccountLogService;

    public CustomerManagerImpl(CustomerService customerService,
                               CustomerAddressService customerAddressService,
                               CustomerAccountService customerAccountService,
                               CustomerAccountLogService customerAccountLogService) {
        this.customerService = customerService;
        this.customerAddressService = customerAddressService;
        this.customerAccountService = customerAccountService;
        this.customerAccountLogService = customerAccountLogService;
    }

    @Override
    public void validate(CustomerValidBO bo) {
        // 客户ID
        Long customerId = bo.getCustomerId();
        // 客户地址ID
        Long addressId = bo.getAddressId();
        // 必要参数验证
        Assert.notNull(customerId, "客户信息异常");
        Assert.notNull(addressId, "客户信息异常");
        // 获取客户信息
        Customer customer = customerService.validateCustomer(customerId);
        // 获取客户地址信息
        CustomerAddress address = customerAddressService.validateAddress(addressId, customerId);
        // 设置客户信息和地址信息
        bo.setCustomer(customer).setAddress(address);
    }

    @Override
    public void updateAccountChargeMonth(CustomerAccountMonthChargeBO bo) {
        if (bo.getSourceId() == null
                || StringUtils.isBlank(bo.getSourceSn())
                || bo.getAmount() == null
                || bo.getAmount() <= 0
                || bo.getCustomerId() == null
                || StringUtils.isBlank(bo.getPayType())
                || StringUtils.isBlank(bo.getRemark())) {
            throw new ApiException("参数异常");
        }
        // 验证客户
        Customer customer = customerService.validateCustomer(bo.getCustomerId());
        // 验证账户
        CustomerAccount account = customerAccountService.getByCustomerIdAndType(customer.getId(), AccountTypeEnum.MONTH.getCode());
        Assert.notNull(account, "账户不存在");
        // 充值
        customerAccountService.updateCharge(account.getId(), bo.getAmount());

        // 日志
        CustomerAccountLog log = customerAccountLogService.createLog(account.getCustomerId(), account.getId(), AccountOptEnum.RECHARGE.getCode(), bo.getOptDate(), bo.getAmount(), account.getResidue(), bo.getRemark(), bo.getImageUrl());
        log.setPayType(bo.getPayType())
                .setSourceId(bo.getSourceId())
                .setSourceNo(bo.getSourceSn());
        customerAccountLogService.saveLog(log);
    }

    @Override
    public void updateAccountConsume(CustomerAccountConsumeBO bo) {
        if (bo.getSourceId() == null
                || StringUtils.isBlank(bo.getSourceNo())
                || bo.getAmount() == null
                || bo.getAmount() <= 0
                || bo.getCustomerId() == null
                || bo.getAccountId() == null
                || StringUtils.isBlank(bo.getRemark())) {
            throw new ApiException("参数异常");
        }
        // 验证客户
        Customer customer = customerService.validateCustomer(bo.getCustomerId());
        // 验证客户账户
        CustomerAccount account = customerAccountService.validateAccount(bo.getAccountId(), bo.getAmount());
        // 断言：账户
        boolean equalsFlag = customer.getId().equals(account.getCustomerId());
        Assert.isTrue(equalsFlag, "账户不一致");
        // 断言：支付类型
        boolean isNotBalance = account.getType().equals(AccountTypeEnum.BALANCE.getCode());
        Assert.isFalse(isNotBalance, "支付异常");
        // 结算标记
        int orderFinanceFlag = 0;
        if (account.getType().equals(AccountTypeEnum.BALANCE.getCode())) {
            orderFinanceFlag = 1;
        }
        bo.setAccountType(account.getType());
        bo.setResidue(account.getResidue());
        bo.setOrderFinanceFlag(orderFinanceFlag);
        // 更新账户金额
        customerAccountService.updateConsume(account.getId(), bo.getAmount());

        // 日志
        CustomerAccountLog log = customerAccountLogService.createLog(bo.getCustomerId(), bo.getAccountId(), AccountOptEnum.CONSUME.getCode(), new Date(), -bo.getAmount(), bo.getResidue(), bo.getRemark(), null);
        log.setSourceId(bo.getSourceId())
                .setSourceNo(bo.getSourceNo())
                .setPayType(bo.getPayType());
        customerAccountLogService.saveLog(log);
    }

    @Override
    public void updateAccountRefund(CustomerAccountRefundBO bo) {
        if (bo.getSourceId() == null
                || StringUtils.isBlank(bo.getSourceNo())
                || bo.getAmount() == null
                || bo.getAmount() <= 0
                || bo.getCustomerId() == null
                || bo.getAccountId() == null
                || StringUtils.isBlank(bo.getRemark())) {
            throw new ApiException("参数异常");
        }
        // 验证客户
        Customer customer = customerService.validateCustomer(bo.getCustomerId());
        // 验证客户账户
        CustomerAccount account = customerAccountService.validateAccount(bo.getAccountId());
        // 断言：账户一致
        boolean equalsFlag = customer.getId().equals(account.getCustomerId());
        Assert.isTrue(equalsFlag, "账户不一致");
        bo.setAccountType(account.getType());
        bo.setResidue(account.getResidue());
        // 更新账户金额
        customerAccountService.updateRefund(account.getId(), bo.getAmount());
        // 日志
        CustomerAccountLog log = customerAccountLogService.createLog(bo.getCustomerId(), bo.getAccountId(), AccountOptEnum.REFUND.getCode(), new Date(), bo.getAmount(), bo.getResidue(), bo.getRemark(), null);
        log.setPayType(bo.getPayType());
        customerAccountLogService.saveLog(log);
    }
}
