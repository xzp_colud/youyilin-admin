package com.youyilin.customer.manager;

import com.youyilin.customer.bo.CustomerAccountMonthChargeBO;
import com.youyilin.customer.bo.CustomerValidBO;
import com.youyilin.customer.bo.CustomerAccountConsumeBO;
import com.youyilin.customer.bo.CustomerAccountRefundBO;

public interface CustomerManager {

    /**
     * 验证
     */
    void validate(CustomerValidBO bo);

    /**
     * 欠款账户充值
     */
    void updateAccountChargeMonth(CustomerAccountMonthChargeBO bo);

    /**
     * 客户账户消费
     */
    void updateAccountConsume(CustomerAccountConsumeBO dto);

    /**
     * 客户账户退款
     */
    void updateAccountRefund(CustomerAccountRefundBO dto);
}
