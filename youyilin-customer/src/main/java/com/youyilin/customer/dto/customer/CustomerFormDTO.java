package com.youyilin.customer.dto.customer;

import com.youyilin.common.utils.BeanHelper;
import com.youyilin.customer.entity.Customer;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 客户表单DTO
 */
@Data
public class CustomerFormDTO {

    private Long id;
    @NotBlank(message = "用户名称不能为空")
    @Size(max = 16, message = "用户名称长度不能超过16字符")
    private String userName;

    @NotBlank(message = "姓名不能为空")
    @Size(max = 64, message = "用户名称长度不能超过64字符")
    private String name;

    @NotBlank(message = "联系电话不能为空")
    @Size(max = 11, message = "用户名称长度不能超过11字符")
    private String phone;

    @NotBlank(message = "地址不能为空")
    private String province;
    @NotBlank(message = "地址不能为空")
    private String city;
    @NotBlank(message = "地址不能为空")
    private String area;
    @NotNull(message = "地址不能为空")
    @Size(max = 128, message = "地址长度不能超过128字符")
    private String detail;

    private String avatarUrl;
    private String sex;
    private String autograph;
    private Integer status;
    private Integer wholesaleFlag;

    public Customer convertToEntity() {
        return BeanHelper.map(this, Customer.class);
    }
}
