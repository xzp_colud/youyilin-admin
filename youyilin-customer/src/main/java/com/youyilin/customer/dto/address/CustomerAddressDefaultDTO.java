package com.youyilin.customer.dto.address;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 地址默认表单DTO
 */
@Data
public class CustomerAddressDefaultDTO {

    @NotNull(message = "参数异常")
    private Long id;
}
