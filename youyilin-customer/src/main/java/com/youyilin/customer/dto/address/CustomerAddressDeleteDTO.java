package com.youyilin.customer.dto.address;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 地址删除表单DTO
 */
@Data
public class CustomerAddressDeleteDTO {

    @NotNull(message = "参数异常")
    private Long id;
}
