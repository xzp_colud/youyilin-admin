package com.youyilin.customer.dto.request;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 订单 客户
 */
@Data
public class RequestOrderCustomer {

    /**
     * 客户ID
     */
    @NotNull(message = "客户不能为空")
    private Long customerId;
    /**
     * 地址ID
     */
    @NotNull(message = "客户地址不能为空")
    private Long addressId;

    // 冗余
    private String customerName;
    private String customerPhone;
    private String province;

    private String receiverName;
    private String receiverPhone;
    private String receiverProvince;
    private String receiverCity;
    private String receiverArea;
    private String receiverDetail;
}
