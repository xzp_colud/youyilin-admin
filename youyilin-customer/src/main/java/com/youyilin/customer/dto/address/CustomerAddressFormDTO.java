package com.youyilin.customer.dto.address;

import com.youyilin.common.utils.BeanHelper;
import com.youyilin.customer.entity.CustomerAddress;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 客户收货地址表单DTO
 */
@Data
public class CustomerAddressFormDTO {

    private Long id;
    // 当前客户
    @NotNull(message = "客户不能为空")
    private Long customerId;
    // 收货人
    @NotBlank(message = "收货人不能为空")
    @Size(max = 64, message = "收货人长度不能超过64字符")
    private String receiverName;
    // 收货电话
    @NotBlank(message = "收货电话不能为空")
    @Size(max = 11, message = "收货电话长度不能超过11字符")
    private String receiverPhone;
    // 省
    @NotBlank(message = "地址不能为空")
    private String province;
    // 市
    @NotBlank(message = "地址不能为空")
    private String city;
    // 区
    @NotBlank(message = "地址不能为空")
    private String area;
    // 详情
    @NotBlank(message = "地址不能为空")
    @Size(max = 128, message = "地址长度不能超过128字符")
    private String detail;
    // 默认标记
    private Integer defaultFlag;
    // 备注
    private String remark;

    public CustomerAddress convertToEntity() {
        return BeanHelper.map(this, CustomerAddress.class);
    }
}
