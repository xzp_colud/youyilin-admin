package com.youyilin.customer.dto.account;

import lombok.Data;

/**
 * 客户列表查询参数DTO
 */
@Data
public class CustomerAccountPageQueryDTO {

    // 账户类型
    private Integer type;
    // 用户名称
    private String userName;
}
