package com.youyilin.customer.dto.customer;

import lombok.Data;

/**
 * 客户列表查询参数DTO
 */
@Data
public class CustomerPageQueryDTO {

    // 批发标记
    private Integer wholesaleFlag;
    // 状态
    private Integer status;
    // 用户名称
    private String userName;
    // 姓名
    private String name;
    // 联系电话
    private String phone;
}
