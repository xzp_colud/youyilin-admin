package com.youyilin.customer.dto.account;

import com.youyilin.common.utils.BeanHelper;
import com.youyilin.customer.entity.CustomerAccount;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 客户账户初始化表单DTO
 */
@Data
public class CustomerAccountFormDTO {

    @NotNull(message = "客户不能为空")
    private Long customerId;
    @NotNull(message = "类型不能为空")
    private Integer type;

    public CustomerAccount convertToEntity() {
        return BeanHelper.map(this, CustomerAccount.class);
    }
}
