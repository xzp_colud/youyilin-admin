package com.youyilin.customer.dto.request;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class RequestMiniCart implements Serializable {
    private static final long serialVersionUID = 2886237323039400375L;

    @NotNull(message = "产品不能为空")
    private Long productId;
    @NotNull(message = "SKU不能为空")
    private Long skuId;
    @NotNull(message = "数量不能为空")
    private BigDecimal num;
}
