package com.youyilin.customer.dto.address;

import com.youyilin.common.utils.BeanHelper;
import com.youyilin.customer.entity.CustomerAddress;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@Accessors(chain = true)
public class CustomerMiniAddressFormDTO {

    private Long id;
    @NotBlank(message = "收货人不能为空")
    @Size(max = 64, message = "收货人长度不能超过64字符")
    private String receiverName;
    @NotBlank(message = "收货电话不能为空")
    @Size(max = 11, message = "收货电话长度不能超过11字符")
    private String receiverPhone;
    @NotBlank(message = "地址不能为空")
    private String province;
    @NotBlank(message = "地址不能为空")
    private String city;
    @NotBlank(message = "地址不能为空")
    private String area;
    @NotBlank(message = "地址不能为空")
    @Size(max = 128, message = "地址长度不能超过128字符")
    private String detail;
    private Integer defaultFlag;

    public CustomerAddress convertToEntity() {
        return BeanHelper.map(this, CustomerAddress.class);
    }
}
