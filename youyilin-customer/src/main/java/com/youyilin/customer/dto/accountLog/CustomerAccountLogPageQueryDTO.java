package com.youyilin.customer.dto.accountLog;

import lombok.Data;

@Data
public class CustomerAccountLogPageQueryDTO {

    // 账户
    private Long accountId;
    // 类型
    private Integer optType;
    // 用户名称
    private String userName;
}
