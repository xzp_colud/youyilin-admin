package com.youyilin.customer.dto.account;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 客户账户充值表单DTO
 */
@Data
public class CustomerAccountChargeFormDTO {

    @NotNull(message = "账户不能为空")
    private Long accountId;
    // 交易金额
    @NotNull(message = "金额需大于零")
    private BigDecimal chargeAmount;
    // 备注
    @NotBlank(message = "备注不能为空")
    private String remark;
    // 交易类型
    @NotBlank(message = "交易类型不能为空")
    private String payType;
    // 交易时间
    private Date optDate;
    // 交易图片
    private String imageUrl;
}
