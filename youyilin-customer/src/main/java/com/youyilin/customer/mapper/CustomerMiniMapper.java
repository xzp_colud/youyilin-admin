package com.youyilin.customer.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.common.bean.Page;
import com.youyilin.customer.entity.CustomerMini;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface CustomerMiniMapper extends BaseMapper<CustomerMini> {

    Integer getTotal(Page<CustomerMini> page);

    List<CustomerMini> getPage(Page<CustomerMini> page);
}
