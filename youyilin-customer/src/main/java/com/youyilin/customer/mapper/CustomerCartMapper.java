package com.youyilin.customer.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.customer.entity.CustomerCart;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CustomerCartMapper extends BaseMapper<CustomerCart> {
}
