package com.youyilin.customer.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.common.bean.Page;
import com.youyilin.customer.dto.accountLog.CustomerAccountLogPageQueryDTO;
import com.youyilin.customer.entity.CustomerAccountLog;
import com.youyilin.customer.vo.accountLog.CustomerAccountLogPageVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 账户 日志 Mapper
 */
@Mapper
public interface CustomerAccountLogMapper extends BaseMapper<CustomerAccountLog> {

    Integer getTotal(Page<CustomerAccountLogPageQueryDTO> page);

    List<CustomerAccountLogPageVO> getPage(Page<CustomerAccountLogPageQueryDTO> page);
}
