package com.youyilin.customer.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.common.bean.Page;
import com.youyilin.customer.dto.account.CustomerAccountPageQueryDTO;
import com.youyilin.customer.entity.CustomerAccount;
import com.youyilin.customer.vo.account.CustomerAccountPageVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 账户 Mapper
 */
@Mapper
public interface CustomerAccountMapper extends BaseMapper<CustomerAccount> {

    Integer getTotal(Page<CustomerAccountPageQueryDTO> page);

    List<CustomerAccountPageVO> getPage(Page<CustomerAccountPageQueryDTO> page);

    int charge(@Param("id") Long accountId, @Param("amount") Long amount);

    void consume(@Param("id") Long accountId, @Param("amount") Long amount);

    void refund(@Param("id") Long accountId, @Param("amount") Long amount);
}
