package com.youyilin.customer.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.common.bean.Page;
import com.youyilin.customer.dto.customer.CustomerPageQueryDTO;
import com.youyilin.customer.entity.Customer;
import com.youyilin.customer.vo.customer.CustomerPageVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 客户/消费者 mapper
 */
@Mapper
public interface CustomerMapper extends BaseMapper<Customer> {

    Integer getTotal(Page<CustomerPageQueryDTO> page);

    List<CustomerPageVO> getPage(Page<CustomerPageQueryDTO> page);
}
