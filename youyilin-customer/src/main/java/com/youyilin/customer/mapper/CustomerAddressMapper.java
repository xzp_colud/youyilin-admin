package com.youyilin.customer.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.customer.entity.CustomerAddress;
import org.apache.ibatis.annotations.Mapper;

/**
 * 客户收货地址 Mapper
 */
@Mapper
public interface CustomerAddressMapper extends BaseMapper<CustomerAddress> {

}
