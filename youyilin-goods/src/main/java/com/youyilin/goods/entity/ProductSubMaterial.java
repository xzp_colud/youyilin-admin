package com.youyilin.goods.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.FormatAmountUtil;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 产品辅料
 */
@Data
@Accessors(chain = true)
@TableName("product_sub_material")
public class ProductSubMaterial implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 绑定产品ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "source_product_id")
    private Long sourceProductId;
    /**
     * 方案ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "plan_id")
    private Long planId;
    /**
     * 产品ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "product_id")
    private Long productId;
    /**
     * 种类
     */
    @TableField(value = "product_name")
    private String productName;
    /**
     * 产品价格ID
     */
    @TableField(value = "product_price_id")
    private Long productPriceId;
    /**
     * 颜色
     */
    private String color;
    /**
     * 尺寸
     */
    private String size;
    /**
     * 用量
     */
    private BigDecimal used;
    /**
     * 单价
     */
    private Long amount;
    /**
     * 总价
     */
    @TableField(value = "total_amount")
    private Long totalAmount;

    public String getAmountText() {
        return FormatAmountUtil.format(this.amount);
    }

    public String getTotalAmountText() {
        return FormatAmountUtil.format(this.totalAmount);
    }
}
