package com.youyilin.goods.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.FormatAmountUtil;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 产品面料
 */
@Data
@Accessors(chain = true)
@TableName("product_fabric")
public class ProductFabric  implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 绑定产品ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "source_product_id")
    private Long sourceProductId;
    /**
     * 方案ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "plan_id")
    private Long planId;
    /**
     * 供货商ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "supplier_id")
    private Long supplierId;
    /**
     * 供货商
     */
    @TableField(value = "supplier_name")
    private String supplierName;
    /**
     * 产品ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "product_id")
    private Long productId;
    /**
     * 产品名称
     */
    @TableField(value = "product_name")
    private String productName;
    /**
     * 产品价格ID
     */
    @TableField(value = "product_price_id")
    private Long productPriceId;
    /**
     * 门幅
     */
    @TableField(value = "gate_width")
    private String gateWidth;
    /**
     * 成分
     */
    private String makeup;
    /**
     * 单件用量
     */
    @TableField(value = "one_usage")
    private BigDecimal oneUsage;
    /**
     * 单价
     */
    private Long amount;
    /**
     * 总价
     */
    @TableField(value = "total_amount")
    private Long totalAmount;

    public String getAmountText() {
        return FormatAmountUtil.format(this.amount);
    }

    public String getTotalAmountText() {
        return FormatAmountUtil.format(this.totalAmount);
    }
}
