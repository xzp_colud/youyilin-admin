package com.youyilin.goods.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 产品参数
 */
@Data
@Accessors(chain = true)
@TableName("product_params")
public class ProductParams implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 产品ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "product_id")
    private Long productId;
    /**
     * 参数ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "params_id")
    private Long paramsId;
    /**
     * 参数值
     */
    private String value;

    @TableField(exist = false)
    private String name;
}
