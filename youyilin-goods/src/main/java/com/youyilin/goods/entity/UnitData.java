package com.youyilin.goods.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * 销售单位键值
 */
@Data
@Accessors(chain = true)
@TableName("goods_unit_data")
public class UnitData implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 键值
     */
//    @NotBlank(message = "键值标签不能为空")
    @Size(max = 64, message = "销售单位键值标签长度不能超过64字符")
    private String label;
    /**
     * 键值
     */
//    @NotBlank(message = "键值文本不能为空")
    @Size(max = 64, message = "销售单位键值文本长度不能超过64字符")
    private String text;
    /**
     * 键值
     */
    @NotBlank(message = "键值不能为空")
    @Size(max = 64, message = "销售单位键值长度不能超过64字符")
    private String value;

    @NotNull(message = "销售单位不能为空")
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "unit_id")
    private Long unitId;

    /**
     * 关联的产品
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "product_id")
    private Long productId;

    @TableLogic
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;
}
