package com.youyilin.goods.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.enums.StatusEnum;
import com.youyilin.common.utils.FormatAmountUtil;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 产品价格SKU
 */
@Data
@Accessors(chain = true)
@TableName("product_price")
public class ProductPrice implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * SKU 名称
     */
    @TableField(value = "sku_name")
    private String skuName;
    /**
     * 销售价(吊牌价)(市场价)
     */
    @TableField(value = "sell_price")
    private Long sellPrice;
    /**
     * 批发价
     */
    @TableField(value = "wholesale_price")
    private Long wholesalePrice;
    /**
     * 采购价
     */
    @TableField(value = "cost_price")
    private Long costPrice;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 库存
     */
    private BigDecimal inventory;
    /**
     * 冻结库存
     */
    @TableField(value = "froze_inventory")
    private BigDecimal frozeInventory;
    /**
     * 产品ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "product_id")
    private Long productId;
    /**
     * 方案ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "plan_id")
    private Long planId;
    /**
     * 方案名称
     */
    @TableField(value = "plan_title")
    private String planTitle;

    @TableField(exist = false)
    private String skuPath;

    public String getSellPriceText() {
        return FormatAmountUtil.format(this.sellPrice);
    }

    public String getCostPriceText() {
        return FormatAmountUtil.format(this.costPrice);
    }

    public String getWholesalePriceText() {
        return FormatAmountUtil.format(this.wholesalePrice);
    }

    public boolean isDisabled() {
        return this.status == null || !this.status.equals(StatusEnum.NORMAL.getCode());
    }
}
