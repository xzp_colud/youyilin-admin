package com.youyilin.goods.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 商品销售方式
 */
@Data
@Accessors(chain = true)
@TableName("product_unit")
public class ProductUnit implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 商品ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "product_id")
    private Long productId;
    /**
     * 销售方式ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "unit_id")
    private Long unitId;
    /**
     * 销售方式键值ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "unit_data_id")
    private Long unitDataId;
}
