package com.youyilin.goods.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 商品分类-商品规格 关系表
 */
@Data
@TableName("goods_category_unit")
public class CategoryUnit implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 商品分类ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "category_id")
    private Long categoryId;
    /**
     * 商品规格ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "unit_id")
    private Long unitId;
}
