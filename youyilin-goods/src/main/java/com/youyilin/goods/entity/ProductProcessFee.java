package com.youyilin.goods.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.FormatAmountUtil;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 产品-加工费
 */
@Data
@Accessors(chain = true)
@TableName("product_process_fee")
public class ProductProcessFee implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 源产品ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "source_product_id")
    private Long sourceProductId;
    /**
     * 方案ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "plan_id")
    private Long planId;
    /**
     * 数量
     */
    private BigDecimal num;
    /**
     * 金额
     */
    private Long amount;

    public String getAmountText() {
        return FormatAmountUtil.format(this.amount);
    }
}
