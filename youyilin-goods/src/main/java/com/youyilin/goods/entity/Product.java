package com.youyilin.goods.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.enums.StatusEnum;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 产品
 */
@Data
@Accessors(chain = true)
@TableName("product")
public class Product implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    @TableField(value = "create_date", fill = FieldFill.INSERT)
    private Date createDate;
    @TableField(fill = FieldFill.INSERT)
    private String creator;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 商品名称
     */
    @NotBlank(message = "商品名称不能为空")
    @Size(max = 64, message = "商品名称长度不能超过64字符")
    private String name;
    /**
     * 内部编号
     */
    @Size(max = 64, message = "内部编号长度不能超过64字符")
    private String sn;
    /**
     * 供货商
     */
    @NotNull(message = "供货商不能为空")
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "supplier_id")
    private Long supplierId;
    @TableField(value = "supplier_name")
    private String supplierName;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 分类
     */
    @NotNull(message = "商品分类不能为空")
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "category_id")
    private Long categoryId;
    @TableField(value = "category_name")
    private String categoryName;
    /**
     * 封面图
     */
    @TableField(value = "image_url")
    private String imageUrl;
    /**
     * 显示价
     */
    @TableField(value = "price_text")
    private String priceText;
    /**
     * 销量
     */
    private BigDecimal sales;

    @TableLogic
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;

    public String getIdString() {
        return id == null ? "" : id.toString();
    }

    public boolean isDisabled() {
        return this.status == null || !this.status.equals(StatusEnum.NORMAL.getCode());
    }
}
