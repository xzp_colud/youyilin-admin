package com.youyilin.goods.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * 供货商
 */
@Data
@Accessors(chain = true)
@TableName("goods_supplier")
public class Supplier implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 供货商名称
     */
    @NotBlank(message = "供货商名称不能为空")
    @Size(max = 64, message = "供货商名称长度不能超过64个字符")
    private String name;
    /**
     * 拼音首字母
     */
    private String pinyin;
    /**
     * 联系人
     */
    @NotBlank(message = "供货商联系人不能为空")
    @Size(max = 64, message = "联系人长度不能超过64字符")
    @TableField(value = "link_man")
    private String linkMan;
    /**
     * 手机
     */
    @NotBlank(message = "供货商电话不能为空")
    @Size(min = 11, max = 13, message = "电话长度不能低于11字符或超过13个字符")
    private String phone;
    /**
     * 地址
     */
    private String province;
    private String city;
    private String area;
    private String detail;
    /**
     * 描述
     */
    @Size(max = 128, message = "描述长度不能超过128字符")
    private String description;
    /**
     * 状态
     */
    @NotNull(message = "状态不能为空")
    private Integer status;

    @TableLogic
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;
}
