package com.youyilin.goods.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 空间图片
 */
@Data
@TableName("goods_space_image")
public class SpaceImage implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 图片名称
     */
    @NotBlank(message = "图片名称不能为空")
    private String name;
    /**
     * 图片地址
     */
    @NotBlank(message = "请上传文件")
    private String url;
    /**
     * 文件后缀
     */
    private String suffix;
    /**
     * 文件大小
     */
    private String size;
    /**
     * 分辨率
     */
    private String resolution;
    /**
     * 空间ID
     */
    @NotNull(message = "所在空间不能为空")
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "space_id")
    private Long spaceId;

    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    private Integer delFlag;
}
