package com.youyilin.goods.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.FormatAmountUtil;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 产品工艺
 */
@Data
@Accessors(chain = true)
@TableName("product_technology")
public class ProductTechnology implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 绑定产品ID
     */
    @TableField(value = "source_product_id")
    private Long sourceProductId;
    /**
     * 方案ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "plan_id")
    private Long planId;
    /**
     * 供货商ID
     */
    @TableField(value = "supplier_id")
    private Long supplierId;
    /**
     * 供货商名称
     */
    @TableField(value = "supplier_name")
    private String supplierName;
    /**
     * 工艺名称
     */
    @TableField(value = "technology_name")
    private String technologyName;
    /**
     * 工艺描述
     */
    private String description;
    /**
     * 总价
     */
    @TableField(value = "total_amount")
    private Integer totalAmount;

    public String getTotalAmountText() {
        return FormatAmountUtil.format(this.totalAmount);
    }
}
