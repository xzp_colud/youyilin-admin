package com.youyilin.goods.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 商品销售单位对应的SKU
 */
@Data
@Accessors(chain = true)
public class ProductUnitPrice implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long modifier;

    /**
     * 商品ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "product_id")
    private Long productId;
    /**
     * SKU ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "product_price_id")
    private Long productPriceId;
    /**
     * SKU 路径
     */
    @TableField(value = "sku_path")
    private String skuPath;
}
