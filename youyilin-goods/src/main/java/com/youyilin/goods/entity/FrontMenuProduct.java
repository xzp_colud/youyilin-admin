package com.youyilin.goods.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 分类产品
 */
@Data
@Accessors(chain = true)
@TableName("front_menu_product")
public class FrontMenuProduct implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
    private String modifier;

    /**
     * 菜单ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "front_menu_id")
    private Long frontMenuId;
    /**
     * 产品ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "product_id")
    private Long productId;
    /**
     * 排序
     */
    private Integer sort;
}
