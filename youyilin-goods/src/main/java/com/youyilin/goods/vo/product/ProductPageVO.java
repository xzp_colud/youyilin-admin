package com.youyilin.goods.vo.product;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.goods.vo.product.page.ProductPageParams;
import com.youyilin.goods.vo.product.page.ProductPagePrice;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 商品列表VO
 */
@Data
@Accessors(chain = true)
public class ProductPageVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 修改时间
    private Date modifyDate;
    // 商品名称
    private String name;
    // 内部编号
    private String sn;
    // 供应商ID
    @JsonSerialize(using = ToStringSerializer.class)
    private Long supplierId;
    // 供货商名称
    private String supplierName;
    // 状态
    private Integer status;
    // 分类名称
    private String categoryName;
    // 封面图
    private String imageUrl;
    // 显示价
    private String priceText;
    // 销量
    private BigDecimal sales;
    // sku列表
    private List<ProductPagePrice> priceList;
    // 参数列表
    private List<ProductPageParams> paramsList;
}
