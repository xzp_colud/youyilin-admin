package com.youyilin.goods.vo.product.page;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.goods.entity.ProductParams;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Data
@Accessors(chain = true)
public class ProductPageParams {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 名称
    private String name;
    // 参数值
    private String value;

    public static List<ProductPageParams> convert(List<ProductParams> sourceList) {
        if (CollectionUtils.isEmpty(sourceList)) {
            return new ArrayList<>();
        }
        return BeanHelper.map(sourceList, ProductPageParams.class);
    }
}
