package com.youyilin.goods.vo.product.detail;

import com.youyilin.common.utils.FormatAmountUtil;
import com.youyilin.goods.entity.ProductProcessFee;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.collections.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
@Accessors(chain = true)
public class ProductDetailPlanProcessFee {

    // 数量
    private BigDecimal num;
    // 金额
    private String amountText;

    public static List<ProductDetailPlanProcessFee> convertByEntity(List<ProductProcessFee> sourceList) {
        if (CollectionUtils.isEmpty(sourceList)) {
            return new ArrayList<>();
        }
        List<ProductDetailPlanProcessFee> targetList = new ArrayList<>();
        for (ProductProcessFee item : sourceList) {
            ProductDetailPlanProcessFee target = new ProductDetailPlanProcessFee();
            target.setNum(item.getNum())
                    .setAmountText(FormatAmountUtil.format(item.getAmount()));

            targetList.add(target);
        }

        return targetList;
    }
}
