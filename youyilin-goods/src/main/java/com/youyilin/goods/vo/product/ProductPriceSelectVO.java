package com.youyilin.goods.vo.product;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.enums.BooleanEnum;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.goods.entity.ProductPrice;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 商品SKU下拉列表
 */
@Data
@Accessors(chain = true)
public class ProductPriceSelectVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // sku 名称
    private String skuName;
    // 销售价
    private Long sellPrice;
    // 批发价
    private Long wholesalePrice;
    // 成本价
    private Long costPrice;
    // 状态
    private Integer status;

    public boolean isDisabled() {
        return !(this.status != null && this.status.equals(BooleanEnum.TRUE.getCode()));
    }

    public static List<ProductPriceSelectVO> convertByEntity(List<ProductPrice> sourceList) {
        if (CollectionUtils.isEmpty(sourceList)) {
            return new ArrayList<>();
        }
        return BeanHelper.map(sourceList, ProductPriceSelectVO.class);
    }
}
