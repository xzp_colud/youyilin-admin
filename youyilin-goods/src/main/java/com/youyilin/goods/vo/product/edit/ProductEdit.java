package com.youyilin.goods.vo.product.edit;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.goods.entity.Product;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ProductEdit {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 商品名称
    private String name;
    // 内部编号
    private String sn;
    // 供货商
    @JsonSerialize(using = ToStringSerializer.class)
    private Long supplierId;
    // 分类ID
    @JsonSerialize(using = ToStringSerializer.class)
    private Long categoryId;
    // 分类名称
    private String categoryName;
    // 状态
    private Integer status;
    // 封面图
    private String imageUrl;

    public static ProductEdit convertByEntity(Product product) {
        return BeanHelper.map(product, ProductEdit.class);
    }
}
