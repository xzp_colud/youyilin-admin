package com.youyilin.goods.vo.product.edit;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.common.utils.FormatAmountUtil;
import com.youyilin.goods.entity.ProductFabric;
import com.youyilin.goods.vo.product.page.ProductPagePrice;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.collections.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
@Accessors(chain = true)
public class ProductEditPlanFabric {

    // 产品ID
    @JsonSerialize(using = ToStringSerializer.class)
    private Long productId;
    // 产品名称
    private String productName;
    // 供货商ID
    @JsonSerialize(using = ToStringSerializer.class)
    private Long supplierId;
    // 供应商名称
    private String supplierName;
    // 商品sku
    @JsonSerialize(using = ToStringSerializer.class)
    private Long productPriceId;
    // 门幅
    private String gateWidth;
    // 成分
    private String makeup;
    // 单件用量
    private BigDecimal oneUsage;
    // 单价
    private Long amount;
    // 总价
    private Long totalAmount;
    // sku列表
    private List<ProductPagePrice> priceList;

    public String getAmountText() {
        return FormatAmountUtil.format(this.amount);
    }

    public String getTotalAmountText() {
        return FormatAmountUtil.format(this.totalAmount);
    }

    public static List<ProductEditPlanFabric> convertByEntity(List<ProductFabric> sourceList) {
        if (CollectionUtils.isEmpty(sourceList)) {
            return new ArrayList<>();
        }
        return BeanHelper.map(sourceList, ProductEditPlanFabric.class);
    }
}
