package com.youyilin.goods.vo.params;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 属性参数列表VO
 */
@Data
@Accessors(chain = true)
public class ParamsPageVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 修改时间
    private Date modifyDate;
    // 名称
    private String name;
    // 是否必填
    private Integer required;
}
