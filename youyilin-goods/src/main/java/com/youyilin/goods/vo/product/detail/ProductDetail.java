package com.youyilin.goods.vo.product.detail;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.goods.entity.Product;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ProductDetail {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long categoryId;
    // 商品名称
    private String name;
    // 内部编号
    private String sn;
    // 供货商名称
    private String supplierName;
    // 分类名称
    private String categoryName;
    // 状态
    private Integer status;
    // 封面图
    private String imageUrl;

    public static ProductDetail convertByEntity(Product product) {
        return BeanHelper.map(product, ProductDetail.class);
    }
}
