package com.youyilin.goods.vo.product.edit;

import com.youyilin.common.utils.FormatAmountUtil;
import com.youyilin.goods.entity.ProductTechnology;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Data
@Accessors(chain = true)
public class ProductEditPlanTechnology {

    // 工艺商名称
    private String supplierName;
    // 工艺名称
    private String technologyName;
    // 金额
    private String totalAmountText;

    public static List<ProductEditPlanTechnology> convertByEntity(List<ProductTechnology> sourceList) {
        if (CollectionUtils.isEmpty(sourceList)) {
            return new ArrayList<>();
        }
        List<ProductEditPlanTechnology> targetList = new ArrayList<>();
        for (ProductTechnology item : sourceList) {
            ProductEditPlanTechnology target = new ProductEditPlanTechnology();
            target.setSupplierName(item.getSupplierName())
                    .setTechnologyName(item.getTechnologyName())
                    .setTotalAmountText(FormatAmountUtil.format(item.getTotalAmount()));

            targetList.add(target);
        }

        return targetList;
    }
}
