package com.youyilin.goods.vo.product.edit;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.goods.dto.unit.UnitDTO;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Data
@Accessors(chain = true)
public class ProductEditUnit {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 规格名称
    private String name;
    // 规格值列表
    private List<ProductEditUnitData> itemList;

    public static List<ProductEditUnit> convertByUnitDTO(List<UnitDTO> sourceList) {
        if (CollectionUtils.isEmpty(sourceList)) {
            return new ArrayList<>();
        }
        return JSONArray.parseArray(JSONObject.toJSONString(sourceList), ProductEditUnit.class);
    }
}
