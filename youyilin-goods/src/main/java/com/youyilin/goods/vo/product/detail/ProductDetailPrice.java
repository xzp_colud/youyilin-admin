package com.youyilin.goods.vo.product.detail;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.common.utils.FormatAmountUtil;
import com.youyilin.goods.entity.ProductPrice;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Data
@Accessors(chain = true)
public class ProductDetailPrice {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // sku 名称
    private String skuName;
    // sku 路径
    private String skuPath;
    // 销售价
    private Long sellPrice;
    // 批发价
    private Long wholesalePrice;
    // 采购价
    private Long costPrice;
    // 状态
    private Integer status;
    // 当前库存
    private Integer inventory;
    // 冻结库存
    private Integer frozeInventory;
    // 方案名称
    private String planTitle;

    public String getSellPriceText() {
        return FormatAmountUtil.format(this.sellPrice);
    }

    public String getCostPriceText() {
        return FormatAmountUtil.format(this.costPrice);
    }

    public String getWholesalePriceText() {
        return FormatAmountUtil.format(this.wholesalePrice);
    }

    public static List<ProductDetailPrice> convertByEntity(List<ProductPrice> sourceList) {
        if (CollectionUtils.isEmpty(sourceList)) {
            return new ArrayList<>();
        }
        return BeanHelper.map(sourceList, ProductDetailPrice.class);
    }
}
