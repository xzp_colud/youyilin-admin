package com.youyilin.goods.vo.category;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.enums.BooleanEnum;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.goods.entity.Category;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 分类VO
 */
@Data
@Accessors(chain = true)
public class CategoryVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 分类名称
    private String title;
    // 状态
    private Integer status;

    public boolean isDisabled() {
        return status == null || !status.equals(BooleanEnum.TRUE.getCode());
    }

    public static CategoryVO convertByEntity(Category category) {
        return BeanHelper.map(category, CategoryVO.class);
    }

    public static List<CategoryVO> convertByEntity(List<Category> list) {
        return BeanHelper.map(list, CategoryVO.class);
    }
}
