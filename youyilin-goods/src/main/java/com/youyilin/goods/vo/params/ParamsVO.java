package com.youyilin.goods.vo.params;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.goods.entity.Params;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;

/**
 *
 */
@Data
@Accessors(chain = true)
public class ParamsVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 修改时间
    private Date modifyDate;
    // 名称
    private String name;
    // 是否必填
    private Integer required;

    public static ParamsVO convertByEntity(Params params) {
        return BeanHelper.map(params, ParamsVO.class);
    }

    public static List<ParamsVO> convertByEntity(List<Params> list) {
        return BeanHelper.map(list, ParamsVO.class);
    }
}
