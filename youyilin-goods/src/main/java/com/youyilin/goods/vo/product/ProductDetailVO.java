package com.youyilin.goods.vo.product;

import com.youyilin.goods.vo.product.detail.*;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class ProductDetailVO {

    // 商品信息
    private ProductDetail product;
    // 属性参数信息
    private List<ProductDetailParams> paramsList;
    // 规格列表
    private List<ProductDetailUnit> unitList;
    // SKU 列表
    private List<ProductDetailPrice> priceList;
    // 方案列表
    private List<ProductDetailPlan> planList;
    // 描述文案
    private String productDescription;
}
