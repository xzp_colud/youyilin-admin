package com.youyilin.goods.vo.category;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.goods.entity.Category;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;

/**
 * 分类详情VO
 */
@Data
@Accessors(chain = true)
public class CategoryDetailVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 修改时间
    private Date modifyDate;
    // 分类名称
    private String title;
    // 状态
    private Integer status;
    // 排序
    private Integer sort;
    // 已配置参数列表
    private List<String> paramsList;
    // 已配置规格列表
    private List<String> unitList;

    public static CategoryDetailVO convertByEntity(Category category) {
        return BeanHelper.map(category, CategoryDetailVO.class);
    }
}
