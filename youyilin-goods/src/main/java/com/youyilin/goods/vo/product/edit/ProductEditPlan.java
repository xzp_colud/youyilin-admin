package com.youyilin.goods.vo.product.edit;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.goods.entity.ProductPlan;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Data
@Accessors(chain = true)
public class ProductEditPlan {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 方案标题
    private String title;
    // 面料
    private List<ProductEditPlanFabric> fabricList;
    // 配料
    private List<ProductEditPlanIngredient> ingredientList;
    // 辅料
    private List<ProductEditPlanSubMaterial> subMaterialList;
    // 工艺
    private List<ProductEditPlanTechnology> technologyList;
    // 加工费
    private List<ProductEditPlanProcessFee> processFeeList;

    public static List<ProductEditPlan> convertByEntity(List<ProductPlan> sourceList) {
        if (CollectionUtils.isEmpty(sourceList)) {
            return new ArrayList<>();
        }
        return BeanHelper.map(sourceList, ProductEditPlan.class);
    }
}
