package com.youyilin.goods.vo.supplier;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 供货商列表VO
 */
@Data
@Accessors(chain = true)
public class SupplierPageVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    private Date modifyDate;
    // 名称
    private String name;
    // 拼音首字母
    private String pinyin;
    // 联系人
    private String linkMan;
    // 手机
    private String phone;
    // 地址
    private String province;
    private String city;
    private String area;
    private String detail;
    // 描述
    private String description;
    // 状态
    private Integer status;
}
