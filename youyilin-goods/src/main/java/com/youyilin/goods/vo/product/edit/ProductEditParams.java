package com.youyilin.goods.vo.product.edit;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.goods.entity.Params;
import com.youyilin.goods.entity.ProductParams;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Data
@Accessors(chain = true)
public class ProductEditParams {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 参数名称
    private String name;
    // 参数值
    private String value;
    // 是否必填
    private boolean require;

    public static List<ProductEditParams> convert(List<Params> paramsList, List<ProductParams> productParamsList) {
        if (CollectionUtils.isEmpty(paramsList)) {
            return new ArrayList<>();
        }
        if (CollectionUtils.isEmpty(productParamsList)) {
            List<ProductEditParams> targetList = new ArrayList<>();
            for (Params item : paramsList) {
                ProductEditParams target = new ProductEditParams();
                target.setId(item.getId())
                        .setName(item.getName())
                        .setRequire(item.isRequire());

                targetList.add(target);
            }
            return targetList;
        }
        List<ProductEditParams> targetList = new ArrayList<>();
        Map<Long, ProductParams> productParamsMap = productParamsList.stream().collect(Collectors.toMap(ProductParams::getParamsId, item -> item));
        for (Params item : paramsList) {
            ProductParams productParams = productParamsMap.get(item.getId());
            ProductEditParams target = new ProductEditParams();
            target.setId(item.getId())
                    .setName(item.getName())
                    .setRequire(item.isRequire())
                    .setValue(productParams == null ? "" : productParams.getValue());

            targetList.add(target);
        }
        return targetList;
    }
}
