package com.youyilin.goods.vo.product.detail;

import com.youyilin.common.utils.BeanHelper;
import com.youyilin.common.utils.FormatAmountUtil;
import com.youyilin.goods.entity.ProductFabric;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.collections.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
@Accessors(chain = true)
public class ProductDetailPlanFabric {

    // 产品名称
    private String productName;
    // 供应商名称
    private String supplierName;
    // 门幅
    private String gateWidth;
    // 成分
    private String makeup;
    // 单件用量
    private BigDecimal oneUsage;
    // 单价
    private Long amount;
    // 总价
    private Long totalAmount;

    public String getAmountText() {
        return FormatAmountUtil.format(this.amount);
    }

    public String getTotalAmountText() {
        return FormatAmountUtil.format(this.totalAmount);
    }

    public static List<ProductDetailPlanFabric> convertByEntity(List<ProductFabric> sourceList) {
        if (CollectionUtils.isEmpty(sourceList)) {
            return new ArrayList<>();
        }
        return BeanHelper.map(sourceList, ProductDetailPlanFabric.class);
    }
}
