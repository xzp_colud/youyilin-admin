package com.youyilin.goods.vo.product.edit;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.FormatAmountUtil;
import com.youyilin.goods.entity.ProductPrice;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Data
@Accessors(chain = true)
public class ProductEditPrice {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // sku名称
    private String skuName;
    // 路径
    private String skuPath;
    // 销售价
    private String sellPriceText;
    // 批发价
    private String wholesalePriceText;
    // 采购价
    private String costPriceText;
    // 状态
    private Integer status;
    // 方案ID
    @JsonSerialize(using = ToStringSerializer.class)
    private Long planId;

    public static List<ProductEditPrice> convertByEntity(List<ProductPrice> sourceList) {
        if (CollectionUtils.isEmpty(sourceList)) {
            return new ArrayList<>();
        }
        List<ProductEditPrice> targetList = new ArrayList<>();
        for (ProductPrice item : sourceList) {
            ProductEditPrice target = new ProductEditPrice();
            target.setId(item.getId())
                    .setSkuName(item.getSkuName())
                    .setSellPriceText(FormatAmountUtil.format(item.getSellPrice()))
                    .setWholesalePriceText(FormatAmountUtil.format(item.getWholesalePrice()))
                    .setCostPriceText(FormatAmountUtil.format(item.getCostPrice()))
                    .setStatus(item.getStatus())
                    .setPlanId(item.getPlanId());

            targetList.add(target);
        }

        return targetList;
    }
}
