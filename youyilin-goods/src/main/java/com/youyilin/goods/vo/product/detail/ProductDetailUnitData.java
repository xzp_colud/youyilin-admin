package com.youyilin.goods.vo.product.detail;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ProductDetailUnitData {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 规格ID
    @JsonSerialize(using = ToStringSerializer.class)
    private Long skuId;
    // 规格值
    private String value;
}
