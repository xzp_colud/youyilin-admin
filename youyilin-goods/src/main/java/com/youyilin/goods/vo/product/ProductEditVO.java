package com.youyilin.goods.vo.product;

import com.youyilin.goods.vo.product.edit.*;
import com.youyilin.goods.vo.supplier.SupplierSelectVO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 商品编辑页VO
 */
@Data
@Accessors(chain = true)
public class ProductEditVO {

    // 商品
    private ProductEdit product;
    // 供货商
    private List<SupplierSelectVO> supplierList;
    // 属性参数
    private List<ProductEditParams> paramsList;
    // 规格列表
    private List<ProductEditUnit> unitList;
    // sku列表
    private List<ProductEditPrice> priceList;
    // 方案列表
    private List<ProductEditPlan> planList;
    // 描述文案
    private String productDescription;
}
