package com.youyilin.goods.vo.product.edit;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ProductEditUnitData {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long unitId;
    // 同ID
    private String uid;
    // 规格标签
    private String label;
    // 规格文本
    private String text;
    // 规格内容
    private String value;
    // 是否选中
    private boolean checked;
    // 是否可以删除
    private boolean closable;
}
