package com.youyilin.goods.vo.product.edit;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.common.utils.FormatAmountUtil;
import com.youyilin.goods.entity.ProductSubMaterial;
import com.youyilin.goods.vo.product.page.ProductPagePrice;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.collections.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
@Accessors(chain = true)
public class ProductEditPlanSubMaterial {

    // 产品ID
    @JsonSerialize(using = ToStringSerializer.class)
    private Long productId;
    // 产品名称
    private String productName;
    // 商品sku
    @JsonSerialize(using = ToStringSerializer.class)
    private Long productPriceId;
    // 颜色
    private String color;
    // 尺寸
    private String size;
    // 用量
    private BigDecimal used;
    // 单价
    private Long amount;
    // 总价
    private Long totalAmount;
    // sku列表
    private List<ProductPagePrice> priceList;

    public String getAmountText() {
        return FormatAmountUtil.format(this.amount);
    }

    public String getTotalAmountText() {
        return FormatAmountUtil.format(this.totalAmount);
    }

    public static List<ProductEditPlanSubMaterial> convertByEntity(List<ProductSubMaterial> sourceList) {
        if (CollectionUtils.isEmpty(sourceList)) {
            return new ArrayList<>();
        }
        return BeanHelper.map(sourceList, ProductEditPlanSubMaterial.class);
    }
}
