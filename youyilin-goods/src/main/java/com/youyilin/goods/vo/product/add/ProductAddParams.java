package com.youyilin.goods.vo.product.add;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.goods.entity.Params;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Data
@Accessors(chain = true)
public class ProductAddParams {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 参数名称
    private String name;
    // 是否必填
    private boolean require;

    public static List<ProductAddParams> convert(List<Params> sourceList) {
        if (CollectionUtils.isEmpty(sourceList)) {
            return new ArrayList<>();
        }
        List<ProductAddParams> targetList = new ArrayList<>();
        for (Params item : sourceList) {
            ProductAddParams target = new ProductAddParams();
            target.setId(item.getId())
                    .setName(item.getName())
                    .setRequire(item.isRequire());

            targetList.add(target);
        }
        return targetList;
    }
}
