package com.youyilin.goods.vo.product.detail;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.goods.dto.unit.UnitDTO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class ProductDetailUnit {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 规格名称
    private String name;
    // 规格值列表
    private List<ProductDetailUnitData> itemList;

    public static List<ProductDetailUnit> convertByUnitDTO(List<UnitDTO> unitDTOList) {
        return JSONObject.parseArray(JSONObject.toJSONString(unitDTOList), ProductDetailUnit.class);
    }
}
