package com.youyilin.goods.vo.category;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 分类列表VO
 */
@Data
@Accessors(chain = true)
public class CategoryPageVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 修改时间
    private Date modifyDate;
    // 名称
    private String title;
    // 排序
    private Integer sort;
    // 状态
    private Integer status;
}
