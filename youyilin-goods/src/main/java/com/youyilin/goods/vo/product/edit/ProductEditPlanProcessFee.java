package com.youyilin.goods.vo.product.edit;

import com.youyilin.common.utils.FormatAmountUtil;
import com.youyilin.goods.entity.ProductProcessFee;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.collections.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
@Accessors(chain = true)
public class ProductEditPlanProcessFee {

    // 数量
    private BigDecimal num;
    // 金额
    private String amountText;

    public static List<ProductEditPlanProcessFee> convertByEntity(List<ProductProcessFee> sourceList) {
        if (CollectionUtils.isEmpty(sourceList)) {
            return new ArrayList<>();
        }
        List<ProductEditPlanProcessFee> targetList = new ArrayList<>();
        for (ProductProcessFee item : sourceList) {
            ProductEditPlanProcessFee target = new ProductEditPlanProcessFee();
            target.setNum(item.getNum())
                    .setAmountText(FormatAmountUtil.format(item.getAmount()));

            targetList.add(target);
        }

        return targetList;
    }
}
