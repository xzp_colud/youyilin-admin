package com.youyilin.goods.vo.supplier;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.enums.BooleanEnum;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.goods.entity.Supplier;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 供货商下拉列表VO
 */
@Data
@Accessors(chain = true)
public class SupplierSelectVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 供货商名称
    private String name;
    // 状态
    private Integer status;

    public boolean isDisabled() {
        return !(this.status != null && this.status.equals(BooleanEnum.TRUE.getCode()));
    }

    public static List<SupplierSelectVO> convertByEntity(List<Supplier> list) {
        return BeanHelper.map(list, SupplierSelectVO.class);
    }
}
