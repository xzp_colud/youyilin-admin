package com.youyilin.goods.vo.supplier;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.goods.entity.Supplier;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 供货商VO
 */
@Data
public class SupplierVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    private Date modifyDate;
    // 名称
    private String name;
    // 拼音首字母
    private String pinyin;
    // 联系人
    private String linkMan;
    // 手机
    private String phone;
    // 地址
    private String province;
    private String city;
    private String area;
    private String detail;
    // 描述
    private String description;
    // 状态
    private Integer status;

    public static SupplierVO convertByEntity(Supplier supplier) {
        return BeanHelper.map(supplier, SupplierVO.class);
    }

    public static List<SupplierVO> convertByEntity(List<Supplier> list) {
        return BeanHelper.map(list, SupplierVO.class);
    }
}
