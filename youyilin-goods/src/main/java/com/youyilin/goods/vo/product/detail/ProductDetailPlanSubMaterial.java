package com.youyilin.goods.vo.product.detail;

import com.youyilin.common.utils.BeanHelper;
import com.youyilin.common.utils.FormatAmountUtil;
import com.youyilin.goods.entity.ProductSubMaterial;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.collections.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
@Accessors(chain = true)
public class ProductDetailPlanSubMaterial {

    // 产品名称
    private String productName;
    // 颜色
    private String color;
    // 尺寸
    private String size;
    // 用量
    private BigDecimal used;
    // 单价
    private Long amount;
    // 总价
    private Long totalAmount;

    public String getAmountText() {
        return FormatAmountUtil.format(this.amount);
    }

    public String getTotalAmountText() {
        return FormatAmountUtil.format(this.totalAmount);
    }

    public static List<ProductDetailPlanSubMaterial> convertByEntity(List<ProductSubMaterial> sourceList) {
        if (CollectionUtils.isEmpty(sourceList)) {
            return new ArrayList<>();
        }
        return BeanHelper.map(sourceList, ProductDetailPlanSubMaterial.class);
    }
}
