package com.youyilin.goods.vo.product;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.goods.vo.product.add.ProductAddParams;
import com.youyilin.goods.vo.product.add.ProductAddUnit;
import com.youyilin.goods.vo.supplier.SupplierSelectVO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 商品新增页VO
 */
@Data
@Accessors(chain = true)
public class ProductAddVO {

    // 分类ID
    @JsonSerialize(using = ToStringSerializer.class)
    private Long categoryId;
    // 分类名称
    private String categoryName;
    // 参数列表
    private List<ProductAddParams> paramsList;
    // 规格列表
    private List<ProductAddUnit> unitList;
    // 供应商列表
    private List<SupplierSelectVO> supplierList;
}
