package com.youyilin.goods.vo.category;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.goods.entity.Category;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 分类编辑VO
 */
@Data
@Accessors(chain = true)
public class CategoryEditVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    // 分类名称
    private String title;
    // 状态
    private Integer status;
    // 排序
    private Integer sort;
    // 选中参数列表
    private List<String> paramsIds;
    // 选中规格列表
    private List<String> unitIds;

    public static CategoryEditVO convertByEntity(Category category) {
        return BeanHelper.map(category, CategoryEditVO.class);
    }
}
