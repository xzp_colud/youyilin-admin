package com.youyilin.goods.vo.product.detail;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.youyilin.goods.entity.Params;
import com.youyilin.goods.entity.ProductParams;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Data
@Accessors(chain = true)
public class ProductDetailParams {

    @JsonIgnore
    private Long id;
    // 名称
    private String name;
    // 参数值
    private String value;

    public static List<ProductDetailParams> convert(List<Params> paramsList, List<ProductParams> productParamsList) {
        if (CollectionUtils.isEmpty(paramsList)) {
            return new ArrayList<>();
        }
        if (CollectionUtils.isEmpty(productParamsList)) {
            List<ProductDetailParams> targetList = new ArrayList<>();
            for (Params item : paramsList) {
                ProductDetailParams target = new ProductDetailParams();
                target.setName(item.getName());

                targetList.add(target);
            }
            return targetList;
        }
        List<ProductDetailParams> targetList = new ArrayList<>();
        Map<Long, ProductParams> productParamsMap = productParamsList.stream().collect(Collectors.toMap(ProductParams::getParamsId, item -> item));
        for (Params item : paramsList) {
            ProductParams productParams = productParamsMap.get(item.getId());
            ProductDetailParams target = new ProductDetailParams();
            target.setName(item.getName())
                    .setValue(productParams == null ? "" : productParams.getValue());

            targetList.add(target);
        }
        return targetList;
    }
}
