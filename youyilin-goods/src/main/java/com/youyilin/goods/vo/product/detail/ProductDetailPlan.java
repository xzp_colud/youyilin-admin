package com.youyilin.goods.vo.product.detail;

import com.youyilin.common.utils.BeanHelper;
import com.youyilin.goods.entity.ProductPlan;
import jdk.nashorn.internal.ir.annotations.Ignore;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class ProductDetailPlan {

    @Ignore
    private Long id;
    // 方案名称
    private String title;
    // 面料
    private List<ProductDetailPlanFabric> fabricList;
    // 配料
    private List<ProductDetailPlanIngredient> ingredientList;
    // 辅料
    private List<ProductDetailPlanSubMaterial> subMaterialList;
    // 工艺
    private List<ProductDetailPlanTechnology> technologyList;
    // 加工费
    private List<ProductDetailPlanProcessFee> processFeeList;

    public static List<ProductDetailPlan> convertByEntity(List<ProductPlan> sourceList) {
        return BeanHelper.map(sourceList, ProductDetailPlan.class);
    }
}
