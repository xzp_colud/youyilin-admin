package com.youyilin.goods.enums;

/**
 * 商品参数分类
 */
public enum ParamsTypeEnum {

    BASE_PARAMS(1, "参数"),
    ASSURE(2, "保障"),
    SERVE(3, "服务");

    private int code;
    private String info;

    ParamsTypeEnum(int code, String info) {
        this.code = code;
        this.info = info;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    /**
     * 验证是否存在
     */
    public static boolean findIndex(int code) {
        for (ParamsTypeEnum t : ParamsTypeEnum.values()) {
            if (t.getCode() == code) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    /**
     * 查找info
     */
    public static String findInfo(int code) {
        for (ParamsTypeEnum t : ParamsTypeEnum.values()) {
            if (t.getCode() == code) {
                return t.getInfo();
            }
        }
        return null;
    }
}
