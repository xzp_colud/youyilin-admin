package com.youyilin.goods.enums;

/**
 * 商品物料分类
 */
public enum ProductMaterialTypeEnum {

    FABRIC(1, "面料"),
    INGREDIENT(2, "配料"),
    SUB_MATERIAL(3, "辅料");

    private int code;
    private String info;

    ProductMaterialTypeEnum(int code, String info) {
        this.code = code;
        this.info = info;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
