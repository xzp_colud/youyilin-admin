package com.youyilin.goods.dto.category;

import com.youyilin.common.utils.BeanHelper;
import com.youyilin.goods.entity.Category;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * 分类保存DTO
 */
@Data
public class CategoryFormDTO {

    private Long id;
    // 名称
    @NotBlank(message = "分类名称不能为空")
    @Size(max = 50, message = "类目名称长度不能超过50字符")
    private String title;
    // 排序
    private Integer sort;
    // 状态
    @NotNull(message = "状态不能为空")
    private Integer status;
    // 已选中的参数列表
    private List<Long> paramsIds;
    // 已选中的规格列表
    private List<Long> unitIds;

    public Category convertToEntity() {
        return BeanHelper.map(this, Category.class);
    }
}
