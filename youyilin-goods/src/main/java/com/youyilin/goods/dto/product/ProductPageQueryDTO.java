package com.youyilin.goods.dto.product;

import lombok.Data;

@Data
public class ProductPageQueryDTO {

    private Long supplierId;
    private Long categoryId;
    private Integer status;
    private String sn;
    private String name;
    private String categoryName;
    private String supplierName;
}
