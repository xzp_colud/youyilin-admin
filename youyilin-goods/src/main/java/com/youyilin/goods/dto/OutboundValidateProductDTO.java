package com.youyilin.goods.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * 出库单验证产品DTO
 */
@Data
@Accessors(chain = true)
public class OutboundValidateProductDTO {

    // 商品ID
    private Long productId;
    // SKU ID
    private Long skuId;
    // 出库数量
    private BigDecimal qty;

    // ----- 冗余字段 ----- //
    // 图片
    private String imageUrl;
    // 商品名称
    private String productName;
    // 编号
    private String serialNum;
    // 分类ID
    private Long categoryId;
    // 分类名称
    private String categoryName;
    // 供货商ID
    private Long supplierId;
    // 供货商名称
    private String supplierName;
    // sku 名称
    private String skuName;
}
