package com.youyilin.goods.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * 采购单验证商品DTO
 */
@Data
@Accessors(chain = true)
public class PurchaseValidateProductDTO {

    // 商品ID
    private Long productId;
    // SKU ID
    private Long skuId;
    // 购买数量
    private BigDecimal buyNum;
    // 批发价标识
    private Integer wholesalePriceFlag;

    // ----- 生成(冗余)字段 ----- //
    // 图片
    private String imageUrl;
    // 商品名称
    private String productName;
    // 编号
    private String serialNum;
    // 分类ID
    private Long categoryId;
    // 分类名称
    private String categoryName;
    // 供货商ID
    private Long supplierId;
    // 供货商名称
    private String supplierName;
    // sku 名称
    private String skuName;
    // 单价
    private Long price;
    // 实际单价
    private Long amount;
    // 总额
    private Long totalAmount;
    // 折扣总额
    private Long totalDiscount;
    // 折扣
    private Integer discount;
}
