package com.youyilin.goods.dto.unit;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.goods.entity.UnitData;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Data
@Accessors(chain = true)
public class UnitDataDTO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    private String label;
    private String text;
    private String value;
    private Long unitId;
    private Long productId;
    // 是否选中
    private boolean checked;
    // 是否可以删除
    private boolean closable;

    public static List<UnitDataDTO> convertByEntity(List<UnitData> sourceList) {
        if (CollectionUtils.isEmpty(sourceList)) {
            return new ArrayList<>();
        }
        return BeanHelper.map(sourceList, UnitDataDTO.class);
    }
}
