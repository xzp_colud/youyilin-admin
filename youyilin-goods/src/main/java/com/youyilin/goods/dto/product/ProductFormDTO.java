//package com.youyilin.goods.dto.product;
//
//import com.youyilin.goods.dto.product.edit.ProductFormParams;
//import com.youyilin.goods.dto.product.edit.ProductFormPlan;
//import com.youyilin.goods.dto.product.edit.ProductFormPrice;
//import com.youyilin.goods.dto.product.edit.ProductFormUnit;
//import lombok.Data;
//
//import javax.validation.constraints.NotBlank;
//import javax.validation.constraints.NotNull;
//import javax.validation.constraints.Size;
//import java.util.List;
//
//@Data
//public class ProductFormDTO {
//
//    private Long id;
//    // 商品名称
//    @NotBlank(message = "商品名称不能为空")
//    @Size(max = 64, message = "商品名称长度不能超过64字符")
//    private String name;
//    // 内部编号
//    @Size(max = 64, message = "内部编号长度不能超过64字符")
//    private String sn;
//    // 供货商
//    @NotNull(message = "供货商不能为空")
//    private Long supplierId;
//    // 分类
//    @NotNull(message = "分类不能为空")
//    private Long categoryId;
//    // 状态
//    private Integer status;
//    // 封面图
//    private String imageUrl;
//    // 描述文案
//    private String productDescription;
//    // 参数列表
//    private List<ProductFormParams> paramsList;
//    // 规格列表
//    private List<ProductFormUnit> unitList;
//    // sku 列表
//    private List<ProductFormPrice> skuList;
//    // 方案列表
//    private List<ProductFormPlan> planList;
//}
