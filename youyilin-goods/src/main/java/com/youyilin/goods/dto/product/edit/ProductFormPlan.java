//package com.youyilin.goods.dto.product.edit;
//
//import lombok.Data;
//
//import java.util.List;
//
//@Data
//public class ProductFormPlan {
//
//    private String uid;
//    // 方案名称
//    private String title;
//    // 面料
//    private List<ProductFormPlanFabric> fabricList;
//    // 配料
//    private List<ProductFormPlanIngredient> ingredientList;
//    // 辅料
//    private List<ProductFormPlanSubMaterial> subMaterialList;
//    // 工艺
//    private List<ProductFormPlanTechnology> technologyList;
//    // 加工费
//    private List<ProductFormPlanProcessFee> processFeeList;
//}
