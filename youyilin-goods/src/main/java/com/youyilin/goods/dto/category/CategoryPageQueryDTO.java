package com.youyilin.goods.dto.category;

import lombok.Data;

/**
 * 分类列表查询参数DTO
 */
@Data
public class CategoryPageQueryDTO {

    // 状态
    private Integer status;
    // 名称
    private String title;
}
