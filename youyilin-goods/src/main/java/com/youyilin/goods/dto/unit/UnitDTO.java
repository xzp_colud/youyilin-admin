package com.youyilin.goods.dto.unit;

import com.youyilin.common.utils.BeanHelper;
import com.youyilin.goods.entity.Unit;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Data
@Accessors(chain = true)
public class UnitDTO {

    private Long id;
    private String name;

    private List<UnitDataDTO> itemList;

    public static List<UnitDTO> convertByEntity(List<Unit> sourceList) {
        if (CollectionUtils.isEmpty(sourceList)) {
            return new ArrayList<>();
        }
        return BeanHelper.map(sourceList, UnitDTO.class);
    }
}
