package com.youyilin.goods.dto.params;

import com.youyilin.common.utils.BeanHelper;
import com.youyilin.goods.entity.Params;
import lombok.Data;

/**
 * 属性参数表单DTO
 */
@Data
public class ParamsFormDTO {

    private Long id;
    // 名称
    private String name;
    // 是否必填
    private Integer required;

    public Params convertToEntity() {
        return BeanHelper.map(this, Params.class);
    }
}
