package com.youyilin.goods.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * 销售单验证商品DTO
 */
@Data
@Accessors(chain = true)
public class SalesValidateProductDTO {

    // 商品ID
    private Long productId;
    // SKU ID
    private Long skuId;
    // 购买数量
    private BigDecimal num;
    // 折扣
    private Integer discount;
    // 备注
    private String remark;

    // ----- 生成(冗余)字段 ----- //
    // 图片
    private String imageUrl;
    // 商品名称
    private String productName;
    // 编号
    private String serialNum;
    // 分类ID
    private Long categoryId;
    // 分类名称
    private String categoryName;
    // 供货商ID
    private Long supplierId;
    // 供货商名称
    private String supplierName;
    // SKU ID
    private Long productPriceId;
    // sku 名称
    private String skuName;
    // 单价
    private Long sellPrice;
    // 实际单价
    private Long sellAmount;
    // 总额
    private Long totalSell;
    // 折扣总额
    private Long totalDiscount;
    // 成本单价
    private Long costAmount;
    // 成本总价
    private Long totalCost;
}
