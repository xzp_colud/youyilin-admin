package com.youyilin.goods.dto.supplier;

import lombok.Data;

/**
 * 供货商列表查询参数DTO
 */
@Data
public class SupplierPageQueryDTO {

    private Integer status;
    private String province;
    private String name;
    private String pinyin;
    private String linkMan;
    private String phone;
}
