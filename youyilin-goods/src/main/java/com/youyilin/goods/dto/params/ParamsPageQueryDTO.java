package com.youyilin.goods.dto.params;

import lombok.Data;

/**
 * 参数属性列表查询参数DTO
 */
@Data
public class ParamsPageQueryDTO {

    // 名称
    private String name;
}
