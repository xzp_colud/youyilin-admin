package com.youyilin.goods.dto.supplier;

import com.youyilin.common.utils.BeanHelper;
import com.youyilin.goods.entity.Supplier;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 供货商表单
 */
@Data
public class SupplierFormDTO {

    private Long id;
    // 供货商名称
    @NotBlank(message = "供货商名称不能为空")
    @Size(max = 64, message = "供货商名称长度不能超过64个字符")
    private String name;
    // 联系人
    @NotBlank(message = "供货商联系人不能为空")
    @Size(max = 64, message = "联系人长度不能超过64字符")
    private String linkMan;
    // 手机
    @NotBlank(message = "供货商电话不能为空")
    @Size(min = 11, max = 13, message = "电话长度不能低于11字符或超过13个字符")
    private String phone;
    // 地址
    private String province;
    private String city;
    private String area;
    private String detail;
    // 描述
    @Size(max = 128, message = "描述长度不能超过128字符")
    private String description;
    // 状态
    @NotNull(message = "状态不能为空")
    private Integer status;

    public Supplier convertToEntity() {
        return BeanHelper.map(this, Supplier.class);
    }
}
