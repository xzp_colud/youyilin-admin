package com.youyilin.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.goods.entity.CategoryUnit;

import java.util.List;

/**
 * 商品分类-规格 服务类
 */
public interface CategoryUnitService extends IService<CategoryUnit> {

    /**
     * 按分类查询
     *
     * @param categoryId 分类ID
     * @return ArrayList
     */
    List<CategoryUnit> listByCategoryId(Long categoryId);

    /**
     * 按分类查询规格IDS
     *
     * @param categoryId 分类ID
     * @return ArrayList
     */
    List<String> listUnitIdsByCategoryId(Long categoryId);

    /**
     * 按商品分类查询规格名称
     *
     * @param categoryId 分类ID
     * @return ArrayList
     */
    List<String> listUnitNameByCategoryId(Long categoryId);

    /**
     * 保存
     *
     * @param categoryId 分类ID
     * @param unitIds    规格IDS
     */
    void saveCategoryUnitBatch(long categoryId, List<Long> unitIds);

    /**
     * 按分类删除
     */
    void delByCategoryId(Long categoryId);
}
