package com.youyilin.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.exception.ApiException;
import com.youyilin.goods.mapper.ProductUnitMapper;
import com.youyilin.goods.model.dto.UnitDto;
import com.youyilin.goods.entity.ProductUnit;
import com.youyilin.goods.entity.UnitData;
import com.youyilin.goods.service.ProductUnitService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 商品销售方式 服务实现类
 */
@Service
public class ProductUnitImpl extends ServiceImpl<ProductUnitMapper, ProductUnit> implements ProductUnitService {

    @Override
    public List<ProductUnit> listByProductId(Long productId) {
        if (productId == null) {
            return new ArrayList<>();
        }
        return super.list(new LambdaQueryWrapper<ProductUnit>().eq(ProductUnit::getProductId, productId));
    }

    @Override
    public void saveProductUnit(long productId, List<UnitDto> saveUnitDtoList) {
        if (CollectionUtils.isEmpty(saveUnitDtoList)) {
            throw new ApiException("销售方式不能为空");
        }

        List<ProductUnit> saveProductUnitList = new ArrayList<>();
        for (UnitDto unit : saveUnitDtoList) {
            List<UnitData> unitDataList = unit.getItemList();
            if (CollectionUtils.isEmpty(unitDataList)) {
                throw new ApiException("销售方式不能为空");
            }
            for (UnitData unitData : unitDataList) {
                ProductUnit saveProductUnit = new ProductUnit();
                saveProductUnit.setUnitId(unit.getId());
                saveProductUnit.setUnitDataId(unitData.getId());
                saveProductUnit.setProductId(productId);

                saveProductUnitList.add(saveProductUnit);
            }
        }

        super.saveBatch(saveProductUnitList);
    }

    @Override
    public void delByProductId(long productId) {
        super.remove(new LambdaQueryWrapper<ProductUnit>().eq(ProductUnit::getProductId, productId));
    }
}
