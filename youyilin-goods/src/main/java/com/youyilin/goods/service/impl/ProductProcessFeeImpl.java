package com.youyilin.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.exception.ApiException;
import com.youyilin.goods.entity.ProductProcessFee;
import com.youyilin.goods.mapper.ProductProcessFeeMapper;
import com.youyilin.goods.service.ProductProcessFeeService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 产品-加工费 实现类
 */
@Service
public class ProductProcessFeeImpl extends ServiceImpl<ProductProcessFeeMapper, ProductProcessFee> implements ProductProcessFeeService {

    @Override
    public List<ProductProcessFee> listBySourceProductId(Long sourceProductId) {
        return super.list(new LambdaQueryWrapper<ProductProcessFee>().eq(ProductProcessFee::getSourceProductId, sourceProductId));
    }

    @Override
    public Map<Long, List<ProductProcessFee>> mapBySourceProductId(Long sourceProductId) {
        List<ProductProcessFee> feeList = this.listBySourceProductId(sourceProductId);
        if (CollectionUtils.isEmpty(feeList)) {
            return new HashMap<>();
        }
        return feeList.stream().collect(Collectors.groupingBy(ProductProcessFee::getPlanId));
    }

    @Override
    public void delByProductId(Long productId) {
        super.remove(new LambdaQueryWrapper<ProductProcessFee>().eq(ProductProcessFee::getSourceProductId, productId));
    }

    @Override
    public void saveProcessFeeBatch(Long productId, List<ProductProcessFee> batchList) {
        if (CollectionUtils.isEmpty(batchList)) {
            return;
        }

        List<String> numList = new ArrayList<>();
        batchList.forEach(item -> {
            if (item.getNum() == null || item.getNum().compareTo(BigDecimal.ZERO) < 0) {
                throw new ApiException("[加工费] 数量不能为空");
            }
            if (item.getAmount() == null || item.getAmount() < 0) {
                throw new ApiException("[加工费] 金额不能为空");
            }
            if (numList.contains(item.getNum().toString())) {
                throw new ApiException("[加工费] 数量重复");
            }
            numList.add(item.getNum().toString());
            item.setSourceProductId(productId);
        });

        super.saveBatch(batchList);
    }
}
