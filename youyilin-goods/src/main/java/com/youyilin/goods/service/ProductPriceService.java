package com.youyilin.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.goods.entity.ProductPrice;

import java.util.List;
import java.util.Map;

/**
 * 商品SKU 服务类
 */
public interface ProductPriceService extends IService<ProductPrice> {

    /**
     * 按商品查询
     *
     * @param productId 商品ID
     * @return ArrayList
     */
    List<ProductPrice> listByProductId(Long productId);

    /**
     * 按商品查询
     *
     * @param productIds 商品ID
     * @return ArrayList
     */
    List<ProductPrice> listByProductIds(List<Long> productIds);

    /**
     * 查询并分组
     *
     * @param ids IDS
     * @return Map
     */
    Map<Long, ProductPrice> mapByIds(List<Long> ids);

    /**
     * 按商品分组SKU
     *
     * @param productIds 商品ID
     * @return Map
     */
    Map<Long, List<ProductPrice>> mapByProductIds(List<Long> productIds);

    /**
     * 某商品最小销售价
     *
     * @param productId 商品
     * @return Integer
     */
    Integer minSellPriceByProductId(Long productId);

    /**
     * 获取商品SKU ID
     *
     * @param skuNames 商品SKU名称
     * @return ArrayList
     */
    List<ProductPrice> listIdsByNames(List<String> skuNames);

    /**
     * 保存
     *
     * @param productId            商品ID
     * @param saveProductPriceList SKU
     */
    void saveProductPrice(Long productId, List<ProductPrice> saveProductPriceList);

    /**
     * 删除
     *
     * @param productId 商品ID
     */
    void delByProductId(Long productId);

    /**
     * 验证商品
     *
     * @param id             商品SKU ID
     * @param validateStatus 是否验证状态
     */
    void validatePriceById(Long id, boolean validateStatus);

    /**
     * 验证是否存在
     *
     * @param id SKU
     * @return ProductPrice
     */
    ProductPrice validatePrice(Long id);

    /**
     * 验证商品SKU是否售卖
     *
     * @param ids IDS
     */
    void validatePriceSell(List<Long> ids);
}
