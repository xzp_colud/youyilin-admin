package com.youyilin.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.enums.StatusEnum;
import com.youyilin.common.exception.ApiException;
import com.youyilin.common.exception.Assert;
import com.youyilin.goods.entity.ProductPrice;
import com.youyilin.goods.mapper.ProductPriceMapper;
import com.youyilin.goods.service.ProductPriceService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 商品SKU 服务实现类
 */
@Service
public class ProductPriceImpl extends ServiceImpl<ProductPriceMapper, ProductPrice> implements ProductPriceService {

    @Override
    public List<ProductPrice> listByProductId(Long productId) {
        return super.list(new LambdaQueryWrapper<ProductPrice>()
                .eq(ProductPrice::getProductId, productId));
    }

    @Override
    public List<ProductPrice> listByProductIds(List<Long> productIds) {
        return super.list(new LambdaQueryWrapper<ProductPrice>()
                .in(ProductPrice::getProductId, productIds));
    }

    @Override
    public Map<Long, ProductPrice> mapByIds(List<Long> ids) {
        List<ProductPrice> priceList = this.listByIds(ids);
        if (CollectionUtils.isEmpty(priceList)) {
            return new HashMap<>();
        }
        return priceList.stream().collect(Collectors.toMap(ProductPrice::getId, item -> item));
    }

    @Override
    public Map<Long, List<ProductPrice>> mapByProductIds(List<Long> productIds) {
        if (productIds.isEmpty()) {
            return new HashMap<>();
        }
        List<ProductPrice> productPriceList = this.listByProductIds(productIds);
        if (productPriceList.isEmpty()) {
            return new HashMap<>();
        }
        return productPriceList.stream().collect(Collectors.groupingBy(ProductPrice::getProductId));
    }

    @Override
    public Integer minSellPriceByProductId(Long productId) {
        return baseMapper.minSellPriceByProductId(productId);
    }

    @Override
    public List<ProductPrice> listIdsByNames(List<String> skuNames) {
        return super.list(new LambdaQueryWrapper<ProductPrice>()
                .select(ProductPrice::getId, ProductPrice::getSkuName, ProductPrice::getProductId)
                .in(ProductPrice::getSkuName, skuNames));
    }

    @Override
    public void saveProductPrice(Long productId, List<ProductPrice> saveProductPriceList) {
        List<ProductPrice> saveUpdateList = new ArrayList<>();

        for (ProductPrice productPrice : saveProductPriceList) {
            if (productPrice.getSellPrice() == null || productPrice.getSellPrice() < 0) {
                throw new ApiException("销售价不能为空");
            }
            if (productPrice.getCostPrice() == null || productPrice.getCostPrice() < 0) {
                throw new ApiException("进货价不能为空");
            }
            if (productPrice.getStatus() == null || productPrice.getStatus() != StatusEnum.NORMAL.getCode()) {
                productPrice.setStatus(StatusEnum.DISABLED.getCode());
            }
            productPrice.setInventory(BigDecimal.ZERO);
            productPrice.setProductId(productId);

            saveUpdateList.add(productPrice);
        }

        super.saveOrUpdateBatch(saveUpdateList);
    }

    @Override
    public void delByProductId(Long productId) {
        super.remove(new LambdaQueryWrapper<ProductPrice>().eq(ProductPrice::getProductId, productId));
    }

    @Override
    public void validatePriceById(Long id, boolean validateStatus) {
        LambdaQueryWrapper<ProductPrice> query = new LambdaQueryWrapper<>();
        query.eq(ProductPrice::getId, id);
        if (validateStatus) {
            query.eq(ProductPrice::getStatus, StatusEnum.NORMAL.getCode());
        }
        ProductPrice productPrice = super.getOne(query);
        if (productPrice == null) {
            throw new ApiException("SKU不存在或已禁用");
        }
    }

    @Override
    public ProductPrice validatePrice(Long id) {
        ProductPrice price = super.getById(id);
        Assert.notNull(price, "SKU不存在");

        return price;
    }

    @Override
    public void validatePriceSell(List<Long> ids) {
        List<ProductPrice> priceList = this.listByIds(ids);
        if (CollectionUtils.isEmpty(priceList) || priceList.size() != ids.size()) {
            throw new ApiException("SKU不存在或已禁用");
        }
        for (ProductPrice item : priceList) {
            if (item.isDisabled()) {
                throw new ApiException("SKU" + item.getSkuName() + "已禁用");
            }
        }
    }
}
