package com.youyilin.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.goods.entity.CategoryParams;

import java.util.List;

/**
 * 商品分类 - 商品参数 服务类
 */
public interface CategoryParamsService extends IService<CategoryParams> {

    /**
     * 按分类查询
     *
     * @param categoryId 分类ID
     * @return ArrayList
     */
    List<CategoryParams> listByCategoryId(Long categoryId);

    /**
     * 按分类查询参数IDS
     *
     * @param categoryId 分类ID
     * @return ArrayList
     */
    List<String> listParamsIdsByCategoryId(Long categoryId);

    /**
     * 按分类查询参数名称
     *
     * @param categoryId 分类ID
     * @return ArrayList
     */
    List<String> listParamsNameByCategoryId(Long categoryId);

    /**
     * 按分类删除
     *
     * @param categoryId 分类ID
     */
    void delByCategoryId(Long categoryId);

    /**
     * 保存
     *
     * @param categoryId 商品分类ID
     * @param ids        参数IDS
     */
    void saveParamsBatch(Long categoryId, List<Long> ids);
}
