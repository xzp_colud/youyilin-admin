package com.youyilin.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.goods.entity.FrontMenu;

import java.util.List;

public interface FrontMenuService extends IService<FrontMenu> {

    /**
     * 所有分类
     *
     * @return ArrayList
     */
    List<FrontMenu> listAll();

    /**
     * 所有正常的分类
     *
     * @return ArrayList
     */
    List<FrontMenu> listAllNormal();

    /**
     * 按名称查询
     *
     * @param name 名称
     * @return FrontMenu
     */
    FrontMenu getByName(String name);

    /**
     * 保存
     */
    void saveFrontMenu(FrontMenu en);

    /**
     * 修改状态
     */
    void updateStatus(FrontMenu en);

    /**
     * 验证名称唯一性
     */
    void validateNameUnique(FrontMenu en);
}
