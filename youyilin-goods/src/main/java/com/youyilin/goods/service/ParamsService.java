package com.youyilin.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.goods.entity.Params;

import java.util.List;

/**
 * 商品参数 服务类
 */
public interface ParamsService extends IService<Params> {

    /**
     * 按名称查询
     *
     * @param name 名称
     * @return Params
     */
    Params getByName(String name);

    /**
     * 按分类ID查询属性参数
     *
     * @param categoryId 分类ID
     * @return ArrayList
     */
    List<Params> listByCategoryId(Long categoryId);

    /**
     * 名称唯一性
     */
    void validateNameUnique(Params en);

    /**
     * 验证属性参数是否存在
     *
     * @param id 属性参数ID
     */
    Params validateParams(Long id);
}
