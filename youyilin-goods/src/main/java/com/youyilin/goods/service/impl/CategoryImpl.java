package com.youyilin.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.enums.StatusEnum;
import com.youyilin.common.exception.Assert;
import com.youyilin.common.utils.ObjectUniqueUtil;
import com.youyilin.goods.entity.Category;
import com.youyilin.goods.mapper.CategoryMapper;
import com.youyilin.goods.service.CategoryService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryImpl extends ServiceImpl<CategoryMapper, Category> implements CategoryService {

    @Override
    public Category getByTitle(String title) {
        return super.getOne(new LambdaQueryWrapper<Category>()
                .eq(Category::getTitle, title));
    }

    @Override
    public List<Category> listAll(Integer status) {
        LambdaQueryWrapper<Category> query = new LambdaQueryWrapper<>();
        if (status != null) {
            query.eq(Category::getStatus, status);
        }
        return super.list(query.orderByAsc(Category::getSort));
    }

    @Override
    public List<Category> listIdsByTitles(List<String> categoryTitles) {
        return super.list(new LambdaQueryWrapper<Category>()
                .select(Category::getId, Category::getTitle)
                .in(Category::getTitle, categoryTitles));
    }

    @Override
    public Category validateCategory(Long id) {
        Category category = super.getById(id);
        Assert.notNull(category, "分类不存在");

        return category;
    }

    @Override
    public Category validateCategoryStatus(Long id) {
        Category category = this.validateCategory(id);
        boolean statusFlag = category.getStatus() != null && category.getStatus() == StatusEnum.NORMAL.getCode();
        Assert.isTrue(statusFlag, category.getTitle() + " 已禁用");

        return category;
    }

    @Override
    public void validateTitleUnique(Category en) {
        Category titleUnique = this.getByTitle(en.getTitle());
        Assert.isTrue(ObjectUniqueUtil.validateObjectUnique(en, titleUnique), "名称已存在");
    }
}
