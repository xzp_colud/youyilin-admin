package com.youyilin.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.goods.entity.ProductPlan;

import java.util.List;

/**
 * 产品方案
 */
public interface ProductPlanService extends IService<ProductPlan> {

    /**
     * 按产品查询
     *
     * @param productId 产品ID
     * @return ArrayList
     */
    List<ProductPlan> listByProductId(Long productId);

    /**
     * 按产品删除
     *
     * @param productId 产品ID
     */
    void delByProductId(Long productId);

    /**
     * 保存
     */
    void savePlanBatch(Long productId, List<ProductPlan> list);
}
