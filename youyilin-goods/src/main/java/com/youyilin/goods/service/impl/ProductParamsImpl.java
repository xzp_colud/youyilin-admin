package com.youyilin.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.exception.Assert;
import com.youyilin.goods.entity.ProductParams;
import com.youyilin.goods.mapper.ProductParamsMapper;
import com.youyilin.goods.service.ProductParamsService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 产品参数
 */
@Service
public class ProductParamsImpl extends ServiceImpl<ProductParamsMapper, ProductParams> implements ProductParamsService {

    @Override
    public List<ProductParams> listByProductId(Long productId) {
        return baseMapper.listByProductId(productId);
    }

    @Override
    public List<ProductParams> listByProductIds(List<Long> productIds) {
        if (CollectionUtils.isEmpty(productIds)) {
            return new ArrayList<>();
        }
        return baseMapper.listByProductIds(productIds);
    }

    @Override
    public void saveProductParamsBatch(Long productId, List<ProductParams> list) {
        if (CollectionUtils.isEmpty(list)) return;

        list.forEach(item -> {
            Assert.notNull(item.getParamsId(), "参数错误");
            item.setProductId(productId);
        });

        super.saveBatch(list);
    }

    @Override
    public void delByProductId(Long productId) {
        super.remove(new LambdaQueryWrapper<ProductParams>().eq(ProductParams::getProductId, productId));
    }
}
