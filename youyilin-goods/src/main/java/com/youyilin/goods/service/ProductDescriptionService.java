package com.youyilin.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.goods.entity.ProductDescription;

public interface ProductDescriptionService extends IService<ProductDescription> {

    /**
     * 按产品查询
     *
     * @param productId 产品ID
     */
    String getByProductId(Long productId);

    /**
     * 保存
     *
     * @param productId   产品ID
     * @param description 描述
     */
    void saveDescription(Long productId, String description);

    /**
     * 删除
     *
     * @param productId 产品ID
     */
    void delByProductId(Long productId);
}
