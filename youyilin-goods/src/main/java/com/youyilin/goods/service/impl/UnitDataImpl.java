package com.youyilin.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.bean.Page;
import com.youyilin.common.exception.ApiException;
import com.youyilin.goods.entity.UnitData;
import com.youyilin.goods.mapper.UnitDataMapper;
import com.youyilin.goods.service.UnitDataService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 销售单位键值 服务实现类
 */
@Service
public class UnitDataImpl extends ServiceImpl<UnitDataMapper, UnitData> implements UnitDataService {

    @Override
    public Integer getTotal(Page<UnitData> page) {
        return baseMapper.getTotal(page);
    }

    @Override
    public List<UnitData> getPage(Page<UnitData> page) {
        return baseMapper.getPage(page);
    }

    @Override
    public List<UnitData> listByUnitId(Long unitId) {
        LambdaQueryWrapper<UnitData> query = new LambdaQueryWrapper<>();
        if (unitId != null) {
            query.eq(UnitData::getUnitId, unitId);
        }
        query.isNull(UnitData::getProductId);
        return super.list(query);
    }

    @Override
    public List<UnitData> listByProductId(Long productId) {
        return super.list(new LambdaQueryWrapper<UnitData>().eq(UnitData::getProductId, productId));
    }

    @Override
    public List<UnitData> listForEdit(Long productId) {
        return baseMapper.listForEdit(productId);
    }

    @Override
    public List<UnitData> listForDetail(Long productId) {
        return baseMapper.listForDetail(productId);
    }

    @Override
    public Map<Long, List<UnitData>> mapByUnitIds(List<Long> unitIds) {
        List<UnitData> unitDataList = super.list(new LambdaQueryWrapper<UnitData>()
                .in(UnitData::getUnitId, unitIds)
                .isNull(UnitData::getProductId));
        return generateMapByUnitId(unitDataList);
    }

    @Override
    public Map<Long, List<UnitData>> mapByIds(List<Long> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return new HashMap<>();
        }
        List<UnitData> unitDataList = super.list(new LambdaQueryWrapper<UnitData>()
                .in(UnitData::getId, ids));
        return generateMapByUnitId(unitDataList);
    }

    @Override
    public Map<Long, List<UnitData>> mapByProductId(Long productId) {
        return generateMapByUnitId(this.listByProductId(productId));
    }

    private Map<Long, List<UnitData>> generateMapByUnitId(List<UnitData> unitDataList) {
        if (CollectionUtils.isEmpty(unitDataList)) {
            return new HashMap<>();
        }
        return unitDataList.stream().collect(Collectors.groupingBy(UnitData::getUnitId));
    }

    @Override
    public void saveUnitData(UnitData en) {
//        this.validateValuesUnique(en);
        en.setLabel(en.getValue());
        en.setText(en.getText());
        if (en.getId() == null) {
            super.save(en);
        } else {
            UnitData oldUnitData = super.getById(en.getId());
            if (oldUnitData == null || !oldUnitData.getUnitId().equals(en.getUnitId())) {
                throw new ApiException("前后数据不一致");
            }

            super.updateById(en);
        }
    }

    @Override
    public void saveUnitDataByProduct(UnitData en) {
        if (StringUtils.isBlank(en.getValue())
                || en.getUnitId() == null
                || en.getProductId() == null) {
            throw new ApiException("参数异常");
        }
        UnitData save = new UnitData();
        save.setUnitId(en.getUnitId());
        save.setProductId(en.getProductId());
        save.setValue(en.getValue());
        save.setLabel(en.getValue());
        save.setText(en.getValue());

        super.save(save);

        en.setId(save.getId());
    }

    @Override
    public void updateProductId(Long randomId, Long productId) {
        super.update(new LambdaUpdateWrapper<UnitData>()
                .set(UnitData::getProductId, productId)
                .eq(UnitData::getProductId, randomId));
    }

    @Override
    public void delByProductId(Long randomId) {
        super.remove(new LambdaQueryWrapper<UnitData>().eq(UnitData::getProductId, randomId));
    }

    @Override
    public void delByIdsForProduct(Long productId, List<Long> delUnitDataList) {
        if (CollectionUtils.isNotEmpty(delUnitDataList)) {
            List<UnitData> unitDataList = super.listByIds(delUnitDataList);
            unitDataList.forEach(item -> {
                if (item.getProductId() == null) {
                    throw new ApiException("参数异常");
                }
                if (!item.getProductId().toString().equals(productId.toString())) {
                    throw new ApiException("参数异常");
                }
            });

            super.removeByIds(delUnitDataList);
        }
    }
}
