package com.youyilin.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.exception.ApiException;
import com.youyilin.goods.mapper.ProductFabricMapper;
import com.youyilin.goods.entity.ProductFabric;
import com.youyilin.goods.service.ProductFabricService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ProductFabricImpl extends ServiceImpl<ProductFabricMapper, ProductFabric> implements ProductFabricService {

    @Override
    public List<ProductFabric> listBySourceProductId(Long productId) {
        return super.list(new LambdaQueryWrapper<ProductFabric>().eq(ProductFabric::getSourceProductId, productId));
    }

    @Override
    public Map<Long, List<ProductFabric>> mapBySourceProductId(Long sourceProductId) {
        List<ProductFabric> fabricList = this.listBySourceProductId(sourceProductId);
        if (CollectionUtils.isEmpty(fabricList)) {
            return new HashMap<>();
        }
        return fabricList.stream().collect(Collectors.groupingBy(ProductFabric::getPlanId));
    }

    @Override
    public List<ProductFabric> listByPlanId(Long productPlanId) {
        return super.list(new LambdaQueryWrapper<ProductFabric>().eq(ProductFabric::getPlanId, productPlanId));
    }

    @Override
    public List<ProductFabric> listByPlanIds(List<Long> productPlanIds) {
        return super.list(new LambdaQueryWrapper<ProductFabric>().in(ProductFabric::getPlanId, productPlanIds));
    }

    @Override
    public void updateRedundancyProductName(Long productId, String productName) {
        super.update(new LambdaUpdateWrapper<ProductFabric>()
                .set(ProductFabric::getProductName, productName)
                .eq(ProductFabric::getProductId, productId));
    }

    @Override
    public void delByProductId(Long productId) {
        super.remove(new LambdaQueryWrapper<ProductFabric>().eq(ProductFabric::getSourceProductId, productId));
    }

    @Override
    public void saveFabricBatch(Long productId, List<ProductFabric> batchList) {
        if (CollectionUtils.isEmpty(batchList)) {
            return;
        }

        batchList.forEach(item -> {
            if (item.getSupplierId() == null) {
                throw new ApiException("[面料] 厂家不能为空");
            }
            if (item.getProductId() == null) {
                throw new ApiException("[面料] 型号不能为空");
            }
            if (item.getProductPriceId() == null) {
                throw new ApiException("[面料] SKU不能为空");
            }
            if (item.getOneUsage() == null || item.getOneUsage().compareTo(BigDecimal.ZERO) < 0) {
                throw new ApiException("[面料] 单件用量不能为空");
            }
            if (item.getAmount() == null || item.getAmount() < 0) {
                throw new ApiException("[面料] 单价不能为零");
            }
            if (item.getTotalAmount() == null || item.getTotalAmount() < 0) {
                throw new ApiException("[面料] 总价不能为零");
            }
            item.setSourceProductId(productId);
        });

        super.saveBatch(batchList);
    }
}
