package com.youyilin.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.exception.ApiException;
import com.youyilin.common.utils.ObjectUniqueUtil;
import com.youyilin.goods.mapper.SpaceMapper;
import com.youyilin.goods.entity.Space;
import com.youyilin.goods.service.SpaceService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 空间 服务实现类
 */
@Service
public class SpaceImpl extends ServiceImpl<SpaceMapper, Space> implements SpaceService {

    @Override
    public Space getByName(String name) {
        return super.getOne(new LambdaQueryWrapper<Space>().eq(Space::getName, name));
    }

    @Override
    public List<Space> listAll() {
        return super.list(new LambdaQueryWrapper<Space>().orderByAsc(Space::getSort));
    }

    @Override
    public void saveSpace(Space en) {
        this.validateNameUnique(en);
        if (en.getId() == null) {
            super.save(en);
        } else {
            throw new ApiException("不允许修改");
        }
    }

    @Override
    public void validateNameUnique(Space en) {
        Space nameUnique = this.getByName(en.getName());
        if (!ObjectUniqueUtil.validateObjectUnique(en, nameUnique)) {
            throw new ApiException("空间名称已存在");
        }
    }
}
