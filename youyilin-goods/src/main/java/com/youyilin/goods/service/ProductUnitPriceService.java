package com.youyilin.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.goods.entity.ProductUnitPrice;

import java.util.List;
import java.util.Map;

/**
 * 商品销售方式SKU路径 服务类
 */
public interface ProductUnitPriceService extends IService<ProductUnitPrice> {

    /**
     * 某商品销售方式SKU
     *
     * @param productId 商品ID
     * @return ArrayList
     */
    List<ProductUnitPrice> listByProductId(Long productId);

    /**
     * 根据商品ID获取规格与sku的关系并分组
     *
     * @param productId 商品ID
     * @return Map
     */
    Map<Long, ProductUnitPrice> mapByProductId(Long productId);

    /**
     * 保存
     *
     * @param productId                商品ID
     * @param saveProductUnitPriceList SKU路径
     */
    void saveProductUnitPrice(long productId, List<ProductUnitPrice> saveProductUnitPriceList);

    /**
     * 删除
     *
     * @param productId 商品ID
     */
    void delByProductId(long productId);
}
