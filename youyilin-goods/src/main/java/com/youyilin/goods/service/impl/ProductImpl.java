package com.youyilin.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.enums.StatusEnum;
import com.youyilin.common.exception.ApiException;
import com.youyilin.common.utils.FormatAmountUtil;
import com.youyilin.common.utils.ObjectUniqueUtil;
import com.youyilin.goods.entity.Product;
import com.youyilin.goods.mapper.ProductMapper;
import com.youyilin.goods.service.ProductService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ProductImpl extends ServiceImpl<ProductMapper, Product> implements ProductService {

    @Override
    public Product getBySn(String serialNum) {
        return super.getOne(new LambdaQueryWrapper<Product>()
                .eq(Product::getSn, serialNum));
    }

    @Override
    public Map<Long, List<Product>> mapByCategory() {
        List<Product> productList = baseMapper.listByCategory();
        if (CollectionUtils.isEmpty(productList)) {
            return new HashMap<>();
        }
        return productList.stream().collect(Collectors.groupingBy(Product::getCategoryId));
    }

    @Override
    public List<Product> listIdsByNames(List<String> names) {
        return super.list(new LambdaQueryWrapper<Product>()
                .select(Product::getId, Product::getName, Product::getSupplierId, Product::getSupplierName)
                .in(Product::getName, names));
    }

    @Override
    public Map<Long, Product> mapByIds(List<Long> ids) {
        List<Product> productList = super.listByIds(ids);
        if (CollectionUtils.isEmpty(productList)) {
            return new HashMap<>();
        }
        return productList.stream().collect(Collectors.toMap(Product::getId, item -> item));
    }

    @Override
    public void saveProduct(Product en) {
        this.validateSnUnique(en);
        if (en.getId() == null) {
//            en.setStatus(StatusEnum.NORMAL.getCode());
            if (en.getStatus() == null) {
                en.setStatus(StatusEnum.DISABLED.getCode());
            }
            en.setSales(BigDecimal.ZERO);
            super.save(en);
        } else {
            Product oldProduct = super.getById(en.getId());
            if (oldProduct == null
                    || oldProduct.getCategoryId() == null
                    || !oldProduct.getCategoryId().toString().equals(en.getCategoryId().toString())) {
                throw new ApiException("商品分类不一致");
            }
            super.updateById(en);
        }
    }

    @Override
    public void updateRedundancySupplierName(Long supplierId, String supplierName) {
        super.update(new LambdaUpdateWrapper<Product>()
                .set(Product::getSupplierName, supplierName)
                .eq(Product::getSupplierId, supplierId));
    }

    @Override
    public void updateRedundancyCategoryName(Long categoryId, String categoryName) {
        super.update(new LambdaUpdateWrapper<Product>()
                .set(Product::getCategoryName, categoryName)
                .eq(Product::getCategoryId, categoryId));
    }

    @Override
    public void updateMinPrice(Long id, Integer price) {
        if (id == null || price == null || price <= 0) {
            throw new ApiException("商品异常");
        }
        String priceText = FormatAmountUtil.format(price);
        super.update(new LambdaUpdateWrapper<Product>()
                .set(Product::getPriceText, priceText)
                .eq(Product::getId, id));
    }

    @Override
    public void updateStatus(Long id, Integer status) {
        Product product = super.getById(id);
        if (product == null) return;

        if (product.getStatus() != null && product.getStatus().equals(status)) return;

        Integer updateStatus = status.equals(StatusEnum.NORMAL.getCode()) ? StatusEnum.NORMAL.getCode() : StatusEnum.DISABLED.getCode();

        super.update(new LambdaUpdateWrapper<Product>()
                .set(Product::getStatus, updateStatus)
                .eq(Product::getId, id));
    }

    @Override
    public void validateSnUnique(Product en) {
        if (StringUtils.isNotBlank(en.getSn())) {
            Product snUnique = this.getBySn(en.getSn());
            if (!ObjectUniqueUtil.validateObjectUnique(en, snUnique)) {
                throw new ApiException("内部编号已存在");
            }
        }
    }

    @Override
    public Product validateProduct(Long id) {
        Product product = super.getById(id);
        if (product == null) {
            throw new ApiException("商品信息不存在");
        }

        return product;
    }

    @Override
    public Product validateProductSell(Long id) {
        Product product = this.validateProduct(id);
        if (product.isDisabled()) {
            throw new ApiException("商品已下架");
        }

        return product;
    }

    @Override
    public void validateProductSell(List<Long> ids) {
        List<Product> productList = super.listByIds(ids);
        if (CollectionUtils.isEmpty(productList) || productList.size() != ids.size()) {
            throw new ApiException("商品已下架");
        }
        for (Product item : productList) {
            if (item.isDisabled()) {
                throw new ApiException("商品 " + item.getName() + " 已下架");
            }
        }
    }
}
