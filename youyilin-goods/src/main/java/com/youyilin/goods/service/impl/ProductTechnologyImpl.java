package com.youyilin.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.exception.ApiException;
import com.youyilin.goods.entity.ProductTechnology;
import com.youyilin.goods.mapper.ProductTechnologyMapper;
import com.youyilin.goods.service.ProductTechnologyService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ProductTechnologyImpl extends ServiceImpl<ProductTechnologyMapper, ProductTechnology> implements ProductTechnologyService {

    @Override
    public List<ProductTechnology> listBySourceProductId(Long sourceProductId) {
        return super.list(new LambdaQueryWrapper<ProductTechnology>().eq(ProductTechnology::getSourceProductId, sourceProductId));
    }

    @Override
    public Map<Long, List<ProductTechnology>> mapBySourceProductId(Long sourceProductId) {
        List<ProductTechnology> technologyList = this.listBySourceProductId(sourceProductId);
        if (CollectionUtils.isEmpty(technologyList)) {
            return new HashMap<>();
        }
        return technologyList.stream().collect(Collectors.groupingBy(ProductTechnology::getPlanId));
    }

    @Override
    public void delByProductId(Long productId) {
        super.remove(new LambdaQueryWrapper<ProductTechnology>().eq(ProductTechnology::getSourceProductId, productId));
    }

    @Override
    public void saveTechnologyBatch(Long productId, List<ProductTechnology> batchList) {
        if (CollectionUtils.isEmpty(batchList)) {
            return;
        }

        batchList.forEach(item -> {
            if (StringUtils.isBlank(item.getSupplierName())) {
                throw new ApiException("[工艺] 厂家不能为空");
            }
            if (StringUtils.isBlank(item.getTechnologyName())) {
                throw new ApiException("[工艺] 工艺不能为空");
            }
            item.setSourceProductId(productId);
        });

        super.saveBatch(batchList);
    }
}
