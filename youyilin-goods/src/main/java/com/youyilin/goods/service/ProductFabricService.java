package com.youyilin.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.goods.entity.ProductFabric;

import java.util.List;
import java.util.Map;

public interface ProductFabricService extends IService<ProductFabric> {

    /**
     * 按产品ID查询
     *
     * @param sourceProductId 源产品ID
     * @return ArrayList
     */
    List<ProductFabric> listBySourceProductId(Long sourceProductId);

    /**
     * 按产品ID查询
     *
     * @param sourceProductId 源产品ID
     * @return Map
     */
    Map<Long, List<ProductFabric>> mapBySourceProductId(Long sourceProductId);

    /**
     * 按方案查询
     *
     * @param productPlanId 方案ID
     * @return ArrayList
     */
    List<ProductFabric> listByPlanId(Long productPlanId);

    /**
     * 按方案查询
     *
     * @param productPlanIds 方案ID
     * @return ArrayList
     */
    List<ProductFabric> listByPlanIds(List<Long> productPlanIds);

    /**
     * 冗余产品名称
     */
    void updateRedundancyProductName(Long productId, String productName);

    /**
     * 按产品ID删除
     *
     * @param productId 产品ID
     */
    void delByProductId(Long productId);

    /**
     * 批量保存
     */
    void saveFabricBatch(Long productId, List<ProductFabric> batchList);
}
