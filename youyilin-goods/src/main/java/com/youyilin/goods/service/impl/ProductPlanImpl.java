package com.youyilin.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.exception.ApiException;
import com.youyilin.goods.mapper.ProductPlanMapper;
import com.youyilin.goods.entity.ProductPlan;
import com.youyilin.goods.service.ProductPlanService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 产品方案
 */
@Service
public class ProductPlanImpl extends ServiceImpl<ProductPlanMapper, ProductPlan> implements ProductPlanService {

    @Override
    public List<ProductPlan> listByProductId(Long productId) {
        return super.list(new LambdaQueryWrapper<ProductPlan>().eq(ProductPlan::getProductId, productId));
    }

    @Override
    public void delByProductId(Long productId) {
        super.remove(new LambdaQueryWrapper<ProductPlan>().eq(ProductPlan::getProductId, productId));
    }

    @Override
    public void savePlanBatch(Long productId, List<ProductPlan> list) {
        if (CollectionUtils.isEmpty(list)) return;

        if (productId == null) {
            throw new ApiException("产品异常");
        }

        for (ProductPlan item : list) {
            if (item.getId() == null) {
                throw new ApiException("方案异常");
            }
            if (StringUtils.isBlank(item.getTitle())) {
                throw new ApiException("方案标题不能为空");
            }
            item.setProductId(productId);
        }
        super.saveBatch(list);
    }
}
