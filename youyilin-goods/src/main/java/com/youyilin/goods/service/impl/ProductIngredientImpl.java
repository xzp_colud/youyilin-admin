package com.youyilin.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.exception.ApiException;
import com.youyilin.goods.mapper.ProductIngredientMapper;
import com.youyilin.goods.entity.ProductIngredient;
import com.youyilin.goods.service.ProductIngredientService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ProductIngredientImpl extends ServiceImpl<ProductIngredientMapper, ProductIngredient> implements ProductIngredientService {

    @Override
    public List<ProductIngredient> listBySourceProductId(Long sourceProductId) {
        return super.list(new LambdaQueryWrapper<ProductIngredient>().eq(ProductIngredient::getSourceProductId, sourceProductId));
    }

    @Override
    public Map<Long, List<ProductIngredient>> mapBySourceProductId(Long sourceProductId) {
        List<ProductIngredient> ingredientList = this.listBySourceProductId(sourceProductId);
        if (CollectionUtils.isEmpty(ingredientList)) {
            return new HashMap<>();
        }
        return ingredientList.stream().collect(Collectors.groupingBy(ProductIngredient::getPlanId));
    }

    @Override
    public List<ProductIngredient> listByPlanId(Long productPlanId) {
        return super.list(new LambdaQueryWrapper<ProductIngredient>().eq(ProductIngredient::getPlanId, productPlanId));
    }

    @Override
    public List<ProductIngredient> listByPlanIds(List<Long> productPlanIds) {
        return super.list(new LambdaQueryWrapper<ProductIngredient>().in(ProductIngredient::getPlanId, productPlanIds));
    }

    @Override
    public void updateRedundancyProductName(Long productId, String productName) {
        super.update(new LambdaUpdateWrapper<ProductIngredient>()
                .set(ProductIngredient::getProductName, productName)
                .eq(ProductIngredient::getProductId, productId));
    }

    @Override
    public void delByProductId(Long productId) {
        super.remove(new LambdaQueryWrapper<ProductIngredient>().eq(ProductIngredient::getSourceProductId, productId));
    }

    @Override
    public void saveIngredientBatch(Long productId, List<ProductIngredient> batchList) {
        if (CollectionUtils.isEmpty(batchList)) {
            return;
        }

        batchList.forEach(item -> {
            if (item.getSupplierId() == null) {
                throw new ApiException("[配料] 厂家不能为空");
            }
            if (item.getProductId() == null) {
                throw new ApiException("[配料] 品名不能为空");
            }
            if (item.getProductPriceId() == null) {
                throw new ApiException("[配料] SKU不能为空");
            }
            if (item.getOneUsage() == null || item.getOneUsage().compareTo(BigDecimal.ZERO) < 0) {
                throw new ApiException("[配料] 单件用量不能为空");
            }
            if (item.getAmount() == null || item.getAmount() < 0) {
                throw new ApiException("[配料] 单价不能为零");
            }
            if (item.getTotalAmount() == null || item.getTotalAmount() < 0) {
                throw new ApiException("[配料] 总价不能为零");
            }
            item.setSourceProductId(productId);
        });

        super.saveBatch(batchList);
    }
}
