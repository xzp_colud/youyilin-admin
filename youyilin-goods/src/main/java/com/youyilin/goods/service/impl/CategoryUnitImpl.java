package com.youyilin.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.goods.mapper.CategoryUnitMapper;
import com.youyilin.goods.entity.CategoryUnit;
import com.youyilin.goods.service.CategoryUnitService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 类目销售方式 服务实现类
 */
@Service
public class CategoryUnitImpl extends ServiceImpl<CategoryUnitMapper, CategoryUnit> implements CategoryUnitService {

    @Override
    public List<CategoryUnit> listByCategoryId(Long categoryId) {
        return super.list(new LambdaQueryWrapper<CategoryUnit>().eq(CategoryUnit::getCategoryId, categoryId));
    }

    @Override
    public List<String> listUnitIdsByCategoryId(Long categoryId) {
        return super.listObjs(new LambdaQueryWrapper<CategoryUnit>()
                .select(CategoryUnit::getUnitId)
                .eq(CategoryUnit::getCategoryId, categoryId), Object::toString);
    }

    @Override
    public List<String> listUnitNameByCategoryId(Long categoryId) {
        return baseMapper.listUnitNameByCategoryId(categoryId);
    }

    @Override
    public void saveCategoryUnitBatch(long categoryId, List<Long> unitIds) {
        if (CollectionUtils.isEmpty(unitIds)) {
            return;
        }

        List<CategoryUnit> saveList = unitIds.stream().map(item -> {
            CategoryUnit save = new CategoryUnit();
            save.setCategoryId(categoryId);
            save.setUnitId(item);

            return save;
        }).collect(Collectors.toList());

        super.saveBatch(saveList);
    }

    @Override
    public void delByCategoryId(Long categoryId) {
        super.remove(new LambdaQueryWrapper<CategoryUnit>().eq(CategoryUnit::getCategoryId, categoryId));
    }
}
