package com.youyilin.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.common.bean.Page;
import com.youyilin.goods.entity.WebsiteImage;

import java.util.List;

public interface WebsiteImageService extends IService<WebsiteImage> {

    Integer getTotal(Page<WebsiteImage> page);

    List<WebsiteImage> getPage(Page<WebsiteImage> page);

    List<WebsiteImage> listAll();
}
