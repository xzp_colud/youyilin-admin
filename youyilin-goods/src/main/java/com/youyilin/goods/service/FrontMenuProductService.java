package com.youyilin.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.common.bean.Page;
import com.youyilin.goods.model.dto.FrontMenuProductDto;
import com.youyilin.goods.entity.FrontMenuProduct;
import com.youyilin.goods.model.request.RequestFrontMenuProduct;

import java.util.List;

public interface FrontMenuProductService extends IService<FrontMenuProduct> {

    /**
     * 列表
     */
    List<FrontMenuProductDto> getPage(Page<FrontMenuProductDto> page);

    /**
     * 列表
     */
    List<FrontMenuProductDto> listPageByFrontMenuId(Long frontMenuId);

    /**
     * 保存
     */
    void saveFrontMenuProduct(RequestFrontMenuProduct request);

    /**
     * 按分类删除
     */
    void delByFrontMenuId(Long frontMenuId);
}
