package com.youyilin.goods.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.RMsg;
import com.youyilin.common.exception.ApiException;
import com.youyilin.goods.mapper.SpaceImageMapper;
import com.youyilin.goods.entity.SpaceImage;
import com.youyilin.goods.service.SpaceImageService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 空间图片 服务实现类
 */
@Service
public class SpaceImageImpl extends ServiceImpl<SpaceImageMapper, SpaceImage> implements SpaceImageService {

    @Override
    public Integer getTotal(Page<SpaceImage> page) {
        return baseMapper.getTotal(page);
    }

    @Override
    public List<SpaceImage> getPage(Page<SpaceImage> page) {
        return baseMapper.getPage(page);
    }

    @Override
    public void saveSpaceImage(SpaceImage en) {
        if (en.getId() != null) {
            throw new ApiException(RMsg.NO_PARAMS.getMsg());
        }
        super.save(en);
    }

    @Override
    public void delSpaceImage(Long id) {
        if (id == null) return;
        super.removeById(id);
    }
}
