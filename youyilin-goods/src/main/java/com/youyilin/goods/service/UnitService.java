package com.youyilin.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.common.bean.Page;
import com.youyilin.goods.model.dto.UnitDto;
import com.youyilin.goods.entity.Unit;

import java.util.List;

/**
 * 销售单位 服务类
 */
public interface UnitService extends IService<Unit> {

    Integer getTotal(Page<Unit> page);

    List<Unit> getPage(Page<Unit> page);

    /**
     * 根据销售单位名称查询
     *
     * @param name 销售单位名称
     * @return Object
     */
    Unit getByName(String name);

    /**
     * 所有销售方式
     */
    List<Unit> listAll();

    /**
     * 某销售方式下(包含键值)(默认)
     */
    List<UnitDto> listAllDtoByUnitIds(List<Long> unitIds);

    /**
     * 某销售方式下(包含键值)(由商品添加)
     */
    List<UnitDto> listAllDtoByProductId(List<Long> unitIds, Long productId);

    /**
     * 某商品所有销售方式(包含键值)(详情)
     */
    List<UnitDto> listForDetail(Long productId);

    /**
     * 某商品所有销售方式(包含键值)(编辑)
     */
    List<UnitDto> listForEdit(Long productId);

    /**
     * 某类目下的销售方式(包含键值)(新增)
     */
    List<UnitDto> listForAdd(Long categoryId);

    /**
     * 保存
     */
    void saveUnit(Unit en);

    /**
     * 验证销售单位名称唯一性
     */
    void validateNameUnique(Unit en);
}
