package com.youyilin.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.common.bean.Page;
import com.youyilin.goods.entity.UnitData;

import java.util.List;
import java.util.Map;

/**
 * 规格键值 服务类
 */
public interface UnitDataService extends IService<UnitData> {

    Integer getTotal(Page<UnitData> page);

    List<UnitData> getPage(Page<UnitData> page);

    /**
     * 根据规格键值查询
     *
     * @param unitId 规格ID
     * @return ArrayList
     */
    List<UnitData> listByUnitId(Long unitId);

    /**
     * 根据规格键值查询
     *
     * @param productId 销售单位ID
     * @return ArrayList
     */
    List<UnitData> listByProductId(Long productId);

    /**
     * 某商品所有规格键值(编辑)
     *
     * @param productId 商品ID
     * @return ArrayList
     */
    List<UnitData> listForEdit(Long productId);

    /**
     * 某商品所有规格键值(详情)
     *
     * @param productId 商品ID
     * @return ArrayList
     */
    List<UnitData> listForDetail(Long productId);

    /**
     * 按规格分组
     *
     * @param unitIds 销售单位ID
     * @return HashMap
     */
    Map<Long, List<UnitData>> mapByUnitIds(List<Long> unitIds);

    /**
     * 按规格分组
     *
     * @param ids 销售方式键值ID
     * @return HashMap
     */
    Map<Long, List<UnitData>> mapByIds(List<Long> ids);

    /**
     * 按规格分组
     *
     * @param productId 商品ID
     * @return HashMap
     */
    Map<Long, List<UnitData>> mapByProductId(Long productId);

    /**
     * 添加
     */
    void saveUnitData(UnitData en);

    /**
     * 保存通过产品界面添加
     */
    void saveUnitDataByProduct(UnitData en);

    /**
     * 修改保存后的产品ID
     *
     * @param randomId  随机ID
     * @param productId 商品ID
     */
    void updateProductId(Long randomId, Long productId);

    /**
     * 删除 随机生成出来的键值
     *
     * @param randomId 产品随机ID
     */
    void delByProductId(Long randomId);

    /**
     * 删除
     */
    void delByIdsForProduct(Long productId, List<Long> delUnitDataList);
}
