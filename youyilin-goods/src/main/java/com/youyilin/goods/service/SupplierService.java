package com.youyilin.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.goods.entity.Supplier;

import java.util.List;

/**
 * 供货商 服务类
 */
public interface SupplierService extends IService<Supplier> {

    /**
     * 根据名称查询
     *
     * @param name 供货商名称
     * @return Object
     */
    Supplier getByName(String name);

    /**
     * 所有供货商
     *
     * @param status 状态
     * @return ArrayList
     */
    List<Supplier> listAll(Integer status);

    /**
     * 按名称只查询ID
     *
     * @return ArrayList
     */
    List<Supplier> listIdsByNames(List<String> supplierNames);

    /**
     * 验证供货商名称唯一性
     */
    void validateNameUnique(Supplier en);

    /**
     * 验证供货商是否存在
     *
     * @param id 供应商ID
     * @return Object 供应商
     */
    Supplier validateSupplier(Long id);

    /**
     * 验证供货商状态
     *
     * @param id 供应商ID
     * @return Supplier
     */
    Supplier validateSupplierStatus(Long id);
}
