package com.youyilin.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.goods.mapper.CategoryParamsMapper;
import com.youyilin.goods.entity.CategoryParams;
import com.youyilin.goods.service.CategoryParamsService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 商品分类 - 商品参数 实现类
 */
@Service
public class CategoryParamsImpl extends ServiceImpl<CategoryParamsMapper, CategoryParams> implements CategoryParamsService {

    @Override
    public List<CategoryParams> listByCategoryId(Long categoryId) {
        return super.list(new LambdaQueryWrapper<CategoryParams>().eq(CategoryParams::getCategoryId, categoryId));
    }

    @Override
    public List<String> listParamsIdsByCategoryId(Long categoryId) {
        return super.listObjs(new LambdaQueryWrapper<CategoryParams>()
                .select(CategoryParams::getParamsId)
                .eq(CategoryParams::getCategoryId, categoryId), Object::toString);
    }

    @Override
    public List<String> listParamsNameByCategoryId(Long categoryId) {
        return baseMapper.listParamsNameByCategoryId(categoryId);
    }

    @Override
    public void delByCategoryId(Long categoryId) {
        super.remove(new LambdaQueryWrapper<CategoryParams>().eq(CategoryParams::getCategoryId, categoryId));
    }

    @Override
    public void saveParamsBatch(Long categoryId, List<Long> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return;
        }

        List<CategoryParams> batchList = ids.stream().map(item -> {
            CategoryParams cp = new CategoryParams();
            cp.setCategoryId(categoryId);
            cp.setParamsId(item);
            return cp;
        }).collect(Collectors.toList());

        super.saveBatch(batchList);
    }
}
