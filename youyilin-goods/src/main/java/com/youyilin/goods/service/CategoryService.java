package com.youyilin.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.goods.entity.Category;

import java.util.List;

/**
 * 类目 服务类
 */
public interface CategoryService extends IService<Category> {

    /**
     * 根据类目名称查询
     *
     * @param title 名称
     * @return Object
     */
    Category getByTitle(String title);

    /**
     * 所有类目
     *
     * @param status 状态
     * @return ArrayList
     */
    List<Category> listAll(Integer status);

    /**
     * 按名称只查询ID
     *
     * @param categoryTitles 名称
     * @return ArrayList
     */
    List<Category> listIdsByTitles(List<String> categoryTitles);

    /**
     * 验证分类是否存在
     */
    Category validateCategory(Long id);

    /**
     * 验证分类状态
     */
    Category validateCategoryStatus(Long id);

    /**
     * 验证类目名称唯一性
     */
    void validateTitleUnique(Category en);
}
