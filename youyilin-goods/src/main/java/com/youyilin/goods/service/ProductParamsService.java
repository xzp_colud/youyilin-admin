package com.youyilin.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.goods.entity.ProductParams;

import java.util.List;

/**
 * 产品参数 服务类
 */
public interface ProductParamsService extends IService<ProductParams> {

    /**
     * 按产品查询
     *
     * @param productId 产品ID
     * @return ArrayList
     */
    List<ProductParams> listByProductId(Long productId);

    /**
     * 按产品查询
     *
     * @param productIds 产品IDS
     * @return ArrayList
     */
    List<ProductParams> listByProductIds(List<Long> productIds);

    /**
     * 保存
     *
     * @param productId 产品ID
     * @param list      参数
     */
    void saveProductParamsBatch(Long productId, List<ProductParams> list);

    /**
     * 按产品删除
     *
     * @param productId 产品ID
     */
    void delByProductId(Long productId);
}
