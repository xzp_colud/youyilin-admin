package com.youyilin.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.exception.ApiException;
import com.youyilin.goods.entity.ProductUnitPrice;
import com.youyilin.goods.mapper.ProductUnitPriceMapper;
import com.youyilin.goods.service.ProductUnitPriceService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 商品销售方式SKU路径 服务实现类
 */
@Service
public class ProductUnitPriceImpl extends ServiceImpl<ProductUnitPriceMapper, ProductUnitPrice> implements ProductUnitPriceService {

    @Override
    public List<ProductUnitPrice> listByProductId(Long productId) {
        return super.list(new LambdaQueryWrapper<ProductUnitPrice>().eq(ProductUnitPrice::getProductId, productId));
    }

    @Override
    public Map<Long, ProductUnitPrice> mapByProductId(Long productId) {
        List<ProductUnitPrice> unitPriceList = this.listByProductId(productId);
        if (CollectionUtils.isEmpty(unitPriceList)) {
            return new HashMap<>();
        }
        return unitPriceList.stream().collect(Collectors.toMap(ProductUnitPrice::getProductPriceId, productUnitPrice -> productUnitPrice));
    }

    @Override
    public void saveProductUnitPrice(long productId, List<ProductUnitPrice> saveProductUnitPriceList) {
        if (CollectionUtils.isEmpty(saveProductUnitPriceList)) {
            throw new ApiException("销售方式异常");
        }
        for (ProductUnitPrice productUnitPrice : saveProductUnitPriceList) {
            if (productUnitPrice.getProductPriceId() == null
                    || StringUtils.isBlank(productUnitPrice.getSkuPath())) {
                throw new ApiException("销售方式异常");
            }
            productUnitPrice.setProductId(productId);
        }

        super.saveBatch(saveProductUnitPriceList);
    }

    @Override
    public void delByProductId(long productId) {
        super.remove(new LambdaQueryWrapper<ProductUnitPrice>().eq(ProductUnitPrice::getProductId, productId));
    }
}
