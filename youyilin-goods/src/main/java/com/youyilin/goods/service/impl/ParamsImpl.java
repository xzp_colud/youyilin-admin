package com.youyilin.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.exception.Assert;
import com.youyilin.common.utils.ObjectUniqueUtil;
import com.youyilin.goods.entity.Params;
import com.youyilin.goods.mapper.ParamsMapper;
import com.youyilin.goods.service.ParamsService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 商品参数 实现类
 */
@Service
public class ParamsImpl extends ServiceImpl<ParamsMapper, Params> implements ParamsService {

    @Override
    public Params getByName(String name) {
        return super.getOne(new LambdaQueryWrapper<Params>().eq(Params::getName, name));
    }

    @Override
    public List<Params> listByCategoryId(Long categoryId) {
        return baseMapper.listByCategoryId(categoryId);
    }

    @Override
    public void validateNameUnique(Params en) {
        Params nameUnique = this.getByName(en.getName());
        Assert.isTrue(ObjectUniqueUtil.validateObjectUnique(en, nameUnique), "名称已存在");
    }

    @Override
    public Params validateParams(Long id) {
        Params params = super.getById(id);
        Assert.notNull(params, "属性参数不存在");

        return params;
    }
}
