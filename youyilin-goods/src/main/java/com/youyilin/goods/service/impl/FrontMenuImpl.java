package com.youyilin.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.enums.StatusEnum;
import com.youyilin.common.exception.Assert;
import com.youyilin.common.utils.ObjectUniqueUtil;
import com.youyilin.goods.entity.FrontMenu;
import com.youyilin.goods.mapper.FrontMenuMapper;
import com.youyilin.goods.service.FrontMenuService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FrontMenuImpl extends ServiceImpl<FrontMenuMapper, FrontMenu> implements FrontMenuService {

    @Override
    public List<FrontMenu> listAll() {
        return super.list(new LambdaQueryWrapper<FrontMenu>().orderByAsc(FrontMenu::getSort));
    }

    @Override
    public List<FrontMenu> listAllNormal() {
        return super.list(new LambdaQueryWrapper<FrontMenu>()
                .eq(FrontMenu::getStatus, StatusEnum.NORMAL.getCode())
                .orderByAsc(FrontMenu::getSort));
    }

    @Override
    public FrontMenu getByName(String name) {
        return super.getOne(new LambdaQueryWrapper<FrontMenu>().eq(FrontMenu::getName, name));
    }

    @Override
    public void saveFrontMenu(FrontMenu en) {
        this.validateNameUnique(en);

        if (en.getId() == null) {
            super.save(en);
        } else {
            FrontMenu oldFrontMenu = super.getById(en.getId());
            Assert.notNull(oldFrontMenu, "未查询到信息");

            super.updateById(en);
        }
    }

    @Override
    public void updateStatus(FrontMenu en) {
        FrontMenu frontMenu = super.getById(en.getId());
        if (frontMenu == null) return;

        Integer status = en.getStatus() == null || !en.getStatus().equals(StatusEnum.NORMAL.getCode()) ? StatusEnum.DISABLED.getCode() : StatusEnum.NORMAL.getCode();

        super.update(new LambdaUpdateWrapper<FrontMenu>()
                .set(FrontMenu::getStatus, status)
                .eq(FrontMenu::getId, en.getId()));
    }

    @Override
    public void validateNameUnique(FrontMenu en) {
        FrontMenu nameUnique = this.getByName(en.getName());
        Assert.isTrue(ObjectUniqueUtil.validateObjectUnique(en, nameUnique), "名称已存在");
    }
}
