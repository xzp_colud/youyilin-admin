package com.youyilin.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.goods.entity.Space;

import java.util.List;

/**
 * 空间 服务类
 */
public interface SpaceService extends IService<Space> {

    /**
     * 按名称查询
     *
     * @param name 名称
     * @return Object
     */
    Space getByName(String name);

    /**
     * 所有
     */
    List<Space> listAll();

    /**
     * 保存
     */
    void saveSpace(Space en);

    /**
     * 验证名称唯一性
     */
    void validateNameUnique(Space en);
}
