package com.youyilin.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.bean.Page;
import com.youyilin.common.enums.StatusEnum;
import com.youyilin.goods.mapper.WebsiteImageMapper;
import com.youyilin.goods.entity.WebsiteImage;
import com.youyilin.goods.service.WebsiteImageService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WebsiteImageImpl extends ServiceImpl<WebsiteImageMapper, WebsiteImage> implements WebsiteImageService {

    @Override
    public Integer getTotal(Page<WebsiteImage> page) {
        return baseMapper.getTotal(page);
    }

    @Override
    public List<WebsiteImage> getPage(Page<WebsiteImage> page) {
        return baseMapper.getPage(page);
    }

    @Override
    public List<WebsiteImage> listAll() {
        return super.list(new LambdaQueryWrapper<WebsiteImage>()
                .eq(WebsiteImage::getStatus, StatusEnum.NORMAL.getCode())
                .orderByAsc(WebsiteImage::getSort)
                .orderByDesc(WebsiteImage::getId));
    }
}
