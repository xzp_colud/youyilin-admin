package com.youyilin.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.exception.ApiException;
import com.youyilin.goods.mapper.ProductSubMaterialMapper;
import com.youyilin.goods.entity.ProductSubMaterial;
import com.youyilin.goods.service.ProductSubMaterialService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ProductSubMaterialImpl extends ServiceImpl<ProductSubMaterialMapper, ProductSubMaterial> implements ProductSubMaterialService {

    @Override
    public List<ProductSubMaterial> listBySourceProductId(Long sourceProductId) {
        return super.list(new LambdaQueryWrapper<ProductSubMaterial>().eq(ProductSubMaterial::getSourceProductId, sourceProductId));
    }

    @Override
    public Map<Long, List<ProductSubMaterial>> mapBySourceProductId(Long sourceProductId) {
        List<ProductSubMaterial> subMaterialList = this.listBySourceProductId(sourceProductId);
        if (CollectionUtils.isEmpty(subMaterialList)) {
            return new HashMap<>();
        }
        return subMaterialList.stream().collect(Collectors.groupingBy(ProductSubMaterial::getPlanId));
    }

    @Override
    public List<ProductSubMaterial> listByPlanId(Long productPlanId) {
        return super.list(new LambdaQueryWrapper<ProductSubMaterial>().eq(ProductSubMaterial::getPlanId, productPlanId));
    }

    @Override
    public List<ProductSubMaterial> listByPlanIds(List<Long> productPlanIds) {
        return super.list(new LambdaQueryWrapper<ProductSubMaterial>().in(ProductSubMaterial::getPlanId, productPlanIds));
    }

    @Override
    public void updateRedundancyProductName(Long productId, String productName) {
        super.update(new LambdaUpdateWrapper<ProductSubMaterial>()
                .set(ProductSubMaterial::getProductName, productName)
                .eq(ProductSubMaterial::getProductId, productId));
    }

    @Override
    public void delByProductId(Long productId) {
        super.remove(new LambdaQueryWrapper<ProductSubMaterial>().eq(ProductSubMaterial::getSourceProductId, productId));
    }

    @Override
    public void saveSubMaterialBatch(Long productId, List<ProductSubMaterial> batchList) {
        if (CollectionUtils.isEmpty(batchList)) {
            return;
        }

        batchList.forEach(item -> {
            if (item.getProductId() == null) {
                throw new ApiException("[辅料] 种类不能为空");
            }
            if (item.getProductPriceId() == null) {
                throw new ApiException("[辅料] SKU不能为空");
            }
//            if (StringUtils.isBlank(item.getColor())) {
//                throw new ApiException("[辅料] 颜色不能为空");
//            }
//            if (StringUtils.isBlank(item.getSize())) {
//                throw new ApiException("[辅料] 尺寸不能为空");
//            }
            if (item.getUsed() == null || item.getUsed().compareTo(BigDecimal.ZERO) < 0) {
                throw new ApiException("[辅料] 用量不能为空");
            }
            if (item.getAmount() == null || item.getAmount() < 0) {
                throw new ApiException("[辅料] 单价不能为零");
            }
            if (item.getTotalAmount() == null || item.getTotalAmount() < 0) {
                throw new ApiException("[辅料] 单价不能为零");
            }
            item.setSourceProductId(productId);
        });

        super.saveBatch(batchList);
    }
}
