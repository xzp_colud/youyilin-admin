package com.youyilin.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.goods.model.dto.UnitDto;
import com.youyilin.goods.entity.ProductUnit;

import java.util.List;

/**
 * 商品销售方式 服务类
 */
public interface ProductUnitService extends IService<ProductUnit> {

    /**
     * 某商品所有销售方式
     *
     * @param productId 商品ID
     */
    List<ProductUnit> listByProductId(Long productId);

    /**
     * 保存
     *
     * @param productId       商品ID
     * @param saveUnitDtoList 销售方式
     */
    void saveProductUnit(long productId, List<UnitDto> saveUnitDtoList);

    /**
     * 删除
     *
     * @param productId 商品ID
     */
    void delByProductId(long productId);
}
