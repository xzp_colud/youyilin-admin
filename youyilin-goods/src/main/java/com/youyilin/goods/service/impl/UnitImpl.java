package com.youyilin.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.RMsg;
import com.youyilin.common.exception.ApiException;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.common.utils.ObjectUniqueUtil;
import com.youyilin.goods.mapper.CategoryUnitMapper;
import com.youyilin.goods.mapper.UnitMapper;
import com.youyilin.goods.model.dto.UnitDto;
import com.youyilin.goods.entity.Unit;
import com.youyilin.goods.entity.UnitData;
import com.youyilin.goods.service.UnitDataService;
import com.youyilin.goods.service.UnitService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UnitImpl extends ServiceImpl<UnitMapper, Unit> implements UnitService {

    private final CategoryUnitMapper categoryUnitMapper;
    private final UnitDataService unitDataService;

    public UnitImpl(CategoryUnitMapper categoryUnitMapper, UnitDataService unitDataService) {
        this.categoryUnitMapper = categoryUnitMapper;
        this.unitDataService = unitDataService;
    }

    @Override
    public Integer getTotal(Page<Unit> page) {
        return baseMapper.getTotal(page);
    }

    @Override
    public List<Unit> getPage(Page<Unit> page) {
        return baseMapper.getPage(page);
    }

    @Override
    public Unit getByName(String name) {
        return super.getOne(new LambdaQueryWrapper<Unit>()
                .eq(Unit::getName, name));
    }

    @Override
    public List<Unit> listAll() {
        return super.list(new LambdaQueryWrapper<Unit>().orderByAsc(Unit::getSort));
    }

    @Override
    public List<UnitDto> listAllDtoByUnitIds(List<Long> unitIds) {
        List<Unit> unitList = super.listByIds(unitIds);
        if (CollectionUtils.isEmpty(unitIds)) {
            return new ArrayList<>();
        }
        List<UnitDto> unitDtoList = BeanHelper.map(unitList, UnitDto.class);
        Map<Long, List<UnitData>> unitDataMap = unitDataService.mapByUnitIds(unitIds);
        unitDtoList.forEach(item -> item.setItemList(unitDataMap.get(item.getId())));
        return unitDtoList;
    }

    @Override
    public List<UnitDto> listAllDtoByProductId(List<Long> unitIds, Long productId) {
        List<UnitDto> unitDtoList = this.listAllDtoByUnitIds(unitIds);
        if (CollectionUtils.isEmpty(unitDtoList)) {
            return new ArrayList<>();
        }
        if (productId == null) {
            return unitDtoList;
        }
        Map<Long, List<UnitData>> unitDataMapForProduct = unitDataService.mapByProductId(productId);
        unitDtoList.forEach(item -> {
            List<UnitData> itemList = item.getItemList();
            if (CollectionUtils.isEmpty(itemList)) {
                itemList = new ArrayList<>();
            }
            List<UnitData> unitDataListForProduct = unitDataMapForProduct.get(item.getId());
            if (CollectionUtils.isNotEmpty(unitDataListForProduct)) {
                itemList.addAll(unitDataListForProduct);
                item.setItemList(itemList);
            }
        });
        return unitDtoList;
    }

    @Override
    public List<UnitDto> listForDetail(Long productId) {
        if (productId == null) {
            return new ArrayList<>();
        }
        List<UnitData> productUnitDataList = unitDataService.listForDetail(productId);
        if (CollectionUtils.isEmpty(productUnitDataList)) {
            return new ArrayList<>();
        }
        return generateByUnitData(productUnitDataList);
    }

    @Override
    public List<UnitDto> listForEdit(Long productId) {
        if (productId == null) {
            return new ArrayList<>();
        }
        List<UnitData> productUnitDataList = unitDataService.listForEdit(productId);
        if (CollectionUtils.isEmpty(productUnitDataList)) {
            return new ArrayList<>();
        }
        return generateByUnitData(productUnitDataList);
    }

    private List<UnitDto> generateByUnitData(List<UnitData> unitDataList) {
        Set<Long> unitIdsSet = new HashSet<>();
        Set<Long> unitDataIdsSet = new HashSet<>();
        for (UnitData unitData : unitDataList) {
            unitIdsSet.add(unitData.getUnitId());
            unitDataIdsSet.add(unitData.getId());
        }
        List<Unit> unitList = super.listByIds(unitIdsSet);
        List<UnitDto> unitDtoList = BeanHelper.map(unitList, UnitDto.class);

        List<Long> unitDataIdsList = new ArrayList<>(unitDataIdsSet);
        Map<Long, List<UnitData>> unitDataMap = unitDataService.mapByIds(unitDataIdsList);
        unitDtoList.forEach(item -> item.setItemList(unitDataMap.get(item.getId())));
        return unitDtoList;
    }

    @Override
    public List<UnitDto> listForAdd(Long categoryId) {
        List<Long> unitIds = categoryUnitMapper.listUnitIdsByCategoryId(categoryId);
        if (CollectionUtils.isEmpty(unitIds)) {
            return new ArrayList<>();
        }
        return this.listAllDtoByUnitIds(unitIds);
    }

    @Override
    public void saveUnit(Unit en) {
        this.validateNameUnique(en);

        if (en.getId() == null) {
            super.save(en);
        } else {
            Unit oldUnit = super.getById(en.getId());
            if (oldUnit == null) {
                throw new ApiException(RMsg.NO_PARAMS.getMsg());
            }
            super.updateById(en);
        }
    }

    @Override
    public void validateNameUnique(Unit en) {
        Unit nameUnique = this.getByName(en.getName());
        if (!ObjectUniqueUtil.validateObjectUnique(en, nameUnique)) {
            throw new ApiException("销售单位已存在");
        }
    }
}
