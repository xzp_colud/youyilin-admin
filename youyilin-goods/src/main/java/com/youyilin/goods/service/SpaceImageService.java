package com.youyilin.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.common.bean.Page;
import com.youyilin.goods.entity.SpaceImage;

import java.util.List;

/**
 * 空间图片 服务类
 */
public interface SpaceImageService extends IService<SpaceImage> {

    Integer getTotal(Page<SpaceImage> page);

    List<SpaceImage> getPage(Page<SpaceImage> page);

    /**
     * 保存
     */
    void saveSpaceImage(SpaceImage en);

    /**
     * 删除
     */
    void delSpaceImage(Long id);
}
