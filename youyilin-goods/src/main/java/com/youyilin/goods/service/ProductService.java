package com.youyilin.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.goods.entity.Product;

import java.util.List;
import java.util.Map;

/**
 * 商品 服务类
 */
public interface ProductService extends IService<Product> {

    /**
     * 根据商品编码查询
     *
     * @param serialNum 商品编码
     * @return Object
     */
    Product getBySn(String serialNum);

    /**
     * 获取所有类目N个商品
     *
     * @return HashMap
     */
    Map<Long, List<Product>> mapByCategory();

    /**
     * 按名称查询ID
     *
     * @param names 名称
     * @return ArrayList
     */
    List<Product> listIdsByNames(List<String> names);

    /**
     * 查询并分组
     *
     * @param ids IDS
     * @return Map
     */
    Map<Long, Product> mapByIds(List<Long> ids);

    /**
     * 保存
     */
    void saveProduct(Product en);

    /**
     * 冗余供货商名称
     *
     * @param supplierId   供货商ID
     * @param supplierName 供货商名称
     */
    void updateRedundancySupplierName(Long supplierId, String supplierName);

    /**
     * 冗余类目名称及类目编码
     *
     * @param categoryId   类目ID
     * @param categoryName 类目名称
     */
    void updateRedundancyCategoryName(Long categoryId, String categoryName);

    /**
     * 更新商品外侧最低销售价
     *
     * @param id    商品
     * @param price 外侧最低价
     */
    void updateMinPrice(Long id, Integer price);

    /**
     * 修改产品的状态
     *
     * @param id     产品ID
     * @param status 修改之后的状态
     */
    void updateStatus(Long id, Integer status);

    /**
     * 验证商品名称唯一性
     */
    void validateSnUnique(Product en);

    /**
     * 验证商品
     */
    Product validateProduct(Long id);

    /**
     * 验证商品是否售卖
     */
    Product validateProductSell(Long id);

    /**
     * 验证商品是否售卖
     *
     * @param ids IDS
     */
    void validateProductSell(List<Long> ids);
}
