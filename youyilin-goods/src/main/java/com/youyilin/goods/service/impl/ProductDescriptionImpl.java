package com.youyilin.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.goods.mapper.ProductDescriptionMapper;
import com.youyilin.goods.entity.ProductDescription;
import com.youyilin.goods.service.ProductDescriptionService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Service
public class ProductDescriptionImpl extends ServiceImpl<ProductDescriptionMapper, ProductDescription> implements ProductDescriptionService {

    @Override
    public String getByProductId(Long productId) {
        ProductDescription productDescription = super.getOne(new LambdaQueryWrapper<ProductDescription>().eq(ProductDescription::getProductId, productId));
        if (productDescription == null) return null;

        return productDescription.getDescription();
    }

    @Override
    public void saveDescription(Long productId, String description) {
        if (StringUtils.isBlank(description)) return;

        ProductDescription save = new ProductDescription();
        save.setProductId(productId)
                .setDescription(description);

        super.save(save);
    }

    @Override
    public void delByProductId(Long productId) {
        super.remove(new LambdaQueryWrapper<ProductDescription>().eq(ProductDescription::getProductId, productId));
    }
}
