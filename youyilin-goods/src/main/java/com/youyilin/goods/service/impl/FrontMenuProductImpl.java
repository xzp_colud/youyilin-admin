package com.youyilin.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.bean.Page;
import com.youyilin.goods.mapper.FrontMenuProductMapper;
import com.youyilin.goods.model.dto.FrontMenuProductDto;
import com.youyilin.goods.entity.FrontMenuProduct;
import com.youyilin.goods.model.request.RequestFrontMenuProduct;
import com.youyilin.goods.service.FrontMenuProductService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class FrontMenuProductImpl extends ServiceImpl<FrontMenuProductMapper, FrontMenuProduct> implements FrontMenuProductService {

    @Override
    public List<FrontMenuProductDto> getPage(Page<FrontMenuProductDto> page) {
        return baseMapper.getPage(page);
    }

    @Override
    public List<FrontMenuProductDto> listPageByFrontMenuId(Long frontMenuId) {
        return baseMapper.listPageByFrontMenuId(frontMenuId);
    }

    @Override
    @Transactional
    public void saveFrontMenuProduct(RequestFrontMenuProduct request) {
        Long frontMenuId = request.getFrontMenuId();
        List<FrontMenuProduct> saveBatchList = new ArrayList<>();
        for (FrontMenuProduct fmp : request.getProductList()) {
            FrontMenuProduct save = new FrontMenuProduct();
            save.setFrontMenuId(frontMenuId);
            save.setProductId(fmp.getProductId());
            save.setSort(fmp.getSort());
            saveBatchList.add(save);
        }

        this.delByFrontMenuId(frontMenuId);
        super.saveBatch(saveBatchList);
    }

    @Override
    public void delByFrontMenuId(Long frontMenuId) {
        super.remove(new LambdaQueryWrapper<FrontMenuProduct>().eq(FrontMenuProduct::getFrontMenuId, frontMenuId));
    }
}
