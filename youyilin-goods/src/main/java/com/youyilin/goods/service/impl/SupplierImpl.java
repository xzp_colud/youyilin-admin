package com.youyilin.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyilin.common.enums.BooleanEnum;
import com.youyilin.common.exception.Assert;
import com.youyilin.common.utils.ObjectUniqueUtil;
import com.youyilin.goods.entity.Supplier;
import com.youyilin.goods.mapper.SupplierMapper;
import com.youyilin.goods.service.SupplierService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 供货商 服务实现类
 */
@Slf4j
@Service
public class SupplierImpl extends ServiceImpl<SupplierMapper, Supplier> implements SupplierService {

    @Override
    public Supplier getByName(String name) {
        return super.getOne(new LambdaQueryWrapper<Supplier>()
                .eq(Supplier::getName, name));
    }

    @Override
    public List<Supplier> listAll(Integer status) {
        LambdaQueryWrapper<Supplier> query = new LambdaQueryWrapper<>();
        if (status != null) {
            query.eq(Supplier::getStatus, status);
        }
        return super.list(query);
    }

    @Override
    public List<Supplier> listIdsByNames(List<String> supplierNames) {
        return super.list(new LambdaQueryWrapper<Supplier>()
                .select(Supplier::getId, Supplier::getName)
                .in(Supplier::getName, supplierNames));
    }

    @Override
    public void validateNameUnique(Supplier en) {
        Supplier nameUnique = this.getByName(en.getName());
        Assert.isTrue(ObjectUniqueUtil.validateObjectUnique(en, nameUnique), "供应商名称已存在");
    }

    @Override
    public Supplier validateSupplier(Long id) {
        Supplier supplier = super.getById(id);
        Assert.notNull(supplier, "供应商不存在");

        return supplier;
    }

    @Override
    public Supplier validateSupplierStatus(Long id) {
        Supplier supplier = this.validateSupplier(id);
        boolean statusFlag = supplier.getStatus() != null && supplier.getStatus().equals(BooleanEnum.TRUE.getCode());
        Assert.isTrue(statusFlag, "供应商 " + supplier.getName() + " 已禁用");

        return supplier;
    }
}
