package com.youyilin.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.goods.entity.ProductTechnology;

import java.util.List;
import java.util.Map;

public interface ProductTechnologyService extends IService<ProductTechnology> {

    /**
     * 按产品ID查询
     *
     * @param sourceProductId 源产品ID
     * @return ArrayList
     */
    List<ProductTechnology> listBySourceProductId(Long sourceProductId);

    /**
     * 按产品ID查询
     *
     * @param sourceProductId 源产品ID
     * @return Map
     */
    Map<Long, List<ProductTechnology>> mapBySourceProductId(Long sourceProductId);

    /**
     * 按产品ID删除
     *
     * @param productId 产品ID
     */
    void delByProductId(Long productId);

    /**
     * 批量保存
     */
    void saveTechnologyBatch(Long productId, List<ProductTechnology> batchList);
}
