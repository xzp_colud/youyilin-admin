package com.youyilin.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youyilin.goods.entity.ProductProcessFee;

import java.util.List;
import java.util.Map;

/**
 * 产品-加工费 服务类
 */
public interface ProductProcessFeeService extends IService<ProductProcessFee> {

    /**
     * 按产品ID查询
     *
     * @param sourceProductId 源产品ID
     * @return ArrayList
     */
    List<ProductProcessFee> listBySourceProductId(Long sourceProductId);

    /**
     * 按产品ID查询
     *
     * @param sourceProductId 源产品ID
     * @return Map
     */
    Map<Long, List<ProductProcessFee>> mapBySourceProductId(Long sourceProductId);

    /**
     * 按产品ID删除
     *
     * @param productId 产品ID
     */
    void delByProductId(Long productId);

    /**
     * 批量保存
     */
    void saveProcessFeeBatch(Long productId, List<ProductProcessFee> batchList);
}
