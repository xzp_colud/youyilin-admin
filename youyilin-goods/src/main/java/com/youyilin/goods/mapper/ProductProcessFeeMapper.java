package com.youyilin.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.goods.entity.ProductProcessFee;
import org.apache.ibatis.annotations.Mapper;

/**
 * 产品-加工费 Mapper
 */
@Mapper
public interface ProductProcessFeeMapper extends BaseMapper<ProductProcessFee> {
}
