package com.youyilin.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.common.bean.Page;
import com.youyilin.goods.entity.UnitData;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 销售单位键值 Mapper
 */
@Mapper
public interface UnitDataMapper extends BaseMapper<UnitData> {

    Integer getTotal(Page<UnitData> page);

    List<UnitData> getPage(Page<UnitData> page);

    List<UnitData> listForEdit(Long productId);

    List<UnitData> listForDetail(Long productId);
}
