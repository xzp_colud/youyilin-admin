package com.youyilin.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.goods.entity.ProductPlan;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ProductPlanMapper extends BaseMapper<ProductPlan> {
}
