package com.youyilin.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.goods.entity.ProductFabric;
import org.apache.ibatis.annotations.Mapper;

/**
 * 产品面料
 */
@Mapper
public interface ProductFabricMapper extends BaseMapper<ProductFabric> {

    void asyncPrice();
}
