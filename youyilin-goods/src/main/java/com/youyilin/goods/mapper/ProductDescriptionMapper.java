package com.youyilin.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.goods.entity.ProductDescription;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ProductDescriptionMapper extends BaseMapper<ProductDescription> {
}
