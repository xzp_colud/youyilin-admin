package com.youyilin.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.goods.entity.ProductPrice;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品SKU Mapper
 */
@Mapper
public interface ProductPriceMapper extends BaseMapper<ProductPrice> {
    Integer minSellPriceByProductId(Long productId);
}
