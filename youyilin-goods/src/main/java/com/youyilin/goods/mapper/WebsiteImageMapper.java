package com.youyilin.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.common.bean.Page;
import com.youyilin.goods.entity.WebsiteImage;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface WebsiteImageMapper extends BaseMapper<WebsiteImage> {

    Integer getTotal(Page<WebsiteImage> page);

    List<WebsiteImage> getPage(Page<WebsiteImage> page);
}
