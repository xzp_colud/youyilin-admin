package com.youyilin.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.goods.entity.ProductUnitPrice;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品销售方式SKU路径 Mapper
 */
@Mapper
public interface ProductUnitPriceMapper extends BaseMapper<ProductUnitPrice> {
}
