package com.youyilin.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.common.bean.Page;
import com.youyilin.goods.dto.params.ParamsPageQueryDTO;
import com.youyilin.goods.entity.Params;
import com.youyilin.goods.vo.params.ParamsPageVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 商品参数 Mapper
 */
@Mapper
public interface ParamsMapper extends BaseMapper<Params> {

    Integer getTotal(Page<ParamsPageQueryDTO> page);

    List<ParamsPageVO> getPage(Page<ParamsPageQueryDTO> page);

    List<Params> listByCategoryId(Long categoryId);
}
