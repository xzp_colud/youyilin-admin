package com.youyilin.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.goods.entity.ProductSubMaterial;
import org.apache.ibatis.annotations.Mapper;

/**
 * 产品辅料
 */
@Mapper
public interface ProductSubMaterialMapper extends BaseMapper<ProductSubMaterial> {

    void asyncPrice();
}
