package com.youyilin.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.common.bean.Page;
import com.youyilin.goods.entity.Unit;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 销售单位 Mapper
 */
@Mapper
public interface UnitMapper extends BaseMapper<Unit> {

    Integer getTotal(Page<Unit> page);

    List<Unit> getPage(Page<Unit> page);

    List<Unit> listByCategoryId(Long categoryId);
}
