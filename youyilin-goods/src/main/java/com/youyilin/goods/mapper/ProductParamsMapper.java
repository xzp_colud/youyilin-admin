package com.youyilin.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.goods.entity.ProductParams;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 产品参数
 */
@Mapper
public interface ProductParamsMapper extends BaseMapper<ProductParams> {

    List<ProductParams> listByProductId(Long productId);

    List<ProductParams> listByProductIds(List<Long> productIds);
}
