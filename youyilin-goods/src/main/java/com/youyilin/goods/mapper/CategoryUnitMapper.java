package com.youyilin.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.goods.entity.CategoryUnit;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 类目销售方式 Mapper
 */
@Mapper
public interface CategoryUnitMapper extends BaseMapper<CategoryUnit> {
    List<Long> listUnitIdsByCategoryId(Long categoryId);

    List<String> listUnitNameByCategoryId(Long unitId);
}
