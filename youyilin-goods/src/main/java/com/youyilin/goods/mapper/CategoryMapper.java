package com.youyilin.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.common.bean.Page;
import com.youyilin.goods.dto.category.CategoryPageQueryDTO;
import com.youyilin.goods.entity.Category;
import com.youyilin.goods.vo.category.CategoryPageVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 类目 Mapper
 */
@Mapper
public interface CategoryMapper extends BaseMapper<Category> {

    Integer getTotal(Page<CategoryPageQueryDTO> page);

    List<CategoryPageVO> getPage(Page<CategoryPageQueryDTO> page);
}
