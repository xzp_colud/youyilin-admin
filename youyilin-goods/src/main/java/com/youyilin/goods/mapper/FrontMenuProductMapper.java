package com.youyilin.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.common.bean.Page;
import com.youyilin.goods.model.dto.FrontMenuProductDto;
import com.youyilin.goods.entity.FrontMenuProduct;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface FrontMenuProductMapper extends BaseMapper<FrontMenuProduct> {

    List<FrontMenuProductDto> getPage(Page<FrontMenuProductDto> page);

    List<FrontMenuProductDto> listPageByFrontMenuId(Long frontMenuId);;
}
