package com.youyilin.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.goods.entity.Space;
import org.apache.ibatis.annotations.Mapper;

/**
 * 空间 Mapper
 */
@Mapper
public interface SpaceMapper extends BaseMapper<Space> {

}
