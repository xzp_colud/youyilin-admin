package com.youyilin.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.goods.entity.ProductIngredient;
import org.apache.ibatis.annotations.Mapper;

/**
 * 产品配料
 */
@Mapper
public interface ProductIngredientMapper extends BaseMapper<ProductIngredient> {

    void asyncPrice();
}
