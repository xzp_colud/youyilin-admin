package com.youyilin.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.goods.entity.FrontMenu;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface FrontMenuMapper extends BaseMapper<FrontMenu> {

}
