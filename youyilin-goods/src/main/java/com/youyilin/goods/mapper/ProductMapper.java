package com.youyilin.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.common.bean.Page;
import com.youyilin.goods.dto.product.ProductPageQueryDTO;
import com.youyilin.goods.entity.Product;
import com.youyilin.goods.vo.product.ProductPageVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 商品 Mapper
 */
@Mapper
public interface ProductMapper extends BaseMapper<Product> {

    Integer getTotal(Page<ProductPageQueryDTO> page);

    List<ProductPageVO> getPage(Page<ProductPageQueryDTO> page);

    List<Product> listByCategory();
}
