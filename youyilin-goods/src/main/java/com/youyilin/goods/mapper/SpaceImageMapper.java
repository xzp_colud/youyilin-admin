package com.youyilin.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.common.bean.Page;
import com.youyilin.goods.entity.SpaceImage;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 空间图片 Mapper
 */
@Mapper
public interface SpaceImageMapper extends BaseMapper<SpaceImage> {

    Integer getTotal(Page<SpaceImage> page);

    List<SpaceImage> getPage(Page<SpaceImage> page);
}
