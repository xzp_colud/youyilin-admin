package com.youyilin.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.goods.entity.ProductUnit;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品销售方式 Mapper
 */
@Mapper
public interface ProductUnitMapper extends BaseMapper<ProductUnit> {
}
