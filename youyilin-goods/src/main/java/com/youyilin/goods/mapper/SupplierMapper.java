package com.youyilin.goods.mapper;

import com.youyilin.goods.dto.supplier.SupplierPageQueryDTO;
import com.youyilin.goods.entity.Supplier;
import com.youyilin.common.bean.Page;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.goods.vo.supplier.SupplierPageVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 供货商 Mapper
 */
@Mapper
public interface SupplierMapper extends BaseMapper<Supplier> {

    Integer getTotal(Page<SupplierPageQueryDTO> page);

    List<SupplierPageVO> getPage(Page<SupplierPageQueryDTO> page);
}
