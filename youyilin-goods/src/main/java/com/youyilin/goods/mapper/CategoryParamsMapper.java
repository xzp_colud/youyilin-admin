package com.youyilin.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.goods.entity.CategoryParams;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 商品分类 - 商品参数
 */
@Mapper
public interface CategoryParamsMapper extends BaseMapper<CategoryParams> {
    List<String> listParamsNameByCategoryId(Long categoryId);
}
