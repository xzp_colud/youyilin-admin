package com.youyilin.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyilin.goods.entity.ProductTechnology;
import org.apache.ibatis.annotations.Mapper;

/**
 * 产品工艺
 */
@Mapper
public interface ProductTechnologyMapper extends BaseMapper<ProductTechnology> {
}
