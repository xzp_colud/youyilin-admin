package com.youyilin.goods.model.vo;

import com.youyilin.goods.entity.*;
import lombok.Data;

import java.util.List;

/**
 * 产品详情 视图
 */
@Data
public class ProductDetailVo {

    private Product product;
    private List<ProductParams> paramsList;
    private List<ProductPrice> priceList;
    private List<ProductUnit> unitList;
    private List<ProductUnitPrice> unitPriceList;

    private List<ProductPlanVo> planList;
    private String productDescription;
}
