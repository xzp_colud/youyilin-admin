package com.youyilin.goods.model.request;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * 提交 商品SKU
 */
@Data
public class RequestOrderProductPrice {

    /**
     * SKU ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "商品SKU不能为空")
    private Long id;
    /**
     * 数量
     */
    @NotNull(message = "数量不能为空")
    private BigDecimal num;
    /**
     * 销售单价
     */
    private Long sellPrice;
    /**
     * 折扣
     */
    private Integer discount;
    /**
     * 行备注
     */
    private String remark;
    /**
     * 实际销售单价
     */
    private Long sellAmount;
    /**
     * 优惠总额
     */
    private Long totalDiscount;
    /**
     * 销售总价
     */
    private Long totalSell;
    /**
     * 进货单价
     */
    private Long costAmount;
    /**
     * 进货总价
     */
    private Long totalCost;

    private String skuName;

    public static Integer numToInt(BigDecimal num) {
        if (num == null) {
            num = BigDecimal.ZERO;
        }
        return num.multiply(new BigDecimal("100")).intValue();
    }
}
