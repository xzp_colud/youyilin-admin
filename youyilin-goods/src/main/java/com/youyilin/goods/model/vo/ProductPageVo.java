package com.youyilin.goods.model.vo;

import com.youyilin.goods.entity.Product;
import com.youyilin.goods.entity.ProductParams;
import com.youyilin.goods.entity.ProductPrice;
import lombok.Data;

import java.util.List;

@Data
public class ProductPageVo extends Product {
    private static final long serialVersionUID = 5098921320387028243L;

    private List<ProductPrice> priceList;
    private List<ProductParams> paramsList;
}
