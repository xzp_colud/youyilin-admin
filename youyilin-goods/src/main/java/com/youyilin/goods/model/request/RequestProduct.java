package com.youyilin.goods.model.request;

import com.youyilin.goods.entity.Product;
import com.youyilin.goods.entity.ProductParams;
import com.youyilin.goods.entity.ProductPrice;
import com.youyilin.goods.model.dto.UnitDto;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 商品 保存Request
 */
@Data
public class RequestProduct extends Product {
    private static final long serialVersionUID = -7357796097846536245L;

    /**
     * 由页面传入UUID随机数
     */
    @NotNull(message = "参数异常")
    private Long randomId;
    /**
     * 参数
     */
    private List<ProductParams> paramsList;
    /**
     * 销售规格
     */
    private List<UnitDto> unitList;
    /**
     * 需要删除的销售键值
     */
    private List<Long> delUnitDataList;
    /**
     * SKU
     */
    private List<ProductPrice> priceList;

    /**
     * 方案 样衣、成衣 才需要
     */
    private List<RequestProductPlan> planList;

    /**
     * 产品详情
     */
    private String productDescription;
}
