package com.youyilin.goods.model.request;

import com.youyilin.goods.entity.*;
import lombok.Data;

import java.util.List;

@Data
public class RequestProductPlan {

    private Long id;
    private String title;

    private List<ProductFabric> fabricList;
    private List<ProductIngredient> ingredientList;
    private List<ProductSubMaterial> subMaterialList;
    private List<ProductTechnology> technologyList;
    private List<ProductProcessFee> processFeeList;

    private Long planId;
}
