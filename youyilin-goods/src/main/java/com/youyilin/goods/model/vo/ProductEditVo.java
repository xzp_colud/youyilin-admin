package com.youyilin.goods.model.vo;

import com.youyilin.goods.entity.*;
import lombok.Data;

import java.util.List;

/**
 * 产品编辑
 */
@Data
public class ProductEditVo {

    private Product product;
    private List<ProductParams> paramsList;
    private List<ProductPrice> priceList;
    private List<ProductUnit> unitList;
    private List<ProductUnitPrice> unitPriceList;
    private List<ProductPlanVo> planList;

    private String productDescription;
}
