package com.youyilin.goods.model.request;

import com.youyilin.goods.entity.FrontMenuProduct;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class RequestFrontMenuProduct {

    @NotNull(message = "分类不能为空")
    private Long frontMenuId;
    private List<FrontMenuProduct> productList;
}
