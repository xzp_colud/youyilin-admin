package com.youyilin.goods.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.youyilin.goods.entity.*;
import lombok.Data;

import java.util.List;

@Data
public class ProductPlanVo {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    private String title;

    private List<ProductFabric> fabricList;
    private List<ProductIngredient> ingredientList;
    private List<ProductSubMaterial> subMaterialList;
    private List<ProductTechnology> technologyList;
    private List<ProductProcessFee> processFeeList;
}
