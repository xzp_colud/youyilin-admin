package com.youyilin.goods.model.request;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class RequestMiniProduct {

    // 购物车ID
    private Long id;
    private Long productId;
    private Long skuId;

    private BigDecimal num;
    private Long categoryId;
    private String categoryName;
    private Long supplierId;
    private String supplierName;
    private String productName;
    private String image;
    private String skuName;
    private Long sellAmount;
    private Long costAmount;
    private boolean isSell;
}
