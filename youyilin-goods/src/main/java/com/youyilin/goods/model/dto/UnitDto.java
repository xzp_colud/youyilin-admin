package com.youyilin.goods.model.dto;

import com.youyilin.goods.entity.Unit;
import com.youyilin.goods.entity.UnitData;
import lombok.Data;

import java.util.List;

@Data
public class UnitDto extends Unit {
    private static final long serialVersionUID = -4322454158692731570L;

    private List<UnitData> itemList;
}
