package com.youyilin.goods.model.dto;

import com.youyilin.goods.entity.FrontMenuProduct;
import lombok.Data;

@Data
public class FrontMenuProductDto extends FrontMenuProduct {
    private static final long serialVersionUID = -3565357412794312741L;

    private String image;
    private String productName;
    private String sn;
    private Integer status;
    private String priceText;

    private Integer delFlag;
}
