package com.youyilin.goods.model.request;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 提交商品表单
 */
@Data
public class RequestOrderProduct {

    /**
     * 保存ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /**
     * 商品ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "商品不能为空")
    private Long productId;
    /**
     * 图片
     */
    private String imageUrl;
    /**
     * SKU
     */
    @NotNull(message = "商品SKU不能为空")
    private RequestOrderProductPrice price;

    /**
     * 是否验证销售价与存储一致性
     */
    private boolean validateSellPriceFit;

    private String productName;
    private String sn;
    private Long brandId;
    private String brandName;
    private Long supplierId;
    private String supplierName;
    private Long categoryId;
    private String categoryName;
}
