package com.youyilin.goods.manager.impl;

import com.youyilin.common.exception.Assert;
import com.youyilin.goods.dto.OutboundValidateProductDTO;
import com.youyilin.goods.dto.PurchaseValidateProductDTO;
import com.youyilin.goods.dto.SalesValidateProductDTO;
import com.youyilin.goods.entity.Product;
import com.youyilin.goods.entity.ProductPrice;
import com.youyilin.goods.manager.ProductManager;
import com.youyilin.goods.service.ProductPriceService;
import com.youyilin.goods.service.ProductService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ProductManagerImpl implements ProductManager {

    private final ProductService productService;
    private final ProductPriceService productPriceService;

    public ProductManagerImpl(ProductService productService, ProductPriceService productPriceService) {
        this.productService = productService;
        this.productPriceService = productPriceService;
    }

    /**
     * 采购单验证商品
     */
    @Override
    public void validatePurchaseProductBatch(List<PurchaseValidateProductDTO> productList) {
        // 第一次验证商品
        Set<Long> productIds = new HashSet<>();
        Set<Long> skuIds = new HashSet<>();
        for (PurchaseValidateProductDTO dto : productList) {
            productIds.add(this.validateProduct(dto.getProductId()));
            skuIds.add(this.validateSku(dto.getSkuId(), dto.getBuyNum()));
        }

        // 第二次验证商品
        Map<Long, Product> productMap = this.mapProductByIds(productIds);
        Map<Long, ProductPrice> skuMap = this.mapSkuByIds(skuIds);
        for (PurchaseValidateProductDTO dto : productList) {
            Product product = this.validateSecondProduct(dto.getProductId(), productMap);
            ProductPrice sku = this.validateSecondSku(dto.getProductId(), product.getName(), dto.getSkuId(), skuMap);

            dto.setProductName(product.getName())
                    .setSerialNum(product.getSn())
                    .setCategoryId(product.getCategoryId())
                    .setCategoryName(product.getCategoryName())
                    .setSupplierId(product.getSupplierId())
                    .setSupplierName(product.getSupplierName());

            int num = dto.getBuyNum().multiply(new BigDecimal("100")).intValue();
            boolean isWholesalePriceFlag = dto.getWholesalePriceFlag() != null && dto.getWholesalePriceFlag() == 1;
            // 暂无折扣
            Integer discount = 100;
            Long price = isWholesalePriceFlag ? sku.getWholesalePrice() : sku.getCostPrice();
            long amount = price * discount / 100;

            Long totalPrice = num * price / 100;
            Long totalAmount = num * amount / 100;
            Long totalDiscount = totalPrice - totalAmount;

            dto.setSkuName(sku.getSkuName())
                    .setPrice(price)
                    .setAmount(amount)
                    .setTotalAmount(totalAmount)
                    .setTotalDiscount(totalDiscount)
                    .setDiscount(discount)
                    .setImageUrl(product.getImageUrl());
        }
    }

    /**
     * 销售单验证商品
     *
     * @param productList  商品列表
     * @param useWholesale 批发标识
     */
    @Override
    public void validateSalesProductBatch(List<SalesValidateProductDTO> productList, boolean useWholesale) {
        // 第一次验证商品
        Set<Long> productIds = new HashSet<>();
        Set<Long> skuIds = new HashSet<>();
        for (SalesValidateProductDTO dto : productList) {
            productIds.add(this.validateProduct(dto.getProductId()));
            skuIds.add(this.validateSku(dto.getSkuId(), dto.getNum()));
        }

        // 第二次验证商品
        Map<Long, Product> productMap = this.mapProductByIds(productIds);
        Map<Long, ProductPrice> skuMap = this.mapSkuByIds(skuIds);
        for (SalesValidateProductDTO dto : productList) {
            dto.setDiscount(dto.getDiscount() == null || dto.getDiscount() < 0 || dto.getDiscount() > 100 ? 100 : dto.getDiscount());
            Product product = this.validateSecondProduct(dto.getProductId(), productMap);
            ProductPrice sku = this.validateSecondSku(dto.getProductId(), product.getName(), dto.getSkuId(), skuMap);

            int num = dto.getNum().multiply(new BigDecimal("100")).intValue();
            // 折扣
            Integer discount = dto.getDiscount();
            // 销售单价
            Long sellPrice = sku.getSellPrice();
            // 数量满足标记
            // 金额标记
            boolean wholesalePriceFlag = sku.getWholesalePrice() != null && sku.getWholesalePrice() > 0L;
            if (useWholesale && wholesalePriceFlag) {
                sellPrice = sku.getWholesalePrice();
            }
            long reallySellPrice = sellPrice * discount / 100;

            Long totalPrice = num * sellPrice / 100;
            Long totalSell = num * reallySellPrice / 100;
            Long totalDiscount = totalPrice - totalSell;
            // 成本
            Long costPrice = sku.getCostPrice();
            Long totalCost = costPrice * num / 100;

            dto.setSkuName(sku.getSkuName())
                    .setProductPriceId(dto.getSkuId())
                    .setSellPrice(sellPrice)
                    .setSellAmount(reallySellPrice)
                    .setTotalSell(totalSell)
                    .setTotalDiscount(totalDiscount)
                    .setDiscount(discount)
                    .setCostAmount(costPrice)
                    .setTotalCost(totalCost)
                    .setProductName(product.getName())
                    .setSerialNum(product.getSn())
                    .setCategoryId(product.getCategoryId())
                    .setCategoryName(product.getCategoryName())
                    .setSupplierId(product.getSupplierId())
                    .setSupplierName(product.getSupplierName())
                    .setImageUrl(product.getImageUrl());
        }
    }

    /**
     * 验证出库单商品
     */
    @Override
    public void validateOutboundProductBatch(List<OutboundValidateProductDTO> productList) {
        Set<Long> productIds = new HashSet<>();
        Set<Long> skuIds = new HashSet<>();
        for (OutboundValidateProductDTO dto : productList) {
            productIds.add(this.validateProduct(dto.getProductId()));
            skuIds.add(this.validateSku(dto.getSkuId(), dto.getQty()));
        }

        Map<Long, Product> productMap = this.mapProductByIds(productIds);
        Map<Long, ProductPrice> skuMap = this.mapSkuByIds(skuIds);
        for (OutboundValidateProductDTO dto : productList) {
            Product product = this.validateSecondProduct(dto.getProductId(), productMap);
            ProductPrice sku = this.validateSecondSku(dto.getProductId(), product.getName(), dto.getSkuId(), skuMap);

            dto.setCategoryId(product.getCategoryId())
                    .setCategoryName(product.getCategoryName())
                    .setProductName(product.getName())
                    .setSkuName(sku.getSkuName())
                    .setSupplierId(product.getSupplierId())
                    .setSupplierName(product.getSupplierName())
                    .setImageUrl(product.getImageUrl());
        }
    }

    private Long validateProduct(Long productId) {
        Assert.notNull(productId, "商品不能为空");

        return productId;
    }

    private Long validateSku(Long skuId, BigDecimal num) {
        Assert.notNull(skuId, "商品SKU不能为空");
        Assert.notNull(num, "数量不能为空");
        Assert.isTrue(num.compareTo(BigDecimal.ZERO) > 0, "数量不能为空");

        return skuId;
    }

    private Map<Long, Product> mapProductByIds(Set<Long> productIds) {
        List<Product> productList = productService.listByIds(productIds);

        Assert.notEmpty(productList, "商品不能为空");

        return productList.stream().collect(Collectors.toMap(Product::getId, item -> item));
    }

    private Map<Long, ProductPrice> mapSkuByIds(Set<Long> skuIds) {
        List<ProductPrice> productPriceList = productPriceService.listByIds(skuIds);

        Assert.notEmpty(productPriceList, "商品SKU不能为空");

        return productPriceList.stream().collect(Collectors.toMap(ProductPrice::getId, item -> item));
    }

    private Product validateSecondProduct(Long productId, Map<Long, Product> productMap) {
        // 商品信息
        Product product = productMap.get(productId);

        Assert.notNull(product, "商品信息异常");
        Assert.isFalse(product.isDisabled(), "商品[" + product.getName() + "]已禁用");

        return product;
    }

    private ProductPrice validateSecondSku(Long productId, String productName, Long skuId, Map<Long, ProductPrice> skuMap) {
        ProductPrice sku = skuMap.get(skuId);

        Assert.notNull(sku, "商品SKU信息异常");
        Assert.isTrue(sku.getProductId().equals(productId), "商品信息不一致");
        Assert.isFalse(sku.isDisabled(), "商品SKU[" + sku.getSkuName() + "]已禁用");

        return sku;
    }
}
