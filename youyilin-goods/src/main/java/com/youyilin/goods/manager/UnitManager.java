package com.youyilin.goods.manager;

import com.youyilin.goods.dto.unit.UnitDTO;
import com.youyilin.goods.dto.unit.UnitDataDTO;
import com.youyilin.goods.entity.ProductUnit;
import com.youyilin.goods.entity.Unit;
import com.youyilin.goods.entity.UnitData;
import com.youyilin.goods.mapper.UnitMapper;
import com.youyilin.goods.service.ProductService;
import com.youyilin.goods.service.ProductUnitService;
import com.youyilin.goods.service.UnitDataService;
import com.youyilin.goods.service.UnitService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class UnitManager {

    @Autowired
    private UnitMapper unitMapper;
    @Autowired
    private UnitService unitService;
    @Autowired
    private UnitDataService unitDataService;
    @Autowired
    private ProductService productService;
    @Autowired
    private ProductUnitService productUnitService;

    /**
     * 商品新增时获取规格列表
     *
     * @param categoryId 分类ID
     * @return ArrayList
     */
    public List<UnitDTO> listProductAddByCategoryId(Long categoryId) {
        // 获取和分类关联的规格列表
        List<Unit> unitList = unitMapper.listByCategoryId(categoryId);
        if (CollectionUtils.isEmpty(unitList)) {
            return new ArrayList<>();
        }
        // 规格ID列表
        List<Long> unitIds = new ArrayList<>();
        unitList.forEach(item -> unitIds.add(item.getId()));
        // 按规格ID列表获取规格值并分组
        Map<Long, List<UnitData>> unitDataMap = unitDataService.mapByUnitIds(unitIds);
        // 数据转换
        List<UnitDTO> unitDTOList = UnitDTO.convertByEntity(unitList);
        for (UnitDTO item : unitDTOList) {
            item.setItemList(UnitDataDTO.convertByEntity(unitDataMap.get(item.getId())));
        }

        return unitDTOList;
    }

    /**
     * 商品编辑时获取规格列表
     *
     * @param productId  商品ID
     * @param categoryId 分类ID
     * @return ArrayList
     */
    public List<UnitDTO> listProductEditByProductIdAndCategoryId(Long productId, Long categoryId) {
        // 获取规格和分类关联的列表
        List<UnitDTO> unitDTOList = this.listProductAddByCategoryId(categoryId);
        if (CollectionUtils.isEmpty(unitDTOList)) {
            return new ArrayList<>();
        }
        // 获取产品中新配置的规格列表
        Map<Long, List<UnitData>> unitDataMap = unitDataService.mapByProductId(productId);
        for (UnitDTO item : unitDTOList) {
            List<UnitDataDTO> itemDataList = UnitDataDTO.convertByEntity(unitDataMap.get(item.getId()));
            if (CollectionUtils.isEmpty(itemDataList)) {
                continue;
            }
            for (UnitDataDTO dataItem : itemDataList) {
                dataItem.setClosable(true);
            }
            item.getItemList().addAll(itemDataList);
        }
        // 设置规格列表选中
        List<ProductUnit> productUnitList = productUnitService.listByProductId(productId);
        if (CollectionUtils.isEmpty(productUnitList)) {
            return unitDTOList;
        }
        Map<Long, ProductUnit> productUnitMap = productUnitList.stream().collect(Collectors.toMap(ProductUnit::getUnitDataId, item -> item));
        for (UnitDTO unitItem : unitDTOList) {
            List<UnitDataDTO> unitDataDTOList = unitItem.getItemList();
            for (UnitDataDTO dataItem : unitDataDTOList) {
                if (productUnitMap.containsKey(dataItem.getId())) {
                    dataItem.setChecked(true);
                }
            }
        }
        return unitDTOList;
    }

    /**
     * 商品详情时获取规格列表
     *
     * @param productId 商品ID
     * @return ArrayList
     */
    public List<UnitDTO> listProductDetailByProductId(Long productId) {
        List<ProductUnit> productUnitList = productUnitService.listByProductId(productId);
        if (CollectionUtils.isEmpty(productUnitList)) {
            return new ArrayList<>();
        }
        Set<Long> unitIds = new HashSet<>();
        Set<Long> unitDataIds = new HashSet<>();
        for (ProductUnit item : productUnitList) {
            unitIds.add(item.getUnitId());
            unitDataIds.add(item.getUnitDataId());
        }
        // 规格列表
        List<Unit> unitList = unitService.listByIds(new ArrayList<>(unitIds));
        // 规格值列表
        Map<Long, List<UnitData>> unitDataMap = unitDataService.mapByIds(new ArrayList<>(unitDataIds));
        // 数据转换
        List<UnitDTO> unitDTOList = UnitDTO.convertByEntity(unitList);
        for (UnitDTO item : unitDTOList) {
            item.setItemList(UnitDataDTO.convertByEntity(unitDataMap.get(item.getId())));
        }
        return unitDTOList;
    }
}
