package com.youyilin.goods.manager;

import com.youyilin.goods.dto.OutboundValidateProductDTO;
import com.youyilin.goods.dto.PurchaseValidateProductDTO;
import com.youyilin.goods.dto.SalesValidateProductDTO;

import java.util.List;

public interface ProductManager {

    /**
     * 采购单验证商品
     */
    void validatePurchaseProductBatch(List<PurchaseValidateProductDTO> productList);

    /**
     * 销售单验证商品
     *
     * @param productList  商品列表
     * @param useWholesale 批发标识
     */
    void validateSalesProductBatch(List<SalesValidateProductDTO> productList, boolean useWholesale);

    /**
     * 出库单验证商品
     */
    void validateOutboundProductBatch(List<OutboundValidateProductDTO> productList);
}
