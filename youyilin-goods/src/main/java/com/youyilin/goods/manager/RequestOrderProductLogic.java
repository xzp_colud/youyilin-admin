package com.youyilin.goods.manager;

import com.youyilin.goods.model.request.RequestMiniProduct;
import com.youyilin.goods.model.request.RequestOrderProduct;

import java.util.List;

/**
 * 销售订单 商品
 */
public interface RequestOrderProductLogic {

    /**
     * 刷新购物车中产品信息
     */
    void refreshCartProductInfoBatch(List<RequestMiniProduct> request);

    /**
     * 验证
     */
    void validateMiniProduct(RequestMiniProduct request);

    /**
     * 验证
     */
    void validateMiniProductBatch(List<RequestMiniProduct> request);

    /**
     * 验证
     */
    void validateProductBatch(List<RequestOrderProduct> dto, boolean validateSellPriceFit);
}
