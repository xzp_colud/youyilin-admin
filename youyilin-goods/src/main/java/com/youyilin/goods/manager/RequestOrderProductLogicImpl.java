package com.youyilin.goods.manager;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.youyilin.common.enums.StatusEnum;
import com.youyilin.common.exception.ApiException;
import com.youyilin.goods.entity.Product;
import com.youyilin.goods.entity.ProductPrice;
import com.youyilin.goods.model.request.RequestMiniProduct;
import com.youyilin.goods.model.request.RequestOrderProduct;
import com.youyilin.goods.model.request.RequestOrderProductPrice;
import com.youyilin.goods.service.ProductPriceService;
import com.youyilin.goods.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 销售订单 商品
 */
@Slf4j
@Service
public class RequestOrderProductLogicImpl implements RequestOrderProductLogic {

    private final ProductService productService;
    private final ProductPriceService productPriceService;

    public RequestOrderProductLogicImpl(ProductService productService, ProductPriceService productPriceService) {
        this.productService = productService;
        this.productPriceService = productPriceService;
    }

    @Override
    public void refreshCartProductInfoBatch(List<RequestMiniProduct> list) {
        Set<Long> priceIds = new HashSet<>();
        Set<Long> productIds = new HashSet<>();
        for (RequestMiniProduct item : list) {
            priceIds.add(item.getSkuId());
            productIds.add(item.getProductId());
        }

        List<Product> productList = productService.listByIds(productIds);
        List<ProductPrice> priceList = productPriceService.listByIds(priceIds);

        if (CollectionUtils.isEmpty(productList) || CollectionUtils.isEmpty(priceList)) return;

        Map<Long, List<Product>> productMap = productList.stream().collect(Collectors.groupingBy(Product::getId));
        Map<Long, List<ProductPrice>> priceMap = priceList.stream().collect(Collectors.groupingBy(ProductPrice::getId));
        for (RequestMiniProduct request : list) {
            request.setSell(true);
            List<Product> product = productMap.get(request.getProductId());
            List<ProductPrice> price = priceMap.get(request.getSkuId());
            if (CollectionUtils.isEmpty(product) || CollectionUtils.isEmpty(price) || !product.get(0).getId().equals(price.get(0).getProductId())) {
                request.setSell(false);
                continue;
            }
            Product p = product.get(0);
            ProductPrice pp = price.get(0);
            if (p.getStatus() == null || p.getStatus() != StatusEnum.NORMAL.getCode() || pp.getStatus() == null || pp.getStatus() != StatusEnum.NORMAL.getCode()) {
                request.setSell(false);
            }
            request.setImage(p.getImageUrl());
            request.setSkuName(pp.getSkuName());
            request.setProductName(p.getName());
            request.setSellAmount(pp.getSellPrice());
            request.setCategoryId(p.getCategoryId());
            request.setCategoryName(p.getCategoryName());
            request.setSupplierId(p.getSupplierId());
            request.setSupplierName(p.getSupplierName());
            request.setCostAmount(pp.getCostPrice());
        }
    }

    @Override
    public void validateMiniProduct(RequestMiniProduct request) {
        List<RequestMiniProduct> list = new ArrayList<>();
        list.add(request);

        this.validateMiniProductBatch(list);
    }

    @Override
    public void validateMiniProductBatch(List<RequestMiniProduct> list) {
        Set<Long> productIds = new HashSet<>();
        Set<Long> priceIds = new HashSet<>();
        for (RequestMiniProduct item : list) {
            productIds.add(item.getProductId());
            priceIds.add(item.getSkuId());
        }

        List<Product> productList = productService.listByIds(productIds);
        List<ProductPrice> priceList = productPriceService.listByIds(priceIds);
        if (CollectionUtils.isEmpty(productList) || CollectionUtils.isEmpty(priceList)) {
            throw new ApiException("产品异常");
        }

        Map<Long, List<Product>> productMap = productList.stream().collect(Collectors.groupingBy(Product::getId));
        Map<Long, List<ProductPrice>> priceMap = priceList.stream().collect(Collectors.groupingBy(ProductPrice::getId));

        for (RequestMiniProduct request : list) {

            List<Product> product = productMap.get(request.getProductId());
            List<ProductPrice> price = priceMap.get(request.getSkuId());
            if (CollectionUtils.isEmpty(product) || CollectionUtils.isEmpty(price) || !product.get(0).getId().equals(price.get(0).getProductId())) {
                throw new ApiException("产品异常");
            }
            Product p = product.get(0);
            if (p.getStatus() == null || p.getStatus() != StatusEnum.NORMAL.getCode()) {
                throw new ApiException("产品已下架");
            }
            ProductPrice pp = price.get(0);
            if (pp.getStatus() == null || pp.getStatus() != StatusEnum.NORMAL.getCode()) {
                throw new ApiException("产品已下架");
            }
            request.setProductName(p.getName());
            request.setImage(p.getImageUrl());
            request.setSkuName(pp.getSkuName());
            request.setSellAmount(pp.getSellPrice());
            request.setCategoryId(p.getCategoryId());
            request.setCategoryName(p.getCategoryName());
            request.setSupplierId(p.getSupplierId());
            request.setSupplierName(p.getSupplierName());
            request.setCostAmount(pp.getCostPrice());
        }
    }

    @Override
    public void validateProductBatch(List<RequestOrderProduct> dto, boolean validateSellPriceFit) {
        log.info("【提交 商品】 START {}", new Date().getTime());
        this.validateNull(dto);
        Set<Long> productSet = new HashSet<>();
        Set<Long> productPriceSet = new HashSet<>();
        // 第一次验证
        for (int i = 0; i < dto.size(); i++) {
            int index = i + 1;
            RequestOrderProduct productRequest = dto.get(i);
            Long productId = this.validateProduct(index, productRequest);
            Long productPriceId = this.validateProductPrice(index, productRequest);

            productSet.add(productId);
            productPriceSet.add(productPriceId);

            productRequest.setValidateSellPriceFit(validateSellPriceFit);
        }

        Map<Long, List<Product>> productMap = this.getProductMapByIds(productSet);
        Map<Long, List<ProductPrice>> productPriceMap = this.getProductPriceMapByIds(productPriceSet);

        // 二次验证
        this.validateSecond(dto, productMap, productPriceMap);

        log.info("【提交 商品】 END {} {}", new Date().getTime(), JSONObject.toJSONString(dto));
    }

    private void validateNull(List<RequestOrderProduct> list) {
        if (CollectionUtils.isEmpty(list)) {
            throw new ApiException("商品不能为空");
        }
    }

    private Long validateProduct(int index, RequestOrderProduct productRequest) {
        Long productId = productRequest.getProductId();
        if (productId == null) {
            throw new ApiException("第[" + index + "]个商品异常");
        }
        return productId;
    }

    private Long validateProductPrice(int index, RequestOrderProduct productRequest) {
        RequestOrderProductPrice priceRequest = productRequest.getPrice();
        // SKU ID
        Long priceId = priceRequest.getId();
        if (priceId == null) {
            throw new ApiException("第[" + index + "]个商品信息异常");
        }

        // 单价
        Long sellPrice = priceRequest.getSellPrice();
        // 折扣
        Integer discount = priceRequest.getDiscount();
        // 数量
        Integer num = RequestOrderProductPrice.numToInt(priceRequest.getNum());
        // 验证
        this.validateSellPriceAndDiscountAndNum(index, sellPrice, discount, num);
        if (discount != 100 && StringUtils.isBlank(priceRequest.getRemark())) {
            throw new ApiException("折扣不为100 备注必填");
        }

        // 原价
        Long totalPrice = sellPrice * num / 100;
        // 实际销售价
        Long sellAmount = sellPrice * discount / 100;
        // 销售总价
        Long totalSell = sellAmount * num / 100;
        // 折扣总价
        Long totalDiscount = totalPrice - totalSell;

        priceRequest.setSellAmount(sellAmount);
        priceRequest.setTotalSell(totalSell);
        priceRequest.setTotalDiscount(totalDiscount);

        return priceId;
    }

    private void validateSellPriceAndDiscountAndNum(int index, Long sellPrice, Integer discount, Integer num) {
        if (sellPrice == null || sellPrice < 0) {
            throw new ApiException("第[" + index + "]个商品销售价异常");
        }
        if (discount == null || discount < 0 || discount > 100) {
            throw new ApiException("第[" + index + "]个商品折扣异常");
        }
        if (num == null || num <= 0) {
            throw new ApiException("第[" + index + "]个商品数量异常");
        }
    }

    private Map<Long, List<Product>> getProductMapByIds(Set<Long> productSet) {
        List<Product> productList = productService.listByIds(productSet);
        return productList.stream().collect(Collectors.groupingBy(Product::getId));
    }

    private Map<Long, List<ProductPrice>> getProductPriceMapByIds(Set<Long> productPriceSet) {
        List<ProductPrice> productPriceList = productPriceService.listByIds(productPriceSet);
        return productPriceList.stream().collect(Collectors.groupingBy(ProductPrice::getId));
    }

    private void validateSecond(List<RequestOrderProduct> dto, Map<Long, List<Product>> productMap, Map<Long, List<ProductPrice>> productPriceMap) {
        for (RequestOrderProduct productRequest : dto) {
            this.validateSecondProduct(productRequest, productMap);
            this.validateSecondProductPrice(productRequest, productPriceMap);
        }
    }

    private void validateSecondProduct(RequestOrderProduct productRequest, Map<Long, List<Product>> productMap) {
        Long productId = productRequest.getProductId();
        List<Product> productList = productMap.get(productId);
        if (CollectionUtils.isEmpty(productList) || productList.size() != 1) {
            throw new ApiException("商品异常");
        }
        Product product = productList.get(0);
        if (product.getStatus() == null
                || product.getStatus() != StatusEnum.NORMAL.getCode()) {
            throw new ApiException("[" + product.getName() + "]已下架");
        }

        productRequest.setProductName(product.getName());
        productRequest.setSn(product.getSn());
        productRequest.setSupplierId(product.getSupplierId());
        productRequest.setSupplierName(product.getSupplierName());
        productRequest.setCategoryId(product.getCategoryId());
        productRequest.setCategoryName(product.getCategoryName());
        if (StringUtils.isBlank(productRequest.getImageUrl())) {
            productRequest.setImageUrl(product.getImageUrl());
        }
    }

    private void validateSecondProductPrice(RequestOrderProduct productRequest, Map<Long, List<ProductPrice>> productPriceMap) {
        String productName = productRequest.getProductName();
        Long productId = productRequest.getProductId();
        RequestOrderProductPrice priceRequest = productRequest.getPrice();
        Long productPriceId = priceRequest.getId();
        if (productPriceId == null) {
            throw new ApiException("商品信息异常");
        }
        List<ProductPrice> productPriceList = productPriceMap.get(productPriceId);
        if (CollectionUtils.isEmpty(productPriceList) || productPriceList.size() != 1) {
            throw new ApiException("[" + productName + "]商品信息异常");
        }
        ProductPrice productPrice = productPriceList.get(0);
        if (productPrice.getStatus() == null
                || productPrice.getStatus() != StatusEnum.NORMAL.getCode()) {
            throw new ApiException("[" + productName + "]商品已下架");
        }
        if (productPrice.getProductId() == null
                || !productPrice.getProductId().toString().equals(productId.toString())) {
            throw new ApiException("[" + productName + "]商品已下架");
        }
//        if (productPrice.getInventory() == null || productPrice.getInventory().compareTo(BigDecimal.ZERO) <= 0) {
//            throw new ApiException("[" + productName + "]库存不足");
//        }

        if (productRequest.isValidateSellPriceFit()) {
            priceRequest.setSellPrice(productPrice.getSellPrice());
            priceRequest.setDiscount(100);
            // 重新验证
            this.validateProductPrice(0, productRequest);
        }

        Long costAmount = productPrice.getCostPrice();
        Integer num = RequestOrderProductPrice.numToInt(priceRequest.getNum());
        Long totalCostAmount = costAmount * num / 100;
        priceRequest.setCostAmount(costAmount);
        priceRequest.setTotalCost(totalCostAmount);

        priceRequest.setSkuName(productPrice.getSkuName());
    }
}