package com.youyilin.logic.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class DashboardCustomerCardVO {

    // 选中时间
    private String timeValue;
    // 时间列表
    private List<TimeVO> timeList;
    // 选中数据
    private DashboardCustomerCard item;
    // 数据明细
    private List<DashboardCustomerCard> itemList;
    // 客户总数
    private Long totalNum;
}
