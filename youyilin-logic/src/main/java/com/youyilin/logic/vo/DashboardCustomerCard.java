package com.youyilin.logic.vo;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class DashboardCustomerCard {

    public DashboardCustomerCard() {
    }

    public DashboardCustomerCard(String type, Long thisNewNum, Long thisActiveNum, Long lastNewNum, Long lastActiveNum, String thisNewText, String thisActiveText, String lastNewText, String lastActiveText) {
        this.type = type;
        this.thisNewNum = thisNewNum;
        this.thisActiveNum = thisActiveNum;
        this.lastNewNum = lastNewNum;
        this.lastActiveNum = lastActiveNum;
        this.thisNewText = thisNewText;
        this.thisActiveText = thisActiveText;
        this.lastNewText = lastNewText;
        this.lastActiveText = lastActiveText;
    }

    // 类型
    private String type;
    private String thisNewText;
    private String thisActiveText;
    // 新增数量
    private Long thisNewNum;
    // 活跃数量
    private Long thisActiveNum;
    private String lastNewText;
    private String lastActiveText;
    // 新增数量
    private Long lastNewNum;
    // 活跃数量
    private Long lastActiveNum;
}
