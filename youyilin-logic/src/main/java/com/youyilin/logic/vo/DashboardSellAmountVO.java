package com.youyilin.logic.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.youyilin.common.utils.FormatAmountUtil;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class DashboardSellAmountVO {

    // 客户名称
    private String customerName;
    // 客户电话
    private String customerPhone;
    // 总金额
    @JsonIgnore
    private Long totalMoney;

    private String getTotalMoneyText() {
        return FormatAmountUtil.format(this.totalMoney);
    }
}
