package com.youyilin.logic.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDate;
import java.time.ZoneId;

@Data
@Accessors(chain = true)
public class TimeDTO {

    public TimeDTO() {
        init();
    }

    private String now;
    private String yesterday;
    private String tomorrow;

    private String thisWeekStart;
    private String thisWeekEnd;
    private String lastWeekStart;
    private String lastWeekEnd;

    private String thisMonthStart;
    private String thisMonthEnd;
    private String lastMonthStart;
    private String lastMonthEnd;

    private String thisYearStart;
    private String thisYearEnd;
    private String lastYearStart;
    private String lastYearEnd;

    private void init() {
        String zeroTime = " 00:00:00";
        String sixTime = " 23:59:59";
        LocalDate today = LocalDate.now();
        // 当日
        LocalDate nowStart = today.atStartOfDay(ZoneId.systemDefault()).toLocalDate();
        LocalDate nowEnd = today.plusDays(1);
        // 昨日
        LocalDate yesterday = today.minusDays(1);
        LocalDate yesterdayStart = yesterday.atStartOfDay(ZoneId.systemDefault()).toLocalDate();
        // 本周
        LocalDate thisWeekStart = today.minusDays(today.getDayOfWeek().getValue() - 1);
        LocalDate thisWeekEnd = thisWeekStart.plusDays(6);
        // 上周
        LocalDate lastWeekStart = thisWeekStart.minusDays(7);
        LocalDate lastWeekEnd = lastWeekStart.plusDays(6);
        // 本月
        LocalDate thisMonthStart = today.withDayOfMonth(1);
        LocalDate thisMonthEnd = thisMonthStart.withDayOfMonth(thisMonthStart.lengthOfMonth());
        // 上月
        LocalDate lastMonthStart = today.minusMonths(1).withDayOfMonth(1);
        LocalDate lastMonthEnd = lastMonthStart.withDayOfMonth(lastMonthStart.lengthOfMonth());
        // 本年
        LocalDate thisYearStart = today.withDayOfYear(1);
        LocalDate thisYearEnd = thisYearStart.withDayOfYear(thisYearStart.lengthOfYear());
        // 上年
        LocalDate lastYearStart = today.minusYears(1).withDayOfYear(1);
        LocalDate lastYearEnd = lastYearStart.withDayOfYear(lastYearStart.lengthOfYear());

        this.now = nowStart + zeroTime;
        this.yesterday = yesterdayStart + zeroTime;
        this.tomorrow = nowEnd + zeroTime;
        this.thisWeekStart = thisWeekStart + zeroTime;
        this.thisWeekEnd = thisWeekEnd + sixTime;
        this.lastWeekStart = lastWeekStart + zeroTime;
        this.lastWeekEnd = lastWeekEnd + sixTime;
        this.thisMonthStart = thisMonthStart + zeroTime;
        this.thisMonthEnd = thisMonthEnd + sixTime;
        this.lastMonthStart = lastMonthStart + zeroTime;
        this.lastMonthEnd = lastMonthEnd + sixTime;
        this.thisYearStart = thisYearStart + zeroTime;
        this.thisYearEnd = thisYearEnd + sixTime;
        this.lastYearStart = lastYearStart + zeroTime;
        this.lastYearEnd = lastYearEnd + sixTime;
    }
}
