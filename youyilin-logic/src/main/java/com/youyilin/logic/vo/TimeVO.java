package com.youyilin.logic.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

@Data
@Accessors(chain = true)
public class TimeVO {

    private String label;
    private String value;

    public TimeVO(String label, String value) {
        this.label = label;
        this.value = value;
    }

    public static List<TimeVO> initYMWD() {
        List<TimeVO> list = new ArrayList<>();
        list.add(new TimeVO(TimeEnum.DAY.label, TimeEnum.DAY.value));
        list.add(new TimeVO(TimeEnum.WEEK.label, TimeEnum.WEEK.value));
        list.add(new TimeVO(TimeEnum.MONTH.label, TimeEnum.MONTH.value));
        list.add(new TimeVO(TimeEnum.YEAR.label, TimeEnum.YEAR.value));
        return list;
    }

    public enum TimeEnum {
        DAY("日", "day"),
        WEEK("周", "week"),
        MONTH("月", "month"),
        YEAR("年", "year");

        private String label;
        private String value;

        TimeEnum(String label, String value) {
            this.label = label;
            this.value = value;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}
