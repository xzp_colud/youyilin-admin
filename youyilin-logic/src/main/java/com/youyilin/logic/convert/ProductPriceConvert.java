package com.youyilin.logic.convert;

import com.youyilin.goods.entity.ProductPrice;
import com.youyilin.order.vo.outbound.OutboundOrderEditItemSku;
import com.youyilin.order.vo.purchase.PurchaseOrderEditItemSku;
import com.youyilin.order.vo.sales.OrderEditItemSku;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 商品SKU转换器
 */
public class ProductPriceConvert {

    /**
     * 转换成销售单编辑时的SKU
     *
     * @param sourceList 商品SKU
     * @return ArrayList
     */
    public static List<OrderEditItemSku> convertToOrderEditItemSku(List<ProductPrice> sourceList) {
        if (CollectionUtils.isEmpty(sourceList)) {
            return new ArrayList<>();
        }
        List<OrderEditItemSku> targetList = new ArrayList<>();
        for (ProductPrice sku : sourceList) {
            OrderEditItemSku orderEditItemSku = new OrderEditItemSku();
            orderEditItemSku.setId(sku.getId())
                    .setSkuName(sku.getSkuName())
                    .setSellPrice(sku.getSellPrice())
                    .setStatus(sku.getStatus())
                    .setInventory(sku.getInventory())
                    .setWholesalePrice(sku.getWholesalePrice());

            targetList.add(orderEditItemSku);
        }

        return targetList;
    }

    /**
     * 转换成采购单编辑时的SKU
     *
     * @param sourceList 商品SKU
     * @return ArrayList
     */
    public static List<PurchaseOrderEditItemSku> convertToPurchaseOrderEditItemSku(List<ProductPrice> sourceList) {
        if (CollectionUtils.isEmpty(sourceList)) {
            return new ArrayList<>();
        }
        List<PurchaseOrderEditItemSku> targetList = new ArrayList<>();
        for (ProductPrice sku : sourceList) {
            PurchaseOrderEditItemSku editItemSku = new PurchaseOrderEditItemSku();
            editItemSku.setId(sku.getId())
                    .setSkuName(sku.getSkuName())
                    .setCostPrice(sku.getCostPrice())
                    .setWholesalePrice(sku.getWholesalePrice())
                    .setStatus(sku.getStatus());

            targetList.add(editItemSku);
        }

        return targetList;
    }

    /**
     * 转换成出库单编辑时的SKU
     *
     * @param sourceList 商品SKU
     * @return ArrayList
     */
    public static List<OutboundOrderEditItemSku> convertToOutboundOrderEditItemSku(List<ProductPrice> sourceList) {
        if (CollectionUtils.isEmpty(sourceList)) {
            return new ArrayList<>();
        }
        List<OutboundOrderEditItemSku> targetList = new ArrayList<>();
        for (ProductPrice sku : sourceList) {
            OutboundOrderEditItemSku editItemSku = new OutboundOrderEditItemSku();
            editItemSku.setId(sku.getId())
                    .setSkuName(sku.getSkuName())
                    .setStatus(sku.getStatus());

            targetList.add(editItemSku);
        }

        return targetList;
    }
}
