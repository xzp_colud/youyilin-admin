package com.youyilin.logic.convert;

import com.youyilin.goods.entity.Product;
import com.youyilin.goods.entity.ProductPrice;
import com.youyilin.warehouse.entity.*;
import com.youyilin.warehouse.enums.InventoryLogTypeEnum;

import java.math.BigDecimal;

/**
 * 库存日志转换器
 */
public class InventoryLogConvert {

    public static InventoryLog convertBy(Inventory inventory, InventoryLot lot, Product product, ProductPrice sku,
                                         Warehouse warehouse, WarehouseArea warehouseArea, BigDecimal qty, InventoryLogTypeEnum typeEnum) {
        InventoryLog log = new InventoryLog();
        log.setInventoryId(inventory.getId())
                .setInventoryLotId(lot.getId())
                .setInventoryLotSn(lot.getSn())
                .setQty(qty)
                .setResidueQty(lot.getQty())
                .setProductId(inventory.getProductId())
                .setProductName(product.getName())
                .setSkuId(inventory.getSkuId())
                .setSkuName(sku.getSkuName())
                .setWarehouseId(inventory.getWarehouseId())
                .setWarehouseName(warehouse.getName())
                .setWarehouseAreaId(inventory.getWarehouseAreaId())
                .setWarehouseAreaName(warehouseArea.getName())
                .setType(typeEnum.getCode())
                .setSerialNum(lot.getLotSn())
                .setAmount(0L);
        return log;
    }
}
