package com.youyilin.logic.manager.impl;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.youyilin.common.exception.ApiException;
import com.youyilin.logic.manager.OutboundOrderManager;
import com.youyilin.order.bo.outbound.OutboundOrderThirdConstructBO;
import com.youyilin.order.dto.outbound.OutboundOrderItemDetailThirdConstructDTO;
import com.youyilin.order.entity.OutboundOrder;
import com.youyilin.order.entity.OutboundOrderItem;
import com.youyilin.order.entity.OutboundOrderItemDetail;
import com.youyilin.order.enums.OrderLogEnum;
import com.youyilin.order.enums.OutboundOrderItemStatusEnum;
import com.youyilin.order.enums.OutboundOrderStatusEnum;
import com.youyilin.order.service.OrderLogService;
import com.youyilin.order.service.OutboundOrderItemDetailService;
import com.youyilin.order.service.OutboundOrderItemService;
import com.youyilin.order.service.OutboundOrderService;
import com.youyilin.order.utils.NoUtils;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class OutboundOrderManagerImpl implements OutboundOrderManager {

    private final OutboundOrderService outboundOrderService;
    private final OutboundOrderItemService outboundOrderItemService;
    private final OutboundOrderItemDetailService outboundOrderItemDetailService;
    private final OrderLogService orderLogService;

    public OutboundOrderManagerImpl(OutboundOrderService outboundOrderService,
                                    OutboundOrderItemService outboundOrderItemService,
                                    OutboundOrderItemDetailService outboundOrderItemDetailService,
                                    OrderLogService orderLogService) {
        this.outboundOrderService = outboundOrderService;
        this.outboundOrderItemService = outboundOrderItemService;
        this.outboundOrderItemDetailService = outboundOrderItemDetailService;
        this.orderLogService = orderLogService;
    }

    /**
     * 创建出库单(由系统内部其他订单转换而来)
     */
    @Override
    @Transactional
    public void constructOutboundOrder(OutboundOrderThirdConstructBO bo) {
        OutboundOrder oo = new OutboundOrder();
        oo.setId(IdWorker.getId())
                .setSn(NoUtils.getOutboundOrderSn())
                .setStatus(OutboundOrderStatusEnum.NO_CONFIRM.getCode())
                .setSourceType(OutboundOrder.SourceType.SALE_ORDER.name())
                .setOrderNo(bo.getSourceSn());

        bo.setOutboundOrder(oo);

        // 参数验证
        this.validate(bo);
        // 创建出库单明细
        this.constructOutboundOrderItem(bo);
        // 持久化出库单
        this.saveOutboundOrder(bo);
    }

    private void validate(OutboundOrderThirdConstructBO bo) {
        if (CollectionUtils.isEmpty(bo.getSourceList())) {
            throw new ApiException("出库异常");
        }
        for (OutboundOrderItemDetailThirdConstructDTO dto : bo.getSourceList()) {
            if (dto.getSourceId() == null
                    || dto.getProductId() == null
                    || dto.getSkuId() == null
                    || dto.getQty() == null
                    || dto.getQty().compareTo(BigDecimal.ZERO) < 1) {
                throw new ApiException("出库异常");
            }
        }
    }

    private void constructOutboundOrderItem(OutboundOrderThirdConstructBO bo) {
        Map<Long, List<OutboundOrderItemDetailThirdConstructDTO>> skuMap = bo.getSourceList()
                .stream()
                .collect(Collectors.groupingBy(OutboundOrderItemDetailThirdConstructDTO::getSkuId));

        // 序号
        int index = 1;
        // 出库单ID
        Long outboundId = bo.getOutboundOrder().getId();
        String outboundSn = bo.getOutboundOrder().getSn();
        // 出库单明细
        List<OutboundOrderItem> itemList = new ArrayList<>();
        // 出库单明细详情
        List<OutboundOrderItemDetail> itemDetailList = new ArrayList<>();
        for (Long skuId : skuMap.keySet()) {
            List<OutboundOrderItemDetailThirdConstructDTO> sourceList = skuMap.get(skuId);
            OutboundOrderItemDetailThirdConstructDTO source = sourceList.get(0);
            BigDecimal itemTotalQty = BigDecimal.ZERO;

            // 出库单明细ID
            Long outboundItemId = IdWorker.getId();
            for (OutboundOrderItemDetailThirdConstructDTO dto : sourceList) {
                itemTotalQty = itemTotalQty.add(dto.getQty());
                // 构建出库单明细详情
                OutboundOrderItemDetail itemDetail = new OutboundOrderItemDetail();
                itemDetail.setOrderId(outboundId)
                        .setOrderItemId(outboundItemId)
                        .setSourceGroupId(dto.getSourceGroupId())
                        .setSourceId(dto.getSourceId())
                        .setSourceSn(dto.getSourceSn())
                        .setQty(dto.getQty())
                        .setStatus(OutboundOrderItemStatusEnum.NO_INVENTORY_OUT.getCode());

                itemDetailList.add(itemDetail);
            }
            // 构建出库单明细
            OutboundOrderItem outboundItem = new OutboundOrderItem();
            outboundItem.setId(outboundItemId)
                    .setItemSn(outboundSn + "-" + index)
                    .setOrderId(outboundId)
                    .setSn(outboundSn)
                    .setProductId(source.getProductId())
                    .setProductName(source.getProductName())
                    .setSkuId(source.getSkuId())
                    .setSkuName(source.getSkuName())
                    .setCategoryId(source.getCategoryId())
                    .setCategoryName(source.getCategoryName())
                    .setSupplierId(source.getSupplierId())
                    .setSupplierName(source.getSupplierName())
                    .setQty(itemTotalQty)
                    .setNoOutQty(itemTotalQty)
                    .setStatus(OutboundOrderStatusEnum.NO_CONFIRM.getCode());

            itemList.add(outboundItem);
            index++;
        }

        bo.setItemList(itemList).setItemDetailList(itemDetailList);
    }

    private void saveOutboundOrder(OutboundOrderThirdConstructBO bo) {
        // 持久化出库单
        outboundOrderService.save(bo.getOutboundOrder());
        // 持久化出库单明细
        outboundOrderItemService.saveBatch(bo.getItemList());
        // 持久化出库单明细详情
        outboundOrderItemDetailService.saveBatch(bo.getItemDetailList());
        // 日志
        orderLogService.saveLog(bo.getOutboundOrder().getId(), bo.getOutboundOrder().getSn(), OrderLogEnum.CREATE_OR_UPDATE.getCode());
    }
}
