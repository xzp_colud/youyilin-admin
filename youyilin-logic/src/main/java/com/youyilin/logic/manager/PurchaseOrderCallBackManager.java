package com.youyilin.logic.manager;

public interface PurchaseOrderCallBackManager {

    /**
     * 反馈取消
     *
     * @param id 采购单ID
     */
    void callBackCancel(Long id);

    /**
     * 反馈完成
     *
     * @param id 采购单ID
     */
    void callBackFinish(Long id);
}
