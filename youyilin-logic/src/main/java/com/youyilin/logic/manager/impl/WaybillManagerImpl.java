package com.youyilin.logic.manager.impl;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.youyilin.common.exception.ApiException;
import com.youyilin.logic.manager.WaybillManager;
import com.youyilin.waybill.bo.WaybillThirdConstructBO;
import com.youyilin.waybill.dto.waybill.WaybillItemThirdConstructDTO;
import com.youyilin.waybill.entity.Waybill;
import com.youyilin.waybill.entity.WaybillItem;
import com.youyilin.waybill.enums.WaybillStatusEnum;
import com.youyilin.waybill.service.WaybillItemService;
import com.youyilin.waybill.service.WaybillService;
import com.youyilin.waybill.utils.SnUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class WaybillManagerImpl implements WaybillManager {

    private final WaybillService waybillService;
    private final WaybillItemService waybillItemService;

    public WaybillManagerImpl(WaybillService waybillService, WaybillItemService waybillItemService) {
        this.waybillService = waybillService;
        this.waybillItemService = waybillItemService;
    }

    @Override
    @Transactional
    public void constructWaybill(WaybillThirdConstructBO bo) {
        this.validate(bo);
        this.convertBOToEntity(bo);
        this.constructWaybillItem(bo);
        waybillService.save(bo.getWaybill());
        waybillItemService.saveBatch(bo.getItemList());
    }

    private void validate(WaybillThirdConstructBO bo) {
        if (bo.getId() != null
                || StringUtils.isBlank(bo.getSourceType())
                || StringUtils.isBlank(bo.getReceiveName())
                || StringUtils.isBlank(bo.getReceiveMobile())
                || StringUtils.isBlank(bo.getReceiveProvince())
                || StringUtils.isBlank(bo.getReceiveCity())
                || StringUtils.isBlank(bo.getReceiveArea())
                || StringUtils.isBlank(bo.getReceiveDetail())) {
            throw new ApiException("参数异常");
        }
        if (CollectionUtils.isEmpty(bo.getItemSourceList())) {
            throw new ApiException("明细不能为空");
        }
        for (WaybillItemThirdConstructDTO item : bo.getItemSourceList()) {
            if (item.getProductId() == null
                    || item.getProductPriceId() == null
                    || item.getSourceId() == null
                    || item.getNum() == null
                    || item.getNum().compareTo(BigDecimal.ZERO) <= 0) {
                throw new ApiException("明细参数异常");
            }
        }
    }

    private void convertBOToEntity(WaybillThirdConstructBO bo) {
        Waybill waybill = new Waybill();

        waybill.setId(IdWorker.getId())
                .setSn(SnUtil.getWaybillSn())
                .setReceiveName(bo.getReceiveName())
                .setReceiveMobile(bo.getReceiveMobile())
                .setReceiveProvince(bo.getReceiveProvince())
                .setReceiveCity(bo.getReceiveCity())
                .setReceiveArea(bo.getReceiveArea())
                .setReceiveDetail(bo.getReceiveDetail())
                .setAmount(0L)
                .setStatus(WaybillStatusEnum.DEFAULT.getCode())
                .setSourceType(bo.getSourceType());

        bo.setId(waybill.getId()).setSn(waybill.getSn()).setWaybill(waybill);
    }

    private void constructWaybillItem(WaybillThirdConstructBO bo) {
        List<WaybillItemThirdConstructDTO> itemSourceList = bo.getItemSourceList();

        List<WaybillItem> itemList = new ArrayList<>();
        for (WaybillItemThirdConstructDTO item : itemSourceList) {
            WaybillItem waybillItem = new WaybillItem();
            waybillItem.setWaybillId(bo.getId())
                    .setSourceId(item.getSourceId())
                    .setSourceNo(item.getSourceNo())
                    .setProductId(item.getProductId())
                    .setProductName(item.getProductName())
                    .setProductPriceId(item.getProductPriceId())
                    .setSkuName(item.getSkuName())
                    .setNum(item.getNum());

            itemList.add(waybillItem);
        }

        bo.setItemList(itemList);
    }
}
