package com.youyilin.logic.manager.impl;

import com.youyilin.logic.manager.OutboundOrderCallBackManger;
import com.youyilin.order.entity.*;
import com.youyilin.order.enums.CommonOutboundStatusEnum;
import com.youyilin.order.enums.OrderLogEnum;
import com.youyilin.order.enums.OrderStatusEnum;
import com.youyilin.order.service.*;
import com.youyilin.warehouse.entity.InventoryLog;
import com.youyilin.warehouse.service.InventoryLogService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 出库单出库完成回调
 */
@Service
public class OutboundOrderCallBackMangerImpl implements OutboundOrderCallBackManger {

    private final OutboundOrderService outboundOrderService;
    private final OutboundOrderItemDetailService outboundOrderItemDetailService;
    private final OrderService orderService;
    private final OrderItemService orderItemService;
    private final OrderLogService orderLogService;
    private final CustomerOrderService customerOrderService;
    private final CustomerOrderItemService customerOrderItemService;
    private final CustomerOrderLogService customerOrderLogService;
    private final OutboundOrderInventoryLogService outboundOrderInventoryLogService;
    private final InventoryLogService inventoryLogService;

    public OutboundOrderCallBackMangerImpl(OutboundOrderService outboundOrderService, OutboundOrderItemDetailService outboundOrderItemDetailService, OrderService orderService, OrderItemService orderItemService, OrderLogService orderLogService, CustomerOrderService customerOrderService, CustomerOrderItemService customerOrderItemService, CustomerOrderLogService customerOrderLogService, OutboundOrderInventoryLogService outboundOrderInventoryLogService, InventoryLogService inventoryLogService) {
        this.outboundOrderService = outboundOrderService;
        this.outboundOrderItemDetailService = outboundOrderItemDetailService;
        this.orderService = orderService;
        this.orderItemService = orderItemService;
        this.orderLogService = orderLogService;
        this.customerOrderService = customerOrderService;
        this.customerOrderItemService = customerOrderItemService;
        this.customerOrderLogService = customerOrderLogService;
        this.outboundOrderInventoryLogService = outboundOrderInventoryLogService;
        this.inventoryLogService = inventoryLogService;
    }

    @Override
    @Transactional
    public void callBackInventory(Long outboundId, Long outboundItemId) {
        OutboundOrder order = outboundOrderService.getById(outboundId);
        if (StringUtils.isBlank(order.getSourceType()) || order.getSourceType().equals(OutboundOrder.SourceType.DEFAULT.name())) {
            // 清理冻结库存
            this.clearOutboundFrozeQty(outboundItemId);
            return;
        }
        if (order.getSourceType().equals(OutboundOrder.SourceType.CUSTOMER_ORDER.name())) {
            this.feedBackOutboundInventoryOutForCustomerOrder(outboundItemId);
            return;
        }
        if (order.getSourceType().equals(OutboundOrder.SourceType.SALE_ORDER.name())) {
            this.feedBackOutboundInventoryOutForSaleOrder(outboundItemId);
        }
    }

    /**
     * 清理冻结的库存
     */
    private void clearOutboundFrozeQty(Long outboundItemId) {
//        List<OutboundOrderInventoryLog> inventoryLogList = outboundOrderInventoryLogService.listByOutboundItemId(outboundItemId);
//        if (CollectionUtils.isEmpty(inventoryLogList)) {
//            return;
//        }
//        List<InventoryLog> clearList = new ArrayList<>();
//        for (OutboundOrderInventoryLog item : inventoryLogList) {
////            InventoryLog clearItem = inventoryLogService.createInventoryLog(item.getWarehouseId(), item.getWarehouseAreaId(), item.getProductId(), item.getSkuId(), item.getSn(), item.getQty(), 0L);
////            clearList.add(clearItem);
//        }
//        // 执行清理冻结库存
//        inventoryLogService.clearFrozenQty(clearList);
    }

    /**
     * 反馈小程序订单出库信息
     *
     * @param orderItemId 出库单子集ID
     */
    private void feedBackOutboundInventoryOutForCustomerOrder(Long orderItemId) {
        List<OutboundOrderItemDetail> oidList = outboundOrderItemDetailService.listByOutboundItemId(orderItemId);
        Set<Long> sourceIds = new HashSet<>();
        Set<Long> saleOrderIds = new HashSet<>();
        for (OutboundOrderItemDetail item : oidList) {
            sourceIds.add(item.getSourceId());
            saleOrderIds.add(item.getSourceGroupId());
        }
        // 小程序单子集出库完成
        customerOrderItemService.updateOutboundFinishByIds(new ArrayList<>(sourceIds));
        List<CustomerOrderItem> coiList = customerOrderItemService.listByOrderIds(new ArrayList<>(saleOrderIds));
        Map<Long, List<CustomerOrderItem>> oiMap = coiList.stream().collect(Collectors.groupingBy(CustomerOrderItem::getOrderId));
        List<Long> updateOutboundOrderIds = new ArrayList<>();
        List<CustomerOrderItem> updateOutboundOrder = new ArrayList<>();
        for (Long key : oiMap.keySet()) {
            List<CustomerOrderItem> list = oiMap.get(key);
            if (CollectionUtils.isEmpty(list)) {
                continue;
            }
            boolean breakFlag = false;
            for (CustomerOrderItem item : list) {
                if (!item.getStatus().equals(CommonOutboundStatusEnum.OUTBOUND.getCode())) {
                    breakFlag = true;
                    break;
                }
            }
            if (breakFlag) {
                continue;
            }
            updateOutboundOrderIds.add(key);
            updateOutboundOrder.add(list.get(0));
        }
        if (CollectionUtils.isNotEmpty(updateOutboundOrderIds)) {
            customerOrderService.updateOutboundFinishByIds(updateOutboundOrderIds);
            for (CustomerOrderItem item : updateOutboundOrder) {
                customerOrderLogService.saveLog(item.getOrderId(), item.getSn(), "出库完成", null);
            }
        }
    }

    /**
     * 反馈销售单出库信息
     *
     * @param orderItemId 出库单子集ID
     */
    private void feedBackOutboundInventoryOutForSaleOrder(Long orderItemId) {
        List<OutboundOrderItemDetail> oidList = outboundOrderItemDetailService.listByOutboundItemId(orderItemId);
        Set<Long> sourceIds = new HashSet<>();
        Set<Long> saleOrderIds = new HashSet<>();
        for (OutboundOrderItemDetail item : oidList) {
            sourceIds.add(item.getSourceId());
            saleOrderIds.add(item.getSourceGroupId());
        }
        // 销售单子集出库完成
        orderItemService.updateOutboundFinishByIds(new ArrayList<>(sourceIds));
        List<OrderItem> oiList = orderItemService.listByOrderIds(new ArrayList<>(saleOrderIds));
        Map<Long, List<OrderItem>> oiMap = oiList.stream().collect(Collectors.groupingBy(OrderItem::getOrderId));
        List<Long> updateOutboundOrderIds = new ArrayList<>();
        List<OrderItem> updateOutboundOrder = new ArrayList<>();
        for (Long key : oiMap.keySet()) {
            List<OrderItem> list = oiMap.get(key);
            if (CollectionUtils.isEmpty(list)) {
                continue;
            }
            boolean breakFlag = false;
            for (OrderItem item : list) {
                if (!item.getStatus().equals(OrderStatusEnum.WAIT_SEND.getCode())) {
                    breakFlag = true;
                    break;
                }
            }
            if (breakFlag) {
                continue;
            }
            updateOutboundOrderIds.add(key);
            updateOutboundOrder.add(list.get(0));
        }
        if (CollectionUtils.isNotEmpty(updateOutboundOrderIds)) {
            orderService.updateOutboundFinishByIds(updateOutboundOrderIds);
            for (OrderItem item : updateOutboundOrder) {
                orderLogService.saveLog(item.getOrderId(), item.getOrderSn(), OrderLogEnum.SALE_ORDER_INVENTORY_OUT_FINISH.getCode(), null);
            }
        }
    }

    // TODO 出库单反馈取消
    @Override
    @Transactional
    public void callBackCancel(Long id) {
    }
}
