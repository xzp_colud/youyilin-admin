package com.youyilin.logic.manager.impl;

import com.youyilin.common.exception.Assert;
import com.youyilin.logic.manager.PurchaseOrderCallBackManager;
import com.youyilin.order.entity.OrderLog;
import com.youyilin.order.entity.PurchaseOrder;
import com.youyilin.order.entity.PurchaseOrderSource;
import com.youyilin.order.enums.OrderLogEnum;
import com.youyilin.order.enums.PurchaseOrderSourceTypeEnum;
import com.youyilin.order.service.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class PurchaseOrderCallBackManagerImpl implements PurchaseOrderCallBackManager {

    private final PurchaseOrderService purchaseOrderService;
    private final PurchaseOrderSourceService purchaseOrderSourceService;
    private final OrderLogService orderLogService;
    private final OrderService orderService;
    private final OrderItemService orderItemService;

    public PurchaseOrderCallBackManagerImpl(PurchaseOrderService purchaseOrderService, PurchaseOrderSourceService purchaseOrderSourceService, OrderLogService orderLogService, OrderService orderService, OrderItemService orderItemService) {
        this.purchaseOrderService = purchaseOrderService;
        this.purchaseOrderSourceService = purchaseOrderSourceService;
        this.orderLogService = orderLogService;
        this.orderService = orderService;
        this.orderItemService = orderItemService;
    }

    @Override
    @Transactional
    public void callBackCancel(Long id) {
        PurchaseOrder purchase = purchaseOrderService.getById(id);
        if (StringUtils.isBlank(purchase.getSourceType())) {
            return;
        }
        if (purchase.getSourceType().equals(PurchaseOrderSourceTypeEnum.DEFAULT.name())) {
            return;
        }
        if (purchase.getSourceType().equals(PurchaseOrderSourceTypeEnum.SALE_ORDER_READY.name())) {
            return;
        }
        if (purchase.getSourceType().equals(PurchaseOrderSourceTypeEnum.SALE_ORDER.name())) {
            List<PurchaseOrderSource> list = purchaseOrderSourceService.listByPurchaseIdAndType(id, purchase.getSourceType());
            if (CollectionUtils.isEmpty(list)) {
                return;
            }
            // 删除
            purchaseOrderSourceService.delByPurchaseIdAndType(id, purchase.getSourceType());
            // 源单日志
            List<OrderLog> logList = new ArrayList<>();
            for (PurchaseOrderSource item : list) {
                OrderLog log = new OrderLog();
                log.setOrderId(item.getSourceId())
                        .setSn(item.getSourceSn())
                        .setType(OrderLogEnum.SALE_ORDER_TO_PURCHASE_ORDER_CANCEL.getCode());

                logList.add(log);
            }

            orderLogService.saveBatch(logList);
        }
    }

    @Override
    @Transactional
    public void callBackFinish(Long id) {
        PurchaseOrder purchase = purchaseOrderService.getById(id);
        // 是否为大货采购
        boolean isReadyPurchase = StringUtils.isNotBlank(purchase.getSourceType()) && purchase.getSourceType().equals(PurchaseOrderSourceTypeEnum.SALE_ORDER_READY.name());
        if (isReadyPurchase) {
            List<PurchaseOrderSource> sourceList = purchaseOrderSourceService.listByPurchaseIdAndType(id, purchase.getSourceType());
            Assert.notEmpty(sourceList, "来源明细异常");
            for (PurchaseOrderSource item : sourceList) {
                // 备货采购完成
                orderService.updateReadFinish(item.getSourceId());
                // 备货采购完成
                orderItemService.updateReadFinish(item.getSourceId());
                // 日志
                orderLogService.saveLog(item.getSourceId(), item.getSourceSn(), OrderLogEnum.SALE_ORDER_READY_FINISH.getCode());
            }
        }
    }
}
