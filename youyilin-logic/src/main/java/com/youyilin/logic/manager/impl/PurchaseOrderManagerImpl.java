package com.youyilin.logic.manager.impl;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.youyilin.common.exception.Assert;
import com.youyilin.goods.entity.Product;
import com.youyilin.goods.entity.ProductPrice;
import com.youyilin.goods.service.ProductPriceService;
import com.youyilin.goods.service.ProductService;
import com.youyilin.logic.manager.PurchaseOrderManager;
import com.youyilin.order.bo.purchase.PurchaseOrderThirdConstructBO;
import com.youyilin.order.dto.purchase.PurchaseOrderItemDetailThirdConstructDTO;
import com.youyilin.order.entity.PurchaseOrder;
import com.youyilin.order.entity.PurchaseOrderItem;
import com.youyilin.order.entity.PurchaseOrderSource;
import com.youyilin.order.enums.OrderLogEnum;
import com.youyilin.order.enums.PurchaseOrderItemStatusEnum;
import com.youyilin.order.enums.PurchaseOrderStatusEnum;
import com.youyilin.order.service.OrderLogService;
import com.youyilin.order.service.PurchaseOrderItemService;
import com.youyilin.order.service.PurchaseOrderService;
import com.youyilin.order.service.PurchaseOrderSourceService;
import com.youyilin.order.utils.NoUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

@Service
public class PurchaseOrderManagerImpl implements PurchaseOrderManager {

    private final PurchaseOrderService purchaseOrderService;
    private final PurchaseOrderItemService purchaseOrderItemService;
    private final OrderLogService orderLogService;
    private final PurchaseOrderSourceService purchaseOrderSourceService;
    private final ProductService productService;
    private final ProductPriceService productPriceService;

    public PurchaseOrderManagerImpl(PurchaseOrderService purchaseOrderService, PurchaseOrderItemService purchaseOrderItemService,
                                    OrderLogService orderLogService, PurchaseOrderSourceService purchaseOrderSourceService,
                                    ProductService productService, ProductPriceService productPriceService) {
        this.purchaseOrderService = purchaseOrderService;
        this.purchaseOrderItemService = purchaseOrderItemService;
        this.orderLogService = orderLogService;
        this.purchaseOrderSourceService = purchaseOrderSourceService;
        this.productService = productService;
        this.productPriceService = productPriceService;
    }

    /**
     * 创建出库单(由系统内部其他订单转换而来,非手动创建)
     */
    @Override
    @Transactional
    public void constructPurchaseOrder(PurchaseOrderThirdConstructBO bo) {
        // 验证参数
        this.validatePurchaseOrderInit(bo);
        // 创建采购单
        this.createPurchaseOrder(bo);
        // 刷新采购明细
        this.refreshPurchaseOrderItem(bo);
        // 持久化
        this.savePurchaseOrder(bo);
    }

    private void validatePurchaseOrderInit(PurchaseOrderThirdConstructBO bo) {
        List<PurchaseOrderItemDetailThirdConstructDTO> sourceList = bo.getSourceList();
        Assert.notEmpty(sourceList, "采购明细不能为空");
        Assert.hasLength(bo.getSourceType(), "来源不明确");
        Set<Long> productIds = new HashSet<>();
        Set<Long> skuIds = new HashSet<>();
        for (PurchaseOrderItemDetailThirdConstructDTO item : sourceList) {
            Assert.notNull(item.getProductId(), "采购商品不能为空");
            Assert.notNull(item.getSkuId(), "采购商品SKU不能为空");
            Assert.notNull(item.getBuyNum(), "采购数量不能为空");
            Assert.isTrue(item.getBuyNum().compareTo(BigDecimal.ZERO) > 0, "采购数量不能为空");
            productIds.add(item.getProductId());
            skuIds.add(item.getSkuId());
        }
        List<PurchaseOrderItem> itemList = new ArrayList<>();
        Map<Long, Product> productMap = productService.mapByIds(new ArrayList<>(productIds));
        Map<Long, ProductPrice> PriceMap = productPriceService.mapByIds(new ArrayList<>(skuIds));

        Long totalAmount = 0L;
        for (PurchaseOrderItemDetailThirdConstructDTO item : sourceList) {
            // 断言：商品
            Product product = productMap.get(item.getProductId());
            Assert.notNull(product, "采购商品不存在");
            // 断言：sku
            ProductPrice price = PriceMap.get(item.getSkuId());
            Assert.notNull(price, "采购商品不存在");
            // 数据转换创建采购明细
            PurchaseOrderItem orderItem = convertToPurchaseOrderItem(item, product, price);
            // 设置订单总金额
            totalAmount += orderItem.getTotalAmount();
            itemList.add(orderItem);
        }

        bo.setTotalAmount(totalAmount).setItemList(itemList);
    }

    private PurchaseOrderItem convertToPurchaseOrderItem(PurchaseOrderItemDetailThirdConstructDTO dto, Product product, ProductPrice price) {
        PurchaseOrderItem item = new PurchaseOrderItem();
        Long totalAmount = dto.getBuyNum().multiply(new BigDecimal(price.getCostPrice())).longValue();
        item.setProductId(product.getId())
                .setProductName(product.getName())
                .setCategoryId(product.getCategoryId())
                .setCategoryName(product.getCategoryName())
                .setSupplierId(product.getSupplierId())
                .setSupplierName(product.getSupplierName())
                .setProductPriceId(price.getId())
                .setSkuName(price.getSkuName())
                .setImageUrl(product.getImageUrl())
                .setStatus(PurchaseOrderItemStatusEnum.NO_CONFIRM.getCode())
                .setBuyNum(dto.getBuyNum())
                .setDiscount(100)
                .setWholesalePriceFlag(0)
                .setPrice(price.getCostPrice())
                .setAmount(price.getCostPrice())
                .setTotalDiscount(0L)
                .setTotalAmount(totalAmount)
                .setTotalReallyAmount(totalAmount);

        return item;
    }

    private void createPurchaseOrder(PurchaseOrderThirdConstructBO bo) {
        PurchaseOrder order = new PurchaseOrder();
        order.setId(IdWorker.getId())
                .setSn(NoUtils.getPurchaseOrderSn())
                .setSourceType(bo.getSourceType())
                .setStatus(PurchaseOrderStatusEnum.NO_CONFIRM.getCode())
                .setTotalAmount(bo.getTotalAmount())
                .setTotalMoney(bo.getTotalAmount())
                .setOtherAmount(0L)
                .setPurchaseDate(new Date())
                .setBillDate(new Date());

        bo.setPurchaseOrder(order);
    }

    private void refreshPurchaseOrderItem(PurchaseOrderThirdConstructBO bo) {
        Long purchaseOrderId = bo.getPurchaseOrder().getId();
        String purchaseOrderSn = bo.getPurchaseOrder().getSn();
        for (int i = 0; i < bo.getItemList().size(); i++) {
            int index = i + 1;
            PurchaseOrderItem orderItem = bo.getItemList().get(i);
            orderItem.setOrderId(purchaseOrderId)
                    .setSn(purchaseOrderSn)
                    .setItemSn(purchaseOrderSn + "-" + index);
        }
    }

    private void savePurchaseOrder(PurchaseOrderThirdConstructBO bo) {
        // 持久化采购单
        purchaseOrderService.save(bo.getPurchaseOrder());
        // 持久化采购明细
        purchaseOrderItemService.saveBatch(bo.getItemList());
        // 日志
        orderLogService.saveLog(bo.getPurchaseOrder().getId(), bo.getPurchaseOrder().getSn(), OrderLogEnum.SALE_ORDER_TO_PURCHASE_ORDER.getCode());
        // 持久来源明细
        PurchaseOrderSource source = new PurchaseOrderSource();
        source.setOrderId(bo.getPurchaseOrder().getId())
                .setSourceId(bo.getSourceId())
                .setSourceSn(bo.getSourceSn())
                .setSourceType(bo.getSourceType());
        purchaseOrderSourceService.save(source);
    }
}
