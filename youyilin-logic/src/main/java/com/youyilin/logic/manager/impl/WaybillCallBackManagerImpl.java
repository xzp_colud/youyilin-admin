package com.youyilin.logic.manager.impl;

import com.youyilin.logic.manager.WaybillCallBackManager;
import com.youyilin.order.entity.CustomerOrderItem;
import com.youyilin.order.entity.OrderItem;
import com.youyilin.order.entity.OutboundOrderInventoryLog;
import com.youyilin.order.entity.OutboundOrderItemDetail;
import com.youyilin.order.enums.OrderLogEnum;
import com.youyilin.order.service.*;
import com.youyilin.warehouse.entity.InventoryLog;
import com.youyilin.warehouse.service.InventoryLogService;
import com.youyilin.waybill.entity.Waybill;
import com.youyilin.waybill.entity.WaybillItem;
import com.youyilin.waybill.enums.WaybillSourceType;
import com.youyilin.waybill.service.WaybillItemService;
import com.youyilin.waybill.service.WaybillService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class WaybillCallBackManagerImpl implements WaybillCallBackManager {

    private final WaybillService waybillService;
    private final WaybillItemService waybillItemService;
    private final OrderService orderService;
    private final OrderItemService orderItemService;
    private final CustomerOrderService customerOrderService;
    private final CustomerOrderItemService customerOrderItemService;
    private final OrderLogService orderLogService;
    private final OutboundOrderItemDetailService outboundOrderItemDetailService;
    private final OutboundOrderInventoryLogService outboundOrderInventoryLogService;
    private final InventoryLogService inventoryLogService;

    public WaybillCallBackManagerImpl(WaybillService waybillService, WaybillItemService waybillItemService, OrderService orderService,
                                      OrderItemService orderItemService, CustomerOrderService customerOrderService,
                                      CustomerOrderItemService customerOrderItemService, OrderLogService orderLogService,
                                      OutboundOrderItemDetailService outboundOrderItemDetailService, OutboundOrderInventoryLogService outboundOrderInventoryLogService,
                                      InventoryLogService inventoryLogService) {
        this.waybillService = waybillService;
        this.waybillItemService = waybillItemService;
        this.orderService = orderService;
        this.orderItemService = orderItemService;
        this.customerOrderService = customerOrderService;
        this.customerOrderItemService = customerOrderItemService;
        this.orderLogService = orderLogService;
        this.outboundOrderItemDetailService = outboundOrderItemDetailService;
        this.outboundOrderInventoryLogService = outboundOrderInventoryLogService;
        this.inventoryLogService = inventoryLogService;
    }

    @Override
    @Transactional
    public void callBackSend(Long waybillId) {
        Waybill waybill = waybillService.getById(waybillId);
        if (StringUtils.isBlank(waybill.getSourceType())) {
            return;
        }
        if (waybill.getSourceType().equals(WaybillSourceType.CUSTOMER_ORDER.name())) {
            this.doFeedBackCustomerOrder(waybill);
            return;
        }
        if (waybill.getSourceType().equals(WaybillSourceType.SALE_ORDER.name())) {
            this.doFeedBackSaleOrder(waybill);
        }
    }

    private Set<Long> initSourceIds(Waybill waybill) {
        List<WaybillItem> itemList = waybillItemService.listByWaybillId(waybill.getId());
        if (CollectionUtils.isEmpty(itemList)) {
            return new HashSet<>();
        }
        Set<Long> sourceIds = new HashSet<>();
        for (WaybillItem item : itemList) {
            sourceIds.add(item.getSourceId());
        }
        return sourceIds;
    }

    private void doFeedBackCustomerOrder(Waybill waybill) {
        Set<Long> customerItemIds = this.initSourceIds(waybill);
        if (CollectionUtils.isEmpty(customerItemIds)) {
            return;
        }
        // 反馈发货信息
        List<Long> orderItemIds = new ArrayList<>(customerItemIds);
        List<CustomerOrderItem> orderItemList = customerOrderItemService.listByIds(customerItemIds);
        if (CollectionUtils.isEmpty(orderItemList)) {
            return;
        }
        Set<Long> customerOrderIds = new HashSet<>();
        for (CustomerOrderItem item : orderItemList) {
            customerOrderIds.add(item.getOrderId());
        }
        // 添加物流信息
        customerOrderService.updateStatusSend(new ArrayList<>(customerOrderIds), waybill.getExpressNo(), waybill.getExpressName(), waybill.getSureDate());

        // 清理冻结库存
        this.doClearOutboundOrderInventory(orderItemIds);
    }

    private void doFeedBackSaleOrder(Waybill waybill) {
        // 销售单子集IDS
        Set<Long> saleOrderItemIds = this.initSourceIds(waybill);
        if (CollectionUtils.isEmpty(saleOrderItemIds)) {
            return;
        }
        List<Long> orderItemIds = new ArrayList<>(saleOrderItemIds);
        // 反馈发货信息
        List<OrderItem> orderItemList = orderItemService.listByIds(saleOrderItemIds);
        if (CollectionUtils.isEmpty(orderItemList)) {
            return;
        }
        Set<Long> salesOrderIds = new HashSet<>();
        for (OrderItem item : orderItemList) {
            salesOrderIds.add(item.getOrderId());
        }
        // 更新订单状态为已发货
        orderService.updateSendFinish(new ArrayList<>(salesOrderIds));
        // 更新订单子集状态为已发货
        orderItemService.updateSendFinish(orderItemIds);
        // 日志
        for (OrderItem item : orderItemList) {
            orderLogService.saveLog(item.getOrderId(), item.getOrderSn(), OrderLogEnum.SALE_ORDER_SEND_FINISH.getCode(), null);
        }
        // 清理冻结库存
//        this.doClearOutboundOrderInventory(orderItemIds);
    }

    private void doClearOutboundOrderInventory(List<Long> sourceIds) {
        // 查询出库明细
        List<OutboundOrderItemDetail> outboundOrderItemDetailList = outboundOrderItemDetailService.listBySourceIds(sourceIds);
        if (CollectionUtils.isEmpty(outboundOrderItemDetailList)) {
            return;
        }
        List<Long> outboundItemIds = new ArrayList<>();
        for (OutboundOrderItemDetail item : outboundOrderItemDetailList) {
            outboundItemIds.add(item.getOrderItemId());
        }
        // 查询出库信息
        List<OutboundOrderInventoryLog> inventoryLogList = outboundOrderInventoryLogService.listByOutboundItemIds(outboundItemIds);
        List<InventoryLog> clearList = new ArrayList<>();
        // 清理冻结库存
        for (OutboundOrderInventoryLog item : inventoryLogList) {
//            InventoryLog clearItem = inventoryLogService.createInventoryLog(item.getWarehouseId(), item.getWarehouseAreaId(), item.getProductId(), item.getSkuId(), item.getSn(), item.getQty(), 0L);
//            clearList.add(clearItem);
        }
        if (CollectionUtils.isEmpty(clearList)) {
            return;
        }
        // 执行清理冻结库存
        inventoryLogService.clearFrozenQty(clearList);
    }
}
