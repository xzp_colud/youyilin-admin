package com.youyilin.logic.manager;

import com.youyilin.order.bo.purchase.PurchaseOrderThirdConstructBO;

public interface PurchaseOrderManager {

    /**
     * 内部订单转换成采购单
     *
     * @param bo 采购单BO
     */
    void constructPurchaseOrder(PurchaseOrderThirdConstructBO bo);
}
