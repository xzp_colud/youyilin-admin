package com.youyilin.logic.manager;

import com.youyilin.waybill.bo.WaybillThirdConstructBO;

public interface WaybillManager {

    /**
     * 内部订单转换成货运单
     *
     * @param bo 货运单BO
     */
    void constructWaybill(WaybillThirdConstructBO bo);
}
