package com.youyilin.logic.manager;

import com.youyilin.order.bo.outbound.OutboundOrderThirdConstructBO;

public interface OutboundOrderManager {

    /**
     * 内部订单转换成出库单
     *
     * @param bo 出库单BO
     */
    void constructOutboundOrder(OutboundOrderThirdConstructBO bo);
}
