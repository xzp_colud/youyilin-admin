package com.youyilin.logic.manager;

public interface WaybillCallBackManager {

    /**
     * 反馈发货
     *
     * @param waybillId 货运单ID
     */
    void callBackSend(Long waybillId);
}
