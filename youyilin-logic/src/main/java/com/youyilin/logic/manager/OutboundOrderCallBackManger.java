package com.youyilin.logic.manager;

public interface OutboundOrderCallBackManger {

    /**
     * 出库单出库反馈
     *
     * @param outboundId     出库单ID
     * @param outboundItemId 出库单明细ID
     */
    void callBackInventory(Long outboundId, Long outboundItemId);

    /**
     * 出库单反馈取消
     */
    void callBackCancel(Long id);
}
