package com.youyilin.logic.service.product;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.goods.dto.product.ProductPageQueryDTO;
import com.youyilin.goods.vo.product.ProductAddVO;
import com.youyilin.goods.vo.product.ProductDetailVO;
import com.youyilin.goods.vo.product.ProductEditVO;
import com.youyilin.goods.vo.product.ProductPageVO;

public interface ProductNewLogic {

    /**
     * 获取分页列表
     */
    Pager<ProductPageVO> getPageList(Page<ProductPageQueryDTO> page);

    /**
     * 获取商品新增线下
     *
     * @param categoryId 分类ID
     * @return ProductAddVO
     */
    ProductAddVO getAddVOByCategoryId(Long categoryId);

    /**
     * 获取商品编辑信息
     *
     * @param id 商品ID
     * @return ProductEditVO
     */
    ProductEditVO getEditVOById(Long id);

    /**
     * 获取商品详情信息
     *
     * @param id 商品ID
     * @return ProductDetailVO
     */
    ProductDetailVO getDetailVOById(Long id);
}
