package com.youyilin.logic.service.customer;

import com.youyilin.customer.dto.request.RequestMiniCart;
import com.youyilin.customer.vo.CustomerCartVo;

import java.util.List;

public interface CustomerMiniCartLogic {

    /**
     * 查询购物车列表
     *
     * @return ArrayList
     */
    List<CustomerCartVo> listCart();

    /**
     * 统计当前数量
     *
     * @return Long
     */
    Long countCart();

    /**
     * 保存
     */
    void saveProductToCart(RequestMiniCart request);

    /**
     * 删除
     */
    void delByIds(List<Long> ids);
}
