package com.youyilin.logic.service.warehouse;

import com.youyilin.warehouse.dto.InventoryLot.InventoryLotCheckFormDTO;
import com.youyilin.warehouse.dto.InventoryLot.InventoryLotDeleteFormDTO;
import com.youyilin.warehouse.dto.InventoryLot.InventoryLotMoveFormDTO;
import com.youyilin.warehouse.dto.InventoryLot.InventoryLotSaveFormDTO;
import com.youyilin.warehouse.vo.inventoryLot.InventoryLotPageVO;

import java.util.List;

public interface InventoryLotLogic {

    /**
     * 按库存获取库存批次列表
     *
     * @param inventoryId 库存ID
     * @return ArrayList
     */
    List<InventoryLotPageVO> listByInventoryId(Long inventoryId);

    /**
     * 盘点新增
     */
    void saveInventoryLot(InventoryLotSaveFormDTO dto);

    /**
     * 库存批次盘点出库
     */
    void updateInventoryLotCheckOut(InventoryLotCheckFormDTO dto);

    /**
     * 库存批次盘点入库
     */
    void updateInventoryLotCheckIn(InventoryLotCheckFormDTO dto);

    /**
     * 库存批次移库
     */
    void updateInventoryLotMove(InventoryLotMoveFormDTO dto);

    /**
     * 库存批次删除
     */
    void delInventoryLot(InventoryLotDeleteFormDTO dto);
}
