package com.youyilin.logic.service.order.sales;

import com.youyilin.order.vo.sales.OrderDiscountPageVO;

import java.util.List;

public interface OrderDiscountLogic {

    /**
     * 根据订单id查询优惠明细
     *
     * @param orderId 订单id
     * @return ArrayList
     */
    List<OrderDiscountPageVO> listByOrderId(Long orderId);
}
