package com.youyilin.logic.service.order;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.order.dto.OrderLogPageQueryDTO;
import com.youyilin.order.vo.OrderLogPageVO;

public interface OrderLogLogic {

    /**
     * 获取分页列表
     */
    Pager<OrderLogPageVO> getPageList(Page<OrderLogPageQueryDTO> page);
}
