package com.youyilin.logic.service.order.settled;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.order.dto.settled.*;
import com.youyilin.order.vo.settled.SettledOrderDetailVO;
import com.youyilin.order.vo.settled.SettledOrderPageVO;

public interface SettledOrderLogic {

    /**
     * 分页列表
     */
    Pager<SettledOrderPageVO> getPageList(Page<SettledOrderPageQueryDTO> page);

    /**
     * 获取上期结余
     *
     * @param customerId 客户ID
     * @return String
     */
    String getLastBalanceByCustomerId(Long customerId);

    /**
     * 获取结款单详情
     */
    SettledOrderDetailVO getDetailVOById(Long id);

    /**
     * 保存
     */
    void saveSettled(SettledOrderFormDTO dto);

    /**
     * 确认
     */
    void updateConfirm(SettledOrderConfirmFormDTO dto);

    /**
     * 取消
     */
    void updateCancel(SettledOrderCancelFormDTO dto);

    /**
     * 审核
     */
    void updateFinish(SettledOrderFinishFormDTO dto);
}
