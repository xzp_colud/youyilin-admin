package com.youyilin.logic.service.order.outbound;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.order.dto.outbound.*;
import com.youyilin.order.vo.outbound.OutboundOrderDetailVO;
import com.youyilin.order.vo.outbound.OutboundOrderEditVO;
import com.youyilin.order.vo.outbound.OutboundOrderPageVO;

public interface OutboundOrderLogic {

    /**
     * 获取分页列表
     */
    Pager<OutboundOrderPageVO> getPageList(Page<OutboundOrderPageQueryDTO> page);

    /**
     * 获取出库单详情信息
     */
    OutboundOrderDetailVO getDetailVOById(Long id);

    /**
     * 获取出库单编辑信息
     */
    OutboundOrderEditVO getEditVOById(Long id);

    /**
     * 保存
     */
    void saveOutbound(OutboundOrderFormDTO dto);

    /**
     * 确认
     */
    void updateConfirm(OutboundOrderConfirmFormDTO dto);

    /**
     * 出库
     */
    void updateInventory(OutboundOrderInventoryFormDTO dto);

    /**
     * 取消
     */
    void updateCancel(OutboundOrderCancelFormDTO dto);
}
