package com.youyilin.logic.service.order.mini;

import com.youyilin.order.model.request.RequestMiniOrder;
import com.youyilin.order.model.request.RequestMiniOrderAddress;
import com.youyilin.order.model.request.RequestMiniOrderSend;
import com.youyilin.order.model.vo.CustomerOrderVo;
import com.youyilin.wechat.model.response.JsApiResponse;

import java.util.List;

public interface CustomerMiniOrderLogic {

    /**
     * 列表
     *
     * @param current  当前页
     * @param pageSize 每页数量
     * @return ArrayList
     */
    List<CustomerOrderVo> getPage(Integer current, Integer pageSize, Integer status);

    /**
     * 查询订单
     *
     * @param orderId 订单ID
     * @return CustomerOrderVo
     */
    CustomerOrderVo getOrder(Long orderId);

    /**
     * 保存订单
     */
    String saveOrder(RequestMiniOrder request);

    /**
     * 订单取消
     */
    void cancel(Long orderId);

    /**
     * 付款
     */
    JsApiResponse pay(Long orderId);

    /**
     * 付款成功
     */
    void payCancel(Long orderId);

    /**
     * 付款成功
     *
     * @param paymentId      付款单
     * @param transactionId  流水号
     * @param paySuccessTime 付款时间
     * @param typeName       支付方式
     * @param outTradeNo     请求单号
     */
    void paySuccess(Long paymentId, String transactionId, String paySuccessTime, String typeName, String outTradeNo);

    /**
     * 收货
     *
     * @param id 订单ID
     */
    void receive(Long id);

    /**
     * 转出库单
     *
     * @param orderIds 订单
     */
    void outbound(List<Long> orderIds);

    /**
     * 发货
     *
     * @param request request
     */
    void send(RequestMiniOrderSend request);

    /**
     * 修改地址
     *
     * @param request
     */
    void updateAddress(RequestMiniOrderAddress request);
}
