package com.youyilin.logic.service.warehouse;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.warehouse.dto.warehouseArea.WarehouseAreaDeleteFormDTO;
import com.youyilin.warehouse.dto.warehouseArea.WarehouseAreaFormDTO;
import com.youyilin.warehouse.dto.warehouseArea.WarehouseAreaPageQueryDTO;
import com.youyilin.warehouse.vo.warehouseArea.WarehouseAreaEditVO;
import com.youyilin.warehouse.vo.warehouseArea.WarehouseAreaPageVO;
import com.youyilin.warehouse.vo.warehouseArea.WarehouseAreaVO;

import java.util.List;

public interface WarehouseAreaLogic {

    /**
     * 获取分页列表
     */
    Pager<WarehouseAreaPageVO> getPageList(Page<WarehouseAreaPageQueryDTO> page);

    /**
     * 获取所有仓库
     *
     * @return ArrayList
     */
    List<WarehouseAreaVO> listAll();

    /**
     * 获取所有仓库
     *
     * @return ArrayList
     */
    List<WarehouseAreaVO> listAllByWarehouseId(Long warehouseId);

    /**
     * 获取编辑VO
     *
     * @param id ID
     * @return WarehouseEditVO
     */
    WarehouseAreaEditVO getEditVOById(Long id);

    /**
     * 保存
     */
    void saveWarehouseArea(WarehouseAreaFormDTO dto);

    /**
     * 删除
     */
    void delWarehouseArea(WarehouseAreaDeleteFormDTO dto);
}
