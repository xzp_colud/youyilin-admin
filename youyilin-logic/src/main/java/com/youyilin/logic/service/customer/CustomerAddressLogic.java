package com.youyilin.logic.service.customer;

import com.youyilin.customer.dto.address.CustomerAddressDefaultDTO;
import com.youyilin.customer.dto.address.CustomerAddressDeleteDTO;
import com.youyilin.customer.dto.address.CustomerAddressFormDTO;
import com.youyilin.customer.vo.address.CustomerAddressEditVO;
import com.youyilin.customer.vo.address.CustomerAddressPageVO;

import java.util.List;

public interface CustomerAddressLogic {

    /**
     * 获取客户的收货地址列表
     *
     * @param customerId 客户ID
     * @return ArrayList
     */
    List<CustomerAddressPageVO> listByCustomerId(Long customerId);

    /**
     * 获取收货地址编辑VO
     *
     * @param id 收货地址ID
     * @return CustomerAddressEditVO
     */
    CustomerAddressEditVO getEditVOById(Long id);

    /**
     * 保存
     */
    void saveAddress(CustomerAddressFormDTO dto);

    /**
     * 设置默认
     */
    void updateDefault(CustomerAddressDefaultDTO dto);

    /**
     * 删除
     */
    void delAddress(CustomerAddressDeleteDTO dto);
}
