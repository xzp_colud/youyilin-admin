package com.youyilin.logic.service.customer;

import com.youyilin.customer.dto.address.CustomerMiniAddressFormDTO;
import com.youyilin.customer.vo.address.CustomerAddressDetailVO;
import com.youyilin.customer.vo.address.CustomerAddressEditVO;
import com.youyilin.customer.vo.address.CustomerAddressPageVO;

import java.util.List;

public interface CustomerMiniAddressLogic {

    /**
     * 列表
     */
    List<CustomerAddressPageVO> listAddress();

    /**
     * 编辑查询
     */
    CustomerAddressEditVO getAddress(Long id);

    /**
     * 获取默认地址
     */
    CustomerAddressDetailVO getDefault();

    /**
     * 保存
     */
    void saveAddress(CustomerMiniAddressFormDTO request);

    /**
     * 设置默认
     */
    void updateDefault(Long id);

    /**
     * 删除
     */
    void delAddress(Long id);
}
