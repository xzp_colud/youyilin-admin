package com.youyilin.logic.service.order.purchase;

import com.youyilin.order.dto.purchase.PurchaseItemReviseNumDTO;
import com.youyilin.order.vo.purchase.PurchaseOrderItemPageVO;

import java.util.List;

public interface PurchaseOrderItemLogic {

    /**
     * 根据采购单id查询采购单明细
     *
     * @param purchaseOrderId 采购单id
     * @return ArrayList
     */
    List<PurchaseOrderItemPageVO> listByPurchaseOrderId(Long purchaseOrderId);

    /**
     * 修正实际检验的米数
     */
    void updateReviseNum(PurchaseItemReviseNumDTO dto);
}
