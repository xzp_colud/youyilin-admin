package com.youyilin.logic.service.warehouse;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.warehouse.dto.inventory.InventoryDeleteFormDTO;
import com.youyilin.warehouse.dto.inventory.InventoryMoveFormDTO;
import com.youyilin.warehouse.dto.inventory.InventoryPageQueryDTO;
import com.youyilin.warehouse.vo.inventory.InventoryCheckVO;
import com.youyilin.warehouse.vo.inventory.InventoryErrorVO;
import com.youyilin.warehouse.vo.inventory.InventoryPageVO;
import com.youyilin.warehouse.vo.inventory.InventoryVO;

import java.util.List;

public interface InventoryLogic {

    /**
     * 获取分页列表
     */
    Pager<InventoryPageVO> getPageList(Page<InventoryPageQueryDTO> page);

    /**
     * 获取分页列表
     */
    List<InventoryErrorVO> listError();

    /**
     * 按商品名称精准查询
     *
     * @param productName 商品名称
     * @return ArrayList
     */
    List<InventoryCheckVO> listByProductName(String productName);

    /**
     * 按商品名称模糊查询
     *
     * @param productName 商品名称
     * @return ArrayList
     */
    List<InventoryCheckVO> listLikeByProductName(String productName);

    /**
     * 入库按商品查询
     *
     * @param productId 商品ID
     * @return ArrayList
     */
    List<InventoryVO> queryIntoByProductId(Long productId);

    /**
     * 出库按SKU查询
     *
     * @param skuId skuId
     * @return ArrayList
     */
    List<InventoryVO> queryOutBySkuId(Long skuId);

    /**
     * 库存同步
     */
    void asyncInventory();

    /**
     * 删除库存
     */
    void delInventory(InventoryDeleteFormDTO dto);

    /**
     * 移库
     */
    void updateInventoryMove(InventoryMoveFormDTO dto);
}
