package com.youyilin.logic.service;

import com.youyilin.logic.vo.DashboardCustomerCardVO;

public interface DashboardLogic {

    /**
     * 获取控制面板客户数据
     */
    DashboardCustomerCardVO getDashboardCustomerCard();
}
