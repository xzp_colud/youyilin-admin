package com.youyilin.logic.service.order.purchase;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.order.dto.purchase.*;
import com.youyilin.order.vo.purchase.PurchaseOrderDetailVO;
import com.youyilin.order.vo.purchase.PurchaseOrderEditVO;
import com.youyilin.order.vo.purchase.PurchaseOrderPageVO;

public interface PurchaseOrderLogic {

    /**
     * 获取分页列表
     */
    Pager<PurchaseOrderPageVO> getPageList(Page<PurchaseOrderPageQueryDTO> page);

    /**
     * 获取采购单详情VO
     *
     * @param id 采购单id
     * @return PurchaseOrderDetailVO
     */
    PurchaseOrderDetailVO getDetailVOById(Long id);

    /**
     * 获取采购单编辑VO
     *
     * @param id 采购单id
     * @return PurchaseOrderEditVO
     */
    PurchaseOrderEditVO getEditVOById(Long id);

    /**
     * 保存
     */
    void savePurchaseOrder(PurchaseOrderFormDTO dto);

    /**
     * 确认
     */
    void updateConfirm(PurchaseOrderConfirmFormDTO dto);

    /**
     * 检验
     */
    void updateCheck(PurchaseOrderCheckFormDTO dto);

    /**
     * 批量入库
     */
    void updateInventoryBatch(PurchaseOrderInventoryBatchFormDTO dto);

    /**
     * 入库
     */
    void updateInventory(PurchaseOrderInventoryFormDTO dto);

    /**
     * 取消
     */
    void updateCancel(PurchaseOrderCancelFormDTO dto);
}
