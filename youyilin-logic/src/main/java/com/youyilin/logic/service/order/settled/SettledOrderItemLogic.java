package com.youyilin.logic.service.order.settled;

import com.youyilin.order.vo.settled.SettledOrderItemPageVO;

import java.util.List;

public interface SettledOrderItemLogic {

    /**
     * 根据结款单id查询结款单明细
     *
     * @param settledId 结款单ID
     * @return ArrayList
     */
    List<SettledOrderItemPageVO> listBySettledId(Long settledId);
}
