package com.youyilin.logic.service.order.sales;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.order.dto.sales.*;
import com.youyilin.order.vo.sales.*;

import java.util.List;

public interface OrderLogic {

    /**
     * 分页列表
     */
    Pager<OrderPageVO> getPageList(Page<OrderPageQueryDTO> page);

    /**
     * 获取编辑信息
     *
     * @param id 销售单ID
     */
    OrderEditVO getEditVOById(Long id);

    /**
     * 获取详情信息
     *
     * @param id 销售单ID
     */
    OrderDetailVO getDetailVOById(Long id);

    /**
     * 获取修改收货地址信息
     *
     * @param id 销售单ID
     * @return OrderUpdateAddressVO
     */
    OrderUpdateAddressVO getUpdateAddressVOById(Long id);

    /**
     * 保存订单
     */
    void saveOrder(OrderFormDTO dto);

    /**
     * 确认(提交)订单
     */
    void updateSubmit(OrderSubmitFormDTO dto);

    /**
     * 修改订单状态为已支付
     */
    void updatePay(OrderPayFormDTO dto);

    /**
     * 修改订单状态为备货入库中
     */
    void updateReadyPurchaseProcessing(OrderReadyPurchaseFormDTO dto);

    /**
     * 修改订单状态为出库中
     */
    void updateInventoryOut(OrderInventoryFormDTO dto);

    /**
     * 修改订单状态为发货中
     */
    void updateSend(OrderSendFormDTO dto);

    /**
     * 修改订单状态为已取消
     */
    void updateCancel(OrderCancelFormDTO dto);

    /**
     * 修改订单的收货地址
     */
    void updateAddress(OrderAddressFormDTO dto);

    /**
     * 获取结款列表
     *
     * @return ArrayList
     */
    List<OrderSettledPageVO> listSettledPage();

    /**
     * 获取销售转采购之前的查询
     *
     * @param id 销售单ID
     * @return ArrayList
     */
    List<OrderToPurchaseQueryQtyVO> getPurchaseInfo(Long id);

    /**
     * 销售订单转原料采购订单
     */
    void doSalesOrderToPurchaseOrder(OrderPurchaseFormDTO dto);

    /**
     * 销售单转备货采购信息查询
     *
     * @param id 销售单ID
     * @return ArrayList
     */
    List<OrderToPurchaseQueryQtyVO> getReadyPurchaseInfo(Long id);
}
