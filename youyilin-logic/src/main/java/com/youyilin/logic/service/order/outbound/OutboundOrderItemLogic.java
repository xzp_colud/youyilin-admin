package com.youyilin.logic.service.order.outbound;

import com.youyilin.order.vo.outbound.OutboundOrderItemPageVO;

import java.util.List;

public interface OutboundOrderItemLogic {

    /**
     * 根据出库单获取商品明细
     *
     * @param outboundId 出库单ID
     * @return ArrayList
     */
    List<OutboundOrderItemPageVO> listByOutboundId(Long outboundId);
}
