package com.youyilin.logic.service.product;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.goods.dto.supplier.SupplierFormDTO;
import com.youyilin.goods.dto.supplier.SupplierPageQueryDTO;
import com.youyilin.goods.vo.supplier.SupplierPageVO;
import com.youyilin.goods.vo.supplier.SupplierVO;

import java.util.List;

public interface SupplierLogic {

    /**
     * 获取分页列表
     */
    Pager<SupplierPageVO> getPageList(Page<SupplierPageQueryDTO> page);

    /**
     * 获取供货商
     */
    SupplierVO getVOById(Long id);

    /**
     * 获取所有供货商
     */
    List<SupplierVO> listAll();

    /**
     * 保存
     */
    void saveSupplier(SupplierFormDTO dto);
}
