package com.youyilin.logic.service.warehouse;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.warehouse.dto.warehouse.WarehouseDeleteFormDTO;
import com.youyilin.warehouse.dto.warehouse.WarehouseFormDTO;
import com.youyilin.warehouse.dto.warehouse.WarehousePageQueryDTO;
import com.youyilin.warehouse.vo.warehouse.WarehouseEditVO;
import com.youyilin.warehouse.vo.warehouse.WarehousePageVO;
import com.youyilin.warehouse.vo.warehouse.WarehouseVO;

import java.util.List;

public interface WarehouseLogic {

    /**
     * 获取分页列表
     */
    Pager<WarehousePageVO> getPageList(Page<WarehousePageQueryDTO> page);

    /**
     * 获取所有仓库
     *
     * @return ArrayList
     */
    List<WarehouseVO> listAll();

    /**
     * 获取编辑VO
     *
     * @param id ID
     * @return WarehouseEditVO
     */
    WarehouseEditVO getEditVOById(Long id);

    /**
     * 保存
     */
    void saveWarehouse(WarehouseFormDTO dto);

    /**
     * 删除
     */
    void delWarehouse(WarehouseDeleteFormDTO dto);
}
