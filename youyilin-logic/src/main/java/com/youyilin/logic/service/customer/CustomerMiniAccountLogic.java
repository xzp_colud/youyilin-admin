package com.youyilin.logic.service.customer;

import com.youyilin.customer.vo.account.CustomerMiniAccountGridVO;

import java.util.List;

public interface CustomerMiniAccountLogic {

    /**
     * 获取当前小程序客户账户列表
     */
    List<CustomerMiniAccountGridVO> miniList();
}
