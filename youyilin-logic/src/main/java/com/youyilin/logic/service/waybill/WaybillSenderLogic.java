package com.youyilin.logic.service.waybill;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.waybill.dto.sender.WaybillSenderFormDTO;
import com.youyilin.waybill.dto.sender.WaybillSenderPageQueryDTO;
import com.youyilin.waybill.vo.sender.WaybillSenderEditVO;
import com.youyilin.waybill.vo.sender.WaybillSenderPageVO;

import java.util.List;

public interface WaybillSenderLogic {

    /**
     * 获取分页列表
     */
    Pager<WaybillSenderPageVO> getPageList(Page<WaybillSenderPageQueryDTO> page);

    /**
     * 所有发件人
     */
    List<WaybillSenderPageVO> listAllNormal();

    /**
     * 获取编辑信息
     */
    WaybillSenderEditVO getEditVOById(Long id);

    /**
     * 保存
     */
    void saveSender(WaybillSenderFormDTO dto);
}
