package com.youyilin.logic.service.order.refund;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.order.dto.refund.OrderRefundCheckDTO;
import com.youyilin.order.dto.refund.OrderRefundPageQueryDTO;
import com.youyilin.order.dto.refund.OrderRefundSaveDTO;
import com.youyilin.order.vo.refund.OrderRefundCheckVO;
import com.youyilin.order.vo.refund.OrderRefundDetailVO;
import com.youyilin.order.vo.refund.OrderRefundPageVO;

public interface OrderRefundLogic {

    /**
     * 获取分页列表
     */
    Pager<OrderRefundPageVO> getPageList(Page<OrderRefundPageQueryDTO> page);

    /**
     * 获取退款审核信息
     */
    OrderRefundCheckVO getCheckVOById(Long id);

    /**
     * 获取退款详情信息
     */
    OrderRefundDetailVO getDetailVOById(Long id);

    /**
     * 申请退款
     */
    void addRefund(OrderRefundSaveDTO dto);

    /**
     * 退款审核
     */
    void updateCheck(OrderRefundCheckDTO dto);
}
