package com.youyilin.logic.service.customer;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.customer.dto.account.CustomerAccountChargeFormDTO;
import com.youyilin.customer.dto.account.CustomerAccountFormDTO;
import com.youyilin.customer.dto.account.CustomerAccountPageQueryDTO;
import com.youyilin.customer.vo.account.CustomerAccountPageVO;

import java.util.List;

public interface CustomerAccountLogic {

    /**
     * 获取分页列表
     */
    Pager<CustomerAccountPageVO> getPageList(Page<CustomerAccountPageQueryDTO> page);

    /**
     * 按客户获取账户列表
     *
     * @param customerId 账户ID
     * @return ArrayList
     */
    List<CustomerAccountPageVO> listByCustomerId(Long customerId);

    /**
     * 保存
     */
    void saveAccount(CustomerAccountFormDTO dto);

    /**
     * 充值
     */
    void updateCharge(CustomerAccountChargeFormDTO dto);
}
