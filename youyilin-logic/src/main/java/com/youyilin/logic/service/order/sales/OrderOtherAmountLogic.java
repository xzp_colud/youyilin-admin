package com.youyilin.logic.service.order.sales;

import com.youyilin.order.vo.sales.OrderOtherAmountPageVO;

import java.util.List;

public interface OrderOtherAmountLogic {

    /**
     * 根据订单id查询其他费用明细
     *
     * @param orderId 订单id
     * @return ArrayList
     */
    List<OrderOtherAmountPageVO> listByOrderId(Long orderId);
}
