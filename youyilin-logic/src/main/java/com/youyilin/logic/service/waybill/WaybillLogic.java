package com.youyilin.logic.service.waybill;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.waybill.dto.waybill.WaybillConfirmFormDTO;
import com.youyilin.waybill.dto.waybill.WaybillInfoFormDTO;
import com.youyilin.waybill.dto.waybill.WaybillPageQueryDTO;
import com.youyilin.waybill.vo.waybill.WaybillDetailVO;
import com.youyilin.waybill.vo.waybill.WaybillPageVO;
import com.youyilin.waybill.vo.waybill.WaybillVO;

public interface WaybillLogic {

    /**
     * 获取货运单列表
     */
    Pager<WaybillPageVO> getPageList(Page<WaybillPageQueryDTO> page);

    /**
     * 获取货运单VO
     *
     * @param id 货运单ID
     * @return WaybillVO
     */
    WaybillVO getVOById(Long id);

    /**
     * 获取货运单详情
     *
     * @param id 货运单ID
     * @return WaybillDetailVO
     */
    WaybillDetailVO getDetailVOById(Long id);

    /**
     * 货运单确认
     */
    void updateConfirm(WaybillConfirmFormDTO dto);

    /**
     * 货运单信息补充完整
     */
    void updateInfo(WaybillInfoFormDTO dto);
}
