package com.youyilin.logic.service.product;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.goods.dto.params.ParamsFormDTO;
import com.youyilin.goods.dto.params.ParamsPageQueryDTO;
import com.youyilin.goods.vo.params.ParamsPageVO;
import com.youyilin.goods.vo.params.ParamsVO;

import java.util.List;

public interface ParamsLogic {

    /**
     * 获取分页列表
     */
    Pager<ParamsPageVO> getPageList(Page<ParamsPageQueryDTO> page);

    /**
     * 获取属性参数
     *
     * @param id 属性参数ID
     * @return ParamsVO
     */
    ParamsVO getParamsVOById(Long id);

    /**
     * 获取所有的属性参数
     *
     * @return ArrayList
     */
    List<ParamsVO> listAll();

    /**
     * 按分类ID获取属性参数
     *
     * @param categoryId 分类ID
     * @return ArrayList
     */
    List<ParamsVO> listByCategoryId(Long categoryId);

    /**
     * 保存
     */
    void saveParams(ParamsFormDTO dto);
}
