package com.youyilin.logic.service.warehouse;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.warehouse.dto.inventoryLog.InventoryLogPageQueryDTO;
import com.youyilin.warehouse.vo.inventoryLog.InventoryLogPageVO;

import java.util.List;

public interface InventoryLogLogic {

    /**
     * 获取分页列表
     */
    Pager<InventoryLogPageVO> getPageList(Page<InventoryLogPageQueryDTO> page);

    /**
     * 按库存获取库存日志列表
     *
     * @param inventoryId 库存ID
     * @return ArrayList
     */
    List<InventoryLogPageVO> listByInventoryId(Long inventoryId);

    /**
     * 按库存批次获取库存日志列表
     *
     * @param inventoryLotId 库存批次ID
     * @return ArrayList
     */
    List<InventoryLogPageVO> listByInventoryLotId(Long inventoryLotId);
}
