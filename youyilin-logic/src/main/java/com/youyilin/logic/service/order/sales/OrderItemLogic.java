package com.youyilin.logic.service.order.sales;

import com.youyilin.order.vo.sales.OrderItemPageVO;

import java.util.List;

public interface OrderItemLogic {

    /**
     * 根据订单id获取订单明细
     *
     * @param orderId 订单ID
     * @return ArrayList
     */
    List<OrderItemPageVO> listByOrderId(Long orderId);
}
