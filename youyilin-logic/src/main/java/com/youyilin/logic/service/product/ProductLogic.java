package com.youyilin.logic.service.product;

import com.youyilin.goods.model.request.RequestProduct;

/**
 * 商品保存
 */
public interface ProductLogic {

    /**
     * 保存
     */
    void saveProduct(RequestProduct request);

    /**
     * 删除
     */
    void delProduct(Long id);
}
