package com.youyilin.logic.service.order.sales;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.order.dto.sales.OrderCollectPageQueryDTO;
import com.youyilin.order.vo.sales.OrderCollectPageVO;

import java.util.List;

public interface OrderCollectLogic {

    /**
     * 获取分页列表
     */
    Pager<OrderCollectPageVO> getPageList(Page<OrderCollectPageQueryDTO> page);

    /**
     * 根据订单id查询收款明细
     *
     * @param orderId 订单id
     * @return ArrayList
     */
    List<OrderCollectPageVO> listByOrderId(Long orderId);
}
