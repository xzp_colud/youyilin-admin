package com.youyilin.logic.service.customer;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.customer.dto.accountLog.CustomerAccountLogPageQueryDTO;
import com.youyilin.customer.vo.accountLog.CustomerAccountLogPageVO;

public interface CustomerAccountLogLogic {

    /**
     * 获取分页列表
     */
    Pager<CustomerAccountLogPageVO> getPageList(Page<CustomerAccountLogPageQueryDTO> page);
}
