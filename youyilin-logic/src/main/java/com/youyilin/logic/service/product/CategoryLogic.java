package com.youyilin.logic.service.product;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.goods.dto.category.CategoryFormDTO;
import com.youyilin.goods.dto.category.CategoryPageQueryDTO;
import com.youyilin.goods.vo.category.CategoryDetailVO;
import com.youyilin.goods.vo.category.CategoryEditVO;
import com.youyilin.goods.vo.category.CategoryPageVO;
import com.youyilin.goods.vo.category.CategoryVO;

import java.util.List;

public interface CategoryLogic {

    /**
     * 获取分页列表
     */
    Pager<CategoryPageVO> getPageList(Page<CategoryPageQueryDTO> page);

    /**
     * 获取分类信息
     *
     * @param id 分类ID
     * @return CategoryVO
     */
    CategoryVO getVOById(Long id);

    /**
     * 获取所有分类
     */
    List<CategoryVO> listAll();

    /**
     * 获取编辑信息
     *
     * @param id 分类ID
     * @return CategoryEditVO
     */
    CategoryEditVO getEditVOById(Long id);

    /**
     * 获取详情信息
     *
     * @param id 分类ID
     * @return CategoryDetailVO
     */
    CategoryDetailVO getDetailVOById(Long id);

    /**
     * 保存
     */
    void saveCategory(CategoryFormDTO dto);
}
