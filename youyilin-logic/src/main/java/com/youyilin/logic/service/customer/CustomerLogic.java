package com.youyilin.logic.service.customer;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.customer.dto.customer.CustomerFormDTO;
import com.youyilin.customer.dto.customer.CustomerPageQueryDTO;
import com.youyilin.customer.vo.customer.CustomerDetailVO;
import com.youyilin.customer.vo.customer.CustomerEditVO;
import com.youyilin.customer.vo.customer.CustomerPageVO;

public interface CustomerLogic {

    /**
     * 获取分页列表
     */
    Pager<CustomerPageVO> getPageList(Page<CustomerPageQueryDTO> pager);

    /**
     * 获取编辑VO
     */
    CustomerEditVO getEditVOById(Long id);

    /**
     * 获取详情VO
     */
    CustomerDetailVO getDetailVOById(Long id);

    /**
     * 保存
     *
     * @param dto 表单DTO
     */
    void saveCustomer(CustomerFormDTO dto);
}
