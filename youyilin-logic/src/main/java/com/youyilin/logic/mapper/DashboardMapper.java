package com.youyilin.logic.mapper;

import com.youyilin.logic.vo.DashboardSellAmountVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface DashboardMapper {

    /**
     * 统计所有客户数量
     */
    Long countTotalCustomerNum();

    /**
     * 统计新增的客户数量
     *
     * @param start 开始时间
     * @param end   结束时间
     */
    Long countNewCustomerNum(String start, String end);

    /**
     * 统计活跃的客户数
     *
     * @param start 开始时间
     * @param end   结束时间
     */
    Long countActiveCustomerNum(String start, String end);

    /**
     * 统计客户的销售额
     *
     * @param start 开始时间
     * @param end   结束时间
     */
    List<DashboardSellAmountVO> sumCustomerAmount(String start, String end);
}
