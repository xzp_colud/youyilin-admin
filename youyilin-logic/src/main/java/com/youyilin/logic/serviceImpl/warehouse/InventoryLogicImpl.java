package com.youyilin.logic.serviceImpl.warehouse;

import com.alibaba.fastjson.JSONObject;
import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.exception.ApiException;
import com.youyilin.common.exception.Assert;
import com.youyilin.goods.entity.Product;
import com.youyilin.goods.entity.ProductPrice;
import com.youyilin.goods.service.ProductPriceService;
import com.youyilin.goods.service.ProductService;
import com.youyilin.logic.service.warehouse.InventoryLogic;
import com.youyilin.order.utils.NoUtils;
import com.youyilin.warehouse.dto.InventoryInDTO;
import com.youyilin.warehouse.dto.InventoryOutDTO;
import com.youyilin.warehouse.dto.inventory.InventoryDeleteFormDTO;
import com.youyilin.warehouse.dto.inventory.InventoryMoveFormDTO;
import com.youyilin.warehouse.dto.inventory.InventoryPageQueryDTO;
import com.youyilin.warehouse.entity.Inventory;
import com.youyilin.warehouse.entity.InventoryLot;
import com.youyilin.warehouse.manager.InventoryManager;
import com.youyilin.warehouse.mapper.InventoryMapper;
import com.youyilin.warehouse.service.InventoryLotService;
import com.youyilin.warehouse.service.InventoryService;
import com.youyilin.warehouse.vo.inventory.InventoryCheckVO;
import com.youyilin.warehouse.vo.inventory.InventoryErrorVO;
import com.youyilin.warehouse.vo.inventory.InventoryPageVO;
import com.youyilin.warehouse.vo.inventory.InventoryVO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class InventoryLogicImpl implements InventoryLogic {

    private final InventoryMapper inventoryMapper;
    private final InventoryService inventoryService;
    private final ProductService productService;
    private final ProductPriceService productPriceService;
    private final InventoryLotService inventoryLotService;
    private final InventoryManager inventoryManager;

    public InventoryLogicImpl(InventoryMapper inventoryMapper, InventoryService inventoryService,
                              ProductService productService, ProductPriceService productPriceService,
                              InventoryLotService inventoryLotService, InventoryManager inventoryManager) {
        this.inventoryMapper = inventoryMapper;
        this.inventoryService = inventoryService;
        this.productService = productService;
        this.productPriceService = productPriceService;
        this.inventoryLotService = inventoryLotService;
        this.inventoryManager = inventoryManager;
    }

    @Override
    public Pager<InventoryPageVO> getPageList(Page<InventoryPageQueryDTO> page) {
        Integer total = inventoryMapper.getTotal(page);
        if (total == 0) {
            return new Pager<>(0, new ArrayList<>());
        }
        List<InventoryPageVO> list = inventoryMapper.getPage(page);
        return new Pager<>(total, list);
    }

    @Override
    public List<InventoryErrorVO> listError() {
        return inventoryMapper.listError();
    }

    @Override
    public List<InventoryCheckVO> listByProductName(String productName) {
        if (StringUtils.isBlank(productName)) {
            return new ArrayList<>();
        }
        productName = productName.trim();
        if (StringUtils.isBlank(productName)) {
            return new ArrayList<>();
        }
        return inventoryMapper.listByProductName(productName);
    }

    @Override
    public List<InventoryCheckVO> listLikeByProductName(String productName) {
        if (StringUtils.isBlank(productName)) {
            return new ArrayList<>();
        }
        productName = productName.trim();
        if (StringUtils.isBlank(productName)) {
            return new ArrayList<>();
        }
        return inventoryMapper.listLikeByProductName(productName);
    }

    @Override
    public List<InventoryVO> queryIntoByProductId(Long productId) {
        if (productId == null) {
            return new ArrayList<>();
        }

        // 找出所有的库区
        List<InventoryVO> allList = inventoryMapper.listAllWarehouseArea();
        if (allList.isEmpty()) {
            return new ArrayList<>();
        }
        List<InventoryVO> existList = inventoryMapper.listInventoryOutByProductId(productId);
        if (existList.isEmpty()) {
            return allList;
        }
        Map<Long, List<InventoryVO>> mapByWarehouseArea = existList.stream().collect(Collectors.groupingBy(InventoryVO::getWarehouseAreaId));

        List<InventoryVO> listExist = new ArrayList<>();
        List<InventoryVO> listOther = new ArrayList<>();
        for (InventoryVO dto : allList) {
            List<InventoryVO> existMap = mapByWarehouseArea.get(dto.getWarehouseAreaId());
            if (CollectionUtils.isNotEmpty(existMap)) {
                BigDecimal totalQty = BigDecimal.ZERO;
                for (InventoryVO idto : existMap) {
                    totalQty = totalQty.add(idto.getQty());
                }
                dto.setQty(totalQty);

                listExist.add(dto);
            } else {
                listOther.add(dto);
            }
        }

        List<InventoryVO> returnList = new ArrayList<>();
        returnList.addAll(listExist);
        returnList.addAll(listOther);
        return returnList;
    }

    @Override
    public List<InventoryVO> queryOutBySkuId(Long skuId) {
        List<InventoryVO> list = inventoryMapper.listInventoryOutBySkuId(skuId);
        if (CollectionUtils.isEmpty(list)) {
            return new ArrayList<>();
        }
        // 库存
        List<Long> inventoryIds = new ArrayList<>();
        list.forEach(item -> inventoryIds.add(item.getId()));
        // 获取批次信息
        List<InventoryLot> lotList = inventoryLotService.listQtyByInventoryIds(inventoryIds);
        Map<Long, List<InventoryLot>> lotMap = lotList.stream().collect(Collectors.groupingBy(InventoryLot::getInventoryId));
        // 数据组装
        List<InventoryVO> voList = new ArrayList<>();
        for (InventoryVO item : list) {
            List<InventoryLot> lot = lotMap.get(item.getId());
            if (CollectionUtils.isEmpty(lot)) {
                continue;
            }
            for (InventoryLot lotItem : lot) {
                InventoryVO vo = JSONObject.parseObject(JSONObject.toJSONString(item), InventoryVO.class);
                vo.setId(lotItem.getId())
                        .setInventoryLotSn(lotItem.getSn())
                        .setLotSn(lotItem.getLotSn())
                        .setQty(lotItem.getQty())
                        .setFrozenQty(lotItem.getFrozenQty());
                voList.add(vo);
            }
        }
        return voList;
    }

    @Override
    public void asyncInventory() {
        inventoryMapper.asyncInventoryZero();
        inventoryMapper.asyncInventory();
    }

    @Override
    public void delInventory(InventoryDeleteFormDTO dto) {
        // 验证批次
        Inventory inventory = inventoryService.validateInventory(dto.getId());
        boolean noQty = inventory.getQty() != null && inventory.getQty().compareTo(BigDecimal.ZERO) == 0;
        Assert.isTrue(noQty, "库存数量不为0");
        inventoryManager.delByInventory(inventory.getId());
    }

    @Override
    @Transactional
    public void updateInventoryMove(InventoryMoveFormDTO dto) {
        // 库存信息
        Inventory inventory = inventoryService.validateInventory(dto.getId());
        if (inventory.getQty().compareTo(BigDecimal.ZERO) <= 0) {
            throw new ApiException("产品库存余量为0，无需操作");
        }
        if (inventory.getWarehouseId().equals(dto.getWarehouseId()) && inventory.getWarehouseAreaId().equals(dto.getWarehouseAreaId())) {
            throw new ApiException("已在当前库区，无需操作");
        }
        if (inventory.getFrozenQty() != null && inventory.getFrozenQty().compareTo(BigDecimal.ZERO) > 0) {
            throw new ApiException("存在冻结库存不能移动");
        }
        // 批次明细
        List<InventoryLot> lotList = inventoryLotService.listByInventoryId(inventory.getId());
        // 商品
        Product product = productService.validateProduct(inventory.getProductId());
        // SKU
        ProductPrice productPrice = productPriceService.validatePrice(inventory.getSkuId());
        // 出库DTO
        List<InventoryOutDTO> outList = new ArrayList<>();
        // 入库DTO
        List<InventoryInDTO> inList = new ArrayList<>();
        // 批次号
        String lotSn = NoUtils.getLotSn();
        int index = 1;
        for (InventoryLot lot : lotList) {
            if (lot.getQty().compareTo(BigDecimal.ZERO) > 0) {
                InventoryOutDTO out = new InventoryOutDTO();
                out.setId(lot.getId())
                        .setQty(lot.getQty())
                        .setProductId(inventory.getProductId())
                        .setProductName(product.getName())
                        .setSkuId(inventory.getSkuId())
                        .setSkuName(productPrice.getSkuName())
                        .setSourceSn(lot.getLotSn());
                outList.add(out);

                String lotSnNew = lotSn + "-" + index++;
                InventoryInDTO in = new InventoryInDTO();
                in.setSourceSn(lot.getLotSn())
                        .setSn(lotSnNew)
                        .setLotSn(lot.getLotSn())
                        .setQty(lot.getQty())
                        .setProductId(inventory.getProductId())
                        .setProductName(product.getName())
                        .setSkuId(inventory.getSkuId())
                        .setSkuName(productPrice.getSkuName())
                        .setWarehouseId(dto.getWarehouseId())
                        .setWarehouseAreaId(dto.getWarehouseAreaId());
                inList.add(in);
            }
        }
        if (CollectionUtils.isEmpty(outList)) {
            return;
        }
        // 先出库
        inventoryManager.moveOut(outList);
        // 再入库
        inventoryManager.moveIn(inList);
    }
}
