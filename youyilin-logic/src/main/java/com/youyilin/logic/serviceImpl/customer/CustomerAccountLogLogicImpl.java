package com.youyilin.logic.serviceImpl.customer;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.customer.dto.accountLog.CustomerAccountLogPageQueryDTO;
import com.youyilin.customer.mapper.CustomerAccountLogMapper;
import com.youyilin.customer.vo.accountLog.CustomerAccountLogPageVO;
import com.youyilin.logic.service.customer.CustomerAccountLogLogic;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerAccountLogLogicImpl implements CustomerAccountLogLogic {

    private final CustomerAccountLogMapper customerAccountLogMapper;

    public CustomerAccountLogLogicImpl(CustomerAccountLogMapper customerAccountLogMapper) {
        this.customerAccountLogMapper = customerAccountLogMapper;
    }

    @Override
    public Pager<CustomerAccountLogPageVO> getPageList(Page<CustomerAccountLogPageQueryDTO> page) {
        Integer total = customerAccountLogMapper.getTotal(page);
        if (total == 0) {
            return new Pager<>(0, new ArrayList<>());
        }
        List<CustomerAccountLogPageVO> list = customerAccountLogMapper.getPage(page);
        return new Pager<>(total, list);
    }
}
