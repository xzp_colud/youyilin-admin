package com.youyilin.logic.serviceImpl.order.sales;

import com.youyilin.logic.service.order.sales.OrderDiscountLogic;
import com.youyilin.order.service.OrderDiscountService;
import com.youyilin.order.vo.sales.OrderDiscountPageVO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderDiscountLogicImpl implements OrderDiscountLogic {

    private final OrderDiscountService orderDiscountService;

    public OrderDiscountLogicImpl(OrderDiscountService orderDiscountService) {
        this.orderDiscountService = orderDiscountService;
    }

    @Override
    public List<OrderDiscountPageVO> listByOrderId(Long orderId) {
        if (orderId == null) {
            return new ArrayList<>();
        }
        return OrderDiscountPageVO.convertByEntity(orderDiscountService.listByOrderId(orderId));
    }
}
