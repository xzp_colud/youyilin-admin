package com.youyilin.logic.serviceImpl.customer;

import com.youyilin.customer.enums.AccountTypeEnum;
import com.youyilin.customer.entity.CustomerAccount;
import com.youyilin.customer.entity.CustomerMini;
import com.youyilin.customer.vo.account.CustomerMiniAccountGridVO;
import com.youyilin.customer.service.CustomerAccountService;
import com.youyilin.customer.service.CustomerMiniService;
import com.youyilin.logic.service.customer.CustomerMiniAccountLogic;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerMiniAccountLogicImpl implements CustomerMiniAccountLogic {

    private final CustomerMiniService customerMiniService;
    private final CustomerAccountService customerAccountService;

    public CustomerMiniAccountLogicImpl(CustomerMiniService customerMiniService, CustomerAccountService customerAccountService) {
        this.customerMiniService = customerMiniService;
        this.customerAccountService = customerAccountService;
    }

    @Override
    public List<CustomerMiniAccountGridVO> miniList() {
        CustomerMini cm = customerMiniService.isLoginNoException();
        if (cm == null) {
            return this.listDefault();
        }
        List<CustomerAccount> list = customerAccountService.listByCustomerId(cm.getId());
        if (CollectionUtils.isEmpty(list)) {
            return this.listDefault();
        }

        List<CustomerMiniAccountGridVO> voList = new ArrayList<>();
        for (CustomerAccount item : list) {
            if (item.getType() == AccountTypeEnum.BALANCE.getCode()
                    || item.getType() == AccountTypeEnum.INTEGRAL.getCode()) {
                CustomerMiniAccountGridVO vo = new CustomerMiniAccountGridVO();
                vo.setLabel(AccountTypeEnum.queryInfoByCode(item.getType()));
                vo.setValue(item.getResidueText());
                voList.add(vo);
            }
        }
        return voList;
    }

    private List<CustomerMiniAccountGridVO> listDefault() {
        List<CustomerMiniAccountGridVO> voList = new ArrayList<>();
        for (AccountTypeEnum item : AccountTypeEnum.values()) {
            if (item.getCode() == AccountTypeEnum.BALANCE.getCode()
                    || item.getCode() == AccountTypeEnum.INTEGRAL.getCode()) {
                CustomerMiniAccountGridVO vo = new CustomerMiniAccountGridVO();
                vo.setLabel(item.getInfo());
                vo.setValue("0");
            }
        }
        return voList;
    }
}
