package com.youyilin.logic.serviceImpl.order.sales;

import com.youyilin.logic.service.order.sales.OrderOtherAmountLogic;
import com.youyilin.order.service.OrderOtherAmountService;
import com.youyilin.order.vo.sales.OrderOtherAmountPageVO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderOtherAmountLogicImpl implements OrderOtherAmountLogic {

    private final OrderOtherAmountService orderOtherAmountService;

    public OrderOtherAmountLogicImpl(OrderOtherAmountService orderOtherAmountService) {
        this.orderOtherAmountService = orderOtherAmountService;
    }

    @Override
    public List<OrderOtherAmountPageVO> listByOrderId(Long orderId) {
        if (orderId == null) {
            return new ArrayList<>();
        }
        return OrderOtherAmountPageVO.convertByEntity(orderOtherAmountService.listByOrderId(orderId));
    }
}
