package com.youyilin.logic.serviceImpl.product;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.youyilin.common.enums.StatusEnum;
import com.youyilin.common.exception.ApiException;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.goods.entity.*;
import com.youyilin.goods.model.dto.UnitDto;
import com.youyilin.goods.model.request.RequestProduct;
import com.youyilin.goods.model.request.RequestProductPlan;
import com.youyilin.goods.service.*;
import com.youyilin.logic.service.product.ProductLogic;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ProductLogicImpl implements ProductLogic {

    private final ProductService productService;
    private final SupplierService supplierService;
    private final CategoryService categoryService;
    private final UnitService unitService;
    private final UnitDataService unitDataService;
    private final ProductPriceService productPriceService;
    private final ProductUnitService productUnitService;
    private final ProductUnitPriceService productUnitPriceService;
    private final ParamsService paramsService;
    private final ProductParamsService productParamsService;
    private final ProductPlanService productPlanService;
    private final ProductFabricService productFabricService;
    private final ProductIngredientService productIngredientService;
    private final ProductSubMaterialService productSubMaterialService;
    private final ProductTechnologyService productTechnologyService;
    private final ProductProcessFeeService productProcessFeeService;
    private final ProductDescriptionService productDescriptionService;

    public ProductLogicImpl(ProductService productService, SupplierService supplierService, CategoryService categoryService,
                            UnitService unitService, UnitDataService unitDataService, ProductPriceService productPriceService,
                            ProductUnitService productUnitService, ProductUnitPriceService productUnitPriceService,
                            ParamsService paramsService, ProductParamsService productParamsService, ProductPlanService productPlanService,
                            ProductFabricService productFabricService, ProductIngredientService productIngredientService,
                            ProductSubMaterialService productSubMaterialService, ProductTechnologyService productTechnologyService,
                            ProductProcessFeeService productProcessFeeService, ProductDescriptionService productDescriptionService) {
        this.productService = productService;
        this.supplierService = supplierService;
        this.categoryService = categoryService;
        this.unitService = unitService;
        this.unitDataService = unitDataService;
        this.productPriceService = productPriceService;
        this.productUnitService = productUnitService;
        this.productUnitPriceService = productUnitPriceService;
        this.paramsService = paramsService;
        this.productParamsService = productParamsService;
        this.productPlanService = productPlanService;
        this.productFabricService = productFabricService;
        this.productIngredientService = productIngredientService;
        this.productSubMaterialService = productSubMaterialService;
        this.productTechnologyService = productTechnologyService;
        this.productProcessFeeService = productProcessFeeService;
        this.productDescriptionService = productDescriptionService;
    }

    @Override
    @Transactional
    public void saveProduct(RequestProduct request) {
        // 验证供货商
        Supplier supplier = supplierService.validateSupplierStatus(request.getSupplierId());
        // 验证类目
        Category category = categoryService.validateCategoryStatus(request.getCategoryId());
        // 持久化商品
        request.setSupplierName(supplier.getName());
        request.setCategoryName(category.getTitle());
        Product product = this.insertProduct(request);
        // 商品ID
        long productId = product.getId();
        request.setId(productId);
        // 验证产品参数
        List<ProductParams> paramsList = this.validateParams(category.getId(), request.getParamsList());
        // 先修改产品ID
        unitDataService.updateProductId(request.getRandomId(), productId);
        // 移除删除的部分
        unitDataService.delByIdsForProduct(productId, request.getDelUnitDataList());
        // 验证方案
        this.validatePlan(request);
        this.validatePlanPrice(request);
        // 验证销售规格 验证SKU
        this.validateUnitAndPriceNull(request);
        this.validateUnit(request);
        List<ProductUnitPrice> saveProductUnitPriceList = this.validateSku(request);

        productPriceService.delByProductId(productId);
        productPriceService.saveProductPrice(productId, request.getPriceList());

        productUnitService.delByProductId(productId);
        productUnitService.saveProductUnit(productId, request.getUnitList());

        productUnitPriceService.delByProductId(productId);
        productUnitPriceService.saveProductUnitPrice(productId, saveProductUnitPriceList);

        // 参数
        productParamsService.delByProductId(productId);
        productParamsService.saveProductParamsBatch(productId, paramsList);
        // 详情
        productDescriptionService.delByProductId(productId);
        productDescriptionService.saveDescription(productId, request.getProductDescription());

        this.saveProduct(productId, request);

        // 最低价
        Integer minSellPrice = productPriceService.minSellPriceByProductId(productId);
        productService.updateMinPrice(productId, minSellPrice);

        // 产品名称冗余
        this.redundancyProductName(productId, product.getName());

        log.info("【商品添加】 Request：{}", JSONObject.toJSONString(request));
    }

    @Override
    @Transactional
    public void delProduct(Long id) {
        // 验证商品
        productService.validateProduct(id);
        // 逻辑删除商品
        productService.removeById(id);
        // 其余关联属性不做删除
    }

    // 成衣 样衣
    private void saveProduct(Long productId, RequestProduct request) {
        List<RequestProductPlan> requestPlanList = request.getPlanList();
        if (CollectionUtils.isEmpty(requestPlanList)) {
            return;
        }

        List<ProductPlan> planList = new ArrayList<>();
        List<ProductFabric> fabricList = new ArrayList<>();
        List<ProductIngredient> ingredientList = new ArrayList<>();
        List<ProductSubMaterial> subMaterialList = new ArrayList<>();
        List<ProductTechnology> technologyList = new ArrayList<>();
        List<ProductProcessFee> processFeeList = new ArrayList<>();

        for (RequestProductPlan item : requestPlanList) {
            ProductPlan plan = new ProductPlan();
            plan.setId(item.getPlanId());
            plan.setTitle(item.getTitle());

            planList.add(plan);

            List<ProductFabric> fabrics = item.getFabricList();
            List<ProductIngredient> ingredients = item.getIngredientList();
            List<ProductSubMaterial> subMaterials = item.getSubMaterialList();
            List<ProductTechnology> technologies = item.getTechnologyList();
            List<ProductProcessFee> productProcessFees = item.getProcessFeeList();

            if (CollectionUtils.isNotEmpty(fabrics)) {
                fabrics.forEach(fItem -> fItem.setPlanId(item.getPlanId()));
                fabricList.addAll(fabrics);
            }
            if (CollectionUtils.isNotEmpty(ingredients)) {
                ingredients.forEach(iItem -> iItem.setPlanId(item.getPlanId()));
                ingredientList.addAll(ingredients);
            }
            if (CollectionUtils.isNotEmpty(subMaterials)) {
                subMaterials.forEach(sItem -> sItem.setPlanId(item.getPlanId()));
                subMaterialList.addAll(subMaterials);
            }
            if (CollectionUtils.isNotEmpty(technologies)) {
                technologies.forEach(tItem -> tItem.setPlanId(item.getPlanId()));
                technologyList.addAll(technologies);
            }
            if (CollectionUtils.isNotEmpty(productProcessFees)) {
                productProcessFees.forEach(pItem -> pItem.setPlanId(item.getPlanId()));
                processFeeList.addAll(productProcessFees);
            }
        }

        productPlanService.delByProductId(productId);
        productPlanService.savePlanBatch(productId, planList);

        productFabricService.delByProductId(productId);
        productFabricService.saveFabricBatch(productId, fabricList);

        productIngredientService.delByProductId(productId);
        productIngredientService.saveIngredientBatch(productId, ingredientList);

        productSubMaterialService.delByProductId(productId);
        productSubMaterialService.saveSubMaterialBatch(productId, subMaterialList);

        productTechnologyService.delByProductId(productId);
        productTechnologyService.saveTechnologyBatch(productId, technologyList);

        productProcessFeeService.delByProductId(productId);
        productProcessFeeService.saveProcessFeeBatch(productId, processFeeList);
    }

    // 冗余名称
    private void redundancyProductName(Long productId, String name) {
        productFabricService.updateRedundancyProductName(productId, name);
        productIngredientService.updateRedundancyProductName(productId, name);
        productSubMaterialService.updateRedundancyProductName(productId, name);
    }

    private List<ProductParams> validateParams(Long categoryId, List<ProductParams> requestList) {
        List<Params> paramsList = paramsService.listByCategoryId(categoryId);
        if (CollectionUtils.isEmpty(paramsList)) return new ArrayList<>();

        Map<Long, List<ProductParams>> requestMap = requestList.stream().collect(Collectors.groupingBy(ProductParams::getParamsId));
        List<ProductParams> returnList = new ArrayList<>();
        for (Params item : paramsList) {
            List<ProductParams> existList = requestMap.get(item.getId());
            ProductParams pp = new ProductParams();
            pp.setParamsId(item.getId());
            if (CollectionUtils.isNotEmpty(existList)) {
                pp.setValue(existList.get(0).getValue());
            }

            returnList.add(pp);
        }

        return returnList;
    }

    private void validateUnitAndPriceNull(RequestProduct dto) {
        List<UnitDto> saveUnitDtoList = dto.getUnitList();
        if (CollectionUtils.isEmpty(saveUnitDtoList)) {
            throw new ApiException("未选择销售方式");
        }
        List<ProductPrice> savePriceList = dto.getPriceList();
        if (CollectionUtils.isEmpty(savePriceList)) {
            throw new ApiException("SKU配置不正确");
        }
    }

    /**
     * 验证销售单位
     */
    private void validateUnit(RequestProduct dto) {
        // 排序
        List<UnitDto> saveUnitDtoList = dto.getUnitList();
        if (saveUnitDtoList.size() > 1) {
            saveUnitDtoList.sort((o1, o2) -> (int) (o2.getId() - o1.getId()));
        }
        // 销售方式keys
        List<Long> unitIds = new ArrayList<>();
        saveUnitDtoList.forEach(item -> unitIds.add(item.getId()));
        // 查询
        List<UnitDto> findUnitDtoList = unitService.listAllDtoByProductId(unitIds, dto.getId());
        if (CollectionUtils.isEmpty(findUnitDtoList)) {
            throw new ApiException("销售方式异常");
        }
        Map<Long, List<UnitDto>> findUnitDtoMap = findUnitDtoList.stream().collect(Collectors.groupingBy(UnitDto::getId));
        for (UnitDto saveUnit : saveUnitDtoList) {
            if (saveUnit.getId() == null) {
                throw new ApiException("入参异常");
            }
            List<UnitData> saveItemData = saveUnit.getItemList();
            if (CollectionUtils.isEmpty(saveItemData)) {
                throw new ApiException("入参异常");
            }
            if (saveItemData.size() > 1) {
                saveItemData.sort((o1, o2) -> (int) (o2.getId() - o1.getId()));
            }
            // 已配置的销售方式
            List<UnitDto> existUnitDtoList = findUnitDtoMap.get(saveUnit.getId());
            if (CollectionUtils.isEmpty(existUnitDtoList) || existUnitDtoList.size() != 1) {
                throw new ApiException("入参异常");
            }
            UnitDto findUnit = existUnitDtoList.get(0);
            List<UnitData> findItemData = findUnit.getItemList();
            if (CollectionUtils.isEmpty(findItemData)) {
                throw new ApiException("销售方式异常");
            }
            List<Long> findItemDataIds = new ArrayList<>();
            findItemData.forEach(item -> findItemDataIds.add(item.getId()));

            // 传入与配置验证
            for (UnitData saveUnitData : saveItemData) {
                if (saveUnitData.getId() == null) {
                    throw new ApiException("入参异常");
                }
                if (!findItemDataIds.contains(saveUnitData.getId())) {
                    throw new ApiException("入参异常");
                }
            }
        }
    }

    /**
     * 验证方案
     */
    private void validatePlan(RequestProduct request) {
        List<RequestProductPlan> planList = request.getPlanList();
        if (CollectionUtils.isEmpty(planList)) {
            return;
        }

        for (RequestProductPlan item : planList) {
            item.setPlanId(IdWorker.getId());
        }
    }

    private void validatePlanPrice(RequestProduct request) {
        List<RequestProductPlan> planList = request.getPlanList();
        if (CollectionUtils.isEmpty(planList)) {
            request.getPriceList().forEach(item -> item.setPlanId(null));
        } else {
            Map<Long, List<RequestProductPlan>> planMap = planList.stream().collect(Collectors.groupingBy(RequestProductPlan::getId));
            request.getPriceList().forEach(item -> {
                if (item.getPlanId() != null) {
                    List<RequestProductPlan> existList = planMap.get(item.getPlanId());
                    if (CollectionUtils.isEmpty(existList)) {
                        item.setPlanId(null);
                    } else {
                        item.setPlanId(existList.get(0).getPlanId());
                        item.setPlanTitle(existList.get(0).getTitle());
                    }
                }
            });
        }
    }

    /**
     * 验证SKU
     */
    private List<ProductUnitPrice> validateSku(RequestProduct dto) {
        List<ProductPrice> generatePriceDtoList = this.generateSku(dto);
        List<ProductPrice> savePriceDtoList = dto.getPriceList();
        if (generatePriceDtoList.size() != savePriceDtoList.size()) {
            throw new ApiException("SKU配置异常");
        }
        Map<String, List<ProductPrice>> generatePriceDtoMap = generatePriceDtoList.stream().collect(Collectors.groupingBy(ProductPrice::getSkuPath));
        List<ProductUnitPrice> saveProductUnitPriceList = new ArrayList<>();
        for (ProductPrice ppDto : savePriceDtoList) {
            if (StringUtils.isBlank(ppDto.getSkuPath())) {
                throw new ApiException("SKU传入异常");
            }
            if (ppDto.getSellPrice() == null || ppDto.getSellPrice() < 0) {
                throw new ApiException("请输入正确的销售价");
            }
            if (ppDto.getWholesalePrice() == null || ppDto.getWholesalePrice() < 0) {
                throw new ApiException("请输入正确的批发价");
            }
            if (ppDto.getCostPrice() == null || ppDto.getCostPrice() < 0) {
                throw new ApiException("请输入正确的采购价");
            }
            if (ppDto.getStatus() == null || !ppDto.getStatus().equals(StatusEnum.NORMAL.getCode())) {
                ppDto.setStatus(StatusEnum.DISABLED.getCode());
            }
            if (ppDto.getWholesalePrice() > ppDto.getSellPrice()) {
                throw new ApiException("批发价与销售价设置异常");
            }
            List<ProductPrice> findProductPriceDtoList = generatePriceDtoMap.get(ppDto.getSkuPath());
            if (CollectionUtils.isEmpty(findProductPriceDtoList) || findProductPriceDtoList.size() != 1) {
                throw new ApiException("SKU配置异常");
            }
            ppDto.setSkuName(findProductPriceDtoList.get(0).getSkuName());
            // 库存数量不做任何操作
            if (ppDto.getId() == null) {
                ppDto.setId(IdWorker.getId());
            }

            // 销售单位关联
            ProductUnitPrice saveProductUnitPrice = new ProductUnitPrice();
            saveProductUnitPrice.setProductPriceId(ppDto.getId());
            saveProductUnitPrice.setSkuPath(ppDto.getSkuPath());

            saveProductUnitPriceList.add(saveProductUnitPrice);
        }
        return saveProductUnitPriceList;
    }

    /**
     * 按传入的已选的销售单位构建SKU
     */
    private List<ProductPrice> generateSku(RequestProduct dto) {
        List<UnitDto> saveUnitDtoList = dto.getUnitList();
        List<ProductPrice> generateSkuList = new ArrayList<>();
        for (UnitDto unitDto : saveUnitDtoList) {
            List<UnitData> unitDataList = unitDto.getItemList();
            generateSkuList = this.generateSku(generateSkuList, unitDto, unitDataList);
        }
        return generateSkuList;
    }

    /**
     * 构建SKU
     */
    private List<ProductPrice> generateSku(List<ProductPrice> generateSkuList, UnitDto unitDto, List<UnitData> unitDataList) {
        List<ProductPrice> resultList = new ArrayList<>();

        boolean isNull = CollectionUtils.isEmpty(generateSkuList);
        for (UnitData unitData : unitDataList) {
            String skuPath = unitData.getUnitId().toString() + ":" + unitData.getId().toString();
            String skuName = unitDto.getName() + ":" + unitData.getValue();
            if (isNull) {
                ProductPrice productPriceDto = new ProductPrice();
                productPriceDto.setSkuPath(skuPath);
                productPriceDto.setSkuName(skuName);
                resultList.add(productPriceDto);
                continue;
            }
            for (ProductPrice productPrice : generateSkuList) {
                ProductPrice productPriceDto = new ProductPrice();
                String skuPathNew = productPrice.getSkuPath() + ";" + skuPath;
                String skuNameNew = productPrice.getSkuName() + ";" + skuName;
                productPriceDto.setSkuPath(skuPathNew);
                productPriceDto.setSkuName(skuNameNew);
                resultList.add(productPriceDto);
            }
        }
        return resultList;
    }

    /**
     * 保存商品
     */
    private Product insertProduct(RequestProduct dto) {
        Product product = BeanHelper.map(dto, Product.class);
        productService.saveProduct(product);
        return product;
    }
}
