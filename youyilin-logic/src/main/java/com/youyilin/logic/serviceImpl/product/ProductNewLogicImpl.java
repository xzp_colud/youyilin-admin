package com.youyilin.logic.serviceImpl.product;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.goods.dto.product.ProductPageQueryDTO;
import com.youyilin.goods.dto.unit.UnitDTO;
import com.youyilin.goods.entity.*;
import com.youyilin.goods.manager.UnitManager;
import com.youyilin.goods.mapper.ProductMapper;
import com.youyilin.goods.service.*;
import com.youyilin.goods.vo.product.ProductAddVO;
import com.youyilin.goods.vo.product.ProductDetailVO;
import com.youyilin.goods.vo.product.ProductEditVO;
import com.youyilin.goods.vo.product.ProductPageVO;
import com.youyilin.goods.vo.product.add.ProductAddParams;
import com.youyilin.goods.vo.product.add.ProductAddUnit;
import com.youyilin.goods.vo.product.detail.*;
import com.youyilin.goods.vo.product.edit.*;
import com.youyilin.goods.vo.product.page.ProductPageParams;
import com.youyilin.goods.vo.product.page.ProductPagePrice;
import com.youyilin.goods.vo.supplier.SupplierSelectVO;
import com.youyilin.logic.service.product.ProductNewLogic;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ProductNewLogicImpl implements ProductNewLogic {

    private final ProductMapper productMapper;
    private final ProductService productService;
    private final ProductPriceService productPriceService;
    private final ProductUnitPriceService productUnitPriceService;
    private final ProductParamsService productParamsService;
    private final ProductPlanService productPlanService;
    private final ProductFabricService productFabricService;
    private final ProductIngredientService productIngredientService;
    private final ProductSubMaterialService productSubMaterialService;
    private final ProductTechnologyService productTechnologyService;
    private final ProductProcessFeeService productProcessFeeService;
    private final ProductDescriptionService productDescriptionService;
    private final CategoryService categoryService;
    private final SupplierService supplierService;
    private final ParamsService paramsService;
    private final UnitManager unitManager;

    public ProductNewLogicImpl(ProductParamsService productParamsService, ProductMapper productMapper,
                               ProductService productService, SupplierService supplierService,
                               ProductPriceService productPriceService, ProductUnitPriceService productUnitPriceService,
                               ProductPlanService productPlanService, ProductFabricService productFabricService,
                               UnitManager unitManager, ProductIngredientService productIngredientService,
                               ProductSubMaterialService productSubMaterialService, ParamsService paramsService,
                               ProductTechnologyService productTechnologyService, CategoryService categoryService,
                               ProductDescriptionService productDescriptionService, ProductProcessFeeService productProcessFeeService) {
        this.productParamsService = productParamsService;
        this.productMapper = productMapper;
        this.productService = productService;
        this.supplierService = supplierService;
        this.productPriceService = productPriceService;
        this.productUnitPriceService = productUnitPriceService;
        this.productPlanService = productPlanService;
        this.productFabricService = productFabricService;
        this.unitManager = unitManager;
        this.productIngredientService = productIngredientService;
        this.productSubMaterialService = productSubMaterialService;
        this.paramsService = paramsService;
        this.productTechnologyService = productTechnologyService;
        this.categoryService = categoryService;
        this.productDescriptionService = productDescriptionService;
        this.productProcessFeeService = productProcessFeeService;
    }

    @Override
    public Pager<ProductPageVO> getPageList(Page<ProductPageQueryDTO> page) {
        Integer total = productMapper.getTotal(page);
        if (total == 0) {
            return new Pager<>(0, new ArrayList<>());
        }
        List<ProductPageVO> voList = productMapper.getPage(page);
        List<Long> productIds = new ArrayList<>();
        voList.forEach(item -> productIds.add(item.getId()));
        List<ProductPrice> productPriceList = productPriceService.listByProductIds(productIds);
        List<ProductParams> productParamsList = productParamsService.listByProductIds(productIds);
        Map<Long, List<ProductPrice>> productPriceMap = productPriceList.stream().collect(Collectors.groupingBy(ProductPrice::getProductId));
        Map<Long, List<ProductParams>> productParamsMap = productParamsList.stream().collect(Collectors.groupingBy(ProductParams::getProductId));
        for (ProductPageVO vo : voList) {
            List<ProductPagePrice> existPriceList = ProductPagePrice.convertByEntity(productPriceMap.get(vo.getId()));
            List<ProductPageParams> existParamsList = ProductPageParams.convert(productParamsMap.get(vo.getId()));
            vo.setPriceList(existPriceList);
            vo.setParamsList(existParamsList);
        }
        return new Pager<>(total, voList);
    }

    @Override
    public ProductAddVO getAddVOByCategoryId(Long categoryId) {
        Category category = categoryService.getById(categoryId);
        if (category == null) {
            return new ProductAddVO();
        }
        // 属性参数
        List<Params> paramsList = paramsService.listByCategoryId(categoryId);
        List<ProductAddParams> addParamsList = ProductAddParams.convert(paramsList);
        // 规格
        List<UnitDTO> unitDTOList = unitManager.listProductAddByCategoryId(categoryId);
        List<ProductAddUnit> addUnitList = ProductAddUnit.convertByUnitDTO(unitDTOList);
        // 供货商
        List<SupplierSelectVO> supplierList = SupplierSelectVO.convertByEntity(supplierService.listAll(null));
        // 数据转换
        ProductAddVO vo = new ProductAddVO();
        vo.setCategoryId(categoryId)
                .setCategoryName(category.getTitle())
                .setParamsList(addParamsList)
                .setUnitList(addUnitList)
                .setSupplierList(supplierList);
        return vo;
    }

    @Override
    public ProductEditVO getEditVOById(Long id) {
        Product product = productService.getById(id);
        if (product == null) {
            return new ProductEditVO();
        }
        // 分类ID
        Long categoryId = product.getCategoryId();
        // 属性参数
        List<Params> paramsList = paramsService.listByCategoryId(categoryId);
        List<ProductParams> productParamsList = productParamsService.listByProductId(id);
        List<ProductEditParams> editParamsList = ProductEditParams.convert(paramsList, productParamsList);
        // 描述文案
        String productDescription = productDescriptionService.getByProductId(id);
        // sku列表
        List<ProductPrice> productPriceList = productPriceService.listByProductId(id);
        List<ProductEditPrice> editPriceList = ProductEditPrice.convertByEntity(productPriceList);
        // 规格和sku关联列表
        Map<Long, ProductUnitPrice> unitPriceMap = productUnitPriceService.mapByProductId(id);
        for (ProductEditPrice item : editPriceList) {
            ProductUnitPrice unitPrice = unitPriceMap.get(item.getId());
            if (unitPrice == null) {
                continue;
            }
            item.setSkuPath(unitPrice.getSkuPath());
        }
        // 方案
        List<ProductEditPlan> editPlanList = this.getEditPlan(id);
        // 规格
        List<UnitDTO> unitDTOList = unitManager.listProductEditByProductIdAndCategoryId(id, categoryId);
        List<ProductEditUnit> editUnitList = ProductEditUnit.convertByUnitDTO(unitDTOList);
        // 供货商
        List<SupplierSelectVO> supplierList = SupplierSelectVO.convertByEntity(supplierService.listAll(null));

        // 数据转换
        ProductEditVO vo = new ProductEditVO();
        vo.setProduct(ProductEdit.convertByEntity(product))
                .setSupplierList(supplierList)
                .setPriceList(editPriceList)
                .setParamsList(editParamsList)
                .setPlanList(editPlanList)
                .setProductDescription(productDescription)
                .setUnitList(editUnitList);
        return vo;
    }

    private List<ProductEditPlan> getEditPlan(Long id) {
        // 方案列表
        List<ProductPlan> productPlanList = productPlanService.listByProductId(id);
        List<ProductEditPlan> editPlanList = ProductEditPlan.convertByEntity(productPlanList);
        if (CollectionUtils.isNotEmpty(editPlanList)) {
            Set<Long> productIds = new HashSet<>();
            // 面料
            Map<Long, List<ProductFabric>> productFabricMap = new HashMap<>();
            List<ProductFabric> productFabricList = productFabricService.listBySourceProductId(id);
            if (CollectionUtils.isNotEmpty(productFabricList)) {
                for (ProductFabric item : productFabricList) {
                    productIds.add(item.getProductId());
                }
                productFabricMap = productFabricList.stream().collect(Collectors.groupingBy(ProductFabric::getPlanId));
            }
            // 配料
            Map<Long, List<ProductIngredient>> productIngredientMap = new HashMap<>();
            List<ProductIngredient> productIngredientList = productIngredientService.listBySourceProductId(id);
            if (CollectionUtils.isNotEmpty(productIngredientList)) {
                for (ProductIngredient item : productIngredientList) {
                    productIds.add(item.getProductId());
                }
                productIngredientMap = productIngredientList.stream().collect(Collectors.groupingBy(ProductIngredient::getPlanId));
            }
            // 辅料
            Map<Long, List<ProductSubMaterial>> productSubMaterialMap = new HashMap<>();
            List<ProductSubMaterial> productSubMaterialList = productSubMaterialService.listBySourceProductId(id);
            if (CollectionUtils.isNotEmpty(productSubMaterialList)) {
                for (ProductSubMaterial item : productSubMaterialList) {
                    productIds.add(item.getProductId());
                }
                productSubMaterialMap = productSubMaterialList.stream().collect(Collectors.groupingBy(ProductSubMaterial::getPlanId));
            }
            // 工艺
            Map<Long, List<ProductTechnology>> productTechnologyMap = productTechnologyService.mapBySourceProductId(id);
            // 生产费用
            Map<Long, List<ProductProcessFee>> productProcessFeeMap = productProcessFeeService.mapBySourceProductId(id);
            // 商品SKU列表
            Map<Long, List<ProductPrice>> productPriceMap = productPriceService.mapByProductIds(new ArrayList<>(productIds));
            for (ProductEditPlan item : editPlanList) {
                List<ProductEditPlanFabric> editFabricList = ProductEditPlanFabric.convertByEntity(productFabricMap.get(item.getId()));
                List<ProductEditPlanIngredient> editIngredientList = ProductEditPlanIngredient.convertByEntity(productIngredientMap.get(item.getId()));
                List<ProductEditPlanSubMaterial> editSubMaterialList = ProductEditPlanSubMaterial.convertByEntity(productSubMaterialMap.get(item.getId()));

                if (CollectionUtils.isNotEmpty(editFabricList)) {
                    for (ProductEditPlanFabric fabricItem : editFabricList) {
                        fabricItem.setPriceList(ProductPagePrice.convertByEntity(productPriceMap.get(fabricItem.getProductId())));
                    }
                }
                if (CollectionUtils.isNotEmpty(editIngredientList)) {
                    for (ProductEditPlanIngredient ingredientItem : editIngredientList) {
                        ingredientItem.setPriceList(ProductPagePrice.convertByEntity(productPriceMap.get(ingredientItem.getProductId())));
                    }
                }
                if (CollectionUtils.isNotEmpty(editSubMaterialList)) {
                    for (ProductEditPlanSubMaterial subMaterialItem : editSubMaterialList) {
                        subMaterialItem.setPriceList(ProductPagePrice.convertByEntity(productPriceMap.get(subMaterialItem.getProductId())));
                    }
                }

                item.setFabricList(editFabricList)
                        .setIngredientList(editIngredientList)
                        .setSubMaterialList(editSubMaterialList)
                        .setTechnologyList(ProductEditPlanTechnology.convertByEntity(productTechnologyMap.get(item.getId())))
                        .setProcessFeeList(ProductEditPlanProcessFee.convertByEntity(productProcessFeeMap.get(item.getId())));
            }
        }
        return editPlanList;
    }

    @Override
    public ProductDetailVO getDetailVOById(Long id) {
        Product product = productService.getById(id);
        if (product == null) {
            return new ProductDetailVO();
        }
        // 属性参数
        List<Params> paramsList = paramsService.listByCategoryId(product.getCategoryId());
        List<ProductParams> productParamsList = productParamsService.listByProductId(id);
        List<ProductDetailParams> detailParamsList = ProductDetailParams.convert(paramsList, productParamsList);
        // 规格
        List<UnitDTO> unitDTOList = unitManager.listProductDetailByProductId(id);
        // sku 列表
        List<ProductPrice> priceList = productPriceService.listByProductId(id);
        List<ProductDetailPrice> detailPriceList = ProductDetailPrice.convertByEntity(priceList);
        // 规格和sku关联列表
        Map<Long, ProductUnitPrice> unitPriceMap = productUnitPriceService.mapByProductId(id);
        for (ProductDetailPrice item : detailPriceList) {
            ProductUnitPrice unitPrice = unitPriceMap.get(item.getId());
            if (unitPrice == null) {
                continue;
            }
            item.setSkuPath(unitPrice.getSkuPath());
        }
        // 方案列表
        List<ProductPlan> productPlanList = productPlanService.listByProductId(id);
        List<ProductDetailPlan> detailPlanList = ProductDetailPlan.convertByEntity(productPlanList);
        if (CollectionUtils.isNotEmpty(detailPlanList)) {
            // 面料
            Map<Long, List<ProductFabric>> productFabricMap = productFabricService.mapBySourceProductId(id);
            // 配料
            Map<Long, List<ProductIngredient>> productIngredientMap = productIngredientService.mapBySourceProductId(id);
            // 辅料
            Map<Long, List<ProductSubMaterial>> productSubMaterialMap = productSubMaterialService.mapBySourceProductId(id);
            // 工艺
            Map<Long, List<ProductTechnology>> productTechnologyMap = productTechnologyService.mapBySourceProductId(id);
            // 生产费用
            Map<Long, List<ProductProcessFee>> productProcessFeeMap = productProcessFeeService.mapBySourceProductId(id);
            for (ProductDetailPlan item : detailPlanList) {
                item.setFabricList(ProductDetailPlanFabric.convertByEntity(productFabricMap.get(item.getId())));
                item.setIngredientList(ProductDetailPlanIngredient.convertByEntity(productIngredientMap.get(item.getId())));
                item.setSubMaterialList(ProductDetailPlanSubMaterial.convertByEntity(productSubMaterialMap.get(item.getId())));
                item.setTechnologyList(ProductDetailPlanTechnology.convertByEntity(productTechnologyMap.get(item.getId())));
                item.setProcessFeeList(ProductDetailPlanProcessFee.convertByEntity(productProcessFeeMap.get(item.getId())));
            }
        }
        // 描述文案
        String productDescription = productDescriptionService.getByProductId(id);

        ProductDetailVO vo = new ProductDetailVO();
        vo.setProduct(ProductDetail.convertByEntity(product))
                .setParamsList(detailParamsList)
                .setUnitList(ProductDetailUnit.convertByUnitDTO(unitDTOList))
                .setPriceList(detailPriceList)
                .setPlanList(detailPlanList)
                .setProductDescription(productDescription);
        return vo;
    }
}
