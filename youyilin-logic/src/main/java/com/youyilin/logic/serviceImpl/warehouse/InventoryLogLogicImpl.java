package com.youyilin.logic.serviceImpl.warehouse;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.logic.service.warehouse.InventoryLogLogic;
import com.youyilin.warehouse.dto.inventoryLog.InventoryLogPageQueryDTO;
import com.youyilin.warehouse.mapper.InventoryLogMapper;
import com.youyilin.warehouse.service.InventoryLogService;
import com.youyilin.warehouse.vo.inventoryLog.InventoryLogPageVO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class InventoryLogLogicImpl implements InventoryLogLogic {

    private final InventoryLogMapper inventoryLogMapper;
    private final InventoryLogService inventoryLogService;

    public InventoryLogLogicImpl(InventoryLogMapper inventoryLogMapper, InventoryLogService inventoryLogService) {
        this.inventoryLogMapper = inventoryLogMapper;
        this.inventoryLogService = inventoryLogService;
    }

    @Override
    public Pager<InventoryLogPageVO> getPageList(Page<InventoryLogPageQueryDTO> page) {
        Integer total = inventoryLogMapper.getTotal(page);
        if (total == 0) {
            return new Pager<>(0, new ArrayList<>());
        }
        List<InventoryLogPageVO> list = inventoryLogMapper.getPage(page);
        return new Pager<>(total, list);
    }

    @Override
    public List<InventoryLogPageVO> listByInventoryId(Long inventoryId) {
        if (inventoryId == null) {
            return new ArrayList<>();
        }
        return InventoryLogPageVO.convertByEntity(inventoryLogService.listByInventoryId(inventoryId));
    }

    @Override
    public List<InventoryLogPageVO> listByInventoryLotId(Long inventoryLotId) {
        if (inventoryLotId == null) {
            return new ArrayList<>();
        }
        return InventoryLogPageVO.convertByEntity(inventoryLogService.listByInventoryLotId(inventoryLotId));
    }
}
