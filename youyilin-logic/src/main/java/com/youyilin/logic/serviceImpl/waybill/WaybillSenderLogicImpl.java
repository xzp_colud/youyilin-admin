package com.youyilin.logic.serviceImpl.waybill;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.enums.BooleanEnum;
import com.youyilin.common.exception.Assert;
import com.youyilin.common.utils.MobileUtil;
import com.youyilin.logic.service.waybill.WaybillSenderLogic;
import com.youyilin.waybill.dto.sender.WaybillSenderFormDTO;
import com.youyilin.waybill.dto.sender.WaybillSenderPageQueryDTO;
import com.youyilin.waybill.entity.WaybillSender;
import com.youyilin.waybill.mapper.WaybillSenderMapper;
import com.youyilin.waybill.service.WaybillSenderService;
import com.youyilin.waybill.vo.sender.WaybillSenderEditVO;
import com.youyilin.waybill.vo.sender.WaybillSenderPageVO;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class WaybillSenderLogicImpl implements WaybillSenderLogic {

    private final WaybillSenderMapper waybillSenderMapper;
    private final WaybillSenderService waybillSenderService;

    public WaybillSenderLogicImpl(WaybillSenderMapper waybillSenderMapper, WaybillSenderService waybillSenderService) {
        this.waybillSenderMapper = waybillSenderMapper;
        this.waybillSenderService = waybillSenderService;
    }

    @Override
    public Pager<WaybillSenderPageVO> getPageList(Page<WaybillSenderPageQueryDTO> page) {
        Integer total = waybillSenderMapper.getTotal(page);
        if (total == 0) {
            return new Pager<>(0, new ArrayList<>());
        }
        List<WaybillSenderPageVO> list = waybillSenderMapper.getPage(page);
        return new Pager<>(total, list);
    }

    @Override
    public List<WaybillSenderPageVO> listAllNormal() {
        List<WaybillSender> list = waybillSenderService.listAllNormal();
        if (CollectionUtils.isEmpty(list)) {
            return new ArrayList<>();
        }
        return WaybillSenderPageVO.convertByEntity(list);
    }

    @Override
    public WaybillSenderEditVO getEditVOById(Long id) {
        WaybillSender sender = waybillSenderService.getById(id);
        return sender == null ? new WaybillSenderEditVO() : WaybillSenderEditVO.convertByEntity(sender);
    }

    @Override
    @Transactional
    public void saveSender(WaybillSenderFormDTO dto) {
        WaybillSender sender = dto.convertToEntity();
        Assert.isFalse(MobileUtil.isNotMobile(sender.getMobile()), "发件电话不正确");

        if (sender.getDefaultFlag() != null && sender.getDefaultFlag().equals(BooleanEnum.TRUE.getCode())) {
            waybillSenderService.updateDefaultFalse(sender.getId());
        }

        if (sender.getId() == null) {
            waybillSenderService.save(sender);
        } else {
            waybillSenderService.validateSender(sender.getId());
            waybillSenderService.updateById(sender);
        }
    }
}
