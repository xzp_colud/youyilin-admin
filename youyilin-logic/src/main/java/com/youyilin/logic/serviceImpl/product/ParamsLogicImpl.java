package com.youyilin.logic.serviceImpl.product;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.goods.dto.params.ParamsFormDTO;
import com.youyilin.goods.dto.params.ParamsPageQueryDTO;
import com.youyilin.goods.entity.Params;
import com.youyilin.goods.mapper.ParamsMapper;
import com.youyilin.goods.service.ParamsService;
import com.youyilin.goods.vo.params.ParamsPageVO;
import com.youyilin.goods.vo.params.ParamsVO;
import com.youyilin.logic.service.product.ParamsLogic;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ParamsLogicImpl implements ParamsLogic {

    private final ParamsMapper paramsMapper;
    private final ParamsService paramsService;

    public ParamsLogicImpl(ParamsMapper paramsMapper, ParamsService paramsService) {
        this.paramsMapper = paramsMapper;
        this.paramsService = paramsService;
    }

    @Override
    public Pager<ParamsPageVO> getPageList(Page<ParamsPageQueryDTO> page) {
        Integer total = paramsMapper.getTotal(page);
        if (total == 0) {
            return new Pager<>(0, new ArrayList<>());
        }
        List<ParamsPageVO> list = paramsMapper.getPage(page);
        return new Pager<>(total, list);
    }

    @Override
    public ParamsVO getParamsVOById(Long id) {
        Params params = paramsService.getById(id);
        return params == null ? new ParamsVO() : ParamsVO.convertByEntity(params);
    }

    @Override
    public List<ParamsVO> listAll() {
        List<Params> list = paramsService.list();
        return CollectionUtils.isEmpty(list) ? new ArrayList<>() : ParamsVO.convertByEntity(list);
    }

    @Override
    public List<ParamsVO> listByCategoryId(Long categoryId) {
        List<Params> list = paramsMapper.listByCategoryId(categoryId);
        return CollectionUtils.isEmpty(list) ? new ArrayList<>() : ParamsVO.convertByEntity(list);
    }

    @Override
    public void saveParams(ParamsFormDTO dto) {
        // 数据转换
        Params params = dto.convertToEntity();
        // 验证名称唯一性
        paramsService.validateNameUnique(params);

        if (params.getId() == null) {
            // 保存
            paramsService.save(params);
        } else {
            // 验证
            paramsService.validateParams(params.getId());
            // 更新
            paramsService.updateById(params);
        }
    }
}
