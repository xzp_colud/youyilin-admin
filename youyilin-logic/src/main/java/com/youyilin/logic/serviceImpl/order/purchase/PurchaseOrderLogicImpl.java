package com.youyilin.logic.serviceImpl.order.purchase;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.enums.BooleanEnum;
import com.youyilin.common.exception.Assert;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.common.utils.DateUtils;
import com.youyilin.goods.dto.PurchaseValidateProductDTO;
import com.youyilin.goods.entity.ProductPrice;
import com.youyilin.goods.manager.ProductManager;
import com.youyilin.goods.service.ProductPriceService;
import com.youyilin.goods.service.ProductService;
import com.youyilin.logic.convert.ProductPriceConvert;
import com.youyilin.logic.manager.impl.PurchaseOrderCallBackManagerImpl;
import com.youyilin.logic.service.order.purchase.PurchaseOrderLogic;
import com.youyilin.order.bo.purchase.PurchaseOrderSaveBO;
import com.youyilin.order.dto.purchase.*;
import com.youyilin.order.entity.OrderInventoryLog;
import com.youyilin.order.entity.PurchaseOrder;
import com.youyilin.order.entity.PurchaseOrderItem;
import com.youyilin.order.enums.OrderLogEnum;
import com.youyilin.order.enums.PurchaseOrderSourceTypeEnum;
import com.youyilin.order.enums.PurchaseOrderStatusEnum;
import com.youyilin.order.mapper.PurchaseOrderMapper;
import com.youyilin.order.service.OrderInventoryLogService;
import com.youyilin.order.service.OrderLogService;
import com.youyilin.order.service.PurchaseOrderItemService;
import com.youyilin.order.service.PurchaseOrderService;
import com.youyilin.order.utils.NoUtils;
import com.youyilin.order.vo.OrderLogDetailVO;
import com.youyilin.order.vo.purchase.*;
import com.youyilin.warehouse.dto.InventoryInDTO;
import com.youyilin.warehouse.manager.InventoryManager;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class PurchaseOrderLogicImpl implements PurchaseOrderLogic {

    private final PurchaseOrderMapper purchaseOrderMapper;
    private final PurchaseOrderService purchaseOrderService;
    private final PurchaseOrderItemService purchaseOrderItemService;
    private final ProductManager productManager;
    private final ProductService productService;
    private final ProductPriceService productPriceService;
    private final OrderLogService orderLogService;
    private final OrderInventoryLogService orderInventoryLogService;
    private final PurchaseOrderCallBackManagerImpl purchaseOrderCallBackManagerImpl;
    private final InventoryManager inventoryManager;

    public PurchaseOrderLogicImpl(PurchaseOrderMapper purchaseOrderMapper, PurchaseOrderService purchaseOrderService,
                                  PurchaseOrderItemService purchaseOrderItemService, ProductManager productManager,
                                  ProductService productService, ProductPriceService productPriceService, OrderLogService orderLogService,
                                  OrderInventoryLogService orderInventoryLogService, PurchaseOrderCallBackManagerImpl purchaseOrderCallBackManagerImpl, InventoryManager inventoryManager) {
        this.purchaseOrderMapper = purchaseOrderMapper;
        this.purchaseOrderService = purchaseOrderService;
        this.purchaseOrderItemService = purchaseOrderItemService;
        this.productManager = productManager;
        this.productService = productService;
        this.productPriceService = productPriceService;
        this.orderLogService = orderLogService;
        this.orderInventoryLogService = orderInventoryLogService;
        this.purchaseOrderCallBackManagerImpl = purchaseOrderCallBackManagerImpl;
        this.inventoryManager = inventoryManager;
    }

    @Override
    public Pager<PurchaseOrderPageVO> getPageList(Page<PurchaseOrderPageQueryDTO> page) {
        Integer total = purchaseOrderMapper.getTotal(page);
        if (total == 0) {
            return new Pager<>(0, new ArrayList<>());
        }
        List<PurchaseOrderPageVO> list = purchaseOrderMapper.getPage(page);
        return new Pager<>(total, list);
    }

    @Override
    public PurchaseOrderDetailVO getDetailVOById(Long id) {
        // 获取采购单
        PurchaseOrder po = purchaseOrderService.getById(id);
        if (po == null) {
            return new PurchaseOrderDetailVO();
        }
        // 数据转换
        PurchaseOrderDetailVO vo = new PurchaseOrderDetailVO();
        // 采购单
        vo.setOrder(PurchaseOrderDetail.convertByEntity(po));
        // 商品明细
        List<PurchaseOrderDetailItem> itemList = PurchaseOrderDetailItem.convertByEntity(purchaseOrderItemService.listByOrderId(id));
        vo.setItemList(itemList);
        // 入库明细
        List<PurchaseOrderDetailItemIn> inList = PurchaseOrderDetailItemIn.convertByEntity(orderInventoryLogService.listByPurchaseId(id));
        if (CollectionUtils.isNotEmpty(inList)) {
            Map<Long, List<PurchaseOrderDetailItemIn>> inMap = inList.stream().collect(Collectors.groupingBy(PurchaseOrderDetailItemIn::getOrderItemId));
            for (PurchaseOrderDetailItem item : itemList) {
                if (inMap.containsKey(item.getId())) {
                    item.setInList(inMap.get(item.getId()));
                }
            }
        }
        // 日志明细
//        vo.setLogList(OrderLogDetailVO.convertByEntity(orderLogService.listBySn(po.getSn())));
        vo.setLogList(new ArrayList<>());
        return vo;
    }

    @Override
    public PurchaseOrderEditVO getEditVOById(Long id) {
        // 获取采购单
        PurchaseOrder po = purchaseOrderService.getById(id);
        if (po == null) {
            return new PurchaseOrderEditVO();
        }
        // 获取明细列表
        List<PurchaseOrderItem> itemList = purchaseOrderItemService.listByOrderId(id);
        // 明细数据转换
        List<PurchaseOrderEditItem> itemVOList = PurchaseOrderEditItem.convertByEntity(itemList);
        // 产品ID集合
        Set<Long> productIds = new HashSet<>();
        itemList.forEach(item -> productIds.add(item.getProductId()));
        // SKU集合
        Map<Long, List<ProductPrice>> priceMap = productPriceService.mapByProductIds(new ArrayList<>(productIds));
        for (PurchaseOrderEditItem item : itemVOList) {
            item.setPriceList(ProductPriceConvert.convertToPurchaseOrderEditItemSku(priceMap.get(item.getProductId())));
        }
        // 设置返回VO
        PurchaseOrderEditVO vo = new PurchaseOrderEditVO();
        // 设置订单
        vo.setOrder(PurchaseOrderEdit.convertByEntity(po));
        // 设置明细
        vo.setItemList(itemVOList);
        return vo;
    }

    @Override
    @Transactional
    public void savePurchaseOrder(PurchaseOrderFormDTO dto) {
        // 创建保存BO
        PurchaseOrderSaveBO bo = new PurchaseOrderSaveBO();
        bo.setDto(dto).setId(dto.getId()).setEdit(dto.getId() != null);

        // 验证旧的采购单
        if (bo.isEdit()) {
            PurchaseOrder oldPurchaseOrder = purchaseOrderService.validatePurchaseOrderEdit(bo.getId());
            // 设置系统采购单号
            bo.setSn(oldPurchaseOrder.getSn());
        }

        // 验证商品信息
        List<PurchaseValidateProductDTO> productList = BeanHelper.map(dto.getProductList(), PurchaseValidateProductDTO.class);
        productManager.validatePurchaseProductBatch(productList);
        bo.setProductList(productList);

        // 创建订单
        PurchaseOrder order = this.createPurchaseOrder(bo);
        // 创建订单详情
        List<PurchaseOrderItem> orderItemList = this.createPurchaseOrderItem(bo);
        // 刷新订单金额
        this.refreshOrderAmount(order, orderItemList);

        // 保存订单
        if (bo.isEdit()) {
            purchaseOrderService.updateById(order);
        } else {
            purchaseOrderService.save(order);
        }

        // 删除商品明细
        purchaseOrderItemService.delByPurchaseId(order.getId());
        // 保存商品明细
        purchaseOrderItemService.saveBatch(orderItemList);

        // 日志
        orderLogService.saveLog(order.getId(), order.getSn(), OrderLogEnum.CREATE_OR_UPDATE.getCode());
    }

    /**
     * 创建采购订单
     */
    private PurchaseOrder createPurchaseOrder(PurchaseOrderSaveBO bo) {
        PurchaseOrder purchaseOrder = bo.getDto().convertToEntity();
        if (!bo.isEdit()) {
            bo.setId(IdWorker.getId()).setSn(NoUtils.getPurchaseOrderSn());
        }
        purchaseOrder.setId(bo.getId())
                .setSn(bo.getSn())
                .setStatus(PurchaseOrderStatusEnum.NO_CONFIRM.getCode())
                .setPurchaseDate(DateUtils.parseDate(bo.getDto().getPurchaseTime()))
                .setBillDate(DateUtils.parseDate(bo.getDto().getBillTime()))
                .setSourceType(PurchaseOrderSourceTypeEnum.DEFAULT.name());
        return purchaseOrder;
    }

    /**
     * 创建采购单明细
     */
    private List<PurchaseOrderItem> createPurchaseOrderItem(PurchaseOrderSaveBO bo) {
        List<PurchaseOrderItem> itemList = new ArrayList<>();
        List<PurchaseValidateProductDTO> productList = bo.getProductList();
        for (int i = 0; i < productList.size(); i++) {
            int index = i + 1;
            PurchaseValidateProductDTO product = productList.get(i);
            // 数据转换
            PurchaseOrderItem item = BeanHelper.map(product, PurchaseOrderItem.class);

            item.setOrderId(bo.getId());
            item.setSn(bo.getSn());
            item.setItemSn(bo.getSn() + "-" + index);
            item.setProductPriceId(product.getSkuId());
            item.setStatus(PurchaseOrderStatusEnum.NO_CONFIRM.getCode());

            itemList.add(item);
        }
        return itemList;
    }

    /**
     * 刷新订单金额
     */
    private void refreshOrderAmount(PurchaseOrder order, List<PurchaseOrderItem> itemList) {
        Long totalAmount = 0L;
        for (PurchaseOrderItem item : itemList) {
            totalAmount += item.getTotalAmount();
        }

        order.setOtherAmount(0L);
        order.setTotalAmount(totalAmount);

        Long totalMoney = totalAmount + order.getOtherAmount();
        order.setTotalMoney(totalMoney);
    }

    @Override
    @Transactional
    public void updateConfirm(PurchaseOrderConfirmFormDTO dto) {
        Long id = dto.getId();
        List<PurchaseOrderConfirmFormItem> confirmItemList = dto.getItemList();
        // 验证订单是否能确认
        PurchaseOrder order = purchaseOrderService.validatePurchaseOrderConfirm(id);
        // 获取明细
        List<PurchaseOrderItem> itemList = purchaseOrderItemService.listByOrderId(id);
        // 验证明细是否一致
        Assert.isFalse(CollectionUtils.isEmpty(confirmItemList) || confirmItemList.size() != itemList.size(), "采购明细异常");
        Map<Long, PurchaseOrderItem> itemMap = itemList.stream().collect(Collectors.toMap(PurchaseOrderItem::getId, item -> item));
        // 验证是否有下架的商品或SKU
        Set<Long> productIds = new HashSet<>();
        Set<Long> skuIds = new HashSet<>();
        for (PurchaseOrderItem item : itemList) {
            productIds.add(item.getProductId());
            skuIds.add(item.getProductPriceId());
        }
        // 逆向修正商品采购价
        List<ProductPrice> updatePriceList = new ArrayList<>();
        // 采购明细修正列表
        List<PurchaseOrderItem> updateItemList = new ArrayList<>();
        BigDecimal hundred = new BigDecimal("100");
        long totalAmount = 0L;
        for (PurchaseOrderConfirmFormItem item : confirmItemList) {
            PurchaseOrderItem existItem = itemMap.get(item.getId());
            Assert.notNull(existItem, "采购明细不存在");
            Assert.isFalse(item.getReportNum() == null || item.getReportNum().compareTo(BigDecimal.ZERO) <= 0, "实际采购数量不能为0");
            Assert.isFalse(item.getReportAmount() == null || item.getReportAmount().compareTo(BigDecimal.ZERO) <= 0, "实际采购金额不能为0");
            // 修正明细
            long reportAmount = item.getReportAmount().multiply(hundred).longValue();
            long totalReportAmount = reportAmount * item.getReportNum().longValue();
            PurchaseOrderItem updateItem = new PurchaseOrderItem();
            updateItem
                    .setId(item.getId())
                    .setReportNum(item.getReportNum())
                    .setReportAmount(reportAmount)
                    .setTotalReportAmount(totalReportAmount);
            updateItemList.add(updateItem);
            // 修正采购单价
            ProductPrice updatePriceItem = new ProductPrice();
            updatePriceItem.setId(existItem.getProductPriceId());
            // 是否为批发价
            if (BooleanEnum.isTrue(existItem.getWholesalePriceFlag())) {
                updatePriceItem.setWholesalePrice(reportAmount);
            } else {
                updatePriceItem.setCostPrice(reportAmount);
            }
            updatePriceList.add(updatePriceItem);
            // 订单总价
            totalAmount += totalReportAmount;
        }
        productService.validateProductSell(new ArrayList<>(productIds));
        productPriceService.validatePriceSell(new ArrayList<>(skuIds));
        // 订单确认
        purchaseOrderService.updateConfirm(id);
        // 修正订单总价
        PurchaseOrder updateOrder = new PurchaseOrder();
        updateOrder.setId(order.getId())
                .setTotalAmount(totalAmount)
                .setTotalMoney(totalAmount - order.getOtherAmount());
        purchaseOrderService.updateById(updateOrder);
        // 明细确认
        purchaseOrderItemService.updateConfirm(id);
        // 修正供货商报告价、报告数量、报告总价
        purchaseOrderItemService.updateBatchById(updateItemList);
        // 插入日志
        orderLogService.saveLog(id, order.getSn(), OrderLogEnum.SUBMIT.getCode());
        // 逆向修改商品的采购价
        productPriceService.updateBatchById(updatePriceList);
    }

    @Override
    @Transactional
    public void updateCheck(PurchaseOrderCheckFormDTO dto) {
        // 订单
        Long id = dto.getId();
        // 检验明细
        List<PurchaseOrderItem> checkList = PurchaseOrderCheckFormItem.convertToEntity(dto.getItemList());
        // 订单是否能检验
        PurchaseOrder order = purchaseOrderService.validatePurchaseOrderInventory(id);
        // 商品检验验证
        List<PurchaseOrderItem> itemList = purchaseOrderItemService.validateCheck(id, checkList);
        // 明细更新为检验
        purchaseOrderItemService.updateBatchById(itemList);
        // 日志
        List<String> logRemark = new ArrayList<>();
        itemList.forEach(item -> logRemark.add(item.getItemSn()));
        orderLogService.saveLog(order.getId(), order.getSn(), OrderLogEnum.PURCHASE_CHECK.getCode(), logRemark.toString());
    }

    @Override
    @Transactional
    public void updateInventoryBatch(PurchaseOrderInventoryBatchFormDTO dto) {
//        // 采购单ID
//        Long id = dto.getOrderId();
//        // 明细集合
//        List<Long> itemIds = dto.getItemIds();
//        // 仓库ID
//        Long warehouseId = dto.getWarehouseId();
//        // 库区ID
//        Long warehouseAreaId = dto.getWarehouseAreaId();
//        // 订单
//        PurchaseOrder order = purchaseOrderService.validatePurchaseOrderInventory(id);
//        // 产品批量入库
//        List<PurchaseOrderItem> itemList = purchaseOrderItemService.validateInventoryInBatch(id, itemIds);
//        // 仓库
//        Warehouse warehouse = warehouseService.validateWarehouse(warehouseId);
//        // 仓库库区
//        WarehouseArea warehouseArea = warehouseAreaService.validateWarehouseArea(warehouseId, warehouseAreaId);
//        // 库存日志
//        List<InventoryLog> inventoryList = new ArrayList<>();
//        List<OrderInventoryLog> orderInventoryLogList = new ArrayList<>();
//        List<String> orderItemSn = new ArrayList<>();
//        for (PurchaseOrderItem orderItem : itemList) {
//            InventoryLog log = inventoryLogService.createInventoryLog(warehouseId, warehouseAreaId, orderItem.getProductId(), orderItem.getProductPriceId(), orderItem.getItemSn(), orderItem.getReallyNum(), orderItem.getAmount());
//            inventoryList.add(log);
//
//            // 订单库存日志
//            OrderInventoryLog oil = new OrderInventoryLog();
//            oil.setOrderId(order.getId())
//                    .setSn(order.getSn())
//                    .setOrderItemId(orderItem.getId())
//                    .setItemSn(orderItem.getItemSn())
//                    .setWarehouseId(warehouse.getId())
//                    .setWarehouseName(warehouse.getName())
//                    .setWarehouseAreaId(warehouseArea.getId())
//                    .setWarehouseAreaName(warehouseArea.getName())
//                    .setQty(orderItem.getReallyNum())
//                    .setType(OrderInventoryLog.Type.IN.name());
//
//            orderInventoryLogList.add(oil);
//            orderItemSn.add(orderItem.getItemSn());
//        }
//        // 更新订单明细状态为入库
//        purchaseOrderItemService.updateInventoryBatch(itemIds);
//        // 执行入库
//        inventoryLogService.inventoryIn(inventoryList);
//        // 保存入库日志
//        orderInventoryLogService.saveBatch(orderInventoryLogList);
//        // 保存日志
//        orderLogService.saveLog(order.getId(), order.getSn(), OrderLogEnum.PURCHASE_INVENTORY_IN_BATCH.getCode(), orderItemSn.toString());
//        // 是否全部入库
//        this.checkOrderInventoryInAll(order.getId(), order.getSn());
        Assert.isTrue(true, "接口已废弃");
    }

    @Override
    @Transactional
    public void updateInventory(PurchaseOrderInventoryFormDTO dto) {
        // 采购单ID
        Long id = dto.getOrderId();
        // 采购单明细ID
        Long itemId = dto.getOrderItemId();
        // 入库明细
        List<PurchaseOrderInventoryFormItem> inItemList = dto.getItemList();
        Assert.notEmpty(inItemList, "入库明细不能为空");
        // 订单
        PurchaseOrder purchaseOrder = purchaseOrderService.validatePurchaseOrderInventory(id);
        // 验证明细是否能入库
        PurchaseOrderItem purchaseOrderItem = purchaseOrderItemService.validateInventoryIn(itemId, id);
        // 验证合计数量是否一致
        this.inventoryCheckQty(inItemList, purchaseOrderItem);
        // 入库明细
        List<InventoryInDTO> inventoryInDTOList = new ArrayList<>();
        for (PurchaseOrderInventoryFormItem inItem : inItemList) {
            String itemLotSn = NoUtils.getLotSn();
            InventoryInDTO inventoryInDTO = new InventoryInDTO();
            inventoryInDTO.setWarehouseId(inItem.getWarehouseId())
                    .setWarehouseAreaId(inItem.getWarehouseAreaId())
                    .setProductId(purchaseOrderItem.getProductId())
                    .setProductName(purchaseOrderItem.getProductName())
                    .setSkuId(purchaseOrderItem.getProductPriceId())
                    .setSkuName(purchaseOrderItem.getSkuName())
                    .setSn(itemLotSn)
                    .setLotSn(inItem.getLotSn())
                    .setQty(inItem.getQty())
                    .setSourceSn(StringUtils.isBlank(inItem.getLotSn()) ? itemLotSn : inItem.getLotSn());
            inventoryInDTOList.add(inventoryInDTO);
        }
        // 执行入库
        inventoryManager.inventoryInByPurchaseOrder(inventoryInDTOList);
        // 成功入库明细
        List<OrderInventoryLog> oilList = BeanHelper.map(inventoryInDTOList, OrderInventoryLog.class);
        for (OrderInventoryLog item : oilList) {
            item.setOrderId(purchaseOrder.getId())
                    .setSn(purchaseOrder.getSn())
                    .setOrderItemId(purchaseOrderItem.getId())
                    .setItemSn(purchaseOrderItem.getItemSn())
                    .setType(OrderInventoryLog.Type.IN.name());
        }
        // 修改明细状态
        purchaseOrderItemService.updateInventory(itemId);
        // 保存入库明细
        orderInventoryLogService.saveBatch(oilList);
        // 订单日志
        orderLogService.saveLog(purchaseOrder.getId(), purchaseOrder.getSn(), OrderLogEnum.PURCHASE_INVENTORY_IN.getCode(), purchaseOrderItem.getItemSn());

        // 是否全部入库
        this.checkOrderInventoryInAll(purchaseOrder.getId(), purchaseOrder.getSn());
    }

    private void inventoryCheckQty(List<PurchaseOrderInventoryFormItem> inItemList, PurchaseOrderItem purchaseOrderItem) {
        BigDecimal dbQty = purchaseOrderItem.getReviseNum() != null ? purchaseOrderItem.getReviseNum() : purchaseOrderItem.getReallyNum();
        BigDecimal totalQty = BigDecimal.ZERO;
        for (PurchaseOrderInventoryFormItem inItem : inItemList) {
            Assert.notNull(inItem.getWarehouseId(), "仓库不能为空");
            Assert.notNull(inItem.getWarehouseAreaId(), "库区不能为空");
            Assert.notNull(inItem.getQty(), "数量不能为空");

            totalQty = totalQty.add(inItem.getQty());
        }
        totalQty = totalQty.setScale(2, RoundingMode.HALF_UP);
        boolean qtyFlag = dbQty.compareTo(totalQty) == 0;
        Assert.isTrue(qtyFlag, "入库数量不等于检验数量");
    }

    /**
     * 明细是否全部入库
     *
     * @param orderId 订单
     * @param OrderSn 订单编号
     */
    private void checkOrderInventoryInAll(Long orderId, String OrderSn) {
        boolean isAllIn = purchaseOrderItemService.isAllInventoryIn(orderId);
        if (isAllIn) {
            // 订单完结
            purchaseOrderService.updateFinish(orderId);
            // 日志
            orderLogService.saveLog(orderId, OrderSn, OrderLogEnum.FINISH.getCode(), null);
            // 反馈入库完成
            purchaseOrderCallBackManagerImpl.callBackFinish(orderId);
        }
    }

    @Override
    @Transactional
    public void updateCancel(PurchaseOrderCancelFormDTO dto) {
        Long id = dto.getId();
        // 验证订单
        PurchaseOrder order = purchaseOrderService.validatePurchaseOrderCancel(id);
        // 订单取消
        purchaseOrderService.updateCancel(id);
        // 明细取消
        purchaseOrderItemService.updateCancel(id);
        // 插入日志
        orderLogService.saveLog(id, order.getSn(), OrderLogEnum.SUBMIT.getCode());
        // 反馈
        purchaseOrderCallBackManagerImpl.callBackCancel(id);
    }
}
