package com.youyilin.logic.serviceImpl.warehouse;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.exception.Assert;
import com.youyilin.logic.service.warehouse.WarehouseAreaLogic;
import com.youyilin.warehouse.dto.warehouseArea.WarehouseAreaDeleteFormDTO;
import com.youyilin.warehouse.dto.warehouseArea.WarehouseAreaFormDTO;
import com.youyilin.warehouse.dto.warehouseArea.WarehouseAreaPageQueryDTO;
import com.youyilin.warehouse.entity.Warehouse;
import com.youyilin.warehouse.entity.WarehouseArea;
import com.youyilin.warehouse.manager.InventoryManager;
import com.youyilin.warehouse.mapper.WarehouseAreaMapper;
import com.youyilin.warehouse.service.InventoryService;
import com.youyilin.warehouse.service.WarehouseAreaService;
import com.youyilin.warehouse.service.WarehouseService;
import com.youyilin.warehouse.vo.warehouseArea.WarehouseAreaEditVO;
import com.youyilin.warehouse.vo.warehouseArea.WarehouseAreaPageVO;
import com.youyilin.warehouse.vo.warehouseArea.WarehouseAreaVO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class WarehouseAreaLogicImpl implements WarehouseAreaLogic {

    private final WarehouseAreaMapper warehouseAreaMapper;
    private final WarehouseAreaService warehouseAreaService;
    private final WarehouseService warehouseService;
    private final InventoryService inventoryService;
    private final InventoryManager inventoryManager;

    public WarehouseAreaLogicImpl(WarehouseAreaMapper warehouseAreaMapper, WarehouseAreaService warehouseAreaService,
                                  WarehouseService warehouseService, InventoryService inventoryService,
                                  InventoryManager inventoryManager) {
        this.warehouseAreaMapper = warehouseAreaMapper;
        this.warehouseAreaService = warehouseAreaService;
        this.warehouseService = warehouseService;
        this.inventoryService = inventoryService;
        this.inventoryManager = inventoryManager;
    }

    @Override
    public Pager<WarehouseAreaPageVO> getPageList(Page<WarehouseAreaPageQueryDTO> page) {
        Integer total = warehouseAreaMapper.getTotal(page);
        if (total == 0) {
            return new Pager<>(0, new ArrayList<>());
        }
        List<WarehouseAreaPageVO> list = warehouseAreaMapper.getPage(page);
        return new Pager<>(total, list);
    }

    @Override
    public List<WarehouseAreaVO> listAll() {
        return WarehouseAreaVO.convertByEntity(warehouseAreaService.list());
    }

    @Override
    public List<WarehouseAreaVO> listAllByWarehouseId(Long warehouseId) {
        if (warehouseId == null) {
            return new ArrayList<>();
        }
        return WarehouseAreaVO.convertByEntity(warehouseAreaService.listAllByWarehouseId(warehouseId));
    }

    @Override
    public WarehouseAreaEditVO getEditVOById(Long id) {
        WarehouseArea dbWarehouseArea = warehouseAreaService.getById(id);
        return dbWarehouseArea == null ? new WarehouseAreaEditVO() : WarehouseAreaEditVO.convertByEntity(dbWarehouseArea);
    }

    @Override
    public void saveWarehouseArea(WarehouseAreaFormDTO dto) {
        // 数据转换
        WarehouseArea warehouseArea = dto.convertToEntity();
        warehouseArea.setSn(dto.getName());
        // 验证仓库
        Warehouse warehouse = warehouseService.validateWarehouse(dto.getWarehouseId());
        // 验证编号唯一性
        warehouseAreaService.validateSnUnique(warehouseArea);
        if (warehouseArea.getId() == null) {
            // 保存
            warehouseAreaService.save(warehouseArea);
        } else {
            // 禁止修改所属仓库
            WarehouseArea oldWarehouseArea = warehouseAreaService.validateWarehouseArea(warehouseArea.getId());
            Assert.isTrue(oldWarehouseArea.getWarehouseId().equals(warehouse.getId()), "禁止修改所属仓库");
            // 更新
            warehouseAreaService.updateById(warehouseArea);
        }
    }

    @Override
    @Transactional
    public void delWarehouseArea(WarehouseAreaDeleteFormDTO dto) {
        Long warehouseAreaId = dto.getId();
        // 验证库区
        warehouseAreaService.validateWarehouseArea(warehouseAreaId);
        // 验证库存余量
        int inventoryNum = inventoryService.countProductNumByWarehouseAreaId(warehouseAreaId);
        Assert.isFalse(inventoryNum > 0, "当前库区还有产品库存余量");
        // 删除库存信息
        inventoryManager.delByWarehouseAreaId(warehouseAreaId);
        // 移除库区
        warehouseAreaService.removeById(warehouseAreaId);
    }
}
