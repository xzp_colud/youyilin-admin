package com.youyilin.logic.serviceImpl.customer;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.exception.ApiException;
import com.youyilin.common.utils.CharterUtil;
import com.youyilin.common.utils.MobileUtil;
import com.youyilin.customer.dto.customer.CustomerFormDTO;
import com.youyilin.customer.dto.customer.CustomerPageQueryDTO;
import com.youyilin.customer.entity.Customer;
import com.youyilin.customer.mapper.CustomerMapper;
import com.youyilin.customer.service.CustomerService;
import com.youyilin.customer.vo.customer.CustomerDetailVO;
import com.youyilin.customer.vo.customer.CustomerEditVO;
import com.youyilin.customer.vo.customer.CustomerPageVO;
import com.youyilin.logic.service.customer.CustomerLogic;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerLogicImpl implements CustomerLogic {

    private final CustomerMapper customerMapper;
    private final CustomerService customerService;

    public CustomerLogicImpl(CustomerMapper customerMapper, CustomerService customerService) {
        this.customerMapper = customerMapper;
        this.customerService = customerService;
    }

    @Override
    public Pager<CustomerPageVO> getPageList(Page<CustomerPageQueryDTO> page) {
        Integer total = customerMapper.getTotal(page);
        if (total == 0) {
            return new Pager<>(0, new ArrayList<>());
        }
        List<CustomerPageVO> list = customerMapper.getPage(page);
        return new Pager<>(total, list);
    }

    @Override
    public CustomerEditVO getEditVOById(Long id) {
        Customer dbCustomer = customerService.getById(id);
        return dbCustomer == null ? new CustomerEditVO() : CustomerEditVO.convertByEntity(dbCustomer);
    }

    @Override
    public CustomerDetailVO getDetailVOById(Long id) {
        Customer dbCustomer = customerService.getById(id);
        return dbCustomer == null ? new CustomerDetailVO() : CustomerDetailVO.convertByEntity(dbCustomer);
    }

    @Override
    public void saveCustomer(CustomerFormDTO dto) {
        Customer en = dto.convertToEntity();

        if (!CharterUtil.characterStringOrNum(en.getUserName())) {
            throw new ApiException("用户名只能输入数字和英文");
        }
        if (MobileUtil.isNotMobile(en.getPhone())) {
            throw new ApiException("联系电话不正确");
        }
        customerService.validateUserNameUnique(en);
        customerService.validatePhoneUnique(en);

        if (en.getId() == null) {
            // 初始化密码
            en.setPassword(customerService.getResetPassword(en.getPhone()));
            customerService.save(en);
        } else {
            customerService.updateById(en);
        }
    }
}
