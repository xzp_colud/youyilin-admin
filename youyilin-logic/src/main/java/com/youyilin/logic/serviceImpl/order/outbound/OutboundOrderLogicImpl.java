package com.youyilin.logic.serviceImpl.order.outbound;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.exception.Assert;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.goods.dto.OutboundValidateProductDTO;
import com.youyilin.goods.entity.ProductPrice;
import com.youyilin.goods.manager.ProductManager;
import com.youyilin.goods.service.ProductPriceService;
import com.youyilin.logic.convert.ProductPriceConvert;
import com.youyilin.logic.manager.OutboundOrderCallBackManger;
import com.youyilin.logic.service.order.outbound.OutboundOrderLogic;
import com.youyilin.order.bo.outbound.OutboundOrderSaveBO;
import com.youyilin.order.dto.outbound.*;
import com.youyilin.order.entity.OutboundOrder;
import com.youyilin.order.entity.OutboundOrderInventoryLog;
import com.youyilin.order.entity.OutboundOrderItem;
import com.youyilin.order.entity.OutboundOrderItemDetail;
import com.youyilin.order.enums.CommonOutboundStatusEnum;
import com.youyilin.order.enums.OrderLogEnum;
import com.youyilin.order.enums.OutboundOrderItemStatusEnum;
import com.youyilin.order.mapper.OutboundOrderMapper;
import com.youyilin.order.service.*;
import com.youyilin.order.utils.NoUtils;
import com.youyilin.order.vo.OrderLogDetailVO;
import com.youyilin.order.vo.outbound.*;
import com.youyilin.warehouse.dto.InventoryOutDTO;
import com.youyilin.warehouse.manager.InventoryManager;
import com.youyilin.warehouse.service.InventoryLogService;
import com.youyilin.warehouse.service.InventoryService;
import com.youyilin.warehouse.service.WarehouseAreaService;
import com.youyilin.warehouse.service.WarehouseService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class OutboundOrderLogicImpl implements OutboundOrderLogic {

    private final OutboundOrderMapper outboundOrderMapper;
    private final OutboundOrderCallBackManger outboundOrderCallBackManger;
    private final OutboundOrderService outboundOrderService;
    private final OutboundOrderItemService outboundOrderItemService;
    private final OutboundOrderItemDetailService outboundOrderItemDetailService;
    private final OutboundOrderInventoryLogService outboundOrderInventoryLogService;
    private final OrderLogService orderLogService;
    private final InventoryService inventoryService;
    private final InventoryLogService inventoryLogService;
    private final WarehouseService warehouseService;
    private final WarehouseAreaService warehouseAreaService;
    private final ProductManager productManager;
    private final ProductPriceService productPriceService;
    @Autowired
    private InventoryManager inventoryManager;

    public OutboundOrderLogicImpl(OutboundOrderService outboundOrderService, OutboundOrderMapper outboundOrderMapper,
                                  OutboundOrderCallBackManger outboundOrderCallBackManger, ProductManager productManager,
                                  OutboundOrderItemService outboundOrderItemService, OutboundOrderItemDetailService outboundOrderItemDetailService,
                                  OutboundOrderInventoryLogService outboundOrderInventoryLogService, OrderLogService orderLogService,
                                  InventoryService inventoryService, ProductPriceService productPriceService,
                                  InventoryLogService inventoryLogService, WarehouseService warehouseService,
                                  WarehouseAreaService warehouseAreaService) {
        this.outboundOrderService = outboundOrderService;
        this.outboundOrderMapper = outboundOrderMapper;
        this.outboundOrderCallBackManger = outboundOrderCallBackManger;
        this.productManager = productManager;
        this.outboundOrderItemService = outboundOrderItemService;
        this.outboundOrderItemDetailService = outboundOrderItemDetailService;
        this.outboundOrderInventoryLogService = outboundOrderInventoryLogService;
        this.orderLogService = orderLogService;
        this.inventoryService = inventoryService;
        this.productPriceService = productPriceService;
        this.inventoryLogService = inventoryLogService;
        this.warehouseService = warehouseService;
        this.warehouseAreaService = warehouseAreaService;
    }

    @Override
    public Pager<OutboundOrderPageVO> getPageList(Page<OutboundOrderPageQueryDTO> page) {
        Integer total = outboundOrderMapper.getTotal(page);
        if (total == 0) {
            return new Pager<>(0, new ArrayList<>());
        }
        List<OutboundOrderPageVO> list = outboundOrderMapper.getPage(page);
        return new Pager<>(total, list);
    }

    @Override
    public OutboundOrderDetailVO getDetailVOById(Long id) {
        // 出库单
        OutboundOrder outbound = outboundOrderService.getById(id);
        if (outbound == null) {
            return new OutboundOrderDetailVO();
        }
        // 商品合计明细
        List<OutboundOrderDetailItem> itemList = OutboundOrderDetailItem.convertByEntity(outboundOrderItemService.listByOutboundId(id));
        // 细分明细
        List<OutboundOrderItemDetail> itemDetailList = outboundOrderItemDetailService.listByOutboundId(id);
        Map<Long, List<OutboundOrderItemDetail>> itemDetailMap = itemDetailList.stream().collect(Collectors.groupingBy(OutboundOrderItemDetail::getOrderItemId));
        // 出库明细
        List<OutboundOrderInventoryLog> inventoryLogList = outboundOrderInventoryLogService.listByOutboundId(id);
        Map<Long, List<OutboundOrderInventoryLog>> inventoryLogMap = inventoryLogList.stream().collect(Collectors.groupingBy(OutboundOrderInventoryLog::getOutboundItemId));
        // 设置关联信息
        for (OutboundOrderDetailItem item : itemList) {
            item.setDetailList(OutboundOrderDetailItemDetail.convertByEntity(itemDetailMap.get(item.getId())));
            item.setInventoryLogList(OutboundOrderDetailItemOut.convertByEntity(inventoryLogMap.get(item.getId())));
        }
        // 返回VO
        OutboundOrderDetailVO vo = new OutboundOrderDetailVO();
        vo.setOrder(OutboundOrderDetail.convertByEntity(outbound))
                .setOrderItemList(itemList)
                .setLogList(new ArrayList<>());
//                .setLogList(OrderLogDetailVO.convertByEntity(orderLogService.listBySn(outbound.getSn())));
        return vo;
    }

    @Override
    public OutboundOrderEditVO getEditVOById(Long id) {
        // 出库单
        OutboundOrder outbound = outboundOrderService.getById(id);
        if (outbound == null) {
            return new OutboundOrderEditVO();
        }
        // 商品明细
        List<OutboundOrderEditItem> itemList = OutboundOrderEditItem.convertByEntity(outboundOrderItemService.listByOutboundId(id));
        // 商品IDS
        Set<Long> productIds = new HashSet<>();
        itemList.forEach(item -> productIds.add(item.getProductId()));
        Map<Long, List<ProductPrice>> skuMap = productPriceService.mapByProductIds(new ArrayList<>(productIds));
        // 设置SKU信息
        for (OutboundOrderEditItem item : itemList) {
            item.setPriceList(ProductPriceConvert.convertToOutboundOrderEditItemSku(skuMap.get(item.getProductId())));
        }
        OutboundOrderEditVO vo = new OutboundOrderEditVO();
        vo.setOrder(OutboundOrderEdit.convertByEntity(outbound))
                .setItemList(itemList);
        return vo;
    }

    @Override
    @Transactional
    public void saveOutbound(OutboundOrderFormDTO dto) {
        OutboundOrderSaveBO bo = new OutboundOrderSaveBO();
        bo.setDto(dto).setId(dto.getId()).setEdit(dto.getId() != null);
        if (bo.isEdit()) {
            OutboundOrder oldOutbound = outboundOrderService.validateEdit(bo.getId());
            bo.setSn(oldOutbound.getSn());
        }
        // 产品验证
        List<OutboundValidateProductDTO> itemList = BeanHelper.map(dto.getItemList(), OutboundValidateProductDTO.class);
        productManager.validateOutboundProductBatch(itemList);
        // 构建出库单
        this.constructOutbound(bo);
        // 构建出库单明细
        this.constructOutboundItem(bo, itemList);
        // 持久化出库单
        this.save(bo);
    }

    /**
     * 构建出库单
     */
    private void constructOutbound(OutboundOrderSaveBO bo) {
        OutboundOrder outbound = bo.getDto().convertToEntity();

        outbound.setStatus(CommonOutboundStatusEnum.DEFAULT.getCode())
                .setSourceType(OutboundOrder.SourceType.DEFAULT.name());

        bo.setOutboundOrder(outbound);
        if (bo.isEdit()) {
            return;
        }
        outbound.setId(IdWorker.getId()).setSn(NoUtils.getOutboundOrderSn());
        bo.setId(outbound.getId()).setSn(outbound.getSn());
    }

    /**
     * 构建出库单明细
     */
    private void constructOutboundItem(OutboundOrderSaveBO bo, List<OutboundValidateProductDTO> sourceList) {
        List<OutboundOrderItem> itemList = BeanHelper.map(sourceList, OutboundOrderItem.class);

        List<OutboundOrderItemDetail> itemDetailList = new ArrayList<>();
        for (int i = 0; i < itemList.size(); i++) {
            int index = i + 1;

            Long itemId = IdWorker.getId();
            OutboundOrderItem item = itemList.get(i);
            item.setId(itemId)
                    .setItemSn(bo.getSn() + "-" + index)
                    .setOrderId(bo.getId())
                    .setSn(bo.getSn())
                    .setNoOutQty(item.getQty())
                    .setStatus(OutboundOrderItemStatusEnum.NO_CONFIRM.getCode());

            OutboundOrderItemDetail itemDetail = new OutboundOrderItemDetail();
            itemDetail.setSourceGroupId(bo.getId())
                    .setSourceId(itemId)
                    .setSourceSn(item.getItemSn())
                    .setOrderId(bo.getId())
                    .setOrderItemId(itemId)
                    .setQty(item.getQty())
                    .setStatus(OutboundOrderItemStatusEnum.NO_CONFIRM.getCode());
            itemDetailList.add(itemDetail);
        }

        bo.setItemList(itemList).setItemDetailList(itemDetailList);
    }

    /**
     * 持久化出库单
     */
    private void save(OutboundOrderSaveBO bo) {
        if (bo.isEdit()) {
            outboundOrderService.updateById(bo.getOutboundOrder());
        } else {
            outboundOrderService.save(bo.getOutboundOrder());
        }
        // 删除明细
        outboundOrderItemService.delByOutboundId(bo.getId());
        // 删除明细详情
        outboundOrderItemDetailService.delByOutboundId(bo.getId());
        // 保存明细
        outboundOrderItemService.saveBatch(bo.getItemList());
        // 保存明细详情
        outboundOrderItemDetailService.saveBatch(bo.getItemDetailList());
        // 日志
        orderLogService.saveLog(bo.getId(), bo.getSn(), OrderLogEnum.CREATE_OR_UPDATE.getCode());
    }

    @Override
    @Transactional
    public void updateConfirm(OutboundOrderConfirmFormDTO dto) {
        Long id = dto.getId();
        // 验证出库单
        OutboundOrder oo = outboundOrderService.validateOutboundConfirm(id);
        // 更新状态为已确认
        outboundOrderService.updateConfirm(id);
        // 更新出库单明细为已确认
        outboundOrderItemService.updateConfirmByOutboundId(id);
        // 更新出库单明细为已确认
        outboundOrderItemDetailService.updateConfirmByOutboundId(id);
        // 日志
        orderLogService.saveLog(id, oo.getSn(), OrderLogEnum.OUTBOUND_CONFIRM.getCode());
    }

    @Override
    @Transactional
    public void updateInventory(OutboundOrderInventoryFormDTO dto) {
        // 出库单
        Long id = dto.getOrderId();
        // 出库单明细
        Long itemId = dto.getOrderItemId();
        // 出库总数
        BigDecimal totalQty = BigDecimal.ZERO;
        for (OutboundOrderInventoryFormOut item : dto.getOutList()) {
            Assert.isFalse(item.getQty().compareTo(BigDecimal.ZERO) <= 0, "数量不能为零");
            totalQty = totalQty.add(item.getQty());
        }
        // 验证出库单是否可以出库
        OutboundOrder outboundOrder = outboundOrderService.validateOutboundInventory(id);
        // 验证出库明细是否可以出库
        OutboundOrderItem outboundOrderItem = outboundOrderItemService.validateOutboundItemInventory(itemId, totalQty);
        Assert.isTrue(outboundOrderItem.getOrderId().equals(outboundOrder.getId()), "订单信息不一致");
        // 出库明细
        List<InventoryOutDTO> outList = new ArrayList<>();
        for (OutboundOrderInventoryFormOut item : dto.getOutList()) {
            InventoryOutDTO out = new InventoryOutDTO();
            out.setId(item.getId())
                    .setProductId(outboundOrderItem.getProductId())
                    .setProductName(outboundOrderItem.getProductName())
                    .setSkuId(outboundOrderItem.getSkuId())
                    .setSkuName(outboundOrderItem.getSkuName())
                    .setQty(item.getQty())
                    .setSourceSn(outboundOrderItem.getItemSn());
            outList.add(out);
        }
        // 执行出库
        inventoryManager.inventoryOutByOutboundOrder(outList);
        // 出库单出库明细
        List<OutboundOrderInventoryLog> inventoryLogList = BeanHelper.map(outList, OutboundOrderInventoryLog.class);
        for (OutboundOrderInventoryLog item : inventoryLogList) {
            item.setId(null)
                    .setSn(NoUtils.getOutboundOrderInventorySn())
                    .setOutboundId(outboundOrder.getId())
                    .setOutboundSn(outboundOrder.getSn())
                    .setOutboundItemId(outboundOrderItem.getId())
                    .setOutboundItemSn(outboundOrderItem.getItemSn());
        }
        outboundOrderInventoryLogService.saveBatch(inventoryLogList);
        // 商品明细出库
        outboundOrderItemService.updateInventory(itemId);
        // 日志
        orderLogService.saveLog(outboundOrder.getId(), outboundOrder.getSn(), OrderLogEnum.OUTBOUND_INVENTORY_OUT.getCode(), outboundOrderItem.getItemSn());
        // 详情出库
        outboundOrderItemDetailService.updateOutByOutboundItemId(itemId);
        // 是否全部出库
        if (outboundOrderItemService.isInventoryOutAll(outboundOrder.getId())) {
            // 订单完结
            outboundOrderService.updateFinish(outboundOrder.getId());
            // 日志
            orderLogService.saveLog(outboundOrder.getId(), outboundOrder.getSn(), OrderLogEnum.FINISH.getCode(), outboundOrderItem.getItemSn());
        }
        // 反馈出库信息
        outboundOrderCallBackManger.callBackInventory(id, itemId);
    }

    @Override
    @Transactional
    public void updateCancel(OutboundOrderCancelFormDTO dto) {
        Long id = dto.getId();
        // 验证出库单
        OutboundOrder oo = outboundOrderService.validateOutboundCancel(id);
        boolean isFalse = StringUtils.isNotBlank(oo.getSourceType()) && !oo.getSourceType().equals(OutboundOrder.SourceType.DEFAULT.name());
        Assert.isFalse(isFalse, "该出库单不支持取消");
        // 更新状态为已取消
        outboundOrderService.updateCancel(id);
        // 更新出库单明细为已取消
        outboundOrderItemService.updateCancelByOutboundId(id);
        // 更新出库单明细为已取消
        outboundOrderItemDetailService.updateCancelByOutboundId(id);
        // 日志
        orderLogService.saveLog(id, oo.getSn(), OrderLogEnum.CANCEL.getCode());
        // 反馈出库信息
        outboundOrderCallBackManger.callBackCancel(id);
    }
}
