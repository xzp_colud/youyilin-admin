package com.youyilin.logic.serviceImpl.order.mini;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.youyilin.common.bean.Page;
import com.youyilin.common.exception.ApiException;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.common.utils.DateUtils;
import com.youyilin.common.utils.MobileUtil;
import com.youyilin.customer.entity.CustomerAddress;
import com.youyilin.customer.entity.CustomerCart;
import com.youyilin.customer.entity.CustomerMini;
import com.youyilin.customer.service.CustomerAddressService;
import com.youyilin.customer.service.CustomerCartService;
import com.youyilin.customer.service.CustomerMiniService;
import com.youyilin.goods.manager.RequestOrderProductLogic;
import com.youyilin.goods.model.request.RequestMiniProduct;
import com.youyilin.logic.manager.WaybillManager;
import com.youyilin.logic.service.order.mini.CustomerMiniOrderLogic;
import com.youyilin.order.entity.*;
import com.youyilin.order.enums.CommonOutboundStatusEnum;
import com.youyilin.order.enums.CustomerOrderStatusEnum;
import com.youyilin.order.enums.OutboundOrderItemStatusEnum;
import com.youyilin.order.enums.OutboundOrderStatusEnum;
import com.youyilin.order.model.dto.OutboundOrderItemDto;
import com.youyilin.order.model.dto.RequestMiniOrderDto;
import com.youyilin.order.model.request.RequestMiniOrder;
import com.youyilin.order.model.request.RequestMiniOrderAddress;
import com.youyilin.order.model.request.RequestMiniOrderSend;
import com.youyilin.order.model.vo.CustomerOrderItemVo;
import com.youyilin.order.model.vo.CustomerOrderVo;
import com.youyilin.order.service.*;
import com.youyilin.order.utils.NoUtils;
import com.youyilin.waybill.bo.WaybillThirdConstructBO;
import com.youyilin.waybill.dto.waybill.WaybillItemThirdConstructDTO;
import com.youyilin.waybill.enums.WaybillSourceType;
import com.youyilin.wechat.model.entity.WechatPayment;
import com.youyilin.wechat.model.response.JsApiResponse;
import com.youyilin.wechat.service.WechatPaymentService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service
public class CustomerMiniOrderLogicImpl implements CustomerMiniOrderLogic {

    private final RequestOrderProductLogic requestOrderProductLogic;
    private final CustomerMiniService customerMiniService;
    private final CustomerCartService customerCartService;
    private final CustomerAddressService customerAddressService;
    private final CustomerOrderService customerOrderService;
    private final CustomerOrderItemService customerOrderItemService;
    private final CustomerOrderLogService customerOrderLogService;
    private final CustomerOrderFinanceService customerOrderFinanceService;
    private final WechatPaymentService wechatPaymentService;
    private final WaybillManager waybillManager;
    private final OutboundOrderService outboundOrderService;
    private final OutboundOrderItemService outboundOrderItemService;
    private final OutboundOrderItemDetailService outboundOrderItemDetailService;

    public CustomerMiniOrderLogicImpl(RequestOrderProductLogic requestOrderProductLogic, CustomerMiniService customerMiniService,
                                      CustomerCartService customerCartService, CustomerAddressService customerAddressService,
                                      CustomerOrderService customerOrderService, CustomerOrderItemService customerOrderItemService,
                                      CustomerOrderLogService customerOrderLogService, CustomerOrderFinanceService customerOrderFinanceService,
                                      WechatPaymentService wechatPaymentService, WaybillManager waybillManager, OutboundOrderService outboundOrderService, OutboundOrderItemService outboundOrderItemService, OutboundOrderItemDetailService outboundOrderItemDetailService) {
        this.requestOrderProductLogic = requestOrderProductLogic;
        this.customerMiniService = customerMiniService;
        this.customerCartService = customerCartService;
        this.customerAddressService = customerAddressService;
        this.customerOrderService = customerOrderService;
        this.customerOrderItemService = customerOrderItemService;
        this.customerOrderLogService = customerOrderLogService;
        this.customerOrderFinanceService = customerOrderFinanceService;
        this.wechatPaymentService = wechatPaymentService;
        this.waybillManager = waybillManager;
        this.outboundOrderService = outboundOrderService;
        this.outboundOrderItemService = outboundOrderItemService;
        this.outboundOrderItemDetailService = outboundOrderItemDetailService;
    }

    @Override
    public List<CustomerOrderVo> getPage(Integer current, Integer pageSize, Integer status) {
        CustomerMini cm = customerMiniService.isLoginNoException();
        if (cm == null) return new ArrayList<>();

        CustomerOrder search = new CustomerOrder();
        search.setStatus(status);
        search.setCustomerId(cm.getId());
        search.setOpenid(cm.getOpenid());
        Page<CustomerOrder> page = new Page<>();
        page.setPageNum(current);
        page.setPageSize(pageSize);
        page.setSearch(search);
        List<CustomerOrder> list = customerOrderService.getPage(page);
        List<CustomerOrderVo> voList = BeanHelper.map(list, CustomerOrderVo.class);
        this.refreshOrderItem(voList);
        return voList;
    }

    @Override
    public CustomerOrderVo getOrder(Long id) {
        CustomerMini cm = customerMiniService.isLoginNoException();
        if (cm == null) return new CustomerOrderVo();

        CustomerOrder order = customerOrderService.getById(id);
        if (order == null || !order.getOpenid().equals(cm.getOpenid())) {
            return new CustomerOrderVo();
        }
        CustomerOrderVo vo = BeanHelper.map(order, CustomerOrderVo.class);

        List<CustomerOrderVo> voList = new ArrayList<>();
        voList.add(vo);
        this.refreshOrderItem(voList);

        return voList.get(0);
    }

    private void refreshOrderItem(List<CustomerOrderVo> list) {
        if (CollectionUtils.isEmpty(list)) return;

        List<Long> orderIds = new ArrayList<>();
        list.forEach(item -> orderIds.add(item.getId()));

        List<CustomerOrderItem> orderItemList = customerOrderItemService.listByOrderIds(orderIds);
        if (CollectionUtils.isEmpty(orderItemList)) return;

        List<CustomerOrderItemVo> itemVoList = BeanHelper.map(orderItemList, CustomerOrderItemVo.class);
        Map<Long, List<CustomerOrderItemVo>> itemVoMap = itemVoList.stream().collect(Collectors.groupingBy(CustomerOrderItemVo::getOrderId));
        for (CustomerOrderVo item : list) {
            List<CustomerOrderItemVo> findList = itemVoMap.get(item.getId());
            if (CollectionUtils.isEmpty(findList)) continue;

            item.setItemList(findList);
        }
    }

    @Override
    @Transactional
    public String saveOrder(RequestMiniOrder request) {
        RequestMiniOrderDto dto = new RequestMiniOrderDto();
        dto.setRequest(request);
        this.initCustomer(dto);
        this.initCustomerAddress(dto);
        this.validateProduct(dto);
        this.createOrder(dto);
        this.createOrderItem(dto);
        customerOrderService.save(dto.getOrder());
        customerOrderItemService.saveBatch(dto.getItemList());
        customerCartService.removeBatchByIds(request.getCartIds());

        Long orderId = dto.getOrder().getId();
        String orderSn = dto.getOrder().getSn();
        customerOrderLogService.saveLog(orderId, orderSn, "订单保存", JSONObject.toJSONString(dto.getOrder()));

        return orderId.toString();
    }

    @Override
    @Transactional
    public void cancel(Long id) {
        CustomerMini cm = customerMiniService.isLogin();
        CustomerOrder co = customerOrderService.validateStatusToCancel(id);
        if (co == null || !co.getOpenid().equals(cm.getOpenid())) {
            throw new ApiException("取消失败");
        }
        customerOrderService.updateStatusCancel(id);

        customerOrderLogService.saveLog(co.getId(), co.getSn(), "订单取消", JSONObject.toJSONString(co));
    }

    @Override
    @Transactional
    public JsApiResponse pay(Long id) {
        log.info("【订单付款】");
        CustomerMini cm = customerMiniService.isLogin();
        CustomerOrder co = customerOrderService.validateStatusToPay(id);
        if (!cm.getOpenid().equals(co.getOpenid())) {
            throw new ApiException("付款异常");
        }

        JsApiResponse jsApiResponse = wechatPaymentService.miniPay(cm.getOpenid(), cm.getAppId(), co.getTotalMoney(), co.getId(), co.getSn(), "订单付款");

        customerOrderLogService.saveLog(co.getId(), co.getSn(), "发起付款", JSONObject.toJSONString(co));
        return jsApiResponse;
    }

    @Override
    public void payCancel(Long orderId) {
        CustomerOrder co = customerOrderService.getById(orderId);
        if (co == null) return;

        if (co.getStatus() != null && co.getStatus().equals(CustomerOrderStatusEnum.DEFAULT.getCode())) {
            customerOrderLogService.saveLog(co.getId(), co.getSn(), "付款取消", JSONObject.toJSONString(co));
        }
    }

    @Override
    @Transactional
    public void paySuccess(Long paymentId, String transactionId, String paySuccessTime, String typeName, String outTradeNo) {
        WechatPayment payment = wechatPaymentService.getById(paymentId);
        if (payment == null) {
            throw new ApiException("付款单号不存在");
        }
        CustomerOrder co = customerOrderService.validateStatusToPay(payment.getSourceId());
        // 订单付款成功
        customerOrderService.updateStatusPaySuccess(co.getId());
        // 微信记录成功
        wechatPaymentService.updateSuccess(paymentId, transactionId, paySuccessTime);
        // 订单付款记录
        customerOrderFinanceService.saveFinance(co.getId(), co.getSn(), transactionId, payment.getAmount(), paySuccessTime, typeName, outTradeNo);
        // 操作日志
        customerOrderLogService.saveLog(co.getId(), co.getSn(), "付款成功", JSONObject.toJSONString(co));
    }

    @Override
    @Transactional
    public void receive(Long id) {
        CustomerMini cm = customerMiniService.isLogin();
        CustomerOrder co = customerOrderService.validateStatusToReceive(id);
        if (!cm.getOpenid().equals(co.getOpenid())) {
            throw new ApiException("订单异常");
        }
        customerOrderService.updateStatusReceive(id);
        customerOrderLogService.saveLog(co.getId(), co.getSn(), "订单收货", JSONObject.toJSONString(co));
    }

    @Override
    @Transactional
    public void outbound(List<Long> orderIds) {
        // 验证
        List<CustomerOrder> coList = this.checkCustomerOrderOutbound(orderIds);
        // 创建出库单
        OutboundOrder oo = new OutboundOrder();
        oo.setId(IdWorker.getId());
        oo.setSn(NoUtils.getOutboundOrderSn());
        oo.setStatus(OutboundOrderStatusEnum.NO_CONFIRM.getCode());
        oo.setSourceType(OutboundOrder.SourceType.CUSTOMER_ORDER.name());
        // 创建出库单子集
        List<OutboundOrderItemDto> ooiList = this.createOutboundItem(oo, orderIds);
        List<OutboundOrderItem> ooiSaveList = BeanHelper.map(ooiList, OutboundOrderItem.class);
        List<OutboundOrderItemDetail> ooiDetailSaveList = new ArrayList<>();
        ooiList.forEach(item -> ooiDetailSaveList.addAll(item.getDetailList()));
        // 保存
        outboundOrderService.save(oo);
        outboundOrderItemService.saveBatch(ooiSaveList);
        outboundOrderItemDetailService.saveBatch(ooiDetailSaveList);
        // 反馈出库单信息
        for (CustomerOrder item : coList) {
            // 操作日志
            customerOrderLogService.saveLog(item.getId(), item.getSn(), "申请出库", JSONObject.toJSONString(item));
        }
        customerOrderService.updateOutboundProcessByOutboundId(oo.getId(), oo.getSn(), orderIds);
    }

    private List<CustomerOrder> checkCustomerOrderOutbound(List<Long> orderIds) {
        List<CustomerOrder> coList = customerOrderService.listByIds(orderIds);
        if (CollectionUtils.isEmpty(coList)) {
            throw new ApiException("未查询到订单信息");
        }
        if (orderIds.size() != coList.size()) {
            throw new ApiException("订单信息不一致");
        }
        for (CustomerOrder item : coList) {
            if (item.getStatus() == null || !item.getStatus().equals(CustomerOrderStatusEnum.PAID_NO_SEND.getCode())) {
                throw new ApiException(item.getSn() + " 订单禁止操作");
            }
            if (item.getOutboundId() != null) {
                throw new ApiException(item.getSn() + " 订单已申请出库");
            }
        }

        return coList;
    }

    private List<OutboundOrderItemDto> createOutboundItem(OutboundOrder oo, List<Long> orderIds) {
        List<CustomerOrderItem> coiList = customerOrderItemService.listByOrderIds(orderIds);
        Map<Long, List<CustomerOrderItem>> coiMap = coiList.stream().collect(Collectors.groupingBy(CustomerOrderItem::getSkuId));

        List<OutboundOrderItemDto> ooiList = new ArrayList<>();
        int index = 1;
        for (Long skuId : coiMap.keySet()) {
            List<CustomerOrderItem> itemList = coiMap.get(skuId);
            CustomerOrderItem item = itemList.get(0);
            BigDecimal qty = BigDecimal.ZERO;

            Long itemId = IdWorker.getId();
            List<OutboundOrderItemDetail> detailList = new ArrayList<>();
            for (CustomerOrderItem customerOrderItem : itemList) {
                qty = qty.add(customerOrderItem.getNum());
                // 拆分明细
                OutboundOrderItemDetail itemDetail = new OutboundOrderItemDetail();
                itemDetail.setOrderId(oo.getId())
                        .setOrderItemId(itemId)
                        .setSourceGroupId(customerOrderItem.getOrderId())
                        .setSourceId(customerOrderItem.getId())
                        .setSourceSn(customerOrderItem.getSn())
                        .setQty(customerOrderItem.getNum())
                        .setStatus(OutboundOrderItemStatusEnum.NO_INVENTORY_OUT.getCode());

                detailList.add(itemDetail);
            }

            OutboundOrderItemDto ooi = BeanHelper.map(item, OutboundOrderItemDto.class);
            ooi.setId(itemId)
                    .setOrderId(oo.getId())
                    .setQty(qty)
                    .setNoOutQty(qty)
                    .setSn(oo.getSn())
                    .setItemSn(oo.getSn() + "_" + index++)
                    .setStatus(OutboundOrderItemStatusEnum.NO_CONFIRM.getCode());
            ooi.setDetailList(detailList);

            ooiList.add(ooi);
        }

        return ooiList;
    }

    @Override
    @Transactional
    public void send(RequestMiniOrderSend request) {
        // 订单
        CustomerOrder co = customerOrderService.validateStatusToSend(request.getOrderId());
        // 明细
        List<CustomerOrderItem> itemList = customerOrderItemService.listByOrderId(co.getId());
        // 货运单BO
        List<WaybillItemThirdConstructDTO> itemSourceList = new ArrayList<>();
        for (CustomerOrderItem item : itemList) {
            WaybillItemThirdConstructDTO itemSource = new WaybillItemThirdConstructDTO();
            itemSource.setSourceId(item.getId())
                    .setSourceNo(item.getSn())
                    .setProductId(item.getProductId())
                    .setProductName(item.getProductName())
                    .setProductPriceId(item.getSkuId())
                    .setSkuName(item.getSkuName())
                    .setNum(item.getNum());

            itemSourceList.add(itemSource);
        }
        WaybillThirdConstructBO waybillBO = new WaybillThirdConstructBO();
        waybillBO.setSourceType(WaybillSourceType.SALE_ORDER.name())
                .setReceiveName(co.getName())
                .setReceiveMobile(co.getMobile())
                .setReceiveProvince(co.getProvince())
                .setReceiveCity(co.getCity())
                .setReceiveArea(co.getArea())
                .setReceiveDetail(co.getDetail())
                .setItemSourceList(itemSourceList);
        // 生成货运单
        waybillManager.constructWaybill(waybillBO);
        // 订单发货
        customerOrderService.updateStatusSend(co.getId());
        // 操作日志
        customerOrderLogService.saveLog(co.getId(), co.getSn(), "申请发货", JSONObject.toJSONString(co));
    }

    @Override
    @Transactional
    public void updateAddress(RequestMiniOrderAddress request) {
        if (MobileUtil.isNotMobile(request.getMobile())) {
            throw new ApiException("手机号不正确");
        }
        CustomerOrder co = customerOrderService.validateStatusToSend(request.getOrderId());

        CustomerOrder updateCo = new CustomerOrder();
        updateCo.setId(co.getId())
                .setName(request.getName())
                .setMobile(request.getMobile())
                .setProvince(request.getProvince())
                .setCity(request.getCity())
                .setArea(request.getArea())
                .setDetail(request.getDetail());

        customerOrderService.updateById(updateCo);

        // 操作日志
        customerOrderLogService.saveLog(co.getId(), co.getSn(), "修改收货地址", JSONObject.toJSONString(co));
    }

    private void initCustomer(RequestMiniOrderDto dto) {
        CustomerMini cm = customerMiniService.isLogin();
        dto.setCm(cm);
    }

    private void initCustomerAddress(RequestMiniOrderDto dto) {
        CustomerAddress address = customerAddressService.getById(dto.getRequest().getAddressId());
        if (address == null || !address.getCustomerId().equals(dto.getCm().getId())) {
            throw new ApiException("地址异常");
        }
        dto.setAddress(address);
    }

    private void validateProduct(RequestMiniOrderDto dto) {
        CustomerMini cm = dto.getCm();
        List<Long> cartIds = dto.getRequest().getCartIds();
        List<CustomerCart> cartList = customerCartService.listByIds(cartIds);
        if (CollectionUtils.isEmpty(cartList)) {
            throw new ApiException("未选择产品");
        }

        List<RequestMiniProduct> requestMiniProductList = new ArrayList<>();
        for (CustomerCart item : cartList) {
            if (!item.getOpenid().equals(cm.getOpenid())) {
                throw new ApiException("产品异常");
            }
            RequestMiniProduct miniProduct = new RequestMiniProduct();
            miniProduct.setProductId(item.getProductId());
            miniProduct.setSkuId(item.getSkuId());
            miniProduct.setNum(item.getNum());

            requestMiniProductList.add(miniProduct);
        }

        requestOrderProductLogic.validateMiniProductBatch(requestMiniProductList);

        dto.setProductList(requestMiniProductList);
    }

    private void createOrder(RequestMiniOrderDto dto) {
        CustomerMini cm = dto.getCm();
        CustomerAddress address = dto.getAddress();
        Date countDownTime = DateUtils.addHours(new Date(), 2);
        CustomerOrder order = new CustomerOrder();
        order.setId(IdWorker.getId())
                .setSn(NoUtils.getCustomerOrderSn())
                .setCustomerId(cm.getId())
                .setOpenid(cm.getOpenid())
                .setName(address.getReceiverName())
                .setMobile(address.getReceiverPhone())
                .setProvince(address.getProvince())
                .setCity(address.getCity())
                .setArea(address.getArea())
                .setDetail(address.getDetail())
                .setTotalAmount(0L)
                .setTotalDiscount(0L)
                .setTotalOther(0L)
                .setTotalMoney(0L)
                .setTotalCost(0L)
                .setStatus(CustomerOrderStatusEnum.DEFAULT.getCode())
                .setOutboundStatus(CommonOutboundStatusEnum.DEFAULT.getCode())
                .setCountDownTime(countDownTime)
                .setRemark(dto.getRequest().getRemark());
        dto.setOrder(order);
    }

    private void createOrderItem(RequestMiniOrderDto dto) {
        CustomerOrder order = dto.getOrder();
        List<CustomerOrderItem> itemList = BeanHelper.map(dto.getProductList(), CustomerOrderItem.class);
        Long totalSell = 0L;
        Long totalCost = 0L;
        for (int i = 0; i < itemList.size(); i++) {
            CustomerOrderItem item = itemList.get(i);
            item.setOrderId(order.getId());
            item.setSn(order.getSn() + "_" + (i + 1));
            item.setTotalSell(new BigDecimal(item.getSellAmount()).multiply(item.getNum()).longValue());
            item.setTotalCost(new BigDecimal(item.getCostAmount()).multiply(item.getNum()).longValue());
            item.setTotalDiscount(0L);
            item.setTotalMoney(item.getTotalSell());

            totalSell += item.getTotalSell();
            totalCost += item.getTotalCost();
        }
        order.setTotalAmount(totalSell);
        order.setTotalCost(totalCost);
        order.setTotalMoney(totalSell);

        dto.setItemList(itemList);
    }
}
