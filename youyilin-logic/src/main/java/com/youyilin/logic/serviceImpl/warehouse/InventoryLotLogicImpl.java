package com.youyilin.logic.serviceImpl.warehouse;

import com.youyilin.common.exception.ApiException;
import com.youyilin.common.exception.Assert;
import com.youyilin.goods.entity.Product;
import com.youyilin.goods.entity.ProductPrice;
import com.youyilin.goods.service.ProductPriceService;
import com.youyilin.goods.service.ProductService;
import com.youyilin.logic.convert.InventoryLogConvert;
import com.youyilin.logic.service.warehouse.InventoryLotLogic;
import com.youyilin.order.utils.NoUtils;
import com.youyilin.warehouse.dto.InventoryInDTO;
import com.youyilin.warehouse.dto.InventoryLot.InventoryLotCheckFormDTO;
import com.youyilin.warehouse.dto.InventoryLot.InventoryLotDeleteFormDTO;
import com.youyilin.warehouse.dto.InventoryLot.InventoryLotMoveFormDTO;
import com.youyilin.warehouse.dto.InventoryLot.InventoryLotSaveFormDTO;
import com.youyilin.warehouse.dto.InventoryOutDTO;
import com.youyilin.warehouse.entity.*;
import com.youyilin.warehouse.enums.InventoryLogTypeEnum;
import com.youyilin.warehouse.manager.InventoryManager;
import com.youyilin.warehouse.service.*;
import com.youyilin.warehouse.vo.inventoryLot.InventoryLotPageVO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class InventoryLotLogicImpl implements InventoryLotLogic {

    private final InventoryLotService inventoryLotService;
    private final InventoryService inventoryService;
    private final InventoryLogService inventoryLogService;
    private final ProductService productService;
    private final ProductPriceService productPriceService;
    private final WarehouseService warehouseService;
    private final WarehouseAreaService warehouseAreaService;
    private final InventoryManager inventoryManager;

    public InventoryLotLogicImpl(InventoryLotService inventoryLotService, InventoryService inventoryService,
                                 InventoryLogService inventoryLogService, ProductService productService,
                                 ProductPriceService productPriceService, WarehouseService warehouseService,
                                 WarehouseAreaService warehouseAreaService, InventoryManager inventoryManager) {
        this.inventoryLotService = inventoryLotService;
        this.inventoryService = inventoryService;
        this.inventoryLogService = inventoryLogService;
        this.productService = productService;
        this.productPriceService = productPriceService;
        this.warehouseService = warehouseService;
        this.warehouseAreaService = warehouseAreaService;
        this.inventoryManager = inventoryManager;
    }

    @Override
    public List<InventoryLotPageVO> listByInventoryId(Long inventoryId) {
        return InventoryLotPageVO.conventByEntity(inventoryLotService.listByInventoryId(inventoryId));
    }

    @Override
    @Transactional
    public void saveInventoryLot(InventoryLotSaveFormDTO dto) {
        BigDecimal qty = dto.getQty();
        Assert.isFalse(qty.compareTo(BigDecimal.ZERO) < 0, "盘点数量不能小于0");
        // 商品
        Product product = productService.validateProduct(dto.getProductId());
        // SKU
        ProductPrice sku = productPriceService.validatePrice(dto.getSkuId());
        InventoryInDTO in = new InventoryInDTO();
        in.setProductId(dto.getProductId())
                .setProductName(product.getName())
                .setSkuId(sku.getId())
                .setSkuName(sku.getSkuName())
                .setSn(NoUtils.getLotSn())
                .setWarehouseId(dto.getWarehouseId())
                .setWarehouseAreaId(dto.getWarehouseAreaId())
                .setLotSn(dto.getLotSn())
                .setQty(dto.getQty());
        inventoryManager.saveLot(in);
    }

    @Override
    @Transactional
    public void updateInventoryLotCheckOut(InventoryLotCheckFormDTO dto) {
        // 库存批次ID
        Long lotId = dto.getId();
        // 数量
        BigDecimal qty = dto.getQty();
        // 获取库存批次
        InventoryLot lot = inventoryLotService.validateInventoryLot(lotId);
        // 库存信息
        Inventory inventory = inventoryService.validateInventory(lot.getInventoryId());
        // 商品
        Product product = productService.validateProduct(inventory.getProductId());
        // SKu
        ProductPrice price = productPriceService.validatePrice(inventory.getSkuId());
        // 出库列表
        InventoryOutDTO out = new InventoryOutDTO();
        out.setSourceSn(lot.getLotSn())
                .setId(lotId)
                .setQty(qty)
                .setProductId(product.getId())
                .setProductName(product.getName())
                .setSkuId(price.getId())
                .setSkuName(price.getSkuName());
        // 执行出库
        inventoryManager.checkOut(out);
    }

    @Override
    @Transactional
    public void updateInventoryLotCheckIn(InventoryLotCheckFormDTO dto) {
        // 数量
        BigDecimal qty = dto.getQty();
        // 库存批次ID
        Long lotId = dto.getId();
        // 获取库存批次
        InventoryLot lot = inventoryLotService.validateInventoryLot(lotId);
        // 库存信息
        Inventory inventory = inventoryService.validateInventory(lot.getInventoryId());
        // 入库列表
        lot.setQty(lot.getQty().add(qty));
        inventory.setQty(inventory.getQty().add(qty));
        // 日志
        this.doSaveLog(lot, inventory, qty, InventoryLogTypeEnum.CHECK_IN);
        // 执行入库
        inventoryService.updateById(inventory);
        inventoryLotService.updateById(lot);
    }

    @Override
    @Transactional
    public void updateInventoryLotMove(InventoryLotMoveFormDTO dto) {
        // 库存信息
        InventoryLot lot = inventoryLotService.validateInventoryLot(dto.getId());
        if (lot.getQty().compareTo(BigDecimal.ZERO) <= 0) {
            throw new ApiException("余量为0，无需操作");
        }
        Inventory inventory = inventoryService.validateInventory(lot.getInventoryId());
        if (inventory.getWarehouseId().equals(dto.getWarehouseId()) && inventory.getWarehouseAreaId().equals(dto.getWarehouseAreaId())) {
            throw new ApiException("已在当前库区，无需操作");
        }
        if (inventory.getFrozenQty() != null && inventory.getFrozenQty().compareTo(BigDecimal.ZERO) > 0) {
            throw new ApiException("存在冻结库存不能移动");
        }
        // 商品
        Product product = productService.validateProduct(inventory.getProductId());
        // SKU
        ProductPrice productPrice = productPriceService.validatePrice(inventory.getSkuId());
        // 出库DTO
        InventoryOutDTO out = new InventoryOutDTO();
        out.setId(lot.getId())
                .setQty(lot.getQty())
                .setProductId(inventory.getProductId())
                .setProductName(product.getName())
                .setSkuId(inventory.getSkuId())
                .setSkuName(productPrice.getSkuName())
                .setSourceSn(lot.getLotSn());
        // 入库DTO
        String lotSnNew = NoUtils.getLotSn();
        InventoryInDTO in = new InventoryInDTO();
        in.setSourceSn(lot.getLotSn())
                .setSn(lotSnNew)
                .setLotSn(lot.getLotSn())
                .setQty(lot.getQty())
                .setProductId(inventory.getProductId())
                .setProductName(product.getName())
                .setSkuId(inventory.getSkuId())
                .setSkuName(productPrice.getSkuName())
                .setWarehouseId(dto.getWarehouseId())
                .setWarehouseAreaId(dto.getWarehouseAreaId());
        // 先出库
        List<InventoryOutDTO> outList = new ArrayList<>();
        outList.add(out);
        inventoryManager.moveOut(outList);
        // 再入库
        List<InventoryInDTO> inList = new ArrayList<>();
        inList.add(in);
        inventoryManager.moveIn(inList);
    }

    @Override
    @Transactional
    public void delInventoryLot(InventoryLotDeleteFormDTO dto) {
        // 验证批次
        InventoryLot lot = inventoryLotService.validateInventoryLot(dto.getId());
        boolean noQty = lot.getQty() != null && lot.getQty().compareTo(BigDecimal.ZERO) == 0;
        Assert.isTrue(noQty, "库存数量不为零");
        // 库存信息
        Inventory inventory = inventoryService.validateInventory(lot.getInventoryId());
        // 保存日志
        this.doSaveLog(lot, inventory, BigDecimal.ZERO, InventoryLogTypeEnum.DELETE);
        // 删除
        inventoryLotService.removeById(lot.getId());
    }

    /**
     * 保存日志
     */
    private void doSaveLog(InventoryLot lot, Inventory inventory, BigDecimal qty, InventoryLogTypeEnum typeEnum) {
        // 商品
        Product product = productService.validateProduct(inventory.getProductId());
        // SKu
        ProductPrice price = productPriceService.validatePrice(inventory.getSkuId());
        // 仓库
        Warehouse warehouse = warehouseService.validateWarehouse(inventory.getWarehouseId());
        // 库区
        WarehouseArea warehouseArea = warehouseAreaService.validateWarehouseArea(inventory.getWarehouseAreaId());
        // 日志
        InventoryLog log = InventoryLogConvert.convertBy(inventory, lot, product, price, warehouse, warehouseArea, qty, typeEnum);
        inventoryLogService.save(log);
    }
}
