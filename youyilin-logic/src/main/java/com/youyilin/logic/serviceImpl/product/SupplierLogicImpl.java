package com.youyilin.logic.serviceImpl.product;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.enums.BooleanEnum;
import com.youyilin.common.utils.ChineseCharToEnUtil;
import com.youyilin.goods.dto.supplier.SupplierFormDTO;
import com.youyilin.goods.dto.supplier.SupplierPageQueryDTO;
import com.youyilin.goods.entity.Supplier;
import com.youyilin.goods.mapper.SupplierMapper;
import com.youyilin.goods.service.ProductService;
import com.youyilin.goods.service.SupplierService;
import com.youyilin.goods.vo.supplier.SupplierPageVO;
import com.youyilin.goods.vo.supplier.SupplierVO;
import com.youyilin.logic.service.product.SupplierLogic;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SupplierLogicImpl implements SupplierLogic {

    private final SupplierMapper supplierMapper;
    private final SupplierService supplierService;
    private final ProductService productService;

    public SupplierLogicImpl(SupplierMapper supplierMapper, SupplierService supplierService, ProductService productService) {
        this.supplierMapper = supplierMapper;
        this.supplierService = supplierService;
        this.productService = productService;
    }

    @Override
    public Pager<SupplierPageVO> getPageList(Page<SupplierPageQueryDTO> page) {
        Integer total = supplierMapper.getTotal(page);
        if (total == 0) {
            return new Pager<>(total, new ArrayList<>());
        }
        List<SupplierPageVO> list = supplierMapper.getPage(page);
        return new Pager<>(total, list);
    }

    @Override
    public SupplierVO getVOById(Long id) {
        Supplier supplier = supplierService.getById(id);
        return supplier == null ? null : SupplierVO.convertByEntity(supplier);
    }

    @Override
    public List<SupplierVO> listAll() {
        List<Supplier> list = supplierService.listAll(BooleanEnum.TRUE.getCode());
        return SupplierVO.convertByEntity(list);
    }

    @Override
    public void saveSupplier(SupplierFormDTO dto) {
        // 数据转换
        Supplier supplier = dto.convertToEntity();
        // 验证供货商名称是否唯一
        supplierService.validateNameUnique(supplier);
        supplier.setPinyin(ChineseCharToEnUtil.getPinYinHeadChar(supplier.getName()));

        if (supplier.getId() == null) {
            // 保存
            supplierService.save(supplier);
        } else {
            // 验证供货商是否存在
            supplierService.validateSupplier(dto.getId());
            // 更新
            supplierService.updateById(supplier);
        }
        // 冗余更新商品中的供货商名称
        productService.updateRedundancySupplierName(dto.getId(), dto.getName());
    }
}
