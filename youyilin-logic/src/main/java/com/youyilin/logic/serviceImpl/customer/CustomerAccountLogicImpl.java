package com.youyilin.logic.serviceImpl.customer;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.exception.Assert;
import com.youyilin.customer.dto.account.CustomerAccountChargeFormDTO;
import com.youyilin.customer.dto.account.CustomerAccountFormDTO;
import com.youyilin.customer.dto.account.CustomerAccountPageQueryDTO;
import com.youyilin.customer.entity.CustomerAccount;
import com.youyilin.customer.entity.CustomerAccountLog;
import com.youyilin.customer.enums.AccountOptEnum;
import com.youyilin.customer.mapper.CustomerAccountMapper;
import com.youyilin.customer.service.CustomerAccountLogService;
import com.youyilin.customer.service.CustomerAccountService;
import com.youyilin.customer.service.CustomerService;
import com.youyilin.customer.vo.account.CustomerAccountPageVO;
import com.youyilin.logic.service.customer.CustomerAccountLogic;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerAccountLogicImpl implements CustomerAccountLogic {

    private final CustomerAccountMapper customerAccountMapper;
    private final CustomerAccountService customerAccountService;
    private final CustomerAccountLogService customerAccountLogService;
    private final CustomerService customerService;

    public CustomerAccountLogicImpl(CustomerAccountMapper customerAccountMapper, CustomerAccountService customerAccountService,
                                    CustomerAccountLogService customerAccountLogService, CustomerService customerService) {
        this.customerAccountMapper = customerAccountMapper;
        this.customerAccountService = customerAccountService;
        this.customerAccountLogService = customerAccountLogService;
        this.customerService = customerService;
    }

    @Override
    public Pager<CustomerAccountPageVO> getPageList(Page<CustomerAccountPageQueryDTO> page) {
        Integer total = customerAccountMapper.getTotal(page);
        if (total == 0) {
            return new Pager<>(0, new ArrayList<>());
        }
        List<CustomerAccountPageVO> list = customerAccountMapper.getPage(page);
        return new Pager<>(total, list);
    }

    @Override
    public List<CustomerAccountPageVO> listByCustomerId(Long customerId) {
        return CustomerAccountPageVO.convertByEntity(customerAccountService.listByCustomerId(customerId));
    }

    @Override
    public void saveAccount(CustomerAccountFormDTO dto) {
        // 将数据传输对象转换为实体对象
        CustomerAccount account = dto.convertToEntity();
        // 验证客户是否有效
        customerService.validateCustomer(account.getCustomerId());
        // 验证账户是否唯一
        customerAccountService.validateUnique(account);
        // 设置账户总额为0
        account.setTotal(0L);
        // 设置账户余额为0
        account.setResidue(0L);
        // 保存账户信息
        customerAccountService.save(account);
    }

    @Override
    public void updateCharge(CustomerAccountChargeFormDTO dto) {
        // 验证账户是否有效
        CustomerAccount account = customerAccountService.validateAccount(dto.getAccountId());
        Assert.isTrue(account.isBalance(), "账户异常");
        Long amount = dto.getChargeAmount().multiply(new BigDecimal("100")).longValue();
        Assert.isTrue(amount > 0L, "金额需大于零");
        // 验证客户是否有效
        customerService.validateCustomer(account.getCustomerId());
        // 充值
        customerAccountService.updateCharge(account.getId(), amount);

        // 日志
        CustomerAccountLog log = customerAccountLogService.createLog(account.getCustomerId(), dto.getAccountId(), AccountOptEnum.RECHARGE.getCode(), dto.getOptDate(), amount, account.getResidue(), dto.getRemark(), dto.getImageUrl());
        log.setPayType(dto.getPayType());
        customerAccountLogService.saveLog(log);
    }
}
