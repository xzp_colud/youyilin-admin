package com.youyilin.logic.serviceImpl.order.sales;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.logic.service.order.sales.OrderCollectLogic;
import com.youyilin.order.dto.sales.OrderCollectPageQueryDTO;
import com.youyilin.order.enums.OrderCollectEnum;
import com.youyilin.order.mapper.OrderCollectMapper;
import com.youyilin.order.service.OrderCollectService;
import com.youyilin.order.vo.sales.OrderCollectPageVO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderCollectLogicImpl implements OrderCollectLogic {

    private final OrderCollectMapper orderCollectMapper;
    private final OrderCollectService orderCollectService;

    public OrderCollectLogicImpl(OrderCollectMapper orderCollectMapper, OrderCollectService orderCollectService) {
        this.orderCollectMapper = orderCollectMapper;
        this.orderCollectService = orderCollectService;
    }

    @Override
    public Pager<OrderCollectPageVO> getPageList(Page<OrderCollectPageQueryDTO> page) {
        Integer total = orderCollectMapper.getTotal(page);
        if (total == 0) {
            return new Pager<>(0, new ArrayList<>());
        }
        List<OrderCollectPageVO> list = orderCollectMapper.getPage(page);
        return new Pager<>(total, list);
    }

    @Override
    public List<OrderCollectPageVO> listByOrderId(Long orderId) {
        if (orderId == null) {
            return new ArrayList<>();
        }
        return OrderCollectPageVO.convertByEntity(orderCollectService.listByOrderId(orderId, OrderCollectEnum.SUCCESS.getCode()));
    }
}
