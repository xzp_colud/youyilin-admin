package com.youyilin.logic.serviceImpl.order.refund;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.exception.Assert;
import com.youyilin.common.utils.SecurityUtil;
import com.youyilin.customer.bo.CustomerAccountRefundBO;
import com.youyilin.customer.manager.CustomerManager;
import com.youyilin.logic.service.order.refund.OrderRefundLogic;
import com.youyilin.order.dto.refund.OrderRefundCheckDTO;
import com.youyilin.order.dto.refund.OrderRefundPageQueryDTO;
import com.youyilin.order.dto.refund.OrderRefundSaveDTO;
import com.youyilin.order.entity.Order;
import com.youyilin.order.entity.OrderCollect;
import com.youyilin.order.entity.OrderRefund;
import com.youyilin.order.enums.OrderCollectEnum;
import com.youyilin.order.enums.OrderLogEnum;
import com.youyilin.order.mapper.OrderRefundMapper;
import com.youyilin.order.service.OrderCollectService;
import com.youyilin.order.service.OrderLogService;
import com.youyilin.order.service.OrderRefundService;
import com.youyilin.order.service.OrderService;
import com.youyilin.order.utils.NoUtils;
import com.youyilin.order.vo.refund.OrderRefundCheckVO;
import com.youyilin.order.vo.refund.OrderRefundDetailVO;
import com.youyilin.order.vo.refund.OrderRefundPageVO;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class OrderRefundLogicImpl implements OrderRefundLogic {

    private final OrderRefundMapper orderRefundMapper;
    private final OrderRefundService orderRefundService;
    private final OrderService orderService;
    private final OrderCollectService orderCollectService;
    private final OrderLogService orderLogService;
    private final CustomerManager customerManager;

    public OrderRefundLogicImpl(OrderRefundMapper orderRefundMapper, OrderRefundService orderRefundService,
                                OrderService orderService, OrderCollectService orderCollectService,
                                OrderLogService orderLogService, CustomerManager customerManager) {
        this.orderRefundMapper = orderRefundMapper;
        this.orderRefundService = orderRefundService;
        this.orderService = orderService;
        this.orderCollectService = orderCollectService;
        this.orderLogService = orderLogService;
        this.customerManager = customerManager;
    }

    @Override
    public Pager<OrderRefundPageVO> getPageList(Page<OrderRefundPageQueryDTO> page) {
        Integer total = orderRefundMapper.getTotal(page);
        if (total == 0) {
            return new Pager<>(0, new ArrayList<>());
        }
        List<OrderRefundPageVO> list = orderRefundMapper.getPage(page);
        return new Pager<>(total, list);
    }

    @Override
    public OrderRefundCheckVO getCheckVOById(Long id) {
        OrderRefund refund = orderRefundService.getById(id);
        return refund == null ? new OrderRefundCheckVO() : OrderRefundCheckVO.convertByEntity(refund);
    }

    @Override
    public OrderRefundDetailVO getDetailVOById(Long id) {
        OrderRefund refund = orderRefundService.getById(id);
        return refund == null ? new OrderRefundDetailVO() : OrderRefundDetailVO.convertByEntity(refund);
    }

    @Override
    @Transactional
    public void addRefund(OrderRefundSaveDTO dto) {
        // 销售单ID
        Long saleOrderId = dto.getOrderId();
        // 申请人
        Long applyId = SecurityUtil.getUserId();
        String applyName = SecurityUtil.getUsername();
        // 验证是否存在退款记录
        orderRefundService.validateExcludeFail(saleOrderId);
        // 验证订单是否能退款
        Order order = orderService.validateCanRefund(saleOrderId);

        // 保存退款记录
        OrderRefund refund = new OrderRefund();
        refund.setSn(NoUtils.getRefundOrderPrefix())
                .setSourceId(saleOrderId)
                .setSourceSn(order.getSn())
                .setCustomerId(order.getCustomerId())
                .setCustomerName(order.getCustomerName())
                .setAmount(order.getSellMoney())
                .setRemark(dto.getRemark())
                .setStatus(OrderRefund.Status.WAITING.name())
                .setApplyId(applyId)
                .setApplyName(applyName)
                .setApplyDate(new Date());
        orderRefundService.save(refund);
        // 更新订单状态为退款中
        orderService.updateRefundProcessing(saleOrderId);
        // 订单日志
        orderLogService.saveLog(order.getId(), order.getSn(), OrderLogEnum.REFUND.getCode());
        // 退款单创建
        orderLogService.saveLog(refund.getId(), refund.getSn(), OrderLogEnum.CREATE_OR_UPDATE.getCode());
    }

    @Override
    @Transactional
    public void updateCheck(OrderRefundCheckDTO dto) {
        boolean isCheckSuccess = dto.getStatus().equals(OrderRefund.Status.SUCCESS.name());
        if (isCheckSuccess) {
            this.checkSuccess(dto);
        } else {
            this.checkFail(dto);
        }
    }

    private void checkSuccess(OrderRefundCheckDTO dto) {
        // 退款单ID
        Long refundId = dto.getId();
        // 验证是否可以审核
        OrderRefund refund = orderRefundService.validateCanCheck(refundId);
        // 获取源单
        Order order = orderService.validateOrder(refund.getSourceId());
        // 收款记录
        CustomerAccountRefundBO bo = this.constructCustomerRefund(order.getId(), dto.getRemark());
        bo.setSourceId(refund.getId()).setSourceNo(refund.getSn());
        // 操作客户账户退款
        customerManager.updateAccountRefund(bo);
        // 记录成功
        orderRefundService.updateSuccess(refund.getId(), dto.getRemark());
        // 更新状态为已成功
        orderService.updateRefundSuccess(order.getId());
        // 退款单日志
        orderLogService.saveLog(refund.getId(), refund.getSn(), OrderLogEnum.REFUND_APPLY_CHECK.getCode());
        // 订单日志
        orderLogService.saveLog(order.getId(), order.getSn(), OrderLogEnum.SALE_ORDER_REFUND_SUCCESS.getCode());
    }

    private CustomerAccountRefundBO constructCustomerRefund(Long orderId, String remark) {
        List<OrderCollect> collectList = orderCollectService.listByOrderId(orderId, OrderCollectEnum.SUCCESS.getCode());
        Assert.isFalse(CollectionUtils.isEmpty(collectList) || collectList.size() != 1, "未查询到订单付款记录");
        OrderCollect collect = collectList.get(0);
        CustomerAccountRefundBO bo = new CustomerAccountRefundBO();
        bo.setCustomerId(collect.getCustomerId())
                .setAccountId(collect.getAccountId())
                .setAmount(collect.getAmount())
                .setRemark(remark);
        return bo;
    }

    private void checkFail(OrderRefundCheckDTO dto) {
        // 验证是否可以审核
        OrderRefund refund = orderRefundService.validateCanCheck(dto.getId());
        // 获取源单
        Order order = orderService.validateOrder(refund.getSourceId());
        // 更新状态为失败
        orderRefundService.updateFail(refund.getId(), dto.getRemark());
        // 更新状态为已取消
        orderService.updateRefundFail(order.getId());
        // 退款单日志
        orderLogService.saveLog(refund.getId(), refund.getSn(), OrderLogEnum.REFUND_APPLY_CHECK.getCode());
        // 源单日志
        orderLogService.saveLog(order.getId(), order.getSn(), OrderLogEnum.SALE_ORDER_REFUND_FAIL.getCode());
    }
}
