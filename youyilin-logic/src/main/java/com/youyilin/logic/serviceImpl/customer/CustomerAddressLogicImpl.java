package com.youyilin.logic.serviceImpl.customer;

import com.youyilin.common.enums.BooleanEnum;
import com.youyilin.customer.dto.address.CustomerAddressDefaultDTO;
import com.youyilin.customer.dto.address.CustomerAddressDeleteDTO;
import com.youyilin.customer.dto.address.CustomerAddressFormDTO;
import com.youyilin.customer.entity.CustomerAddress;
import com.youyilin.customer.service.CustomerAddressService;
import com.youyilin.customer.service.CustomerService;
import com.youyilin.customer.vo.address.CustomerAddressEditVO;
import com.youyilin.customer.vo.address.CustomerAddressPageVO;
import com.youyilin.logic.service.customer.CustomerAddressLogic;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CustomerAddressLogicImpl implements CustomerAddressLogic {

    private final CustomerAddressService customerAddressService;
    private final CustomerService customerService;

    public CustomerAddressLogicImpl(CustomerAddressService customerAddressService, CustomerService customerService) {
        this.customerAddressService = customerAddressService;
        this.customerService = customerService;
    }

    @Override
    public List<CustomerAddressPageVO> listByCustomerId(Long customerId) {
        return CustomerAddressPageVO.convertByEntity(customerAddressService.listByCustomerId(customerId));
    }

    @Override
    public CustomerAddressEditVO getEditVOById(Long id) {
        CustomerAddress dbAddress = customerAddressService.getById(id);
        return dbAddress == null ? new CustomerAddressEditVO() : CustomerAddressEditVO.convertByEntity(dbAddress);
    }

    @Override
    @Transactional
    public void saveAddress(CustomerAddressFormDTO dto) {
        // 将数据传输对象转换为实体对象
        CustomerAddress address = dto.convertToEntity();
        if (address.getDefaultFlag() == null) {
            address.setDefaultFlag(BooleanEnum.FALSE.getCode());
        }
        // 验证客户是否有效
        customerService.validateCustomer(address.getCustomerId());
        // 保存地址
        customerAddressService.saveCustomerAddress(address);
    }

    @Override
    public void updateDefault(CustomerAddressDefaultDTO dto) {
        // 验证地址是否存在
        CustomerAddress dbAddress = customerAddressService.validateAddress(dto.getId());
        // 取消之前的默认地址
        customerAddressService.updateDefaultCancel(dbAddress.getCustomerId());
        // 更新为默认地址
        customerAddressService.updateDefault(dbAddress.getId());
    }

    @Override
    public void delAddress(CustomerAddressDeleteDTO dto) {
        // 获取地址是否存在
        CustomerAddress dbAddress = customerAddressService.getById(dto.getId());
        if (dbAddress == null) {
            return;
        }
        // 移除地址
        customerAddressService.removeById(dbAddress.getId());
    }
}
