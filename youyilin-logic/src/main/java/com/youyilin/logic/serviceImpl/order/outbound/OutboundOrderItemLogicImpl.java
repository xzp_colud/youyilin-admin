package com.youyilin.logic.serviceImpl.order.outbound;

import com.youyilin.logic.service.order.outbound.OutboundOrderItemLogic;
import com.youyilin.order.service.OutboundOrderItemService;
import com.youyilin.order.vo.outbound.OutboundOrderItemPageVO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OutboundOrderItemLogicImpl implements OutboundOrderItemLogic {

    private final OutboundOrderItemService outboundOrderItemService;

    public OutboundOrderItemLogicImpl(OutboundOrderItemService outboundOrderItemService) {
        this.outboundOrderItemService = outboundOrderItemService;
    }

    @Override
    public List<OutboundOrderItemPageVO> listByOutboundId(Long outboundId) {
        if (outboundId == null) {
            return new ArrayList<>();
        }
        return OutboundOrderItemPageVO.convertByEntity(outboundOrderItemService.listByOutboundId(outboundId));
    }
}
