package com.youyilin.logic.serviceImpl.order.sales;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.enums.BooleanEnum;
import com.youyilin.common.exception.ApiException;
import com.youyilin.common.exception.Assert;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.common.utils.MobileUtil;
import com.youyilin.customer.bo.CustomerAccountConsumeBO;
import com.youyilin.customer.bo.CustomerValidBO;
import com.youyilin.customer.entity.Customer;
import com.youyilin.customer.entity.CustomerAddress;
import com.youyilin.customer.manager.CustomerManager;
import com.youyilin.goods.dto.SalesValidateProductDTO;
import com.youyilin.goods.entity.ProductFabric;
import com.youyilin.goods.entity.ProductIngredient;
import com.youyilin.goods.entity.ProductPrice;
import com.youyilin.goods.entity.ProductSubMaterial;
import com.youyilin.goods.manager.ProductManager;
import com.youyilin.goods.service.*;
import com.youyilin.logic.convert.ProductPriceConvert;
import com.youyilin.logic.manager.OutboundOrderManager;
import com.youyilin.logic.manager.PurchaseOrderManager;
import com.youyilin.logic.manager.WaybillManager;
import com.youyilin.logic.service.order.sales.OrderLogic;
import com.youyilin.order.bo.outbound.OutboundOrderThirdConstructBO;
import com.youyilin.order.bo.purchase.PurchaseOrderThirdConstructBO;
import com.youyilin.order.bo.sales.OrderSaveBO;
import com.youyilin.order.dto.outbound.OutboundOrderItemDetailThirdConstructDTO;
import com.youyilin.order.dto.purchase.PurchaseOrderItemDetailThirdConstructDTO;
import com.youyilin.order.dto.sales.*;
import com.youyilin.order.entity.Order;
import com.youyilin.order.entity.OrderItem;
import com.youyilin.order.entity.OrderItemRawMaterial;
import com.youyilin.order.entity.OrderOtherAmount;
import com.youyilin.order.enums.*;
import com.youyilin.order.mapper.OrderMapper;
import com.youyilin.order.service.*;
import com.youyilin.order.utils.NoUtils;
import com.youyilin.order.vo.OrderLogDetailVO;
import com.youyilin.order.vo.sales.*;
import com.youyilin.warehouse.service.InventoryService;
import com.youyilin.waybill.bo.WaybillThirdConstructBO;
import com.youyilin.waybill.dto.waybill.WaybillItemThirdConstructDTO;
import com.youyilin.waybill.enums.WaybillSourceType;
import com.youyilin.waybill.enums.WaybillStatusEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class OrderLogicImpl implements OrderLogic {

    private final OrderMapper orderMapper;
    private final OrderService orderService;
    private final OrderItemService orderItemService;
    private final OrderOtherAmountService orderOtherAmountService;
    private final OrderDiscountService orderDiscountService;
    private final OrderLogService orderLogService;
    private final OrderCollectService orderCollectService;
    private final OrderRefundService orderRefundService;
    private final OrderItemRawMaterialService orderItemRawMaterialService;
    private final ProductService productService;
    private final ProductPriceService productPriceService;
    private final CustomerManager customerManager;
    private final OutboundOrderManager outboundOrderManager;
    private final ProductManager productManager;
    private final WaybillManager waybillManager;
    private final PurchaseOrderSourceService purchaseOrderSourceService;
    private final ProductFabricService productFabricService;
    private final ProductIngredientService productIngredientService;
    private final ProductSubMaterialService productSubMaterialService;
    private final PurchaseOrderManager purchaseOrderManager;
    private final PurchaseOrderItemService purchaseOrderItemService;
    private final InventoryService inventoryService;
    private final OutboundOrderItemService outboundOrderItemService;

    public OrderLogicImpl(OrderItemService orderItemService, OrderMapper orderMapper, OrderService orderService,
                          OrderOtherAmountService orderOtherAmountService, OrderDiscountService orderDiscountService,
                          OrderLogService orderLogService, OrderCollectService orderCollectService, OrderRefundService orderRefundService,
                          OrderItemRawMaterialService orderItemRawMaterialService, ProductManager productManager, ProductService productService,
                          ProductPriceService productPriceService, CustomerManager customerManager,
                          OutboundOrderManager outboundOrderManager, WaybillManager waybillManager,
                          PurchaseOrderSourceService purchaseOrderSourceService,
                          ProductFabricService productFabricService, ProductIngredientService productIngredientService,
                          ProductSubMaterialService productSubMaterialService, PurchaseOrderManager purchaseOrderManager,
                          PurchaseOrderItemService purchaseOrderItemService, InventoryService inventoryService, OutboundOrderItemService outboundOrderItemService) {
        this.orderItemService = orderItemService;
        this.orderMapper = orderMapper;
        this.orderService = orderService;
        this.orderOtherAmountService = orderOtherAmountService;
        this.orderDiscountService = orderDiscountService;
        this.orderLogService = orderLogService;
        this.orderCollectService = orderCollectService;
        this.orderRefundService = orderRefundService;
        this.orderItemRawMaterialService = orderItemRawMaterialService;
        this.productManager = productManager;
        this.productService = productService;
        this.productPriceService = productPriceService;
        this.customerManager = customerManager;
        this.outboundOrderManager = outboundOrderManager;
        this.waybillManager = waybillManager;
        this.purchaseOrderSourceService = purchaseOrderSourceService;
        this.productFabricService = productFabricService;
        this.productIngredientService = productIngredientService;
        this.productSubMaterialService = productSubMaterialService;
        this.purchaseOrderManager = purchaseOrderManager;
        this.purchaseOrderItemService = purchaseOrderItemService;
        this.inventoryService = inventoryService;
        this.outboundOrderItemService = outboundOrderItemService;
    }

    @Override
    public Pager<OrderPageVO> getPageList(Page<OrderPageQueryDTO> page) {
        Integer total = orderMapper.getTotal(page);
        if (total == 0) {
            return new Pager<>(0, new ArrayList<>());
        }
        List<OrderPageVO> list = orderMapper.getPage(page);
        return new Pager<>(total, list);
    }

    @Override
    public OrderEditVO getEditVOById(Long id) {
        // 销售单
        Order dbOrder = orderService.getById(id);
        if (dbOrder == null) {
            return new OrderEditVO();
        }
        // 销售单明细
        List<OrderItem> itemList = orderItemService.listByOrderId(id);
        List<OrderEditItem> itemVOList = OrderEditItem.convertByEntity(itemList);
        // 销售单其他费用明细
        List<OrderOtherAmount> otherAmountList = orderOtherAmountService.listByOrderId(id);
        // 产品SKU
        Set<Long> productIds = new HashSet<>();
        itemList.forEach(item -> productIds.add(item.getProductId()));
        // SKU集合
        Map<Long, List<ProductPrice>> priceMap = productPriceService.mapByProductIds(new ArrayList<>(productIds));
        for (OrderEditItem item : itemVOList) {
            item.setPriceList(ProductPriceConvert.convertToOrderEditItemSku(priceMap.get(item.getProductId())));
        }
        // 构建销售单编辑VO
        OrderEditVO vo = new OrderEditVO();
        // 赋值
        vo.setOrder(OrderEdit.convertByEntity(dbOrder))
                .setItemList(itemVOList)
                .setOtherAmountList(OrderEditOtherAmount.convertByEntity(otherAmountList));
        return vo;
    }

    @Override
    public OrderDetailVO getDetailVOById(Long id) {
        // 销售单
        Order order = orderService.getById(id);
        if (order == null) {
            return new OrderDetailVO();
        }
        // 构建销售单详情VO
        OrderDetailVO vo = new OrderDetailVO();
        // 销售单
        vo.setOrder(OrderDetail.convertByEntity(order));
        // 销售单商品明细
        List<OrderDetailItem> detailItemList = OrderDetailItem.convertByEntity(orderItemService.listByOrderId(id));
        vo.setItemList(detailItemList);
        // 日志明细
//        vo.setLogList(OrderLogDetailVO.convertByEntity(orderLogService.listBySn(order.getSn())));
        vo.setLogList(new ArrayList<>());
        // 收款明细
        vo.setCollectList(OrderDetailCollect.convertByEntity(orderCollectService.listByOrderId(order.getId(), OrderCollectEnum.SUCCESS.getCode())));
        // 采购记录
        vo.setPurchaseFlag(purchaseOrderSourceService.countBySourceIdAndType(id, PurchaseOrderSourceTypeEnum.SALE_ORDER.name()) > 0);
        // 原料明细
        List<OrderItemRawMaterial> rawMaterialList = orderItemRawMaterialService.listByOrderId(id);
        if (CollectionUtils.isNotEmpty(rawMaterialList)) {
            List<OrderDetailItemRawMaterial> detailItemRawMaterialList = OrderDetailItemRawMaterial.convertByEntity(rawMaterialList);
            Map<Long, List<OrderDetailItemRawMaterial>> detailItemRawMaterialMap = detailItemRawMaterialList.stream().collect(Collectors.groupingBy(OrderDetailItemRawMaterial::getOrderItemId));
            for (OrderDetailItem item : detailItemList) {
                item.setRawMaterialList(detailItemRawMaterialMap.get(item.getId()));
            }
        }
        return vo;
    }

    @Override
    public OrderUpdateAddressVO getUpdateAddressVOById(Long id) {
        Order order = orderService.getById(id);
        return order == null ? new OrderUpdateAddressVO() : OrderUpdateAddressVO.convertByEntity(order);
    }

    @Override
    @Transactional
    public void saveOrder(OrderFormDTO dto) {
        log.info("【提交 订单】 START {} {}", new Date().getTime(), JSONObject.toJSONString(dto));
        Assert.notNull(dto.getOrderType(), "订单异常");
        OrderSaveBO bo = new OrderSaveBO();
        bo.setDto(dto).setId(dto.getId()).setEdit(dto.getId() != null);
        if (bo.isEdit()) {
            Order oldOrder = orderService.validateCanEdit(bo.getId(), dto.getCustomerId());
            bo.setSn(oldOrder.getSn());
        }

        // 验证客户信息
        CustomerValidBO customerValidBO = new CustomerValidBO();
        customerValidBO.setCustomerId(dto.getCustomerId()).setAddressId(dto.getAddressId());
        customerManager.validate(customerValidBO);
        // 验证产品信息
        boolean wholesaleFlag = dto.getOrderType().equals(OrderTypeEnum.WHOLESALE.getCode());
        List<SalesValidateProductDTO> itemList = BeanHelper.map(dto.getProductList(), SalesValidateProductDTO.class);
        productManager.validateSalesProductBatch(itemList, wholesaleFlag);
        // 设置客户信息及收货地址信息
        bo.setCustomer(customerValidBO.getCustomer()).setAddress(customerValidBO.getAddress());
        // 构建销售单
        this.constructOrder(bo);
        // 构建销售单其他费用明细
        this.constructOrderOtherAmount(bo);
        // 构建销售单明细
        this.constructOrderItem(bo, itemList);
        // 刷新销售单金额
        this.refreshOrderAmount(bo);
        // 持久化销售单
        this.save(bo);
    }

    private void save(OrderSaveBO bo) {
        // 销售单ID
        Long id = bo.getId();
        // 销售单编号
        String sn = bo.getSn();
        if (bo.isEdit()) {
            orderService.updateById(bo.getOrder());
        } else {
            orderService.save(bo.getOrder());
        }

        // 商品明细
        orderItemService.delByOrderId(id);
        orderItemService.saveBatch(bo.getItemList());

        // 折扣明细
        orderDiscountService.delByOrderId(id);
        if (bo.getOrder().getDiscountAmount() > 0) {
            orderDiscountService.saveOrderDiscount(id, sn, bo.getOrder().getDiscountAmount(), "产品有折扣");
        }

        // 其他费用
        orderOtherAmountService.deleteByOrderId(id);
        if (CollectionUtils.isNotEmpty(bo.getOtherAmountList())) {
            orderOtherAmountService.saveBatch(bo.getOtherAmountList());
        }

        // 日志
        orderLogService.saveLog(id, sn, OrderLogEnum.CREATE_OR_UPDATE.getCode());
    }

    /**
     * 构建销售单
     */
    private void constructOrder(OrderSaveBO bo) {
        // 客户信息
        Customer customer = bo.getCustomer();
        // 客户地址
        CustomerAddress address = bo.getAddress();
        // 数据转换
        Order order = BeanHelper.map(bo.getDto(), Order.class);
        if (!bo.isEdit()) {
            bo.setId(IdWorker.getId());
            bo.setSn(NoUtils.getSaleOrderSn());
            order.setSn(bo.getSn());
        }
        // 设置收货地址
        order.setId(bo.getId())
                .setSn(bo.getSn())
                .setStatus(OrderStatusEnum.NO_SUBMIT.getCode())
                .setSettledStatus(CommonSettledStatusEnum.DEFAULT.getCode())
                .setRefundStatus(CommonRefundStatusEnum.DEFAULT.getCode())
                .setOutboundStatus(CommonOutboundStatusEnum.DEFAULT.getCode())
                .setWaybillStatus(WaybillStatusEnum.DEFAULT.getCode())
                .setFinanceFlag(BooleanEnum.FALSE.getCode())
                .setCustomerName(customer.getName())
                .setCustomerPhone(customer.getPhone())
                .setProvince(customer.getProvince())
                .setReceiverName(address.getReceiverName())
                .setReceiverPhone(address.getReceiverPhone())
                .setReceiverProvince(address.getProvince())
                .setReceiverCity(address.getCity())
                .setReceiverArea(address.getArea())
                .setReceiverDetail(address.getDetail())
                .setOtherAmount(0L);

        bo.setOrder(order);
    }

    /**
     * 构建销售单其他费用明细
     */
    private void constructOrderOtherAmount(OrderSaveBO bo) {
        List<OrderFormOtherAmount> otherAmountList = bo.getDto().getOtherAmountList();
        if (CollectionUtils.isEmpty(otherAmountList)) {
            return;
        }
        Long otherAmount = 0L;
        for (OrderFormOtherAmount item : otherAmountList) {
            Assert.hasLength(item.getTitle(), "其他费用名称不能为空");
            Assert.isFalse(item.getAmount() == null || item.getAmount().compareTo(BigDecimal.ZERO) < 0, "其他费用金额异常");
            item.setPrice(item.getAmount().multiply(new BigDecimal("100")).longValue());
            otherAmount += item.getPrice();
        }
        List<OrderOtherAmount> saveOtherAmountList = BeanHelper.map(otherAmountList, OrderOtherAmount.class);
        Long orderId = bo.getId();
        String sn = bo.getSn();
        saveOtherAmountList.forEach(item -> {
            item.setOrderId(orderId);
            item.setOrderSn(sn);
        });
        bo.setOtherAmountList(saveOtherAmountList);
        // 设置费用总额
        bo.getOrder().setOtherAmount(otherAmount);
    }

    /**
     * 构建销售单商品明细
     */
    private void constructOrderItem(OrderSaveBO bo, List<SalesValidateProductDTO> itemList) {
        List<OrderItem> orderItemList = BeanHelper.map(itemList, OrderItem.class);
        for (int i = 0; i < orderItemList.size(); i++) {
            int index = i + 1;
            OrderItem item = orderItemList.get(i);
            item.setOrderId(bo.getId())
                    .setOrderSn(bo.getSn())
                    .setStatus(OrderStatusEnum.NO_SUBMIT.getCode())
                    .setItemSn(bo.getSn() + "-" + index);
        }

        bo.setItemList(orderItemList);
    }

    /**
     * 刷新订单金额
     */
    private void refreshOrderAmount(OrderSaveBO bo) {
        Order order = bo.getOrder();
        List<OrderItem> itemList = bo.getItemList();
        BigDecimal totalNum = BigDecimal.ZERO;
        Long totalSellAmount = 0L;
        Long totalCost = 0L;
        Long totalDiscount = 0L;
        for (int i = 0; i < itemList.size(); i++) {
            int index = i + 1;
            OrderItem orderItem = itemList.get(i);

            totalNum = totalNum.add(orderItem.getNum());
            totalCost += orderItem.getTotalCost();
            totalSellAmount += orderItem.getTotalSellPrice();
            totalDiscount += orderItem.getTotalDiscount();

            orderItem.setItemSn(order.getSn() + "-" + index);
        }

        order.setDiscountAmount(totalDiscount);
        order.setTotalNum(totalNum);
        order.setCostAmount(totalCost);
        order.setSellAmount(totalSellAmount);

        // 实际付款金额
        long sellMoney = totalSellAmount + order.getOtherAmount() - order.getDiscountAmount();
        Assert.isTrue(sellMoney >= 0L, "订单金额异常");
        order.setSellMoney(sellMoney);
    }

    @Override
    @Transactional
    public void updateSubmit(OrderSubmitFormDTO dto) {
        // 销售单ID
        Long id = dto.getId();
        // 验证销售单
        Order order = orderService.validateOrderSubmit(id);
        // 获取明细
        List<OrderItem> itemList = orderItemService.listByOrderId(id);
        // 验证是否有下架的商品或SKU
        Set<Long> productIds = new HashSet<>();
        Set<Long> skuIds = new HashSet<>();
        for (OrderItem item : itemList) {
            productIds.add(item.getProductId());
            skuIds.add(item.getProductPriceId());
        }
        productService.validateProductSell(new ArrayList<>(productIds));
        productPriceService.validatePriceSell(new ArrayList<>(skuIds));
        // 更新销售单状态为已提交
        orderService.updateSubmit(id);
        // 更新销售单明细为已提交
        orderItemService.updateSubmit(id);
        // 日志
        orderLogService.saveLog(order.getId(), order.getSn(), OrderLogEnum.SUBMIT.getCode());

        // 保存原料明细
        List<OrderItemRawMaterial> rawMaterialList = constructOrderItemRawMaterial(order, itemList);
        if (CollectionUtils.isNotEmpty(rawMaterialList)) {
            orderItemRawMaterialService.saveBatch(rawMaterialList);
        }
    }

    /**
     * 构建销售单子集原料明细
     *
     * @param order    销售单
     * @param itemList 销售单子集
     * @return ArrayList
     */
    private List<OrderItemRawMaterial> constructOrderItemRawMaterial(Order order, List<OrderItem> itemList) {
        Long id = order.getId();
        // 生成具体的源料明细
        List<PurchaseOrderItemDetailThirdConstructDTO> sourceRawMaterialList = this.doSalesOrderToPurchaseOrderInit(id, null, false, true);
        if (CollectionUtils.isEmpty(sourceRawMaterialList)) {
            return new ArrayList<>();
        }
        // 刷新sku
        this.refreshToPurchaseOrderItemSkuName(sourceRawMaterialList);
        // 构建销售单原料明细
        List<OrderItemRawMaterial> rawMaterialList = new ArrayList<>();
        Map<Long, List<PurchaseOrderItemDetailThirdConstructDTO>> sourceRawMaterialMap = sourceRawMaterialList.stream().collect(Collectors.groupingBy(PurchaseOrderItemDetailThirdConstructDTO::getSourceId));
        for (OrderItem item : itemList) {
            if (sourceRawMaterialMap.containsKey(item.getId())) {
                List<PurchaseOrderItemDetailThirdConstructDTO> sourceList = sourceRawMaterialMap.get(item.getId());

                for (PurchaseOrderItemDetailThirdConstructDTO sourceItem : sourceList) {
                    OrderItemRawMaterial rawMaterial = new OrderItemRawMaterial();
                    rawMaterial.setOrderId(id)
                            .setOrderSn(order.getSn())
                            .setOrderItemId(item.getId())
                            .setOrderItemSn(item.getItemSn())
                            .setSourceProductId(sourceItem.getProductId())
                            .setSourceProductName(sourceItem.getProductName())
                            .setSourceSkuId(sourceItem.getSkuId())
                            .setSourceSkuName(sourceItem.getSkuName())
                            .setOneUsage(sourceItem.getOneUsage())
                            .setMakeNum(sourceItem.getMakeNum())
                            .setTotalUsage(sourceItem.getBuyNum())
                            .setTypeName(sourceItem.getCategoryName());

                    rawMaterialList.add(rawMaterial);
                }
            }
        }
        return rawMaterialList;
    }

    @Override
    @Transactional
    public void updatePay(OrderPayFormDTO dto) {
        log.info("【付款 订单】 START {} {}", new Date().getTime(), JSONObject.toJSONString(dto));

        // 销售单ID
        Long id = dto.getOrderId();
        // 客户ID
        Long customerId = dto.getCustomerId();
        // 账户ID
        Long accountId = dto.getAccountId();
        // 更新订单
        Order order = orderService.validateOrderPay(id);
        CustomerAccountConsumeBO bo = new CustomerAccountConsumeBO();
        bo.setSourceId(order.getId())
                .setSourceNo(order.getSn())
                .setRemark("订单付款")
                .setCustomerId(customerId)
                .setAccountId(accountId)
                .setAmount(order.getSellMoney());
        // 更新客户账户金额
        customerManager.updateAccountConsume(bo);
        // 是否为备货的
        if (BooleanEnum.isTrue(order.getReadyFlag())) {
            // 更新销售单已付款
            orderService.updatePayReady(id, bo.getOrderFinanceFlag(), bo.getPayType());
            // 更新销售单明细已付款
            orderItemService.updatePayReady(id);
        } else {
            // 更新销售单已付款
            orderService.updatePay(id, bo.getOrderFinanceFlag(), bo.getPayType());
            // 更新销售单明细已付款
            orderItemService.updatePay(id);
        }
        // 收款记录
        orderCollectService.saveCollect(order.getId(), order.getSn(), order.getCustomerId(), dto.getAccountId(), order.getSellAmount(), bo.getPayType());
        // 日志
        orderLogService.saveLog(order.getId(), order.getSn(), OrderLogEnum.SALE_ORDER_PAY.getCode());
    }

    @Override
    public void updateReadyPurchaseProcessing(OrderReadyPurchaseFormDTO dto) {
        Long id = dto.getId();
        // 验证订单是否可以备货入库
        Order order = orderService.validateCanReadyPurchase(id);
        // 是否已有大货采购
        if (purchaseOrderSourceService.countBySourceIdAndType(id, PurchaseOrderSourceTypeEnum.SALE_ORDER_READY.name()) > 0L) {
            throw new ApiException("已处于大货采购中");
        }
        // 查询订单明细 构建来源明细
        List<PurchaseOrderItemDetailThirdConstructDTO> sourceList = new ArrayList<>();
        List<OrderItem> itemList = orderItemService.listByOrderId(id);
        for (OrderItem item : itemList) {
            PurchaseOrderItemDetailThirdConstructDTO sourceItem = new PurchaseOrderItemDetailThirdConstructDTO();
            sourceItem.setProductId(item.getProductId())
                    .setSkuId(item.getProductPriceId())
                    .setBuyNum(item.getNum());

            sourceList.add(sourceItem);
        }
        // 构建BO 转成采购单
        PurchaseOrderThirdConstructBO bo = new PurchaseOrderThirdConstructBO(id, order.getSn(), PurchaseOrderSourceTypeEnum.SALE_ORDER_READY.name());
        bo.setSourceList(sourceList);
        purchaseOrderManager.constructPurchaseOrder(bo);
        // 更新状态为 备货入库中
        orderService.updateReadyProcessing(id);
        // 更新明细状态为 备货入库中
        orderItemService.updateReadyProcessing(id);
        // 生成订单日志
        orderLogService.saveLog(order.getId(), order.getSn(), OrderLogEnum.SALE_ORDER_READY_TO_PURCHASE_ORDER.getCode());
    }

    @Override
    @Transactional
    public void updateInventoryOut(OrderInventoryFormDTO dto) {
        // 销售单ID
        Long id = dto.getOrderId();
        // 订单
        Order order = orderService.validateOrderOutbound(id);
        Assert.isTrue(order.getStatus() == OrderStatusEnum.WAIT_INVENTORY.getCode(), "状态异常");
        // 是否存在退款申请
        orderRefundService.validateExcludeFail(id);
        // 创建出库单
        OutboundOrderThirdConstructBO bo = new OutboundOrderThirdConstructBO();
        bo.setSourceSn(order.getSn())
                .setSourceList(OutboundOrderItemDetailThirdConstructDTO.convertByOrderItem(orderItemService.listByOrderId(id)));
        outboundOrderManager.constructOutboundOrder(bo);
        // 销售单状态为出库中
        orderService.updateOutboundProcessByOutboundId(bo.getOutboundOrder().getId(), bo.getOutboundOrder().getSn(), order.getId());
        // 销售单明细状态为出库中
        orderItemService.updateOutbound(id);
        // 操作日志
        orderLogService.saveLog(order.getId(), order.getSn(), OrderLogEnum.SALE_ORDER_INVENTORY_OUT.getCode(), JSONObject.toJSONString(order));
    }

    @Override
    @Transactional
    public void updateSend(OrderSendFormDTO dto) {
        // 销售单ID
        Long id = dto.getId();
        // 验证订单是否可以发货
        Order order = orderService.validateOrderSend(id);
        // 是否存在退款申请
        orderRefundService.validateExcludeFail(order.getId());
        // 订单子集
        List<OrderItem> orderItemList = orderItemService.listByOrderId(order.getId());
        // 货运单BO
        List<WaybillItemThirdConstructDTO> itemSourceList = new ArrayList<>();
        for (OrderItem item : orderItemList) {
            WaybillItemThirdConstructDTO itemSource = new WaybillItemThirdConstructDTO();
            itemSource.setSourceId(item.getId())
                    .setSourceNo(item.getItemSn())
                    .setProductId(item.getProductId())
                    .setProductName(item.getProductName())
                    .setProductPriceId(item.getProductPriceId())
                    .setSkuName(item.getSkuName())
                    .setNum(item.getNum());

            itemSourceList.add(itemSource);
        }
        WaybillThirdConstructBO waybillBO = new WaybillThirdConstructBO();
        waybillBO.setSourceType(WaybillSourceType.SALE_ORDER.name())
                .setReceiveName(order.getReceiverName())
                .setReceiveMobile(order.getReceiverPhone())
                .setReceiveProvince(order.getReceiverProvince())
                .setReceiveCity(order.getReceiverCity())
                .setReceiveArea(order.getReceiverArea())
                .setReceiveDetail(order.getReceiverDetail())
                .setItemSourceList(itemSourceList);
        // 生成货运单
        waybillManager.constructWaybill(waybillBO);
        // 修改订单状态为发货中
        orderService.updateSend(order.getId(), waybillBO.getId(), waybillBO.getSn());
        // 日志
        orderLogService.saveLog(order.getId(), order.getSn(), OrderLogEnum.SALE_ORDER_SEND.getCode(), null);
    }

    @Override
    @Transactional
    public void updateCancel(OrderCancelFormDTO dto) {
        // 销售单ID
        Long id = dto.getId();
        // 验证销售单是否能取消
        Order order = orderService.validateOrderCancel(id);
        // 销售单取消
        orderService.updateCancel(id);
        // 销售单商品明细取消
        orderItemService.updateCancelByOrderId(id);
        // 日志
        orderLogService.saveLog(order.getId(), order.getSn(), OrderLogEnum.CANCEL.getCode());
    }

    @Override
    @Transactional
    public void updateAddress(OrderAddressFormDTO dto) {
        // 销售单ID
        Long id = dto.getId();
        // 验证手机号
        Assert.isFalse(MobileUtil.isNotMobile(dto.getReceiverPhone()), "手机号格式不正确");
        // 验证销售单
        Order order = orderService.validateUpdateAddress(id);
        // 修改销售单收货地址
        orderService.updateAddress(id, dto.getReceiverName(), dto.getReceiverPhone(), dto.getReceiverProvince(), dto.getReceiverCity(), dto.getReceiverArea(), dto.getReceiverDetail());
        // 日志
        orderLogService.saveLog(order.getId(), order.getSn(), OrderLogEnum.SALE_ORDER_ADDRESS.getCode());
    }

    @Override
    public List<OrderSettledPageVO> listSettledPage() {
        List<OrderSettledPageItem> list = orderMapper.listSettledPage();
        Map<Long, List<OrderSettledPageItem>> customerMap = list.stream().collect(Collectors.groupingBy(OrderSettledPageItem::getCustomerId));

        List<OrderSettledPageVO> voList = new ArrayList<>();
        for (Long customerId : customerMap.keySet()) {

            List<OrderSettledPageItem> itemList = customerMap.get(customerId);
            if (CollectionUtils.isEmpty(itemList)) {
                continue;
            }
            OrderSettledPageItem item = itemList.get(0);
            Map<String, List<OrderSettledPageItem>> itemMap = itemList.stream().collect(Collectors.groupingBy(OrderSettledPageItem::getDay));

            OrderSettledPageVO vo = new OrderSettledPageVO();
            vo.setCustomerId(customerId)
                    .setCustomerName(item.getCustomerName())
                    .setCustomerPhone(item.getCustomerPhone())
                    .setItemList(itemMap);

            voList.add(vo);
        }
        return voList;
    }

    @Override
    public List<OrderToPurchaseQueryQtyVO> getPurchaseInfo(Long id) {
        Order order = orderService.getById(id);
        if (order == null) {
            return new ArrayList<>();
        }
        // 初始化
        List<PurchaseOrderItemDetailThirdConstructDTO> sourcePurchaseList = this.doSalesOrderToPurchaseOrderInit(id, null, false, true);
        // 合并
        List<PurchaseOrderItemDetailThirdConstructDTO> purchaseList = this.mergeToPurchaseOrderItemNum(sourcePurchaseList);
        if (CollectionUtils.isEmpty(purchaseList)) {
            return new ArrayList<>();
        }
        // 刷新sku名称
        this.refreshToPurchaseOrderItemSkuName(purchaseList);
        // SKU IDS
        List<Long> skuIds = new ArrayList<>();
        for (PurchaseOrderItemDetailThirdConstructDTO item : purchaseList) {
            skuIds.add(item.getSkuId());
        }
        // 整体需求量
        Map<Long, BigDecimal> totalQtyMap = orderItemRawMaterialService.sumTotalRawMaterialBySkuIds(skuIds);
        // 在途采购数量
        Map<Long, BigDecimal> purchaseQtyMap = purchaseOrderItemService.sumConfirmAndNoCheckQtyBySkuIds(skuIds);
        // 在途检验数量
        Map<Long, BigDecimal> checkQtyMap = purchaseOrderItemService.sumCheckAndNoInventoryQtyBySkuIds(skuIds);
        // 库存数量
        Map<Long, BigDecimal> inventoryQtyMap = inventoryService.sumQtyBySkuIds(skuIds);
        // 返回列表
        List<OrderToPurchaseQueryQtyVO> voList = new ArrayList<>();
        for (PurchaseOrderItemDetailThirdConstructDTO item : purchaseList) {
            Long skuId = item.getSkuId();
            // 整体需求量
            BigDecimal totalQty = totalQtyMap.getOrDefault(skuId, BigDecimal.ZERO);
            // 本次需求量
            BigDecimal thisQty = item.getBuyNum();
            // 在途采购数量
            BigDecimal purchaseQty = purchaseQtyMap.getOrDefault(skuId, BigDecimal.ZERO);
            // 在途检验数量
            BigDecimal checkQty = checkQtyMap.getOrDefault(skuId, BigDecimal.ZERO);
            // 库存数量
            BigDecimal inventoryQty = inventoryQtyMap.getOrDefault(skuId, BigDecimal.ZERO);

            OrderToPurchaseQueryQtyVO vo = new OrderToPurchaseQueryQtyVO();
            vo.setSkuName(item.getSkuName())
                    .setTotalQty(totalQty)
                    .setThisQty(thisQty)
                    .setPurchaseQty(purchaseQty)
                    .setCheckQty(checkQty)
                    .setInventoryQty(inventoryQty)
                    .setProductName(item.getProductName())
                    .setCategoryName(item.getCategoryName());

            voList.add(vo);
        }
        return voList;
    }

    @Override
    @Transactional
    public void doSalesOrderToPurchaseOrder(OrderPurchaseFormDTO dto) {
        Long orderId = dto.getId();
        List<OrderPurchaseFormItem> itemList = dto.getItemList();
        // 验证销售单是否可以生成采购单
        Order order = orderService.validateCanPurchase(orderId);
        // 验证是否有关联的采购单
        if (purchaseOrderSourceService.countBySourceIdAndType(orderId, PurchaseOrderSourceTypeEnum.SALE_ORDER.name()) > 0L) {
            throw new ApiException("正处于采购中");
        }
        // 构建BO
        PurchaseOrderThirdConstructBO bo = new PurchaseOrderThirdConstructBO(order.getId(), order.getSn(), PurchaseOrderSourceTypeEnum.SALE_ORDER.name());
        // 初始化用料信息
        List<PurchaseOrderItemDetailThirdConstructDTO> sourcePurchaseList = this.doSalesOrderToPurchaseOrderInit(orderId, itemList, true, BooleanEnum.isTrue(dto.getSubMaterialFlag()));
        // 合并
        List<PurchaseOrderItemDetailThirdConstructDTO> purchaseList = this.mergeToPurchaseOrderItemNum(sourcePurchaseList);
        Assert.notEmpty(purchaseList, "原材料为空，转采购失败");
        bo.setSourceList(purchaseList);
        // 转成采购单
        purchaseOrderManager.constructPurchaseOrder(bo);
        // 生成订单日志
        orderLogService.saveLog(order.getId(), order.getSn(), OrderLogEnum.SALE_ORDER_TO_PURCHASE_ORDER.getCode());
    }

    @Override
    public List<OrderToPurchaseQueryQtyVO> getReadyPurchaseInfo(Long id) {
        // 获取销售单
        Order order = orderService.getById(id);
        if (order == null) {
            return new ArrayList<>();
        }
        // 销售明细
        List<OrderItem> itemList = orderItemService.listByOrderId(id);
        List<Long> skuIds = new ArrayList<>();
        for (OrderItem item : itemList) {
            skuIds.add(item.getProductPriceId());
        }
        // 库存量
        Map<Long, BigDecimal> inventoryQtyMap = inventoryService.sumQtyBySkuIds(skuIds);
        // 占用出库量
        Map<Long, BigDecimal> outboundQtyMap = outboundOrderItemService.sumNoOutboundQtyBySkuIds(skuIds);
        // 返回列表
        List<OrderToPurchaseQueryQtyVO> voList = new ArrayList<>();
        for (OrderItem item : itemList) {
            Long skuId = item.getProductPriceId();
            OrderToPurchaseQueryQtyVO vo = new OrderToPurchaseQueryQtyVO();

            // 库存量
            BigDecimal inventoryQty = inventoryQtyMap.getOrDefault(skuId, BigDecimal.ZERO);
            // 占用销售出库量
            BigDecimal outboundQty = outboundQtyMap.getOrDefault(skuId, BigDecimal.ZERO);

            vo.setProductName(item.getProductName())
                    .setCategoryName(item.getCategoryName())
                    .setSkuName(item.getSkuName())
                    .setThisQty(item.getNum())
                    .setInventoryQty(inventoryQty)
                    .setOutboundQty(outboundQty);

            voList.add(vo);
        }
        return voList;
    }

    /**
     * 销售单转采购单初始化
     *
     * @param orderId 销售单ID
     */
    private List<PurchaseOrderItemDetailThirdConstructDTO> doSalesOrderToPurchaseOrderInit(Long orderId, List<OrderPurchaseFormItem> formItemList, boolean checkForm, boolean subMaterialFlag) {
        // 获取商品明细
        List<OrderItem> itemList = orderItemService.listByOrderId(orderId);
        Map<Long, BigDecimal> formItemMap = new HashMap<>();
        if (checkForm) {
            Assert.notEmpty(itemList, "销售明细不能为空");
            for (OrderPurchaseFormItem item : formItemList) {
                Assert.notNull(item.getId(), "参数异常");
                Assert.notNull(item.getNum(), "参数异常");
                Assert.isFalse(item.getNum().compareTo(BigDecimal.ZERO) < 0, "参数异常");
                formItemMap.put(item.getId(), item.getNum());
            }
        }
        // 所有商品ID、SKU ID
        Set<Long> skuIds = new HashSet<>();
        for (OrderItem item : itemList) {
            skuIds.add(item.getProductPriceId());
        }
        // 获取商品SKU列表
        Map<Long, ProductPrice> priceMap = productPriceService.mapByIds(new ArrayList<>(skuIds));
        // 获取配置的方案
        Set<Long> planIds = new HashSet<>();
        for (ProductPrice item : priceMap.values()) {
            planIds.add(item.getPlanId());
        }
        List<Long> queryPlanIds = new ArrayList<>(planIds);
        // 获取方案面料列表
        List<ProductFabric> fabricList = productFabricService.listByPlanIds(queryPlanIds);
        Map<Long, List<ProductFabric>> fabricMap = fabricList.stream().collect(Collectors.groupingBy(ProductFabric::getPlanId));
        // 获取方案配料列表
        List<ProductIngredient> ingredientList = productIngredientService.listByPlanIds(queryPlanIds);
        Map<Long, List<ProductIngredient>> ingredientMap = ingredientList.stream().collect(Collectors.groupingBy(ProductIngredient::getPlanId));
        // 获取方案辅料列表
        List<ProductSubMaterial> subMaterialList = productSubMaterialService.listByPlanIds(queryPlanIds);
        Map<Long, List<ProductSubMaterial>> subMaterialMap = subMaterialList.stream().collect(Collectors.groupingBy(ProductSubMaterial::getPlanId));
        // 合并采购信息
        List<PurchaseOrderItemDetailThirdConstructDTO> detailList = new ArrayList<>();
        for (OrderItem item : itemList) {
            BigDecimal num = item.getNum();
            if (checkForm) {
                num = formItemMap.get(item.getId());
                Assert.notNull(num, "数量异常");
            }
            ProductPrice price = priceMap.get(item.getProductPriceId());
            if (price.getPlanId() == null) {
                continue;
            }
            List<ProductFabric> existFabricList = fabricMap.get(price.getPlanId());
            if (CollectionUtils.isNotEmpty(existFabricList)) {
                detailList.addAll(PurchaseOrderItemDetailThirdConstructDTO.convertByFabric(existFabricList, num, item.getId()));
            }
            List<ProductIngredient> existIngredientList = ingredientMap.get(price.getPlanId());
            if (CollectionUtils.isNotEmpty(existIngredientList)) {
                detailList.addAll(PurchaseOrderItemDetailThirdConstructDTO.convertByIngredient(existIngredientList, num, item.getId()));
            }
            if (subMaterialFlag) {
                List<ProductSubMaterial> existSubMaterialList = subMaterialMap.get(price.getPlanId());
                if (CollectionUtils.isNotEmpty(existSubMaterialList)) {
                    detailList.addAll(PurchaseOrderItemDetailThirdConstructDTO.convertBySubMaterial(existSubMaterialList, num, item.getId()));
                }
            }
        }
        return detailList;
    }

    /**
     * 刷新SKU名称
     */
    private void refreshToPurchaseOrderItemSkuName(List<PurchaseOrderItemDetailThirdConstructDTO> detailList) {
        if (CollectionUtils.isEmpty(detailList)) {
            return;
        }
        Set<Long> sourceSkuIds = new HashSet<>();
        for (PurchaseOrderItemDetailThirdConstructDTO item : detailList) {
            sourceSkuIds.add(item.getSkuId());
        }
        Map<Long, ProductPrice> sourcePriceMap = productPriceService.mapByIds(new ArrayList<>(sourceSkuIds));
        for (PurchaseOrderItemDetailThirdConstructDTO item : detailList) {
            ProductPrice price = sourcePriceMap.get(item.getSkuId());
            item.setSkuName(price == null ? null : price.getSkuName());
        }
    }

    /**
     * 合并数量
     */
    private List<PurchaseOrderItemDetailThirdConstructDTO> mergeToPurchaseOrderItemNum(List<PurchaseOrderItemDetailThirdConstructDTO> detailList) {
        if (CollectionUtils.isEmpty(detailList)) {
            return new ArrayList<>();
        }
        List<PurchaseOrderItemDetailThirdConstructDTO> resultList = new ArrayList<>();
        Map<Long, List<PurchaseOrderItemDetailThirdConstructDTO>> detailMap = detailList.stream().collect(Collectors.groupingBy(PurchaseOrderItemDetailThirdConstructDTO::getSkuId));
        for (List<PurchaseOrderItemDetailThirdConstructDTO> dtoList : detailMap.values()) {
            PurchaseOrderItemDetailThirdConstructDTO dto = dtoList.get(0);
            BigDecimal buyNum = BigDecimal.ZERO;
            for (PurchaseOrderItemDetailThirdConstructDTO item : dtoList) {
                buyNum = buyNum.add(item.getBuyNum());
            }
            buyNum = buyNum.setScale(2, RoundingMode.HALF_UP);

            // 设置合并后的值
            PurchaseOrderItemDetailThirdConstructDTO resultItem = new PurchaseOrderItemDetailThirdConstructDTO();
            resultItem.setProductName(dto.getProductName())
                    .setProductId(dto.getProductId())
                    .setSkuId(dto.getSkuId())
                    .setBuyNum(buyNum)
                    .setCategoryName(dto.getCategoryName())
                    .setSkuName(dto.getSkuName());

            resultList.add(resultItem);
        }
        return resultList;
    }
}
