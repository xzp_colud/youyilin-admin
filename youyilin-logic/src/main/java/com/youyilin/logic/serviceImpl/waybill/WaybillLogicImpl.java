package com.youyilin.logic.serviceImpl.waybill;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.logic.manager.WaybillCallBackManager;
import com.youyilin.logic.service.waybill.WaybillLogic;
import com.youyilin.waybill.dto.waybill.WaybillConfirmFormDTO;
import com.youyilin.waybill.dto.waybill.WaybillInfoFormDTO;
import com.youyilin.waybill.dto.waybill.WaybillPageQueryDTO;
import com.youyilin.waybill.entity.Waybill;
import com.youyilin.waybill.entity.WaybillSender;
import com.youyilin.waybill.enums.WaybillStatusEnum;
import com.youyilin.waybill.mapper.WaybillMapper;
import com.youyilin.waybill.service.WaybillItemService;
import com.youyilin.waybill.service.WaybillSenderService;
import com.youyilin.waybill.service.WaybillService;
import com.youyilin.waybill.vo.waybill.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class WaybillLogicImpl implements WaybillLogic {

    private final WaybillMapper waybillMapper;
    private final WaybillService waybillService;
    private final WaybillItemService waybillItemService;
    private final WaybillSenderService waybillSenderService;
    private final WaybillCallBackManager waybillCallBackManager;

    public WaybillLogicImpl(WaybillMapper waybillMapper, WaybillService waybillService, WaybillItemService waybillItemService,
                            WaybillSenderService waybillSenderService, WaybillCallBackManager waybillCallBackManager) {
        this.waybillMapper = waybillMapper;
        this.waybillService = waybillService;
        this.waybillItemService = waybillItemService;
        this.waybillSenderService = waybillSenderService;
        this.waybillCallBackManager = waybillCallBackManager;
    }

    @Override
    public Pager<WaybillPageVO> getPageList(Page<WaybillPageQueryDTO> page) {
        Integer total = waybillMapper.getTotal(page);
        if (total == 0) {
            return new Pager<>(0, new ArrayList<>());
        }
        List<WaybillPageVO> list = waybillMapper.getPage(page);
        return new Pager<>(total, list);
    }

    @Override
    public WaybillVO getVOById(Long id) {
        Waybill waybill = waybillService.getById(id);
        return waybill == null ? new WaybillVO() : WaybillVO.convertByEntity(waybill);
    }

    @Override
    public WaybillDetailVO getDetailVOById(Long id) {
        Waybill waybill = waybillService.getById(id);
        if (waybill == null) {
            return new WaybillDetailVO();
        }

        WaybillDetailVO vo = new WaybillDetailVO();
        vo.setWaybill(WaybillDetail.convertByEntity(waybill))
                .setItemList(WaybillDetailItem.convertByEntity(waybillItemService.listByWaybillId(id)));
        return vo;
    }

    @Override
    public void updateConfirm(WaybillConfirmFormDTO dto) {
        // 验证货运单
        Waybill waybill = waybillService.validateConfirm(dto.getId());
        // 更新货运单状态为已确认
        waybillService.updateConfirm(waybill.getId());
    }

    @Override
    @Transactional
    public void updateInfo(WaybillInfoFormDTO dto) {
        // 货运单ID
        Long id = dto.getId();
        // 发件人
        Long senderId = dto.getSenderId();
        // 验证货运单
        Waybill waybill = waybillService.validateInfo(id);
        // 验证发件人
        WaybillSender sender = waybillSenderService.validateSenderStatus(senderId);
        // 更新货运单信息 -- 完成
        Waybill updateWaybill = new Waybill();
        updateWaybill.setId(waybill.getId())
                .setSenderName(sender.getName())
                .setSenderMobile(sender.getMobile())
                .setSenderProvince(sender.getProvince())
                .setSenderCity(sender.getCity())
                .setSenderArea(sender.getArea())
                .setSenderDetail(sender.getDetail())
                .setStatus(WaybillStatusEnum.FINISH.getCode())
                .setAmount(dto.getPrice().multiply(new BigDecimal(100)).longValue())
                .setExpressName(dto.getExpressName())
                .setExpressNo(dto.getExpressNo())
                .setSendDate(dto.getSendDate())
                .setPayType(dto.getPayType());
        waybillService.updateById(updateWaybill);
        // 发货回调
        waybillCallBackManager.callBackSend(id);
    }
}
