package com.youyilin.logic.serviceImpl.customer;

import com.youyilin.common.exception.ApiException;
import com.youyilin.common.utils.BeanHelper;
import com.youyilin.customer.entity.CustomerCart;
import com.youyilin.customer.entity.CustomerMini;
import com.youyilin.customer.dto.request.RequestMiniCart;
import com.youyilin.customer.vo.CustomerCartVo;
import com.youyilin.customer.service.CustomerCartService;
import com.youyilin.customer.service.CustomerMiniService;
import com.youyilin.goods.manager.RequestOrderProductLogic;
import com.youyilin.goods.model.request.RequestMiniProduct;
import com.youyilin.logic.service.customer.CustomerMiniCartLogic;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerMiniCartLogicImpl implements CustomerMiniCartLogic {

    private final CustomerMiniService customerMiniService;
    private final RequestOrderProductLogic requestOrderProductLogic;
    private final CustomerCartService customerCartService;

    public CustomerMiniCartLogicImpl(CustomerMiniService customerMiniService, RequestOrderProductLogic requestOrderProductLogic,
                                     CustomerCartService customerCartService) {
        this.customerMiniService = customerMiniService;
        this.requestOrderProductLogic = requestOrderProductLogic;
        this.customerCartService = customerCartService;
    }

    @Override
    public List<CustomerCartVo> listCart() {
        CustomerMini cm = customerMiniService.isLoginNoException();
        if (cm == null) return new ArrayList<>();

        List<CustomerCart> cartList = customerCartService.listByOpenid(cm.getOpenid());
        if (CollectionUtils.isEmpty(cartList)) {
            return new ArrayList<>();
        }

        List<RequestMiniProduct> requestMiniProductList = new ArrayList<>();
        for (CustomerCart item : cartList) {
            RequestMiniProduct miniProduct = new RequestMiniProduct();
            miniProduct.setId(item.getId());
            miniProduct.setProductId(item.getProductId());
            miniProduct.setProductName(item.getProductName());
            miniProduct.setSkuId(item.getSkuId());
            miniProduct.setSkuName(item.getSkuName());
            miniProduct.setImage(item.getImage());
            miniProduct.setSellAmount(item.getSellPrice());
            miniProduct.setNum(item.getNum());

            requestMiniProductList.add(miniProduct);
        }

        requestOrderProductLogic.refreshCartProductInfoBatch(requestMiniProductList);

        return BeanHelper.map(requestMiniProductList, CustomerCartVo.class);
    }

    @Override
    public Long countCart() {
        CustomerMini cm = customerMiniService.isLoginNoException();
        if (cm == null) return 0L;

        return customerCartService.countByOpenid(cm.getOpenid());
    }

    @Override
    public void saveProductToCart(RequestMiniCart request) {
        CustomerMini cm = customerMiniService.isLogin();
        if (request.getNum().compareTo(BigDecimal.ZERO) <= 0) {
            throw new ApiException("数量异常");
        }
        RequestMiniProduct rmp = new RequestMiniProduct();
        rmp.setProductId(request.getProductId());
        rmp.setSkuId(request.getSkuId());
        requestOrderProductLogic.validateMiniProduct(rmp);

        CustomerCart insert = new CustomerCart();
        insert.setCustomerId(cm.getId())
                .setOpenid(cm.getOpenid())
                .setProductId(request.getProductId())
                .setProductName(rmp.getProductName())
                .setSkuId(request.getSkuId())
                .setSkuName(rmp.getSkuName())
                .setImage(rmp.getImage())
                .setNum(request.getNum())
                .setSellPrice(rmp.getSellAmount());

        customerCartService.save(insert);
    }

    @Override
    public void delByIds(List<Long> ids) {
        List<CustomerCart> cartList = customerCartService.listByIds(ids);
        if (CollectionUtils.isEmpty(cartList)) return;

        CustomerMini cm = customerMiniService.isLogin();
        for (CustomerCart item : cartList) {
            if (!item.getOpenid().equals(cm.getOpenid())) {
                throw new ApiException("删除失败");
            }
        }

        customerCartService.removeBatchByIds(ids);
    }
}
