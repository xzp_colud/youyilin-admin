package com.youyilin.logic.serviceImpl.order.settled;

import com.youyilin.logic.service.order.settled.SettledOrderItemLogic;
import com.youyilin.order.service.SettledOrderItemService;
import com.youyilin.order.vo.settled.SettledOrderItemPageVO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SettledOrderItemLogicImpl implements SettledOrderItemLogic {

    private final SettledOrderItemService settledOrderItemService;

    public SettledOrderItemLogicImpl(SettledOrderItemService settledOrderItemService) {
        this.settledOrderItemService = settledOrderItemService;
    }

    @Override
    public List<SettledOrderItemPageVO> listBySettledId(Long settledId) {
        return SettledOrderItemPageVO.listByEntity(settledOrderItemService.listBySettledId(settledId));
    }
}
