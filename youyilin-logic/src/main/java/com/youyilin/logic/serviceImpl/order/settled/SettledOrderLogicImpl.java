package com.youyilin.logic.serviceImpl.order.settled;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.exception.Assert;
import com.youyilin.common.utils.DateUtils;
import com.youyilin.common.utils.FormatAmountUtil;
import com.youyilin.customer.bo.CustomerAccountMonthChargeBO;
import com.youyilin.customer.entity.Customer;
import com.youyilin.customer.manager.CustomerManager;
import com.youyilin.customer.service.CustomerService;
import com.youyilin.logic.service.order.settled.SettledOrderLogic;
import com.youyilin.order.bo.settled.SettledOrderSaveBO;
import com.youyilin.order.dto.settled.*;
import com.youyilin.order.entity.Order;
import com.youyilin.order.entity.OrderLog;
import com.youyilin.order.entity.SettledOrder;
import com.youyilin.order.entity.SettledOrderItem;
import com.youyilin.order.enums.CommonSettledStatusEnum;
import com.youyilin.order.enums.OrderLogEnum;
import com.youyilin.order.enums.SettledOrderStatusEnum;
import com.youyilin.order.mapper.SettledOrderMapper;
import com.youyilin.order.service.OrderLogService;
import com.youyilin.order.service.OrderService;
import com.youyilin.order.service.SettledOrderItemService;
import com.youyilin.order.service.SettledOrderService;
import com.youyilin.order.utils.NoUtils;
import com.youyilin.order.vo.settled.SettledOrderDetail;
import com.youyilin.order.vo.settled.SettledOrderDetailItem;
import com.youyilin.order.vo.settled.SettledOrderDetailVO;
import com.youyilin.order.vo.settled.SettledOrderPageVO;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class SettledOrderLogicImpl implements SettledOrderLogic {

    private final SettledOrderMapper settledOrderMapper;
    private final SettledOrderService settledOrderService;
    private final SettledOrderItemService settledOrderItemService;
    private final CustomerService customerService;
    private final OrderService orderService;
    private final OrderLogService orderLogService;
    private final CustomerManager customerManager;

    public SettledOrderLogicImpl(SettledOrderMapper settledOrderMapper, SettledOrderService settledOrderService,
                                 SettledOrderItemService settledOrderItemService, CustomerService customerService,
                                 OrderService orderService, OrderLogService orderLogService, CustomerManager customerManager) {
        this.settledOrderMapper = settledOrderMapper;
        this.settledOrderService = settledOrderService;
        this.settledOrderItemService = settledOrderItemService;
        this.customerService = customerService;
        this.orderService = orderService;
        this.orderLogService = orderLogService;
        this.customerManager = customerManager;
    }

    @Override
    public Pager<SettledOrderPageVO> getPageList(Page<SettledOrderPageQueryDTO> page) {
        Integer total = settledOrderMapper.getTotal(page);
        if (total == 0) {
            return new Pager<>(0, new ArrayList<>());
        }
        List<SettledOrderPageVO> list = settledOrderMapper.getPage(page);
        return new Pager<>(total, list);
    }

    @Override
    public String getLastBalanceByCustomerId(Long customerId) {
        SettledOrder order = settledOrderService.getLastSettledByCustomerId(customerId);
        if (order == null || order.getBalanceAmount() == null) {
            return "0";
        }
        return FormatAmountUtil.format(order.getBalanceAmount());
    }

    @Override
    public SettledOrderDetailVO getDetailVOById(Long id) {
        SettledOrder settled = settledOrderService.getById(id);
        if (settled == null) {
            return new SettledOrderDetailVO();
        }
        SettledOrderDetailVO vo = new SettledOrderDetailVO();
        vo.setSettled(SettledOrderDetail.convertByEntity(settled))
                .setItemList(SettledOrderDetailItem.convertByEntity(settledOrderItemService.listBySettledId(id)))
                .setLogList(new ArrayList<>());
//                .setLogList(OrderLogDetailVO.convertByEntity(orderLogService.listBySn(settled.getSn())));
        return vo;
    }

    @Override
    @Transactional
    public void saveSettled(SettledOrderFormDTO dto) {
        // 客户ID
        Long customerId = dto.getCustomerId();
        // 订单列表
        List<Long> orderIds = dto.getOrderIds();
        // 验证客户
        Customer customer = customerService.validateCustomer(customerId);
        // 验证当前客户是否可以结款
        settledOrderService.validateSettledProcessingByCustomerId(customerId);
        // 验证销售单
        List<Order> orderList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(orderIds)) {
            orderList = orderService.validateCanSettledBatch(orderIds, customerId);
        }
        // 上次的结款单
        SettledOrder lastSettled = settledOrderService.getLastSettledByCustomerId(customerId);

        // 创建结款单
        SettledOrderSaveBO bo = new SettledOrderSaveBO();
        bo.setDto(dto).setCustomer(customer).setOrderList(orderList).setLastSettled(lastSettled);
        // 创建结款单
        this.constructSettledOrder(bo);

        // 持久化结款单
        settledOrderService.save(bo.getSettled());
        // 持久化结款单明细
        if (CollectionUtils.isNotEmpty(bo.getItemList())) {
            settledOrderItemService.saveBatch(bo.getItemList());
        }
        // 日志
        orderLogService.saveLog(bo.getId(), bo.getSn(), OrderLogEnum.CREATE_OR_UPDATE.getCode());
        // 反馈结款信息
        this.callBackToSalesOrder(bo.getId(), CommonSettledStatusEnum.PROCESSING);
    }

    /**
     * 构造销售结款单
     */
    private void constructSettledOrder(SettledOrderSaveBO bo) {
        // 结款单ID 编号
        Long settledId = IdWorker.getId();
        String settledSn = NoUtils.getSettledPaymentOrderPrefix();
        // 实际结款金额
        BigDecimal dtoReallyAmount = bo.getDto().getReallyAmount();
        Assert.isFalse(dtoReallyAmount == null || dtoReallyAmount.compareTo(BigDecimal.ZERO) <= 0, "实际金额需大于零");
        long reallyAmount = dtoReallyAmount.multiply(new BigDecimal("100")).longValue();
        // 客户
        Customer customer = bo.getCustomer();
        // 结款单明细
        List<SettledOrderItem> itemList = new ArrayList<>();
        // 统计预计结款总额
        long totalPlanAmount = 0L;
        for (int i = 0; i < bo.getOrderList().size(); i++) {
            // 序号
            int index = i + 1;
            // 销售单
            Order item = bo.getOrderList().get(i);
            // 销售单结款金额
            Long planAmount = item.getSellAmount();
            // 结款总额
            totalPlanAmount = totalPlanAmount + planAmount;
            // 结款单明细
            SettledOrderItem settledItem = new SettledOrderItem();
            settledItem
                    .setSettledOrderId(settledId)
                    .setSettledOrderSn(settledSn)
                    .setSn(settledSn + "-" + index)
                    .setSourceId(item.getId())
                    .setSourceNo(item.getSn())
                    .setAmount(planAmount);
            itemList.add(settledItem);
        }
        // 结款时间
        String now = DateUtils.getDate();
        String year = now.substring(0, 4);
        String month = now.substring(5, 7);
        // 上次结余
        Long lastBalanceAmount = 0L;
        if (bo.getLastSettled() != null) {
            lastBalanceAmount = bo.getLastSettled().getBalanceAmount();
        }
        // 本次结余
//        Long balanceAmount = reallyAmount - totalPlanAmount + lastBalanceAmount;
        // 结款单
        SettledOrder settledOrder = new SettledOrder();
        settledOrder
                .setId(settledId)
                .setSn(settledSn)
                .setCustomerId(customer.getId())
                .setCustomerName(customer.getName())
                .setCustomerPhone(customer.getPhone())
                .setStatus(SettledOrderStatusEnum.DEFAULT.getCode())
                .setYear(year)
                .setMonth(month)
                .setPlanAmount(totalPlanAmount)
                .setReallyAmount(reallyAmount)
                .setLastBalanceAmount(lastBalanceAmount)
//                .setBalanceAmount(balanceAmount)
                .setBalanceAmount(0L)
                .setRemark(bo.getDto().getRemark());

        bo.setId(settledId).setSn(settledSn).setSettled(settledOrder).setItemList(itemList);
    }

    @Override
    @Transactional
    public void updateConfirm(SettledOrderConfirmFormDTO dto) {
        // 结款单ID
        Long id = dto.getId();
        // 验证结款单是否可以确认
        SettledOrder settled = settledOrderService.validateConfirm(id);
        // 更新状态为已确认
        settledOrderService.updateConfirm(id);
        // 日志
        orderLogService.saveLog(settled.getId(), settled.getSn(), OrderLogEnum.SUBMIT.getCode());
    }

    @Override
    @Transactional
    public void updateCancel(SettledOrderCancelFormDTO dto) {
        // 结款单ID
        Long id = dto.getId();
        // 验证结款单是否可以取消
        SettledOrder settled = settledOrderService.validateCancel(id);
        // 更新状态为已取消
        settledOrderService.updateCancel(id);
        // 日志
        orderLogService.saveLog(settled.getId(), settled.getSn(), OrderLogEnum.CANCEL.getCode());
        // 反馈结款信息
        this.callBackToSalesOrder(settled.getId(), CommonSettledStatusEnum.CANCEL);
    }

    @Override
    @Transactional
    public void updateFinish(SettledOrderFinishFormDTO dto) {
        Assert.isTrue(dto.getCollectAmount() != null && dto.getCollectAmount().compareTo(BigDecimal.ZERO) >= 0, "收款金额不能为空");
        // 结款单ID
        Long id = dto.getId();
        // 验证结款单是否可以完成
        SettledOrder settled = settledOrderService.validateFinish(id);
        // 计算本次结余
        Long collectAmount = dto.getCollectAmount().multiply(new BigDecimal("100")).longValue();
        Long balanceAmount = settled.getLastBalanceAmount() + collectAmount - settled.getReallyAmount();
        SettledOrder updateSo = new SettledOrder();
        updateSo.setId(settled.getId())
                .setCollectAmount(collectAmount)
                .setBalanceAmount(balanceAmount)
                .setCollectRemark(dto.getRemark())
                .setCollectType(dto.getPayType())
                .setCollectImg(dto.getImageUrl())
                .setCollectDate(dto.getOptDate())
                .setStatus(SettledOrderStatusEnum.FINISH.getCode())
                .setFinishDate(new Date());
        // 更新状态为已完成
        settledOrderService.updateById(updateSo);
        // 日志
        orderLogService.saveLog(settled.getId(), settled.getSn(), OrderLogEnum.FINISH.getCode());
        // 账户充值
        CustomerAccountMonthChargeBO bo = new CustomerAccountMonthChargeBO();
        bo.setSourceId(settled.getId())
                .setSourceSn(settled.getSn())
                .setAmount(settled.getReallyAmount())
                .setPayType(dto.getPayType())
                .setImageUrl(dto.getImageUrl())
                .setOptDate(new Date())
                .setCustomerId(settled.getCustomerId())
                .setRemark("结款单结算充值");
        customerManager.updateAccountChargeMonth(bo);
        // 反馈结款信息
        this.callBackToSalesOrder(settled.getId(), CommonSettledStatusEnum.FINISH);
    }

    /**
     * 反馈结款单信息
     *
     * @param settledId 结款单ID
     */
    private void callBackToSalesOrder(Long settledId, CommonSettledStatusEnum status) {
        List<SettledOrderItem> itemList = settledOrderItemService.listBySettledId(settledId);
        if (CollectionUtils.isEmpty(itemList)) {
            return;
        }
//        Assert.notEmpty(itemList, "参数异常");
        List<Long> orderIds = new ArrayList<>();
        for (SettledOrderItem item : itemList) {
            orderIds.add(item.getSourceId());
        }
        // 更新销售单结款状态
        orderService.updateSettledStatus(orderIds, status.getCode());
        // 日志
        Integer type = OrderLogEnum.SETTLED.getCode();
        if (status == CommonSettledStatusEnum.CANCEL) {
            type = OrderLogEnum.SETTLED_CANCEL.getCode();
        } else if (status == CommonSettledStatusEnum.PROCESSING) {
            type = OrderLogEnum.SETTLED_APPLY.getCode();
        }
        List<OrderLog> logList = new ArrayList<>();
        for (SettledOrderItem item : itemList) {
            OrderLog log = new OrderLog();
            log.setOrderId(item.getSourceId())
                    .setSn(item.getSourceNo())
                    .setType(type);
            logList.add(log);
        }
        orderLogService.saveBatch(logList);
    }
}
