package com.youyilin.logic.serviceImpl.warehouse;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.exception.Assert;
import com.youyilin.common.utils.DateUtils;
import com.youyilin.logic.service.warehouse.WarehouseLogic;
import com.youyilin.warehouse.dto.warehouse.WarehouseDeleteFormDTO;
import com.youyilin.warehouse.dto.warehouse.WarehouseFormDTO;
import com.youyilin.warehouse.dto.warehouse.WarehousePageQueryDTO;
import com.youyilin.warehouse.entity.Warehouse;
import com.youyilin.warehouse.manager.InventoryManager;
import com.youyilin.warehouse.mapper.WarehouseMapper;
import com.youyilin.warehouse.service.InventoryService;
import com.youyilin.warehouse.service.WarehouseAreaService;
import com.youyilin.warehouse.service.WarehouseService;
import com.youyilin.warehouse.vo.warehouse.WarehouseEditVO;
import com.youyilin.warehouse.vo.warehouse.WarehousePageVO;
import com.youyilin.warehouse.vo.warehouse.WarehouseVO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class WarehouseLogicImpl implements WarehouseLogic {

    private final WarehouseMapper warehouseMapper;
    private final WarehouseService warehouseService;
    private final WarehouseAreaService warehouseAreaService;
    private final InventoryService inventoryService;
    private final InventoryManager inventoryManager;

    public WarehouseLogicImpl(WarehouseMapper warehouseMapper, WarehouseService warehouseService,
                              WarehouseAreaService warehouseAreaService, InventoryService inventoryService,
                              InventoryManager inventoryManager) {
        this.warehouseMapper = warehouseMapper;
        this.warehouseService = warehouseService;
        this.warehouseAreaService = warehouseAreaService;
        this.inventoryService = inventoryService;
        this.inventoryManager = inventoryManager;
    }

    @Override
    public Pager<WarehousePageVO> getPageList(Page<WarehousePageQueryDTO> page) {
        Integer total = warehouseMapper.getTotal(page);
        if (total == 0) {
            return new Pager<>(0, new ArrayList<>());
        }
        List<WarehousePageVO> list = warehouseMapper.getPage(page);
        return new Pager<>(total, list);
    }

    @Override
    public List<WarehouseVO> listAll() {
        return WarehouseVO.convertByEntity(warehouseService.list());
    }

    @Override
    public WarehouseEditVO getEditVOById(Long id) {
        Warehouse dbWarehouse = warehouseService.getById(id);
        return dbWarehouse == null ? new WarehouseEditVO() : WarehouseEditVO.convertByEntity(dbWarehouse);
    }

    @Override
    public void saveWarehouse(WarehouseFormDTO dto) {
        Warehouse warehouse = dto.convertToEntity();
        warehouse.setSn("WN" + DateUtils.dateTimeNow());
        // 验证仓库编号是否唯一
        warehouseService.validateSnUnique(warehouse);
        if (warehouse.getId() == null) {
            // 保存
            warehouseService.save(warehouse);
        } else {
            // 验证
            warehouseService.validateWarehouse(warehouse.getId());
            // 更新
            warehouseService.updateById(warehouse);
        }
    }

    @Override
    @Transactional
    public void delWarehouse(WarehouseDeleteFormDTO dto) {
        Long warehouseId = dto.getId();
        // 验证仓库
        warehouseService.validateWarehouse(warehouseId);
        // 验证库存余量
        int inventoryNum = inventoryService.countProductNumByWarehouseId(warehouseId);
        Assert.isFalse(inventoryNum > 0, "当前仓库还有产品库存余量");
        // 删除库存信息
        inventoryManager.delByWarehouseId(warehouseId);
        // 移除仓库
        warehouseService.removeById(warehouseId);
        // 移除库区
        warehouseAreaService.delWarehouseAreaByWarehouseId(warehouseId);
    }
}
