package com.youyilin.logic.serviceImpl.order;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.logic.service.order.OrderLogLogic;
import com.youyilin.order.dto.OrderLogPageQueryDTO;
import com.youyilin.order.mapper.OrderLogMapper;
import com.youyilin.order.vo.OrderLogPageVO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderLogLogicImpl implements OrderLogLogic {

    private final OrderLogMapper orderLogMapper;

    public OrderLogLogicImpl(OrderLogMapper orderLogMapper) {
        this.orderLogMapper = orderLogMapper;
    }

    @Override
    public Pager<OrderLogPageVO> getPageList(Page<OrderLogPageQueryDTO> page) {
        Integer total = orderLogMapper.getTotal(page);
        if (total == 0) {
            return new Pager<>(0, new ArrayList<>());
        }
        List<OrderLogPageVO> list = orderLogMapper.getPage(page);
        return new Pager<>(total, list);
    }
}
