package com.youyilin.logic.serviceImpl;

import com.youyilin.logic.mapper.DashboardMapper;
import com.youyilin.logic.service.DashboardLogic;
import com.youyilin.logic.vo.DashboardCustomerCard;
import com.youyilin.logic.vo.DashboardCustomerCardVO;
import com.youyilin.logic.vo.TimeDTO;
import com.youyilin.logic.vo.TimeVO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DashboardLogicImpl implements DashboardLogic {

    private final DashboardMapper dashboardMapper;

    public DashboardLogicImpl(DashboardMapper dashboardMapper) {
        this.dashboardMapper = dashboardMapper;
    }

    @Override
    public DashboardCustomerCardVO getDashboardCustomerCard() {
        TimeDTO dto = new TimeDTO();
        // 新增客户数 当日、昨日、本周、上周、本月、上月、本年、上年
        Long newCustomerNow = dashboardMapper.countNewCustomerNum(dto.getNow(), dto.getTomorrow());
        Long newCustomerYesterday = dashboardMapper.countNewCustomerNum(dto.getYesterday(), dto.getNow());
        Long newCustomerThisWeek = dashboardMapper.countNewCustomerNum(dto.getThisWeekStart(), dto.getThisWeekEnd());
        Long newCustomerLastWeek = dashboardMapper.countNewCustomerNum(dto.getLastWeekStart(), dto.getLastWeekEnd());
        Long newCustomerThisMonth = dashboardMapper.countNewCustomerNum(dto.getThisMonthStart(), dto.getThisMonthEnd());
        Long newCustomerLastMonth = dashboardMapper.countNewCustomerNum(dto.getLastMonthStart(), dto.getLastMonthEnd());
        Long newCustomerThisYear = dashboardMapper.countNewCustomerNum(dto.getThisYearStart(), dto.getThisYearEnd());
        Long newCustomerLastYear = dashboardMapper.countNewCustomerNum(dto.getLastYearStart(), dto.getLastYearEnd());
        // 活跃客户数 当日、昨日、本周、上周、本月、上月、本年、上年
        Long activeCustomerNow = dashboardMapper.countActiveCustomerNum(dto.getNow(), dto.getTomorrow());
        Long activeCustomerYesterday = dashboardMapper.countActiveCustomerNum(dto.getYesterday(), dto.getNow());
        Long activeCustomerThisWeek = dashboardMapper.countActiveCustomerNum(dto.getThisWeekStart(), dto.getThisWeekEnd());
        Long activeCustomerLastWeek = dashboardMapper.countActiveCustomerNum(dto.getLastWeekStart(), dto.getLastWeekEnd());
        Long activeCustomerThisMonth = dashboardMapper.countActiveCustomerNum(dto.getThisMonthStart(), dto.getThisMonthEnd());
        Long activeCustomerLastMonth = dashboardMapper.countActiveCustomerNum(dto.getLastMonthStart(), dto.getLastMonthEnd());
        Long activeCustomerThisYear = dashboardMapper.countActiveCustomerNum(dto.getThisYearStart(), dto.getThisYearEnd());
        Long activeCustomerLastYear = dashboardMapper.countActiveCustomerNum(dto.getLastYearStart(), dto.getLastYearEnd());

        List<DashboardCustomerCard> customerCardList = new ArrayList<>();
        customerCardList.add(new DashboardCustomerCard(TimeVO.TimeEnum.DAY.getValue(), newCustomerNow, activeCustomerNow, newCustomerYesterday, activeCustomerYesterday, "当日新增数", "当日活跃数", "昨日新增数", "昨日活跃数"));
        customerCardList.add(new DashboardCustomerCard(TimeVO.TimeEnum.WEEK.getValue(), newCustomerThisWeek, activeCustomerThisWeek, newCustomerLastWeek, activeCustomerLastWeek, "本周新增数", "本周活跃数", "上周新增数", "上周活跃数"));
        customerCardList.add(new DashboardCustomerCard(TimeVO.TimeEnum.MONTH.getValue(), newCustomerThisMonth, activeCustomerThisMonth, newCustomerLastMonth, activeCustomerLastMonth, "本月新增数", "本月活跃数", "上月新增数", "上月活跃数"));
        customerCardList.add(new DashboardCustomerCard(TimeVO.TimeEnum.YEAR.getValue(), newCustomerThisYear, activeCustomerThisYear, newCustomerLastYear, activeCustomerLastYear, "本年新增数", "本年活跃数", "去年新增数", "去年活跃数"));

        DashboardCustomerCardVO vo = new DashboardCustomerCardVO();
        vo.setTimeList(TimeVO.initYMWD())
                .setItemList(customerCardList)
                .setItem(customerCardList.get(0))
                .setTimeValue(TimeVO.TimeEnum.DAY.getValue())
                .setTotalNum(dashboardMapper.countTotalCustomerNum());
        return vo;
    }

}
