package com.youyilin.logic.serviceImpl.customer;

import com.youyilin.common.enums.BooleanEnum;
import com.youyilin.customer.dto.address.CustomerMiniAddressFormDTO;
import com.youyilin.customer.entity.CustomerAddress;
import com.youyilin.customer.entity.CustomerMini;
import com.youyilin.customer.service.CustomerAddressService;
import com.youyilin.customer.service.CustomerMiniService;
import com.youyilin.customer.vo.address.CustomerAddressDetailVO;
import com.youyilin.customer.vo.address.CustomerAddressEditVO;
import com.youyilin.customer.vo.address.CustomerAddressPageVO;
import com.youyilin.logic.service.customer.CustomerMiniAddressLogic;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerMiniAddressLogicImpl implements CustomerMiniAddressLogic {

    private final CustomerMiniService customerMiniService;
    private final CustomerAddressService customerAddressService;

    public CustomerMiniAddressLogicImpl(CustomerMiniService customerMiniService, CustomerAddressService customerAddressService) {
        this.customerMiniService = customerMiniService;
        this.customerAddressService = customerAddressService;
    }

    @Override
    public List<CustomerAddressPageVO> listAddress() {
        CustomerMini cm = customerMiniService.isLoginNoException();
        if (cm == null) {
            return new ArrayList<>();
        }

        return CustomerAddressPageVO.convertByEntity(customerAddressService.listByCustomerId(cm.getId()));
    }

    @Override
    public CustomerAddressEditVO getAddress(Long id) {
        CustomerAddress address = customerAddressService.getById(id);
        return address == null ? new CustomerAddressEditVO() : CustomerAddressEditVO.convertByEntity(address);
    }

    @Override
    public CustomerAddressDetailVO getDefault() {
        CustomerMini cm = customerMiniService.isLoginNoException();
        if (cm == null) {
            return null;
        }

        List<CustomerAddress> list = customerAddressService.listByCustomerId(cm.getId());
        if (CollectionUtils.isEmpty(list)) {
            return null;
        }

        CustomerAddress defaultAddress = null;
        for (CustomerAddress item : list) {
            if (item.getDefaultFlag() != null && item.getDefaultFlag().equals(BooleanEnum.TRUE.getCode())) {
                defaultAddress = item;
                break;
            }
            if (defaultAddress == null) {
                defaultAddress = item;
            }
        }
        return CustomerAddressDetailVO.convertByEntity(defaultAddress);
    }

    @Override
    public void saveAddress(CustomerMiniAddressFormDTO dto) {
        // 验证是否已登录
        CustomerMini cm = customerMiniService.isLogin();
        // 数据转换
        CustomerAddress address = dto.convertToEntity();
        address.setCustomerId(cm.getId());
        // 保存
        customerAddressService.saveCustomerAddress(address);
    }

    @Override
    public void updateDefault(Long id) {
        // 是否已登录
        CustomerMini cm = customerMiniService.isLogin();
        // 验证地址
        CustomerAddress address = customerAddressService.validateAddress(id);
        // 是否一致
        if (!address.getCustomerId().equals(cm.getId())) {
            return;
        }
        // 取消默认
        customerAddressService.updateDefaultCancel(cm.getId());
        // 修改为默认
        customerAddressService.updateDefault(id);
    }

    @Override
    public void delAddress(Long id) {
        // 验证是否登录
        CustomerMini cm = customerMiniService.isLogin();
        // 获取地址
        CustomerAddress address = customerAddressService.getById(id);
        // 是否一致
        if (address == null || !address.getCustomerId().equals(cm.getId())) {
            return;
        }
        // 删除
        customerAddressService.removeById(id);
    }
}
