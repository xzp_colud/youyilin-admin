package com.youyilin.logic.serviceImpl.product;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.enums.BooleanEnum;
import com.youyilin.goods.dto.category.CategoryFormDTO;
import com.youyilin.goods.dto.category.CategoryPageQueryDTO;
import com.youyilin.goods.entity.Category;
import com.youyilin.goods.mapper.CategoryMapper;
import com.youyilin.goods.service.CategoryParamsService;
import com.youyilin.goods.service.CategoryService;
import com.youyilin.goods.service.CategoryUnitService;
import com.youyilin.goods.service.ProductService;
import com.youyilin.goods.vo.category.CategoryDetailVO;
import com.youyilin.goods.vo.category.CategoryEditVO;
import com.youyilin.goods.vo.category.CategoryPageVO;
import com.youyilin.goods.vo.category.CategoryVO;
import com.youyilin.logic.service.product.CategoryLogic;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryLogicImpl implements CategoryLogic {

    private final CategoryMapper categoryMapper;
    private final CategoryService categoryService;
    private final CategoryParamsService categoryParamsService;
    private final CategoryUnitService categoryUnitService;
    private final ProductService productService;

    public CategoryLogicImpl(CategoryMapper categoryMapper, CategoryService categoryService,
                             CategoryParamsService categoryParamsService, CategoryUnitService categoryUnitService,
                             ProductService productService) {
        this.categoryMapper = categoryMapper;
        this.categoryService = categoryService;
        this.categoryParamsService = categoryParamsService;
        this.categoryUnitService = categoryUnitService;
        this.productService = productService;
    }

    @Override
    public Pager<CategoryPageVO> getPageList(Page<CategoryPageQueryDTO> page) {
        Integer total = categoryMapper.getTotal(page);
        if (total == 0) {
            return new Pager<>(0, new ArrayList<>());
        }
        List<CategoryPageVO> list = categoryMapper.getPage(page);
        return new Pager<>(total, list);
    }

    @Override
    public CategoryVO getVOById(Long id) {
        Category category = categoryService.getById(id);
        return category == null ? new CategoryVO() : CategoryVO.convertByEntity(category);
    }

    @Override
    public List<CategoryVO> listAll() {
        List<Category> list = categoryService.listAll(BooleanEnum.TRUE.getCode());
        return CategoryVO.convertByEntity(list);
    }

    @Override
    public CategoryEditVO getEditVOById(Long id) {
        Category category = categoryService.getById(id);
        if (category == null) {
            return new CategoryEditVO();
        }
        // 数据转换
        CategoryEditVO vo = CategoryEditVO.convertByEntity(category);
        // 已配置参数IDS
        vo.setParamsIds(categoryParamsService.listParamsIdsByCategoryId(id));
        // 已配置规格IDS
        vo.setUnitIds(categoryUnitService.listUnitIdsByCategoryId(id));
        return vo;
    }

    @Override
    public CategoryDetailVO getDetailVOById(Long id) {
        Category category = categoryService.getById(id);
        if (category == null) {
            return new CategoryDetailVO();
        }
        // 数据转换
        CategoryDetailVO vo = CategoryDetailVO.convertByEntity(category);
        // 已配置参数名称
        vo.setParamsList(categoryParamsService.listParamsNameByCategoryId(id));
        // 已配置规格名称
        vo.setUnitList(categoryUnitService.listUnitNameByCategoryId(id));
        return vo;
    }

    @Override
    @Transactional
    public void saveCategory(CategoryFormDTO dto) {
        Category category = dto.convertToEntity();
        // 验证标题唯一性
        categoryService.validateTitleUnique(category);

        if (category.getId() == null) {
            // 保存
            categoryService.save(category);
        } else {
            // 编辑验证
            categoryService.validateCategory(category.getId());
            // 更新
            categoryService.updateById(category);
        }

        // 分类ID
        Long categoryId = category.getId();
        // 删除已配置的参数
        categoryParamsService.delByCategoryId(categoryId);
        // TODO 断言：删除的属性是否关联了商品
        // 保存参数
        categoryParamsService.saveParamsBatch(categoryId, dto.getParamsIds());

        // 删除已配置的规格
        categoryUnitService.delByCategoryId(categoryId);
        // TODO 断言：删除的规格是否关联的商品
        // 保存规格
        categoryUnitService.saveCategoryUnitBatch(categoryId, dto.getUnitIds());
        // 冗余商品中的分类名称
        productService.updateRedundancyCategoryName(categoryId, dto.getTitle());
    }
}
