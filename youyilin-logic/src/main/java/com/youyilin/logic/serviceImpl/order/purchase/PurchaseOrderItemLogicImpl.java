package com.youyilin.logic.serviceImpl.order.purchase;

import com.youyilin.common.exception.Assert;
import com.youyilin.logic.service.order.purchase.PurchaseOrderItemLogic;
import com.youyilin.order.dto.purchase.PurchaseItemReviseNumDTO;
import com.youyilin.order.entity.PurchaseOrder;
import com.youyilin.order.entity.PurchaseOrderItem;
import com.youyilin.order.enums.OrderLogEnum;
import com.youyilin.order.enums.PurchaseOrderItemStatusEnum;
import com.youyilin.order.service.OrderLogService;
import com.youyilin.order.service.PurchaseOrderItemService;
import com.youyilin.order.service.PurchaseOrderService;
import com.youyilin.order.vo.purchase.PurchaseOrderItemPageVO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class PurchaseOrderItemLogicImpl implements PurchaseOrderItemLogic {

    private final PurchaseOrderService purchaseOrderService;
    private final PurchaseOrderItemService purchaseOrderItemService;
    private final OrderLogService orderLogService;

    public PurchaseOrderItemLogicImpl(PurchaseOrderService purchaseOrderService,
                                      PurchaseOrderItemService purchaseOrderItemService,
                                      OrderLogService orderLogService) {
        this.purchaseOrderService = purchaseOrderService;
        this.purchaseOrderItemService = purchaseOrderItemService;
        this.orderLogService = orderLogService;
    }

    @Override
    public List<PurchaseOrderItemPageVO> listByPurchaseOrderId(Long purchaseOrderId) {
        if (purchaseOrderId == null) {
            return new ArrayList<>();
        }
        return PurchaseOrderItemPageVO.convertByEntity(purchaseOrderItemService.listByOrderId(purchaseOrderId));
    }

    @Override
    @Transactional
    public void updateReviseNum(PurchaseItemReviseNumDTO dto) {
        // 采购单ID
        Long purchaseId = dto.getPurchaseId();
        // 明细ID
        Long id = dto.getId();
        // 修正数量
        BigDecimal reviseNum = dto.getReviseNum();
        Assert.isFalse(reviseNum == null || reviseNum.compareTo(BigDecimal.ZERO) <= 0, "修正米数需大于0");
        // 验证采购单
        PurchaseOrder purchaseOrder = purchaseOrderService.validatePurchaseOrder(purchaseId);
        // 验证采购明细
        PurchaseOrderItem purchaseOrderItem = purchaseOrderItemService.validatePurchaseOrderItem(id);
        // 订单是否一致
        boolean isEquals = purchaseId.equals(purchaseOrderItem.getOrderId());
        Assert.isTrue(isEquals, "采购单异常");
        // 状态是否允许
        boolean statusFlag = purchaseOrderItem.getStatus() != null && purchaseOrderItem.getStatus().equals(PurchaseOrderItemStatusEnum.CHECK_NO_IN.getCode());
        Assert.isTrue(statusFlag, "只允许操作已检验未入库商品");
        // 修正实际检验数量
        purchaseOrderItemService.updateReviseNum(id, reviseNum);
        // 保存日志
        String remark = purchaseOrderItem.getItemSn() + ": 修改数量 " + reviseNum + " 原因: " + dto.getRemark();
        orderLogService.saveLog(purchaseId, purchaseOrder.getSn(), OrderLogEnum.PURCHASE_ITEM_REVISE_NUM.getCode(), remark);
    }
}
