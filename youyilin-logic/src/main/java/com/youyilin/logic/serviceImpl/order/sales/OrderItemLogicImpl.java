package com.youyilin.logic.serviceImpl.order.sales;

import com.youyilin.logic.service.order.sales.OrderItemLogic;
import com.youyilin.order.service.OrderItemService;
import com.youyilin.order.vo.sales.OrderItemPageVO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderItemLogicImpl implements OrderItemLogic {

    private final OrderItemService orderItemService;

    public OrderItemLogicImpl(OrderItemService orderItemService) {
        this.orderItemService = orderItemService;
    }

    @Override
    public List<OrderItemPageVO> listByOrderId(Long orderId) {
        if (orderId == null) {
            return new ArrayList<>();
        }
        return OrderItemPageVO.convertByEntity(orderItemService.listByOrderId(orderId));
    }
}
