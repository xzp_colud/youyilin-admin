package com.youyilin.controller;

import com.youyilin.common.bean.R;
import com.youyilin.common.utils.SecurityUtil;
import com.youyilin.system.dto.user.UserResetPasswordDto;
import com.youyilin.system.dto.user.UserUpdateMeFormDTO;
import com.youyilin.system.entity.SysUser;
import com.youyilin.system.logic.SysMenuLogic;
import com.youyilin.system.logic.SysUserLogic;
import com.youyilin.system.service.SysUserService;
import com.youyilin.system.vo.user.UserVO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class LoginController {

    private final SysUserService sysUserService;
    private final SysMenuLogic sysMenuLogic;
    private final SysUserLogic sysUserLogic;

    public LoginController(SysUserService sysUserService, SysMenuLogic sysMenuLogic, SysUserLogic sysUserLogic) {
        this.sysUserService = sysUserService;
        this.sysMenuLogic = sysMenuLogic;
        this.sysUserLogic = sysUserLogic;
    }

    @GetMapping("/api/isLogin")
    public R<Boolean> isLogin() {
        try {
            SecurityUtil.getLoginUser();
            return R.succ(true);
        } catch (Exception e) {
            return R.fail();
        }
    }

    /**
     * 登录用户
     */
    @GetMapping("getLoginUser")
    public R<Map<String, Object>> getLoginUser() {
        String userName = SecurityUtil.getUsername();
        SysUser sysUser = sysUserService.getByUserName(userName);
        if (sysUser != null) {
            List<String> perms = sysMenuLogic.listLoginPerms(sysUser.getId());
            Map<String, Object> map = new HashMap<>();
            map.put("userName", sysUser.getUserName());
            map.put("avatarUrl", sysUser.getAvatarUrl());
            map.put("realName", sysUser.getRealName());
            map.put("jobNumber", sysUser.getJobNumber());
            map.put("loginDate", sysUser.getLoginDate());
            map.put("loginIp", sysUser.getLoginIp());
            map.put("loginLocation", sysUser.getLoginLocation());
            map.put("perms", perms);
            return R.succ(map);
        }
        return R.succ(new HashMap<>());
    }

    /**
     * 获取登录者信息
     */
    @GetMapping("getUserInfo")
    public R<UserVO> getById() {
        String userName = SecurityUtil.getUsername();
        return R.succ(sysUserLogic.getByUserName(userName));
    }

    /**
     * 更新自己的用户信息
     */
    @PostMapping("updateUser")
    public R<Map<String, Object>> updateUser(@RequestBody UserUpdateMeFormDTO dto) {
        sysUserLogic.updateLoginUserInfo(dto);
        return R.success();
    }

    /**
     * 修改密码
     */
    @PostMapping("changePassword")
    public R<Map<String, Object>> updateUser(@RequestBody UserResetPasswordDto dto) {
        sysUserLogic.updatePassword(dto);
        return R.success();
    }
}
