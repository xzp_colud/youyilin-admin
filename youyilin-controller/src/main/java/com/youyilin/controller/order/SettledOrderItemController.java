package com.youyilin.controller.order;

import com.youyilin.common.bean.R;
import com.youyilin.logic.service.order.settled.SettledOrderItemLogic;
import com.youyilin.order.vo.settled.SettledOrderItemPageVO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 结款单明细 控制层
 */
@RestController
@RequestMapping("settled.order.item")
public class SettledOrderItemController {

    private final SettledOrderItemLogic settledOrderItemLogic;

    public SettledOrderItemController(SettledOrderItemLogic settledOrderItemLogic) {
        this.settledOrderItemLogic = settledOrderItemLogic;
    }

    /**
     * 某结款单明细
     */
    @GetMapping("/listBySettledId/{settledId}")
    public R<List<SettledOrderItemPageVO>> getPageList(@PathVariable Long settledId) {
        return R.succ(settledOrderItemLogic.listBySettledId(settledId));
    }
}
