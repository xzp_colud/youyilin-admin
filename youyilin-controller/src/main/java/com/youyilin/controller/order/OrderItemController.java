package com.youyilin.controller.order;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.R;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.logic.service.order.sales.OrderItemLogic;
import com.youyilin.order.vo.sales.OrderItemPageVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 订单商品 控制层
 */
@RestController
@RequestMapping("order.item")
public class OrderItemController {

    private final OrderItemLogic orderItemLogic;

    public OrderItemController(OrderItemLogic orderItemLogic) {
        this.orderItemLogic = orderItemLogic;
    }

    /**
     * 某销售单商品明细
     */
    @Log(title = "销售订单", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('order:item')")
    @GetMapping("/listByOrderId/{orderId}")
    public R<List<OrderItemPageVO>> getPageList(@PathVariable Long orderId) {
        return R.succ(orderItemLogic.listByOrderId(orderId));
    }
}
