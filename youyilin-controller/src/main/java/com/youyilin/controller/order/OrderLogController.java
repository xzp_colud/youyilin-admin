package com.youyilin.controller.order;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.bean.R;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.logic.service.order.OrderLogLogic;
import com.youyilin.order.dto.OrderLogPageQueryDTO;
import com.youyilin.order.vo.OrderLogPageVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 订单日志 控制层
 */
@RestController
@RequestMapping("order.log")
public class OrderLogController {

    private final OrderLogLogic orderLogLogic;

    public OrderLogController(OrderLogLogic orderLogLogic) {
        this.orderLogLogic = orderLogLogic;
    }

    /**
     * 分页列表
     */
    @Log(title = "订单日志", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('order:log:page')")
    @GetMapping("getPageList")
    public R<Pager<OrderLogPageVO>> getPageList(Page<OrderLogPageQueryDTO> page, OrderLogPageQueryDTO s) {
        page.setSearch(s);
        return R.succ(orderLogLogic.getPageList(page));
    }
}
