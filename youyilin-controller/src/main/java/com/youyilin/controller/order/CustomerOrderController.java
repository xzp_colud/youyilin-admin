package com.youyilin.controller.order;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.bean.R;
import com.youyilin.common.entity.EnumEntity;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.order.enums.CustomerOrderStatusEnum;
import com.youyilin.order.enums.CommonOutboundStatusEnum;
import com.youyilin.logic.service.order.mini.CustomerMiniOrderLogic;
import com.youyilin.order.entity.CustomerOrder;
import com.youyilin.order.model.request.RequestMiniOrder;
import com.youyilin.order.model.request.RequestMiniOrderAddress;
import com.youyilin.order.model.request.RequestMiniOrderSend;
import com.youyilin.order.model.vo.CustomerOrderVo;
import com.youyilin.order.service.CustomerOrderService;
import com.youyilin.wechat.model.response.JsApiResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 订单
 */
@RestController
@RequestMapping("customer.order")
public class CustomerOrderController {

    private final CustomerOrderService customerOrderService;
    private final CustomerMiniOrderLogic customerMiniOrderLogic;

    public CustomerOrderController(CustomerOrderService customerOrderService, CustomerMiniOrderLogic customerMiniOrderLogic) {
        this.customerOrderService = customerOrderService;
        this.customerMiniOrderLogic = customerMiniOrderLogic;
    }

    /**
     * 列表
     */
    @Log(title = "客户订单", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('customer:order:list')")
    @GetMapping("getPageList")
    public R<Pager<CustomerOrder>> getPageList(Page<CustomerOrder> page, CustomerOrder s) {
        page.setSearch(s);
        Integer totalNum = customerOrderService.getTotal(page);
        List<CustomerOrder> list = customerOrderService.getPage(page);
        return R.succ(new Pager<>(totalNum, list));
    }

    /**
     * 查询
     */
    @Log(title = "客户订单", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('customer:order:query')")
    @GetMapping("/getOrder/{id}")
    public R<CustomerOrder> getOrder(@PathVariable Long id) {
        return R.succ(customerOrderService.getById(id));
    }

    /**
     * 订单枚举
     *
     * @return ArrayList
     */
    @GetMapping("listStatus")
    public R<List<EnumEntity>> listStatus() {
        return R.succ(CustomerOrderStatusEnum.listEnum());
    }

    /**
     * 订单枚举
     *
     * @return ArrayList
     */
    @GetMapping("listOutboundStatus")
    public R<List<EnumEntity>> listOutboundStatus() {
        return R.succ(CommonOutboundStatusEnum.listEnum());
    }

    /**
     * 修改收货地址
     *
     * @param request request
     * @return String
     */
    @Log(title = "客户订单", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('customer:order:address')")
    @PostMapping("updateAddress")
    public R<String> updateAddress(@RequestBody @Valid RequestMiniOrderAddress request) {
        customerMiniOrderLogic.updateAddress(request);
        return R.success("发货成功");
    }

    /**
     * 出库
     *
     * @param orderIds 订单
     */
    @Log(title = "客户订单", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('customer:order:outbound')")
    @PostMapping("outbound")
    public R<String> outbound(@RequestBody List<Long> orderIds) {
        customerMiniOrderLogic.outbound(orderIds);
        return R.success("申请成功");
    }

    /**
     * 发货
     *
     * @param request request
     * @return String
     */
    @Log(title = "客户订单", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('customer:order:send')")
    @PostMapping("send")
    public R<String> send(@RequestBody @Valid RequestMiniOrderSend request) {
        customerMiniOrderLogic.send(request);
        return R.success("发货成功");
    }

    // ----- 小程序 ----- //

    /**
     * 小程序 列表
     *
     * @param current  当前页
     * @param pageSize 每页数量
     * @param status   状态
     * @return ArrayList
     */
    @GetMapping("miniPage")
    public R<List<CustomerOrderVo>> miniAdd(@RequestParam(defaultValue = "1") Integer current, @RequestParam(defaultValue = "10") Integer pageSize, Integer status) {
        return R.succ(customerMiniOrderLogic.getPage(current, pageSize, status));
    }

    /**
     * 小程序 按订单查询
     *
     * @param id 订单
     * @return CustomerOrderVo
     */
    @GetMapping("/miniSelect/{id}")
    public R<CustomerOrderVo> miniSelect(@PathVariable Long id) {
        return R.succ(customerMiniOrderLogic.getOrder(id));
    }

    /**
     * 小程序 订单新增
     *
     * @param request request
     * @return String
     */
    @PostMapping("miniAdd")
    public R<String> miniAdd(@RequestBody @Valid RequestMiniOrder request) {
        return R.succ(customerMiniOrderLogic.saveOrder(request));
    }

    /**
     * 小程序 订单取消
     *
     * @param order request
     * @return String
     */
    @PostMapping("miniCancel")
    public R<String> miniAdd(@RequestBody CustomerOrder order) {
        customerMiniOrderLogic.cancel(order.getId());
        return R.success("操作成功");
    }

    /**
     * 小程序 发起支付
     *
     * @param order request
     * @return String
     */
    @PostMapping("miniPay")
    public R<JsApiResponse> miniPay(@RequestBody CustomerOrder order) {
        return R.succ(customerMiniOrderLogic.pay(order.getId()));
    }

    /**
     * 小程序 支付取消
     *
     * @param order request
     * @return String
     */
    @PostMapping("miniPayCancel")
    public R<String> miniPayCancel(@RequestBody CustomerOrder order) {
        customerMiniOrderLogic.payCancel(order.getId());
        return R.succ("付款取消");
    }

    /**
     * 小程序 订单收货
     *
     * @param order request
     * @return String
     */
    @PostMapping("miniReceive")
    public R<String> miniReceive(@RequestBody CustomerOrder order) {
        customerMiniOrderLogic.receive(order.getId());
        return R.succ("收货成功");
    }
}
