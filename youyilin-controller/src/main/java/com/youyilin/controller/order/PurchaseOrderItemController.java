package com.youyilin.controller.order;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.logic.service.order.purchase.PurchaseOrderItemLogic;
import com.youyilin.order.dto.purchase.PurchaseItemReviseNumDTO;
import com.youyilin.order.vo.purchase.PurchaseOrderItemPageVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 采购订单商品 控制层
 */
@RestController
@RequestMapping("purchase.order.item")
public class PurchaseOrderItemController {

    private final PurchaseOrderItemLogic purchaseOrderItemLogic;

    public PurchaseOrderItemController(PurchaseOrderItemLogic purchaseOrderItemLogic) {
        this.purchaseOrderItemLogic = purchaseOrderItemLogic;
    }

    /**
     * 某采购订单商品明细
     */
    @Log(title = "出库单", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('purchase:item')")
    @GetMapping("/listByOrderId/{orderId}")
    public R<List<PurchaseOrderItemPageVO>> getPageList(@PathVariable Long orderId) {
        return R.succ(purchaseOrderItemLogic.listByPurchaseOrderId(orderId));
    }

    /**
     * 执行修正检验米数
     */
    @Log(title = "出库单", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('purchase:revise')")
    @PostMapping("revise")
    public R<String> revise(@RequestBody @Valid PurchaseItemReviseNumDTO dto) {
        purchaseOrderItemLogic.updateReviseNum(dto);
        return R.success(RMsg.SUCCESS.getMsg());
    }
}
