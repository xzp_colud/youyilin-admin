package com.youyilin.controller.order;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.R;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.logic.service.order.outbound.OutboundOrderItemLogic;
import com.youyilin.order.vo.outbound.OutboundOrderItemPageVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 出库单明细 控制层
 */
@RestController
@RequestMapping("outbound.order.item")
public class OutboundOrderItemController {

    private final OutboundOrderItemLogic outboundOrderItemLogic;

    public OutboundOrderItemController(OutboundOrderItemLogic outboundOrderItemLogic) {
        this.outboundOrderItemLogic = outboundOrderItemLogic;
    }

    /**
     * 某出库单商品明细
     */
    @Log(title = "出库单", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('outbound:item')")
    @GetMapping("/listByOrderId/{outboundId}")
    public R<List<OutboundOrderItemPageVO>> listByOrderId(@PathVariable Long outboundId) {
        return R.succ(outboundOrderItemLogic.listByOutboundId(outboundId));
    }
}
