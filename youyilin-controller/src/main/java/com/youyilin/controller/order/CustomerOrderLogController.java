package com.youyilin.controller.order;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.R;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.order.entity.CustomerOrderLog;
import com.youyilin.order.service.CustomerOrderLogService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 订单-日志
 */
@RestController
@RequestMapping("customer.order.log")
public class CustomerOrderLogController {

    private final CustomerOrderLogService customerOrderLogService;

    public CustomerOrderLogController(CustomerOrderLogService customerOrderLogService) {
        this.customerOrderLogService = customerOrderLogService;
    }

    /**
     * 按订单查询
     *
     * @param orderId 订单ID
     * @return ArrayList
     */
    @Log(title = "客户订单", businessType = BusinessTypeEnum.PAGE)
//    @PreAuthorize("hasAuthority('customer:order:item:list')")
    @GetMapping("/listByOrderId/{orderId}")
    public R<List<CustomerOrderLog>> listByOrderId(@PathVariable Long orderId) {
        return R.succ(customerOrderLogService.listByOrderId(orderId));
    }
}
