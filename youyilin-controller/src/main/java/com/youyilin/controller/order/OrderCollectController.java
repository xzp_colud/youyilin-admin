package com.youyilin.controller.order;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.bean.R;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.logic.service.order.sales.OrderCollectLogic;
import com.youyilin.order.dto.sales.OrderCollectPageQueryDTO;
import com.youyilin.order.vo.sales.OrderCollectPageVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 订单收款记录 控制层
 */
@RestController
@RequestMapping("order.collect")
public class OrderCollectController {

    private final OrderCollectLogic orderCollectLogic;

    public OrderCollectController(OrderCollectLogic orderCollectLogic) {
        this.orderCollectLogic = orderCollectLogic;
    }

    /**
     * 分页列表
     */
    @Log(title = "销售订单", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('order:collect')")
    @GetMapping("getPageList")
    public R<Pager<OrderCollectPageVO>> getPageList(Page<OrderCollectPageQueryDTO> page, OrderCollectPageQueryDTO s) {
        page.setSearch(s);
        return R.succ(orderCollectLogic.getPageList(page));
    }
}
