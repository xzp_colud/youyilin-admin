package com.youyilin.controller.order;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.R;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.logic.service.order.sales.OrderDiscountLogic;
import com.youyilin.order.vo.sales.OrderDiscountPageVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 订单优惠明细 控制层
 */
@RestController
@RequestMapping("order.discount")
public class OrderDiscountController {

    private final OrderDiscountLogic orderDiscountLogic;

    public OrderDiscountController(OrderDiscountLogic orderDiscountLogic) {
        this.orderDiscountLogic = orderDiscountLogic;
    }

    /**
     * 某销售单折扣明细
     */
    @Log(title = "销售订单", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('order:discount')")
    @GetMapping("/listByOrderId/{orderId}")
    public R<List<OrderDiscountPageVO>> getPageList(@PathVariable Long orderId) {
        return R.succ(orderDiscountLogic.listByOrderId(orderId));
    }
}
