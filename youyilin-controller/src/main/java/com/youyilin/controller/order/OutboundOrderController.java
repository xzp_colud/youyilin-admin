package com.youyilin.controller.order;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.logic.service.order.outbound.OutboundOrderLogic;
import com.youyilin.order.dto.outbound.*;
import com.youyilin.order.vo.outbound.OutboundOrderDetailVO;
import com.youyilin.order.vo.outbound.OutboundOrderEditVO;
import com.youyilin.order.vo.outbound.OutboundOrderPageVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 出库单 控制器
 */
@RestController
@RequestMapping("outbound.order")
public class OutboundOrderController {

    private final OutboundOrderLogic outboundOrderLogic;

    public OutboundOrderController(OutboundOrderLogic outboundOrderLogic) {
        this.outboundOrderLogic = outboundOrderLogic;
    }

    /**
     * 列表
     */
    @Log(title = "出库单", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('outbound:page')")
    @GetMapping("getPageList")
    public R<Pager<OutboundOrderPageVO>> getPageList(Page<OutboundOrderPageQueryDTO> page, OutboundOrderPageQueryDTO order) {
        page.setSearch(order);
        return R.succ(outboundOrderLogic.getPageList(page));
    }

    /**
     * 出库单详情查询
     */
    @Log(title = "出库单", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('outbound:query')")
    @GetMapping("/queryDetail/{outboundId}")
    public R<OutboundOrderDetailVO> queryDetail(@PathVariable Long outboundId) {
        return R.succ(outboundOrderLogic.getDetailVOById(outboundId));
    }

    /**
     * 出库单编辑查询
     */
    @Log(title = "出库单", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('outbound:save')")
    @GetMapping("/queryEdit/{outboundId}")
    public R<OutboundOrderEditVO> queryEdit(@PathVariable Long outboundId) {
        return R.succ(outboundOrderLogic.getEditVOById(outboundId));
    }

    /**
     * 执行新增
     */
    @Log(title = "出库单", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('outbound:save')")
    @PostMapping("add")
    public R<String> add(@RequestBody @Valid OutboundOrderFormDTO dto) {
        outboundOrderLogic.saveOutbound(dto);
        return R.success(RMsg.ADD_SUCCESS.getMsg());
    }

    /**
     * 执行确认
     */
    @Log(title = "出库单", businessType = BusinessTypeEnum.OTHER, remark = "确认")
    @PreAuthorize("hasAuthority('outbound:confirm')")
    @PostMapping("confirm")
    public R<String> confirm(@RequestBody @Valid OutboundOrderConfirmFormDTO dto) {
        outboundOrderLogic.updateConfirm(dto);
        return R.success(RMsg.SUCCESS.getMsg());
    }

    /**
     * 执行出库
     */
    @Log(title = "出库单", businessType = BusinessTypeEnum.OTHER, remark = "出库")
    @PreAuthorize("hasAuthority('outbound:out')")
    @PostMapping("inventoryOut")
    public R<String> inventoryOut(@RequestBody @Valid OutboundOrderInventoryFormDTO dto) {
        outboundOrderLogic.updateInventory(dto);
        return R.success(RMsg.SUCCESS.getMsg());
    }

    /**
     * 执行取消
     */
    @Log(title = "出库单", businessType = BusinessTypeEnum.OTHER, remark = "取消")
    @PreAuthorize("hasAuthority('outbound:cancel')")
    @PostMapping("cancel")
    public R<String> cancel(@RequestBody @Valid OutboundOrderCancelFormDTO dto) {
        outboundOrderLogic.updateCancel(dto);
        return R.success(RMsg.SUCCESS.getMsg());
    }
}
