package com.youyilin.controller.order;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.logic.service.order.sales.OrderLogic;
import com.youyilin.order.dto.sales.*;
import com.youyilin.order.enums.OrderTypeEnum;
import com.youyilin.order.vo.sales.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 订单 控制器
 */
@RestController
@RequestMapping("order")
public class OrderController {

    private final OrderLogic orderLogic;

    public OrderController(OrderLogic orderLogic) {
        this.orderLogic = orderLogic;
    }

    /**
     * 分页列表
     */
    @Log(title = "销售订单", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('order:page')")
    @GetMapping("getPageList")
    public R<Pager<OrderPageVO>> getPageList(Page<OrderPageQueryDTO> page, OrderPageQueryDTO s) {
        page.setSearch(s);
        return R.succ(orderLogic.getPageList(page));
    }

    /**
     * 查询销售单详情
     */
    @Log(title = "销售订单", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('order:detail')")
    @GetMapping("/queryDetail/{orderId}")
    public R<OrderDetailVO> queryDetail(@PathVariable Long orderId) {
        return R.succ(orderLogic.getDetailVOById(orderId));
    }

    /**
     * 查询销售单编辑
     */
    @Log(title = "销售订单", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('order:edit')")
    @GetMapping("/queryEdit/{orderId}")
    public R<OrderEditVO> queryEdit(@PathVariable Long orderId) {
        return R.succ(orderLogic.getEditVOById(orderId));
    }

    /**
     * 查询销售单修改地址信息
     */
    @Log(title = "销售订单", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('order:address')")
    @GetMapping("/queryAddress/{orderId}")
    public R<OrderUpdateAddressVO> queryAddress(@PathVariable Long orderId) {
        return R.succ(orderLogic.getUpdateAddressVOById(orderId));
    }

    /**
     * 新增订单(类型为线下)
     */
    @Log(title = "销售订单", businessType = BusinessTypeEnum.INSERT)
    @PreAuthorize("hasAuthority('order:add')")
    @PostMapping("add")
    public R<String> add(@RequestBody @Valid OrderFormDTO dto) {
        dto.setOrderType(OrderTypeEnum.OFFLINE.getCode());
        orderLogic.saveOrder(dto);
        return R.success(RMsg.ADD_SUCCESS.getMsg());
    }

    /**
     * 新增订单(类型为批发单)
     */
    @Log(title = "销售订单", businessType = BusinessTypeEnum.INSERT)
    @PreAuthorize("hasAuthority('order:add')")
    @PostMapping("addWholesale")
    public R<String> addWholesale(@RequestBody @Valid OrderFormDTO dto) {
        dto.setOrderType(OrderTypeEnum.WHOLESALE.getCode());
        orderLogic.saveOrder(dto);
        return R.success(RMsg.ADD_SUCCESS.getMsg());
    }

    /**
     * 提交订单
     */
    @Log(title = "销售订单", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('order:submit')")
    @PostMapping("submit")
    public R<String> submit(@RequestBody @Valid OrderSubmitFormDTO dto) {
        orderLogic.updateSubmit(dto);
        return R.success(RMsg.SUCCESS.getMsg());
    }

    /**
     * 订单付款
     */
    @Log(title = "销售订单", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('order:pay')")
    @PostMapping("pay")
    public R<String> pay(@RequestBody @Valid OrderPayFormDTO dto) {
        orderLogic.updatePay(dto);
        return R.success(RMsg.SUCCESS.getMsg());
    }

    /**
     * 订单出库
     */
    @Log(title = "销售订单", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('order:out')")
    @PostMapping("inventoryOut")
    public R<String> inventoryOut(@RequestBody @Valid OrderInventoryFormDTO dto) {
        orderLogic.updateInventoryOut(dto);
        return R.success(RMsg.SUCCESS.getMsg());
    }

    /**
     * 订单发货
     */
    @Log(title = "销售订单", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('order:send')")
    @PostMapping("send")
    public R<String> send(@RequestBody @Valid OrderSendFormDTO dto) {
        orderLogic.updateSend(dto);
        return R.success(RMsg.SUCCESS.getMsg());
    }

    /**
     * 订单取消
     */
    @Log(title = "销售订单", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('order:cancel')")
    @PostMapping("cancel")
    public R<String> cancel(OrderCancelFormDTO dto) {
        orderLogic.updateCancel(dto);
        return R.succ(RMsg.SUCCESS.getMsg());
    }

    /**
     * 订单收货地址
     */
    @Log(title = "销售订单", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('order:address')")
    @PostMapping("address")
    public R<String> address(@RequestBody @Valid OrderAddressFormDTO dto) {
        orderLogic.updateAddress(dto);
        return R.success(RMsg.SUCCESS.getMsg());
    }

    /**
     * 获取结款列表
     */
    @Log(title = "销售订单", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('order:settle')")
    @GetMapping("listSettledPage")
    public R<List<OrderSettledPageVO>> list() {
        return R.succ(orderLogic.listSettledPage());
    }

    /**
     * 获取原料采购信息
     */
    @Log(title = "销售订单", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('order:purchaseQuery')")
    @GetMapping("getPurchaseInfo")
    public R<List<OrderToPurchaseQueryQtyVO>> getPurchaseInfo(Long id) {
        return R.succ(orderLogic.getPurchaseInfo(id));
    }

    /**
     * 销售单转原料采购
     */
    @Log(title = "销售订单", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('order:purchase')")
    @PostMapping("purchase")
    public R<String> purchase(@RequestBody @Valid OrderPurchaseFormDTO dto) {
        orderLogic.doSalesOrderToPurchaseOrder(dto);
        return R.success(RMsg.SUCCESS.getMsg());
    }

    /**
     * 获取备货采购信息
     */
    @Log(title = "销售订单", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('order:readyQuery')")
    @GetMapping("getReadyPurchaseInfo")
    public R<List<OrderToPurchaseQueryQtyVO>> getReadyPurchaseInfo(Long id) {
        return R.succ(orderLogic.getReadyPurchaseInfo(id));
    }

    /**
     * 销售单转备货采购
     */
    @Log(title = "销售订单", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('order:readyPurchase')")
    @PostMapping("readyPurchase")
    public R<String> readyPurchase(@RequestBody @Valid OrderReadyPurchaseFormDTO dto) {
        orderLogic.updateReadyPurchaseProcessing(dto);
        return R.success(RMsg.SUCCESS.getMsg());
    }
}
