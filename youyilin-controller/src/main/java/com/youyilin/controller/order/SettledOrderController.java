package com.youyilin.controller.order;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.logic.service.order.settled.SettledOrderLogic;
import com.youyilin.order.dto.settled.*;
import com.youyilin.order.vo.settled.SettledOrderDetailVO;
import com.youyilin.order.vo.settled.SettledOrderPageVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 结款单 控制器
 */
@RestController
@RequestMapping("settled.order")
public class SettledOrderController {

    private final SettledOrderLogic settledOrderLogic;

    public SettledOrderController(SettledOrderLogic settledOrderLogic) {
        this.settledOrderLogic = settledOrderLogic;
    }

    /**
     * 列表
     */
    @Log(title = "结款单", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('settled:page')")
    @GetMapping("getPageList")
    public R<Pager<SettledOrderPageVO>> getPageList(Page<SettledOrderPageQueryDTO> page, SettledOrderPageQueryDTO order) {
        page.setSearch(order);
        return R.succ(settledOrderLogic.getPageList(page));
    }

    /**
     * 获取上期结余
     */
    @GetMapping("/getLastBalance/{customerId}")
    public R<String> getLastBalance(@PathVariable Long customerId) {
        return R.succ(settledOrderLogic.getLastBalanceByCustomerId(customerId));
    }

    /**
     * 获取结款单详情
     */
    @GetMapping("/queryDetail/{id}")
    public R<SettledOrderDetailVO> queryDetail(@PathVariable Long id) {
        return R.succ(settledOrderLogic.getDetailVOById(id));
    }

    /**
     * 执行保存
     */
    @Log(title = "结款单", businessType = BusinessTypeEnum.INSERT)
    @PostMapping("save")
    public R<String> save(@RequestBody @Valid SettledOrderFormDTO dto) {
        settledOrderLogic.saveSettled(dto);
        return R.success(RMsg.SUCCESS.getMsg());
    }

    /**
     * 执行确认
     */
    @PostMapping("confirm")
    public R<String> confirm(@RequestBody @Valid SettledOrderConfirmFormDTO dto) {
        settledOrderLogic.updateConfirm(dto);
        return R.success(RMsg.SUCCESS.getMsg());
    }

    /**
     * 执行取消
     */
    @PostMapping("cancel")
    public R<String> cancel(@RequestBody @Valid SettledOrderCancelFormDTO dto) {
        settledOrderLogic.updateCancel(dto);
        return R.success(RMsg.SUCCESS.getMsg());
    }

    /**
     * 执行完成
     */
    @PostMapping("finish")
    public R<String> finish(@RequestBody @Valid SettledOrderFinishFormDTO dto) {
        settledOrderLogic.updateFinish(dto);
        return R.success(RMsg.SUCCESS.getMsg());
    }
}
