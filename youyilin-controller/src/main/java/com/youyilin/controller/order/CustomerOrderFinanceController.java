package com.youyilin.controller.order;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.R;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.order.entity.CustomerOrderFinance;
import com.youyilin.order.service.CustomerOrderFinanceService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 订单-收款记录
 */
@RestController
@RequestMapping("customer.order.finance")
public class CustomerOrderFinanceController {

    private final CustomerOrderFinanceService customerOrderFinanceService;

    public CustomerOrderFinanceController(CustomerOrderFinanceService customerOrderFinanceService) {
        this.customerOrderFinanceService = customerOrderFinanceService;
    }

    /**
     * 按订单查询
     *
     * @param orderId 订单ID
     * @return ArrayList
     */
    @Log(title = "客户订单", businessType = BusinessTypeEnum.PAGE)
//    @PreAuthorize("hasAuthority('customer:order:finance:list')")
    @GetMapping("/listByOrderId/{orderId}")
    public R<List<CustomerOrderFinance>> listByOrderId(@PathVariable Long orderId) {
        return R.succ(customerOrderFinanceService.listByOrderId(orderId));
    }
}
