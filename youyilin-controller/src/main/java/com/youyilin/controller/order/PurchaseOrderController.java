package com.youyilin.controller.order;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.logic.service.order.purchase.PurchaseOrderLogic;
import com.youyilin.order.dto.purchase.*;
import com.youyilin.order.vo.purchase.PurchaseOrderDetailVO;
import com.youyilin.order.vo.purchase.PurchaseOrderEditVO;
import com.youyilin.order.vo.purchase.PurchaseOrderPageVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 采购单 控制器
 */
@RestController
@RequestMapping("purchase.order")
public class PurchaseOrderController {

    private final PurchaseOrderLogic purchaseOrderLogic;

    public PurchaseOrderController(PurchaseOrderLogic purchaseOrderLogic) {
        this.purchaseOrderLogic = purchaseOrderLogic;
    }

    /**
     * 列表
     */
    @Log(title = "采购单", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('purchase:page')")
    @GetMapping("getPageList")
    public R<Pager<PurchaseOrderPageVO>> getPageList(Page<PurchaseOrderPageQueryDTO> page, PurchaseOrderPageQueryDTO order) {
        page.setSearch(order);
        return R.succ(purchaseOrderLogic.getPageList(page));
    }

    /**
     * 采购单详情查询
     */
    @Log(title = "出库单", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('purchase:detail')")
    @GetMapping("/queryDetail/{orderId}")
    public R<PurchaseOrderDetailVO> getOrder(@PathVariable Long orderId) {
        return R.succ(purchaseOrderLogic.getDetailVOById(orderId));
    }

    /**
     * 采购单编辑查询
     */
    @Log(title = "出库单", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('purchase:save')")
    @GetMapping("/queryEdit/{orderId}")
    public R<PurchaseOrderEditVO> getOrderEdit(@PathVariable Long orderId) {
        return R.succ(purchaseOrderLogic.getEditVOById(orderId));
    }

    /**
     * 执行新增
     */
    @Log(title = "采购单", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('purchase:save')")
    @PostMapping("add")
    public R<String> add(@RequestBody @Valid PurchaseOrderFormDTO dto) {
        purchaseOrderLogic.savePurchaseOrder(dto);
        return R.success(RMsg.ADD_SUCCESS.getMsg());
    }

    /**
     * 执行确认
     */
    @Log(title = "采购单", businessType = BusinessTypeEnum.OTHER, remark = "确认")
    @PreAuthorize("hasAuthority('purchase:confirm')")
    @PostMapping("confirm")
    public R<String> confirm(@RequestBody @Valid PurchaseOrderConfirmFormDTO dto) {
        purchaseOrderLogic.updateConfirm(dto);
        return R.success(RMsg.SUCCESS.getMsg());
    }

    /**
     * 执行检验
     */
    @Log(title = "采购单", businessType = BusinessTypeEnum.OTHER, remark = "检验")
    @PreAuthorize("hasAuthority('purchase:check')")
    @PostMapping("check")
    public R<String> check(@RequestBody @Valid PurchaseOrderCheckFormDTO dto) {
        purchaseOrderLogic.updateCheck(dto);
        return R.success(RMsg.SUCCESS.getMsg());
    }

    /**
     * 执行批量入库
     */
    @Log(title = "采购单", businessType = BusinessTypeEnum.OTHER, remark = "批量入库")
    @PreAuthorize("hasAuthority('purchase:in')")
    @PostMapping("inventoryInBatch")
    public R<String> inventoryInBatch(@RequestBody @Valid PurchaseOrderInventoryBatchFormDTO dto) {
        purchaseOrderLogic.updateInventoryBatch(dto);
        return R.success(RMsg.SUCCESS.getMsg());
    }

    /**
     * 执行入库
     */
    @Log(title = "采购单", businessType = BusinessTypeEnum.OTHER, remark = "入库")
    @PreAuthorize("hasAuthority('purchase:in')")
    @PostMapping("inventoryIn")
    public R<String> inventoryIn(@RequestBody @Valid PurchaseOrderInventoryFormDTO dto) {
        purchaseOrderLogic.updateInventory(dto);
        return R.success(RMsg.SUCCESS.getMsg());
    }

    /**
     * 执行取消
     */
    @Log(title = "采购单", businessType = BusinessTypeEnum.OTHER, remark = "取消")
    @PreAuthorize("hasAuthority('purchase:cancel')")
    @PostMapping("cancel")
    public R<String> cancel(@RequestBody @Valid PurchaseOrderCancelFormDTO dto) {
        purchaseOrderLogic.updateCancel(dto);
        return R.success(RMsg.SUCCESS.getMsg());
    }
}
