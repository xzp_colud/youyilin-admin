package com.youyilin.controller.order;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.logic.service.order.refund.OrderRefundLogic;
import com.youyilin.order.dto.refund.OrderRefundCheckDTO;
import com.youyilin.order.dto.refund.OrderRefundPageQueryDTO;
import com.youyilin.order.dto.refund.OrderRefundSaveDTO;
import com.youyilin.order.vo.refund.OrderRefundCheckVO;
import com.youyilin.order.vo.refund.OrderRefundDetailVO;
import com.youyilin.order.vo.refund.OrderRefundPageVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 订单退款 控制层
 */
@RestController
@RequestMapping("order.refund")
public class OrderRefundController {

    private final OrderRefundLogic orderRefundLogic;

    public OrderRefundController(OrderRefundLogic orderRefundLogic) {
        this.orderRefundLogic = orderRefundLogic;
    }

    /**
     * 列表
     */
    @Log(title = "退款单", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('refund:page')")
    @GetMapping("getPageList")
    public R<Pager<OrderRefundPageVO>> getPageList(Page<OrderRefundPageQueryDTO> page, OrderRefundPageQueryDTO s) {
        page.setSearch(s);
        return R.succ(orderRefundLogic.getPageList(page));
    }

    /**
     * 审核查询
     */
    @Log(title = "退款单", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('refund:checkQuery')")
    @GetMapping("/queryCheck/{id}")
    public R<OrderRefundCheckVO> queryCheck(@PathVariable Long id) {
        return R.succ(orderRefundLogic.getCheckVOById(id));
    }

    /**
     * 详情查询
     */
    @Log(title = "退款单", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('refund:deatil')")
    @GetMapping("/queryDetail/{id}")
    public R<OrderRefundDetailVO> queryDetail(@PathVariable Long id) {
        return R.succ(orderRefundLogic.getDetailVOById(id));
    }

    /**
     * 新增
     */
    @Log(title = "退款单", businessType = BusinessTypeEnum.INSERT)
    @PreAuthorize("hasAuthority('refund:save')")
    @PostMapping("add")
    public R<String> add(@RequestBody @Valid OrderRefundSaveDTO dto) {
        orderRefundLogic.addRefund(dto);
        return R.success(RMsg.SUCCESS.getMsg());
    }

    /**
     * 审核
     */
    @Log(title = "退款单", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('refund:check')")
    @PostMapping("check")
    public R<String> check(@RequestBody @Valid OrderRefundCheckDTO dto) {
        orderRefundLogic.updateCheck(dto);
        return R.success(RMsg.SUCCESS.getMsg());
    }
}
