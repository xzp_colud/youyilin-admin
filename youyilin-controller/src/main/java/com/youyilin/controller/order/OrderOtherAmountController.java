package com.youyilin.controller.order;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.R;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.logic.service.order.sales.OrderOtherAmountLogic;
import com.youyilin.order.vo.sales.OrderOtherAmountPageVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 订单其他费用 控制层
 */
@RestController
@RequestMapping("order.other.amount")
public class OrderOtherAmountController {

    private final OrderOtherAmountLogic orderOtherAmountLogic;

    public OrderOtherAmountController(OrderOtherAmountLogic orderOtherAmountLogic) {
        this.orderOtherAmountLogic = orderOtherAmountLogic;
    }

    /**
     * 某销售单其他费用明细
     */
    @Log(title = "销售订单", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('order:otherAmount')")
    @GetMapping("/listByOrderId/{orderId}")
    public R<List<OrderOtherAmountPageVO>> getPageList(@PathVariable Long orderId) {
        return R.succ(orderOtherAmountLogic.listByOrderId(orderId));
    }
}
