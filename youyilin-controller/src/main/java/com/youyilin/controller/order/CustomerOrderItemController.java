package com.youyilin.controller.order;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.R;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.order.entity.CustomerOrderItem;
import com.youyilin.order.service.CustomerOrderItemService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 订单-产品
 */
@RestController
@RequestMapping("customer.order.item")
public class CustomerOrderItemController {

    private final CustomerOrderItemService customerOrderItemService;

    public CustomerOrderItemController(CustomerOrderItemService customerOrderItemService) {
        this.customerOrderItemService = customerOrderItemService;
    }

    /**
     * 按订单查询产品
     *
     * @param orderId 订单
     * @return ArrayList
     */
    @Log(title = "客户订单", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('customer:order:item:list')")
    @GetMapping("/listByOrderId/{orderId}")
    public R<List<CustomerOrderItem>> listByOrderId(@PathVariable Long orderId) {
        return R.succ(customerOrderItemService.listByOrderId(orderId));
    }
}
