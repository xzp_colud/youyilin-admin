package com.youyilin.controller.framework;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.R;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.common.redis.RedisService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * 缓存管理
 */
@RestController
@RequestMapping("monitor.cache")
public class CacheController {

    private final RedisService redisService;

    public CacheController(RedisService redisService) {
        this.redisService = redisService;
    }

    /**
     * 根据Redis Key 前缀获取key
     */
    @Log(title = "缓存列表", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('monitor:cache:listKeysByPrefix')")
    @GetMapping("/listKeysByPrefix/{prefix}")
    public R<List<String>> listKeysByPrefix(@PathVariable String prefix) {
        if (StringUtils.isBlank(prefix)) {
            return R.succ(new ArrayList<>());
        }
        String keys = prefix + ":*";
        Set<String> likesKeysSet = redisService.likes(keys);
        if (CollectionUtils.isEmpty(likesKeysSet)) {
            return R.succ(new ArrayList<>());
        }
        List<String> keysList = new ArrayList<>();
        for (String s : likesKeysSet) {
            keysList.add(s.replace(prefix + ":", ""));
        }
        return R.succ(keysList);
    }

    /**
     * 获取Object
     */
    @Log(title = "缓存列表", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('monitor:cache:getObjectByKey')")
    @PostMapping("/getObjectByKey")
    public R<String> getObjectByKey(String prefix, String key) {
        if (StringUtils.isBlank(prefix) || StringUtils.isBlank(key)) {
            return R.succ(null);
        }
        String redisKey = prefix + ":" + key;
        if (redisService.hasKey(redisKey)) {
            return R.succ(redisService.get(redisKey));
        }
        return R.succ(null);
    }

    /**
     * 删除Object
     */
    @Log(title = "缓存列表", businessType = BusinessTypeEnum.DELETE)
    @PreAuthorize("hasAuthority('monitor:cache:delObjectByKey')")
    @PostMapping("/delObjectByKey")
    public R<String> delObjectByKey(String prefix, String key) {
        if (StringUtils.isBlank(prefix) || StringUtils.isBlank(key)) {
            return R.success();
        }
        String redisKey = prefix + ":" + key;
        if (redisService.hasKey(redisKey)) {
            redisService.delete(redisKey);
            return R.success();
        }
        return R.success();
    }
}
