package com.youyilin.controller.framework;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.R;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.common.redis.RedisPrefix;
import com.youyilin.common.redis.RedisService;
import com.youyilin.system.vo.user.OnlineUserVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * 在线用户
 */
@RestController
@RequestMapping("monitor.online")
public class OnlineController {

    private final RedisService redisService;

    public OnlineController(RedisService redisService) {
        this.redisService = redisService;
    }

    /**
     * 列表
     */
    @Log(title = "在线用户", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('monitor:online:list')")
    @GetMapping("list")
    public R<List<OnlineUserVO>> list() {
        String keys = RedisPrefix.TOKEN_COMPUTER_KEY_PREFIX + "*";
        Set<String> keysSet = redisService.likes(keys);
        String mKeys = RedisPrefix.TOKEN_MOBILE_KEY_PREFIX + "*";
        Set<String> mKeysSet = redisService.likes(mKeys);
        if (keysSet.size() == 0 && mKeysSet.size() == 0) {
            return R.succ(new ArrayList<>());
        }
        keysSet.addAll(mKeysSet);
        List<OnlineUserVO> online = new ArrayList<>();
        for (String key : keysSet) {
            OnlineUserVO admin = redisService.getObject(key, OnlineUserVO.class);
            online.add(admin);
        }
        return R.succ(online);
    }

    /**
     * 强退用户
     */
    @Log(title = "在线用户", businessType = BusinessTypeEnum.DELETE)
    @PreAuthorize("hasAuthority('monitor:online:logout')")
    @PostMapping("/logout/{createTokenId}")
    public R<String> logout(@PathVariable String createTokenId) {
        String pcKey = RedisPrefix.TOKEN_COMPUTER_KEY_PREFIX + createTokenId;
        String mobileKey = RedisPrefix.TOKEN_MOBILE_KEY_PREFIX + createTokenId;
        redisService.delete(pcKey);
        redisService.delete(mobileKey);

        return R.success();
    }
}
