package com.youyilin.controller.framework;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.R;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.framework.server.Server;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("monitor.server")
public class ServerController {

    @Log(title = "服务器", businessType = BusinessTypeEnum.SELECT)
//    @PreAuthorize("hasAuthority('monitor:server:list')")
    @GetMapping("list")
    public R<Server> list() throws Exception {
        Server server = new Server();
        server.copyTo();
        return R.succ(server);
    }
}
