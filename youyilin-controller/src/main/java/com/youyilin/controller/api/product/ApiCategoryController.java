package com.youyilin.controller.api.product;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.R;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.goods_api.logic.ApiCategoryLogic;
import com.youyilin.goods_api.vo.ApiCategoryProductVO;
import com.youyilin.goods_api.vo.ApiCategoryVO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 开放类目 前端控制器
 */
@RestController
@RequestMapping("/api/goods.category")
public class ApiCategoryController {

    private final ApiCategoryLogic apiCategoryLogic;

    public ApiCategoryController(ApiCategoryLogic apiCategoryLogic) {
        this.apiCategoryLogic = apiCategoryLogic;
    }

    /**
     * 所有类目
     */
    @Log(title = "类目", businessType = BusinessTypeEnum.SELECT)
    @GetMapping("all")
    public R<List<ApiCategoryVO>> all() {
        return R.succ(apiCategoryLogic.listCategory());
    }

    /**
     * 页面查询 所有类目(带商品数量)
     */
    @GetMapping("search")
    public R<List<ApiCategoryProductVO>> search() {
        return R.succ(apiCategoryLogic.listCategoryProduct());
    }
}
