package com.youyilin.controller.api.product;

import com.youyilin.common.bean.R;
import com.youyilin.goods_api.logic.ApiProductLogic;
import com.youyilin.goods_api.vo.product.ApiProductDetail;
import com.youyilin.goods_api.vo.product.ApiProductDetailVO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 开放商品 前端控制器
 */
@RestController
@RequestMapping("/api/product")
public class ApiProductController {

    private final ApiProductLogic apiProductLogic;

    public ApiProductController(ApiProductLogic apiProductLogic) {
        this.apiProductLogic = apiProductLogic;
    }

    /**
     * 商品查询
     */
    @GetMapping("page")
    public R<List<ApiProductDetail>> page(Integer current, Integer pageSize, Long frontMenuId, String name) {
        return R.succ(apiProductLogic.page(current, pageSize, frontMenuId, name));
    }

    /**
     * 商品查询
     */
    @GetMapping("/getProduct/{id}")
    public R<ApiProductDetailVO> getSearchById(@PathVariable Long id) {
        return R.succ(apiProductLogic.getProduct(id));
    }
}
