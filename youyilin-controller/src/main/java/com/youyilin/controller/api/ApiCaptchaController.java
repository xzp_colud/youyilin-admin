package com.youyilin.controller.api;

import com.youyilin.common.bean.R;
import com.youyilin.common.redis.RedisPrefix;
import com.youyilin.common.redis.RedisService;
import com.youyilin.common.utils.uuid.IdUtils;
import com.google.code.kaptcha.Producer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.FastByteArrayOutputStream;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 图片验证码
 */
@Slf4j
@RestController
public class ApiCaptchaController {

    private final RedisService redisService;
    private final Producer captchaProducer;

    public ApiCaptchaController(RedisService redisService, Producer captchaProducer) {
        this.redisService = redisService;
        this.captchaProducer = captchaProducer;
    }

    /**
     * 生成验证码
     */
    @PostMapping("/api/captchaImage")
    public R<Map<String, String>> getCode() {
        // 保存验证码信息
        String uuid = IdUtils.simpleUUID();

        // 生成验证码
        String capStr = captchaProducer.createText();
        BufferedImage image = captchaProducer.createImage(capStr);

        // 转换流信息写出
        FastByteArrayOutputStream os = new FastByteArrayOutputStream();
        try {
            ImageIO.write(image, "jpg", os);
        } catch (IOException e) {
            return R.fail(e.getMessage());
        }
        redisService.add(RedisPrefix.CAPTCHA_KEY_PREFIX + uuid, capStr, 5, TimeUnit.MINUTES);

        Map<String, String> map = new HashMap<>();
        map.put("captcha", capStr);
        map.put("uuid", uuid);
        map.put("img", Base64.getEncoder().encodeToString(os.toByteArray()));

        return R.succ(map);
    }
}
