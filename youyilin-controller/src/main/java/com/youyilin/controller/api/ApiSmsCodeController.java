package com.youyilin.controller.api;

import com.youyilin.common.bean.R;
import com.youyilin.common.enums.StatusEnum;
import com.youyilin.common.redis.RedisPrefix;
import com.youyilin.common.redis.RedisService;
import com.youyilin.common.utils.MobileUtil;
import com.youyilin.system.entity.SysUser;
import com.youyilin.system.service.SysUserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * 短信验证码
 */
@Slf4j
@RestController
public class ApiSmsCodeController {

    @Autowired
    RedisService redisService;
    @Autowired
    private SysUserService sysUserService;
//    @Autowired
//    private SmsService smsService;

    /**
     * 生成短信验证码
     */
    @PostMapping("/api/smsCode")
    public R getAdminCode(String phone) {
        if (StringUtils.isBlank(phone)) {
            return R.fail("请输入手机号");
        }
        if (MobileUtil.isNotMobile(phone)) {
            return R.fail("请输入正确手机号");
        }
        SysUser sysUser = sysUserService.getByPhone(phone);
        if (sysUser == null
                || sysUser.getStatus() == null
                || sysUser.getStatus() != StatusEnum.NORMAL.getCode()) {
            return R.success("发送成功");
        }
        String code = createCode();
        log.info("【短信验证码为】 {}", code);
        redisService.add(RedisPrefix.SMS_KEY_PREFIX + phone, code, 5, TimeUnit.MINUTES);
        return R.succ(code);
    }

    // 生成4为验证码
    private String createCode() {
        StringBuilder code = new StringBuilder();
        for (int i = 1; i < 5; i++) {
            int codeNum = (int) (Math.random() * 9 + 1);
            code.append(codeNum);
        }
        return code.toString();
    }
}
