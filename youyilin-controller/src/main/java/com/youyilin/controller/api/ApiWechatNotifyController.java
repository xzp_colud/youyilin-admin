package com.youyilin.controller.api;

import com.alibaba.fastjson.JSONObject;
import com.wechat.pay.java.core.notification.Notification;
import com.wechat.pay.java.core.notification.NotificationParser;
import com.wechat.pay.java.core.notification.RSANotificationConfig;
import com.wechat.pay.java.core.notification.RequestParam;
import com.wechat.pay.java.core.util.GsonUtil;
import com.wechat.pay.java.service.payments.model.Transaction;
import com.youyilin.common.exception.ApiException;
import com.youyilin.logic.service.order.mini.CustomerMiniOrderLogic;
import com.youyilin.order.utils.NoUtils;
import com.youyilin.wechat.enums.WechatPaymentStatusEnum;
import com.youyilin.wechat.model.entity.WechatConfigMch;
import com.youyilin.wechat.model.entity.WechatPayment;
import com.youyilin.wechat.service.WechatConfigMchService;
import com.youyilin.wechat.service.WechatPaymentService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@Slf4j
@RestController
@RequestMapping("/api/wechatNotify")
public class ApiWechatNotifyController {

    // 支付成功
    private final static String EVENT_TYPE_PAY_SUCCESS = "TRANSACTION.SUCCESS";
    // 支付成功
    private final static String RESOURCE_TYPE_PAY_SUCCESS = "encrypt-resource";

    private final WechatConfigMchService wechatConfigMchService;
    private final WechatPaymentService wechatPaymentService;
    private final CustomerMiniOrderLogic customerMiniOrderLogic;

    public ApiWechatNotifyController(WechatConfigMchService wechatConfigMchService, WechatPaymentService wechatPaymentService, CustomerMiniOrderLogic customerMiniOrderLogic) {
        this.wechatConfigMchService = wechatConfigMchService;
        this.wechatPaymentService = wechatPaymentService;
        this.customerMiniOrderLogic = customerMiniOrderLogic;
    }

    /**
     * 微信支付通知
     */
    @RequestMapping(value = "/paySuccess/{paymentId}", method = RequestMethod.POST)
    public void payNotify(HttpServletRequest request, HttpServletResponse response, @PathVariable Long paymentId, @RequestBody String requestBody) throws IOException {
        try {
            String signature = request.getHeader("Wechatpay-Signature");
            String nonce = request.getHeader("Wechatpay-Nonce");
            String timestamp = request.getHeader("Wechatpay-Timestamp");
            String serial = request.getHeader("Wechatpay-Serial");
            String signatureType = request.getHeader("Wechatpay-Signature-Type");
            log.info("【微信支付通知】 【参数】 signature：{}, nonce: {}, timestamp: {}, serial: {}, signatureType: {}, body: {}", signatureType, nonce, timestamp, serial, signatureType, requestBody);
            this.payNotify(signature, nonce, timestamp, serial, signatureType, paymentId, requestBody);
        } catch (Exception e) {
            log.error("【微信支付通知】", e);
        }
        response.setStatus(200);
        PrintWriter pw = response.getWriter();
        pw.print("SUCCESS");
        pw.close();
    }


    private void payNotify(String signature, String nonce, String timestamp, String serial, String signatureType, Long paymentId, String requestBody) {
        RequestParam requestParam = new RequestParam.Builder()
                .serialNumber(serial)
                .nonce(nonce)
                .signature(signature)
                .timestamp(timestamp)
                .signType(signatureType)
                .body(requestBody)
                .build();

        Notification notification = GsonUtil.getGson().fromJson(requestBody, Notification.class);

        this.validateNotify(notification);

        // 支付成功
        if (isPaySuccess(notification)) {
            try {
                this.paySuccess(requestParam, paymentId);
            } catch (Exception e) {
                log.error("参数异常", e);
                throw new ApiException("参数异常");
            }
        }
    }

    /**
     * 验证通知类型
     */
    private void validateNotify(Notification notification) {
        if (notification == null) {
            throw new ApiException("通知参数异常");
        }
        String eventType = notification.getEventType();
        String resourceType = notification.getResourceType();
        // 通知类型异常
        boolean isNotifyTypeError = StringUtils.isBlank(eventType) || StringUtils.isBlank(resourceType);
        if (isNotifyTypeError) {
            throw new ApiException("通知类型异常");
        }
    }

    /**
     * 是否为支付成功通知
     */
    private boolean isPaySuccess(Notification notification) {
        String eventType = notification.getEventType();
        String resourceType = notification.getResourceType();

        return StringUtils.isNotBlank(eventType) && eventType.equals(EVENT_TYPE_PAY_SUCCESS) && StringUtils.isNotBlank(resourceType) && resourceType.equals(RESOURCE_TYPE_PAY_SUCCESS);
    }

    private synchronized void paySuccess(RequestParam requestParam, Long paymentId) {
        WechatPayment appPayment = wechatPaymentService.getById(paymentId);
        if (appPayment == null) {
            return;
        }

        WechatConfigMch configMch = wechatConfigMchService.getPayConfig(appPayment.getAppid());
        RSANotificationConfig config = new RSANotificationConfig.Builder()
                .certificates(configMch.getWechatPayPath())
                .apiV3Key(configMch.getApiKey())
                .build();

        Transaction transaction = new NotificationParser(config).parse(requestParam, Transaction.class);
        log.info("【微信解密】 {}", JSONObject.toJSONString(transaction));
        // 只有是成功的才进行业务处理
        if (this.validateTradeState(transaction.getTradeState())) {
            this.validateTransactionId(transaction.getTransactionId());

            WechatPayment payment = this.validateOutTradeNo(transaction.getOutTradeNo());
            // 单号没有的不处理 已处理过的不进行处理
            if (payment == null || this.validateHandleFlag(payment)) {
                return;
            }
            // 不一致
            if (!payment.getId().equals(paymentId)) {
                return;
            }

            this.validateAppidAndMachid(payment, transaction.getAppid(), transaction.getMchid());
            this.validateTradeType(payment, transaction.getTradeType());
            this.validateAmount(payment, transaction.getAmount().getTotal());

            // 处理的业务类型
            String sourceNo = payment.getSourceNo();
            if (sourceNo.startsWith(NoUtils.CUSTOMER_ORDER_PREFIX)) {
                customerMiniOrderLogic.paySuccess(payment.getId(), transaction.getTransactionId(), transaction.getSuccessTime(), "微信支付", payment.getOutTradeNo());
            }
        }
    }

    /**
     * 付款成功验证
     */
    private boolean validateTradeState(Transaction.TradeStateEnum tradeState) {
        return tradeState != null && tradeState.name().equals("SUCCESS");
    }

    /**
     * 验证单号
     */
    private WechatPayment validateOutTradeNo(String outTradeNo) {
        if (StringUtils.isBlank(outTradeNo)) {
            throw new ApiException("商户单号不能为空");
        }
        return wechatPaymentService.getByOutTradeNo(outTradeNo);
    }

    /**
     * 验证应用ID和商户ID 是否和支付时的单号一致
     */
    private void validateAppidAndMachid(WechatPayment payment, String appid, String machid) {
        if (StringUtils.isBlank(appid)) {
            throw new ApiException("应用ID不能为空");
        }
        if (StringUtils.isBlank(machid)) {
            throw new ApiException("商户不能为空");
        }
        if (!payment.getAppid().equals(appid)) {
            throw new ApiException("应用不一致");
        }
        if (!payment.getMchid().equals(machid)) {
            throw new ApiException("商户不一致");
        }
    }

    /**
     * 验证微信生成的流水号
     */
    private void validateTransactionId(String transactionId) {
        if (StringUtils.isBlank(transactionId)) {
            throw new ApiException("交易流水号不能为空");
        }
    }

    /**
     * 验证交易类型是否一致
     */
    private void validateTradeType(WechatPayment payment, Transaction.TradeTypeEnum tradeType) {
        if (tradeType == null) {
            throw new ApiException("交易类型不能为空");
        }
        if (!payment.getTradeType().equals(tradeType.name())) {
            throw new ApiException("交易类型不一致");
        }
    }

    /**
     * 验证金额是否一致
     */
    private void validateAmount(WechatPayment payment, Integer amount) {
        if (amount == null) {
            throw new ApiException("金额不能为空");
        }
        if (!payment.getAmount().equals(amount.longValue())) {
            throw new ApiException("交易金额不一致");
        }
    }

    /**
     * 是否已经处理过
     */
    private boolean validateHandleFlag(WechatPayment payment) {
        return payment.getStatus() != null && payment.getStatus().equals(WechatPaymentStatusEnum.PROCESSED.getCode());
    }
}
