package com.youyilin.controller.api;

import com.youyilin.report.model.ExportSysMenu;
import com.youyilin.report.utils.ExcelUtil;
import com.youyilin.system.enums.MenuTypeEnum;
import io.github.yedaxia.apidocs.Docs;
import io.github.yedaxia.apidocs.DocsConfig;
import org.springframework.aop.support.AopUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Api 文档生成
 */
@RestController
@RequestMapping("/api/doc")
public class ApiDocController {

    private final ApplicationContext applicationContext;

    public ApiDocController(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @GetMapping("controller")
    public void getController(HttpServletResponse response) throws IOException {
        List<ExportSysMenu> menuList = new ArrayList<>();
        Map<String, Object> a = applicationContext.getBeansWithAnnotation(RestController.class);
        for (String b : a.keySet()) {
            Object value = a.get(b);
            Class<?> allClass = AopUtils.getTargetClass(value);
            Method[] methods = allClass.getDeclaredMethods();
            int index = 1;
            for (Method method : methods) {
                PreAuthorize preAuthorize = method.getAnnotation(PreAuthorize.class);
                if (preAuthorize != null) {
                    String c = preAuthorize.value();
                    String d = c.replace("hasAuthority('", "").replace("')", "");
                    ExportSysMenu menu = new ExportSysMenu();
                    menu.setMenuName(d);
                    menu.setMenuCode(d);
                    menu.setPerms(d);
                    menu.setType(MenuTypeEnum.BUTTON.getCode());
                    menu.setStatus(1);
                    menu.setLink(0);
                    menu.setSort(index++);

                    menuList.add(menu);
                }
            }
        }
        ExcelUtil.browserWriteExcel(response, menuList, new ExportSysMenu(), "菜单权限", "权限", 0);
    }

    public static void main(String[] args) {
        String userDir = System.getProperty("user.dir");
        String staticDocPath = "/youyilin-admin/src/main/resources/static";
        staticDocPath = userDir + staticDocPath;
        System.out.println(userDir);
        DocsConfig config = new DocsConfig();
        config.setProjectPath(userDir); // 项目根目录
        config.setProjectName("youyilin-api"); // 项目名称
        config.setApiVersion("apidoc");       // 声明该API的版本
        config.setDocsPath(staticDocPath); // 生成API 文档所在目录
        config.setAutoGenerate(Boolean.TRUE);  // 配置自动生成
        Docs.buildHtmlDocs(config); // 执行生成文档
    }
}
