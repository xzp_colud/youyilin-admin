package com.youyilin.controller.api.website;

import com.youyilin.common.bean.R;
import com.youyilin.goods.entity.WebsiteImage;
import com.youyilin.goods.service.WebsiteImageService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/website.image")
public class ApiWebsiteImageController {

    private final WebsiteImageService websiteImageService;

    public ApiWebsiteImageController(WebsiteImageService websiteImageService) {
        this.websiteImageService = websiteImageService;
    }

    @GetMapping("list")
    public R<List<WebsiteImage>> list() {
        return R.succ(websiteImageService.listAll());
    }
}
