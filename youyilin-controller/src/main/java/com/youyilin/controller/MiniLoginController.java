package com.youyilin.controller;

import com.youyilin.common.bean.R;
import com.youyilin.common.entity.LoginAdminEntity;
import com.youyilin.common.utils.SecurityUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MiniLoginController {

    @GetMapping("/api/isMiniLogin")
    public R<String> isLogin() {
        try {
            LoginAdminEntity admin = SecurityUtil.getLoginUser();
            return R.succ(admin.getOpenid());
        } catch (Exception e) {
            return R.fail();
        }
    }
}
