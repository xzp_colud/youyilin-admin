package com.youyilin.controller.quartz;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.common.exception.TaskException;
import com.youyilin.quartz.constant.Constants;
import com.youyilin.quartz.model.entity.SysJob;
import com.youyilin.quartz.service.SysJobService;
import com.youyilin.quartz.util.CronUtils;
import org.apache.commons.lang3.StringUtils;
import org.quartz.SchedulerException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 定时任务
 */
@RestController
@RequestMapping("quartz.job")
public class SysJobController {

    private final SysJobService jobService;

    public SysJobController(SysJobService jobService) {
        this.jobService = jobService;
    }

    /**
     * 列表
     */
    @Log(title = "定时任务", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('quartz:job:page')")
    @GetMapping(value = "getPageList")
    public R<Pager<SysJob>> list(Page<SysJob> page, SysJob s) {
        page.setSearch(s);
        Integer totalNum = jobService.getTotal(page);
        List<SysJob> pageList = jobService.getPage(page);
        return R.succ(new Pager<>(totalNum, pageList));
    }

    /**
     * ID
     */
    @Log(title = "定时任务", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('quartz:job:id')")
    @GetMapping(value = "{id}")
    public R<SysJob> getById(@PathVariable String id) {
        return R.succ(jobService.getById(id));
    }

    /**
     * 新增
     */
    @Log(title = "定时任务", businessType = BusinessTypeEnum.INSERT)
    @PreAuthorize("hasAuthority('quartz:job:add')")
    @PostMapping(value = "add")
    public R<String> doAdd(@RequestBody SysJob job) throws SchedulerException, TaskException {
        if (!CronUtils.isValid(job.getCron())) {
            return R.fail("新增任务'" + job.getJobName() + "'失败，Cron表达式不正确");
        } else if (StringUtils.containsIgnoreCase(job.getInvokeTarget(), Constants.LOOKUP_RMI)) {
            return R.fail("新增任务'" + job.getJobName() + "'失败，目标字符串不允许'rmi://'调用");
        } else if (StringUtils.containsIgnoreCase(job.getInvokeTarget(), Constants.LOOKUP_LDAP)) {
            return R.fail("新增任务'" + job.getJobName() + "'失败，目标字符串不允许'ldap://'调用");
        } else if (StringUtils.containsIgnoreCase(job.getInvokeTarget(), Constants.HTTP)) {
            return R.fail("新增任务'" + job.getJobName() + "'失败，目标字符串不允许'http(s)//'调用");
        } else if (StringUtils.containsIgnoreCase(job.getInvokeTarget(), Constants.HTTPS)) {
            return R.fail("新增任务'" + job.getJobName() + "'失败，目标字符串不允许'http(s)//'调用");
        }
        jobService.saveJob(job);
        return R.success(RMsg.ADD_SUCCESS.getMsg());
    }

    /**
     * 编辑
     */
    @Log(title = "定时任务", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('quartz:job:edit')")
    @PostMapping(value = "edit")
    public R<String> doEdit(@RequestBody SysJob job) throws SchedulerException, TaskException {
        if (!CronUtils.isValid(job.getCron())) {
            return R.fail("修改任务'" + job.getJobName() + "'失败，Cron表达式不正确");
        } else if (StringUtils.containsIgnoreCase(job.getInvokeTarget(), Constants.LOOKUP_RMI)) {
            return R.fail("修改任务'" + job.getJobName() + "'失败，目标字符串不允许'rmi://'调用");
        } else if (StringUtils.containsIgnoreCase(job.getInvokeTarget(), Constants.LOOKUP_LDAP)) {
            return R.fail("修改任务'" + job.getJobName() + "'失败，目标字符串不允许'ldap://'调用");
        } else if (StringUtils.containsIgnoreCase(job.getInvokeTarget(), Constants.HTTP)) {
            return R.fail("新增任务'" + job.getJobName() + "'失败，目标字符串不允许'http(s)//'调用");
        } else if (StringUtils.containsIgnoreCase(job.getInvokeTarget(), Constants.HTTPS)) {
            return R.fail("新增任务'" + job.getJobName() + "'失败，目标字符串不允许'http(s)//'调用");
        }
        jobService.saveJob(job);
        return R.success(RMsg.EDIT_SUCCESS.getMsg());
    }

//    /**
//     * 状态修改
//     */
//    @Log(title = "定时任务", businessType = BusinessTypeEnum.STATUS)
//    @PreAuthorize("hasAuthority('quartz:job:status')")
//    @PostMapping(value = "status")
//    public R status(@RequestBody SysJobEntity en) throws SchedulerException, TaskException {
//        jobService.updateStatus(en.getId(), en.getStatus());
//        return R.success(RMsg.SUCCESS.getMsg());
//    }

    /**
     * 删除
     */
    @Log(title = "定时任务", businessType = BusinessTypeEnum.DELETE)
    @PreAuthorize("hasAuthority('quartz:job:del')")
    @PostMapping(value = "del")
    public R<String> doDel(@RequestBody SysJob en) throws SchedulerException {
        jobService.removeJobByJobName(en.getJobName());
        return R.success(RMsg.DELETE_SUCCESS.getMsg());
    }

    /**
     * 立即执行一次
     */
    @Log(title = "定时任务", businessType = BusinessTypeEnum.UPDATE, remark = "执行一次")
    @PreAuthorize("hasAuthority('quartz:job:run')")
    @PostMapping(value = "run")
    public R<String> doRun(@RequestBody SysJob en) throws SchedulerException {
        jobService.updateSysJobRun(en.getId());
        return R.success(RMsg.SUCCESS.getMsg());
    }
}