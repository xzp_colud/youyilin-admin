package com.youyilin.controller.quartz;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.bean.R;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.quartz.model.entity.SysJobLog;
import com.youyilin.quartz.service.SysJobLogService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 定时任务日志
 */
@RestController
@RequestMapping("quartz.job.log")
public class SysJobLogController {

    private final SysJobLogService jobLogService;

    public SysJobLogController(SysJobLogService jobLogService) {
        this.jobLogService = jobLogService;
    }

    /**
     * 列表
     */
    @Log(title = "定时任务日志", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('quartz:jobLog:page')")
    @GetMapping(value = "getPageList")
    public R<Pager<SysJobLog>> list(Page<SysJobLog> page, SysJobLog s) {
        page.setSearch(s);
        Integer totalNum = jobLogService.getTotal(page);
        List<SysJobLog> pageList = jobLogService.getPage(page);
        return R.succ(new Pager<>(totalNum, pageList));
    }

    /**
     * 清空日志
     */
    @Log(title = "定时任务日志", businessType = BusinessTypeEnum.DELETE)
    @PreAuthorize("hasAuthority('quartz:jobLog:clean')")
    @PostMapping("/clean/{jobId}")
    public R<String> clean(@PathVariable Long jobId) {
        jobLogService.clear(jobId);
        return R.success();
    }
}
