package com.youyilin.controller.goods;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.goods.entity.UnitData;
import com.youyilin.goods.service.UnitDataService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 规格键值 前端控制器
 */
@RestController
@RequestMapping("goods.unit.data")
public class UnitDataController {

    private final UnitDataService unitDataService;

    public UnitDataController(UnitDataService unitDataService) {
        this.unitDataService = unitDataService;
    }

    /**
     * 列表
     */
    @Log(title = "规格键值", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('goods:unit:data:page')")
    @GetMapping("getPageList")
    public R<Pager<UnitData>> getPageList(Page<UnitData> page, UnitData s) {
        page.setSearch(s);
        Integer totalNum = unitDataService.getTotal(page);
        List<UnitData> pageList = unitDataService.getPage(page);
        return R.succ(new Pager<>(totalNum, pageList));
    }

    /**
     * ID
     */
    @GetMapping("{id}")
    public R<UnitData> getById(@PathVariable String id) {
        return R.succ(unitDataService.getById(id));
    }

    /**
     * 新增
     */
    @Log(title = "规格键值", businessType = BusinessTypeEnum.INSERT)
    @PreAuthorize("hasAuthority('goods:unit:data:save')")
    @PostMapping("add")
    public R<String> add(@RequestBody @Valid UnitData en) {
        unitDataService.saveUnitData(en);
        return R.success(RMsg.ADD_SUCCESS.getMsg());
    }

    /**
     * 通过产品界面新增
     */
    @Log(title = "规格键值", businessType = BusinessTypeEnum.INSERT)
    @PostMapping("addByProduct")
    public R<String> addByProduct(@RequestBody UnitData en) {
        unitDataService.saveUnitDataByProduct(en);
        return R.succ(en.getId().toString());
    }

    /**
     * 编辑
     */
    @Log(title = "规格键值", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('goods:unit:data:save')")
    @PostMapping("edit")
    public R<String> edit(@RequestBody @Valid UnitData en) {
        unitDataService.saveUnitData(en);
        return R.success(RMsg.EDIT_SUCCESS.getMsg());
    }

    /**
     * 某销售单位下所有键值
     */
    @GetMapping("/listByUnitId/{unitId}")
    public R<List<UnitData>> listByUnitId(@PathVariable Long unitId) {
        return R.succ(unitDataService.listByUnitId(unitId));
    }

    /**
     * 删除随机出来的键值
     */
    @PostMapping("delByProductId")
    public R<String> delByProductId(@RequestBody UnitData en) {
        unitDataService.delByProductId(en.getProductId());
        return R.succ("");
    }
}
