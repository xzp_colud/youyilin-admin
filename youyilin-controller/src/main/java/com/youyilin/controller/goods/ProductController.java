package com.youyilin.controller.goods;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.goods.dto.product.ProductPageQueryDTO;
import com.youyilin.goods.entity.Product;
import com.youyilin.logic.service.product.ProductLogic;
import com.youyilin.goods.model.request.RequestProduct;
import com.youyilin.goods.service.ProductService;
import com.youyilin.goods.vo.product.ProductAddVO;
import com.youyilin.goods.vo.product.ProductDetailVO;
import com.youyilin.goods.vo.product.ProductEditVO;
import com.youyilin.goods.vo.product.ProductPageVO;
import com.youyilin.logic.service.product.ProductNewLogic;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 商品 前端控制器
 */
@RestController
@RequestMapping("product")
public class ProductController {

    private final ProductService productService;
    private final ProductLogic productLogic;
    private final ProductNewLogic productNewLogic;

    public ProductController(ProductService productService, ProductLogic productLogic, ProductNewLogic productNewLogic) {
        this.productService = productService;
        this.productLogic = productLogic;
        this.productNewLogic = productNewLogic;
    }

    /**
     * 列表
     */
    @Log(title = "商品", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('product:page')")
    @GetMapping("getPageList")
    public R<Pager<ProductPageVO>> getPageList(Page<ProductPageQueryDTO> page, ProductPageQueryDTO s) {
        page.setSearch(s);
        return R.succ(productNewLogic.getPageList(page));
    }

    /**
     * 新增查询
     */
    @PreAuthorize("hasAuthority('product:query')")
    @RequestMapping("/queryAdd/{categoryId}")
    public R<ProductAddVO> add(@PathVariable Long categoryId) {
        return R.succ(productNewLogic.getAddVOByCategoryId(categoryId));
    }

    /**
     * 编辑查询
     */
    @PreAuthorize("hasAuthority('product:query')")
    @RequestMapping("/queryEdit/{id}")
    public R<ProductEditVO> edit(@PathVariable Long id) {
        return R.succ(productNewLogic.getEditVOById(id));
    }

    /**
     * 详情查询
     */
    @PreAuthorize("hasAuthority('product:query')")
    @RequestMapping("/queryDetail/{id}")
    public R<ProductDetailVO> detail(@PathVariable Long id) {
        return R.succ(productNewLogic.getDetailVOById(id));
    }

    /**
     * 新增
     */
    @Log(title = "商品", businessType = BusinessTypeEnum.INSERT)
    @PreAuthorize("hasAuthority('product:save')")
    @PostMapping("add")
    public R<String> add(@RequestBody @Valid RequestProduct request) {
        productLogic.saveProduct(request);
        return R.success(RMsg.ADD_SUCCESS.getMsg());
    }

    /**
     * 编辑
     */
    @Log(title = "商品", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('product:save')")
    @PostMapping("edit")
    public R<String> edit(@RequestBody @Valid RequestProduct request) {
        productLogic.saveProduct(request);
        return R.success(RMsg.EDIT_SUCCESS.getMsg());
    }

    /**
     * 删除
     */
    @Log(title = "商品", businessType = BusinessTypeEnum.DELETE)
    @PreAuthorize("hasAuthority('product:del')")
    @PostMapping("del")
    public R<String> del(@RequestBody Product product) {
        productLogic.delProduct(product.getId());
        return R.success(RMsg.DELETE_SUCCESS.getMsg());
    }

    /**
     * 启用/禁用
     */
    @Log(title = "商品", businessType = BusinessTypeEnum.DELETE)
    @PreAuthorize("hasAuthority('product:save')")
    @PostMapping("updateStatus")
    public R<String> updateStatus(@RequestBody Product product) {
        productService.updateStatus(product.getId(), product.getStatus());
        return R.succ(RMsg.SUCCESS.getMsg());
    }
}
