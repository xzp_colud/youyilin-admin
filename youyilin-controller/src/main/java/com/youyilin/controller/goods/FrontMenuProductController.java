package com.youyilin.controller.goods;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.goods.model.dto.FrontMenuProductDto;
import com.youyilin.goods.model.request.RequestFrontMenuProduct;
import com.youyilin.goods.service.FrontMenuProductService;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 分类产品
 */
@RestController
@RequestMapping("front.menu.product")
public class FrontMenuProductController {

    private final FrontMenuProductService frontMenuProductService;

    public FrontMenuProductController(FrontMenuProductService frontMenuProductService) {
        this.frontMenuProductService = frontMenuProductService;
    }

    /**
     * 列表
     */
    @Log(title = "分类", businessType = BusinessTypeEnum.PAGE)
    @GetMapping("/listPageByFrontMenuId/{frontMenuId}")
    public R<List<FrontMenuProductDto>> listPageByFrontMenuId(@PathVariable Long frontMenuId) {
        return R.succ(frontMenuProductService.listPageByFrontMenuId(frontMenuId));
    }

    /**
     * 保存
     */
    @PostMapping("save")
    public R<String> save(@RequestBody @Valid RequestFrontMenuProduct request) {
        frontMenuProductService.saveFrontMenuProduct(request);
        return R.success(RMsg.ADD_SUCCESS.getMsg());
    }
}
