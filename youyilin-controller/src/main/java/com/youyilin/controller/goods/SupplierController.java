package com.youyilin.controller.goods;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.goods.dto.supplier.SupplierFormDTO;
import com.youyilin.goods.dto.supplier.SupplierPageQueryDTO;
import com.youyilin.goods.vo.supplier.SupplierPageVO;
import com.youyilin.goods.vo.supplier.SupplierVO;
import com.youyilin.logic.service.product.SupplierLogic;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 供货商
 */
@RestController
@RequestMapping("goods.supplier")
public class SupplierController {

    private final SupplierLogic supplierLogic;

    public SupplierController(SupplierLogic supplierLogic) {
        this.supplierLogic = supplierLogic;
    }

    /**
     * 列表
     */
    @Log(title = "供货商", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('goods:supplier:page')")
    @GetMapping("getPageList")
    public R<Pager<SupplierPageVO>> getPageList(Page<SupplierPageQueryDTO> page, SupplierPageQueryDTO s) {
        page.setSearch(s);
        return R.succ(supplierLogic.getPageList(page));
    }

    /**
     * 获取供货商
     */
    @GetMapping("/query/{id}")
    public R<SupplierVO> getById(@PathVariable Long id) {
        return R.succ(supplierLogic.getVOById(id));
    }

    /**
     * 所有供货商
     */
    @GetMapping("listAll")
    public R<List<SupplierVO>> listAll() {
        return R.succ(supplierLogic.listAll());
    }

    /**
     * 新增
     */
    @Log(title = "供货商", businessType = BusinessTypeEnum.INSERT)
    @PreAuthorize("hasAuthority('goods:supplier:save')")
    @PostMapping("add")
    public R<String> add(@RequestBody @Valid SupplierFormDTO dto) {
        supplierLogic.saveSupplier(dto);
        return R.success(RMsg.ADD_SUCCESS.getMsg());
    }

    /**
     * 编辑
     */
    @Log(title = "供货商", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('goods:supplier:save')")
    @PostMapping("edit")
    public R<String> edit(@RequestBody @Valid SupplierFormDTO dto) {
        supplierLogic.saveSupplier(dto);
        return R.success(RMsg.EDIT_SUCCESS.getMsg());
    }
}
