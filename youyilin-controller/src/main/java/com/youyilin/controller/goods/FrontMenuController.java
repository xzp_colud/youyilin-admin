package com.youyilin.controller.goods;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.goods.entity.FrontMenu;
import com.youyilin.goods.service.FrontMenuService;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 分类
 */
@RestController
@RequestMapping("front.menu")
public class FrontMenuController {

    private final FrontMenuService frontMenuService;

    public FrontMenuController(FrontMenuService frontMenuService) {
        this.frontMenuService = frontMenuService;
    }

    /**
     * 列表
     */
    @Log(title = "分类", businessType = BusinessTypeEnum.PAGE)
    @GetMapping("listPage")
    public R<List<FrontMenu>> listPage() {
        return R.succ(frontMenuService.listAll());
    }

    /**
     * 查询
     */
    @GetMapping("{id}")
    public R<FrontMenu> getById(@PathVariable String id) {
        return R.succ(frontMenuService.getById(id));
    }

    /**
     * 新增
     */
    @PostMapping("add")
    public R<String> add(@RequestBody @Valid FrontMenu en) {
        frontMenuService.saveFrontMenu(en);
        return R.success(RMsg.ADD_SUCCESS.getMsg());
    }

    /**
     * 编辑
     */
    @PostMapping("edit")
    public R<String> edit(@RequestBody @Valid FrontMenu en) {
        frontMenuService.saveFrontMenu(en);
        return R.success(RMsg.EDIT_SUCCESS.getMsg());
    }

    /**
     * 修改状态
     */
    @PostMapping("updateStatus")
    public R<String> updateStatus(@RequestBody FrontMenu en) {
        frontMenuService.updateStatus(en);
        return R.success(RMsg.SUCCESS.getMsg());
    }
}
