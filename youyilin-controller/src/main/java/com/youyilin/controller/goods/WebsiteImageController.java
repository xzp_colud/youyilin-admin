package com.youyilin.controller.goods;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.bean.R;
import com.youyilin.goods.entity.WebsiteImage;
import com.youyilin.goods.service.WebsiteImageService;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("website.image")
public class WebsiteImageController {

    private final WebsiteImageService websiteImageService;

    public WebsiteImageController(WebsiteImageService websiteImageService) {
        this.websiteImageService = websiteImageService;
    }

    /**
     * 列表
     */
    @GetMapping("getPageList")
    public R<Pager<WebsiteImage>> getPageList(Page<WebsiteImage> page, WebsiteImage s) {
        page.setSearch(s);
        Integer total = websiteImageService.getTotal(page);
        List<WebsiteImage> list = websiteImageService.getPage(page);
        return R.succ(new Pager<>(total, list));
    }

    /**
     * 查询
     */
    @GetMapping("/query/{id}")
    public R<WebsiteImage> query(@PathVariable Long id) {
        return R.succ(websiteImageService.getById(id));
    }

    /**
     * 新增
     */
    @PostMapping("add")
    public R<String> add(@RequestBody @Valid WebsiteImage en) {
        websiteImageService.save(en);
        return R.success();
    }

    /**
     * 编辑
     */
    @PostMapping("edit")
    public R<String> edit(@RequestBody @Valid WebsiteImage en) {
        websiteImageService.updateById(en);
        return R.success();
    }

    /**
     * 删除
     */
    @PostMapping("del")
    public R<String> del(@RequestBody WebsiteImage en) {
        websiteImageService.removeById(en.getId());
        return R.success();
    }
}
