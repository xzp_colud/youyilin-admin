package com.youyilin.controller.goods;

import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import com.youyilin.goods.entity.Space;
import com.youyilin.goods.service.SpaceService;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 空间 前端控制器
 */
@RestController
@RequestMapping("goods.space")
public class SpaceController {

    private final SpaceService spaceService;

    public SpaceController(SpaceService spaceService) {
        this.spaceService = spaceService;
    }

    /**
     * 列表
     */
    @GetMapping("listAll")
    public R<List<Space>> getPageList() {
        return R.succ(spaceService.listAll());
    }

    /**
     * 新增
     */
    @PostMapping("add")
    public R<String> add(@RequestBody @Valid Space en) {
        spaceService.saveSpace(en);
        return R.success(RMsg.ADD_SUCCESS.getMsg());
    }
}
