package com.youyilin.controller.goods;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.goods.entity.Unit;
import com.youyilin.goods.service.UnitService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 规格 前端控制器
 */
@RestController
@RequestMapping("goods.unit")
public class UnitController {

    private final UnitService unitService;

    public UnitController(UnitService unitService) {
        this.unitService = unitService;
    }

    /**
     * 列表
     */
    @Log(title = "规格", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('goods:unit:page')")
    @GetMapping("getPageList")
    public R<Pager<Unit>> getPageList(Page<Unit> page, Unit s) {
        page.setSearch(s);
        Integer totalNum = unitService.getTotal(page);
        List<Unit> pageList = unitService.getPage(page);
        return R.succ(new Pager<>(totalNum, pageList));
    }

    /**
     * ID
     */
    @GetMapping("{id}")
    public R<Unit> getById(@PathVariable String id) {
        return R.succ(unitService.getById(id));
    }

    /**
     * 新增
     */
    @Log(title = "规格", businessType = BusinessTypeEnum.INSERT)
    @PreAuthorize("hasAuthority('goods:unit:save')")
    @PostMapping("add")
    public R<String> add(@RequestBody @Valid Unit en) {
        unitService.saveUnit(en);
        return R.success(RMsg.ADD_SUCCESS.getMsg());
    }

    /**
     * 编辑
     */
    @Log(title = "规格", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('goods:unit:save')")
    @PostMapping("edit")
    public R<String> edit(@RequestBody @Valid Unit en) {
        unitService.saveUnit(en);
        return R.success(RMsg.EDIT_SUCCESS.getMsg());
    }

    /**
     * 所有销售方式
     */
    @GetMapping("listAll")
    public R<List<Unit>> listAll() {
        return R.succ(unitService.listAll());
    }
}
