package com.youyilin.controller.goods;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import com.youyilin.goods.entity.SpaceImage;
import com.youyilin.goods.service.SpaceImageService;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 空间图片 前端控制器
 */
@RestController
@RequestMapping("goods.space.image")
public class SpaceImageController {

    private final SpaceImageService spaceImageService;

    public SpaceImageController(SpaceImageService spaceImageService) {
        this.spaceImageService = spaceImageService;
    }

    /**
     * 列表
     */
    @GetMapping("getPageList")
    public R<Pager<SpaceImage>> getPageList(Page<SpaceImage> page, SpaceImage en) {
        page.setSearch(en);

        Integer total = spaceImageService.getTotal(page);
        List<SpaceImage> list = spaceImageService.getPage(page);
        return R.succ(new Pager<>(total, list));
    }

    /**
     * 新增
     */
    @PostMapping("add")
    public R<String> add(@RequestBody @Valid SpaceImage en) {
        spaceImageService.saveSpaceImage(en);
        return R.success(RMsg.ADD_SUCCESS.getMsg());
    }

    /**
     * 删除
     */
    @PostMapping("del")
    public R<String> del(@RequestBody SpaceImage en) {
        spaceImageService.delSpaceImage(en.getId());
        return R.success(RMsg.DELETE_SUCCESS.getMsg());
    }
}
