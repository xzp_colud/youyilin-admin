package com.youyilin.controller.goods;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.goods.dto.params.ParamsFormDTO;
import com.youyilin.goods.dto.params.ParamsPageQueryDTO;
import com.youyilin.goods.vo.params.ParamsPageVO;
import com.youyilin.goods.vo.params.ParamsVO;
import com.youyilin.logic.service.product.ParamsLogic;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 参数 前端控制器
 */
@RestController
@RequestMapping("goods.params")
public class ParamsController {

    private final ParamsLogic paramsLogic;

    public ParamsController(ParamsLogic paramsLogic) {
        this.paramsLogic = paramsLogic;
    }

    /**
     * 列表
     */
    @Log(title = "商品分类", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('goods:params:page')")
    @GetMapping("getPageList")
    public R<Pager<ParamsPageVO>> getPageList(Page<ParamsPageQueryDTO> page, ParamsPageQueryDTO s) {
        page.setSearch(s);
        return R.succ(paramsLogic.getPageList(page));
    }

    /**
     * 查询
     */
    @GetMapping("/query/{id}")
    public R<ParamsVO> getById(@PathVariable Long id) {
        return R.succ(paramsLogic.getParamsVOById(id));
    }

    /**
     * 所有
     */
    @GetMapping("listAll")
    public R<List<ParamsVO>> listAll() {
        return R.succ(paramsLogic.listAll());
    }

    /**
     * 新增
     */
    @PreAuthorize("hasAuthority('goods:params:save')")
    @PostMapping("add")
    public R<String> add(@RequestBody @Valid ParamsFormDTO dto) {
        paramsLogic.saveParams(dto);
        return R.success(RMsg.ADD_SUCCESS.getMsg());
    }

    /**
     * 新增
     */
    @PreAuthorize("hasAuthority('goods:params:save')")
    @PostMapping("edit")
    public R<String> edit(@RequestBody @Valid ParamsFormDTO dto) {
        paramsLogic.saveParams(dto);
        return R.success(RMsg.ADD_SUCCESS.getMsg());
    }

    /**
     * 按商品分类查询
     */
    @GetMapping("/listByCategoryId/{categoryId}")
    public R<List<ParamsVO>> listByCategoryId(@PathVariable Long categoryId) {
        return R.succ(paramsLogic.listByCategoryId(categoryId));
    }
}
