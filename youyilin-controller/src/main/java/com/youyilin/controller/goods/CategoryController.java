package com.youyilin.controller.goods;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.goods.dto.category.CategoryFormDTO;
import com.youyilin.goods.dto.category.CategoryPageQueryDTO;
import com.youyilin.goods.vo.category.CategoryDetailVO;
import com.youyilin.goods.vo.category.CategoryEditVO;
import com.youyilin.goods.vo.category.CategoryPageVO;
import com.youyilin.goods.vo.category.CategoryVO;
import com.youyilin.logic.service.product.CategoryLogic;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 商品分类 前端控制器
 */
@RestController
@RequestMapping("goods.category")
public class CategoryController {

    private final CategoryLogic categoryLogic;

    public CategoryController(CategoryLogic categoryLogic) {
        this.categoryLogic = categoryLogic;
    }

    /**
     * 列表
     */
    @Log(title = "商品分类", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('goods:category:page')")
    @GetMapping("getPageList")
    public R<Pager<CategoryPageVO>> getPageList(Page<CategoryPageQueryDTO> page, CategoryPageQueryDTO s) {
        page.setSearch(s);
        return R.succ(categoryLogic.getPageList(page));
    }

    /**
     * 查询
     */
    @GetMapping("/query/{id}")
    public R<CategoryVO> getById(@PathVariable Long id) {
        return R.succ(categoryLogic.getVOById(id));
    }

    /**
     * 所有商品分类
     */
    @GetMapping("listAll")
    public R<List<CategoryVO>> listAll() {
        return R.succ(categoryLogic.listAll());
    }

    /**
     * 分类编辑查询
     */
    @PreAuthorize("hasAuthority('goods:category:query')")
    @GetMapping("/queryEdit/{id}")
    public R<CategoryEditVO> queryEdit(@PathVariable Long id) {
        return R.succ(categoryLogic.getEditVOById(id));
    }

    /**
     * 分类详情查询
     */
    @PreAuthorize("hasAuthority('goods:category:query')")
    @GetMapping("/queryDetail/{id}")
    public R<CategoryDetailVO> queryDetail(@PathVariable Long id) {
        return R.succ(categoryLogic.getDetailVOById(id));
    }

    /**
     * 执行新增
     */
    @Log(title = "商品分类", businessType = BusinessTypeEnum.INSERT)
    @PreAuthorize("hasAuthority('goods:category:save')")
    @PostMapping("add")
    public R<String> add(@RequestBody @Valid CategoryFormDTO dto) {
        categoryLogic.saveCategory(dto);
        return R.success(RMsg.ADD_SUCCESS.getMsg());
    }

    /**
     * 执行编辑
     */
    @Log(title = "商品分类", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('goods:category:save')")
    @PostMapping("edit")
    public R<String> edit(@RequestBody @Valid CategoryFormDTO dto) {
        categoryLogic.saveCategory(dto);
        return R.success(RMsg.EDIT_SUCCESS.getMsg());
    }
}
