package com.youyilin.controller;

import com.youyilin.common.bean.R;
import com.youyilin.goods.vo.product.ProductAddVO;
import com.youyilin.goods.vo.product.ProductDetailVO;
import com.youyilin.goods.vo.product.ProductEditVO;
import com.youyilin.logic.service.product.ProductNewLogic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/testProduct")
public class TestAController {

    @Autowired
    private ProductNewLogic productNewLogic;

    @RequestMapping("/add/{categoryId}")
    public R<ProductAddVO> add(@PathVariable Long categoryId) {
        return R.succ(productNewLogic.getAddVOByCategoryId(categoryId));
    }

    @RequestMapping("/edit/{id}")
    public R<ProductEditVO> edit(@PathVariable Long id) {
        return R.succ(productNewLogic.getEditVOById(id));
    }

    @RequestMapping("/detail/{id}")
    public R<ProductDetailVO> detail(@PathVariable Long id) {
        return R.succ(productNewLogic.getDetailVOById(id));
    }
}
