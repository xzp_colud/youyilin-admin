package com.youyilin.controller.system;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.common.annotation.Log;
import com.youyilin.system.logic.SysErrorLogLogic;
import com.youyilin.system.dto.log.ErrorLogQueryDTO;
import com.youyilin.system.vo.log.ErrorLogPageVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 异常日志
 */
@RestController
@RequestMapping("system.error.log")
public class SysErrorLogController {

    private final SysErrorLogLogic sysErrorLogLogic;

    public SysErrorLogController(SysErrorLogLogic sysErrorLogLogic) {
        this.sysErrorLogLogic = sysErrorLogLogic;
    }

    /**
     * 列表
     */
    @Log(title = "异常日志", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('system:errorLog:page')")
    @GetMapping("getPageList")
    public R<Pager<ErrorLogPageVO>> getPageList(Page<ErrorLogQueryDTO> page, ErrorLogQueryDTO s) {
        page.setSearch(s);
        return R.succ(sysErrorLogLogic.getPageList(page));
    }

    /**
     * 删除
     */
    @Log(title = "异常日志", businessType = BusinessTypeEnum.DELETE)
    @PreAuthorize("hasAuthority('system:errorLog:del')")
    @PostMapping("del")
    public R<String> del(@RequestBody List<Long> ids) {
        sysErrorLogLogic.delErrorLogList(ids);
        return R.success(RMsg.DELETE_SUCCESS.getMsg());
    }
}
