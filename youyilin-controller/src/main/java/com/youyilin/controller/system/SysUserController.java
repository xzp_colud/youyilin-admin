package com.youyilin.controller.system;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.system.dto.user.UserFormDTO;
import com.youyilin.system.dto.user.UserQueryDTO;
import com.youyilin.system.logic.SysUserLogic;
import com.youyilin.system.vo.user.UserAddVO;
import com.youyilin.system.vo.user.UserEditVO;
import com.youyilin.system.vo.user.UserPageVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 用户
 */
@RestController
@RequestMapping("system.user")
public class SysUserController {

    private final SysUserLogic sysUserLogic;

    public SysUserController(SysUserLogic sysUserLogic) {
        this.sysUserLogic = sysUserLogic;
    }

    /**
     * 列表
     */
    @Log(title = "用户", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('system:user:page')")
    @GetMapping("getPageList")
    public R<Pager<UserPageVO>> getPageList(Page<UserQueryDTO> page, UserQueryDTO s) {
        page.setSearch(s);
        return R.succ(sysUserLogic.getPageList(page));
    }

    /**
     * 新增查询
     */
    @Log(title = "用户", businessType = BusinessTypeEnum.INSERT)
    @PreAuthorize("hasAuthority('system:user:query')")
    @GetMapping("queryAdd")
    public R<UserAddVO> queryAdd() {
        return R.succ(sysUserLogic.queryAdd());
    }

    /**
     * 编辑查询
     */
    @Log(title = "用户", businessType = BusinessTypeEnum.INSERT)
    @PreAuthorize("hasAuthority('system:user:query')")
    @GetMapping("/queryEdit/{id}")
    public R<UserEditVO> queryEdit(@PathVariable Long id) {
        return R.succ(sysUserLogic.queryEdit(id));
    }

    /**
     * 执行新增
     */
    @Log(title = "用户", businessType = BusinessTypeEnum.INSERT)
    @PreAuthorize("hasAuthority('system:user:save')")
    @PostMapping("add")
    public R<String> add(@RequestBody @Valid UserFormDTO dto) {
        sysUserLogic.saveUser(dto);
        return R.success(RMsg.ADD_SUCCESS.getMsg());
    }

    /**
     * 执行编辑
     */
    @Log(title = "用户", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('system:user:save')")
    @PostMapping("edit")
    public R<String> edit(@RequestBody @Valid UserFormDTO dto) {
        sysUserLogic.saveUser(dto);
        return R.success(RMsg.EDIT_SUCCESS.getMsg());
    }

    /**
     * 执行重置密码
     */
    @Log(title = "用户", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('system:user:resetPwd')")
    @PostMapping("/resetPwd/{userName}")
    public R<String> resetPassword(@PathVariable @Valid String userName) {
        sysUserLogic.resetPwd(userName);
        return R.success("重置成功, 密码为123456");
    }
}
