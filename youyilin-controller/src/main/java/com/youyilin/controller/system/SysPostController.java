package com.youyilin.controller.system;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.common.annotation.Log;
import com.youyilin.system.dto.post.PostFormDTO;
import com.youyilin.system.dto.post.PostQueryDTO;
import com.youyilin.system.logic.SysPostLogic;
import com.youyilin.system.vo.post.PostAddVO;
import com.youyilin.system.vo.post.PostEditVO;
import com.youyilin.system.vo.post.PostPageVO;
import javax.validation.Valid;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * 岗位
 */
@RestController
@RequestMapping("system.post")
public class SysPostController {

    private final SysPostLogic sysPostLogic;

    public SysPostController(SysPostLogic sysPostLogic) {
        this.sysPostLogic = sysPostLogic;
    }

    /**
     * 列表
     */
    @Log(title = "岗位", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('system:post:page')")
    @GetMapping("getPageList")
    public R<Pager<PostPageVO>> getPageList(Page<PostQueryDTO> page, PostQueryDTO s) {
        page.setSearch(s);
        return R.succ(sysPostLogic.getPageList(page));
    }

    /**
     * 查询
     */
    @Log(title = "岗位", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('system:post:query')")
    @GetMapping("queryAdd")
    public R<PostAddVO> queryAdd() {
        return R.succ(sysPostLogic.queryAdd());
    }

    /**
     * 查询
     */
    @Log(title = "岗位", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('system:post:query')")
    @GetMapping("/queryEdit/{id}")
    public R<PostEditVO> queryEdit(@PathVariable Long id) {
        return R.succ(sysPostLogic.queryEdit(id));
    }

    /**
     * 新增
     */
    @Log(title = "岗位", businessType = BusinessTypeEnum.INSERT)
    @PreAuthorize("hasAuthority('system:post:save')")
    @PostMapping("add")
    public R<String> add(@RequestBody @Valid PostFormDTO dto) {
        sysPostLogic.savePost(dto);
        return R.success(RMsg.ADD_SUCCESS.getMsg());
    }

    /**
     * 编辑
     */
    @Log(title = "岗位", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('system:post:save')")
    @PostMapping("edit")
    public R<String> edit(@RequestBody @Valid PostFormDTO dto) {
        sysPostLogic.savePost(dto);
        return R.success(RMsg.EDIT_SUCCESS.getMsg());
    }

    /**
     * 删除
     */
    @Log(title = "岗位", businessType = BusinessTypeEnum.DELETE)
    @PreAuthorize("hasAuthority('system:post:del')")
    @PostMapping("del")
    public R<String> del(@RequestBody PostFormDTO dto) {
        sysPostLogic.delPost(dto.getId());
        return R.success(RMsg.DELETE_SUCCESS.getMsg());
    }
}