package com.youyilin.controller.system;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.common.annotation.Log;
import com.youyilin.system.logic.SysNoticeLogic;
import com.youyilin.system.dto.notice.SysNoticeFormDTO;
import com.youyilin.system.entity.SysNotice;
import com.youyilin.system.dto.notice.SysNoticeQueryDTO;
import com.youyilin.system.vo.notice.SysNoticeVO;
import com.youyilin.system.service.SysNoticeService;
import javax.validation.Valid;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * 通知管理
 */
@RestController
@RequestMapping("system.notice")
public class SysNoticeController {

    private final SysNoticeService sysNoticeService;
    private final SysNoticeLogic sysNoticeLogic;

    public SysNoticeController(SysNoticeService sysNoticeService, SysNoticeLogic sysNoticeLogic) {
        this.sysNoticeService = sysNoticeService;
        this.sysNoticeLogic = sysNoticeLogic;
    }

    /**
     * 列表
     */
    @Log(title = "通知管理", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('system:notice:page')")
    @GetMapping("getPageList")
    public R<Pager<SysNoticeVO>> getPageList(Page<SysNoticeQueryDTO> page, SysNoticeQueryDTO s) {
        page.setSearch(s);
        return R.succ(sysNoticeLogic.getPageList(page));
    }

    /**
     * 查询
     */
    @Log(title = "通知管理", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('system:notice:id')")
    @GetMapping("/query/{id}")
    public R<SysNoticeVO> getById(@PathVariable Long id) {
        return R.succ(sysNoticeLogic.getById(id));
    }

    /**
     * 新增
     */
    @Log(title = "通知管理", businessType = BusinessTypeEnum.INSERT)
    @PreAuthorize("hasAuthority('system:notice:add')")
    @PostMapping("add")
    public R<String> add(@RequestBody @Valid SysNoticeFormDTO dto) {
        sysNoticeLogic.saveNotice(dto);
        return R.success(RMsg.ADD_SUCCESS.getMsg());
    }

    /**
     * 编辑
     */
    @Log(title = "通知管理", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('system:notice:edit')")
    @PostMapping("edit")
    public R<String> edit(@RequestBody @Valid SysNoticeFormDTO dto) {
        sysNoticeLogic.saveNotice(dto);
        return R.success(RMsg.EDIT_SUCCESS.getMsg());
    }

    /**
     * 禁用
     */
    @Log(title = "通知管理", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('system:notice:edit')")
    @PostMapping("disabled")
    public R<String> disabled(@RequestBody SysNotice en) {
        sysNoticeLogic.updateNoticeDisabled(en.getId());
        return R.success(RMsg.SUCCESS.getMsg());
    }

    /* ---------- 以下不需要权限 ---------- */

    /**
     * 最新一条公告
     */
    @Log(title = "通知管理", businessType = BusinessTypeEnum.SELECT)
    @GetMapping("getUpdated")
    public R<SysNotice> getUpdated() {
        return R.succ(sysNoticeService.getUpdated());
    }
}
