package com.youyilin.controller.system;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.common.annotation.Log;
import com.youyilin.system.dto.role.RoleFormDTO;
import com.youyilin.system.dto.role.RoleQueryDTO;
import com.youyilin.system.logic.SysRoleLogic;
import com.youyilin.system.vo.role.RoleEditVO;
import com.youyilin.system.vo.role.RolePageVO;
import javax.validation.Valid;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * 角色
 */
@RestController
@RequestMapping("system.role")
public class SysRoleController {

    private final SysRoleLogic sysRoleLogic;

    public SysRoleController(SysRoleLogic sysRoleLogic) {
        this.sysRoleLogic = sysRoleLogic;
    }

    /**
     * 列表
     */
    @Log(title = "角色", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('system:role:page')")
    @GetMapping("getPageList")
    public R<Pager<RolePageVO>> getPageList(Page<RoleQueryDTO> page, RoleQueryDTO s) {
        page.setSearch(s);
        return R.succ(sysRoleLogic.getPageList(page));
    }

    /**
     * 编辑查询
     */
    @Log(title = "角色", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('system:role:query')")
    @GetMapping("/queryEdit/{id}")
    public R<RoleEditVO> queryEdit(@PathVariable Long id) {
        return R.succ(sysRoleLogic.queryEdit(id));
    }

    /**
     * 执行新增
     */
    @Log(title = "角色", businessType = BusinessTypeEnum.INSERT)
    @PreAuthorize("hasAuthority('system:role:save')")
    @PostMapping("add")
    public R<String> add(@RequestBody @Valid RoleFormDTO dto) {
        sysRoleLogic.saveRole(dto);
        return R.success(RMsg.ADD_SUCCESS.getMsg());
    }

    /**
     * 执行编辑
     */
    @Log(title = "角色", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('system:role:save')")
    @PostMapping("edit")
    public R<String> edit(@RequestBody @Valid RoleFormDTO dto) {
        sysRoleLogic.saveRole(dto);
        return R.success(RMsg.EDIT_SUCCESS.getMsg());
    }
}
