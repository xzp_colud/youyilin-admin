package com.youyilin.controller.system;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.system.dto.dict.DictDataQueryDTO;
import com.youyilin.system.logic.SysDictDataLogic;
import com.youyilin.system.dto.dict.DictDataFormDTO;
import com.youyilin.system.entity.SysDictData;
import com.youyilin.system.vo.dict.DictDataEditVO;
import com.youyilin.system.vo.dict.DictDataPageVO;
import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.system.vo.dict.DictDataVO;
import javax.validation.Valid;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 字典键值
 */
@RestController
@RequestMapping("system.dict.data")
public class SysDictDataController {

    private final SysDictDataLogic sysDictDataLogic;

    public SysDictDataController(SysDictDataLogic sysDictDataLogic) {
        this.sysDictDataLogic = sysDictDataLogic;
    }

    /**
     * 列表
     */
    @Log(title = "字典键值", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('system:dictData:page')")
    @GetMapping("getPageList")
    public R<Pager<DictDataPageVO>> getPageList(Page<DictDataQueryDTO> page, DictDataQueryDTO s) {
        page.setSearch(s);
        return R.succ(sysDictDataLogic.getPageList(page));
    }

    /**
     * 查询
     */
    @Log(title = "字典键值", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('system:dictData:id')")
    @GetMapping("/queryEdit/{id}")
    public R<DictDataEditVO> queryEdit(@PathVariable Long id) {
        return R.succ(sysDictDataLogic.queryEdit(id));
    }

    /**
     * 根据字典类型查询
     */
    @Log(title = "字典键值", businessType = BusinessTypeEnum.SELECT)
//    @PreAuthorize("hasAuthority('system:dictData:listByDictType')")
    @PostMapping("listByDictTypeList")
    public R<Map<String, List<DictDataVO>>> listByDictType(@RequestBody List<String> dictType) {
        return R.succ(sysDictDataLogic.mapByDictTypeList(dictType));
    }

    /**
     * 执行新增
     */
    @Log(title = "字典键值", businessType = BusinessTypeEnum.INSERT)
    @PreAuthorize("hasAuthority('system:dictData:add')")
    @PostMapping("add")
    public R<String> add(@RequestBody @Valid DictDataFormDTO dto) {
        sysDictDataLogic.saveDictData(dto);
        return R.success(RMsg.ADD_SUCCESS.getMsg());
    }

    /**
     * 执行编辑
     */
    @Log(title = "字典键值", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('system:dictData:edit')")
    @PostMapping("edit")
    public R<String> edit(@RequestBody @Valid DictDataFormDTO dto) {
        sysDictDataLogic.saveDictData(dto);
        return R.success(RMsg.EDIT_SUCCESS.getMsg());
    }

    /**
     * 执行删除
     */
    @Log(title = "字典键值", businessType = BusinessTypeEnum.DELETE)
    @PreAuthorize("hasAuthority('system:dictData:del')")
    @PostMapping("del")
    public R<String> del(@RequestBody SysDictData en) {
        sysDictDataLogic.delDictData(en.getId());
        return R.success(RMsg.DELETE_SUCCESS.getMsg());
    }
}
