package com.youyilin.controller.system;

import com.youyilin.common.bean.Pager;
import com.youyilin.system.logic.SysFileLogic;
import com.youyilin.system.dto.file.FileQueryDTO;
import com.youyilin.system.vo.file.FilePageVO;
import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.R;
import com.youyilin.common.enums.BusinessTypeEnum;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * 文件
 */
@RestController
@RequestMapping("system.file")
public class SysFileController {

    private final SysFileLogic sysFileLogic;

    public SysFileController(SysFileLogic sysFileLogic) {
        this.sysFileLogic = sysFileLogic;
    }

    /**
     * 列表
     */
    @Log(title = "文件", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('system:file:page')")
    @GetMapping("getPageList")
    public R<Pager<FilePageVO>> getPageList(Page<FileQueryDTO> page, FileQueryDTO s) {
        page.setSearch(s);
        return R.succ(sysFileLogic.getPageList(page));
    }
}
