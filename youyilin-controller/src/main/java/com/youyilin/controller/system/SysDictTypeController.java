package com.youyilin.controller.system;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.Pager;
import com.youyilin.system.dto.dict.DictTypeQueryDTO;
import com.youyilin.system.logic.SysDictTypeLogic;
import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.system.dto.dict.DictTypeFormDTO;
import com.youyilin.system.vo.dict.DictTypeEditVO;
import com.youyilin.system.vo.dict.DictTypePageVO;
import javax.validation.Valid;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * 字典
 */
@RestController
@RequestMapping("system.dict.type")
public class SysDictTypeController {

    private final SysDictTypeLogic sysDictTypeLogic;

    public SysDictTypeController(SysDictTypeLogic sysDictTypeLogic) {
        this.sysDictTypeLogic = sysDictTypeLogic;
    }

    /**
     * 列表
     */
    @Log(title = "字典", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('system:dictType:page')")
    @GetMapping("getPageList")
    public R<Pager<DictTypePageVO>> getPageList(Page<DictTypeQueryDTO> page, DictTypeQueryDTO s) {
        page.setSearch(s);
        return R.succ(sysDictTypeLogic.getPageList(page));
    }

    /**
     * 编辑查询
     */
    @Log(title = "字典", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('system:dictType:query')")
    @GetMapping("/queryEdit/{id}")
    public R<DictTypeEditVO> queryEdit(@PathVariable Long id) {
        return R.succ(sysDictTypeLogic.queryEdit(id));
    }

    /**
     * 执行新增
     */
    @Log(title = "字典", businessType = BusinessTypeEnum.INSERT)
    @PreAuthorize("hasAuthority('system:dictType:save')")
    @PostMapping("add")
    public R<String> add(@RequestBody @Valid DictTypeFormDTO dto) {
        sysDictTypeLogic.saveDictType(dto);
        return R.success(RMsg.ADD_SUCCESS.getMsg());
    }

    /**
     * 执行编辑
     */
    @Log(title = "字典", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('system:dictType:save')")
    @PostMapping("edit")
    public R<String> edit(@RequestBody @Valid DictTypeFormDTO dto) {
        sysDictTypeLogic.saveDictType(dto);
        return R.success(RMsg.EDIT_SUCCESS.getMsg());
    }
}
