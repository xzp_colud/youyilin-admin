package com.youyilin.controller.system;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.system.dto.config.SysConfigInsertFormDTO;
import com.youyilin.system.dto.config.SysConfigUpdateFormDTO;
import com.youyilin.system.logic.SysConfigLogic;
import com.youyilin.system.vo.config.SysConfigVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 系统配置
 */
@RestController
@RequestMapping("system.config")
public class SysConfigController {

    private final SysConfigLogic sysConfigLogic;

    public SysConfigController(SysConfigLogic sysConfigLogic) {
        this.sysConfigLogic = sysConfigLogic;
    }

    /**
     * 列表
     */
    @Log(title = "系统配置", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('system:config:page')")
    @GetMapping("getPageList")
    public R<Pager<SysConfigVO>> getPageList(Page<SysConfigVO> page, SysConfigVO s) {
        page.setSearch(s);
        return R.succ(sysConfigLogic.getPageList(page));
    }

    /**
     * 查询
     */
    @Log(title = "系统配置", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('system:config:query')")
    @GetMapping("/query/{id}")
    public R<SysConfigVO> getById(@PathVariable Long id) {
        return R.succ(sysConfigLogic.getById(id));
    }

    /**
     * 所有
     */
    @Log(title = "系统配置", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('system:config:query')")
    @GetMapping("listAll")
    public R<List<SysConfigVO>> listAll() {
        return R.succ(sysConfigLogic.listAll());
    }

    /**
     * 新增
     */
    @Log(title = "系统配置", businessType = BusinessTypeEnum.INSERT)
    @PreAuthorize("hasAuthority('system:config:save')")
    @PostMapping("add")
    public R<String> add(@RequestBody @Valid SysConfigInsertFormDTO dto) {
        sysConfigLogic.saveConfig(dto);
        return R.success(RMsg.ADD_SUCCESS.getMsg());
    }

    /**
     * 编辑
     */
    @Log(title = "系统配置", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('system:config:save')")
    @PostMapping("edit")
    public R<String> edit(@RequestBody @Valid SysConfigInsertFormDTO dto) {
        sysConfigLogic.saveConfig(dto);
        return R.success(RMsg.EDIT_SUCCESS.getMsg());
    }

    /**
     * 更新配置
     */
    @Log(title = "系统配置", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('system:config:update')")
    @PostMapping("updateData")
    public R<String> updateData(@RequestBody @Valid SysConfigUpdateFormDTO dto) {
        sysConfigLogic.updateConfig(dto);
        return R.success(RMsg.SUCCESS.getMsg());
    }
}
