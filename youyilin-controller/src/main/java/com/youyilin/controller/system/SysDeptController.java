package com.youyilin.controller.system;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import com.youyilin.common.entity.TreeNodes;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.common.annotation.Log;
import com.youyilin.system.dto.dept.DeptFormDTO;
import com.youyilin.system.dto.dept.DeptQueryDTO;
import com.youyilin.system.logic.SysDeptLogic;
import com.youyilin.system.vo.dept.DeptAddVO;
import com.youyilin.system.vo.dept.DeptEditVO;
import com.youyilin.system.vo.dept.DeptPageVO;
import javax.validation.Valid;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 部门
 */
@RestController
@RequestMapping("system.dept")
public class SysDeptController {

    private final SysDeptLogic sysDeptLogic;

    public SysDeptController(SysDeptLogic sysDeptLogic) {
        this.sysDeptLogic = sysDeptLogic;
    }

    /**
     * 列表
     */
    @Log(title = "部门", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('system:dept:page')")
    @GetMapping("getPageList")
    public R<Pager<DeptPageVO>> getPageList(Page<DeptQueryDTO> page, DeptQueryDTO s) {
        page.setSearch(s);
        return R.succ(sysDeptLogic.getPageList(page));
    }

    /**
     * 部门结构查询
     */
    @Log(title = "部门", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('system:dept:tree')")
    @GetMapping("queryTree")
    public R<List<TreeNodes>> listTree() {
        return R.succ(sysDeptLogic.queryTree());
    }

    /**
     * 新增查询
     */
    @Log(title = "部门", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('system:dept:query')")
    @GetMapping("queryAdd")
    public R<DeptAddVO> queryAdd() {
        return R.succ(sysDeptLogic.queryAdd());
    }

    /**
     * 编辑查询
     */
    @Log(title = "部门", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('system:dept:query')")
    @GetMapping("/queryEdit/{id}")
    public R<DeptEditVO> queryEdit(@PathVariable Long id) {
        return R.succ(sysDeptLogic.queryEdit(id));
    }

    /**
     * 执行新增
     */
    @Log(title = "部门", businessType = BusinessTypeEnum.INSERT)
    @PreAuthorize("hasAuthority('system:dept:save')")
    @PostMapping("add")
    public R<String> add(@RequestBody @Valid DeptFormDTO dto) {
        sysDeptLogic.saveSysDept(dto);
        return R.success(RMsg.ADD_SUCCESS.getMsg());
    }

    /**
     * 执行编辑
     */
    @Log(title = "部门", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('system:dept:save')")
    @PostMapping("edit")
    public R<String> edit(@RequestBody @Valid DeptFormDTO dto) {
        sysDeptLogic.saveSysDept(dto);
        return R.success(RMsg.EDIT_SUCCESS.getMsg());
    }

    /**
     * 执行删除
     */
    @Log(title = "部门", businessType = BusinessTypeEnum.DELETE)
    @PreAuthorize("hasAuthority('system:dept:del')")
    @PostMapping("del")
    public R<String> del(@RequestBody DeptFormDTO dto) {
        sysDeptLogic.delSysDept(dto.getId());
        return R.success(RMsg.DELETE_SUCCESS.getMsg());
    }
}
