package com.youyilin.controller.system;

import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.common.annotation.Log;
import com.youyilin.system.dto.menu.MenuFormDTO;
import com.youyilin.system.logic.SysMenuLogic;
import com.youyilin.system.vo.menu.MenuAddVO;
import com.youyilin.system.vo.menu.MenuEditVO;
import com.youyilin.system.vo.menu.MenuPageVO;
import com.youyilin.system.vo.menu.MenuVO;
import javax.validation.Valid;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 菜单权限
 */
@RestController
@RequestMapping("system.menu")
public class SysMenuController {

    private final SysMenuLogic sysMenuLogic;

    public SysMenuController(SysMenuLogic sysMenuLogic) {
        this.sysMenuLogic = sysMenuLogic;
    }

    /**
     * 列表
     */
    @Log(title = "菜单权限", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('system:menu:page')")
    @GetMapping("getPageTree")
    public R<List<MenuPageVO>> getPageTree() {
        return R.succ(sysMenuLogic.getPageList());
    }

    /**
     * 新增查询
     */
    @Log(title = "菜单权限", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('system:menu:query')")
    @GetMapping("queryAdd")
    public R<MenuAddVO> queryAdd() {
        return R.succ(sysMenuLogic.queryAdd());
    }

    /**
     * 编辑查询
     */
    @Log(title = "菜单权限", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('system:menu:query')")
    @GetMapping("/queryEdit/{id}")
    public R<MenuEditVO> queryEdit(@PathVariable Long id) {
        return R.succ(sysMenuLogic.queryEdit(id));
    }
//
//    /**
//     * 菜单(下拉框)
//     */
//    @Log(title = "菜单权限", businessType = BusinessTypeEnum.SELECT)
//    @PreAuthorize("hasAuthority('system:menu:searchTree')")
//    @GetMapping("/search/tree")
//    public R<TreeNodes> treeNodes() {
//        return R.succ(sysMenuLogic.selectTree());
//    }
//
//    /**
//     * 树型菜单
//     */
//    @Log(title = "菜单权限", businessType = BusinessTypeEnum.SELECT)
//    @PreAuthorize("hasAuthority('system:menu:tree')")
//    @GetMapping("tree")
//    public R<List<TreeNodes>> tree() {
//        return R.succ(sysMenuLogic.listTree());
//    }

    /**
     * 登录用户路由
     */
    @Log(title = "菜单权限", businessType = BusinessTypeEnum.SELECT)
    @GetMapping("getRouters")
    public R<List<MenuVO>> getRouters() {
        return R.succ(sysMenuLogic.listLoginRouter());
    }

    /**
     * 执行新增
     */
    @Log(title = "菜单权限", businessType = BusinessTypeEnum.INSERT)
    @PreAuthorize("hasAuthority('system:menu:add')")
    @PostMapping("add")
    public R<String> add(@RequestBody @Valid MenuFormDTO dto) {
        sysMenuLogic.saveMenu(dto);
        return R.success();
    }

    /**
     * 执行编辑
     */
    @Log(title = "菜单权限", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('system:menu:edit')")
    @PostMapping("edit")
    public R<String> edit(@RequestBody @Valid MenuFormDTO dto) {
        sysMenuLogic.saveMenu(dto);
        return R.success();
    }

    /**
     * 执行删除
     */
    @Log(title = "菜单权限", businessType = BusinessTypeEnum.DELETE)
    @PreAuthorize("hasAuthority('system:menu:del')")
    @PostMapping("del")
    public R<String> del(@RequestBody MenuFormDTO dto) {
        sysMenuLogic.delMenu(dto.getId());
        return R.success(RMsg.DELETE_SUCCESS.getMsg());
    }
}
