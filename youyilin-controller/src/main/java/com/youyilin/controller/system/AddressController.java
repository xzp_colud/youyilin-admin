package com.youyilin.controller.system;

import com.youyilin.common.bean.Pager;
import com.youyilin.common.entity.TreeSelect;
import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.R;
import com.youyilin.system.dto.address.AddressQueryDTO;
import com.youyilin.system.entity.Address;
import com.youyilin.system.vo.address.AddressVO;
import com.youyilin.system.service.AddressService;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 国家地理
 */
@RestController
@RequestMapping("address")
public class AddressController {

    private final AddressService addressService;

    public AddressController(AddressService addressService) {
        this.addressService = addressService;
    }

    /**
     * 列表
     */
    @GetMapping("getPageList")
    public R<Pager<AddressVO>> getPageList(Page<AddressQueryDTO> page, AddressQueryDTO s) {
        page.setSearch(s);
        Integer totalNum = addressService.getTotal(page);
        List<AddressVO> pageList = addressService.getPage(page);
        return R.succ(new Pager<>(totalNum, pageList));
    }

    /**
     * 查询
     */
    @GetMapping("{id}")
    public R<Address> getById(@PathVariable Long id) {
        return R.succ(addressService.getById(id));
    }

    /**
     * 按省份查询
     */
    @GetMapping("/province/{province}")
    public R<Address> getProvinceByName(@PathVariable String province) {
        return R.succ(addressService.getProvinceByName(province));
    }

    /**
     * 按上级ID查询
     */
    @GetMapping("/next/{pidId}")
    public R<List<Address>> getNext(@PathVariable Long pidId) {
        return R.succ(addressService.listByPid(pidId));
    }

    /**
     * 树结构
     */
    @GetMapping("listTree")
    public R<List<TreeSelect>> listTree() {
        return R.succ(addressService.listTree());
    }

    /**
     * 保存
     */
    @PostMapping("add")
    public R<String> add(@RequestBody @Valid Address en) {
        if (en.getPid() == null) {
            en.setPid(0L);
        }
        addressService.save(en);
        return R.success();
    }

    /**
     * 更新
     */
    @PostMapping("edit")
    public R<String> edit(@RequestBody @Valid Address en) {
        if (en.getPid() == null) {
            en.setPid(0L);
        }
        addressService.updateById(en);
        return R.success();
    }

    /**
     * 删除
     */
    @PostMapping("del")
    public R<String> del(@RequestBody Address en) {
        if (en.getId() == null) return R.success();

        addressService.removeById(en.getId());
        return R.success();
    }
}
