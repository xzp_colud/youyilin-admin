package com.youyilin.controller.system;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.common.annotation.Log;
import com.youyilin.system.dto.log.OptLogQueryDTO;
import com.youyilin.system.logic.SysOptLogLogic;
import com.youyilin.system.vo.log.OptLogPageVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 操作日志
 */
@RestController
@RequestMapping("system.opt.log")
public class SysOptLogController {

    private final SysOptLogLogic sysOptLogLogic;

    public SysOptLogController(SysOptLogLogic sysOptLogLogic) {
        this.sysOptLogLogic = sysOptLogLogic;
    }

    /**
     * 列表
     */
    @Log(title = "操作日志", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('system:optLog:page')")
    @GetMapping("getPageList")
    public R<Pager<OptLogPageVO>> getPageList(Page<OptLogQueryDTO> page, OptLogQueryDTO s) {
        page.setSearch(s);
        return R.succ(sysOptLogLogic.getPageList(page));
    }

    /**
     * 删除
     */
    @Log(title = "操作日志", businessType = BusinessTypeEnum.DELETE)
    @PreAuthorize("hasAuthority('system:optLog:del')")
    @PostMapping("del")
    public R<String> del(@RequestBody List<Integer> ids) {
        sysOptLogLogic.delOptLog(ids);
        return R.success(RMsg.DELETE_SUCCESS.getMsg());
    }
}