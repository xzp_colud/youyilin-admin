package com.youyilin.controller.system;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.system.logic.SysConfigDataLogic;
import com.youyilin.system.dto.config.SysConfigDataInsertFormDTO;
import com.youyilin.system.dto.config.SysConfigDataPageQueryVO;
import com.youyilin.system.entity.SysConfigData;
import com.youyilin.system.vo.config.SysConfigDataVO;
import javax.validation.Valid;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 系统配置项
 */
@RestController
@RequestMapping("system.config.data")
public class SysConfigDataController {

    private final SysConfigDataLogic sysConfigDataLogic;

    public SysConfigDataController(SysConfigDataLogic sysConfigDataLogic) {
        this.sysConfigDataLogic = sysConfigDataLogic;
    }

    /**
     * 列表
     */
    @Log(title = "系统配置项", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('system:config:page')")
    @GetMapping("getPageList")
    public R<Pager<SysConfigDataVO>> getPageList(Page<SysConfigDataPageQueryVO> page, SysConfigDataPageQueryVO s) {
        page.setSearch(s);
        return R.succ(sysConfigDataLogic.getPageList(page));
    }

    /**
     * 查询
     */
    @Log(title = "系统配置项", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('system:configData:query')")
    @GetMapping("/query/{id}")
    public R<SysConfigDataVO> getById(@PathVariable Long id) {
        return R.succ(sysConfigDataLogic.getById(id));
    }

    /**
     * 查询
     */
    @Log(title = "系统配置项", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('system:configData:query')")
    @GetMapping("/listByConfigId/{configId}")
    public R<List<SysConfigDataVO>> listByConfigId(@PathVariable Long configId) {
        return R.succ(sysConfigDataLogic.listByConfigId(configId));
    }

    /**
     * 新增
     */
    @Log(title = "系统配置项", businessType = BusinessTypeEnum.INSERT)
    @PreAuthorize("hasAuthority('system:configData:save')")
    @PostMapping("add")
    public R<String> add(@RequestBody @Valid SysConfigDataInsertFormDTO dto) {
        sysConfigDataLogic.saveConfigData(dto);
        return R.success(RMsg.ADD_SUCCESS.getMsg());
    }

    /**
     * 编辑
     */
    @Log(title = "系统配置项", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('system:configData:save')")
    @PostMapping("edit")
    public R<String> edit(@RequestBody @Valid SysConfigDataInsertFormDTO dto) {
        sysConfigDataLogic.saveConfigData(dto);
        return R.success(RMsg.EDIT_SUCCESS.getMsg());
    }

    /**
     * 删除
     */
    @Log(title = "系统配置项", businessType = BusinessTypeEnum.DELETE)
    @PreAuthorize("hasAuthority('system:configData:del')")
    @PostMapping("del")
    public R<String> del(@RequestBody @Valid SysConfigData en) {
        sysConfigDataLogic.delConfigData(en.getId());
        return R.success(RMsg.EDIT_SUCCESS.getMsg());
    }
}
