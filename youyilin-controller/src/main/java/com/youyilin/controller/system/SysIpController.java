package com.youyilin.controller.system;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.system.logic.SysIpLogic;
import com.youyilin.system.dto.ip.IpQueryDTO;
import com.youyilin.system.dto.ip.IpFormDTO;
import com.youyilin.system.vo.ip.IpEditVO;
import com.youyilin.system.vo.ip.IpPageVO;
import javax.validation.Valid;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * IP
 */
@RestController
@RequestMapping("system.ip")
public class SysIpController {

    private final SysIpLogic sysIpLogic;

    public SysIpController(SysIpLogic sysIpLogic) {
        this.sysIpLogic = sysIpLogic;
    }

    /**
     * 列表
     */
    @Log(title = "IP", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('system:ip:page')")
    @GetMapping("getPageList")
    public R<Pager<IpPageVO>> getPageList(Page<IpQueryDTO> page, IpQueryDTO s) {
        page.setSearch(s);
        return R.succ(sysIpLogic.getPageList(page));
    }

    /**
     * 编辑查询
     */
    @Log(title = "IP", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('system:ip:query')")
    @GetMapping("/queryEdit/{id}")
    public R<IpEditVO> queryEdit(@PathVariable Long id) {
        return R.succ(sysIpLogic.queryEdit(id));
    }

    /**
     * 执行新增
     */
    @Log(title = "IP", businessType = BusinessTypeEnum.INSERT)
    @PreAuthorize("hasAuthority('system:ip:save')")
    @PostMapping("add")
    public R<String> add(@RequestBody @Valid IpFormDTO dto) {
        sysIpLogic.saveIp(dto);
        return R.success(RMsg.ADD_SUCCESS.getMsg());
    }

    /**
     * 执行编辑
     */
    @Log(title = "IP", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('system:ip:save')")
    @PostMapping("edit")
    public R<String> edit(@RequestBody @Valid IpFormDTO dto) {
        sysIpLogic.saveIp(dto);
        return R.success(RMsg.EDIT_SUCCESS.getMsg());
    }

    /**
     * 执行删除
     */
    @Log(title = "IP", businessType = BusinessTypeEnum.DELETE)
    @PreAuthorize("hasAuthority('system:ip:del')")
    @PostMapping("del")
    public R<String> del(@RequestBody IpFormDTO dto) {
        sysIpLogic.delIp(dto.getId());
        return R.success(RMsg.DELETE_SUCCESS.getMsg());
    }
}
