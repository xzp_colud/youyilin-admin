package com.youyilin.controller.system;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.bean.R;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.common.utils.SecurityUtil;
import com.youyilin.common.annotation.Log;
import com.youyilin.system.logic.SysNoticeUserReadLogic;
import com.youyilin.system.dto.notice.SysNoticeSendFormDTO;
import com.youyilin.system.entity.SysNoticeUserRead;
import com.youyilin.system.vo.notice.SysNoticeUserReadSendVO;
import com.youyilin.system.vo.notice.SysNoticeUserReadVO;
import javax.validation.Valid;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 通知读取 控制层
 */
@RestController
@RequestMapping("system.notice.read")
public class SysNoticeUserReadController {

    private final SysNoticeUserReadLogic sysNoticeUserReadLogic;

    public SysNoticeUserReadController(SysNoticeUserReadLogic sysNoticeUserReadLogic) {
        this.sysNoticeUserReadLogic = sysNoticeUserReadLogic;
    }

    /**
     * 列表
     */
    @Log(title = "通知读取列表", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('system:noticeRead:page')")
    @GetMapping("getPageList")
    public R<Pager<SysNoticeUserReadVO>> getPageList(Page<SysNoticeUserReadVO> page, SysNoticeUserReadVO s) {
        if (!s.isTable()) {
            s.setUserId(SecurityUtil.getUserId());
        }
        page.setSearch(s);
        return R.succ(sysNoticeUserReadLogic.getPageList(page));
    }

    /**
     * 发送列表(选择用户)
     */
    @Log(title = "通知发送列表", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('system:noticeRead:sendPage')")
    @GetMapping("getSendPageList")
    public R<List<SysNoticeUserReadSendVO>> getSendPageList(SysNoticeUserReadSendVO s) {
        return R.succ(sysNoticeUserReadLogic.listUser(s));
    }

    /**
     * 发送
     */
    @Log(title = "通知读取列表", businessType = BusinessTypeEnum.INSERT)
    @PreAuthorize("hasAuthority('system:noticeRead:send')")
    @PostMapping("send")
    public R<String> send(@RequestBody @Valid SysNoticeSendFormDTO dto) {
        sysNoticeUserReadLogic.saveUserRead(dto);
        return R.success("发送成功");
    }

    /**
     * 撤销
     */
    @Log(title = "通知读取列表", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('system:noticeRead:cancel')")
    @PostMapping("cancel")
    public R<String> cancel(@RequestBody List<Long> ids) {
        sysNoticeUserReadLogic.updateCancelByIds(ids);
        return R.success("撤销成功");
    }

    /**
     * 全部撤销
     */
    @Log(title = "通知读取列表", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('system:noticeRead:cancel')")
    @PostMapping("cancelAll")
    public R<String> cancelAll(@RequestBody SysNoticeUserRead en) {
        sysNoticeUserReadLogic.updateCancelByNoticeId(en.getNoticeId());
        return R.success();
    }

    /* ---------- 以下不需要权限 start ---------- */

    /**
     * 用户通知列表(无权限)
     */
    @Log(title = "通知读取列表", businessType = BusinessTypeEnum.PAGE)
    @GetMapping("getUserPageList")
    public R<Pager<SysNoticeUserReadVO>> getUserPageList(Page<SysNoticeUserReadVO> page, SysNoticeUserReadVO s) {
        s.setUserId(SecurityUtil.getUserId());
        page.setSearch(s);
        return R.succ(sysNoticeUserReadLogic.getPageList(page));
    }

    /**
     * 当前未读通知数量
     */
    @GetMapping("getNoReadNum")
    public R<Integer> getNoReadNum() {
        return R.succ(sysNoticeUserReadLogic.countLoginNoReadNum());
    }

    /**
     * 单个已读
     */
    @Log(title = "通知读取列表", businessType = BusinessTypeEnum.UPDATE)
    @PostMapping("read")
    public R<String> read(@RequestBody SysNoticeUserRead en) {
        sysNoticeUserReadLogic.updateLoginRead(en.getId());
        return R.succ("");
    }

    /**
     * 全部已读
     */
    @Log(title = "通知读取列表", businessType = BusinessTypeEnum.UPDATE)
    @PostMapping("readAll")
    public R<String> readAll() {
        sysNoticeUserReadLogic.updateLoginRead();
        return R.success();
    }

    /* ---------- 以下不需要权限 end ---------- */
}
