package com.youyilin.controller.system;

import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.common.annotation.Log;
import com.youyilin.system.dto.role.RoleMenuFormDTO;
import com.youyilin.system.logic.SysRoleMenuLogic;
import com.youyilin.system.vo.role.RoleAuthVO;
import javax.validation.Valid;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * 角色授权
 */
@RestController
@RequestMapping("system.role.menu")
public class SysRoleMenuController {

    private final SysRoleMenuLogic sysRoleMenuLogic;

    public SysRoleMenuController(SysRoleMenuLogic sysRoleMenuLogic) {
        this.sysRoleMenuLogic = sysRoleMenuLogic;
    }

    /**
     * 根据角色查询
     */
    @Log(title = "角色授权", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('system:roleMenu:query')")
    @GetMapping("/getAuth/{roleId}")
    public R<RoleAuthVO> add(@PathVariable Long roleId) {
        return R.succ(sysRoleMenuLogic.getAuth(roleId));
    }

    /**
     * 执行授权
     */
    @Log(title = "角色授权", businessType = BusinessTypeEnum.INSERT)
    @PreAuthorize("hasAuthority('system:roleMenu:save')")
    @PostMapping("auth")
    public R<String> auth(@RequestBody @Valid RoleMenuFormDTO dto) {
        sysRoleMenuLogic.auth(dto);
        return R.success(RMsg.ADD_SUCCESS.getMsg());
    }
}
