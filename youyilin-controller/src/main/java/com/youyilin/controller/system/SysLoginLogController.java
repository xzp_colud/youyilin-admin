package com.youyilin.controller.system;

import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.common.annotation.Log;
import com.youyilin.system.dto.log.LoginLogQueryDTO;
import com.youyilin.system.entity.SysLoginLog;
import com.youyilin.system.logic.SysLoginLogLogic;
import com.youyilin.system.vo.log.LoginLogPageVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * 登录日志
 */
@RestController
@RequestMapping("system.login.log")
public class SysLoginLogController {

    private final SysLoginLogLogic sysLoginLogLogic;

    public SysLoginLogController(SysLoginLogLogic sysLoginLogLogic) {
        this.sysLoginLogLogic = sysLoginLogLogic;
    }

    /**
     * 列表
     */
    @Log(title = "登录日志", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('system:loginLog:page')")
    @GetMapping("getPageList")
    public R<Pager<LoginLogPageVO>> getPageList(Page<LoginLogQueryDTO> page, LoginLogQueryDTO s) {
        page.setSearch(s);
        return R.succ(sysLoginLogLogic.getPageList(page));
    }

    /**
     * 删除
     */
    @Log(title = "登录日志", businessType = BusinessTypeEnum.DELETE)
    @PreAuthorize("hasAuthority('system:loginLog:del')")
    @PostMapping("del")
    public R<String> del(@RequestBody SysLoginLog en) {
        sysLoginLogLogic.delLoginLog(en.getId());
        return R.success(RMsg.DELETE_SUCCESS.getMsg());
    }
}
