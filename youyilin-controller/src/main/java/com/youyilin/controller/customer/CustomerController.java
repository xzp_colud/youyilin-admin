package com.youyilin.controller.customer;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.customer.dto.customer.CustomerFormDTO;
import com.youyilin.customer.dto.customer.CustomerPageQueryDTO;
import com.youyilin.customer.vo.customer.CustomerDetailVO;
import com.youyilin.customer.vo.customer.CustomerEditVO;
import com.youyilin.customer.vo.customer.CustomerPageVO;
import com.youyilin.logic.service.customer.CustomerLogic;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 客户/消费者 控制层
 */
@RestController
@RequestMapping("customer")
public class CustomerController {

    private final CustomerLogic customerLogic;

    public CustomerController(CustomerLogic customerLogic) {
        this.customerLogic = customerLogic;
    }

    /**
     * 获取分页列表
     */
    @Log(title = "客户", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('customer:page')")
    @GetMapping("getPageList")
    public R<Pager<CustomerPageVO>> getPageList(Page<CustomerPageQueryDTO> page, CustomerPageQueryDTO s) {
        page.setSearch(s);
        return R.succ(customerLogic.getPageList(page));
    }

    /**
     * 获取客户编辑信息
     */
    @Log(title = "客户", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('customer:edit')")
    @GetMapping("/queryEdit/{id}")
    public R<CustomerEditVO> queryEdit(@PathVariable Long id) {
        return R.succ(customerLogic.getEditVOById(id));
    }

    /**
     * 获取客户详细信息
     */
    @Log(title = "客户", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('customer:query')")
    @GetMapping("/queryDetail/{id}")
    public R<CustomerDetailVO> queryDetail(@PathVariable Long id) {
        return R.succ(customerLogic.getDetailVOById(id));
    }

    /**
     * 执行新增
     */
    @Log(title = "客户", businessType = BusinessTypeEnum.INSERT)
    @PreAuthorize("hasAuthority('customer:add')")
    @PostMapping("add")
    public R<String> add(@RequestBody @Valid CustomerFormDTO dto) {
        customerLogic.saveCustomer(dto);
        return R.success(RMsg.ADD_SUCCESS.getMsg());
    }

    /**
     * 执行编辑
     */
    @Log(title = "客户", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('customer:edit')")
    @PostMapping("edit")
    public R<String> edit(@RequestBody @Valid CustomerFormDTO dto) {
        customerLogic.saveCustomer(dto);
        return R.success(RMsg.EDIT_SUCCESS.getMsg());
    }
}
