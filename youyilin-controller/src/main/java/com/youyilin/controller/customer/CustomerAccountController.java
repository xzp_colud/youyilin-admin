package com.youyilin.controller.customer;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.customer.dto.account.CustomerAccountChargeFormDTO;
import com.youyilin.customer.dto.account.CustomerAccountFormDTO;
import com.youyilin.customer.dto.account.CustomerAccountPageQueryDTO;
import com.youyilin.logic.service.customer.CustomerMiniAccountLogic;
import com.youyilin.customer.vo.account.CustomerMiniAccountGridVO;
import com.youyilin.customer.vo.account.CustomerAccountPageVO;
import com.youyilin.logic.service.customer.CustomerAccountLogic;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 账户 控制层
 */
@RestController
@RequestMapping("customer.account")
public class CustomerAccountController {

    private final CustomerAccountLogic customerAccountLogic;
    private final CustomerMiniAccountLogic customerMiniAccountLogic;

    public CustomerAccountController(CustomerAccountLogic customerAccountLogic, CustomerMiniAccountLogic customerMiniAccountLogic) {
        this.customerAccountLogic = customerAccountLogic;
        this.customerMiniAccountLogic = customerMiniAccountLogic;
    }

    /**
     * 列表
     */
    @Log(title = "账户", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('account:page')")
    @GetMapping("getPageList")
    public R<Pager<CustomerAccountPageVO>> getPageList(Page<CustomerAccountPageQueryDTO> page, CustomerAccountPageQueryDTO s) {
        page.setSearch(s);
        return R.succ(customerAccountLogic.getPageList(page));
    }

    /**
     * ID
     */
    @Log(title = "账户", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('account:query')")
    @GetMapping("/listByCustomerId/{customerId}")
    public R<List<CustomerAccountPageVO>> listByCustomerId(@PathVariable Long customerId) {
        return R.succ(customerAccountLogic.listByCustomerId(customerId));
    }

    /**
     * 新增
     */
    @Log(title = "账户", businessType = BusinessTypeEnum.INSERT)
    @PreAuthorize("hasAuthority('account:save')")
    @PostMapping("add")
    public R<String> add(@RequestBody @Valid CustomerAccountFormDTO dto) {
        customerAccountLogic.saveAccount(dto);
        return R.success(RMsg.SUCCESS.getMsg());
    }

    /**
     * 充值
     */
    @Log(title = "账户", businessType = BusinessTypeEnum.CHARGE)
    @PreAuthorize("hasAuthority('account:charge')")
    @PostMapping("charge")
    public R<String> charge(@RequestBody @Valid CustomerAccountChargeFormDTO dto) {
        customerAccountLogic.updateCharge(dto);
        return R.success(RMsg.SUCCESS.getMsg());
    }

    // ----- 小程序 ----- //
    @GetMapping("miniList")
    public R<List<CustomerMiniAccountGridVO>> miniList() {
        return R.succ(customerMiniAccountLogic.miniList());
    }
}
