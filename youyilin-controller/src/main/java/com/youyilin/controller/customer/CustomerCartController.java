package com.youyilin.controller.customer;

import com.youyilin.common.bean.R;
import com.youyilin.logic.service.customer.CustomerMiniCartLogic;
import com.youyilin.customer.dto.request.RequestMiniCart;
import com.youyilin.customer.vo.CustomerCartVo;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 购物车
 */
@RestController
@RequestMapping("customer.cart")
public class CustomerCartController {

    private final CustomerMiniCartLogic customerMiniCartLogic;

    public CustomerCartController(CustomerMiniCartLogic customerMiniCartLogic) {
        this.customerMiniCartLogic = customerMiniCartLogic;
    }

    /**
     * 小程序 列表
     *
     * @return ArrayList
     */
    @GetMapping("miniList")
    public R<List<CustomerCartVo>> list() {
        return R.succ(customerMiniCartLogic.listCart());
    }

    /**
     * 小程序 购物车数量统计
     *
     * @return Long
     */
    @GetMapping("miniCount")
    public R<Long> count() {
        return R.succ(customerMiniCartLogic.countCart());
    }

    /**
     * 小程序 添加产品至购物车
     *
     * @param request request
     * @return String
     */
    @PostMapping("miniAdd")
    public R<String> add(@RequestBody @Valid RequestMiniCart request) {
        customerMiniCartLogic.saveProductToCart(request);
        return R.success();
    }

    /**
     * 小程序 删除购物车中产品
     *
     * @param ids 购物车ID
     * @return String
     */
    @PostMapping("miniDel")
    public R<String> del(@RequestBody List<Long> ids) {
        customerMiniCartLogic.delByIds(ids);
        return R.success();
    }
}
