package com.youyilin.controller.customer;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.bean.R;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.customer.dto.accountLog.CustomerAccountLogPageQueryDTO;
import com.youyilin.customer.vo.accountLog.CustomerAccountLogPageVO;
import com.youyilin.logic.service.customer.CustomerAccountLogLogic;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 账户 日志 控制层
 */
@RestController
@RequestMapping("customer.account.log")
public class CustomerAccountLogController {

    private final CustomerAccountLogLogic customerAccountLogLogic;

    public CustomerAccountLogController(CustomerAccountLogLogic customerAccountLogLogic) {
        this.customerAccountLogLogic = customerAccountLogLogic;
    }

    /**
     * 列表
     */
    @Log(title = "账户日志", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('account:log:page')")
    @GetMapping("getPageList")
    public R<Pager<CustomerAccountLogPageVO>> getPageList(Page<CustomerAccountLogPageQueryDTO> page, CustomerAccountLogPageQueryDTO s) {
        page.setSearch(s);
        return R.succ(customerAccountLogLogic.getPageList(page));
    }
}
