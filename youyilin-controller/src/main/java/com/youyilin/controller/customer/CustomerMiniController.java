package com.youyilin.controller.customer;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.bean.R;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.customer.logic.CustomerMiniLogic;
import com.youyilin.customer.entity.CustomerMini;
import com.youyilin.customer.service.CustomerMiniService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 客户/消费者(小程序) 控制层
 */
@RestController
@RequestMapping("customer.mini")
public class CustomerMiniController {

    private final CustomerMiniService customerMiniService;
    private final CustomerMiniLogic customerMiniLogic;

    public CustomerMiniController(CustomerMiniService customerMiniService, CustomerMiniLogic customerMiniLogic) {
        this.customerMiniService = customerMiniService;
        this.customerMiniLogic = customerMiniLogic;
    }

    /**
     * 列表
     */
    @Log(title = "小程序客户", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('customer:mini:list')")
    @GetMapping("getPageList")
    public R<Pager<CustomerMini>> getPageList(Page<CustomerMini> page, CustomerMini s) {
        page.setSearch(s);
        Integer totalNum = customerMiniService.getTotal(page);
        List<CustomerMini> list = customerMiniService.getPage(page);
        return R.succ(new Pager<>(totalNum, list));
    }

    /**
     * 获取信息
     *
     * @return CustomerMini
     */
    @Log(title = "小程序客户", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('customer:mini:query')")
    @GetMapping("/getMini/{id}")
    public R<CustomerMini> getMini(@PathVariable Long id) {
        return R.succ(customerMiniService.getById(id));
    }

    /**
     * 小程序 获取登录信息
     *
     * @return CustomerMini
     */
    @GetMapping("getUserInfo")
    public R<CustomerMini> getUserInfo() {
        return R.succ(customerMiniLogic.getUserInfo());
    }

    /**
     * 小程序 修改微信名称 头像
     */
    @PostMapping("updateInfo")
    public R<String> updateInfo(@RequestBody CustomerMini cm) {
        customerMiniService.updateInfo(cm);
        return R.success();
    }

    /**
     * 小程序 修改手机号
     *
     * @param code 微信获取手机号凭证
     * @return String
     */
    @PostMapping("updatePhone")
    public R<String> updatePhone(String code) {
        customerMiniLogic.updatePhone(code);
        return R.success();
    }
}
