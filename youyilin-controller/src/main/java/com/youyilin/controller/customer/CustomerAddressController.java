package com.youyilin.controller.customer;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.customer.dto.address.CustomerAddressDefaultDTO;
import com.youyilin.customer.dto.address.CustomerAddressDeleteDTO;
import com.youyilin.customer.dto.address.CustomerAddressFormDTO;
import com.youyilin.customer.dto.address.CustomerMiniAddressFormDTO;
import com.youyilin.customer.vo.address.CustomerAddressDetailVO;
import com.youyilin.customer.vo.address.CustomerAddressEditVO;
import com.youyilin.customer.vo.address.CustomerAddressPageVO;
import com.youyilin.logic.service.customer.CustomerAddressLogic;
import com.youyilin.logic.service.customer.CustomerMiniAddressLogic;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 地址 控制层
 */
@RestController
@RequestMapping("customer.address")
public class CustomerAddressController {

    private final CustomerAddressLogic customerAddressLogic;
    private final CustomerMiniAddressLogic customerMiniAddressLogic;

    public CustomerAddressController(CustomerAddressLogic customerAddressLogic, CustomerMiniAddressLogic customerMiniAddressLogic) {
        this.customerAddressLogic = customerAddressLogic;
        this.customerMiniAddressLogic = customerMiniAddressLogic;
    }

    /**
     * 获取编辑信息
     */
    @Log(title = "地址", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('customer:address:query')")
    @GetMapping("/queryEdit/{id}")
    public R<CustomerAddressEditVO> queryEdit(@PathVariable Long id) {
        return R.succ(customerAddressLogic.getEditVOById(id));
    }

    /**
     * 获取客户收货地址列表
     */
    @Log(title = "地址", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('customer:address:query')")
    @GetMapping("/listByCustomerId/{customerId}")
    public R<List<CustomerAddressPageVO>> listByCustomerId(@PathVariable Long customerId) {
        return R.succ(customerAddressLogic.listByCustomerId(customerId));
    }

    /**
     * 执行新增
     */
    @Log(title = "地址", businessType = BusinessTypeEnum.INSERT)
    @PreAuthorize("hasAuthority('customer:address:save')")
    @PostMapping("add")
    public R<String> add(@RequestBody @Valid CustomerAddressFormDTO dto) {
        customerAddressLogic.saveAddress(dto);
        return R.success(RMsg.ADD_SUCCESS.getMsg());
    }

    /**
     * 执行编辑
     */
    @Log(title = "地址", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('customer:address:save')")
    @PostMapping("edit")
    public R<String> edit(@RequestBody @Valid CustomerAddressFormDTO dto) {
        customerAddressLogic.saveAddress(dto);
        return R.success(RMsg.EDIT_SUCCESS.getMsg());
    }

    /**
     * 执行更新为默认地址
     */
    @Log(title = "地址", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('customer:address:save')")
    @PostMapping("defaults")
    public R<String> defaults(@RequestBody @Valid CustomerAddressDefaultDTO dto) {
        customerAddressLogic.updateDefault(dto);
        return R.success(RMsg.SUCCESS.getMsg());
    }

    /**
     * 执行删除
     */
    @Log(title = "地址", businessType = BusinessTypeEnum.DELETE)
    @PreAuthorize("hasAuthority('customer:address:del')")
    @PostMapping("del")
    public R<String> del(@RequestBody @Valid CustomerAddressDeleteDTO dto) {
        customerAddressLogic.delAddress(dto);
        return R.success(RMsg.DELETE_SUCCESS.getMsg());
    }

    // ----- 小程序 ----- //

    /**
     * 小程序 客户地址列表
     */
    @GetMapping("miniList")
    public R<List<CustomerAddressPageVO>> listByMini() {
        return R.succ(customerMiniAddressLogic.listAddress());
    }

    /**
     * 小程序 查询
     */
    @GetMapping("/miniSearch/{id}")
    public R<CustomerAddressEditVO> miniSearch(@PathVariable Long id) {
        return R.succ(customerMiniAddressLogic.getAddress(id));
    }

    /**
     * 小程序 查询默认地址
     */
    @GetMapping("miniDefault")
    public R<CustomerAddressDetailVO> miniDefault() {
        return R.succ(customerMiniAddressLogic.getDefault());
    }

    /**
     * 小程序 新增地址
     */
    @PostMapping("miniAdd")
    public R<String> miniAdd(@RequestBody @Valid CustomerMiniAddressFormDTO dto) {
        customerMiniAddressLogic.saveAddress(dto);
        return R.succ("添加成功");
    }

    /**
     * 小程序 编辑地址
     */
    @PostMapping("miniEdit")
    public R<String> miniEdit(@RequestBody @Valid CustomerMiniAddressFormDTO dto) {
        customerMiniAddressLogic.saveAddress(dto);
        return R.succ("编辑成功");
    }

    /**
     * 小程序 更新默认
     */
    @PostMapping("miniUpdateDefault")
    public R<String> miniUpdateDefault(CustomerMiniAddressFormDTO dto) {
        customerMiniAddressLogic.updateDefault(dto.getId());
        return R.succ("设置成功");
    }

    /**
     * 小程序 删除
     */
    @PostMapping("miniDel")
    public R<String> miniDel(CustomerAddressDeleteDTO dto) {
        customerMiniAddressLogic.delAddress(dto.getId());
        return R.succ("编辑成功");
    }
}
