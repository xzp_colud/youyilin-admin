package com.youyilin.controller;

import lombok.Data;

@Data
public class UpdatePasswordDto {

    // 用户名称
    private String userName;
    // 旧密码
    private String password;
    // 新密码
    private String newPassword;
    // 新密码确认
    private String reNewPassword;
}
