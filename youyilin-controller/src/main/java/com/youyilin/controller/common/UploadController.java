package com.youyilin.controller.common;

import com.youyilin.common.bean.R;
import com.youyilin.common.config.qiniu.QiNiuConfig;
import com.youyilin.common.config.qiniu.QiNiuService;
import com.youyilin.common.enums.StatusEnum;
import com.youyilin.system.entity.SysFile;
import com.youyilin.system.service.SysFileService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * 上传文件
 */
@Slf4j
@RestController
@RequestMapping("/upload")
public class UploadController {

    private final QiNiuConfig qiNiuConfig;
    private final SysFileService sysFileService;

    public UploadController(QiNiuConfig qiNiuConfig, SysFileService sysFileService) {
        this.qiNiuConfig = qiNiuConfig;
        this.sysFileService = sysFileService;
    }

    /**
     * 上传文件
     */
    @PostMapping("file")
    public R<SysFile> uploadFile(@RequestParam("file") MultipartFile file, String fileGroup) {
        try {
            if (StringUtils.isBlank(fileGroup)) {
                fileGroup = "默认";
            }
            String name = file.getResource().getFilename();
            String size = String.valueOf(file.getSize());
            String suffix = name != null ? name.substring(name.lastIndexOf(".") + 1) : null;
            String resolution = "";
            BufferedImage image = ImageIO.read(file.getInputStream());
            if (image != null) {
                String width = String.valueOf(image.getWidth());
                String height = String.valueOf(image.getHeight());
                resolution = width + "*" + height;
            }
            byte[] b = file.getBytes();
            QiNiuService qiNiuService = new QiNiuService.Builder().config(qiNiuConfig).build();
            String url = qiNiuService.uploadBytes(b);
            if (url != null) {
                SysFile sysFile = new SysFile();
                sysFile.setName(name)
                        .setFileGroup(fileGroup)
                        .setUrl(url)
                        .setSuffix(suffix)
                        .setSize(size)
                        .setResolution(resolution)
                        .setStatus(StatusEnum.NORMAL.getCode());
                sysFileService.save(sysFile);
                return R.succ(sysFile);
            }
        } catch (IOException e) {
            log.error("【文件上传失败】", e);
        }
        return R.fail("上传失败");
    }
}
