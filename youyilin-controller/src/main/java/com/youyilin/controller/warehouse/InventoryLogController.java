package com.youyilin.controller.warehouse;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.bean.R;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.logic.service.warehouse.InventoryLogLogic;
import com.youyilin.warehouse.dto.inventoryLog.InventoryLogPageQueryDTO;
import com.youyilin.warehouse.vo.inventoryLog.InventoryLogPageVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 库存日志 控制层
 */
@RestController
@RequestMapping("warehouse.inventory.log")
public class InventoryLogController {

    private final InventoryLogLogic inventoryLogLogic;

    public InventoryLogController(InventoryLogLogic inventoryLogLogic) {
        this.inventoryLogLogic = inventoryLogLogic;
    }

    /**
     * 列表
     */
    @Log(title = "库存日志", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('inventory:log:page')")
    @GetMapping("getPageList")
    public R<Pager<InventoryLogPageVO>> getPageList(Page<InventoryLogPageQueryDTO> page, InventoryLogPageQueryDTO s) {
        page.setSearch(s);
        return R.succ(inventoryLogLogic.getPageList(page));
    }

    /**
     * 列表
     */
    @Log(title = "库存日志", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('inventory:log:page')")
    @GetMapping("listPageByInventory")
    public R<List<InventoryLogPageVO>> listPageByInventory(Long inventoryId) {
        return R.succ(inventoryLogLogic.listByInventoryId(inventoryId));
    }

    /**
     * 列表
     */
    @Log(title = "库存日志", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('inventory:log:page')")
    @GetMapping("listPageByInventoryLot")
    public R<List<InventoryLogPageVO>> listByInventoryLotId(Long inventoryLotId) {
        return R.succ(inventoryLogLogic.listByInventoryLotId(inventoryLotId));
    }
}
