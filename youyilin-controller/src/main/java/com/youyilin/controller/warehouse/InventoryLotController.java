package com.youyilin.controller.warehouse;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.logic.service.warehouse.InventoryLotLogic;
import com.youyilin.warehouse.dto.InventoryLot.InventoryLotCheckFormDTO;
import com.youyilin.warehouse.dto.InventoryLot.InventoryLotDeleteFormDTO;
import com.youyilin.warehouse.dto.InventoryLot.InventoryLotMoveFormDTO;
import com.youyilin.warehouse.dto.InventoryLot.InventoryLotSaveFormDTO;
import com.youyilin.warehouse.vo.inventoryLot.InventoryLotPageVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 库存日志 控制层
 */
@RestController
@RequestMapping("warehouse.inventory.lot")
public class InventoryLotController {

    private final InventoryLotLogic inventoryLotLogic;

    public InventoryLotController(InventoryLotLogic inventoryLotLogic) {
        this.inventoryLotLogic = inventoryLotLogic;
    }

    /**
     * 批次列表
     */
    @Log(title = "库存批次", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('inventory:lot:page')")
    @GetMapping("listPageByInventory")
    public R<List<InventoryLotPageVO>> listByInventoryId(Long inventoryId) {
        return R.succ(inventoryLotLogic.listByInventoryId(inventoryId));
    }

    /**
     * 盘点新增
     */
    @Log(title = "库存批次", businessType = BusinessTypeEnum.INSERT)
    @PreAuthorize("hasAuthority('inventory:lot:save')")
    @PostMapping("save")
    public R<String> save(@RequestBody @Valid InventoryLotSaveFormDTO dto) {
        inventoryLotLogic.saveInventoryLot(dto);
        return R.success(RMsg.SUCCESS.getMsg());
    }

    /**
     * 盘点出库
     */
    @Log(title = "库存批次", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('inventory:lot:checkOut')")
    @PostMapping("checkOut")
    public R<String> checkOut(@RequestBody @Valid InventoryLotCheckFormDTO dto) {
        inventoryLotLogic.updateInventoryLotCheckOut(dto);
        return R.success(RMsg.SUCCESS.getMsg());
    }

    /**
     * 盘点入库
     */
    @Log(title = "库存批次", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('inventory:lot:checkIn')")
    @PostMapping("checkIn")
    public R<String> checkIn(@RequestBody @Valid InventoryLotCheckFormDTO dto) {
        inventoryLotLogic.updateInventoryLotCheckIn(dto);
        return R.success(RMsg.SUCCESS.getMsg());
    }

    /**
     * 移动库存
     */
    @Log(title = "库存批次", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('inventory:lot:move')")
    @PostMapping("move")
    public R<String> move(@RequestBody @Valid InventoryLotMoveFormDTO dto) {
        inventoryLotLogic.updateInventoryLotMove(dto);
        return R.success(RMsg.SUCCESS.getMsg());
    }

    /**
     * 删除批次
     */
    @Log(title = "库存批次", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('inventory:lot:del')")
    @PostMapping("del")
    public R<String> del(@RequestBody @Valid InventoryLotDeleteFormDTO dto) {
        inventoryLotLogic.delInventoryLot(dto);
        return R.success(RMsg.SUCCESS.getMsg());
    }
}
