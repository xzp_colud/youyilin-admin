package com.youyilin.controller.warehouse;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.logic.service.warehouse.InventoryLogic;
import com.youyilin.warehouse.dto.inventory.InventoryDeleteFormDTO;
import com.youyilin.warehouse.dto.inventory.InventoryMoveFormDTO;
import com.youyilin.warehouse.dto.inventory.InventoryPageQueryDTO;
import com.youyilin.warehouse.vo.inventory.InventoryCheckVO;
import com.youyilin.warehouse.vo.inventory.InventoryErrorVO;
import com.youyilin.warehouse.vo.inventory.InventoryPageVO;
import com.youyilin.warehouse.vo.inventory.InventoryVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 库存信息 控制层
 */
@RestController
@RequestMapping("warehouse.inventory")
public class InventoryController {

    private final InventoryLogic inventoryLogic;

    public InventoryController(InventoryLogic inventoryLogic) {
        this.inventoryLogic = inventoryLogic;
    }

    /**
     * 获取分页列表
     */
    @Log(title = "库存信息", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('inventory:page')")
    @GetMapping("getPageList")
    public R<Pager<InventoryPageVO>> getPageList(Page<InventoryPageQueryDTO> page, InventoryPageQueryDTO s) {
        page.setSearch(s);
        return R.succ(inventoryLogic.getPageList(page));
    }

    /**
     * 获取异常列表
     */
    @Log(title = "库存信息", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('inventory:page')")
    @GetMapping("listError")
    public R<List<InventoryErrorVO>> listError() {
        return R.succ(inventoryLogic.listError());
    }

    /**
     * 入库 按商品查询
     */
    @PreAuthorize("hasAuthority('inventory:queryIn')")
    @GetMapping("/listInventoryInByProductId/{productId}")
    public R<List<InventoryVO>> listInventoryInByProductId(@PathVariable Long productId) {
        return R.succ(inventoryLogic.queryIntoByProductId(productId));
    }

    /**
     * 出库 按SKU查询
     */
    @PreAuthorize("hasAuthority('inventory:queryOut')")
    @GetMapping("/listInventoryOutBySkuId/{skuId}")
    public R<List<InventoryVO>> listInventoryOutBySkuId(@PathVariable Long skuId) {
        return R.succ(inventoryLogic.queryOutBySkuId(skuId));
    }

    /**
     * 同步库存
     */
    @Log(title = "库存信息", businessType = BusinessTypeEnum.OTHER)
    @PreAuthorize("hasAuthority('inventory:async')")
    @GetMapping("asyncInventory")
    public R<String> asyncInventory() {
        inventoryLogic.asyncInventory();
        return R.success(RMsg.SUCCESS.getMsg());
    }

    /**
     * 删除库存信息
     */
    @Log(title = "库存信息", businessType = BusinessTypeEnum.DELETE)
    @PreAuthorize("hasAuthority('inventory:del')")
    @PostMapping("del")
    public R<String> del(@RequestBody @Valid InventoryDeleteFormDTO dto) {
        inventoryLogic.delInventory(dto);
        return R.success(RMsg.DELETE_SUCCESS.getMsg());
    }

    /**
     * 库存盘点 按名称精准匹配
     */
    @Log(title = "库存信息", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('inventory:check')")
    @GetMapping("listByProductName")
    public R<List<InventoryCheckVO>> listByProductName(String productName) {
        return R.succ(inventoryLogic.listByProductName(productName));
    }

    /**
     * 库存盘点 按名称模糊匹配
     */
    @Log(title = "库存信息", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('nventory:check')")
    @GetMapping("listLikeByProductName")
    public R<List<InventoryCheckVO>> listLikeByProductName(String productName) {
        return R.succ(inventoryLogic.listLikeByProductName(productName));
    }

    /**
     * 移动库存(整个移动)
     */
    @Log(title = "库存信息", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('inventory:move')")
    @PostMapping("move")
    public R<String> move(@RequestBody @Valid InventoryMoveFormDTO dto) {
        inventoryLogic.updateInventoryMove(dto);
        return R.success(RMsg.SUCCESS.getMsg());
    }
}
