package com.youyilin.controller.warehouse;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.logic.service.warehouse.WarehouseLogic;
import com.youyilin.warehouse.dto.warehouse.WarehouseDeleteFormDTO;
import com.youyilin.warehouse.dto.warehouse.WarehouseFormDTO;
import com.youyilin.warehouse.dto.warehouse.WarehousePageQueryDTO;
import com.youyilin.warehouse.vo.warehouse.WarehouseEditVO;
import com.youyilin.warehouse.vo.warehouse.WarehousePageVO;
import com.youyilin.warehouse.vo.warehouse.WarehouseVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 仓库 控制层
 */
@RestController
@RequestMapping("warehouse")
public class WarehouseController {

    private final WarehouseLogic warehouseLogic;

    public WarehouseController(WarehouseLogic warehouseLogic) {
        this.warehouseLogic = warehouseLogic;
    }

    /**
     * 获取分页列表
     */
    @Log(title = "仓库", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('warehouse:page')")
    @GetMapping("getPageList")
    public R<Pager<WarehousePageVO>> getPageList(Page<WarehousePageQueryDTO> page, WarehousePageQueryDTO s) {
        page.setSearch(s);
        return R.succ(warehouseLogic.getPageList(page));
    }

    /**
     * 获取编辑信息
     */
    @PreAuthorize("hasAuthority('warehouse:query')")
    @GetMapping("/queryEdit/{id}")
    public R<WarehouseEditVO> queryEdit(@PathVariable Long id) {
        return R.succ(warehouseLogic.getEditVOById(id));
    }

    /**
     * 获取所有仓库
     */
    @GetMapping("listAll")
    public R<List<WarehouseVO>> listAll() {
        return R.succ(warehouseLogic.listAll());
    }

    /**
     * 执行新增
     */
    @Log(title = "仓库", businessType = BusinessTypeEnum.INSERT)
    @PreAuthorize("hasAuthority('warehouse:add')")
    @PostMapping("add")
    public R<String> add(@RequestBody @Valid WarehouseFormDTO dto) {
        warehouseLogic.saveWarehouse(dto);
        return R.success(RMsg.ADD_SUCCESS.getMsg());
    }

    /**
     * 执行编辑
     */
    @Log(title = "仓库", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('warehouse:edit')")
    @PostMapping("edit")
    public R<String> edit(@RequestBody @Valid WarehouseFormDTO dto) {
        warehouseLogic.saveWarehouse(dto);
        return R.success(RMsg.EDIT_SUCCESS.getMsg());
    }

    /**
     * 执行删除
     */
    @Log(title = "仓库", businessType = BusinessTypeEnum.DELETE)
    @PreAuthorize("hasAuthority('warehouse:del')")
    @PostMapping("del")
    public R<String> del(@RequestBody @Valid WarehouseDeleteFormDTO dto) {
        warehouseLogic.delWarehouse(dto);
        return R.success(RMsg.DELETE_SUCCESS.getMsg());
    }
}
