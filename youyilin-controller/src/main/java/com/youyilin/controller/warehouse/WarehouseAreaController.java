package com.youyilin.controller.warehouse;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.logic.service.warehouse.WarehouseAreaLogic;
import com.youyilin.warehouse.dto.warehouseArea.WarehouseAreaDeleteFormDTO;
import com.youyilin.warehouse.dto.warehouseArea.WarehouseAreaFormDTO;
import com.youyilin.warehouse.dto.warehouseArea.WarehouseAreaPageQueryDTO;
import com.youyilin.warehouse.vo.warehouseArea.WarehouseAreaEditVO;
import com.youyilin.warehouse.vo.warehouseArea.WarehouseAreaPageVO;
import com.youyilin.warehouse.vo.warehouseArea.WarehouseAreaVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 仓库库区 控制层
 */
@RestController
@RequestMapping("warehouse.area")
public class WarehouseAreaController {

    private final WarehouseAreaLogic warehouseAreaLogic;

    public WarehouseAreaController(WarehouseAreaLogic warehouseAreaLogic) {
        this.warehouseAreaLogic = warehouseAreaLogic;
    }

    /**
     * 列表
     */
    @Log(title = "仓库库区", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('warehouse:area:page')")
    @GetMapping("getPageList")
    public R<Pager<WarehouseAreaPageVO>> getPageList(Page<WarehouseAreaPageQueryDTO> page, WarehouseAreaPageQueryDTO s) {
        page.setSearch(s);
        return R.succ(warehouseAreaLogic.getPageList(page));
    }

    /**
     * 查询
     */
    @PreAuthorize("hasAuthority('warehouse:area:query')")
    @GetMapping("/queryEdit/{id}")
    public R<WarehouseAreaEditVO> getById(@PathVariable Long id) {
        return R.succ(warehouseAreaLogic.getEditVOById(id));
    }

    /**
     * 所有库区
     */
    @GetMapping("listAll")
    public R<List<WarehouseAreaVO>> listAll() {
        return R.succ(warehouseAreaLogic.listAll());
    }

    /**
     * 某仓库所有库区
     */
    @GetMapping("/listAllByWarehouseId/{warehouseId}")
    public R<List<WarehouseAreaVO>> listAllByWarehouseId(@PathVariable Long warehouseId) {
        return R.succ(warehouseAreaLogic.listAllByWarehouseId(warehouseId));
    }

    /**
     * 新增
     */
    @Log(title = "仓库库区", businessType = BusinessTypeEnum.INSERT)
    @PreAuthorize("hasAuthority('warehouse:area:add')")
    @PostMapping("add")
    public R<String> add(@RequestBody @Valid WarehouseAreaFormDTO dto) {
        warehouseAreaLogic.saveWarehouseArea(dto);
        return R.success(RMsg.ADD_SUCCESS.getMsg());
    }

    /**
     * 编辑
     */
    @Log(title = "仓库库区", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('warehouse:area:edit')")
    @PostMapping("edit")
    public R<String> edit(@RequestBody @Valid WarehouseAreaFormDTO dto) {
        warehouseAreaLogic.saveWarehouseArea(dto);
        return R.success(RMsg.EDIT_SUCCESS.getMsg());
    }

    /**
     * 删除
     */
    @Log(title = "仓库库区", businessType = BusinessTypeEnum.DELETE)
    @PreAuthorize("hasAuthority('warehouse:area:del')")
    @PostMapping("del")
    public R<String> del(@RequestBody @Valid WarehouseAreaDeleteFormDTO dto) {
        warehouseAreaLogic.delWarehouseArea(dto);
        return R.success(RMsg.DELETE_SUCCESS.getMsg());
    }
}
