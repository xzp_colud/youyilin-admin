package com.youyilin.controller;

import com.youyilin.common.bean.R;
import com.youyilin.logic.service.DashboardLogic;
import com.youyilin.logic.vo.DashboardCustomerCardVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("dashboard")
public class DashboardController {

    @Autowired
    private DashboardLogic dashboardLogic;

    @GetMapping("customer")
    public R<DashboardCustomerCardVO> getDashboardCustomerCard() {
        return R.succ(dashboardLogic.getDashboardCustomerCard());
    }
}
