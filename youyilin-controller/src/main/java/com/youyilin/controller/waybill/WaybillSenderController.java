package com.youyilin.controller.waybill;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.bean.R;
import com.youyilin.common.bean.RMsg;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.logic.service.waybill.WaybillSenderLogic;
import com.youyilin.waybill.dto.sender.WaybillSenderFormDTO;
import com.youyilin.waybill.dto.sender.WaybillSenderPageQueryDTO;
import com.youyilin.waybill.vo.sender.WaybillSenderEditVO;
import com.youyilin.waybill.vo.sender.WaybillSenderPageVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 发件地址
 */
@RestController
@RequestMapping("waybill.sender")
public class WaybillSenderController {

    private final WaybillSenderLogic waybillSenderLogic;

    public WaybillSenderController(WaybillSenderLogic waybillSenderLogic) {
        this.waybillSenderLogic = waybillSenderLogic;
    }

    /**
     * 列表
     */
    @Log(title = "发件人", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('waybill:sender:page')")
    @GetMapping("getPageList")
    public R<Pager<WaybillSenderPageVO>> getPageList(Page<WaybillSenderPageQueryDTO> page, WaybillSenderPageQueryDTO s) {
        page.setSearch(s);
        return R.succ(waybillSenderLogic.getPageList(page));
    }

    /**
     * 查询
     */
    @Log(title = "发件人", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('waybill:sender:query')")
    @GetMapping("list")
    public R<List<WaybillSenderPageVO>> list() {
        return R.succ(waybillSenderLogic.listAllNormal());
    }

    /**
     * 查询
     */
    @Log(title = "发件人", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('waybill:sender:query')")
    @GetMapping("/queryEdit/{id}")
    public R<WaybillSenderEditVO> queryEdit(@PathVariable Long id) {
        return R.succ(waybillSenderLogic.getEditVOById(id));
    }

    /**
     * 新增
     */
    @Log(title = "发件人", businessType = BusinessTypeEnum.INSERT)
    @PreAuthorize("hasAuthority('waybill:sender:save')")
    @PostMapping("add")
    public R<String> add(@RequestBody @Valid WaybillSenderFormDTO dto) {
        waybillSenderLogic.saveSender(dto);
        return R.success(RMsg.ADD_SUCCESS.getMsg());
    }

    /**
     * 编辑
     */
    @Log(title = "发件人", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('waybill:sender:save')")
    @PostMapping("edit")
    public R<String> edit(@RequestBody @Valid WaybillSenderFormDTO dto) {
        waybillSenderLogic.saveSender(dto);
        return R.success(RMsg.EDIT_SUCCESS.getMsg());
    }
}
