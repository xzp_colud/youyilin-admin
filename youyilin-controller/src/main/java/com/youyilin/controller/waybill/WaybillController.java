package com.youyilin.controller.waybill;

import com.youyilin.common.annotation.Log;
import com.youyilin.common.bean.Page;
import com.youyilin.common.bean.Pager;
import com.youyilin.common.bean.R;
import com.youyilin.common.enums.BusinessTypeEnum;
import com.youyilin.logic.service.waybill.WaybillLogic;
import com.youyilin.waybill.dto.waybill.WaybillConfirmFormDTO;
import com.youyilin.waybill.dto.waybill.WaybillInfoFormDTO;
import com.youyilin.waybill.dto.waybill.WaybillPageQueryDTO;
import com.youyilin.waybill.vo.waybill.WaybillDetailVO;
import com.youyilin.waybill.vo.waybill.WaybillPageVO;
import com.youyilin.waybill.vo.waybill.WaybillVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 货运单
 */
@RestController
@RequestMapping("waybill")
public class WaybillController {

    private final WaybillLogic waybillLogic;

    public WaybillController(WaybillLogic waybillLogic) {
        this.waybillLogic = waybillLogic;
    }

    /**
     * 列表
     */
    @Log(title = "货运单", businessType = BusinessTypeEnum.PAGE)
    @PreAuthorize("hasAuthority('waybill:page')")
    @GetMapping("getPageList")
    public R<Pager<WaybillPageVO>> getPageList(Page<WaybillPageQueryDTO> page, WaybillPageQueryDTO s) {
        page.setSearch(s);
        return R.succ(waybillLogic.getPageList(page));
    }

    /**
     * 货运单
     */
    @Log(title = "货运单", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('waybill:query')")
    @GetMapping("/query/{id}")
    public R<WaybillVO> query(@PathVariable Long id) {
        return R.succ(waybillLogic.getVOById(id));
    }

    /**
     * 货运单详情
     */
    @Log(title = "货运单", businessType = BusinessTypeEnum.SELECT)
    @PreAuthorize("hasAuthority('waybill:query')")
    @GetMapping("/queryDetail/{id}")
    public R<WaybillDetailVO> queryDetail(@PathVariable Long id) {
        return R.succ(waybillLogic.getDetailVOById(id));
    }

    /**
     * 执行确认
     */
    @Log(title = "货运单", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('waybill:sure')")
    @PostMapping("sure")
    public R<String> sure(@RequestBody @Valid WaybillConfirmFormDTO dto) {
        waybillLogic.updateConfirm(dto);
        return R.success();
    }

    /**
     * 执行补充信息
     */
    @Log(title = "货运单", businessType = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('waybill:submit')")
    @PostMapping("submit")
    public R<String> submit(@RequestBody @Valid WaybillInfoFormDTO dto) {
        waybillLogic.updateInfo(dto);
        return R.success();
    }
}
